/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Security - ResourceServerConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.security.config;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.actuate.info.InfoEndpoint;
import org.springframework.boot.actuate.metrics.export.prometheus.PrometheusScrapeEndpoint;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.filter.ForwardedHeaderFilter;

import ch.unige.solidify.auth.client.service.JwtOpaqueTokenIntrospector;
import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.unige.aou.AouConstants;

@Configuration
@EnableWebSecurity
@Profile("!sec-noauth")
public class ResourceServerConfig {

  private final String[] privilegedRoles = { AuthApplicationRole.ADMIN_ID, AuthApplicationRole.TRUSTED_CLIENT_ID, AuthApplicationRole.ROOT_ID };
  private final String[] managementRoles = { AuthApplicationRole.TRUSTED_CLIENT_ID, AuthApplicationRole.ROOT_ID };

  private final JwtOpaqueTokenIntrospector jwtOpaqueTokenIntrospector;

  public ResourceServerConfig(JwtOpaqueTokenIntrospector jwtOpaqueTokenIntrospector) {
    this.jwtOpaqueTokenIntrospector = jwtOpaqueTokenIntrospector;
  }

  @Bean
  SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    // @formatter:off
    http
            .csrf(csrf -> csrf.ignoringRequestMatchers(AouConstants.getDisabledCsrfUrls()))
            .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
            .authorizeHttpRequests(req -> req
                    // Public URLs
                    .requestMatchers(AouConstants.getPublicUrls())
                    .permitAll()
                    // Public management endpoints
                    .requestMatchers(EndpointRequest.to(InfoEndpoint.class, PrometheusScrapeEndpoint.class))
                    .permitAll()
                    // Management endpoints
                    .requestMatchers(EndpointRequest.toAnyEndpoint())
                    .hasAnyRole(this.managementRoles)
                    // Else
                    .anyRequest()
                    .authenticated())
            .oauth2ResourceServer(oauth2 -> oauth2
                    .opaqueToken(token -> token
                            .introspector(this.jwtOpaqueTokenIntrospector)));

            // /admin/oauth2-clients
//            .mvcMatchers(UrlPath.ADMIN_OAUTH2_CLIENTS + "/**")
//            .hasAnyAuthority(this.privilegedRoles)

    // @formatter:on
    return http.build();
  }

  /**
   * Register a ForwardedHeaderFilter for correct HATOEAS links behind a proxy See
   * https://stackoverflow.com/questions/30020188/how-to-configure-spring-hateoas-behind-proxy
   */
  @Bean
  FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilter() {
    final FilterRegistrationBean<ForwardedHeaderFilter> bean = new FilterRegistrationBean<>();
    bean.setFilter(new ForwardedHeaderFilter());
    return bean;
  }

}
