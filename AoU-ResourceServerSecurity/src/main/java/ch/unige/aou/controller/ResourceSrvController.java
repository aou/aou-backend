/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Security - ResourceSrvController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.business.ApplicationRoleService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.UrlPath;

@RestController
@ConditionalOnExpression("'${aou.module.auth.enable:false}' == 'false'")
@RequestMapping(UrlPath.RES_SRV)
public class ResourceSrvController extends ModuleController {

  ResourceSrvController(AouProperties aouProperties, ApplicationRoleService applicationRoleService) {
    super(ModuleName.RES_SRV);
    // Module initialization
    if (aouProperties.getData().isInit()) {
      try {
        applicationRoleService.initDefaultData();
      } catch (final Exception e) {
        throw new SolidifyRuntimeException(e.getMessage());
      }
    }
  }
}
