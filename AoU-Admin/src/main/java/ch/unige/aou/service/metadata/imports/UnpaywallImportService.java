/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - UnpaywallImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.DownloadService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class UnpaywallImportService extends MetadataAndFileImportService {
  private static final Logger log = LoggerFactory.getLogger(UnpaywallImportService.class);

  private final String unpaywallApiUrl;
  private final String unpaywallEmail;

  private static final String UNPAYWALL_FILE_NAME = "_from_unpaywall.pdf";
  private static final String OPEN_ACCESS_LOCATION = "oa_locations";
  private static final String LICENSE_OTHER_AO = "OTHER-OA";
  private static final String LICENSE = "license";

  protected DownloadService downloadService;

  public UnpaywallImportService(AouProperties aouProperties, MessageService messageService, MetadataService metadataService,
          ContributorService contributorService, DocumentFileTypeService documentFileTypeService,
          DocumentFileService documentFileService, DownloadService downloadService, LicenseService licenseService,
          JournalTitleRemoteService journalTitleRemoteService) {
    super(aouProperties, messageService, metadataService, contributorService, documentFileService, documentFileTypeService,
            journalTitleRemoteService, licenseService);
    this.downloadService = downloadService;

    this.unpaywallApiUrl = aouProperties.getMetadata().getImports().getUnpaywall().getUrl();
    this.unpaywallEmail = aouProperties.getMetadata().getImports().getUnpaywall().getEmail();
  }

  public void createDocumentFileFromUnpaywall(Publication publication) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    DepositDoc depositDoc = (DepositDoc) metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    String doi = depositDoc.getIdentifiers().getDoi();
    if (!StringTool.isNullOrEmpty(doi)) {
      String unpaywallJson = this.getJsonFromUnpaywall(doi);
      JSONObject jsonObject = new JSONObject(unpaywallJson);

      this.createPublicationFromJsonResponse(publication, jsonObject);
    } else {
      log.info("There is no doi to check through Unpaywall");
    }
  }

  private void createPublicationFromJsonResponse(Publication publication, JSONObject jsonObject) {
    try {
      // first check if is_oa is true to know that there is an open access copy for the resource
      if (jsonObject.get("is_oa").equals(Boolean.TRUE)) {
        this.parseJsonResponse(publication, jsonObject);
      } else {
        log.info("The doi found through Unpaywall is not in open access");
      }
    } catch (JSONException | SolidifyRuntimeException e) {
      // catch JSONEXception and SolidifyRuntimeException to not block deposit process if Unpaywall import
      // fails
      log.warn("Unable to parse Json returned by Unpaywall", e);
    }
  }

  private void parseJsonResponse(Publication publication, JSONObject jsonObject) {
    String status = jsonObject.get("oa_status").toString();
    if (status.equals("green") || status.equals("gold") || status.equals("hybrid") || this.isBronzeStatus(status)) {

      String urlToDownload;
      String version;
      DocumentFile.AccessLevel accessLevel;
      License license = null;

      /**
       * Parse Unpaywall Json
       */
      if (this.isBronzeStatus(status) && jsonObject.has("has_repository_copy") && jsonObject.getBoolean("has_repository_copy")) {
        /**
         * Case of Bronze Open Access
         */
        Optional<JSONObject> firstLocationWithLicense = IntStream.range(0, jsonObject.getJSONArray(OPEN_ACCESS_LOCATION).length())
                .mapToObj(i -> jsonObject.getJSONArray(OPEN_ACCESS_LOCATION).getJSONObject(i))
                .filter(oa -> oa.has(LICENSE) && !StringTool.isNullOrEmpty(this.getStringOrNull(oa, LICENSE))).findFirst();

        JSONObject readLocation;
        if (firstLocationWithLicense.isPresent()) {
          // A location with license found
          readLocation = firstLocationWithLicense.get();
          accessLevel = DocumentFile.AccessLevel.PUBLIC;
          // in this case we use the OtherOA license by default
          license = this.getLicense(LICENSE_OTHER_AO);
        } else {
          // No location found without a license
          // We don't set any license on the file
          readLocation = jsonObject.getJSONArray(OPEN_ACCESS_LOCATION).getJSONObject(0);
          accessLevel = DocumentFile.AccessLevel.RESTRICTED;
        }
        urlToDownload = this.getStringOrNull(readLocation, "url_for_pdf");
        version = this.getStringOrNull(readLocation, "version");

      } else if (jsonObject.has("best_oa_location")) {
        /**
         * Case of real Open Access
         */
        JSONObject bestOALocation = jsonObject.getJSONObject("best_oa_location");
        String licenseName = this.getStringOrNull(bestOALocation, LICENSE);
        license = this.getLicense(licenseName);

        if (license != null) {
          // if the license name returned by Unpaywall matches one of your license, we use it and set the
          // access to public by default
          accessLevel = DocumentFile.AccessLevel.PUBLIC;
        } else {
          // if the license doesn't match one of the OaU license, we use the OtherOA license and set the
          // access to restricted by default
          license = this.getLicense(LICENSE_OTHER_AO);
          accessLevel = DocumentFile.AccessLevel.RESTRICTED;
        }
        urlToDownload = this.getStringOrNull(bestOALocation, "url_for_pdf");
        version = this.getStringOrNull(bestOALocation, "version");
      } else {
        throw new SolidifyRuntimeException("DOI is not bronze and hasn't any best location");
      }
      if (urlToDownload == null) {
        throw new SolidifyRuntimeException("There is no url to download the file in UnpaywallService");
      }
      this.createDocumentFile(publication, urlToDownload, license, accessLevel, version);
    } else {
      log.info("The license of the file is closed");
    }
  }

  private void createDocumentFile(Publication publication, String urlToDownload, License license, DocumentFile.AccessLevel accessLevel,
          String version) {
    /**
     * Create DocumentFile
     */
    try {
      DocumentFile documentFile = new DocumentFile();
      documentFile.setPublication(publication);
      documentFile.setSourceData(new URI(urlToDownload));
      documentFile.setLicense(license);
      documentFile.setAccessLevel(accessLevel);
      documentFile.setDocumentFileType(this.getDocumentFileTypeFromUnpaywallVersion(version));
      documentFile.setMimetype("application/pdf");
      documentFile.setFileName(CleanTool.cleanFileName(publication.getSubtype().getName() + UNPAYWALL_FILE_NAME));
      documentFile.setIsImported(true);
      this.saveDocumentFileIfSourceDataValid(documentFile);
    } catch (URISyntaxException e) {
      throw new SolidifyRuntimeException("Unable to get file URL from unpaywall", e);
    }
  }

  private License getLicense(String licenseName) {
    if (!StringTool.isNullOrEmpty(licenseName)) {
      return this.licenseService.findById(licenseName.toUpperCase()).orElse(null);
    }
    return null;
  }

  private DocumentFileType getDocumentFileTypeFromUnpaywallVersion(String version) {
    if (!StringTool.isNullOrEmpty(version)) {
      return switch (version) {
        case "publishedVersion" -> this.documentFileTypeService.findByValue("Article (Published version)");
        case "acceptedVersion" -> this.documentFileTypeService.findByValue("Article (Accepted version)");
        case "submittedVersion" -> this.documentFileTypeService.findByValue("Article (Submitted version)");
        default -> null;
      };
    }
    return null;
  }

  private String getJsonFromUnpaywall(String doi) {
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate client = restTemplateBuilder.build();
    return client.getForObject(this.unpaywallApiUrl, String.class, Map.of("doi", doi, "email", this.unpaywallEmail));
  }

  private boolean isBronzeStatus(String status) {
    return status.equals("bronze");
  }
}
