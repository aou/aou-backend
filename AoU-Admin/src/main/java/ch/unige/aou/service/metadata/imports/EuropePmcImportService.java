/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - EuropePmcImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;
import ch.unige.solidify.util.ZipTool;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouDownloadException;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.Funding;
import ch.unige.aou.service.DownloadService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class EuropePmcImportService extends MetadataAndFileImportService {

  private static final Logger log = LoggerFactory.getLogger(EuropePmcImportService.class);

  private static final String EUROPE_PMC_FILE_NAME = "_from_europePmc.pdf";

  private static final String XPATH_DEFAULT_ROOT = "/responseWrapper/resultList/result[pmcid='VALUE']";
  private static final String XPATH_HAS_SUPPLEMENTARY_FILES = EuropePmcImportService.XPATH_DEFAULT_ROOT + "/hasSuppl";
  private static final String XPATH_HAS_PDF = EuropePmcImportService.XPATH_DEFAULT_ROOT + "/hasPDF";
  private static final String XPATH_PDF_URL =
          EuropePmcImportService.XPATH_DEFAULT_ROOT + "/fullTextUrlList/fullTextUrl[documentStyle='pdf']/url";
  private static final String SUPPLEMENTARY_FILES_SUFFIX = "_SupplementaryFiles.zip";

  private String europeApiUrl;
  private String europeApiSupplementaryFilesUrl;

  protected DownloadService downloadService;

  public EuropePmcImportService(AouProperties aouProperties, MessageService messageService, MetadataService metadataService,
          ContributorService contributorService, DocumentFileTypeService documentFileTypeService, DocumentFileService documentFileService,
          LicenseService licenseService, DownloadService downloadService, JournalTitleRemoteService journalTitleRemoteService) {
    super(aouProperties, messageService, metadataService, contributorService, documentFileService, documentFileTypeService,
            journalTitleRemoteService, licenseService);
    this.downloadService = downloadService;

    this.europeApiUrl = aouProperties.getMetadata().getImports().getEuropePmc().getUrl();
    this.europeApiSupplementaryFilesUrl = aouProperties.getMetadata().getImports().getEuropePmc().getSupplementaryFiles().getUrl();
  }

  public DepositDoc searchEuropePmcId(String pmcId, DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(this.europeApiUrl)) {
      Document europePmcDoc = this.getXmlFromEuropePmc(pmcId);
      this.fillDepositDoc(depositDoc, europePmcDoc);
      return depositDoc;
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  private Document getXmlFromEuropePmc(String pmcId) {
    try {
      RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
      RestTemplate client = restTemplateBuilder.rootUri(this.europeApiUrl).build();

      Map<String, String> parameters = new HashMap<>();
      parameters.put("pmcId", pmcId);

      final String xmlString = client.getForObject(this.europeApiUrl, String.class, parameters);
      return XMLTool.parseXML(xmlString);
    } catch (HttpClientErrorException e) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
              this.messageService.get("deposit.error.identifiers.pmcid.not_found", new Object[] { pmcId }));
    } catch (IOException | ParserConfigurationException | SAXException e) {
      throw new SolidifyRuntimeException("An error occurred when searching PMCID '" + pmcId + "' on EuropePmc", e);
    }
  }

  public void fillDepositDoc(DepositDoc depositDoc, Document europePmcDoc) {
    this.fillContributorsOrcid(depositDoc, europePmcDoc,
            (EuropePmcImportService.XPATH_DEFAULT_ROOT + "/authorList/author").replace("VALUE", depositDoc.getIdentifiers().getPmcid()));
    this.fillFundings(depositDoc, europePmcDoc,
            (EuropePmcImportService.XPATH_DEFAULT_ROOT + "/grantsList/grant").replace("VALUE", depositDoc.getIdentifiers().getPmcid()));

  }

  private void fillFundings(DepositDoc depositDoc, Document europePmcDoc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList fundingList = (NodeList) xpath.compile(xpathQuery).evaluate(europePmcDoc, XPathConstants.NODESET);

      if (fundingList.getLength() > 0) {
        this.ensureDepositFundingAgencyExist(depositDoc);
      }
      for (int i = 0; i < fundingList.getLength(); i++) {
        String funderCode = XMLTool.getFirstTextContent(fundingList.item(i), "./grantId");
        String funder = XMLTool.getFirstTextContent(fundingList.item(i), "./agency");

        boolean noExistFunding = depositDoc.getFundings().getFunding().stream().noneMatch(f -> {
          boolean isFunderEqual = false;
          if (f.getFunder() != null && funder != null) {
            isFunderEqual = f.getFunder().equals(funder);
          }
          if (f.getCode() != null && funderCode != null) {
            isFunderEqual = isFunderEqual || f.getCode().equals(funderCode);
          }
          return isFunderEqual;
        });

        if (noExistFunding) {
          Funding funding = new Funding();
          funding.setFunder(funder);
          funding.setCode(funderCode);

          depositDoc.getFundings().getFunding().add(funding);
        }
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill funding agencies", e);
    }
  }

  private void fillContributorsOrcid(DepositDoc depositDoc, Document europePmcDoc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList authorsList = (NodeList) xpath.compile(xpathQuery).evaluate(europePmcDoc, XPathConstants.NODESET);

      if (authorsList.getLength() > 0) {
        this.ensureContributorsExist(depositDoc);
      }
      for (int i = 0; i < this.getNumberMaxContributor(authorsList.getLength()); i++) {
        String orcid = XMLTool.getFirstTextContent(authorsList.item(i), "./authorId[@type='ORCID']");
        if (!StringTool.isNullOrEmpty(orcid)) {
          String firstName = XMLTool.getFirstTextContent(authorsList.item(i), "./firstName");
          String lastName = XMLTool.getFirstTextContent(authorsList.item(i), "./lastName");

          //check if there are homonyms, if so the orcid is not saved since it is not possible to know who it belongs to
          if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                  .filter(c -> (c instanceof Contributor) && ((Contributor) c).getFirstname().equals(firstName)
                          && ((Contributor) c).getLastname().equals(lastName))
                  .count() == 1) {
            ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().stream()
                    .filter(c -> (c instanceof Contributor) && ((Contributor) c).getFirstname().equals(firstName)
                            && ((Contributor) c).getLastname().equals(lastName))
                    .findFirst().orElseThrow()).setOrcid(orcid);
          }
        }
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill contributors from europePmc", e);
    }
  }

  public DepositDoc createDocumentFileFromEuropePmc(Publication publication) throws IOException, NoSuchAlgorithmException {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    DepositDoc depositDoc = (DepositDoc) metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    String pmcId = depositDoc.getIdentifiers().getPmcid();
    Document pmcDoc = this.getXmlFromEuropePmc(pmcId);

    //For the supplementary files -> file type (Appendix), access level Public and license empty
    //For the pdf -> file type Published version, access public, License value from europePmc
    if (this.containsSupplementaryFile(pmcDoc, pmcId)) {
      try {
        this.downloadSupplementaryFilesFromEuropePmc(pmcId, publication);
      } catch (AouDownloadException e) {
        log.warn("Unable to download supplementary files from Europe PMC", e);
      }
    }

    if (this.containsPDF(pmcDoc, pmcId)) {
      String pdfUrl = this.getPdfUrl(pmcDoc, pmcId);
      String licenseName = this.getLicense(pmcDoc, pmcId);

      License license = this.licenseService.findByOpenLicenseId(licenseName);

      DocumentFileType documentFileType = this.documentFileTypeService.findByValue("Article (Published version)");

      DocumentFile documentFile = new DocumentFile();
      documentFile.setPublication(publication);
      documentFile.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);
      documentFile.setDocumentFileType(documentFileType);
      documentFile.setMimetype("application/pdf");
      documentFile.setFileName(CleanTool.cleanFileName(publication.getSubtype().getName() + EUROPE_PMC_FILE_NAME));
      documentFile.setLicense(license);
      documentFile.setIsImported(true);
      try {
        documentFile.setSourceData(new URI(pdfUrl));
      } catch (URISyntaxException e) {
        throw new SolidifyRuntimeException("Unable to get file URL from arXiv", e);
      }
      this.saveDocumentFileIfSourceDataValid(documentFile);
    }
    return depositDoc;
  }

  private void downloadSupplementaryFilesFromEuropePmc(String pmcId, Publication publication)
          throws IOException, NoSuchAlgorithmException {
    /**
     * Download supplementary files in a zip and save them in a temporary folder
     */
    String zipName = pmcId + EuropePmcImportService.SUPPLEMENTARY_FILES_SUFFIX;
    final Path tmpFolder = Paths.get(this.tempFolder).resolve(publication.getResId());
    if (!FileTool.ensureFolderExists(tmpFolder)) {
      throw new IOException("Impossible to create temporary folder");
    }
    String supplementaryFilesUrl = this.europeApiSupplementaryFilesUrl.replace("{pmcId}", pmcId);
    final String destinationFilepath = tmpFolder + "/" + zipName;

    try {
      this.downloadService.download(new URI(supplementaryFilesUrl), Paths.get(destinationFilepath));
    } catch (URISyntaxException e) {
      throw new IOException("Impossible to create URI from europePmc SupplementaryFiles url");
    }

    /*
     * Create folder where zipped files must be extracted
     */
    final String zipFolderName = Paths.get(destinationFilepath).getFileName().toString();
    final String fileHash = ChecksumTool.computeChecksum(
            new BufferedInputStream(new FileInputStream(destinationFilepath), SolidifyConstants.BUFFER_SIZE), ChecksumAlgorithm.CRC32);
    final String extractFolder = tmpFolder + "/unzip/" + zipFolderName + "_" + fileHash + "_" + Instant.now().toEpochMilli();
    final Path extractFolderPath = Paths.get(extractFolder);

    if (!FileTool.ensureFolderExists(extractFolderPath)) {
      throw new IOException("Impossible to create folder to extract zip");
    }

    final ZipTool zip = new ZipTool(destinationFilepath);

    if (zip.checkZip()) {

      if (zip.unzipFiles(extractFolderPath)) {

        /*
         * Delete ZIP file
         */
        FileTool.deleteFile(Paths.get(destinationFilepath));

        /*
         * Loop on extracted files and create a DepositDatafile for each
         */
        final List<Path> extractedFilesPaths = FileTool.findFiles(extractFolderPath);

        DocumentFileType documentFileType = this.documentFileTypeService.findByValue("Appendix");

        for (final Path extractedFilePath : extractedFilesPaths) {
          // note: do not set fileName property as it is automatically set and cleaned in DocumentFileProcessingService
          DocumentFile documentFile = new DocumentFile();
          documentFile.setPublication(publication);
          documentFile.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);
          documentFile.setDocumentFileType(documentFileType);
          documentFile.setSourceData(extractedFilePath.toUri());
          documentFile.setIsImported(true);
          this.saveDocumentFileIfSourceDataValid(documentFile);
        }

      } else {
        throw new IOException("Impossible to unzip zip download from EuropePmc");
      }
    }
  }

  private boolean containsPDF(Document pmcDoc, String pmcId) {
    String value = XMLTool.getFirstTextContent(pmcDoc, EuropePmcImportService.XPATH_HAS_PDF.replace("VALUE", pmcId));
    if (!StringTool.isNullOrEmpty(value)) {
      return value.equals("Y");
    }
    return false;
  }

  private boolean containsSupplementaryFile(Document pmcDoc, String pmcId) {
    String value = XMLTool.getFirstTextContent(pmcDoc, EuropePmcImportService.XPATH_HAS_SUPPLEMENTARY_FILES.replace("VALUE", pmcId));
    if (!StringTool.isNullOrEmpty(value)) {
      return value.equals("Y");
    }
    return false;
  }

  private String getLicense(Document pmcDoc, String pmcId) {
    String queryXPath = EuropePmcImportService.XPATH_DEFAULT_ROOT + "/license";
    return XMLTool.getFirstTextContent(pmcDoc, queryXPath.replace("VALUE", pmcId));
  }

  private String getPdfUrl(Document pmcDoc, String pmcId) {
    String xpath = EuropePmcImportService.XPATH_PDF_URL.replace("VALUE", pmcId);
    return XMLTool.getFirstTextContent(pmcDoc, xpath);
  }
}
