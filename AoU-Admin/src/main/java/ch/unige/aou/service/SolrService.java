/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - SolrService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.business.PublicationService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.PublicationFromFedoraMessage;

@Service
@ConditionalOnBean(AdminController.class)
public class SolrService extends AouService {
  private static final Logger log = LoggerFactory.getLogger(SolrService.class);

  private String solrUrl;
  private final PublicationService publicationService;

  private static final String TOTAL_ITEMS = "numFound";
  private static final String PID = "pid";
  private static final String DOCS = "docs";

  public SolrService(AouProperties aouProperties, MessageService messageService, PublicationService publicationService) {
    super(messageService);
    this.publicationService = publicationService;
    this.solrUrl = aouProperties.getStorage().getSolrUrl();
  }

  public boolean startPublicationsImportFromFedora(String personId, String solrConditions) {
    //Call to solr api
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate client = restTemplateBuilder.build();

    String solrUrlWithConditions = this.solrUrl.replace("{solrConditions}", solrConditions);

    final String jsonString = client.getForObject(solrUrlWithConditions, String.class);
    JSONObject jsonObject = new JSONObject(jsonString).getJSONObject("response");

    int totalItems = jsonObject.getInt(TOTAL_ITEMS);
    if (totalItems <= 0) {
      log.info("There is no publication from solr api corresponding to '{}' condition", solrConditions);
      return false;
    } else {
      log.info("{} objects will be verified to check if they need to be imported from Fedora}", totalItems);
    }

    //Get each pid from the response and create a jms message
    JSONArray pidsArray = this.getArrayOrNull(jsonObject, DOCS);
    List<String> pidsFromFedora = new ArrayList<>();

    if (pidsArray != null && !pidsArray.isEmpty()) {
      pidsFromFedora = StreamSupport.stream(pidsArray.spliterator(), false)
              .map(a -> (JSONObject) a)
              .map(a -> a.getString(PID))
              .toList();
    }

    if (!pidsFromFedora.isEmpty()) {
      List<String> existingArchiveIds = this.publicationService.getExistingArchiveIds(pidsFromFedora);
      int pidToImportTotal = pidsFromFedora.size() - existingArchiveIds.size();
      log.info("{} objects already exist in database", existingArchiveIds.size());
      log.info("{} objects must be imported from Fedora", pidToImportTotal);
      int i = 1;
      for (String pid : pidsFromFedora) {
        if (!existingArchiveIds.contains(pid)) {
          SolidifyEventPublisher.getPublisher().publishEvent(new PublicationFromFedoraMessage(pid, personId, i++ + " / " + pidToImportTotal));
        } else {
          log.debug("pid {} already exists in database", pid);
        }
      }
    }

    return true;
  }

  protected JSONArray getArrayOrNull(JSONObject jsonObject, String propertyName) {
    try {
      if (jsonObject != null && jsonObject.has(propertyName) && !jsonObject.isNull(propertyName)) {
        return jsonObject.getJSONArray(propertyName);
      }
    } catch (JSONException e) {
      return null;
    }
    return null;
  }
}
