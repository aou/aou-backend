/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - GoogleIsbnImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class CongressLibraryIsbnImportService extends MetadataImportService {
  private static final Logger logger = LoggerFactory.getLogger(CongressLibraryIsbnImportService.class);

  private final String apiUrl;
  private static final String RESULTS = "results";
  private static final String ITEM = "item";
  private static final String DESCRIPTION = "description";
  private static final String TITLE = "title";
  private static final String DATE = "date";
  private static final String CREATED_PUBLISHED = "created_published";
  private static final String CONTRIBUTORS = "contributors";
  private static final String LANGUAGE = "language";
  private static final String SUBJECTS = "subjects";
  private static final String ORIGINAL_FORMAT = "original_format";

  public CongressLibraryIsbnImportService(AouProperties aouProperties, MessageService messageService,
          MetadataService metadataService, ContributorService contributorService, JournalTitleRemoteService journalTitleRemoteService,
          LicenseService licenseService) {
    super(aouProperties, messageService, metadataService, contributorService, journalTitleRemoteService, licenseService);
    this.apiUrl = aouProperties.getMetadata().getImports().getIsbn().getCongressLibraryUrl();
  }

  public DepositDoc searchInCongressLibrary(String isbn, DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(this.apiUrl)) {
      logger.debug("Searching congress library for isbn {}", isbn);
      String loc = this.getJsonFromCongressLibrary(isbn); // loc = Library Of Congress
      JSONObject jsonObject = new JSONObject(loc);
      this.fillDepositDoc(isbn, depositDoc, jsonObject);

      return depositDoc;
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  public void fillDepositDoc(String isbn, DepositDoc depositDoc, JSONObject googleJson) {
    //check pagination.to to see if there is any result before getting all the info
    int totalItems = googleJson.getJSONObject("pagination").getInt("to");
    if (totalItems != 1) {
      throw new SolidifyRuntimeException("There is none or more than one result for the isbn specified");
    }
    JSONObject locBookJson = googleJson.getJSONArray(RESULTS).getJSONObject(0);

    this.fillTitle(depositDoc, locBookJson);
    this.fillAbstract(depositDoc, locBookJson);
    this.fillPublicationDate(depositDoc, locBookJson);
    this.fillPublisher(depositDoc, locBookJson.getJSONObject(ITEM));
    this.fillContributors(depositDoc, locBookJson.getJSONObject(ITEM));
    this.fillKeywords(depositDoc, locBookJson.getJSONObject(ITEM));
    this.fillLanguage(depositDoc, locBookJson.getJSONObject(ITEM));
    this.fillPublicationType(depositDoc, locBookJson);
    this.fillIsbn(depositDoc, isbn);

  }

  private void fillPublicationType(DepositDoc depositDoc, JSONObject jsonObject) {
    Object publicationType = this.getArrayOrNull(jsonObject, ORIGINAL_FORMAT).get(0);
    if (!StringTool.isNullOrEmpty(publicationType.toString())) {
      //There are two different values: book or magazine
      String subType = this.getPublicationSubType(publicationType.toString());
      String type = this.getPublicationType(publicationType.toString());
      depositDoc.setSubtype(subType);
      depositDoc.setType(type);
    }
  }

  private String getPublicationType(String value) {
    return switch (value) {
      case "BOOK", "book"-> AouConstants.DEPOSIT_TYPE_LIVRE_NAME;
      case "MAGAZINE", "magazine" -> AouConstants.DEPOSIT_TYPE_JOURNAL_NAME;
      default -> null;
    };
  }

  private String getPublicationSubType(String value) {
    return switch (value) {
      case "BOOK", "book" -> AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME;
      case "MAGAZINE" , "magazine" -> AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME;
      default -> null;
    };
  }

  private void fillTitle(DepositDoc depositDoc, JSONObject jsonObject) {
    String value = this.getStringOrNull(jsonObject, CongressLibraryIsbnImportService.TITLE);
    if (!StringTool.isNullOrEmpty(value)) {
      logger.debug("Congress library importer , fill title : {}", value);
      depositDoc.setTitle(this.getText(value));
    }
  }

  private void fillAbstract(DepositDoc depositDoc, JSONObject jsonObject) {
    Object abstractTxt = jsonObject.getJSONArray(DESCRIPTION).get(0);
    if (!StringTool.isNullOrEmpty(abstractTxt.toString())) {
      logger.debug("Congress library importer , fill Abstract : {}", abstractTxt);
      this.ensureAbstractsExist(depositDoc);
      depositDoc.getAbstracts().getAbstract().add(this.getText(abstractTxt.toString()));
    }
  }

  private void fillPublicationDate(DepositDoc depositDoc, JSONObject jsonObject) {
    String value = this.getStringOrNull(jsonObject, DATE);
    if (!StringTool.isNullOrEmpty(value)) {
      logger.debug("Congress library importer , fill publication date : {}", value);
      this.ensureDatesExist(depositDoc);
      depositDoc.getDates().getDate().add(this.getDateWithType(DateTypes.PUBLICATION, value));
    }
  }

  private void fillPublisher(DepositDoc depositDoc, JSONObject jsonObject) {
    Object createdPublisher = jsonObject.getJSONArray(CREATED_PUBLISHED).get(0);
    if (!StringTool.isNullOrEmpty(createdPublisher.toString())) {
      logger.debug("Congress library importer , fill publisher : {}", createdPublisher);
      this.ensurePublisherExist(depositDoc);
      depositDoc.getPublisher().getNameOrPlace().add(this.factory.createPublisherName(createdPublisher.toString()));
    }
  }

  private void fillContributors(DepositDoc depositDoc, JSONObject jsonObject) {
    JSONArray contributorArray = jsonObject.getJSONArray(CONTRIBUTORS);
    if (contributorArray != null && !contributorArray.isEmpty()) {
      this.ensureContributorsExist(depositDoc);
      for (int i = 0; i < this.getNumberMaxContributor(contributorArray.length()); i++) {
        String authorName = contributorArray.getString(i);
        logger.debug("Congress library importer , fill contributor : {}", authorName);

        if (!StringTool.isNullOrEmpty(authorName)) {
          String[] splitContributor = authorName.split(",");  // the contributor is formatted like : "lastName, firstName, role.",
          // Cut the author name on the last space, assuming the last word is the lastName
          String lastName = this.getNameOrUndefinedValue(splitContributor[0]);
          String firstName = this.getNameOrUndefinedValue(splitContributor[1]);

          Contributor contributor = new Contributor();
          contributor.setLastname(CleanTool.capitalizeString(lastName));
          contributor.setFirstname(CleanTool.capitalizeString(firstName));
          depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
        }
      }
    }
  }

  private void fillKeywords(DepositDoc depositDoc, JSONObject jsonObject) {
    JSONArray keywordArray = this.getArrayOrNull(jsonObject, SUBJECTS);
    if (keywordArray != null && !keywordArray.isEmpty()) {
      this.ensureKeywordsExist(depositDoc);
      for (int i = 0; i < keywordArray.length(); i++) {
        String value = keywordArray.getString(i);
        logger.debug("Congress library importer , fill keyword : {}", value);
        if (!StringTool.isNullOrEmpty(value)) {
          depositDoc.getKeywords().getKeyword().add(value);
        }
      }
    }
  }

  private void fillLanguage(DepositDoc depositDoc, JSONObject jsonObject) {
    Object language = this.getArrayOrNull(jsonObject, LANGUAGE).get(0);
    if (!StringTool.isNullOrEmpty(language.toString())) {
      logger.debug("Congress library importer , fill keyword : {}", language);
      this.ensureLanguagesExist(depositDoc);
      depositDoc.getLanguages().getLanguage().add(CleanTool.getLanguageCode(language.toString()));
    }
  }

  private void fillIsbn(DepositDoc depositDoc, String isbnValue) {
    this.ensureDepositIdentifiersExist(depositDoc);
    depositDoc.getIdentifiers().setIsbn(isbnValue);
  }

  private String getJsonFromCongressLibrary(String isbn) {
    logger.debug("Call Congress library API: {} , for isnb: {}", this.apiUrl, isbn);
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate client = restTemplateBuilder.build();
    return client.getForObject(this.apiUrl, String.class, Map.of("id", isbn));
  }

}
