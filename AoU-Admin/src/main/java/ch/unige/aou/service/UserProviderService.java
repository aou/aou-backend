/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - UserProviderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.service.UserInfoService;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.repository.UserRepository;

/**
 * Implementation of UserInfoService is not done in UserService in order to prevent a circular dependency problem
 */
@Service
@Primary
@ConditionalOnBean(AdminController.class)
public class UserProviderService implements UserInfoService {

  private UserRepository userRepository;

  public UserProviderService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserInfo findByExternalUid(String externalUid) {
    return this.userRepository.findByExternalUid(externalUid);
  }
}
