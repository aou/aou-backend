/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.SolidifyTime;

import ch.unige.aou.business.PublicationService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.PublicationMessage;
import ch.unige.aou.message.PublicationMetadataUpgradeMessage;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.Publication.PublicationStatus;

@Service
@ConditionalOnBean(AdminController.class)
public class PublicationJmsListener extends MessageProcessor<PublicationMessage> {

  private static final Logger log = LoggerFactory.getLogger(PublicationJmsListener.class);

  private final PublicationProcessingService publicationProcessingService;
  private final PublicationService publicationService;

  public PublicationJmsListener(AouProperties aouProperties, PublicationProcessingService publicationProcessingService,
          PublicationService publicationService) {
    super(aouProperties);
    this.publicationProcessingService = publicationProcessingService;
    this.publicationService = publicationService;
  }

  @Override
  public void processMessage(PublicationMessage publicationMessage) {
    log.info("Reading message {}", publicationMessage);
    try {
      Publication publication = this.publicationService.findOne(publicationMessage.getResId());
      while (this.isPublicationInProgress(publication)) {
        // Process publication
        publication = this.publicationProcessingService.processPublication(publication.getResId());
        SolidifyTime.waitInMilliSeconds(200);
      }
      log.info("Publication '{}' ({}) processed", publication.getResId(), publication.getTitle());

    } catch (NoSuchElementException e) {
      log.error("Cannot find publication ({})", publicationMessage.getResId());
    } catch (Exception e) {
      log.error("An error occurred while saving publication", e);
    }
  }

  private boolean isPublicationInProgress(Publication publication) {
    return (publication.getStatus() == PublicationStatus.SUBMITTED || publication.getStatus() == PublicationStatus.CHECKED
            || publication.getStatus() == PublicationStatus.IN_PREPARATION || publication.getStatus() == PublicationStatus.CANCEL_EDITION
            || publication.getStatus() == PublicationStatus.UPDATE_IN_BATCH);
  }

  @JmsListener(destination = "${aou.queue.publications}")
  @Override
  public void receiveMessage(PublicationMessage publicationMessage) {
    this.sendForParallelProcessing(publicationMessage);
  }

  @JmsListener(destination = "${aou.queue.publicationsMetadataUpgrade}")
  public void receiveMessage(PublicationMetadataUpgradeMessage publicationMessage) {
    Publication publication = this.publicationProcessingService.processMetadataToUpgradePublication(publicationMessage.getResId());
    log.info("Publication metadata upgrade processed '{}' (stored metadata version : {}) ", publication.getResId(),
            publication.getMetadataVersion());
  }
}
