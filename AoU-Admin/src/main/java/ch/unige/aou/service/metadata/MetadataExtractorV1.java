/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataExtractorV1.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.io.StringReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import jakarta.xml.bind.JAXB;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LabeledLanguageService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.LabeledLanguage;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.xml.deposit.v1.Abstracts;
import ch.unige.aou.model.xml.deposit.v1.AcademicStructure;
import ch.unige.aou.model.xml.deposit.v1.AcademicStructures;
import ch.unige.aou.model.xml.deposit.v1.AccessLevel;
import ch.unige.aou.model.xml.deposit.v1.AuthorRole;
import ch.unige.aou.model.xml.deposit.v1.Citations;
import ch.unige.aou.model.xml.deposit.v1.Classifications;
import ch.unige.aou.model.xml.deposit.v1.Collections;
import ch.unige.aou.model.xml.deposit.v1.Container;
import ch.unige.aou.model.xml.deposit.v1.ContainerIdentifiers;
import ch.unige.aou.model.xml.deposit.v1.Contributors;
import ch.unige.aou.model.xml.deposit.v1.Dataset;
import ch.unige.aou.model.xml.deposit.v1.DateWithType;
import ch.unige.aou.model.xml.deposit.v1.Dates;
import ch.unige.aou.model.xml.deposit.v1.DepositDoc;
import ch.unige.aou.model.xml.deposit.v1.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v1.Dois;
import ch.unige.aou.model.xml.deposit.v1.Embargo;
import ch.unige.aou.model.xml.deposit.v1.File;
import ch.unige.aou.model.xml.deposit.v1.FileType;
import ch.unige.aou.model.xml.deposit.v1.Files;
import ch.unige.aou.model.xml.deposit.v1.Fundings;
import ch.unige.aou.model.xml.deposit.v1.Group;
import ch.unige.aou.model.xml.deposit.v1.Groups;
import ch.unige.aou.model.xml.deposit.v1.Keywords;
import ch.unige.aou.model.xml.deposit.v1.Links;
import ch.unige.aou.model.xml.deposit.v1.ObjectFactory;
import ch.unige.aou.model.xml.deposit.v1.Pages;
import ch.unige.aou.model.xml.deposit.v1.Publisher;
import ch.unige.aou.model.xml.deposit.v1.Rights;
import ch.unige.aou.model.xml.deposit.v1.Text;
import ch.unige.aou.model.xml.deposit.v1.TextLang;
import ch.unige.aou.model.xml.deposit.v1.deserializer.AbstractsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.AcademicsStructuresDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.CitationsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.ClassificationsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.CollectionsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.ContainerDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.ContainerIdentifiersDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.ContributorsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.DatasetDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.DateWithTypeDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.DatesDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.DepositIdentifiersDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.DoisDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.FilesDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.FundingsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.GroupsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.KeywordsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.LinksDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.PagesDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.PublisherDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.RightsDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.TextDeserializer;
import ch.unige.aou.model.xml.deposit.v1.deserializer.TextLangDeserializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.AbstractsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.AcademicStructuresSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.AuthorRoleSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.CitationsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.ClassificationsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.CollectionsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.ContainerIdentifiersSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.ContributorsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.DatasetSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.DateWithTypeSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.DatesSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.DepositDocSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.DepositIdentifiersSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.DoisSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.FilesSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.FundingsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.GroupsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.KeywordsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.LinksSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.PagesSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.PublisherSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.RightsSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.TextLangSerializer;
import ch.unige.aou.model.xml.deposit.v1.serializer.TextSerializer;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.rest.UnigePersonSearchService;

@Service
@ConditionalOnBean(AdminController.class)
public class MetadataExtractorV1 extends MetadataExtractor {

  ObjectFactory factory = new ObjectFactory();

  public MetadataExtractorV1(AouProperties aouProperties, PublicationTypeService publicationTypeService,
          PublicationSubtypeService publicationSubtypeService, PublicationSubSubtypeService publicationSubSubtypeService,
          StructureService structureService, ResearchGroupService researchGroupService, ContributorService contributorService,
          LabeledLanguageService labeledLanguageService, JournalTitleRemoteService journalTitleRemoteService, PersonService personService,
          MessageService messageService, UnigePersonSearchService unigePersonSearchService) {
    super(aouProperties, new DepositDocV1Adapter(), publicationTypeService, publicationSubtypeService, publicationSubSubtypeService,
            structureService, researchGroupService, contributorService, labeledLanguageService, journalTitleRemoteService, personService,
            messageService, unigePersonSearchService);
  }

  @Override
  public String transformFormDataToMetadata(String jsonData) {
    DepositDoc depositDoc = this.createDepositDocFromJsonFormData(jsonData);
    return this.serializeDepositDocToXml(depositDoc);
  }

  @Override
  public String getPmcIdFromPublication(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
    return this.getPmcId(depositDoc);
  }

  @Override
  public DepositDoc createDepositDocFromJsonFormData(String jsonData) {

    try {
      ObjectMapper mapper = this.getJsonFormDeserializerMapper();

      JsonNode root = mapper.readTree(jsonData);
      JsonNode typeNode = root.at("/type");
      JsonNode contributorsNode = root.at("/contributors");
      JsonNode descriptionNode = root.at("/description");

      DepositDoc depositDoc = mapper.readValue(typeNode.toString(), DepositDoc.class);
      ObjectReader updater = mapper.readerForUpdating(depositDoc);
      depositDoc = updater.readValue(contributorsNode.toString());
      updater = mapper.readerForUpdating(depositDoc);
      depositDoc = updater.readValue(descriptionNode.toString());

      this.replaceFormValuesByMetadataValues(depositDoc);

      return depositDoc;

    } catch (JsonProcessingException e) {
      if (e.getCause() instanceof SolidifyValidationException) {
        throw (SolidifyValidationException) e.getCause();
      }
      throw new SolidifyRuntimeException("unable to parse json data", e);
    }
  }

  @Override
  public String transformMetadataToFormData(String xmlData) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(xmlData);

    this.replaceMetadataValuesByFormValues(depositDoc);

    try {
      ObjectMapper mapper = this.getJsonFormSerializerMapper();
      return mapper.writeValueAsString(depositDoc);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to transform XML data to Json form data", e);
    }
  }

  /**
   * Save the document files properties into the metadata content
   */
  @Override
  public void completeMetadataWithFiles(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    Files files = new Files();
    for (DocumentFile df : publication.getDocumentFiles()) {
      File xmlFile = this.convertDocumentFileToXmlFile(df);
      files.getFile().add(xmlFile);
    }
    depositDoc.setFiles(files);
    //add into the publication metadata the info from file
    String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(xmlMetadata);
  }

  @Override
  public void resetUnigeContributorsDisplayName(Publication publication) {
    // do nothing in V1
  }

  @Override
  public void replaceUnigeContributorsDisplayName(Publication publication) {
    // do nothing in V1
  }

  @Override
  public void completeClassificationsMetadataWithDeweyCodes(Publication publication) {
    // do nothing in V1
  }

  @Override
  public void completeStructureResId(Publication publication) {
    // do nothing in V1
  }

  @Override
  public void completeResearchGroupResId(Publication publication) {
    // do nothing in V1
  }

  @Override
  public void completeContributorsCnIndividuFromOrcid(Publication publication) {
    // do nothing in V1
  }

  @Override
  protected void formatDepositDocDatesToFrontendFormat(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getDates() != null && depositDoc.getDates().getDate() != null) {
      List<DateWithType> dates = depositDoc.getDates().getDate();
      for (DateWithType date : dates) {
        if (!StringTool.isNullOrEmpty(date.getContent())) {
          String dateStr = this.updateDateFromMetadataToFrontendFormat(date.getContent());
          date.setContent(dateStr);
        }
      }
    }
  }

  @Override
  protected void formatDepositDocDatesToMetadataFormat(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getDates() != null && depositDoc.getDates().getDate() != null) {
      List<DateWithType> dates = depositDoc.getDates().getDate();
      for (DateWithType date : dates) {
        if (!StringTool.isNullOrEmpty(date.getContent())) {
          String dateStr = this.updateDateFromFrontendToMetadataFormat(date.getContent());
          date.setContent(dateStr);
        }
      }
    }
  }

  @Override
  public String serializeDepositDocToXml(Object depositDocObject) {
    return this.depositDocAdapter.serializeDepositDocToXml(depositDocObject);
  }

  @Override
  public Object createDepositDocObjectFromXml(String xmlData) {
    if (xmlData == null) {
      throw new SolidifyValidationException(new ValidationError("XmlData is null. Unable to deserialize XML to DepositDoc."));
    }
    return JAXB.unmarshal(new StringReader(xmlData), DepositDoc.class);
  }

  @Override
  public void replaceIdsByTextValues(Object depositDocObj) {
    super.replaceIdsByTextValues(depositDocObj);

    /*
     * Language
     */
    String languageStr = this.getLanguage(depositDocObj);
    if (!StringTool.isNullOrEmpty(languageStr)) {
      Optional<LabeledLanguage> language = this.labeledLanguageService.findById(languageStr);
      language.ifPresent(lang -> this.setLanguage(depositDocObj, lang.getCode()));
    }

    /*
     * Container's title language
     */
    String containerTitleLangStr = this.getContainerTitleLang(depositDocObj);
    if (!StringTool.isNullOrEmpty(containerTitleLangStr)) {
      Optional<LabeledLanguage> language = this.labeledLanguageService.findById(containerTitleLangStr);
      language.ifPresent(lang -> this.setContainerTitleLang(depositDocObj, lang.getCode()));
    }

    /*
     * Citations' languages
     */
    List<MetadataTextLang> citations = this.getCitations(depositDocObj);
    if (citations != null) {
      for (MetadataTextLang citation : citations) {
        if (!StringTool.isNullOrEmpty(citation.getLang())) {
          Optional<LabeledLanguage> language = this.labeledLanguageService.findById(citation.getLang());
          language.ifPresent(lang -> citation.setLang(lang.getCode()));
        }
      }
      this.setCitations(depositDocObj, citations);
    }
  }

  @Override
  public void replaceTextValuesByIds(Object depositDocObj) {
    super.replaceTextValuesByIds(depositDocObj);

    /*
     * Language
     */
    String languageStr = this.getLanguage(depositDocObj);
    if (!StringTool.isNullOrEmpty(languageStr)) {
      LabeledLanguage language = this.labeledLanguageService.findByCode(languageStr);
      if (language != null) {
        this.setLanguage(depositDocObj, language.getResId());
      }
    }

    /*
     * Container's title language
     */
    String containerTitleLangStr = this.getContainerTitleLang(depositDocObj);
    if (!StringTool.isNullOrEmpty(containerTitleLangStr)) {
      LabeledLanguage language = this.labeledLanguageService.findByCode(containerTitleLangStr);
      if (language != null) {
        this.setContainerTitleLang(depositDocObj, language.getResId());
      }
    }

    /*
     * Citations' languages
     */
    List<MetadataTextLang> citations = this.getCitations(depositDocObj);
    if (citations != null) {
      for (MetadataTextLang citation : citations) {
        if (!StringTool.isNullOrEmpty(citation.getLang())) {
          LabeledLanguage language = this.labeledLanguageService.findByCode(citation.getLang());
          if (language != null) {
            citation.setLang(language.getResId());
          }
        }
      }
      this.setCitations(depositDocObj, citations);
    }
  }

  @Override
  public void addResearchGroup(ResearchGroup researchGroup, Publication publication) {
    DepositDoc depositDoc = this.createDepositDocFromJsonFormData(publication.getFormData());
    Group group = new Group();
    group.setName(researchGroup.getName());
    group.setCode(researchGroup.getCode() != null ? new BigInteger(researchGroup.getCode()) : null);

    Groups depositDocGroups = depositDoc.getGroups();
    if (!depositDocGroups.getGroup().contains(group)) {
      depositDocGroups.getGroup().add(group);
    }

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void addStructure(Structure structure, Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    // create a new AcademicStructure object
    AcademicStructure academicStructure = new AcademicStructure();
    academicStructure.setCode(structure.getCodeStruct());
    academicStructure.setName(structure.getName());

    if (depositDoc.getAcademicStructures() != null) {
      final boolean existingStructure = depositDoc.getAcademicStructures().getAcademicStructure().stream()
              .anyMatch(acst -> acst.getCode().equals(structure.getCodeStruct()));

      if (!existingStructure) {
        depositDoc.getAcademicStructures().getAcademicStructure().add(academicStructure);
      }
    } else {
      // create a AcademicStructures
      AcademicStructures academicStructures = new AcademicStructures();
      depositDoc.setAcademicStructures(academicStructures);
      depositDoc.getAcademicStructures().getAcademicStructure().add(academicStructure);
    }

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void removeStructure(Structure structure, Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    List<AcademicStructure> academicStructures = depositDoc.getAcademicStructures().getAcademicStructure();
    List<AcademicStructure> academicStructuresToRemove = new ArrayList<>();
    for (AcademicStructure academicStructure : academicStructures) {
      if (academicStructure.getCode().equals(structure.getCodeStruct())) {
        academicStructuresToRemove.add(academicStructure);
      }
    }

    for (AcademicStructure academicStructureToRemove : academicStructuresToRemove) {
      academicStructures.remove(academicStructureToRemove);
    }

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void addLanguage(LabeledLanguage labeledLanguage, Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
    depositDoc.setLanguage(labeledLanguage.getCode());
    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public boolean isCnIndividuInContributors(Publication publication, String cnIndividu) {
    if (StringTool.isNullOrEmpty(cnIndividu)) {
      return false;
    }

    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
    List<AbstractContributor> contributors = this.getContributors(depositDoc);
    return contributors.stream().anyMatch(abstractContributor -> abstractContributor instanceof ContributorDTO
            && cnIndividu.equals(((ContributorDTO) abstractContributor).getCnIndividu()));
  }

  @Override
  public void removeCnIndividuFromContributors(Publication publication, String cnIndividu) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    // loop on all contributors and replace cnIndividu by null if it matches
    List<AbstractContributor> contributors = this.getContributors(depositDoc);
    for (AbstractContributor abstractContributor : contributors) {
      if (abstractContributor instanceof ContributorDTO) {
        ContributorDTO contributorDTO = (ContributorDTO) abstractContributor;
        if (contributorDTO.getCnIndividu() != null && contributorDTO.getCnIndividu().equals(cnIndividu)) {
          contributorDTO.setCnIndividu(null);
        }
      }
    }
    this.setContributors(depositDoc, contributors);
    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void updatePublicationListWithContributorDisplayName(Publication publication, String cnIndividu, String lastname, String firstname,
          String[] publicationIds) {
    // does nothing in V1
  }

  @Override
  public void cleanMetadata(Publication publication) {
    // does nothing in V1
  }

  @Override
  public void cleanMetadataToClone(Publication publication) {
    // does nothing in V1
  }

  /*******************************************************************************************/

  public String getLanguage(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getLanguage() != null) {
      return depositDoc.getLanguage();
    } else {
      return null;
    }
  }

  void setLanguage(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    depositDoc.setLanguage(value);
  }

  public String getContainerISSN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null && depositDoc.getContainer().getIdentifiers() != null) {
      return depositDoc.getContainer().getIdentifiers().getIssn();
    } else {
      return null;
    }
  }

  public String getContainerISBN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null && depositDoc.getContainer().getIdentifiers() != null) {
      return depositDoc.getContainer().getIdentifiers().getIsbn();
    } else {
      return null;
    }
  }

  public List<String> getDOIs(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null && depositDoc.getIdentifiers().getDois() != null) {
      return depositDoc.getIdentifiers().getDois().getDoi();
    } else {
      return null;
    }
  }

  String getContainerTitleLang(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null && depositDoc.getContainer().getTitle() != null) {
      return depositDoc.getContainer().getTitle().getLang();
    }
    return null;
  }

  void setContainerTitleLang(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() == null) {
      depositDoc.setContainer(this.factory.createContainer());
    }
    if (depositDoc.getContainer().getTitle() == null) {
      depositDoc.getContainer().setTitle(this.factory.createText());
    }
    depositDoc.getContainer().getTitle().setLang(value);
  }

  List<MetadataTextLang> getCitations(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getCitations() != null && depositDoc.getCitations().getCitation() != null) {
      List<MetadataTextLang> textLangs = new ArrayList<>();
      for (Text abstractText : depositDoc.getCitations().getCitation()) {
        textLangs.add(new MetadataTextLang(abstractText.getContent(), abstractText.getLang()));
      }
      return textLangs;
    }
    return null;
  }

  void setCitations(Object depositDocObj, List<MetadataTextLang> citations) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getCitations() == null) {
      depositDoc.setCitations(this.factory.createCitations());
    }
    depositDoc.getCitations().getCitation().clear();
    for (MetadataTextLang textLang : citations) {
      Text text = this.factory.createText();
      text.setContent(textLang.getContent());
      text.setLang(textLang.getLang());
      depositDoc.getCitations().getCitation().add(text);
    }
  }

  /*******************************************************************************************/

  private ObjectMapper getJsonFormDeserializerMapper() {
    SimpleModule module = new SimpleModule();
    module.addDeserializer(Abstracts.class, new AbstractsDeserializer());
    module.addDeserializer(AcademicStructures.class, new AcademicsStructuresDeserializer());
    module.addDeserializer(Citations.class, new CitationsDeserializer());
    module.addDeserializer(Classifications.class, new ClassificationsDeserializer());
    module.addDeserializer(Collections.class, new CollectionsDeserializer());
    module.addDeserializer(Container.class, new ContainerDeserializer());
    module.addDeserializer(ContainerIdentifiers.class, new ContainerIdentifiersDeserializer());
    module.addDeserializer(Contributors.class, new ContributorsDeserializer());
    module.addDeserializer(Dataset.class, new DatasetDeserializer());
    module.addDeserializer(Dates.class, new DatesDeserializer());
    module.addDeserializer(DateWithType.class, new DateWithTypeDeserializer());
    module.addDeserializer(DepositIdentifiers.class, new DepositIdentifiersDeserializer());
    module.addDeserializer(Dois.class, new DoisDeserializer());
    module.addDeserializer(Files.class, new FilesDeserializer());
    module.addDeserializer(Fundings.class, new FundingsDeserializer());
    module.addDeserializer(Groups.class, new GroupsDeserializer());
    module.addDeserializer(Keywords.class, new KeywordsDeserializer());
    module.addDeserializer(Links.class, new LinksDeserializer());
    module.addDeserializer(Pages.class, new PagesDeserializer());
    module.addDeserializer(Publisher.class, new PublisherDeserializer());
    module.addDeserializer(Rights.class, new RightsDeserializer());
    module.addDeserializer(TextLang.class, new TextLangDeserializer());
    module.addDeserializer(Text.class, new TextDeserializer());

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(module);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper;
  }

  private ObjectMapper getJsonFormSerializerMapper() {
    SimpleModule module = new SimpleModule();
    module.addSerializer(Abstracts.class, new AbstractsSerializer());
    module.addSerializer(AcademicStructures.class, new AcademicStructuresSerializer());
    module.addSerializer(AuthorRole.class, new AuthorRoleSerializer());
    module.addSerializer(Citations.class, new CitationsSerializer());
    module.addSerializer(Classifications.class, new ClassificationsSerializer());
    module.addSerializer(Collections.class, new CollectionsSerializer());
    module.addSerializer(ContainerIdentifiers.class, new ContainerIdentifiersSerializer());
    module.addSerializer(Contributors.class, new ContributorsSerializer());
    module.addSerializer(Dataset.class, new DatasetSerializer());
    module.addSerializer(Dates.class, new DatesSerializer());
    module.addSerializer(DateWithType.class, new DateWithTypeSerializer());
    module.addSerializer(DepositDoc.class, new DepositDocSerializer());
    module.addSerializer(DepositIdentifiers.class, new DepositIdentifiersSerializer());
    module.addSerializer(Dois.class, new DoisSerializer());
    module.addSerializer(Files.class, new FilesSerializer());
    module.addSerializer(Fundings.class, new FundingsSerializer());
    module.addSerializer(Groups.class, new GroupsSerializer());
    module.addSerializer(Keywords.class, new KeywordsSerializer());
    module.addSerializer(Links.class, new LinksSerializer());
    module.addSerializer(Pages.class, new PagesSerializer());
    module.addSerializer(Publisher.class, new PublisherSerializer());
    module.addSerializer(Rights.class, new RightsSerializer());
    module.addSerializer(Text.class, new TextSerializer());
    module.addSerializer(TextLang.class, new TextLangSerializer());

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(module);
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    return mapper;
  }

  private DepositDoc getDepositDoc(Publication publication) {
    return (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
  }

  private File convertDocumentFileToXmlFile(DocumentFile documentFile) {
    File file = new File();
    file.setType(FileType.fromValue(documentFile.getDocumentFileType().getValue()));
    file.setAccessLevel(AccessLevel.fromValue(
            documentFile.getAccessLevel().toString().substring(0, 1).toUpperCase() + documentFile.getAccessLevel().toString().substring(1)
                    .toLowerCase()));
    file.setName(documentFile.getFileName());
    file.setSize(BigInteger.valueOf(documentFile.getFileSize()));
    file.setMimetype(documentFile.getMimetype());
    if (documentFile.getLicense() != null) {
      file.setLicense(documentFile.getLicense().getOpenLicenseId());
    }
    if (documentFile.getEmbargoEndDate() != null) {
      Embargo embargo = new Embargo();
      embargo.setAccessLevel(AccessLevel.fromValue(
              documentFile.getEmbargoAccessLevel().toString().substring(0, 1).toUpperCase() + documentFile.getEmbargoAccessLevel().toString()
                      .substring(1).toLowerCase()));
      embargo.setEndDate(documentFile.getEmbargoEndDate());
      file.setEmbargo(embargo);
    }
    return file;
  }
}
