/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DuplicateService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.ExistingPublicationInfo;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.rest.AouSearchCondition;

@Service
@ConditionalOnBean(AdminController.class)
public class DuplicateService {

  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;

  private final String indexPublicationsInProgressName;
  private final String homePage;
  private final FieldsRequest defaultFieldsRequestWithXml;

  public DuplicateService(IndexResourceService<String, PublicationIndexEntry> indexResourceService, AouProperties aouProperties) {
    this.indexResourceService = indexResourceService;
    this.indexPublicationsInProgressName = aouProperties.getIndexing().getInProgressPublicationsIndexName();
    this.homePage = aouProperties.getParameters().getHomepage();
    this.defaultFieldsRequestWithXml = aouProperties.getIndexing().getDefaultFieldsRequestWithXml();
  }

  /******/

  /**
   * Return true if the DOI exists in a publication that is already published or is used by another publication in database.
   *
   * @param doi
   * @param publicationId
   * @return
   */
  public boolean doiAlreadyUsedByAnotherPublication(String doi, String publicationId) {
    if (this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_DOI_CASE_INSENSITIVE, doi, publicationId) != null) {
      return true;
    } else {
      return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(
              AouConstants.INDEX_FIELD_CORRECTIONS_DOI_CASE_INSENSITIVE, doi, publicationId) != null;
    }
  }

  public ExistingPublicationInfo getExistingPublicationInfoByDoi(String doi) {
    ExistingPublicationInfo existingPublicationInfo = this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(
            AouConstants.INDEX_FIELD_DOI_CASE_INSENSITIVE, doi, null);
    if (existingPublicationInfo != null) {
      return existingPublicationInfo;
    } else {
      return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_CORRECTIONS_DOI_CASE_INSENSITIVE, doi, null);
    }
  }

  /******/

  /**
   * Return true if the ArxivId exists in a publication that is already published or is used by another publication in database.
   *
   * @param arxivId
   * @param publicationId
   * @return
   */
  public boolean arxivIdAlreadyUsedByAnotherPublication(String arxivId, String publicationId) {
    return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_ARXIV, arxivId, publicationId) != null;
  }

  public ExistingPublicationInfo getExistingPublicationInfoByArxivId(String arxivId) {
    return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_ARXIV, arxivId, null);
  }

  /******/

  /**
   * Return true if the PMID exists in a publication that is already published or is used by another publication in database.
   *
   * @param pmid
   * @param publicationId
   * @return
   */
  public boolean pmidAlreadyUsedByAnotherPublication(String pmid, String publicationId) {
    if (this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_PMID, pmid, publicationId) != null) {
      return true;
    } else {
      return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_CORRECTIONS_PMID, pmid, publicationId) != null;
    }
  }

  public ExistingPublicationInfo getExistingPublicationInfoByPmid(String pmid) {
    ExistingPublicationInfo existingPublicationInfo = this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_PMID, pmid,
            null);
    if (existingPublicationInfo != null) {
      return existingPublicationInfo;
    } else {
      return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_CORRECTIONS_PMID, pmid, null);
    }
  }

  /******/

  /**
   * Return true if the ISBN exists in a publication that is already published or is used by another publication in database.
   *
   * @param isbn
   * @param publicationId
   * @return
   */
  public boolean bookAlreadyUsedByAnotherPublication(String isbn, String publicationId) {
    return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_ISBN, isbn, publicationId) != null;
  }

  public ExistingPublicationInfo getExistingPublicationInfoByIsbn(String isbn) {
    return this.getUrlAndResIdOfAlreadyUsedInAnotherPublication(AouConstants.INDEX_FIELD_ISBN, isbn, null);
  }

  /*************************************************/

  private ExistingPublicationInfo getUrlAndResIdOfAlreadyUsedInAnotherPublication(String identifierType, String value,
          String exceptPublicationId) {
    // Check if the identifier exists in ES
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    List<AouSearchCondition> conditionList = new ArrayList<>();
    AouSearchCondition identifierCondition = new AouSearchCondition();
    identifierCondition.setType(SearchConditionType.TERM);
    identifierCondition.setField(identifierType);
    identifierCondition.setValue(value);
    conditionList.add(identifierCondition);

    if (identifierType.equals(AouConstants.INDEX_FIELD_ISBN)) {
      // Search on ISBN as MATCH to benefit from ElasticSearch index custom treatment
      identifierCondition.setType(SearchConditionType.MATCH);

      // Look only for existing publications that are books (reusing the same ISBN is allowed for Book chapters)
      AouSearchCondition bookCondition = new AouSearchCondition();
      bookCondition.setType(SearchConditionType.TERM);
      bookCondition.setField(AouConstants.INDEX_FIELD_SUBTYPE);
      bookCondition.setValue(AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME);
      conditionList.add(bookCondition);
    }

    // Do not find the given publicationId
    // Useful if publications with same identifiers have been imported without checking for duplicates, because in this case more than
    // one result may be returned.
    if (!StringTool.isNullOrEmpty(exceptPublicationId)) {
      AouSearchCondition notSamePublicationCondition = new AouSearchCondition();
      notSamePublicationCondition.setType(SearchConditionType.TERM);
      notSamePublicationCondition.setBooleanClauseType(BooleanClauseType.MUST_NOT);
      notSamePublicationCondition.setField(AouConstants.INDEX_FIELD_ID);
      notSamePublicationCondition.setValue(exceptPublicationId);
      conditionList.add(notSamePublicationCondition);
    }

    FacetPage<PublicationIndexEntry> results = this.indexResourceService.search(this.indexPublicationsInProgressName,
            conditionList.stream().map(SearchCondition.class::cast).collect(Collectors.toList()), null, pageable);
    if (results.getTotalElements() > 0) {
      if (StringTool.isNullOrEmpty(results.getContent().get(0).getArchiveId())) {
        return new ExistingPublicationInfo(results.getContent().get(0).getResId());
      }
      String url = this.homePage + "/" + results.getContent().get(0).getArchiveId();
      return new ExistingPublicationInfo(results.getContent().get(0).getResId(), url);

    }
    return null;
  }

  /**
   * Check if there is another publication created with the same title, year and first author
   *
   * @param publicationId
   * @return
   */
  public ExistingPublicationInfo checkPublicationDuplicates(String publicationId) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    //First search publication in the ES to used for the query
    PublicationIndexEntry publicationInIndex = this.indexResourceService.findOne(this.indexPublicationsInProgressName, publicationId,
            this.defaultFieldsRequestWithXml);

    if (publicationInIndex != null) {
      List<AouSearchCondition> conditionList = new ArrayList<>();
      AouSearchCondition titleCondition = new AouSearchCondition();
      titleCondition.setType(SearchConditionType.MATCH);
      titleCondition.setField(AouConstants.INDEX_FIELD_ALIAS_TITLE);
      titleCondition.setValue(publicationInIndex.getTitle());
      conditionList.add(titleCondition);

      if (publicationInIndex.getYear() != null) {
        AouSearchCondition yearCondition = new AouSearchCondition();
        yearCondition.setType(SearchConditionType.TERM);
        yearCondition.setField(AouConstants.INDEX_FIELD_YEAR);
        yearCondition.setValue(publicationInIndex.getYear().toString());
        conditionList.add(yearCondition);
      }

      String contributorFullname = publicationInIndex.getFirstContributorFullname();
      if (!StringTool.isNullOrEmpty(contributorFullname)) {
        AouSearchCondition authorCondition = new AouSearchCondition();
        authorCondition.setType(SearchConditionType.TERM);
        authorCondition.setField("contributors.fullName");
        authorCondition.setValue(contributorFullname);
        conditionList.add(authorCondition);
      }

      // Do not find the current publicationId
      AouSearchCondition notSamePublicationCondition = new AouSearchCondition();
      notSamePublicationCondition.setType(SearchConditionType.TERM);
      notSamePublicationCondition.setBooleanClauseType(BooleanClauseType.MUST_NOT);
      notSamePublicationCondition.setField(AouConstants.INDEX_FIELD_ID);
      notSamePublicationCondition.setValue(publicationId);
      conditionList.add(notSamePublicationCondition);

      // Do not find publications that are DELETED
      AouSearchCondition deletedPublicationCondition = new AouSearchCondition();
      deletedPublicationCondition.setType(SearchConditionType.TERM);
      deletedPublicationCondition.setBooleanClauseType(BooleanClauseType.MUST_NOT);
      deletedPublicationCondition.setField(AouConstants.INDEX_FIELD_STATUS);
      deletedPublicationCondition.getTerms().add(Publication.PublicationStatus.DELETED.toString());
      conditionList.add(deletedPublicationCondition);

      FacetPage<PublicationIndexEntry> results = this.indexResourceService.search(this.indexPublicationsInProgressName,
              conditionList.stream().map(SearchCondition.class::cast).collect(Collectors.toList()), null, pageable);
      if (results.getTotalElements() > 0) {
        PublicationIndexEntry firstPublication = results.getContent().get(0);
        if (StringTool.isNullOrEmpty(firstPublication.getArchiveId())) {
          return new ExistingPublicationInfo(firstPublication.getResId());
        }
        String url = this.homePage + "/" + firstPublication.getArchiveId();
        return new ExistingPublicationInfo(firstPublication.getResId(), url);
      }
    }
    return null;
  }

  public Map<Object, Object> getParamsFromExistingPublicationInfo(ExistingPublicationInfo existingPublicationInfo) {
    if (existingPublicationInfo != null) {
      final Map<Object, Object> params = new HashMap<>();
      if (!StringTool.isNullOrEmpty(existingPublicationInfo.getUrl())) {
        params.put("url", existingPublicationInfo.getUrl());
      } else {
        params.put("resId", existingPublicationInfo.getResId());
      }
      return params;
    }
    return null;
  }
}
