/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DoiCrossrefImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.AccessLevel;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.File;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.model.xml.deposit.v2_4.Funding;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class DoiCrossrefImportService extends MetadataAndFileImportService {

  private static final String CROSSREF_FILE_NAME = "_from_crossref.pdf";

  private static final Logger log = LoggerFactory.getLogger(DoiCrossrefImportService.class);

  private static final String XPATH_CROSSREF = "/doi_records/doi_record/crossref";
  private static final String XPATH_ERROR_NODE = DoiCrossrefImportService.XPATH_CROSSREF + "/error";

  private static final String XPATH_JOURNAL_NODE = DoiCrossrefImportService.XPATH_CROSSREF + "/journal";
  private static final String XPATH_CONFERENCE_NODE = DoiCrossrefImportService.XPATH_CROSSREF + "/conference";
  private static final String XPATH_CHAPTER_NODE = DoiCrossrefImportService.XPATH_CROSSREF + "/book/content_item[@component_type=\"chapter\"]";
  private static final String XPATH_BOOK_NODE = DoiCrossrefImportService.XPATH_CROSSREF + "/book";
  private static final String XPATH_DISSERTATION = DoiCrossrefImportService.XPATH_CROSSREF + "/dissertation";
  private static final String XPATH_PREPRINT_NODE = DoiCrossrefImportService.XPATH_CROSSREF + "/posted_content[@type=\"preprint\"]";
  private static final String XPATH_REPORT_NODE = DoiCrossrefImportService.XPATH_CROSSREF + "/report-paper";

  private static final String XPATH_LICENSE_NODE = "/program/license_ref";

  private static final String[] FILE_URLS_TO_IMPORT = {
          "/doi_data/collection[@property=\"crawler-based\"]/item[@crawler=\"iParadigms\"]/resource[@mime_type=\"application/pdf\"]",
          "/doi_data/collection[@property=\"crawler-based\"]/item[@crawler=\"iParadigms\"]/resource",
          "/doi_data/collection[@property=\"text-mining\"]/item/resource[@mime_type=\"application/pdf\"]"
  };

  private final String apiUrl;
  private final String apiPid;
  private final String[] textsToRemoveFromAbstracts;
  private final Map<String, String> textsToReplaceInAbstracts;
  private final String preprintNoteDepositedInText;

  public DoiCrossrefImportService(AouProperties aouProperties, MessageService messageService, MetadataService metadataService,
          ContributorService contributorService, DocumentFileService documentFileService, DocumentFileTypeService documentFileTypeService,
          JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService) {
    super(aouProperties, messageService, metadataService, contributorService, documentFileService, documentFileTypeService,
            journalTitleRemoteService, licenseService);
    this.apiUrl = aouProperties.getMetadata().getImports().getCrossref().getUrl();
    this.apiPid = aouProperties.getMetadata().getImports().getCrossref().getPid();
    this.textsToRemoveFromAbstracts = aouProperties.getMetadata().getImports().getCrossref().getTextsToRemoveFromAbstracts();
    this.textsToReplaceInAbstracts = aouProperties.getMetadata().getImports().getCrossref().getTextsToReplaceInAbstracts();
    this.preprintNoteDepositedInText = aouProperties.getMetadata().getImports().getCrossref().getPreprintNoteDepositedInText();
  }

  @SuppressWarnings("java:S2259")
  // fileUrl.substring(...) cannot throw an NPE because fileUrl is checked by the method isNullOrEmpty
  // this false positive should be remove when https://jira.sonarsource.com/browse/SONARJAVA-3502 will be solved
  public DepositDoc createDocumentFileFromCrossrefMetadata(Publication publication) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());

    DepositDoc depositDoc = (DepositDoc) metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    String doi = depositDoc.getIdentifiers().getDoi();
    Document crossrefDoc = this.getXmlFromCrossref(doi);

    //Get license if present
    String licenceValue = this.getLicenceValue(crossrefDoc);
    License license = null;
    if (!StringTool.isNullOrEmpty(licenceValue)) {
      license = this.licenseService.findMoreSpecificLicenseByUrl(licenceValue);
    }
    // Fill file metadata
    this.fillFile(depositDoc, crossrefDoc, license);

    // Create corresponding DocumentFile
    String fileUrl = this.getFileUrlByDocumentType(crossrefDoc);
    if (!StringTool.isNullOrEmpty(fileUrl)) {
      String fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
      File file = depositDoc.getFiles().getFile().stream().filter(f -> f.getName().equals(fileName)).findFirst().orElseThrow();

      DocumentFileType documentFileType = this.convertFileTypeDocToDocumentFileType(file.getType());
      DocumentFile.AccessLevel accessLevel = this.convertAccessLevel(file.getAccessLevel());
      String mimeType = file.getMimetype();

      DocumentFile documentFile = new DocumentFile();
      documentFile.setPublication(publication);
      documentFile.setAccessLevel(accessLevel);
      documentFile.setDocumentFileType(documentFileType);
      documentFile.setMimetype(mimeType);
      documentFile.setIsImported(true);
      documentFile.setLicense(license);
      documentFile.setFileName(CleanTool.cleanFileName(publication.getSubtype().getName() + CROSSREF_FILE_NAME));

      try {
        documentFile.setSourceData(new URI(fileUrl));
      } catch (URISyntaxException e) {
        throw new SolidifyRuntimeException("Unable to get file URL from Crossref", e);
      }
      this.saveDocumentFileIfSourceDataValid(documentFile);
    }

    return depositDoc;
  }

  /****************************************************************************************/

  private Document getXmlFromCrossref(String doi) {
    if (!StringTool.isNullOrEmpty(this.apiUrl)) {
      try {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

        Map<String, String> parameters = new HashMap<>();
        parameters.put("pid", this.apiPid);
        parameters.put("doi", doi);

        final String xmlString = client.getForObject(this.apiUrl, String.class, parameters);
        return XMLTool.parseXML(xmlString);
      } catch (HttpClientErrorException e) {
        throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
                this.messageService.get("deposit.error.identifiers.doi.not_found_by_crossref", new Object[] { doi }));
      } catch (IOException | ParserConfigurationException | SAXException e) {
        throw new SolidifyRuntimeException("An error occurred when searching DOI '" + doi + "' on Crossref", e);
      }
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  public DepositDoc searchDoi(String doi, DepositDoc depositDoc) {
    try {
      Document crossrefDoc = this.getXmlFromCrossref(doi);
      this.fillDepositDoc(doi, depositDoc, crossrefDoc);
      return depositDoc;
    } catch (final XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching DOI '" + doi + "' on Crossref", e);
    }
  }

  public void fillDepositDoc(String doi, DepositDoc depositDoc, Document crossrefDoc) throws XPathExpressionException {
    final XPath xpath = XMLTool.buildXPath();

    Node errorNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_ERROR_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
    if (errorNode != null) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
              this.messageService.get("deposit.error.identifiers.doi.not_found_by_crossref", new Object[] { doi }));
    }

    Node journalNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_JOURNAL_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
    Node conferenceNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_CONFERENCE_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
    Node chapterNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_CHAPTER_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
    Node bookNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_BOOK_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
    Node dissertationNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_DISSERTATION).evaluate(crossrefDoc, XPathConstants.NODE);
    Node preprintNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_PREPRINT_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
    Node reportNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_REPORT_NODE).evaluate(crossrefDoc, XPathConstants.NODE);

    if (journalNode != null) {
      this.fillJournalDepositDoc(depositDoc, crossrefDoc);
    } else if (conferenceNode != null) {
      this.fillConferenceDepositDoc(depositDoc, crossrefDoc, doi);
    } else if (chapterNode != null) {
      this.fillChapterDepositDoc(depositDoc, crossrefDoc);
    } else if (bookNode != null) {
      this.fillBookDepositDoc(depositDoc, crossrefDoc);
    } else if (dissertationNode != null) {
      this.fillDissertationDepositDoc(depositDoc, crossrefDoc);
    } else if (preprintNode != null) {
      this.fillPreprintDepositDoc(depositDoc, crossrefDoc);
    } else if (reportNode != null) {
      this.fillReportDepositDoc(depositDoc, crossrefDoc);
    }

    // if the DOI fetched from metadata is different from the one used to retrieve metadata, replace it
    this.ensureDepositIdentifiersExist(depositDoc);
    if (StringTool.isNullOrEmpty(depositDoc.getIdentifiers().getDoi()) || !depositDoc.getIdentifiers().getDoi().equals(doi)) {
      depositDoc.getIdentifiers().setDoi(doi);
    }
  }

  private void fillPreprintDepositDoc(DepositDoc depositDoc, Document crossrefDoc) {
    String xpathRoot = DoiCrossrefImportService.XPATH_PREPRINT_NODE;
    // @formatter:off
    depositDoc.setType(AouConstants.DEPOSIT_TYPE_RAPPORT_ID);
    depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME);
    this.fillTitle(depositDoc, crossrefDoc,                     xpathRoot + "/titles/title");
    this.fillContributorsAndCollaborations(depositDoc, crossrefDoc,              xpathRoot + "/contributors/*[@contributor_role=\"author\"]" , AuthorRole.AUTHOR);

    this.fillAbstracts(depositDoc, crossrefDoc,                 xpathRoot + "/abstract", null);

    this.fillCommercialUrl(depositDoc, crossrefDoc,             xpathRoot + "/doi_data/resource");
    this.fillDoi(depositDoc, crossrefDoc,                       xpathRoot + "/doi_data/doi");
    this.fillFirstOnlineDate(depositDoc, crossrefDoc,           xpathRoot  +"/posted_date/year",
                                                                xpathRoot + "/posted_date/month",
                                                                xpathRoot + "/posted_date/day");

    if (depositDoc.getDates() == null || depositDoc.getDates().getDate().isEmpty()) {
      this.fillFirstOnlineDate(depositDoc, crossrefDoc,         xpathRoot  +"/acceptance_date/year",
                                                                xpathRoot + "/acceptance_date/month",
                                                                xpathRoot + "/acceptance_date/day");
    }

    this.fillNote(depositDoc, crossrefDoc,                      xpathRoot + "/institution/institution_name");
    this.fillLanguages(depositDoc, crossrefDoc,                 List.of(xpathRoot + "/@language"));
    // @formatter:on

    // Complete note
    if (!StringTool.isNullOrEmpty(depositDoc.getNote()) && !StringTool.isNullOrEmpty(this.preprintNoteDepositedInText)) {
      String note = depositDoc.getNote();
      String fullNote = String.format(this.preprintNoteDepositedInText, note);
      depositDoc.setNote(fullNote);
    }
  }

  private void fillJournalDepositDoc(DepositDoc depositDoc, Document crossrefDoc) {
    String xpathRoot = DoiCrossrefImportService.XPATH_JOURNAL_NODE;

    // @formatter:off
    depositDoc.setType(AouConstants.DEPOSIT_TYPE_ARTICLE_ID);
    depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    this.fillTitle(depositDoc, crossrefDoc,                     xpathRoot + "/journal_article/titles/title");
    this.fillPublicationDate(depositDoc, crossrefDoc,           xpathRoot + "/journal_article/publication_date[@media_type=\"print\"]/year",
                                                                xpathRoot + "/journal_article/publication_date[@media_type=\"print\"]/month",
                                                                xpathRoot + "/journal_article/publication_date[@media_type=\"print\"]/day",
                                                                xpathRoot + "/journal_article/publication_date[not(@media_type=\"online\")]/year",
                                                                xpathRoot + "/journal_article/publication_date[not(@media_type=\"online\")]/month",
                                                                xpathRoot + "/journal_article/publication_date[not(@media_type=\"online\")]/day");
    this.fillDoi(depositDoc, crossrefDoc,                       xpathRoot + "/journal_article/doi_data/doi");
    this.fillContainerTitle(depositDoc, crossrefDoc,            xpathRoot + "/journal_metadata/full_title");
    this.fillContainerVolume(depositDoc, crossrefDoc,           xpathRoot + "/journal_issue/journal_volume/volume");

    this.fillIssn(depositDoc, crossrefDoc,                      xpathRoot + "/journal_metadata/issn");
    this.fillContributorsAndCollaborations(depositDoc, crossrefDoc,              xpathRoot + "/journal_article/contributors/*[@contributor_role=\"author\"]", AuthorRole.AUTHOR);
    this.fillPaging(depositDoc, crossrefDoc,                    xpathRoot + "/journal_article/pages/first_page",
                                                                xpathRoot + "/journal_article/pages/last_page");
    this.fillOtherPage(depositDoc, crossrefDoc,                 xpathRoot + "/journal_article/publisher_item/item_number[@item_number_type=\"article_number\"]");
    this.fillCommercialUrl(depositDoc, crossrefDoc,             xpathRoot + "/journal_article/doi_data/resource");
    

    this.fillFirstOnlineDate(depositDoc, crossrefDoc,           xpathRoot + "/journal_article/publication_date[@media_type=\"online\"]/year",
                                                                xpathRoot + "/journal_article/publication_date[@media_type=\"online\"]/month",
                                                                xpathRoot + "/journal_article/publication_date[@media_type=\"online\"]/day");

    this.fillContainerIssue(depositDoc, crossrefDoc,            xpathRoot + "/journal_issue/issue");
    this.fillContainerSpecialIssue(depositDoc, crossrefDoc,     xpathRoot + "/journal_issue/special_numbering");
    this.fillLanguages(depositDoc, crossrefDoc,                 List.of(xpathRoot + "/journal_article/@language", xpathRoot + "/journal_metadata/@language"));
    this.fillAbstracts(depositDoc, crossrefDoc,                 xpathRoot + "/journal_article/abstract", xpathRoot + "/journal_article/abstract/@lang");
    this.fillFundings(depositDoc, crossrefDoc,                  List.of(xpathRoot + "/journal_article/program[@name=\"fundref\"]",
                                                                xpathRoot + "/journal_article/crossmark/custom_metadata/program[@name=\"fundref\"]" ));
    // @formatter:on
  }

  private void fillConferenceDepositDoc(DepositDoc depositDoc, Document crossrefDoc, String doi) throws XPathExpressionException {
    String xpathRoot = DoiCrossrefImportService.XPATH_CONFERENCE_NODE;

    final XPath xpath = XMLTool.buildXPath();
    Node conferencePaperNode = (Node) xpath.compile(xpathRoot + "/conference_paper").evaluate(crossrefDoc, XPathConstants.NODE);

    // Depending on what metadata are present, it may be a "Conference proceedings", a "Conference presentation" (aka conference paper)
    // or a "Proceedings chapter"
    // More than one section may be present, but the DOI used to import the document may indicate which one it is
    String subtype = AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME;
    if (conferencePaperNode != null) {
      String value = XMLTool.getFirstTextContent(crossrefDoc, xpathRoot + "/conference_paper/doi_data/doi");
      if (doi.equals(value)) {
        subtype = AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME;
      }
    }

    // @formatter:off
    depositDoc.setType(AouConstants.DEPOSIT_TYPE_CONFERENCE_ID);
    depositDoc.setSubtype(subtype);
    this.fillTitle(depositDoc, crossrefDoc,                     xpathRoot + "/conference_paper/titles/title[1]");
    this.fillDoi(depositDoc, crossrefDoc,                       xpathRoot + "/conference_paper/doi_data/doi");
    this.fillContainerTitle(depositDoc, crossrefDoc,            xpathRoot + "/event_metadata/conference_name");

    this.fillContributorsAndCollaborations(depositDoc, crossrefDoc,              xpathRoot + "/conference_paper/contributors/*[@contributor_role=\"author\"]", AuthorRole.AUTHOR);
    this.fillPaging(depositDoc, crossrefDoc,                    xpathRoot + "/conference_paper/pages/first_page",
                                                                xpathRoot + "/conference_paper/pages/last_page");
    this.fillOtherPage(depositDoc, crossrefDoc,                 xpathRoot + "/conference_paper/publisher_item/item_number[@item_number_type=\"arNumber\"]");
    this.fillCommercialUrl(depositDoc, crossrefDoc,             xpathRoot + "/conference_paper/doi_data/resource");
    
    this.fillFirstOnlineDate(depositDoc, crossrefDoc,           xpathRoot + "/conference_paper/publication_date[@media_type=\"online\"]/year",
                                                                xpathRoot + "/conference_paper/publication_date[@media_type=\"online\"]/month",
                                                                xpathRoot + "/conference_paper/publication_date[@media_type=\"online\"]/day");

    this.fillContainerConferencePlace(depositDoc, crossrefDoc,  xpathRoot + "/event_metadata/conference_location");
    this.fillContainerConferenceDate(depositDoc, crossrefDoc,   xpathRoot + "/event_metadata/conference_date");

    this.fillIsbn(depositDoc, crossrefDoc,                      xpathRoot + "/proceedings_metadata/isbn");
    this.fillPublisherName(depositDoc, crossrefDoc,             xpathRoot + "/proceedings_metadata/publisher/publisher_name");
    this.fillPublicationDate(depositDoc, crossrefDoc,          xpathRoot + "/proceedings_metadata/publication_date[@media_type=\"print\"]/year",
                                                              xpathRoot + "/proceedings_metadata/publication_date[@media_type=\"print\"]/month",
                                                              xpathRoot + "/proceedings_metadata/publication_date[@media_type=\"print\"]/day",
                                                              xpathRoot + "/proceedings_metadata/publication_date[not(@media_type=\"online\")]/year",
                                                              xpathRoot + "/proceedings_metadata/publication_date[not(@media_type=\"online\")/month",
                                                              xpathRoot + "/proceedings_metadata/publication_date[not(@media_type=\"online\")/day");
    this.fillLanguages(depositDoc, crossrefDoc,                 List.of(xpathRoot + "/conference_paper/@language"));
    this.fillFundings(depositDoc, crossrefDoc,                  List.of(xpathRoot + "/journal_article/program[@name=\"fundref\"]",
                                                                xpathRoot + "/journal_article/crossmark/custom_metadata/program[@name=\"fundref\"]" ));
    // @formatter:on
  }

  private void fillChapterDepositDoc(DepositDoc depositDoc, Document crossrefDoc) {
    String xpathRoot = DoiCrossrefImportService.XPATH_CROSSREF + "/book";

    // @formatter:off
    depositDoc.setType(AouConstants.DEPOSIT_TYPE_LIVRE_ID);
    depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME);
    this.fillTitle(depositDoc, crossrefDoc,                     XPATH_CHAPTER_NODE + "/titles/title");
    this.fillDoi(depositDoc, crossrefDoc,                       xpathRoot + "/content_item/doi_data/doi");
    this.fillPublicationDate(depositDoc, crossrefDoc,           xpathRoot + "/content_item/publication_date[@media_type=\"print\"]/year",
                                                                xpathRoot + "/content_item/publication_date[@media_type=\"print\"]/month",
                                                                xpathRoot + "/content_item/publication_date[@media_type=\"print\"]/day",
                                                                xpathRoot + "/content_item/publication_date[not(@media_type=\"online\")]/year",
                                                                xpathRoot + "/content_item/publication_date[not(@media_type=\"online\")]/month",
                                                                xpathRoot + "/content_item/publication_date[not(@media_type=\"online\")]/day");
    this.fillContainerTitle(depositDoc, crossrefDoc,               xpathRoot + "/book_metadata/titles/title");

    this.fillContributorsAndCollaborations(depositDoc, crossrefDoc,              xpathRoot + "/content_item/contributors/*[@contributor_role=\"author\"]", AuthorRole.AUTHOR);
    this.fillPaging(depositDoc, crossrefDoc,                    xpathRoot + "/content_item/pages/first_page",
                                                                xpathRoot + "/content_item/pages/last_page");
    this.fillOtherPage(depositDoc, crossrefDoc,                 xpathRoot + "/content_item/component_number");

    this.fillCommercialUrl(depositDoc, crossrefDoc,             xpathRoot + "/content_item/doi_data/resource");

    this.fillFirstOnlineDate(depositDoc, crossrefDoc,           xpathRoot + "/content_item/publication_date[@media_type=\"online\"]/year",
                                                                xpathRoot + "/content_item/publication_date[@media_type=\"online\"]/month",
                                                                xpathRoot + "/content_item/publication_date[@media_type=\"online\"]/day");

    this.fillIsbn(depositDoc, crossrefDoc,                      xpathRoot + "/book_metadata/isbn[1]");
    this.fillPublisherName(depositDoc, crossrefDoc,             xpathRoot + "/book_metadata/publisher/publisher_name");
    this.fillPublisherPlace(depositDoc, crossrefDoc,            xpathRoot + "/book_metadata/publisher/publisher_place");
    this.fillLanguages(depositDoc, crossrefDoc,                 List.of(xpathRoot + "/book_metadata/@language"));
    // @formatter:on
  }

  private void fillBookDepositDoc(DepositDoc depositDoc, Document crossrefDoc) throws XPathExpressionException {
    final XPath xpath = XMLTool.buildXPath();

    String bookXpath = this.getBookXpath(crossrefDoc);
    Node bookMetadataNode = (Node) xpath.compile(bookXpath).evaluate(crossrefDoc, XPathConstants.NODE);

    if (bookMetadataNode != null) {
      depositDoc.setType(AouConstants.DEPOSIT_TYPE_LIVRE_ID);
      depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME);
      this.fillTitleAndSubtitle(depositDoc, crossrefDoc, bookXpath + "/titles/title", bookXpath + "/titles/subtitle");
      this.fillDoi(depositDoc, crossrefDoc, bookXpath + "/doi_data/doi");
      this.fillPublicationDate(depositDoc, crossrefDoc,
               bookXpath + "/publication_date[@media_type=\"print\"]/year",
               bookXpath + "/publication_date[@media_type=\"print\"]/month",
               bookXpath + "/publication_date[@media_type=\"print\"]/day",
               bookXpath + "/publication_date[not(@media_type=\"online\")]/year",
               bookXpath + "/publication_date[not(@media_type=\"online\")]/month",
               bookXpath + "/publication_date[not(@media_type=\"online\")]/day");

      this.fillContributorsAndCollaborations(depositDoc, crossrefDoc, bookXpath + "/contributors/*[@contributor_role=\"author\"]",
              AuthorRole.AUTHOR);
      this.fillContributorsAndCollaborations(depositDoc, crossrefDoc, bookXpath + "/contributors/person_name[@contributor_role=\"editor\"]",
              AuthorRole.EDITOR);
      if (depositDoc.getContributors() == null || depositDoc.getContributors().getContributorOrCollaboration().isEmpty()) {
        this.fillContributorsAndCollaborations(depositDoc, crossrefDoc,
                bookXpath + "/set_metadata/contributors/*[@contributor_role=\"author\"]", AuthorRole.AUTHOR);
        this.fillContributorsAndCollaborations(depositDoc, crossrefDoc,
                bookXpath + "/set_metadata/contributors/person_name[@contributor_role=\"editor\"]", AuthorRole.EDITOR);
      }

      this.fillIsbn(depositDoc, crossrefDoc, bookXpath + "/isbn");
      if (depositDoc.getIdentifiers() == null || StringTool.isNullOrEmpty(depositDoc.getIdentifiers().getIsbn())) {
        String xpathRoot = DoiCrossrefImportService.XPATH_BOOK_NODE;
        this.fillIsbn(depositDoc, crossrefDoc, xpathRoot + "/isbn[@media_type=\"print\"]");
      }

      this.fillEdition(depositDoc, crossrefDoc, bookXpath + "/edition_number");
      this.fillCommercialUrl(depositDoc, crossrefDoc, bookXpath + "/doi_data/resource");

      this.fillFirstOnlineDate(depositDoc, crossrefDoc,
              bookXpath + "/publication_date[@media_type=\"online\"]/year",
              bookXpath + "/publication_date[@media_type=\"online\"]/month",
              bookXpath + "/publication_date[@media_type=\"online\"]/day");

      this.fillPublisherName(depositDoc, crossrefDoc, bookXpath + "/publisher/publisher_name");
      this.fillPublisherPlace(depositDoc, crossrefDoc, bookXpath + "/publisher/publisher_place");
      this.fillLanguages(depositDoc, crossrefDoc, List.of(bookXpath + "/@language"));
    }
  }

  private void fillDissertationDepositDoc(DepositDoc depositDoc, Document crossrefDoc) {
    String xpathRoot = DoiCrossrefImportService.XPATH_DISSERTATION;

    // @formatter:off
    depositDoc.setType(AouConstants.DEPOSIT_TYPE_RAPPORT_ID);
    depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME);
    this.fillTitle(depositDoc, crossrefDoc,                     xpathRoot + "/titles/title");
    this.fillDoi(depositDoc, crossrefDoc,                       xpathRoot + "/doi_data/doi");
    this.fillPublicationDate(depositDoc, crossrefDoc,           xpathRoot + "/approval_date[@media_type=\"print\"]/year",
                                                                xpathRoot + "/approval_date[@media_type=\"print\"]/month",
                                                                xpathRoot + "/approval_date[@media_type=\"print\"]/day",
                                                                xpathRoot + "/approval_date[not(@media_type=\"online\")]/year",
                                                                xpathRoot + "/approval_date[not(@media_type=\"online\")]/month",
                                                                xpathRoot + "/approval_date[not(@media_type=\"online\")]/day");
    this.fillContributorsAndCollaborations(depositDoc, crossrefDoc,              xpathRoot + "/contributors/*[@contributor_role=\"author\"]", AuthorRole.AUTHOR);
    // @formatter:on
  }

  private void fillReportDepositDoc(DepositDoc depositDoc, Document crossrefDoc) {
    String reportXpath = this.getReportXpath(crossrefDoc);

    if (!StringTool.isNullOrEmpty(reportXpath)) {
      depositDoc.setType(AouConstants.DEPOSIT_TYPE_RAPPORT_ID);
      depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME);
      this.fillTitleAndSubtitle(depositDoc, crossrefDoc, reportXpath + "/titles/title", reportXpath + "/titles/subtitle");
      this.fillDoi(depositDoc, crossrefDoc, reportXpath + "/doi_data/doi");
      this.fillAbstracts(depositDoc, crossrefDoc, reportXpath + "/abstract", reportXpath + "/abstract/@lang");

      this.fillPublicationDate(depositDoc, crossrefDoc,
              reportXpath + "/publication_date[@media_type=\"print\"]/year",
              reportXpath + "/publication_date[@media_type=\"print\"]/month",
              reportXpath + "/publication_date[@media_type=\"print\"]/day",
              reportXpath + "/publication_date[not(@media_type=\"online\")]/year",
              reportXpath + "/publication_date[not(@media_type=\"online\")]/month",
              reportXpath + "/publication_date[not(@media_type=\"online\")]/day");
      this.fillFirstOnlineDate(depositDoc, crossrefDoc,
              reportXpath + "/publication_date[@media_type=\"online\"]/year",
              reportXpath + "/publication_date[@media_type=\"online\"]/month",
              reportXpath + "/publication_date[@media_type=\"online\"]/day");

      this.fillContributorsAndCollaborations(depositDoc, crossrefDoc, reportXpath + "/contributors/*[@contributor_role=\"author\"]",
              AuthorRole.AUTHOR);
      this.fillContributorsAndCollaborations(depositDoc, crossrefDoc, reportXpath + "/contributors/person_name[@contributor_role=\"editor\"]",
              AuthorRole.EDITOR);

      this.fillEdition(depositDoc, crossrefDoc, reportXpath + "/edition_number");
      this.fillCommercialUrl(depositDoc, crossrefDoc, reportXpath + "/doi_data/resource");

      this.fillPublisherName(depositDoc, crossrefDoc, reportXpath + "/publisher/publisher_name");
      this.fillPublisherPlace(depositDoc, crossrefDoc, reportXpath + "/publisher/publisher_place");
      this.fillLanguages(depositDoc, crossrefDoc, List.of(reportXpath + "/@language"));
    }
  }

  /****************************************************************************************/

  private void fillLanguages(DepositDoc depositDoc, Document crossrefDoc, List<String> xpathQueries) {
    for (String xpathQuery : xpathQueries) {
      String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
      if (!StringTool.isNullOrEmpty(value)) {
        this.ensureLanguagesExist(depositDoc);
        depositDoc.getLanguages().getLanguage().add(CleanTool.getLanguageCode(value));
        if (depositDoc.getTitle() != null) {
          depositDoc.getTitle().setLang(CleanTool.getLanguageCode(value));
        }
        break;
      }
    }
  }

  private void fillEdition(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      depositDoc.setEdition(value);
    }
  }

  private void fillContainerVolume(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value) && (depositDoc.getContainer() == null || depositDoc.getContainer().getVolume() == null)) {
      this.ensureContainerExist(depositDoc);
      value = CleanTool.extractNumbersFromString(value);
      depositDoc.getContainer().setVolume(value);
    }
  }

  private void fillContainerIssue(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureContainerExist(depositDoc);
      value = CleanTool.extractNumbersFromString(value);
      depositDoc.getContainer().setIssue(value);
    }
  }

  private void fillContainerSpecialIssue(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value) && !value.contentEquals("C")) {
      this.ensureContainerExist(depositDoc);
      value = CleanTool.extractNumbersFromString(value);
      depositDoc.getContainer().setSpecialIssue(value);
    }
  }

  private void fillContainerConferencePlace(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setConferencePlace(value);
    }
  }

  @Override
  protected void fillTitle(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value) && depositDoc.getTitle() == null) {
      depositDoc.setTitle(this.getText(value));
    }
  }

  protected void fillTitleAndSubtitle(DepositDoc depositDoc, Document doc, String titleXpathQuery, String subtitleXpathQuery) {
    String title = XMLTool.getFirstTextContent(doc, titleXpathQuery);
    String subtitle = XMLTool.getFirstTextContent(doc, subtitleXpathQuery);
    if (!StringTool.isNullOrEmpty(subtitle)) {
      title = title.concat(": " + subtitle);
    }
    if (!StringTool.isNullOrEmpty(title) && depositDoc.getTitle() == null) {
      depositDoc.setTitle(this.getText(title));
    }
  }

  protected void fillPublicationDate(DepositDoc depositDoc, Document doc, String xpathQueryYear, String xpathQueryMonth,
          String xpathQueryDay, String otherXpathQueryYear, String otherXpathQueryMonth, String otherXpathQueryDay) {
    if (depositDoc.getDates() == null || depositDoc.getDates().getDate() == null || depositDoc.getDates().getDate().isEmpty()
            || depositDoc.getDates().getDate().stream().noneMatch(d -> d.getType().equals(DateTypes.PUBLICATION))) {
      this.fillDate(depositDoc, doc, xpathQueryYear, xpathQueryMonth, xpathQueryDay, DateTypes.PUBLICATION);
      if (depositDoc.getDates() == null || depositDoc.getDates().getDate() == null
              || depositDoc.getDates().getDate().stream().noneMatch(dateWithType -> dateWithType.getType().equals(DateTypes.PUBLICATION))) {
        this.fillDate(depositDoc, doc, otherXpathQueryYear, otherXpathQueryMonth, otherXpathQueryDay, DateTypes.PUBLICATION);
      }
    }
  }

  @Override
  protected void fillDoi(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value) && (depositDoc.getIdentifiers() == null || depositDoc.getIdentifiers().getDoi() == null)) {
      this.ensureDepositIdentifiersExist(depositDoc);
      depositDoc.getIdentifiers().setDoi(value);
    }
  }

  @Override
  protected void fillContainerTitle(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value) && (depositDoc.getContainer() == null || depositDoc.getContainer().getTitle() == null)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setTitle(value);
    }
  }

  private void fillContainerConferenceDate(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {

    String datesRange;
    Node conferenceDateNode = XMLTool.getFirstNode(crossrefDoc, xpathQuery);
    if (conferenceDateNode.getAttributes().getLength() > 0) {
      String startYear = this.getNodeValue(conferenceDateNode, "start_year");
      String startMonth = this.getNodeValue(conferenceDateNode, "start_month");
      String startDay = this.getNodeValue(conferenceDateNode, "start_day");
      String endYear = this.getNodeValue(conferenceDateNode, "end_year");
      String endMonth = this.getNodeValue(conferenceDateNode, "end_month");
      String endDay = this.getNodeValue(conferenceDateNode, "end_day");

      datesRange = this.getDatesRange(startYear, startMonth, startDay, endYear, endMonth, endDay);

    } else { // date is not with a specific format
      datesRange = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    }
    if (!StringTool.isNullOrEmpty(datesRange)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setConferenceDate(datesRange);
    }
  }

  private void fillContributorsAndCollaborations(DepositDoc depositDoc, Document crossrefDoc, String contributorsXPathQuery, AuthorRole role) {
    //check before if contributors was already filled
    if (depositDoc.getContributors() == null || depositDoc.getContributors().getContributorOrCollaboration().isEmpty()) {
      final XPath xpath = XMLTool.buildXPath();
      try {
        NodeList personOrCollaborationList = (NodeList) xpath.compile(contributorsXPathQuery).evaluate(crossrefDoc, XPathConstants.NODESET);

        if (personOrCollaborationList.getLength() > 0) {
          this.ensureContributorsExist(depositDoc);
        }
        for (int i = 0; i < this.getNumberMaxContributor(personOrCollaborationList.getLength()); i++) {
          //check if it is a collaboration or a contributor
          boolean isContributor = XMLTool.getFirstTextContent(personOrCollaborationList.item(i), "./surname") != null ||
                  XMLTool.getFirstTextContent(personOrCollaborationList.item(i), "./given_name") != null;
          if (isContributor) {
            Contributor contributor = this.createContributorFromNode(personOrCollaborationList.item(i), role);
            depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
          } else {
            Collaboration collaboration = new Collaboration();
            collaboration.setName(personOrCollaborationList.item(i).getTextContent());
            // check if it was already added
            if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                    .noneMatch(c -> {
                      try {
                        if (Objects.equals(((Collaboration) c).getName(), collaboration.getName())) {
                          return true;
                        }
                      } catch (ClassCastException e) {
                        return false;
                      }
                      return false;
                    })) {
              depositDoc.getContributors().getContributorOrCollaboration().add(collaboration);
            }
          }
        }

        // add authors that have an affiliation with unige, geneve, or hug
        NodeList authorsListWithAffiliation = (NodeList) xpath
                .compile(contributorsXPathQuery
                        + "[contains(./affiliation, 'Genev') or contains(./affiliation, 'Unige') or contains(./affiliation, 'HUG')]")
                .evaluate(crossrefDoc, XPathConstants.NODESET);
        for (int i = 0; i < authorsListWithAffiliation.getLength(); i++) {
          Contributor contributor = this.createContributorFromNode(authorsListWithAffiliation.item(i), role);
          // check if it was already added
          if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                  .noneMatch(c -> {
                    try {
                      if (Objects.equals(((Contributor) c).getLastname(), contributor.getLastname())
                              && Objects.equals(((Contributor) c).getFirstname(), contributor.getFirstname())) {
                        return true;
                      }
                    } catch (ClassCastException e) {
                      return false;
                    }
                    return false;
                  })) {
            depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
          }
        }
      } catch (XPathExpressionException e) {
        throw new SolidifyRuntimeException("Unable to fill contributors", e);
      }
    }
  }

  private void fillPaging(DepositDoc depositDoc, Document crossrefDoc, String firstPageXpathQuery, String lastPageXpathQuery) {
    String firstPageValue = XMLTool.getFirstTextContent(crossrefDoc, firstPageXpathQuery);
    String lastPageValue = XMLTool.getFirstTextContent(crossrefDoc, lastPageXpathQuery);

    String paging = "";
    if (!StringTool.isNullOrEmpty(firstPageValue)) {
      paging = firstPageValue;
    }
    if (!StringTool.isNullOrEmpty(lastPageValue)) {
      if (!StringTool.isNullOrEmpty(paging)) {
        paging += "-";
      }
      paging += lastPageValue;
    }

    if (!StringTool.isNullOrEmpty(paging)) {
      this.ensurePagesExist(depositDoc);
      depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesPaging(paging));
    }
  }

  private void fillOtherPage(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensurePagesExist(depositDoc);
      depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesOther(value));
    }
  }

  private void fillFundings(DepositDoc depositDoc, Document crossrefDoc, List<String> xpathQueries) {
    for (String xpathQuery : xpathQueries) {

      final XPath xpath = XMLTool.buildXPath();
      try {
        NodeList fundrefList = (NodeList) xpath.compile(xpathQuery).evaluate(crossrefDoc, XPathConstants.NODESET);
        for (int i = 0; i < fundrefList.getLength(); i++) {
          Node fundrefNode = fundrefList.item(i);

          NodeList fundGroupNodeList = (NodeList) xpath.compile(".//assertion[@name=\"fundgroup\"]")
                  .evaluate(fundrefNode, XPathConstants.NODESET);

          for (int j = 0; j < fundGroupNodeList.getLength(); j++) {
            Node fundGroupNode = fundGroupNodeList.item(j);

            String funderName = XMLTool.getFirstTextContent(fundGroupNode, "./assertion[@name=\"funder_name\"]/text()[normalize-space()]");
            if (funderName != null) {
              funderName = funderName.trim();
            }
            String awardNumber = XMLTool.getFirstTextContent(fundGroupNode, "./assertion[@name=\"award_number\"]");
            Funding funding = new Funding();
            funding.setFunder(funderName);
            funding.setCode(awardNumber);

            this.ensureFundingsExist(depositDoc);
            depositDoc.getFundings().getFunding().add(funding);
          }
        }
      } catch (XPathExpressionException e) {
        throw new SolidifyRuntimeException("Unable to fill fundings", e);
      } catch (Exception e) {
        throw new SolidifyRuntimeException("Unable to fill fundings into metadata", e);
      }
    }
  }

  @Override
  protected void fillAbstracts(DepositDoc depositDoc, Document doc, String xpathQuery, String xpathLangQuery) {
    super.fillAbstracts(depositDoc, doc, xpathQuery, xpathLangQuery);

    if (depositDoc.getAbstracts() != null && !depositDoc.getAbstracts().getAbstract().isEmpty()) {
      String abstractContent = depositDoc.getAbstracts().getAbstract().get(0).getContent();

      // remove eventual configured substrings
      for (String textToRemove : this.textsToRemoveFromAbstracts) {
        abstractContent = abstractContent.replace(textToRemove, "");
      }

      // replace eventual configured substrings
      for (Map.Entry<String, String> entry : this.textsToReplaceInAbstracts.entrySet()) {
        abstractContent = abstractContent.replace(entry.getKey(), entry.getValue());
      }

      depositDoc.getAbstracts().getAbstract().get(0).setContent(abstractContent.trim());
    }
  }

  @SuppressWarnings("java:S2259")
  // fileUrl.lastIndexOf('/') cannot throw an NPE because fileUrl is checked by the method isNullOrEmpty
  // this false positive should be removed when https://jira.sonarsource.com/browse/SONARJAVA-3502 will be solved
  private void fillFile(DepositDoc depositDoc, Document crossrefDoc, License license) {
    String fileUrl = this.getFileUrlByDocumentType(crossrefDoc);
    if (!StringTool.isNullOrEmpty(fileUrl)) {
      this.ensureFileExists(depositDoc);
      File file = new File();
      // If there is journal_ref tag, the file's type is ARTICLE_PUBLISHED_VERSION,
      // if not, REPORT->PREPRINT.
      if (this.isJournalRefPresent(crossrefDoc)) {
        file.setType(FileType.ARTICLE_PUBLISHED_VERSION);
      } else if (depositDoc.getType().equals(AouConstants.DEPOSIT_TYPE_LIVRE_NAME)) {
        file.setType(FileType.BOOK_PUBLISHED_VERSION);
      } else if (depositDoc.getType().equals(AouConstants.DEPOSIT_TYPE_CONFERENCE_NAME)) {
        if (depositDoc.getSubtype().equals(AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME)) {
          file.setType(FileType.PROCEEDINGS_CHAPTER_PUBLISHED_VERSION);
        } else {
          file.setType(FileType.PROCEEDINGS_ACCEPTED_VERSION);
        }
      } else if (depositDoc.getType().equals(AouConstants.DEPOSIT_TYPE_RAPPORT_NAME)) {
        file.setType(FileType.REPORT);
      } else {
        file.setType(FileType.PREPRINT);
      }
      file.setAccessLevel(AccessLevel.PUBLIC);
      file.setMimetype("application/pdf");
      file.setName(fileUrl.substring(fileUrl.lastIndexOf('/') + 1));
      if (license != null) {
        file.setLicense(license.getOpenLicenseId());
      }

      depositDoc.getFiles().getFile().add(file);
    }
  }

  private String getLicenceValue(Document crossrefDoc) {
    String prefix = this.getXpathPrefix(crossrefDoc);
    if (prefix == null) {
      return null;
    }
    String fileXPathQuery = DoiCrossrefImportService.XPATH_CROSSREF + prefix
            + DoiCrossrefImportService.XPATH_LICENSE_NODE;
    return XMLTool.getFirstTextContent(crossrefDoc, fileXPathQuery);
  }

  /****************************************************************************************/

  private String getFileUrlByDocumentType(Document crossrefDoc) {
    String prefix = this.getXpathPrefix(crossrefDoc);
    if (prefix == null) {
      return null;
    }

    return Arrays.stream(FILE_URLS_TO_IMPORT).map(url -> {
      String fileXPathQuery = DoiCrossrefImportService.XPATH_CROSSREF + prefix + url;
      String fileUrl = XMLTool.getFirstTextContent(crossrefDoc, fileXPathQuery);
      if (!StringTool.isNullOrEmpty(fileUrl)) {
        return fileUrl;
      }
      return null;
    }).filter(f -> !StringTool.isNullOrEmpty(f)).findFirst().orElse(null);

  }

  private String getXpathPrefix(Document crossrefDoc) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      Node journalNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_JOURNAL_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
      Node conferenceNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_CONFERENCE_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
      Node chapterNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_CHAPTER_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
      Node bookNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_BOOK_NODE).evaluate(crossrefDoc, XPathConstants.NODE);
      Node dissertationNode = (Node) xpath.compile(DoiCrossrefImportService.XPATH_DISSERTATION).evaluate(crossrefDoc, XPathConstants.NODE);

      if (journalNode != null) {
        return "/journal/journal_article";
      } else if (conferenceNode != null) {
        return "/conference/conference_paper";
      } else if (chapterNode != null) {
        return "/book[@book_type=\"other\"]/content_item";
      } else if (bookNode != null) {
        String bookXpath = this.getBookXpath(crossrefDoc);
        if (!StringTool.isNullOrEmpty(bookXpath)) {
          return bookXpath.replace(DoiCrossrefImportService.XPATH_CROSSREF, "");
        }
      } else if (dissertationNode != null) {
        return "/dissertation";
      }
    } catch (XPathExpressionException e) {
      log.error("Error in getting xpath prefix", e);
    }
    return null;
  }

  private boolean isJournalRefPresent(Document crossrefDoc) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, DoiCrossrefImportService.XPATH_JOURNAL_NODE);
    return !StringTool.isNullOrEmpty(value);
  }

  private Contributor createContributorFromNode(Node authors, AuthorRole role) throws XPathExpressionException {
    String surname = XMLTool.getFirstTextContent(authors, "./surname");
    String givenName = XMLTool.getFirstTextContent(authors, "./given_name");
    String affiliation = XMLTool.getFirstTextContent(authors, "./affiliation");

    // If affiliation name doesn't exist, look for eventual ROR ID
    if (StringTool.isNullOrEmpty(affiliation)) {
      // Try to find institution
      XPath xpath = XMLTool.buildXPath();
      NodeList rorIdNodes = (NodeList) xpath.compile("./affiliations/institution/institution_id[@type='ror']")
              .evaluate(authors, XPathConstants.NODESET);
      for (int i = 0; i < rorIdNodes.getLength(); i++) {
        Node rorIdNode = rorIdNodes.item(i);
        if (AouConstants.UNIVERSITY_OF_GENEVA_ROR_ID.equals(rorIdNode.getTextContent())) {
          affiliation = AouConstants.UNIVERSITY_OF_GENEVA_FR;
        } else if (AouConstants.HUG_ROR_ID.equals(rorIdNode.getTextContent())) {
          affiliation = AouConstants.HUG_FR;
        }
      }
    }

    surname = this.getNameOrUndefinedValue(surname);
    givenName = this.getNameOrUndefinedValue(givenName);

    String orcid = XMLTool.getFirstTextContent(authors, "./ORCID");

    Contributor contributor = new Contributor();
    contributor.setLastname(CleanTool.capitalizeString(surname));
    contributor.setFirstname(CleanTool.capitalizeString(givenName));
    contributor.setInstitution(affiliation);
    contributor.setRole(role);
    orcid = this.cleanOrcid(orcid);
    if (!StringTool.isNullOrEmpty(orcid)) {
      contributor.setOrcid(orcid);
    }
    return contributor;
  }

  /**
   * There are three possible top levels nodes in case of books: book_metadata, book_set_metadata, book_series_metadata.
   *
   * @param crossrefDoc
   * @return
   */
  private String getBookXpath(Document crossrefDoc) {
    return this.getValidXpath(crossrefDoc, List.of(
            DoiCrossrefImportService.XPATH_BOOK_NODE + "/book_metadata",
            DoiCrossrefImportService.XPATH_BOOK_NODE + "/book_set_metadata",
            DoiCrossrefImportService.XPATH_BOOK_NODE + "/book_series_metadata"));
  }

  private String getReportXpath(Document crossrefDoc) {
    return this.getValidXpath(crossrefDoc, List.of(
            DoiCrossrefImportService.XPATH_REPORT_NODE + "/report-paper_metadata",
            DoiCrossrefImportService.XPATH_REPORT_NODE + "/report-paper_series_metadata"));
  }

  private String getValidXpath(Document crossrefDoc, List<String> xpathsToCheck) {
    try {
      final XPath xpath = XMLTool.buildXPath();
      for (String xpathToCheck : xpathsToCheck) {
        Node bookMetadataNode = (Node) xpath.compile(xpathToCheck).evaluate(crossrefDoc, XPathConstants.NODE);
        if (bookMetadataNode != null) {
          return xpathToCheck;
        }
      }
      return null;
    } catch (XPathExpressionException e) {
      return null;
    }
  }

  private String getNodeValue(Node node, String property) {
    if (node.getAttributes().getNamedItem(property) != null) {
      return node.getAttributes().getNamedItem(property).getNodeValue();
    }
    return null;
  }

}
