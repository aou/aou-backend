/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.security;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.controller.AouControllerAction;
import ch.unige.aou.model.publication.Publication;

@Service("publicationPermissionService")
@ConditionalOnBean(AdminController.class)
public class PublicationPermissionService extends AbstractAdminPermissionService {
  private static final Logger logger = LoggerFactory.getLogger(PublicationPermissionService.class);

  private final PublicationService publicationService;
  private final UserService userService;

  public PublicationPermissionService(PersonService personService, PublicationService publicationService,
          UserService userService) {
    super(personService);
    this.publicationService = publicationService;
    this.userService = userService;
  }

  /******************************************************************************************************/

  public boolean isAllowed(String targetResId, String documentId, String actionString) {
    try {
      if (this.isRootOrTrustedOrAdminRole()) {
        return true;
      }
      Publication existingResource = this.getExistingResource(targetResId);
      String personId = this.getPersonAuthenticatedId();
      AouControllerAction action = this.getControllerAction(actionString);
      if (action.equals(AouControllerAction.DOWNLOAD_FILE)) {
        if (this.publicationService.isAllowedToDownload(personId, documentId, targetResId)) {
          return this.isAllowedToPerformActionOnResource(personId, existingResource, action);
        }
      }
    } catch (SolidifyRestException e) {
      // Needed to catch for request made with downloadToken in cookie since request are made with anonymous user.
      return false;
    }
    return false;
  }

  public boolean isAllowedToCreate(Publication resource) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonAuthenticatedId();
    return this.isAllowedToPerformActionOnResource(personId, resource, AouControllerAction.CREATE);
  }

  public boolean isAllowedToUpdate(String targetResId, Map<String, Object> updateMap) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    String personId = this.getPersonAuthenticatedId();
    Publication existingResource = this.getExistingResource(targetResId);
    return this.isAllowedToPerformActionOnResource(personId, existingResource, AouControllerAction.UPDATE);
  }

  public boolean isAllowed(String targetResId, String actionString) {
    try {
      if (this.isRootOrTrustedOrAdminRole()) {
        return true;
      }
      String personId = this.getPersonAuthenticatedId();
      Publication existingResource = this.getExistingResource(targetResId);
      if (existingResource == null) {
        throw new NoSuchElementException(targetResId);
      }
      AouControllerAction action = this.getControllerAction(actionString);
      return this.isAllowedToPerformActionOnResource(personId, existingResource, action);
    } catch (SolidifyRestException e) {
      // Needed to catch for request made with downloadToken in cookie since request are made with anonymous user.
      return false;
    }
  }

  public boolean isAllowed(List<String> targetResIds, String actionString) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
   return this.isAllowed(targetResIds.toArray(new String[targetResIds.size()]), actionString);
  }

  public boolean isAllowed(String[] targetResIds, String actionString) {
    for (String id : targetResIds) {
      if (!this.isAllowed(id, actionString)) {
        return false;
      }
    }

    return true;
  }

  public boolean isAllowedToCreate() {
    return this.isRootOrTrustedOrAdminRole() || (this.isUserRole() && this.isUserOfAuthorizedInstitutions());
  }

  /******************************************************************************************************/

  private Publication getExistingResource(String resId) {
    return this.publicationService.findOne(resId);
  }

  private boolean isUserOfAuthorizedInstitutions() {
    return this.userService.isUserMemberOfAuthorizedInstitutions();
  }

  /******************************************************************************************************/

  private boolean isAllowedToPerformActionOnResource(String personId, Publication resourceToCheck, AouControllerAction action) {

    if (StringTool.isNullOrEmpty(personId)) {
      logger.error("unable to check permissions: personId is not set");
      return false;
    }

    final String publicationId = resourceToCheck.getResId();

    // @formatter:off
    return (this.isUserRole()   && this.isUserOfAuthorizedInstitutions()  && this.isActionPermittedForUser(action)) ||
           (this.isResourceCreator(personId, publicationId)               && this.isActionPermittedForCreator(action)) ||
           (this.authenticatedUserBelongsToContributors(publicationId)    && this.isActionPermittedForContributor(action)) ||
           (this.isAllowedToValidate(personId, publicationId)             && this.isActionPermittedForValidator(action, resourceToCheck));
    // @formatter:on
  }

  private boolean isActionPermittedForUser(AouControllerAction action) {
    // @formatter:off
    switch (action) {
      case CREATE:
      case GET:
      case CLONE:

      // manage comments
      case GET_DEPOSIT_COMMENT:
      case ADD_DEPOSIT_COMMENT:
      case LIST_DEPOSIT_COMMENTS:
      case UPDATE_DEPOSIT_COMMENT:
      case DELETE_DEPOSIT_COMMENT:

      // manage files
      case LIST_FILES:
      case DOWNLOAD_FILE:

      // validation
      case VALIDATE_METADATA:
      case CHECK_DUPLICATES:
        return true;
      default:
        return false;
    }
    // @formatter:on
  }

  private boolean isActionPermittedForCreator(AouControllerAction action) {
    return action == AouControllerAction.SUBMIT_FOR_VALIDATION
            || action == AouControllerAction.ENABLE_REVISION
            || this.isActionPermittedForCreatorAndContributorAndValidator(action);
  }

  private boolean isActionPermittedForContributor(AouControllerAction action) {
    return action == AouControllerAction.SUBMIT_FOR_VALIDATION
            || action == AouControllerAction.EXPORT_TO_ORCID
            || this.isActionPermittedForCreatorAndContributorAndValidator(action);
  }

  private boolean isActionPermittedForValidator(AouControllerAction action, Publication resourceToCheck) {

    if (action == AouControllerAction.SUBMIT_FOR_VALIDATION && resourceToCheck.getStatus() == Publication.PublicationStatus.IN_EDITION) {
      return true;
    }

    switch (action) {
      case SUBMIT:
      case REJECT:
      case ASK_FEEDBACK:
      case LIST_VALIDATOR_COMMENTS:
      case RESUME:
      case SEND_BACK_FOR_VALIDATION:
      case ADD_DEPOSIT_VALIDATION_STRUCTURE:
      case DELETE_DEPOSIT_VALIDATION_STRUCTURE:
      case EXPORT_DEPOSITS:
        return true;
      default:
        return this.isActionPermittedForCreatorAndContributorAndValidator(action);
    }
  }

  /**
   * Check if the action can be done by a person who is creator, contributor or validator of a deposit
   *
   * @param action
   * @return
   */
  private boolean isActionPermittedForCreatorAndContributorAndValidator(AouControllerAction action) {
    // @formatter:off
    switch (action) {
      // CRUD
      case GET:
      case HISTORY:
      case UPDATE:
      case DELETE:

      // manage files
      case GET_FILE:
      case ADD_DOCUMENT_FILE:
      case UPDATE_DOCUMENT_FILE:
      case DELETE_DOCUMENT_FILE:
      case UPLOAD_DOCUMENT_FILE:

      // update publication
      case START_METADATA_EDITING:
      case CANCEL_METADATA_EDITING:
      case GET_PENDING_METADATA_UPDATES:
        return true;
      default:
        return false;
    }
    // @formatter:on
  }

  /******************************************************************************************************/

  private boolean isAllowedToValidate(String personId, String publicationId) {
    Publication publication = this.publicationService.findOne(publicationId);

    return this.personService.isAllowedToValidate(personId, publication);
  }

  private boolean isResourceCreator(String personId, String publicationId) {
    return this.publicationService.isPersonPublicationCreator(personId, publicationId);
  }

  private boolean authenticatedUserBelongsToContributors(String publicationId) {
    return this.publicationService.authenticatedUserBelongsToContributors(publicationId);
  }
}
