/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PdfDocumentCreator.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.files;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.layout.font.FontProvider;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;

/**
 * PdfDocumentCreator will generate PDF files
 */
@Service
@ConditionalOnBean(AdminController.class)
public class PdfDocumentCreator {
  private static final Logger log = LoggerFactory.getLogger(PdfDocumentCreator.class);

  private final SpringTemplateEngine templateEngine;
  private final DocumentFileTypeService documentFileTypeService;
  private final MetadataService metadataService;

  private final String tmpPublicationsFolder;
  private static final String AGREEMENT_DOC = "agreement.pdf";
  private static final String AGREEMENT_HTML_FILE = "contrat_diffusion_these";
  private static final String AGREEMENT_PATH = "documents/";

  private final String disDirector;

  public PdfDocumentCreator(SpringTemplateEngine templateEngine, DocumentFileTypeService documentFileTypeService,
          MetadataService metadataService, AouProperties aouProperties) {
    this.templateEngine = templateEngine;
    this.documentFileTypeService = documentFileTypeService;
    this.metadataService = metadataService;
    this.templateEngine.addTemplateResolver(this.getTemplateResolver());
    this.tmpPublicationsFolder = aouProperties.getTempLocation(aouProperties.getPublicationsFolder());
    this.disDirector = aouProperties.getParameters().getDisDirector();
  }

  public DocumentFile generateAgreementPdf(Publication publication, DocumentFile documentFile) {
    final Context thymeleafContext = new Context();
    final Map<String, Object> parameters = new HashMap<>();
    this.populateParametersForDocument(publication, documentFile, parameters);
    thymeleafContext.setVariables(parameters);

    String htmlBody = this.templateEngine.process(AGREEMENT_HTML_FILE + SolidifyConstants.HTML_EXT, thymeleafContext);

    DocumentFile agreementDoc = new DocumentFile();
    agreementDoc.setPublication(publication); // other class attributes are set in DocumentFileService beforeValidate method

    DocumentFileType documentFileType = this.documentFileTypeService.findByValue(AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE);
    agreementDoc.setDocumentFileType(documentFileType);

    try {
      ConverterProperties properties = new ConverterProperties();
      FontProvider fontProvider = new FontProvider();
      fontProvider.addStandardPdfFonts();
      fontProvider.addSystemFonts(); //for fallback
      properties.setFontProvider(fontProvider);

      Path tmpFolder = Paths.get(this.tmpPublicationsFolder).resolve(publication.getResId());
      Path tmpFile = tmpFolder.resolve(Objects.requireNonNull(AGREEMENT_DOC));
      agreementDoc.setSourceData(tmpFile.toUri());

      HtmlConverter.convertToPdf(htmlBody, Files.newOutputStream(tmpFile), properties);

    } catch (FileNotFoundException e) {
      log.error("Error when converting HTML file to PDF Agreement document for publication : {}", publication.getResId());
      throw new SolidifyRuntimeException(
              "Error when converting HTML file to PDF Agreement document for publication : " + publication.getResId());
    } catch (IOException e) {
      log.error("Could not create agreement PDF file for publication: {}", publication.getResId());
      throw new SolidifyRuntimeException("Could not create agreement PDF file for publication: " + publication.getResId());
    }

    return agreementDoc;
  }

  private void populateParametersForDocument(Publication publication, DocumentFile documentFile, Map<String, Object> parameters) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    final Optional<ContributorDTO> authorOpt = metadataExtractor.getFirstAuthor(depositDoc);

    if (authorOpt.isPresent()) {
      parameters.put("disDirector", this.disDirector);
      parameters.put("doctorFirstName", authorOpt.get().getFirstName());
      parameters.put("doctorLastName", authorOpt.get().getLastName());
      parameters.put("doctorEmail", metadataExtractor.getDoctorEmail(depositDoc));
      parameters.put("doctorAddress", metadataExtractor.getDoctorAddress(depositDoc));
      parameters.put("thesisTitle", metadataExtractor.getTitleContent(depositDoc));
      parameters.put("thesisDocument", documentFile);
      parameters.put("thesisDocumentLicense", this.getThesisDocumentLicense(documentFile));
      parameters.put("currentDate", new Date());
    } else {
      throw new SolidifyRuntimeException("First author cannot be found for publication " + publication.getResId());
    }
  }

  private ITemplateResolver getTemplateResolver() {
    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
    templateResolver.setPrefix(PdfDocumentCreator.AGREEMENT_PATH);
    templateResolver.setSuffix(SolidifyConstants.HTML_EXT);
    templateResolver.setTemplateMode(TemplateMode.HTML);
    templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
    templateResolver.setOrder(1);
    templateResolver.setCheckExistence(true);
    templateResolver.setCacheable(false);
    return templateResolver;
  }

  private String getThesisDocumentLicense(DocumentFile documentFile) {
    if (documentFile.getLicense() != null) {
      return documentFile.getLicense().getOpenLicenseId();
    }
    return null;
  }
}
