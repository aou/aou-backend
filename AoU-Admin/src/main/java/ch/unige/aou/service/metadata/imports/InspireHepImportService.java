/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - InspireHepImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.AccessLevel;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.File;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class InspireHepImportService extends MetadataAndFileImportService {
  private static final String INSPIRE_HEP_FILE_NAME = "_from_inspireHep.pdf";

  private static final Logger log = LoggerFactory.getLogger(InspireHepImportService.class);
  private String apiArxivUrl;
  private String apiDoiUrl;

  protected PublicationSubtypeService publicationSubTypeService;

  public InspireHepImportService(AouProperties aouProperties, MessageService messageService,
          MetadataService metadataService, ContributorService contributorService,
          DocumentFileTypeService documentFileTypeService, JournalTitleRemoteService journalTitleRemoteService,
          PublicationSubtypeService publicationSubTypeService, LicenseService licenseService, DocumentFileService documentFileService) {
    super(aouProperties, messageService, metadataService, contributorService, documentFileService, documentFileTypeService,
            journalTitleRemoteService, licenseService);

    this.publicationSubTypeService = publicationSubTypeService;

    this.apiArxivUrl = aouProperties.getMetadata().getImports().getInspireHep().getArxivUrl();
    this.apiDoiUrl = aouProperties.getMetadata().getImports().getInspireHep().getDoiUrl();
  }

  public DepositDoc searchInspireHep(String identifier, boolean isArxivIdentifier, DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(this.apiArxivUrl) && !StringTool.isNullOrEmpty(this.apiDoiUrl)) {
      try {
        JSONObject inspireHepDoc = this.getJsonFromInspireHep(identifier, isArxivIdentifier);
        this.fillDepositDoc(depositDoc, inspireHepDoc);

        return depositDoc;
      } catch (Exception e) {
        throw new SolidifyRuntimeException("An error occurred when searching identifier '" + identifier + "' on InspireHep", e);
      }
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  private JSONObject getJsonFromInspireHep(String identifier, boolean isArxivIdentifier) {
    try {
      RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
      RestTemplate client = restTemplateBuilder.rootUri(this.apiArxivUrl).build();

      Map<String, String> parameters = new HashMap<>();
      final String jsonString;
      if (isArxivIdentifier) {
        parameters.put("arxivId", identifier);
        jsonString = client.getForObject(this.apiArxivUrl, String.class, parameters);
      } else {
        parameters.put("doi", identifier);
        jsonString = client.getForObject(this.apiDoiUrl, String.class, parameters);
      }

      return new JSONObject(jsonString);
    } catch (HttpClientErrorException e) {
      if (isArxivIdentifier) {
        throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
                this.messageService.get("deposit.error.identifiers.arxiv.not_found_by_inspire_hep", new Object[] { identifier }));
      } else {
        throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
                this.messageService.get("deposit.error.identifiers.doi.not_found_by_inspire_hep", new Object[] { identifier }));
      }
    }
  }

  /****************************************************title************************************/

  public void fillDepositDoc(DepositDoc depositDoc, JSONObject inspireHepJson) {
    //check first if the metadata curated property is true to continue getting information from InspireHep
    if (inspireHepJson.getJSONObject("metadata").getBoolean("curated")) {
      JSONObject metadata = inspireHepJson.getJSONObject("metadata");
      this.fillTitle(depositDoc, this.getArrayOrNull(metadata, "titles"));
      this.fillPublicationType(depositDoc, this.getArrayOrNull(metadata, "document_type"));
      this.fillAbstracts(depositDoc, this.getArrayOrNull(metadata, "abstracts"));
      this.fillPublicationDate(depositDoc, this.getArrayOrNull(metadata, "publication_info"));
      this.fillDoi(depositDoc, this.getArrayOrNull(metadata, "dois"));
      this.fillContainerTitle(depositDoc, this.getArrayOrNull(metadata, "publication_info"));
      this.fillContainerVolume(depositDoc, this.getArrayOrNull(metadata, "publication_info"));
      this.fillContributors(depositDoc, this.getArrayOrNull(metadata, "authors"));
      this.fillCollaborations(depositDoc, this.getArrayOrNull(metadata, "collaborations"));
      this.fillKeywords(depositDoc, this.getArrayOrNull(metadata, "keywords"));
      this.fillNote(depositDoc, this.getArrayOrNull(metadata, "public_notes"));
      this.fillArxivId(depositDoc, this.getArrayOrNull(metadata, "arxiv_eprints"));
    } else {
      throw new SolidifyRuntimeException("The metadata found are not curated");
    }

  }

  private void fillPublicationType(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String subType = this.transformInspireHepSubType(jsonArray.getString(0));
      depositDoc.setSubtype(subType);
      Optional<PublicationSubtype> publicationSubtypeOpt = this.publicationSubTypeService.findByName(subType);
      if (publicationSubtypeOpt.isPresent()) {
        depositDoc.setType(publicationSubtypeOpt.get().getPublicationType().getName());
      }
    }
  }

  private void fillArxivId(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String value = this.getStringOrNull(jsonArray.getJSONObject(0), "value");
      if (!StringTool.isNullOrEmpty(value)) {
        this.ensureDepositIdentifiersExist(depositDoc);
        depositDoc.getIdentifiers().setArxiv(value);
      }
    }
  }

  private void fillPublicationDate(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String value = jsonArray.getJSONObject(0).get("year").toString();
      if (!StringTool.isNullOrEmpty(value)) {
        this.ensureDatesExist(depositDoc);
        DateWithType date = new DateWithType();
        date.setType(DateTypes.PUBLICATION);
        date.setContent(value);
        depositDoc.getDates().getDate().add(date);
      }
    }
  }

  private void fillTitle(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String value = this.getStringOrNull(jsonArray.getJSONObject(0), "title");
      if (!StringTool.isNullOrEmpty(value)) {
        value = this.cleanText(value);
        depositDoc.setTitle(this.getText(value));
      }
    }
  }

  private void fillAbstracts(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      this.ensureAbstractsExist(depositDoc);
      for (int i = 0; i < jsonArray.length(); i++) {
        String value = this.getStringOrNull(jsonArray.getJSONObject(i), "value");
        if (!StringTool.isNullOrEmpty(value)) {
          value = this.cleanText(value);
          depositDoc.getAbstracts().getAbstract().add(this.getText(value));
        }
      }
    }
  }

  private void fillDoi(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String value = jsonArray.getJSONObject(0).get("value").toString();
      if (!StringTool.isNullOrEmpty(value)) {
        this.ensureDepositIdentifiersExist(depositDoc);
        depositDoc.getIdentifiers().setDoi(value);
      }
    }
  }

  private void fillContainerTitle(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String journalTitle = this.getStringOrNull(jsonArray.getJSONObject(0), "journal_title");
      if (!StringTool.isNullOrEmpty(journalTitle)) {
        this.ensureContainerExist(depositDoc);
        depositDoc.getContainer().setTitle(journalTitle);
      }
    }
  }

  private void fillContainerVolume(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String volume = this.getStringOrNull(jsonArray.getJSONObject(0), "journal_volume");
      if (!StringTool.isNullOrEmpty(volume)) {
        this.ensureContainerExist(depositDoc);
        depositDoc.getContainer().setVolume(volume);
      }
    }
  }

  @SuppressWarnings("java:S2259")
  // authorName.trim() cannot throw an NPE because authorName is checked by the method isNullOrEmpty
  // this false positive should be remove when https://jira.sonarsource.com/browse/SONARJAVA-3502 will be solved
  private void fillContributors(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      this.ensureContributorsExist(depositDoc);
      for (int i = 0; i < this.getNumberMaxContributor(jsonArray.length()); i++) {
        Contributor contributor = this.createContributorFromJSONObject(jsonArray.getJSONObject(i));
        depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
      }
      // add authors that have an affiliation with unige, geneve, or hug
      List<JSONObject> authors = StreamSupport.stream(jsonArray.spliterator(), false)
              .map(a -> (JSONObject) a)
              .collect(Collectors.toList());
      List<JSONObject> authorsWithAffiliation = authors.stream()
              .filter(obj -> {
                JSONArray affiliations = this.getArrayOrNull(obj, "affiliations");
                if (affiliations != null && affiliations.length() > 0) {
                  String institution = this.getStringOrNull(affiliations.getJSONObject(0), "value");
                  if (institution.startsWith("Geneva") || institution.startsWith("Genève") || institution.startsWith("Genéve")
                          || institution.startsWith("HUG") || institution.startsWith("Unige")) {
                    return true;
                  }
                }
                return false;
              }).collect(Collectors.toList());

      for (int i = 0; i < authorsWithAffiliation.size(); i++) {
        Contributor contributor = this.createContributorFromJSONObject(authorsWithAffiliation.get(i));
        //check if it was already added
        if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                .noneMatch(c -> ((Contributor) c).getLastname().equals(contributor.getLastname())
                        && ((Contributor) c).getFirstname().equals(contributor.getFirstname()) && ((Contributor) c).getInstitution()
                        .equals(contributor.getInstitution()))) {
          depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
        }
      }
    }
  }

  private void fillCollaborations(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      this.ensureContributorsExist(depositDoc);
      for (int i = 0; i < jsonArray.length(); i++) {
        Collaboration collaboration = new Collaboration();
        collaboration.setName(this.getStringOrNull(jsonArray.getJSONObject(i), "value"));
        depositDoc.getContributors().getContributorOrCollaboration().add(collaboration);
      }
    }
  }

  private void fillKeywords(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      this.ensureKeywordsExist(depositDoc);
      for (int i = 0; i < jsonArray.length(); i++) {
        String value = this.getStringOrNull(jsonArray.getJSONObject(i), "value");
        if (!StringTool.isNullOrEmpty(value)) {
          depositDoc.getKeywords().getKeyword().add(value);
        }
      }
    }
  }

  private void fillNote(DepositDoc depositDoc, JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String value = this.getStringOrNull(jsonArray.getJSONObject(0), "value");
      depositDoc.setNote(value);
    }
  }

  private void fillFile(DepositDoc depositDoc, JSONArray jsonArray, FileType documentType, License license) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      this.ensureFileExists(depositDoc);

      String filePath = jsonArray.getJSONObject(0).get("value").toString();
      if (!StringTool.isNullOrEmpty(filePath)) {
        String fileNameWithoutExtension = filePath.lastIndexOf('?') != -1 ? filePath.substring(filePath.lastIndexOf('?') + 1) :
                filePath.substring(filePath.lastIndexOf('/') + 1);
        File file = new File();
        file.setAccessLevel(AccessLevel.PUBLIC);
        file.setMimetype("application/pdf");
        file.setName(fileNameWithoutExtension);
        file.setType(documentType);
        if (license != null) {
          file.setLicense(license.getOpenLicenseId());
        }
        depositDoc.getFiles().getFile().add(file);
      }
    }
  }

  private String transformInspireHepSubType(String subType) {
    switch (subType) {
      case "activity report":
      case "note":
      case "report":
        return AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME;
      case "article":
        return AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME;
      case "book":
        return AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME;
      case "book chapter":
        return AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME;
      case "conference paper":
        return AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME;
      case "proceedings":
        return AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME;
      case "thesis":
        return AouConstants.DEPOSIT_SUBTYPE_THESE_NAME;
      default:
        return AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME;
    }
  }

  private Contributor createContributorFromJSONObject(JSONObject authorObject) {
    String authorName = this.getStringOrNull(authorObject, "full_name");
    // Cut the author name on the last space, assuming the last word is the firstname
    String firstName = null;
    String lastName = null;
    if (!StringTool.isNullOrEmpty(authorName)) {
      authorName = authorName.trim();
      int commaIndex = authorName.lastIndexOf(',');
      if (commaIndex > -1) {
        lastName = authorName.substring(0, commaIndex);
        firstName = authorName.substring(commaIndex + 1);
      } else {
        lastName = authorName;
      }
    }
    firstName = this.getNameOrUndefinedValue(firstName);
    lastName = this.getNameOrUndefinedValue(lastName);

    Contributor contributor = new Contributor();
    contributor.setLastname(CleanTool.capitalizeString(lastName));
    contributor.setFirstname(CleanTool.capitalizeString(firstName));

    JSONArray affiliations = this.getArrayOrNull(authorObject, "affiliations");
    if (affiliations != null && affiliations.length() > 0) {
      String institution = this.getStringOrNull(affiliations.getJSONObject(0), "value");
      if (!StringTool.isNullOrEmpty(institution)) {
        contributor.setInstitution(institution);
      }
    }
    return contributor;
  }

  public void createDocumentFileFromInspireHepMetadata(Publication publication) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    DepositDoc depositDoc = (DepositDoc) metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    String arxivId = depositDoc.getIdentifiers().getArxiv();

    JSONObject inspireHepJson = this.getJsonFromInspireHep(arxivId, true);
    //check first if the metadata curated property is true to continue getting information from InspireHep
    if (inspireHepJson.getJSONObject("metadata").getBoolean("curated")) {
      JSONObject metadata = inspireHepJson.getJSONObject("metadata");

      //Get document type
      String documentType = this.getDocumentType(this.getArrayOrNull(metadata, "document_type"));
      FileType fileType = this.transformDocumentTypeToFileType(documentType);

      //Get license if present
      String licenceValue = this.getLicense(this.getArrayOrNull(metadata, "license"));
      License license = null;
      if (!StringTool.isNullOrEmpty(licenceValue)) {
        license = this.licenseService.findMoreSpecificLicenseByUrl(licenceValue);
      }
      //Fill file metadata
      this.fillFile(depositDoc, this.getArrayOrNull(metadata, "urls"), fileType, license);

      //Create the corresponding DocumentFile
      JSONArray urlsArray = this.getArrayOrNull(metadata, "urls");
      if (urlsArray != null && !urlsArray.isEmpty()) {
        String fileUrl = urlsArray.getJSONObject(0).get("value").toString();
        if (!StringTool.isNullOrEmpty(fileUrl)) {
          String fileNameWithoutExtension = fileUrl.lastIndexOf('?') != -1 ? fileUrl.substring(fileUrl.lastIndexOf('?') + 1) :
                  fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
          File file = depositDoc.getFiles().getFile().stream().filter(f -> f.getName().equals(fileNameWithoutExtension)).findFirst()
                  .orElseThrow();

          DocumentFile documentFile = new DocumentFile();
          documentFile.setPublication(publication);
          documentFile.setAccessLevel(this.convertAccessLevel(AccessLevel.PUBLIC));
          documentFile.setDocumentFileType(this.convertFileTypeDocToDocumentFileType(fileType));
          documentFile.setMimetype(file.getMimetype());
          documentFile.setIsImported(true);
          documentFile.setLicense(license);
          documentFile.setFileName(CleanTool.cleanFileName(publication.getSubtype().getName() + INSPIRE_HEP_FILE_NAME));

          try {
            documentFile.setSourceData(new URI(fileUrl));
          } catch (URISyntaxException e) {
            throw new SolidifyRuntimeException("Unable to get file URL from InspireHep", e);
          }
          this.saveDocumentFileIfSourceDataValid(documentFile);
        }
      }
    } else {
      log.error("Error when getting file from InspireHep {}", arxivId);
    }
  }

  private FileType transformDocumentTypeToFileType(String documentType) {
    switch (documentType) {
      case "article":
        return FileType.ARTICLE_SUBMITTED_VERSION;
      case "activity report":
      case "report":
      case "note":
        return FileType.REPORT;
      case "book":
        return FileType.BOOK_SUBMITTED_VERSION;
      case "book chapter":
        return FileType.BOOK_CHAPTER_SUBMITTED_VERSION;
      case "conference paper":
        return FileType.PRESENTATION;
      case "proceedings":
        return FileType.PROCEEDINGS_SUBMITTED_VERSION;
      case "thesis":
        return FileType.THESIS;
      default:
        return null;
    }
  }

  private String getDocumentType(JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String documentType = jsonArray.getString(0);
      if (!StringTool.isNullOrEmpty(documentType)) {
        return documentType;
      }
    }
    return null;
  }

  private String getLicense(JSONArray jsonArray) {
    if (jsonArray != null && !jsonArray.isEmpty()) {
      String licenseValue = this.getStringOrNull(jsonArray.getJSONObject(0), "license");
      if (!StringTool.isNullOrEmpty(licenseValue)) {
        return licenseValue;
      }
    }
    return null;
  }
}
