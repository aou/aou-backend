/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - OpenAireService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.rest.OpenAireFundingDTO;
import ch.unige.aou.model.rest.OpenAireProjectDTO;

@Service
@ConditionalOnBean(AdminController.class)
public class OpenAireService {

  private static final String XPATH_RESPONSE_HEADER = "/response/header";

  private final String apiUrl;

  public OpenAireService(AouProperties aouProperties) {
    this.apiUrl = aouProperties.getMetadata().getSearch().getOpenAire().getUrl();
  }

  public RestCollection<OpenAireProjectDTO> searchProjectsByTypeAndValue(String type, String value, Pageable pageable) {
    String url = this.getUriComponentBuilder(pageable).queryParam(type, value).toUriString();
    return this.getData(url);
  }

  private UriComponentsBuilder getUriComponentBuilder(Pageable pageable) {
    return UriComponentsBuilder.fromHttpUrl(this.apiUrl)
            .queryParam("size", pageable.getPageSize())
            .queryParam("page", pageable.getPageNumber() + 1);
  }

  private RestCollection<OpenAireProjectDTO> getData(String url) {

    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate client = restTemplateBuilder.build();
    final String openAireXml = client.getForObject(url, String.class);

    try {
      Document openAireDoc = XMLTool.parseXML(openAireXml);
      XPath xpath = XMLTool.buildXPath(this.getOpenAireNamespace());

      NodeList projectNodes = (NodeList) xpath.compile("/response/results/result/metadata/oaf:entity/oaf:project")
              .evaluate(openAireDoc, XPathConstants.NODESET);

      List<OpenAireProjectDTO> projectsList = new ArrayList<>();
      for (int i = 0; i < projectNodes.getLength(); i++) {
        Node projectNode = projectNodes.item(i);

        String program = XMLTool.getFirstTextContent(projectNode, ".//funding_level_0/name");
        String code = XMLTool.getFirstTextContent(projectNode, "./code");
        String acronym = XMLTool.getFirstTextContent(projectNode, "./acronym");
        String title = XMLTool.getFirstTextContent(projectNode, "./title");
        String fundingName = XMLTool.getFirstTextContent(projectNode, "./fundingtree/funder/name");
        String fundingShortName = XMLTool.getFirstTextContent(projectNode, "./fundingtree/funder/shortname");
        String fundingJurisdiction = XMLTool.getFirstTextContent(projectNode, "./fundingtree/funder/jurisdiction");
        String fundingDescription = this.getFundingDescription(projectNode);

        projectsList.add(new OpenAireProjectDTO(program, code, acronym, title,
                new OpenAireFundingDTO(fundingName, fundingShortName, fundingJurisdiction, fundingDescription)));
      }

      long pageNumber = Long.parseLong(XMLTool.getFirstTextContent(openAireDoc, XPATH_RESPONSE_HEADER + "/page"));
      long pageSize = Long.parseLong(XMLTool.getFirstTextContent(openAireDoc, XPATH_RESPONSE_HEADER + "/size"));
      long totalElements = Long.parseLong(XMLTool.getFirstTextContent(openAireDoc, XPATH_RESPONSE_HEADER + "/total"));
      long totalPages = (totalElements % pageSize > 0) ? (totalElements / pageSize) + 1 : totalElements / pageSize;

      RestCollectionPage page = new RestCollectionPage(pageNumber - 1, pageSize, totalPages, totalElements);
      return new RestCollection<>(projectsList, page);

    } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("unable to parse OpenAIRE data", e);
    }
  }

  /**
   * Returns the description of the top funding level in the funding levels hierarchy
   *
   * @param projectNode
   * @return
   */
  private String getFundingDescription(Node projectNode) {
    Node fundingTreeNode = XMLTool.getFirstNode(projectNode, "./fundingtree");
    if (fundingTreeNode != null) {
      // loop from 10 to 0 as the name of the XML tag contains is indexed,
      // but we don't what is the max value (seen so far was 3, but we take some margin)
      for (int i = 10; i >= 0; i--) {
        Node fundingLevelNode = XMLTool.getFirstNode(fundingTreeNode, "./funding_level_" + i);
        if (fundingLevelNode != null) {
          return XMLTool.getFirstTextContent(fundingLevelNode, "./description");
        }
      }
    }
    return null;
  }

  private HashMap<String, String> getOpenAireNamespace() {
    HashMap<String, String> namespaces = new HashMap<>();
    namespaces.put("oaf", "http://namespace.openaire.eu/oaf");
    return namespaces;
  }
}
