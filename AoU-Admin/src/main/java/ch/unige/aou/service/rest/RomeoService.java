/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - RomeoService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;

@Service
@ConditionalOnBean(AdminController.class)
public class RomeoService {

  private static final String FILTER_ON_ISSN = "[[\"issn\",\"equals\",\"%s\"]]";

  /**
   * Constants used to read format returned by ROMEO service
   */
  private static final String ROMEO_ITEMS = "items";
  private static final String ROMEO_PERMITTED_OA = "permitted_oa";
  private static final String ROMEO_PREREQUISITE_FUNDERS = "prerequisite_funders";
  private static final String ROMEO_PREREQUISITES = "prerequisites";
  private static final String ROMEO_LOCATION = "location";

  private static final String ROMEO_ARTICLE_VERSION = "article_version";

  /**
   * Constants used to build AoU format
   */
  private static final String AOU_STATUS = "archive-ouverte-status";
  private static final String AOU_LISTED_IN_DOAJ = "listedInDoaj";
  private static final String AOU_PUBLISHER_POLICIES = "publisherPolicies";
  private static final String AOU_JOURNAL_TYPE = "journalType";
  private static final String AOU_FOOTER = "footer";
  private static final String AOU_SHERPA_ROMEO_URL = "sherpaRomeoUrl";
  private static final String AOU_ERRORS = "errors";
  private static final String AOU_EMBARGOES = "embargoes";
  private static final String AOU_VERSIONS = "versions";
  private static final String AOU_OA_DIFFUSION = "oaDiffusion";
  private static final String AOU_FREE_ACCESS_NOT_AUTHORIZED = "free access not authorized";
  private static final String AOU_NOT_AUTHORIZED = "not-authorized";
  private static final String AOU_POLICIES = "policies";
  private static final String AOU_PREREQUISITE_FUNDERS = "prerequisiteFunders";

  private static final String AOU_PREREQUISITE_FUNDERS_STATUS = "prerequisite-funders";
  private static final String AOU_ADDITIONAL_FEE_STATUS = "additional-fee";
  private static final String AOU_EMPTY_STATUS = "empty";
  private static final String AOU_LOCATION_NOT_ALLOWED_STATUS = "location-not-allowed";
  private static final String AOU_OPEN_ACCESS_PROHIBITED_STATUS = "open-access-prohibited";
  private static final String AOU_PERMITTED_STATUS = "permitted";

  private static final List<String> AOU_ALLOWED_LOCATION_VALUES = List
          .of("any_repository", "any_website", "institutional_repository", "non_commercial_institutional_repository",
                  "non_commercial_repository", "non_commercial_website");

  private static final List<String> AOU_CONDITION_VALUES = List
          .of("Published source must be acknowledged with citation",
                  "Published source must be acknowledged",
                  "Must link to published version with DOI",
                  "Must link to publisher version with DOI",
                  "The publisher will deposit in on behalf of authors to a designated institutional repository, where a deposit agreement exists with the repository");

  /**
   * Mixed constants
   */
  private static final String PUBLISHED = "published";
  private static final String ACCEPTED = "accepted";
  private static final String SUBMITTED = "submitted";
  private static final String LICENCES = "licences";
  private static final String TITLE = "title";
  private static final String CONDITIONS = "conditions";
  private static final String EMBARGO = "embargo";
  private static final String VERSION = "version";
  private static final String PHRASE = "phrase";

  private final String apiUrl;
  private final String apiKey;

  public RomeoService(AouProperties aouProperties) {
    this.apiUrl = aouProperties.getMetadata().getSearch().getRomeo().getUrl();
    this.apiKey = aouProperties.getMetadata().getSearch().getRomeo().getApiKey();
  }

  public String getInfosByIssn(String issn) {
    String filter = String.format(RomeoService.FILTER_ON_ISSN, issn);

    String url = UriComponentsBuilder.fromHttpUrl(this.apiUrl)
            .queryParam("api-key", this.apiKey)
            .queryParam("item-type", "publication")
            .queryParam("format", "Json")
            .toUriString();
    url += "&filter=" + filter;

    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate client = restTemplateBuilder.build();
    final String jsonRomeoApi = client.getForObject(url, String.class);

    return this.transformRomeoJsonDataIntoAouJson(issn, jsonRomeoApi);
  }

  /**
   * Transform the Json returned by the Romeo format into a json string that can be used to display AOU specific informations
   *
   * @param issn
   * @param jsonRomeoApi
   * @return
   */
  public String transformRomeoJsonDataIntoAouJson(String issn, String jsonRomeoApi) {
    ObjectNode romeoInfosFilteredForAou = this.getDataFilteredForAouUseCase(issn, jsonRomeoApi);
    ObjectNode romeoInfosToReturn = this.formatRomeoInfos(romeoInfosFilteredForAou);

    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.enable(SerializationFeature.INDENT_OUTPUT);
      return mapper.writeValueAsString(romeoInfosToReturn);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to serialize ROMEO infos into JSON", e);
    }
  }

  /*************************************************************************/

  /**
   * Parse Json returned by ROMEO for the given ISSN, and keep only data that are related to the Archive ouverte use case (institutional repository)
   *
   * @param issn
   * @param jsonString
   * @return
   */
  private ObjectNode getDataFilteredForAouUseCase(String issn, String jsonString) {
    try {
      final ObjectMapper mapper = new ObjectMapper();
      JsonNode romeoJson = mapper.readTree(jsonString);

      ObjectNode romeoInfos = mapper.createObjectNode();

      // Discard eventual former publisher items
      this.discardFormerPublishers(romeoJson);

      // If there are still more than one item, keep only the first one
      if (romeoJson.has(ROMEO_ITEMS) && romeoJson.get(ROMEO_ITEMS).isArray() && !romeoJson.get(ROMEO_ITEMS).isEmpty()) {

        JsonNode romeoItemNode = romeoJson.get(ROMEO_ITEMS).get(0);

        romeoInfos.put("issn", issn);
        romeoInfos.put(TITLE, romeoItemNode.get(TITLE).get(0).get(TITLE).asText());
        romeoInfos.put(AOU_SHERPA_ROMEO_URL, romeoItemNode.get("system_metadata").get("uri").asText());
        romeoInfos.put(AOU_LISTED_IN_DOAJ, romeoItemNode.get("listed_in_doaj").asText().equals("yes"));

        ObjectNode publisherPoliciesNode = mapper.createObjectNode();
        publisherPoliciesNode.set(PUBLISHED, this.getFilteredPublisherPolicy(romeoItemNode, PUBLISHED));
        publisherPoliciesNode.set(ACCEPTED, this.getFilteredPublisherPolicy(romeoItemNode, ACCEPTED));
        publisherPoliciesNode.set(SUBMITTED, this.getFilteredPublisherPolicy(romeoItemNode, SUBMITTED));
        romeoInfos.set(AOU_PUBLISHER_POLICIES, publisherPoliciesNode);

        JournalType type = this.getJournalType(romeoInfos);
        if (type != null) {
          romeoInfos.put(AOU_JOURNAL_TYPE, type.value());
        } else {
          romeoInfos.putNull(AOU_JOURNAL_TYPE);
        }
        romeoInfos.put("journalTypeText", this.getJournalTypeText(type));

        ArrayNode footer = mapper.createArrayNode();
        footer.add(String.format("Whole set of conditions for this journal can be found in Sherpa Romeo: {%s}.", AOU_SHERPA_ROMEO_URL));
        romeoInfos.set(AOU_FOOTER, footer);

      } else {
        ArrayNode errors = mapper.createArrayNode();
        errors.add("Sherpa Romeo does not provide information for this journal.");
        romeoInfos.set(AOU_ERRORS, errors);
      }

      return romeoInfos;

    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to retrieve ROMEO infos for the AoU use case", e);
    }
  }

  private void discardFormerPublishers(JsonNode romeoNode) {
    List<Integer> indexesToRemove = new ArrayList<>();

    JsonNode itemsNode = romeoNode.get(ROMEO_ITEMS);
    if (itemsNode.isArray()) {
      int itemNodeIndex = 0;
      for (JsonNode itemNode : itemsNode) {
        JsonNode publishersNode = itemNode.get("publishers");
        for (JsonNode publisherNode : publishersNode) {
          if (publisherNode.has("relationship_type") && publisherNode.get("relationship_type").asText().equals("former_publisher")) {
            indexesToRemove.add(itemNodeIndex);
          }
        }
        itemNodeIndex++;
      }
    }

    for (int indexToRemove : indexesToRemove) {
      ((ArrayNode) itemsNode).remove(indexToRemove);
    }
  }

  private ArrayNode getFilteredPublisherPolicy(JsonNode romeoItemNode, String searchedArticleVersion) {
    ObjectMapper mapper = new ObjectMapper();

    List<JsonNode> matchingLevelPoliciesNodes = this.getPermittedOpenAccessPublisherPolicies(romeoItemNode, searchedArticleVersion);

    int totalOpenAccessFound = 0;

    ArrayNode matchingLevelFilteredPermittedOpenAccess = mapper.createArrayNode();

    for (JsonNode matchingLevelPolicyNode : matchingLevelPoliciesNodes) {
      for (JsonNode permittedOaNode : matchingLevelPolicyNode.get(ROMEO_PERMITTED_OA)) {
        totalOpenAccessFound++;

        boolean permittedOaMatch = false;
        if (permittedOaNode.has(ROMEO_ARTICLE_VERSION)) {
          JsonNode articleVersionsNode = permittedOaNode.get(ROMEO_ARTICLE_VERSION);
          if (this.arrayNodeContainsString(articleVersionsNode, searchedArticleVersion)) {
            permittedOaMatch = true;
          }
        }

        if (permittedOaMatch) {
          ObjectNode matchingLevelPermittedOa = mapper.createObjectNode();

          matchingLevelPermittedOa.put(AOU_STATUS, "compatible");

          /*
           * Test if additional Open Access fee are required
           */
          if (permittedOaNode.has("additional_oa_fee") && permittedOaNode.get("additional_oa_fee").asText().equals("yes")) {
            matchingLevelPermittedOa.put(AOU_STATUS, AOU_ADDITIONAL_FEE_STATUS);
          }

          /*
           * If Open Access is allowed only for specific funders -> discard (so far Swiss FN is not in Romeo)
           */
          if (permittedOaNode.has(ROMEO_PREREQUISITES) && permittedOaNode.get(ROMEO_PREREQUISITES).has(ROMEO_PREREQUISITE_FUNDERS)) {
            matchingLevelPermittedOa.put(AOU_STATUS, AOU_PREREQUISITE_FUNDERS_STATUS);
          }

          /*
           * Test if location contains institutional repositories
           */
          this.setMatchingLocation(permittedOaNode, matchingLevelPermittedOa);

          /*
           * If Open Access is prohibited --> discard
           */
          if (matchingLevelPolicyNode.has("open_access_prohibited")
                  && matchingLevelPolicyNode.get("open_access_prohibited").asText().equals("yes")) {
            matchingLevelPermittedOa.put(AOU_STATUS, AOU_OPEN_ACCESS_PROHIBITED_STATUS);
          }

          /*
           * Licences
           */
          this.setLicences(permittedOaNode, matchingLevelPermittedOa);

          /*
           * Conditions
           */
          if (permittedOaNode.has(CONDITIONS)) {
            matchingLevelPermittedOa.set(CONDITIONS, permittedOaNode.get(CONDITIONS));
          }

          /*
           * Embargoes
           */
          if (permittedOaNode.has(EMBARGO)) {
            matchingLevelPermittedOa.set(AOU_EMBARGOES, permittedOaNode.get(EMBARGO));
          }

          /*
           * Prerequisite funders
           */
          this.setPrerequisiteFunders(permittedOaNode, matchingLevelPermittedOa);

          matchingLevelFilteredPermittedOpenAccess.add(matchingLevelPermittedOa);
        }
      }
    }

    if (totalOpenAccessFound == 0) {
      //no info found for the diffusion level -> open access not authorized
      ObjectNode matchingLevelPermittedOa = mapper.createObjectNode();
      matchingLevelPermittedOa.put(AOU_STATUS, AOU_EMPTY_STATUS);
      matchingLevelFilteredPermittedOpenAccess.add(matchingLevelPermittedOa);
    }

    return matchingLevelFilteredPermittedOpenAccess;
  }

  /**
   * Test if location contains institutional repositories. If not --> discard
   *
   * @return
   */
  private void setMatchingLocation(JsonNode permittedOaNode, ObjectNode matchingLevelPermittedOa) {
    boolean allowedLocation = true;
    if (permittedOaNode.has(ROMEO_LOCATION)) {
      allowedLocation = false;
      for (JsonNode locationNode : permittedOaNode.get(ROMEO_LOCATION)) {
        for (JsonNode locationTextNode : locationNode) {
          if (AOU_ALLOWED_LOCATION_VALUES.contains(locationTextNode.asText())) {
            allowedLocation = true;
            matchingLevelPermittedOa.put("matching-location", locationTextNode.asText());
            break;
          }
        }
      }
    }
    if (!allowedLocation) {
      matchingLevelPermittedOa.put(AOU_STATUS, AOU_LOCATION_NOT_ALLOWED_STATUS);
    }
  }

  private void setLicences(JsonNode permittedOaNode, ObjectNode matchingLevelPermittedOa) {
    if (permittedOaNode.has("license")) {
      ObjectMapper mapper = new ObjectMapper();
      matchingLevelPermittedOa.putArray(LICENCES);
      for (JsonNode licenceNode : permittedOaNode.get("license")) {
        ObjectNode oaLicenceObj = mapper.createObjectNode();
        oaLicenceObj.put(PHRASE, licenceNode.get("license_phrases").get(0).get(PHRASE).asText());
        if (licenceNode.has(VERSION)) {
          oaLicenceObj.put(VERSION, licenceNode.get(VERSION).asText());
        }
        ((ArrayNode) matchingLevelPermittedOa.get(LICENCES)).add(oaLicenceObj);
      }
    }
  }

  private void setPrerequisiteFunders(JsonNode permittedOaNode, ObjectNode matchingLevelPermittedOa) {
    if (permittedOaNode.has(ROMEO_PREREQUISITES) && permittedOaNode.get(ROMEO_PREREQUISITES).has(ROMEO_PREREQUISITE_FUNDERS)) {
      ObjectMapper mapper = new ObjectMapper();
      matchingLevelPermittedOa.putArray(AOU_PREREQUISITE_FUNDERS);
      for (JsonNode funderNode : permittedOaNode.get(ROMEO_PREREQUISITES).get(ROMEO_PREREQUISITE_FUNDERS)) {
        ObjectNode funderObj = mapper.createObjectNode();
        funderObj.put("name", funderNode.get("funder_metadata").get("name").get(0).get("name").asText());
        funderObj.put("url", funderNode.get("funder_metadata").get("url").get(0).get("url").asText());
        ((ArrayNode) matchingLevelPermittedOa.get(AOU_PREREQUISITE_FUNDERS)).add(funderObj);
      }
    }
  }

  private List<JsonNode> getPermittedOpenAccessPublisherPolicies(JsonNode itemNode, String searchedArticleVersion) {

    List<JsonNode> openAccessPublisherPolicies = new ArrayList<>();

    JsonNode publisherPoliciesNode = itemNode.get("publisher_policy");
    for (JsonNode publisherPolicyNode : publisherPoliciesNode) {
      boolean matchingPolicy = false;
      if (publisherPolicyNode.has(ROMEO_PERMITTED_OA)) {
        for (JsonNode permittedOaNode : publisherPolicyNode.get(ROMEO_PERMITTED_OA)) {
          if (permittedOaNode.has(ROMEO_ARTICLE_VERSION)) {
            JsonNode articleVersionsNode = permittedOaNode.get(ROMEO_ARTICLE_VERSION);
            if (this.arrayNodeContainsString(articleVersionsNode, searchedArticleVersion)) {
              matchingPolicy = true;
              break;
            }
          }
        }
      }

      if (matchingPolicy) {
        openAccessPublisherPolicies.add(publisherPolicyNode);
      }
    }

    return openAccessPublisherPolicies;
  }

  private JournalType getJournalType(ObjectNode romeoInfos) {
    // use LinkedHashSet to keep insert order while preventing containing the same value twice
    LinkedHashSet<JournalType> types = new LinkedHashSet<>();

    JsonNode permittedOas = romeoInfos.get(AOU_PUBLISHER_POLICIES).get(PUBLISHED);

    if (romeoInfos.get(AOU_LISTED_IN_DOAJ).asBoolean()) {
      types.add(JournalType.GOLD);
    } else if (permittedOas.isEmpty()) {
      //msg 4
      types.add(JournalType.SUBSCRIPTION);
    } else {
      for (JsonNode permittedOa : permittedOas) {
        String status = permittedOa.get(AOU_STATUS).asText();
        if (!status.equals(AOU_LOCATION_NOT_ALLOWED_STATUS)) {
          types.add(this.getJournalTypeFromStatus(status));
        }
      }
    }

    if (types.size() > 1 && types.contains(JournalType.HYBRID)) {
      return JournalType.HYBRID;
    }

    return types.stream().findFirst().orElse(null);
  }

  private JournalType getJournalTypeFromStatus(String status) {
    if (status.equals(AOU_OPEN_ACCESS_PROHIBITED_STATUS)) {
      return JournalType.SUBSCRIPTION;
    } else if (status.equals(AOU_PREREQUISITE_FUNDERS_STATUS)) {
      return JournalType.FUNDERS;
    } else if (status.equals(AOU_ADDITIONAL_FEE_STATUS)) {
      return JournalType.HYBRID;
    } else if (status.equals(AOU_PERMITTED_STATUS)) {
      return JournalType.GOLD;
    } else if (status.equals(AOU_EMPTY_STATUS)) {
      return JournalType.EMPTY;
    } else {
      return JournalType.UNDETERMINED;
    }
  }

  private String getJournalTypeText(JournalType journalType) {

    if (journalType == null) {
      journalType = JournalType.UNDETERMINED;
    }

    switch (journalType) {
      case SUBSCRIPTION:
      case EMPTY:
        return "Subscription. This journal's articles are only available to subscribers.";
      case HYBRID:
        return "Hybrid. This journal contains both subscribers-only and Open Access articles.";
      case GOLD:
        return "Gold. This journal only publishes Open Access articles.";
      default:
        return "Undetermined, due to insufficient information in Sherpa Romeo";
    }

  }

  /*************************************************************************/

  /**
   * Simplify ROMEO infos that are returned by the endpoint according to the logic AoU logic:
   * - funding messages are put in footer
   * - licences, conditions and embargo infos are merged by diffusion text
   *
   * @param romeoInfosFilteredForAou
   * @return
   */
  private ObjectNode formatRomeoInfos(ObjectNode romeoInfosFilteredForAou) {
    final ObjectMapper mapper = new ObjectMapper();
    ObjectNode aouRomeoInfos = mapper.createObjectNode();

    if (!romeoInfosFilteredForAou.has(AOU_ERRORS)) {
      boolean listedInDoaj = romeoInfosFilteredForAou.get(AOU_LISTED_IN_DOAJ).asBoolean();
      aouRomeoInfos.put("issn", romeoInfosFilteredForAou.get("issn").asText());
      aouRomeoInfos.put(AOU_LISTED_IN_DOAJ, listedInDoaj);
      aouRomeoInfos.put(TITLE, romeoInfosFilteredForAou.get(TITLE).asText());
      aouRomeoInfos.put(AOU_SHERPA_ROMEO_URL, romeoInfosFilteredForAou.get(AOU_SHERPA_ROMEO_URL).asText());
      aouRomeoInfos.put(AOU_JOURNAL_TYPE, romeoInfosFilteredForAou.get("journalTypeText").asText());

      aouRomeoInfos.putArray(AOU_FOOTER);
      for (JsonNode footerText : romeoInfosFilteredForAou.get(AOU_FOOTER)) {
        ((ArrayNode) aouRomeoInfos.get(AOU_FOOTER)).add(footerText.asText());
      }

      ObjectNode versionsNode = mapper.createObjectNode();
      if (listedInDoaj) {
        versionsNode.set(PUBLISHED, this.getVersionNode(romeoInfosFilteredForAou, PUBLISHED));
      } else {
        versionsNode.set(PUBLISHED, this.getVersionNode(romeoInfosFilteredForAou, PUBLISHED));
        versionsNode.set(SUBMITTED, this.getVersionNode(romeoInfosFilteredForAou, SUBMITTED));
        versionsNode.set(ACCEPTED, this.getVersionNode(romeoInfosFilteredForAou, ACCEPTED));
      }
      aouRomeoInfos.set(AOU_VERSIONS, versionsNode);

      /*
       * Simplify policies infos
       */
      for (String level : List.of(PUBLISHED, ACCEPTED, SUBMITTED)) {
        if (aouRomeoInfos.get(AOU_VERSIONS).has(level)) {
          ArrayNode policiesNode = (ArrayNode) aouRomeoInfos.get(AOU_VERSIONS).get(level).get(AOU_POLICIES);
          this.moveFundingMessageInFooter(aouRomeoInfos, policiesNode);
          this.mergePoliciesInfosIfSameOaDiffusionMessages(policiesNode);
        }
      }
    } else {
      aouRomeoInfos.set(AOU_ERRORS, romeoInfosFilteredForAou.get(AOU_ERRORS));
    }

    return aouRomeoInfos;
  }

  private void moveFundingMessageInFooter(JsonNode romeo, ArrayNode policiesNode) {
    List<Integer> policyIndexesToRemove = new ArrayList<>();
    int i = 0;
    for (JsonNode policyNode : policiesNode) {
      if (policyNode.has(AOU_OA_DIFFUSION) && policyNode.get(AOU_OA_DIFFUSION).get("text").asText()
              .equals("If your research project was supported by a funder, more advantageous conditions can apply (see link below).")) {
        List<String> footerValues = new ArrayList<>();
        for (JsonNode footerTextNode : romeo.get(AOU_FOOTER)) {
          footerValues.add(footerTextNode.asText());
        }
        // add text into the footer
        if (!footerValues.contains((policyNode.get(AOU_OA_DIFFUSION).get("text").asText()))) {
          ((ArrayNode) romeo.get(AOU_FOOTER)).insert(0, policyNode.get(AOU_OA_DIFFUSION).get("text").asText());
        }
        // remove text from the policy
        policyIndexesToRemove.add(i);
        ((ObjectNode) policyNode.get(AOU_OA_DIFFUSION)).remove("text");
      }
      i++;
    }
    // delete policies whose funding message has been moved into footer
    if (!policyIndexesToRemove.isEmpty()) {
      Collections.reverse(policyIndexesToRemove); // loop in reverse to preserve indexes when deleting
      for (int indexToRemove : policyIndexesToRemove) {
        policiesNode.remove(indexToRemove);
      }
    }
  }

  private void mergePoliciesInfosIfSameOaDiffusionMessages(ArrayNode policiesNode) {
    Map<String, Integer> textIndexes = new HashMap<>();
    int i = 0;
    List<Integer> policyIndexesToRemove = new ArrayList<>();
    for (JsonNode policyNode : policiesNode) {
      if (policyNode.has(AOU_OA_DIFFUSION) && policyNode.get(AOU_OA_DIFFUSION).has("text")) {

        String diffusionText = policyNode.get(AOU_OA_DIFFUSION).get("text").asText();

        if (!textIndexes.containsKey(diffusionText)) {
          textIndexes.put(diffusionText, i);
        } else {
          //same message found --> merge licenses, embargo and conditions
          int index = textIndexes.get(diffusionText);

          ObjectNode policyNodeAtIndex = (ObjectNode) policiesNode.get(index);

          // merge licences into previous policy with same text
          this.mergePolicyInfo(LICENCES, policyNode, policyNodeAtIndex);

          // merge conditions into previous policy with same text
          this.mergePolicyInfo(CONDITIONS, policyNode, policyNodeAtIndex);

          // merge embargoes into previous policy with same text (in this case values are concatenated with a " / ")
          if (policyNode.has(EMBARGO)) {
            if (!policyNodeAtIndex.has(EMBARGO)) {
              policyNodeAtIndex.put(EMBARGO, policyNode.get(EMBARGO).asText());
            } else {
              String embargoStr = policyNodeAtIndex.get(EMBARGO).asText();
              policyNodeAtIndex.put(EMBARGO, embargoStr + " / " + policyNode.get(EMBARGO));
            }
          }

          // add merged policy to the list of policies to remove
          policyIndexesToRemove.add(i);
        }
      }
      i++;
    }

    // delete policies whose infos have been merged into another one
    if (!policyIndexesToRemove.isEmpty()) {
      Collections.reverse(policyIndexesToRemove); // loop in reverse to preserve indexes when deleting
      for (int indexToRemove : policyIndexesToRemove) {
        policiesNode.remove(indexToRemove);
      }
    }
  }

  private void mergePolicyInfo(String propertyName, JsonNode currentPolicyNode, ObjectNode sameDiffusionPolicyNode) {
    if (currentPolicyNode.has(propertyName)) {
      if (!sameDiffusionPolicyNode.has(propertyName)) {
        sameDiffusionPolicyNode.putArray(propertyName);
      }
      for (JsonNode licenceNode : currentPolicyNode.get(propertyName)) {
        ((ArrayNode) sameDiffusionPolicyNode.get(propertyName)).add(licenceNode.asText());
      }
    }
  }

  private ObjectNode getVersionNode(ObjectNode aouRomeoInfos, String searchedArticleVersion) {
    final ObjectMapper mapper = new ObjectMapper();
    ObjectNode versionNode = mapper.createObjectNode();

    if (searchedArticleVersion.equals(PUBLISHED)) {
      versionNode.put(VERSION, "published version");
    } else if (searchedArticleVersion.equals(ACCEPTED)) {
      versionNode.put(VERSION, "accepted version");
    } else if (searchedArticleVersion.equals(SUBMITTED)) {
      versionNode.put(VERSION, "submitted version");
    }

    versionNode.set(AOU_POLICIES, this.getPoliciesArrayNode(aouRomeoInfos.get(AOU_PUBLISHER_POLICIES).get(searchedArticleVersion)));
    return versionNode;
  }

  private ArrayNode getPoliciesArrayNode(JsonNode romeoPolicies) {
    final ObjectMapper mapper = new ObjectMapper();

    ArrayNode policies = mapper.createArrayNode();

    for (JsonNode romeoPolicy : romeoPolicies) {
      if (!romeoPolicy.get(AOU_STATUS).asText().equals(AOU_LOCATION_NOT_ALLOWED_STATUS)) {
        ObjectNode policyNode = mapper.createObjectNode();

        ObjectNode oaDiffusionNode = this.getOpenAccessDiffusionNode(romeoPolicy);
        ArrayNode licencesNode = this.getLicencesArrayNode(romeoPolicy);
        String embargo = this.getEmbargoText(romeoPolicy);
        ArrayNode conditionsNode = this.getConditionsArrayNode(romeoPolicy);

        if (oaDiffusionNode != null && !oaDiffusionNode.isEmpty()) {
          policyNode.set(AOU_OA_DIFFUSION, oaDiffusionNode);
        }

        if (licencesNode != null && !licencesNode.isEmpty()) {
          policyNode.set(LICENCES, licencesNode);
        }

        if (!StringTool.isNullOrEmpty(embargo)) {
          policyNode.put(EMBARGO, embargo);
        }

        if (conditionsNode != null && !conditionsNode.isEmpty()) {
          policyNode.set(CONDITIONS, conditionsNode);
        }

        policies.add(policyNode);
      }
    }

    if (policies.isEmpty()) {
      ObjectNode oaDiffusionNode = mapper.createObjectNode();
      oaDiffusionNode.put("icon", AOU_NOT_AUTHORIZED);
      oaDiffusionNode.put("text", AOU_FREE_ACCESS_NOT_AUTHORIZED);
      ObjectNode policyNode = mapper.createObjectNode();
      policyNode.set(AOU_OA_DIFFUSION, oaDiffusionNode);
      policies.add(policyNode);
    }

    return policies;
  }

  private ObjectNode getOpenAccessDiffusionNode(JsonNode romeoPolicy) {
    final ObjectMapper mapper = new ObjectMapper();
    ObjectNode oaDiffusion = mapper.createObjectNode();

    String status = romeoPolicy.get(AOU_STATUS).asText();
    if (status.equals("compatible")) {
      oaDiffusion.put("icon", "authorized");
      oaDiffusion.put("text", "free access authorized");
    } else if (status.equals(AOU_ADDITIONAL_FEE_STATUS)) {
      oaDiffusion.put("icon", "question");
      oaDiffusion.put("text",
              "Free access authorized, as long as an APC fee was paid for the article or the document includes an <em>Open Access</em> or <em>Creative Commons</em> mention");
    } else if (status.equals(AOU_PREREQUISITE_FUNDERS_STATUS)) {
      oaDiffusion.put("text", "If your research project was supported by a funder, more advantageous conditions can apply (see link below).");
    } else if (status.equals(AOU_OPEN_ACCESS_PROHIBITED_STATUS) || status.equals(AOU_EMPTY_STATUS)) {
      oaDiffusion.put("icon", AOU_NOT_AUTHORIZED);
      oaDiffusion.put("text", AOU_FREE_ACCESS_NOT_AUTHORIZED);
    } else {
      oaDiffusion.put("text", "undetermined access level");
    }
    return oaDiffusion;
  }

  private ArrayNode getLicencesArrayNode(JsonNode romeoPolicy) {
    final ObjectMapper mapper = new ObjectMapper();
    ArrayNode licences = mapper.createArrayNode();

    if (romeoPolicy.has(LICENCES)) {
      for (JsonNode licenceNode : romeoPolicy.get(LICENCES)) {
        String licenceText = licenceNode.get(PHRASE).asText();
        if (licenceNode.has(VERSION)) {
          licenceText += " " + licenceNode.get(VERSION);
        }
        licences.add(licenceText);
      }
    }

    return licences;
  }

  private String getEmbargoText(JsonNode romeoPolicy) {
    if (romeoPolicy.has(AOU_EMBARGOES)) {
      return romeoPolicy.get(AOU_EMBARGOES).get("amount").asText() + " " + romeoPolicy.get(AOU_EMBARGOES).get("units").asText();
    }
    return null;
  }

  private ArrayNode getConditionsArrayNode(JsonNode romeoPolicy) {
    final ObjectMapper mapper = new ObjectMapper();
    ArrayNode conditions = mapper.createArrayNode();

    if (romeoPolicy.has(CONDITIONS)) {
      for (JsonNode conditionNode : romeoPolicy.get(CONDITIONS)) {
        if (!AOU_CONDITION_VALUES.contains(conditionNode.asText())) {
          conditions.add(conditionNode.asText());
        }
      }
    }

    return conditions;
  }

  /*************************************************************************/

  private boolean arrayNodeContainsString(JsonNode node, String searchedValue) {
    if (node instanceof ArrayNode) {
      for (JsonNode childNode : node) {
        if (childNode.asText().equals(searchedValue)) {
          return true;
        }
      }
      return false;
    } else {
      throw new SolidifyRuntimeException("Node is not an array");
    }
  }

  /*************************************************************************/

  private enum JournalType {
    SUBSCRIPTION("subscription"),
    HYBRID("hybrid"),
    EMPTY("empty"),
    FUNDERS("funders"),
    GOLD("gold"),
    UNDETERMINED("undetermined");

    private final String value;

    private JournalType(String value) {
      this.value = value;
    }

    public String value() {
      return this.value;
    }
  }
}
