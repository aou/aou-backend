/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - AbstractAdminPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.security;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.business.PersonService;

public abstract class AbstractAdminPermissionService extends AbstractPermissionService {

  protected PersonService personService;

  public AbstractAdminPermissionService(PersonService personService) {
    this.personService = personService;
  }

  protected String getPersonAuthenticatedId() {
    return this.personService.getLinkedPersonId(this.getAuthentication());
  }

  public boolean isUserConnectedLinkedToPerson(String personId) {
    if (StringTool.isNullOrEmpty(personId)) {
      return false;
    }
    return personId.equals(this.getPersonAuthenticatedId());
  }
}
