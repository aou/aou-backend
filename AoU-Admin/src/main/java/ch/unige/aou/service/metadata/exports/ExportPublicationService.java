/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ExportMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.exports;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.CommentService;
import ch.unige.aou.business.PublicationStructureService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.access.ExportFormat;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.list_deposits.Publications;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.metadata.PublicationsAdapter;
import ch.unige.aou.service.metadata.Xml2BibTexConvertor;
import ch.unige.aou.service.metadata.Xml2TsvConvertor;

@Service
@ConditionalOnBean(AdminController.class)
public class ExportPublicationService {

  private final MetadataService metadataService;
  private final PublicationsAdapter publicationsAdapter;
  private final PublicationStructureService publicationStructureService;
  private final CommentService commentService;
  private final HistoryService historyService;
  private final UserService userService;

  public ExportPublicationService(MetadataService metadataService, PublicationsAdapter publicationsAdapter,
          PublicationStructureService publicationStructureService, CommentService commentService, HistoryService historyService,
          UserService userService) {
    this.metadataService = metadataService;
    this.publicationsAdapter = publicationsAdapter;
    this.publicationStructureService = publicationStructureService;
    this.commentService = commentService;
    this.historyService = historyService;
    this.userService = userService;
  }

  public String exportMetadata(ExportFormat format, List<PublicationIndexEntry> indexResults) {
    return switch (format) {
      case JSON -> this.exportPublicationToJson(indexResults);
      case XML -> this.exportPublicationToXml(indexResults);
      case TSV -> this.exportPublicationToTsv(indexResults);
      case BIBTEX -> this.exportPublicationToBibtex(indexResults);
    };
  }

  private String exportPublicationToTsv(List<PublicationIndexEntry> indexResults) {
    Publications publications = this.getPublications(indexResults);
    return Xml2TsvConvertor.convertToTSV(publications);
  }

  private String exportPublicationToBibtex(List<PublicationIndexEntry> indexResults) {
    Publications publications = this.getPublications(indexResults);
    return Xml2BibTexConvertor.convertToBibTex(publications);
  }

  public String exportPublicationInProgressToTsv(List<PublicationIndexEntry> indexResults) {
    Publications publications = this.getPublications(indexResults, false);
    Map<String, Map<String, Object>> additionalInfoPublications = this.getAdditionalInfoFromPublications(indexResults);
    return Xml2TsvConvertor.convertAllToTSV(publications, additionalInfoPublications);
  }

  private String exportPublicationToXml(List<PublicationIndexEntry> indexResults) {
    Publications publications = this.getPublications(indexResults);
    return this.publicationsAdapter.serializePublicationsToXml(publications);
  }

  private String exportPublicationToJson(List<PublicationIndexEntry> indexResults) {
    Publications publications = this.getPublications(indexResults);
    return this.publicationsAdapter.serializePublicationsToJson(publications);
  }

  public Publications getPublications(List<PublicationIndexEntry> indexResults) {
    return this.getPublications(indexResults, true);
  }

  public Publications getPublications(List<PublicationIndexEntry> indexResults, boolean deleteSensitiveInformation) {
    Publications publications = new Publications();
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
    for (PublicationIndexEntry indexEntry : indexResults) {
      final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(indexEntry.getXmlMetadata());
      if (deleteSensitiveInformation) {
        this.publicationsAdapter.removeSensitiveInformationFromDepositDoc((DepositDoc) depositDoc);
      }
      // When getting publications not published yet, archiveId will be null so resId will be set instead
      String archiveId = indexEntry.getArchiveId() != null ? indexEntry.getArchiveId() : indexEntry.getResId();
      this.publicationsAdapter.addPublication(publications, depositDoc, archiveId, indexEntry.getLastUpdateDate());
    }
    return publications;
  }

  public Map<String, Map<String, Object>> getAdditionalInfoFromPublications(List<PublicationIndexEntry> indexResults) {
    Map<String, Map<String, Object>> additionalInfo = new HashMap<>();
    for (PublicationIndexEntry index : indexResults) {
      Map<String, Object> infos = new HashMap<>();
      infos.put("status", index.getStatus());
      infos.put("depositor", index.getCreator());
      infos.put("depositDate", index.getCreationDate());
      infos.put("validatorName", index.getValidatorName());
      infos.put("validationDate", index.getValidationDate());
      infos.put("lastUpdateDate", index.getLastUpdateDate());

      List<Comment> byPublication = this.commentService.findByPublication(index.getResId());
      infos.put("validatorComment", byPublication.stream().filter(Comment::isOnlyForValidators).map(Comment::getText).toList());
      infos.put("publicComment", byPublication.stream().filter(c -> !c.isOnlyForValidators()).map(Comment::getText).toList());

      List<String> validationStructuresNames = this.publicationStructureService.findByPublication(index.getResId()).stream()
              .filter(s -> s.getLinkType().equals(PublicationStructure.LinkType.METADATA)).map(p -> p.getStructure().getName()).toList();
      infos.put("validationStructure", validationStructuresNames);

      List<String> validationStructuresNamesNotUnige = this.publicationStructureService.findByPublication(index.getResId()).stream()
              .filter(s -> s.getLinkType().equals(PublicationStructure.LinkType.VALIDATION)).map(p -> p.getStructure().getName()).toList();
      infos.put("validationStructuresNamesNotUnige", validationStructuresNamesNotUnige);

      StatusHistory statusHistory = this.historyService.findStatusHistoryOrderByLastDate(index.getResId()).stream().findFirst().orElse(null);
      if (statusHistory != null) {
        try {
          User user = this.userService.findByExternalUid(statusHistory.getCreatedBy());
          infos.put("lastPersonModify", user.getFullName());
        } catch (NoSuchElementException e) {
          infos.put("lastPersonModify", "");
        }
      } else {
        infos.put("lastPersonModify", "");
      }

      additionalInfo.put(index.getArchiveId() != null ? index.getArchiveId() : index.getResId(), infos);
    }
    return additionalInfo;
  }
}
