/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - JournalTitleRemoteService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.rest.IssnAuthInfo;
import ch.unige.aou.model.rest.JournalTitleDTO;
import ch.unige.aou.model.rest.SearchDTO;
import ch.unige.aou.model.tool.CleanTool;

@Service
@ConditionalOnBean(AdminController.class)
public class JournalTitleRemoteService {
  private static final Logger log = LoggerFactory.getLogger(JournalTitleRemoteService.class);
  private static final long DOWNLOAD_TOKEN_INTERVAL = 600000; // ten minutes
  private static final String AUTHENTICATE = "authenticate";
  private static final String NOTICE = "notice";
  private static final String SEARCH = "search";

  private final String remoteServiceUrl;
  private final String user;
  private final String apiKey;

  private String OAuth2Token;

  public JournalTitleRemoteService(AouProperties aouProperties) {
    this.remoteServiceUrl = aouProperties.getMetadata().getSearch().getJournal().getUrl();
    this.user = aouProperties.getMetadata().getSearch().getJournal().getUser();
    this.apiKey = aouProperties.getMetadata().getSearch().getJournal().getApiKey();
  }

  @Scheduled(fixedDelay = DOWNLOAD_TOKEN_INTERVAL)
  protected String getOAuth2TokenFromExternalService() {
    if (!StringTool.isNullOrEmpty(this.remoteServiceUrl) && !StringTool.isNullOrEmpty(this.user) && !StringTool.isNullOrEmpty(this.apiKey)) {
      try {
        String url = this.remoteServiceUrl + "/" + AUTHENTICATE + "/" + this.user + "/" + this.apiKey;
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate client = restTemplateBuilder.rootUri(url).build();

        final IssnAuthInfo jsonString = client.getForObject(url, IssnAuthInfo.class);
        this.OAuth2Token = !Objects.isNull(jsonString) ? jsonString.getToken() : null;
        return this.OAuth2Token;
      } catch (RestClientException e) {
        throw new SolidifyRuntimeException("Unable to get data from " + this.remoteServiceUrl, e);
      }
    } else {
      throw new SolidifyRuntimeException("incomplete configuration");
    }
  }

  public RestCollection<JournalTitleDTO> getJournalTitlesByTitle(SearchDTO searchDTO) {
    String url = this.remoteServiceUrl + "/" + SEARCH + "?natifjson=true";
    this.refreshToken();
    HttpHeaders headers = new HttpHeaders();
    RestTemplate client = this.buildRestClientWithParams(url, headers);
    // build the request
    HttpEntity<SearchDTO> request = new HttpEntity<>(searchDTO, headers);
    List<JournalTitleDTO> journalTitlesList = new ArrayList<>();
    try {
      // make an HTTP GET request with headers
      ResponseEntity<String> response = client.exchange(url, HttpMethod.POST, request, String.class);
      if (response.getStatusCode() == HttpStatus.OK) {
        final ObjectMapper mapper = new ObjectMapper();
        JsonNode neoJsonNode = mapper.readTree(response.getBody());
        if (neoJsonNode.getNodeType().equals(JsonNodeType.ARRAY)) {
          for (int i = 0; i <= neoJsonNode.size(); i++) {
            JournalTitleDTO journalTitle = new JournalTitleDTO();
            final ObjectMapper mapper1 = new ObjectMapper();
            JsonNode jsonNode = mapper1.readTree(String.valueOf(neoJsonNode.get(i)));
            JsonNode graphs = jsonNode.get("fields");
            this.extractFromHttpResponse(journalTitle, graphs);
            if (journalTitle.getIssn() != null) {
              journalTitlesList.add(journalTitle);
            }
          }
        }
      }
    } catch (JsonProcessingException e) {
      log.error("Error can't parse json response for the search by ISSN/Title", e);
    } catch (RestClientException e) {
      log.error("Unable to get data from " + this.remoteServiceUrl, e);
    }
    RestCollectionPage page = new RestCollectionPage(searchDTO.getPage(), searchDTO.getSize(), (journalTitlesList.size() / searchDTO.getSize()),
            journalTitlesList.size());
    return new RestCollection<>(journalTitlesList, page);
  }

  @SuppressWarnings("java:S2259")
  // transformKeyTitleFromISSN() cannot throw an NPE because the value is checked by the method StringTool.isNullOrEmpty()
  public RestCollection<JournalTitleDTO> getJournalTitlesByISSN(String issn) {
    String url = this.remoteServiceUrl + "/" + NOTICE + "/" + issn + "?natifjson=true";
    this.refreshToken();
    // create headers
    HttpHeaders headers = new HttpHeaders();
    RestTemplate client = this.buildRestClientWithParams(url, headers);
    // build the request
    HttpEntity<String> request = new HttpEntity<>(headers);
    List<JournalTitleDTO> journalTitlesList = new ArrayList<>();

    try {
      // make an HTTP GET request with headers
      ResponseEntity<String> response = client.exchange(url, HttpMethod.GET, request, String.class);
      JournalTitleDTO journalTitle = new JournalTitleDTO();
      journalTitle.setIssn(issn);
      // check response
      if (response.getStatusCode() == HttpStatus.OK) {
        final ObjectMapper mapper = new ObjectMapper();
        JsonNode neoJsonNode = mapper.readTree(response.getBody());
        JsonNode graphNodes = neoJsonNode.get("fields");
        this.extractFromHttpResponse(journalTitle, graphNodes);
      }
      journalTitlesList.add(journalTitle);
    } catch (JsonProcessingException e) {
      log.error("Error can't parse json response for the search by ISSN: {}", issn, e);
    } catch (RestClientException ex) { // added this to catch error from calling the external service
      log.error("Error with external research service for the ISSN {}", issn, ex);
    }
    // as the exepected result will be 1 or 0 at last, page and pageZie and totalPages are fixed to 1
    RestCollectionPage page = new RestCollectionPage(1, 1, 1, journalTitlesList.size());
    return new RestCollection<>(journalTitlesList, page);
  }

  private RestTemplate buildRestClientWithParams(String url, HttpHeaders headers) {
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate client = restTemplateBuilder.rootUri(url).build();
    // create headers
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
    //add JWT token to the headers
    headers.add("Authorization", "JWT " + this.OAuth2Token);
    // build the request
    return client;
  }

  private void refreshToken() {
    if (StringTool.isNullOrEmpty(this.OAuth2Token)) {
      this.getOAuth2TokenFromExternalService();
    }
  }

  private void extractFromHttpResponse(JournalTitleDTO journalTitle, JsonNode graphNodes) {
    if (graphNodes instanceof ArrayNode) {
      for (JsonNode element : graphNodes) {
        String title = this.checkValueFromFieldAndSubfield(element, "222", "a");
        if (!StringTool.isNullOrEmpty(title)) {
          journalTitle.setMainTitle(this.cleanTitle(title));
        }

        String urlStr = this.checkValueFromFieldAndSubfield(element, "856", "u");
        if (!StringTool.isNullOrEmpty(urlStr)) {
          journalTitle.setUrl(urlStr);
        }

        String countryCode = this.checkValueFromFieldAndSubfield(element, "044", "c");
        if (!StringTool.isNullOrEmpty(countryCode)) {
          journalTitle.setCountryCode(countryCode);
        }

        String keyTitle = this.checkValueFromFieldAndSubfield(element, "222", "a");
        if (!StringTool.isNullOrEmpty(keyTitle)) {
          journalTitle.setKeytitle(keyTitle);
        }

        String publisher = this.checkValueFromFieldAndSubfield(element, "264", "b");
        if (!StringTool.isNullOrEmpty(publisher)) {
          journalTitle.setPublisher(publisher);
        }

        String issnl = this.checkValueFromFieldAndSubfield(element, "022", "l");
        if (!StringTool.isNullOrEmpty(issnl)) {
          journalTitle.setIssnl(issnl);
          if (journalTitle.getIssn() == null) { // set issn when searching by title
            journalTitle.setIssn(issnl);
          }
        }
      }
    }
  }

  private String checkValueFromFieldAndSubfield(JsonNode element, String field, String subfield) {
    if (element.get(field) != null && element.get(field).get("subfields") != null && element.get(field).get("subfields").isArray()) {
      for (JsonNode properKeyNode : element.get(field).get("subfields")) {
        if (properKeyNode.get(subfield) != null) {
          return properKeyNode.get(subfield).asText();
        }
      }
    }
    return null;
  }

  private String cleanTitle(String value) {
    value = CleanTool.removeTrailingDots(value);
    value = CleanTool.removeTrailingParenthesisAndContent(value);
    value = CleanTool.removeInvisibleCharacters(value);
    value = CleanTool.removeTrailingColon(value);
    value = CleanTool.cleanString(value);
    return value;
  }

}
