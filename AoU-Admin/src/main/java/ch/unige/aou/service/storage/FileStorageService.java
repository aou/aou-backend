/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - FileStorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.storage;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationThumbnail;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.service.MetadataService;

@Profile("storage-file")
@Service
@ConditionalOnBean(AdminController.class)
public class FileStorageService extends StorageService {

  private final Path storagePath;
  private final DocumentFileTypeService documentFileTypeService;
  private final DocumentFileService documentFileService;
  private final LicenseService licenseService;

  public FileStorageService(AouProperties aouProperties, MessageService messageService, PublicationService publicationService,
          UserService userService, MetadataService metadataService, DocumentFileTypeService documentFileTypeService,
          LicenseService licenseService, DocumentFileService documentFileService) {
    super(messageService, publicationService, userService, metadataService);

    // Check configuration
    this.storagePath = Paths.get(URI.create(aouProperties.getStorage().getUrl()));
    if (!FileTool.checkFile(this.storagePath)) {
      throw new SolidifyRuntimeException("Storage location (" + this.storagePath + ") does not exist");
    }

    this.documentFileTypeService = documentFileTypeService;
    this.licenseService = licenseService;
    this.documentFileService = documentFileService;
  }

  @Override
  public void updateExportMetadata(Publication publication) {
    // Do nothing in this implementation
  }

  @Override
  public void generateExportMetadata(Publication publication) {
    // Do nothing in this implementation
  }

  @Override
  public String getNextArchiveId(Publication publication) {
    Path publicationFolder = this.storagePath.resolve(publication.getResId());
    return publicationFolder.toUri().toString();
  }

  @Override
  public void storePublication(Publication publication) throws IOException {
    // Create publication folder
    Path publicationFolder = this.storagePath.resolve(publication.getResId());
    if (!FileTool.checkFile(publicationFolder) && !FileTool.ensureFolderExists(publicationFolder)) {
      throw new SolidifyProcessingException("Cannot create publication folder " + publication.toString());
    }
    // Save location
    publication.setArchiveId(this.getNextArchiveId(publication));
    // Save metadata
    try (FileOutputStream aouMetadata = new FileOutputStream(
            publicationFolder.resolve(AouConstants.AOU + SolidifyConstants.XML_EXT).toFile())) {
      aouMetadata.write(publication.getMetadata().getBytes());
    }
    // Save attachments
    for (DocumentFile attachment : publication.getDocumentFiles()) {
      if (!FileTool.copyFile(Paths.get(attachment.getFinalData()), publicationFolder.resolve(attachment.getFileName()))) {
        throw new SolidifyProcessingException("Cannot save publication attachment " + attachment.getFinalData().toString());
      }
    }
  }

  @Override
  public void revertDocumentFilesAndThumbnailFromStorageInfo(Publication publication) {
    Path publicationPath = this.publicationService.getPublicationFilesFolderPath(publication.getResId());

    List<MetadataFile> metadataFileList = this.publicationService.getMetadataFiles(publication, publication.getMetadataBackup());
    int index = 10;
    for (MetadataFile metadataFile : metadataFileList) {
      if (!metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)) {
        if (publication.getDocumentFiles().stream().noneMatch(df -> df.getFileName().equals(metadataFile.getName()) &&
                BigInteger.valueOf(df.getFileSize()).compareTo(metadataFile.getSize()) == 0)) {
          DocumentFile documentFile = new DocumentFile();
          documentFile.setPublication(publication);

          documentFile.setFileName(metadataFile.getName());
          documentFile.setFileSize(metadataFile.getSize().longValue());
          documentFile.setLabel(metadataFile.getLabel());
          documentFile.setMimetype(metadataFile.getMimeType());
          documentFile.setSortValue(index);
          documentFile.setDocumentFileType(this.documentFileTypeService.convertFileTypeToDocumentFileType(metadataFile.getType()));
          documentFile.setAccessLevel(DocumentFile.AccessLevel.fromMetadataValue(metadataFile.getAccessLevel()));
          License licence = this.licenseService.getLicenseFromType(metadataFile.getLicense());
          documentFile.setLicense(licence);

          if (metadataFile.getEmbargo() != null) {
            DocumentFile.AccessLevel embargoAccessLevel = DocumentFile.AccessLevel.fromMetadataValue(metadataFile.getEmbargo().getAccessLevel());
            documentFile.setEmbargoAccessLevel(embargoAccessLevel);
            documentFile.setEmbargoEndDate(metadataFile.getEmbargo().getEndDate());
          }

          try {
            Path publicationStorageFolder = this.storagePath.resolve(publication.getResId());
            if (!FileTool.copyFile(publicationStorageFolder.resolve(metadataFile.getName()), publicationPath.resolve(metadataFile.getName()))) {
              throw new SolidifyProcessingException("Cannot save publication document file " + documentFile.getFileName());
            }
          } catch (IOException e) {
            throw new SolidifyRuntimeException("Unable to read thumbnail for object" + publication.getArchiveId());
          }

          documentFile.setSourceData(publicationPath.resolve(documentFile.getFileName()).toUri());
          documentFile.getCreation().setWhen(publication.getCreation().getWhen());
          this.documentFileService.save(documentFile);

        } else {
          // Check if the order of the document file is correct as well as the license, accesslevel, embargo and documentFileType has not
          // changed.
          Optional<DocumentFile> documentFileOpt = publication.getDocumentFiles().stream()
                  .filter(df -> BigInteger.valueOf(df.getFileSize()).compareTo(metadataFile.getSize()) == 0 && df.getFileName()
                          .equals(metadataFile.getName()))
                  .findFirst();
          if (documentFileOpt.isPresent()) {
            DocumentFile documentFile = this.setDocumentFilesProperties(documentFileOpt.get(), metadataFile, index);
            this.documentFileService.save(documentFile);
          }
        }
      } else {
        // Verify in case of thumbnail
        if (publication.getThumbnail() == null || !publication.getThumbnail().getFileName().equals(metadataFile.getName()) ||
                BigInteger.valueOf(publication.getThumbnail().getFileSize()).compareTo(metadataFile.getSize()) != 0) {
          PublicationThumbnail thumbnail = new PublicationThumbnail();
          thumbnail.setFileName(metadataFile.getName());
          thumbnail.setFileSize(metadataFile.getSize().longValue());
          thumbnail.setMimeType(metadataFile.getMimeType());

          Path fileThumbnail = publicationPath.resolve(Objects.requireNonNull(metadataFile.getName()));
          try {
            thumbnail.setFileContent(Files.readAllBytes(fileThumbnail));
          } catch (IOException e) {
            throw new SolidifyRuntimeException("Unable to read thumbnail for object" + publication.getArchiveId());
          }
          publication.setThumbnail(thumbnail);
        }
      }
      index = index + 10;
    }
    // Clean document files if needed
    if (!publication.getDocumentFiles().isEmpty()) {
      for (Iterator<DocumentFile> iterator = publication.getDocumentFiles().iterator(); iterator.hasNext(); ) {
        DocumentFile df = iterator.next();
        if (metadataFileList.stream().noneMatch(mf -> df.getFileName().equals(mf.getName()) &&
                BigInteger.valueOf(df.getFileSize()).compareTo(mf.getSize()) == 0)) {
          this.documentFileService.delete(df.getResId());
          iterator.remove();
        }
      }
    }

    // Clean thumbnail if needed
    if (publication.getThumbnail() != null && metadataFileList.stream()
            .noneMatch(mf -> mf.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE))) {
      publication.setThumbnail(null);
    }
  }

  private DocumentFile setDocumentFilesProperties(DocumentFile documentFile, MetadataFile metadataFile, int sortValue) {
    if (documentFile.getSortValue() != sortValue) {
      documentFile.setSortValue(sortValue);
    }
    DocumentFileType documentFileType = this.documentFileTypeService.convertFileTypeToDocumentFileType(metadataFile.getType());
    if (documentFile.getDocumentFileType() != documentFileType) {
      documentFile.setDocumentFileType(documentFileType);
    }

    DocumentFile.AccessLevel accessLevel = DocumentFile.AccessLevel.fromMetadataValue(metadataFile.getAccessLevel());
    if (documentFile.getAccessLevel() != accessLevel) {
      documentFile.setAccessLevel(accessLevel);
    }

    if (documentFile.getLabel() == null || !documentFile.getLabel().equals(metadataFile.getLabel())) {
      documentFile.setLabel(metadataFile.getLabel());
    }

    License licence = this.licenseService.getLicenseFromType(metadataFile.getLicense());
    if (documentFile.getLicense() != licence) {
      documentFile.setLicense(licence);
    }

    if (metadataFile.getEmbargo() != null || documentFile.getEmbargoAccessLevel() != null) {
      if (metadataFile.getEmbargo() == null) {
        documentFile.setEmbargoAccessLevel(null);
        documentFile.setEmbargoEndDate(null);

      } else {
        DocumentFile.AccessLevel embargoAccessLevel = DocumentFile.AccessLevel.fromMetadataValue(metadataFile.getEmbargo().getAccessLevel());
        if (documentFile.getEmbargoAccessLevel() != embargoAccessLevel) {
          documentFile.setEmbargoAccessLevel(embargoAccessLevel);
        }
        if (documentFile.getEmbargoEndDate() != metadataFile.getEmbargo().getEndDate()) {
          documentFile.setEmbargoEndDate(metadataFile.getEmbargo().getEndDate());
        }
      }
    }

    return documentFile;
  }

}
