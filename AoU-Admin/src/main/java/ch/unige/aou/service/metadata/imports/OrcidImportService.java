/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - OrcidImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.service.metadata.imports;

import java.math.BigInteger;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import jakarta.xml.bind.JAXBElement;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.xml.orcid.v3_0.common.ExternalId;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Contributor;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Work;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OrcidService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class OrcidImportService extends MetadataImportService {

  private static final Logger log = LoggerFactory.getLogger(OrcidImportService.class);
  private final OrcidService orcidService;

  public OrcidImportService(AouProperties aouProperties, MessageService messageService,
          MetadataService metadataService, ContributorService contributorService,
          JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService, OrcidService orcidService) {
    super(aouProperties, messageService, metadataService, contributorService, journalTitleRemoteService, licenseService);
    this.orcidService = orcidService;
  }

  public DepositDoc searchOrcidWork(BigInteger putCode, Person person) {
    try {
      Work work = this.orcidService.getWork(putCode, person.getOrcidToken());
      DepositDoc depositDoc = new DepositDoc();
      return this.fillDepositDoc(depositDoc, work);
    } catch (Exception e) {
      throw new SolidifyRuntimeException("An error occurred when searching Work '" + putCode + "' on ORCID", e);
    }
  }

  public DepositDoc fillDepositDoc(DepositDoc depositDoc, Work work) {
    this.fillTitle(depositDoc, work);
    this.fillTypeAndSubtype(depositDoc, work);
    this.fillContributors(depositDoc, work);
    this.fillDates(depositDoc, work);
    this.fillLanguage(depositDoc, work);
    this.fillAbstract(depositDoc, work);
    this.fillContainerTitle(depositDoc, work);
    this.fillCommercialUrl(depositDoc, work);
    this.fillIdentifiers(depositDoc, work);
    return depositDoc;
  }

  private void fillIdentifiers(DepositDoc depositDoc, Work work) {
    if (work.getExternalIds() != null) {
      for (ExternalId externalId : work.getExternalIds().getExternalId()) {
        String type = externalId.getExternalIdType();
        String relationship = externalId.getExternalIdRelationship();
        String value = externalId.getExternalIdValue();
        if ("self".equals(relationship) && !StringTool.isNullOrEmpty(type)) {
          if (depositDoc.getIdentifiers() == null) {
            depositDoc.setIdentifiers(new DepositIdentifiers());
          }
          switch (type) {
            case "doi" -> depositDoc.getIdentifiers().setDoi(value);
            case "isbn" -> depositDoc.getIdentifiers().setIsbn(value);
            case "issn" -> depositDoc.getIdentifiers().setIssn(value);
            case "arxiv" -> depositDoc.getIdentifiers().setArxiv(value);
            case "mmsid" -> depositDoc.getIdentifiers().setMmsid(value);
            case "repec" -> depositDoc.getIdentifiers().setRepec(value);
            case "dblp" -> depositDoc.getIdentifiers().setDblp(value);
            case "urn" -> depositDoc.getIdentifiers().setUrn(value);
            case "pmcid" -> depositDoc.getIdentifiers().setPmcid(value);
          }
          // particular case of pmid that is a number
          if ("pmid".equals(type)) {
            try {
              depositDoc.getIdentifiers().setPmid(BigInteger.valueOf(Long.valueOf(value)));
            } catch (Exception e) {
              log.warn("Unable to parse PMID {}", value);
            }
          }
        }
      }
    }
  }

  private void fillContributors(DepositDoc depositDoc, Work work) {

    for (Contributor contributor : work.getContributors().getContributor()) {
      ch.unige.aou.model.xml.deposit.v2_4.Contributor aouContributor = new ch.unige.aou.model.xml.deposit.v2_4.Contributor();

      // ORCID
      this.fillContributorOrcid(contributor, aouContributor);

      // Role
      if (contributor.getContributorAttributes() != null
              && "co-investigator".equals(contributor.getContributorAttributes().getContributorRole())) {
        aouContributor.setRole(AuthorRole.COLLABORATOR);
      } else {
        // Note: do not check value in metadata as in common-3.0.xsd we can read: "support for original roles will be removed in future versions of the API"
        aouContributor.setRole(AuthorRole.AUTHOR);
      }

      // Name
      this.fillContributorName(contributor, aouContributor);

      // Email
      if (contributor.getContributorEmail() != null) {
        aouContributor.setEmail(contributor.getContributorEmail().getValue());
      }

      // Add contributor to the list
      this.ensureContributorsExist(depositDoc);
      depositDoc.getContributors().getContributorOrCollaboration().add(aouContributor);
    }
  }

  private void fillContributorOrcid(Contributor contributor, ch.unige.aou.model.xml.deposit.v2_4.Contributor aouContributor) {
    if (contributor.getContributorOrcid() != null) {
      Optional<String> orcidOpt = contributor.getContributorOrcid().getContent().stream()
              .filter(elem -> elem.getName().getLocalPart().equals("path")).map(JAXBElement::getValue).findFirst();
      if (orcidOpt.isPresent()) {
        aouContributor.setOrcid(orcidOpt.get());
      }
    }
  }

  private void fillContributorName(Contributor contributor, ch.unige.aou.model.xml.deposit.v2_4.Contributor aouContributor) {
    String contributorName = contributor.getCreditName() != null ? contributor.getCreditName().getValue() : null;
    if (!StringTool.isNullOrEmpty(contributorName)) {
      // Split contributor on space. It may give bad result, but what else ?
      if (contributorName.contains(" ")) {
        aouContributor.setLastname(contributorName.substring(contributorName.lastIndexOf(" ")).trim());
        aouContributor.setFirstname(contributorName.substring(0, contributorName.lastIndexOf(" ")).trim());
      } else {
        aouContributor.setLastname(contributorName);
      }
    }
  }

  private void fillTitle(DepositDoc depositDoc, Work work) {
    // Title
    depositDoc.setTitle(this.getText(work.getTitle().getTitle()));

    // Original title
    if (work.getTitle().getTranslatedTitle() != null) {
      String originalTitle = work.getTitle().getTranslatedTitle().getValue();
      String originalTitleLang = null;
      if (!StringTool.isNullOrEmpty(work.getTitle().getTranslatedTitle().getLanguageCode())) {
        originalTitleLang = CleanTool.getLanguageCode(work.getTitle().getTranslatedTitle().getLanguageCode());
      }
      depositDoc.setOriginalTitle(this.getTextLang(originalTitle, originalTitleLang));
    }
  }

  private void fillTypeAndSubtype(DepositDoc depositDoc, Work work) {
    depositDoc.setSubsubtype(this.getSubSubType(work));
    depositDoc.setSubtype(this.getSubType(work));
    depositDoc.setType(this.getType(work));
  }

  private void fillDates(DepositDoc depositDoc, Work work) {
    if (work.getPublicationDate() != null) {
      String year = null;

      String month = null;
      String day = null;
      if (work.getPublicationDate().getYear() != null) {
        year = String.valueOf(work.getPublicationDate().getYear().getValue());
      }
      if (work.getPublicationDate().getMonth() != null) {
        month = String.valueOf(work.getPublicationDate().getMonth().getValue());
      }
      if (work.getPublicationDate().getDay() != null) {
        day = String.valueOf(work.getPublicationDate().getDay().getValue());
      }
      this.ensureDatesExist(depositDoc);
      DateTypes dateType = DateTypes.PUBLICATION;
      if (AouConstants.DEPOSIT_SUBTYPE_THESE_NAME.equals(depositDoc.getSubtype())) {
        dateType = DateTypes.DEFENSE;
      }
      DateWithType publicationDate = this.getDateWithType(dateType, this.getDateString(year, month, day));
      depositDoc.getDates().getDate().add(publicationDate);
    }
  }

  private void fillAbstract(DepositDoc depositDoc, Work work) {
    if (!StringTool.isNullOrEmpty(work.getShortDescription())) {
      this.ensureAbstractsExist(depositDoc);
      depositDoc.getAbstracts().getAbstract().add(this.getText(work.getShortDescription()));
    }
  }

  private void fillContainerTitle(DepositDoc depositDoc, Work work) {
    if (!StringTool.isNullOrEmpty(work.getJournalTitle())) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setTitle(work.getJournalTitle());
    }
  }

  private void fillCommercialUrl(DepositDoc depositDoc, Work work) {
    if (work.getUrl() != null) {
      depositDoc.setPublisherVersionUrl(work.getUrl().getValue());
    }
  }

  private void fillLanguage(DepositDoc depositDoc, Work work) {
    if (!StringTool.isNullOrEmpty(work.getLanguageCode())) {
      this.ensureLanguagesExist(depositDoc);
      String languageCode = CleanTool.getLanguageCode(work.getLanguageCode());
      depositDoc.getLanguages().getLanguage().add(languageCode);
    }
  }

  private String getSubSubType(Work work) {
    switch (work.getType()) {
      case "book-review":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME;
      case "review":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME;
      case "supervised-student-publication":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME;
      default:
        return null;
    }
  }

  private String getSubType(Work work) {
    switch (work.getType()) {
      case "journal-article", "magazine-article", "book-review", "review", "supervised-student-publication":
        return AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME;
      case "newspaper-article", "newsletter-article":
        return AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME;
      case "journal-issue":
        return AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME;
      case "book", "manual":
        return AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME;
      case "edited-book":
        return AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME;
      case "book-chapter":
        return AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME;
      case "encyclopedia-entry", "dictionary-entry":
        return AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME;
      case "lecture-speech", "conference-abstract":
        return AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME;
      case "conference-poster":
        return AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME;
      case "dissertation-thesis":
        return AouConstants.DEPOSIT_SUBTYPE_THESE_NAME;
      case "report":
        return AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME;
      case "working-paper":
        return AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME;
      case "preprint":
      default:
        return AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME;
    }
  }

  private String getType(Work work) {
    switch (work.getType()) {
      case "journal-article", "magazine-article", "newspaper-article", "book-review", "review", "newsletter-article", "supervised-student-publication":
        return AouConstants.DEPOSIT_TYPE_ARTICLE_NAME;
      case "journal-issue":
        return AouConstants.DEPOSIT_TYPE_JOURNAL_NAME;
      case "book", "edited-book", "book-chapter", "encyclopedia-entry", "manual", "dictionary-entry":
        return AouConstants.DEPOSIT_TYPE_LIVRE_NAME;
      case "lecture-speech", "conference-poster", "conference-abstract":
        return AouConstants.DEPOSIT_TYPE_CONFERENCE_NAME;
      case "dissertation-thesis":
        return AouConstants.DEPOSIT_TYPE_DIPLOME_NAME;
      case "report", "working-paper", "preprint":
      default:
        return AouConstants.DEPOSIT_TYPE_RAPPORT_NAME;
    }
  }
}
