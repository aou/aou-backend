/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - FedoraImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.SwissNationalLibraryUploadService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.PublicationFromFedoraMessage;
import ch.unige.aou.message.PublicationIndexingMessage;
import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.fedora.AdminDatastreamDto;
import ch.unige.aou.model.fedora.AttachmentDto;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.schedule.SwissNationalLibraryUpload;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.xml.fedora.foxml.v1.DatastreamType;
import ch.unige.aou.model.xml.fedora.foxml.v1.DatastreamVersionType;
import ch.unige.aou.model.xml.fedora.foxml.v1.DigitalObject;
import ch.unige.aou.model.xml.fedora.foxml.v1.PropertyType;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.storage.Fedora3Storage;

@Profile("storage-fedora3")
@Service
@ConditionalOnBean(AdminController.class)
public class FedoraImportService extends MetadataAndFileImportService {

  private static final Logger log = LoggerFactory.getLogger(FedoraImportService.class);

  private static final String ATTACHMENT = "ATTACHMENT";
  private static final String THESIS = "THESIS";
  private static final String FULLTEXT = "FULLTEXT";
  private static final String MARCXML_DATASTREAM_NAME = "MARCXML";
  private static final String ADMIN_DATASTREAM_NAME = "ADMIN";
  private static final List<String> HOMEORG_SUFFIXES = List.of("unige.ch", "hcuge.ch", "graduateinstitute.ch");

  private final Fedora3Storage fedora3Storage;
  private final PublicationService publicationService;
  private final PersonService personService;
  private final UserService userService;
  private final SwissNationalLibraryUploadService swissNationalLibraryUploadService;

  private final HistoryService historyService;

  private String marc2pivotXsl;
  private Transformer transformer;

  public FedoraImportService(AouProperties aouProperties, MessageService messageService,
          MetadataService metadataService, ContributorService contributorService,
          DocumentFileService documentFileService, DocumentFileTypeService documentFileTypeService,
          JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService, Fedora3Storage fedora3Storage,
          PublicationService publicationService, PersonService personService, UserService userService,
          SwissNationalLibraryUploadService swissNationalLibraryUploadService, HistoryService historyService) {
    super(aouProperties, messageService, metadataService, contributorService, documentFileService, documentFileTypeService,
            journalTitleRemoteService, licenseService);

    this.fedora3Storage = fedora3Storage;
    this.publicationService = publicationService;
    this.personService = personService;
    this.userService = userService;
    this.swissNationalLibraryUploadService = swissNationalLibraryUploadService;
    this.historyService = historyService;

    try {
      this.loadXsl();

      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      transformerFactory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
      transformerFactory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
      transformerFactory.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);
      this.transformer = transformerFactory.newTransformer();
      this.transformer.setOutputProperty("omit-xml-declaration", "yes");
      this.transformer.setOutputProperty("indent", "no");

    } catch (IOException | TransformerConfigurationException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  private void loadXsl() throws IOException {

    //load xsl for converting from MARCXML to pivot format
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    final org.springframework.core.io.Resource xsl = resolver.getResources("classpath*:/" + AouConstants.XSL_HOME + "/marc2pivot.xsl")[0];
    String fileName = xsl.getFilename();
    if (fileName == null) {
      throw new SolidifyRuntimeException("Resource " + xsl.getURI() + " has no filename");
    }
    log.info("Finding XML transformation: {}", fileName);
    this.marc2pivotXsl = StreamUtils.copyToString(xsl.getInputStream(), Charset.defaultCharset());
  }

  @Transactional
  public Optional<Person> getExistingCreator(PublicationFromFedoraMessage publicationsFromFedoraMessage, AdminDatastreamDto adminDS) {
    if (!StringTool.isNullOrEmpty(adminDS.getSubmissionCn())) {
      try {
        // user already exists in database
        User creatorUser = this.userService.findByExternalUid(adminDS.getSubmissionCn());
        return Optional.of(creatorUser.getPerson());
      } catch (NoSuchElementException e) {
        return Optional.empty();
      }
    } else {
      return this.personService.findById(publicationsFromFedoraMessage.getCreatorResId());
    }
  }

  @Transactional
  public synchronized Person getOrCreateCreatorPerson(PublicationFromFedoraMessage publicationsFromFedoraMessage, AdminDatastreamDto adminDS) {

    if (!StringTool.isNullOrEmpty(adminDS.getSubmissionCn())) {
      String submissionCn = adminDS.getSubmissionCn();
      try {
        // user already exists in database
        User creatorUser = this.userService.findByExternalUid(submissionCn);
        return creatorUser.getPerson();
      } catch (NoSuchElementException e) {
        if (!StringTool.isNullOrEmpty(adminDS.getSubmissionFirstname())
                && !StringTool.isNullOrEmpty(adminDS.getSubmissionLastName())
                && !StringTool.isNullOrEmpty(adminDS.getSubmissionEmail())) {
          // User has never logged in in new Deposit portal, but we can create him
          Person newPerson = new Person();
          newPerson.setLastName(adminDS.getSubmissionLastName());
          newPerson.setFirstName(adminDS.getSubmissionFirstname());
          Person savedPerson = this.personService.save(newPerson);

          User newUser = new User();
          newUser.setPerson(savedPerson);
          newUser.setLastName(adminDS.getSubmissionLastName());
          newUser.setFirstName(adminDS.getSubmissionFirstname());
          newUser.setExternalUid(submissionCn);
          newUser.setEmail(adminDS.getSubmissionEmail());

          for (String homeorgSuffix : HOMEORG_SUFFIXES) {
            if (submissionCn.endsWith(homeorgSuffix)) {
              newUser.setHomeOrganization(homeorgSuffix);
            }
          }

          User savedUser = this.userService.save(newUser);
          return savedUser.getPerson();
        }
      }
    }

    // not enough info the store the historical depositor --> use the id of the person who launched the import process
    Optional<Person> creatorOpt = this.personService.findById(publicationsFromFedoraMessage.getCreatorResId());
    if (creatorOpt.isPresent()) {
      return creatorOpt.get();
    } else {
      String errorMessage = String.format("Unable to find any creator Person for pid '%s' with creator id '%s'",
              publicationsFromFedoraMessage.getPid(), publicationsFromFedoraMessage.getCreatorResId());
      log.error(errorMessage);
      throw new SolidifyRuntimeException(errorMessage);
    }
  }

  public Contributor getExistingContributorByCnIndividu(ContributorDTO contributorDTO) {
    return this.contributorService.findByCnIndividu(contributorDTO.getCnIndividu());
  }

  public Contributor getExistingContributorByFirstNameAndLastNameAndCnIndividuIsNull(ContributorDTO contributorDTO) {
    return this.contributorService.findByFirstNameAndLastNameAndCnIndividuIsNull(contributorDTO.getFirstName(), contributorDTO.getLastName());
  }

  @Transactional
  public synchronized Contributor getOrCreateContributor(ContributorDTO contributorDTO) {
    return this.contributorService.findOrCreate(contributorDTO);
  }

  @Transactional
  public Publication importObjectFromFedora(DigitalObject foxml, String xmlMetadata, Person creator, AdminDatastreamDto adminDS) {
    String pid = foxml.getPID();

    Publication publication = this.publicationService.getPublicationFromXmlMetadata(xmlMetadata, creator, Publication.ImportSource.FEDORA3,
            pid, Publication.PublicationStatus.COMPLETED);

    // Publication must saved here to be then linked with document files below
    publication = this.publicationService.save(publication);
    log.debug("publication saved for object '{}'", pid);

    // import document files
    this.getAndCreateDocumentFiles(foxml, publication, adminDS);
    log.debug("DocumentFiles created for object '{}'", pid);

    // Save Publication again allows to update its creation date and if thumbnail has been found in document files it will be saved as well
    OffsetDateTime creationDate = this.getCreationDate(foxml, adminDS);
    log.debug("creationDate found for object '{}'", pid);
    publication.getCreation().setWhen(creationDate);
    this.publicationService.save(publication);
    log.debug("publication saved again for object '{}'", pid);

    for (DocumentFile documentFile : publication.getDocumentFiles()) {
      this.storeFulltextInCache(foxml, documentFile);
    }

    return publication;
  }

  public StatusHistory updateFirstCompletedDateInStatusHistory(Publication publication) {
    List<StatusHistory> statusHistoryList = this.historyService.findStatusHistoryOrderByLastDate(publication.getResId());
    for (StatusHistory statusHistory : statusHistoryList) {
      if (statusHistory.getStatus().equals("COMPLETED")) {
        StatusHistory savedStatusHistory = this.historyService.updateChangeTime(statusHistory, publication.getCreationTime());
        SolidifyEventPublisher.getPublisher().publishEvent(new PublicationIndexingMessage(publication.getResId()));
        return savedStatusHistory;
      }
    }
    return null;
  }

  public AdminDatastreamDto getAdminDatastreamDto(DigitalObject foxml) {
    try {
      String adminDsXml = this.getLastVersionOfXmlDatastream(foxml, ADMIN_DATASTREAM_NAME);
      Document adminDsDoc = XMLTool.parseXML(adminDsXml);

      AdminDatastreamDto adminDS = new AdminDatastreamDto();

      adminDS.setSubmissionDate(XMLTool.getFirstTextContent(adminDsDoc, "./admin/deposit/SubmissionDate"));
      adminDS.setValidationDate(XMLTool.getFirstTextContent(adminDsDoc, "./admin/deposit/ValidationDate"));
      adminDS.setCreationDate(XMLTool.getFirstTextContent(adminDsDoc, "./admin/deposit/CreationDate"));

      adminDS.setSubmissionCn(XMLTool.getFirstTextContent(adminDsDoc, "./admin/deposit/SubmissionCN"));
      adminDS.setSubmissionFirstname(XMLTool.getFirstTextContent(adminDsDoc, "./admin/deposit/SubmissionFirstname"));
      adminDS.setSubmissionLastName(XMLTool.getFirstTextContent(adminDsDoc, "./admin/deposit/SubmissionName"));
      adminDS.setSubmissionEmail(XMLTool.getFirstTextContent(adminDsDoc, "./admin/deposit/SubmissionEmail"));

      NodeList attachmentsList = (NodeList) XMLTool.buildXPath().compile("/admin/deposit/Attachment")
              .evaluate(adminDsDoc, XPathConstants.NODESET);

      for (int i = 0; i < attachmentsList.getLength(); i++) {
        Node attachmentNode = attachmentsList.item(i);

        AttachmentDto attachmentDto = new AttachmentDto();

        attachmentDto.setDatastreamId(XMLTool.getFirstTextContent(attachmentNode, "./Datastream_ID"));
        attachmentDto.setFileName(XMLTool.getFirstTextContent(attachmentNode, "./FileName"));
        String sizeStr = XMLTool.getFirstTextContent(attachmentNode, "./Size");
        if (!StringTool.isNullOrEmpty(sizeStr)) {
          attachmentDto.setSize(Long.parseLong(sizeStr));
        }
        attachmentDto.setAvailability(XMLTool.getFirstTextContent(attachmentNode, "./Availability"));
        attachmentDto.setDescription(XMLTool.getFirstTextContent(attachmentNode, "./Description"));
        attachmentDto.setDescriptionComplement(XMLTool.getFirstTextContent(attachmentNode, "./DescriptionComplement"));

        String license = XMLTool.getFirstTextContent(attachmentNode, "./License");
        if (StringTool.isNullOrEmpty(license)) {
          license = XMLTool.getFirstTextContent(attachmentNode, "./Licence");
        }
        attachmentDto.setLicense(license);

        attachmentDto.setEmbargoAvailability(XMLTool.getFirstTextContent(attachmentNode, "./Embargo"));
        attachmentDto.setEmbargoEndDate(XMLTool.getFirstTextContent(attachmentNode, "./EmbargoEndDate"));

        String periodStr = XMLTool.getFirstTextContent(attachmentNode, "./Period");
        if (!StringTool.isNullOrEmpty(periodStr)) {
          attachmentDto.setEmbargoPeriod(Integer.parseInt(periodStr));
        }

        attachmentDto.setLastSyncWithSwissNationalLibrarySha1(
                XMLTool.getFirstTextContent(attachmentNode, "./last_sync_with_swiss_national_library_sha1"));
        String lastSyncDate = XMLTool.getFirstTextContent(attachmentNode, "./last_sync_with_swiss_national_library_date");
        if (!StringTool.isNullOrEmpty(lastSyncDate)) {
          attachmentDto.setLastSyncWithSwissNationalLibraryDate(this.parseDate(lastSyncDate));
        }

        adminDS.getAttachments().add(attachmentDto);
      }

      return adminDS;

    } catch (XPathExpressionException | ParserConfigurationException | IOException | SAXException e) {
      throw new SolidifyRuntimeException("Unable to get ADMIN datastream content for object '" + foxml.getPID() + "'", e);
    }
  }

  public DigitalObject getFoxml(String fedoraId) {
    return this.fedora3Storage.getFoxml(fedoraId);
  }

  @SuppressWarnings("java:S2259") // description can never be null
  public void getAndCreateDocumentFiles(DigitalObject foxml, Publication publication, AdminDatastreamDto adminDS) {

    String fedoraId = foxml.getPID();

    List<DatastreamType> datastreams = foxml.getDatastream();
    List<DatastreamType> fileDatastreams = datastreams.stream().filter(ds -> {
      for (String prefix : Fedora3Storage.FILE_DATASTREAM_PREFIXES) {
        if (ds.getID().startsWith(prefix)) {
          return true;
        }
      }
      return false;
    }).collect(Collectors.toList());
    log.debug("fileDatastreams found for object '{}'", fedoraId);

    int sortValue = 10;
    for (DatastreamType fileDS : fileDatastreams) {

      String datastreamId = fileDS.getID();
      DatastreamVersionType lastVersionOfFileDatastream = this.fedora3Storage.getLastVersionOfDatastream(foxml, datastreamId);
      log.debug("DatastreamProfile found for object '{}'->'{}'", fedoraId, datastreamId);

      String mimetype = lastVersionOfFileDatastream.getMIMETYPE();
      long size = lastVersionOfFileDatastream.getSIZE();

      String fileName = null;
      String description = null;
      String descriptionComplement = null;
      String license = null;
      String availability = null;
      String embargoAvailability = null;
      Integer embargoPeriod = null;
      String embargoEndDate = null;
      String lastSyncWithSwissNationalLibrarySha1 = null;
      OffsetDateTime lastSyncWithSwissNationalLibraryDate = null;

      // Try to get informations from ADMIN datastream first
      Optional<AttachmentDto> attachmentDtoOpt = adminDS.getAttachments().stream()
              .filter(attachment -> attachment.getDatastreamId().equals(fileDS.getID())).findFirst();
      if (attachmentDtoOpt.isPresent()) {
        AttachmentDto attachmentDto = attachmentDtoOpt.get();

        description = attachmentDto.getDescription();
        descriptionComplement = attachmentDto.getDescriptionComplement();
        fileName = attachmentDto.getFileName();
        license = attachmentDto.getLicense();
        availability = attachmentDto.getAvailability();
        embargoAvailability = attachmentDto.getEmbargoAvailability();
        embargoPeriod = attachmentDto.getEmbargoPeriod();
        embargoEndDate = attachmentDto.getEmbargoEndDate();
        lastSyncWithSwissNationalLibrarySha1 = attachmentDto.getLastSyncWithSwissNationalLibrarySha1();
        lastSyncWithSwissNationalLibraryDate = attachmentDto.getLastSyncWithSwissNationalLibraryDate();
      }

      // complete values that could not be found in ADMIN datastream
      if (StringTool.isNullOrEmpty(description)) {
        description = lastVersionOfFileDatastream.getLABEL();
      }
      if (StringTool.isNullOrEmpty(fileName)) {
        fileName = this.getFilename(sortValue, lastVersionOfFileDatastream);
      }

      log.debug("Datafile properties computed for object '{}'->'{}'", fedoraId, datastreamId);

      if (!description.equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)) {

        DocumentFile documentFile = this.fedora3Storage.setDocumentFile(publication, datastreamId, fileName, size, description,
                descriptionComplement, availability, license, mimetype, embargoAvailability, embargoEndDate, embargoPeriod,
                adminDS.getSubmissionDate(), lastVersionOfFileDatastream.getCREATED(), sortValue);

        this.importSynchronizationToSwissNationalLibrary(publication, documentFile, datastreamId, fedoraId, lastSyncWithSwissNationalLibrarySha1,
                lastSyncWithSwissNationalLibraryDate);

        DocumentFile savedDocumentFile = this.saveDocumentFileIfSourceDataValid(documentFile);
        publication.getDocumentFiles().add(savedDocumentFile);

        log.info("Document file created from datastream '{}' for pid {}", datastreamId, fedoraId);

        sortValue += 10;

      } else {
        this.setThumbnail(foxml, publication, datastreamId, fileName, description);
      }
    }
  }

  private void importSynchronizationToSwissNationalLibrary(Publication publication, DocumentFile documentFile, String datastreamId,
          String fedoraId, String lastSyncWithSwissNationalLibrarySha1, OffsetDateTime lastSyncWithSwissNationalLibraryDate) {
    if (!StringTool.isNullOrEmpty(lastSyncWithSwissNationalLibrarySha1) && lastSyncWithSwissNationalLibraryDate != null) {
      try {
        // Checksums were calculated with SHA-1 algorithm in previous version of Fedora, while we use SHA-256 now
        // --> during Fedora import, we consider that the last import corresponds to the current file, but we have to compute a new checksum value
        this.downloadFileAndComputeChecksum(documentFile);

        SwissNationalLibraryUpload upload = new SwissNationalLibraryUpload();
        upload.setDocumentFileChecksum(documentFile.getChecksum());
        upload.setDocumentFileName(documentFile.getFileName());
        upload.setDocumentFileType(documentFile.getDocumentFileType().getValue());
        upload.setUploadDate(lastSyncWithSwissNationalLibraryDate);
        upload.setPublication(publication);
        this.swissNationalLibraryUploadService.save(upload);
        log.info("Last synchronization to Swiss National Library imported for datastream '{}' for pid {}", datastreamId, fedoraId);

      } catch (IOException | NoSuchAlgorithmException e) {
        log.error(
                "Unable to download document file to compute its checksum in order to store last synchronization to Swiss National Library");
      }
    }
  }

  private String getFilename(int sortValue, DatastreamVersionType fileDataStreamVersion) {
    String filename = "file" + (sortValue / 10);
    String extension = this.fedora3Storage.getFileExtension(fileDataStreamVersion.getMIMETYPE());

    if (!StringTool.isNullOrEmpty(extension)) {
      return filename + "." + extension;
    }
    return filename;
  }

  private void setThumbnail(DigitalObject foxml, Publication publication, String datastreamId, String filename, String description) {
    if (StringTool.isNullOrEmpty(description) || !description.equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)) {
      throw new SolidifyRuntimeException("Attachment description is not a Vignette");
    }

    DatastreamVersionType lastVersionOfThumbnailDatastream = this.fedora3Storage.getLastVersionOfDatastream(foxml, datastreamId);

    String mimeType = lastVersionOfThumbnailDatastream.getMIMETYPE();
    Long size = lastVersionOfThumbnailDatastream.getSIZE();

    this.fedora3Storage.setThumbnail(publication, datastreamId, mimeType, size, filename);
  }

  public String getDepositDocFromFedora(DigitalObject foxml) {
    String marcXml = this.getLastVersionOfXmlDatastream(foxml, MARCXML_DATASTREAM_NAME);
    String xml = XMLTool.transform(marcXml, this.marc2pivotXsl);

    // Clean XML
    xml = xml.replace("<datasets/>", "");

    return xml;
  }

  private String getLastVersionOfXmlDatastream(DigitalObject foxml, String datastreamName) {
    DatastreamVersionType lastDsVersion = this.fedora3Storage.getLastVersionOfDatastream(foxml, datastreamName);
    Element element = lastDsVersion.getXmlContent().getAny().get(0);
    try {
      StringWriter sw = new StringWriter();
      this.transformer.transform(new DOMSource(element), new StreamResult(sw));
      return sw.toString().trim();
    } catch (TransformerException e) {
      throw new SolidifyRuntimeException("unable to get datastream '" + datastreamName + "' content for object " + foxml.getPID());
    }
  }

  private OffsetDateTime getCreationDate(DigitalObject foxml, AdminDatastreamDto adminDS) {
    OffsetDateTime creationDate = null;
    if (adminDS.getCreationDate() != null) {
      creationDate = this.parseDate(adminDS.getCreationDate());
    }
    if (creationDate == null && adminDS.getSubmissionDate() != null) {
      creationDate = this.parseDate(adminDS.getSubmissionDate());
    }
    if (creationDate == null && adminDS.getValidationDate() != null) {
      creationDate = this.parseDate(adminDS.getValidationDate());
    }
    if (creationDate == null) {
      List<PropertyType> properties = foxml.getObjectProperties().getProperty();
      for (PropertyType property : properties) {
        if (property.getNAME().equals("info:fedora/fedora-system:def/model#createdDate")) {
          creationDate = this.parseDate(property.getVALUE());
          break;
        }
      }
    }
    return creationDate;
  }

  private OffsetDateTime parseDate(String dateStr) {

    List<String> localDateFormats = List.of("yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm:ss");
    for (String dateFormat : localDateFormats) {
      try {
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(dateFormat));
        return OffsetDateTime.of(localDateTime, ZoneOffset.UTC);
      } catch (DateTimeParseException e) {
        log.debug("dateStr is not of format '{}'", dateFormat);
      }
    }

    try {
      return OffsetDateTime.parse(dateStr);
    } catch (DateTimeParseException e) {
      log.warn("unable to parse dateStr '{}'", dateStr);
      return null;
    }
  }

  private void storeFulltextInCache(DigitalObject foxml, DocumentFile documentFile) {
    String fileDatastreamId = this.getDocumentFileDatastreamId(foxml, documentFile);
    List<String> fulltextPossibleDatastreamIds = new ArrayList<>();
    if (fileDatastreamId != null) {
      if (fileDatastreamId.equals(ATTACHMENT + "01") || fileDatastreamId.equals(THESIS)) {
        fulltextPossibleDatastreamIds.add(FULLTEXT);
        fulltextPossibleDatastreamIds.add(FULLTEXT + "01");
        fulltextPossibleDatastreamIds.add(FULLTEXT + "1");
      } else if (fileDatastreamId.startsWith(ATTACHMENT)) {
        String suffix = fileDatastreamId.replace(ATTACHMENT, "");
        if (suffix.startsWith("0")) {
          suffix = suffix.substring(1);
        }
        fulltextPossibleDatastreamIds.add(FULLTEXT + "0" + suffix);
        fulltextPossibleDatastreamIds.add(FULLTEXT + suffix);
      }
    }

    if (!fulltextPossibleDatastreamIds.isEmpty()) {
      List<DatastreamType> datastreams = foxml.getDatastream();
      for (String fulltextDatastreamId : fulltextPossibleDatastreamIds) {
        List<DatastreamType> datastreamTypes = datastreams.stream().filter(ds -> ds.getID().equals(fulltextDatastreamId))
                .collect(Collectors.toList());
        if (datastreamTypes.size() == 1) {
          String fulltext = this.fedora3Storage.getDataStreamContent(foxml.getPID(), fulltextDatastreamId);
          this.documentFileService.storeFulltextInCache(documentFile, fulltext);
          break;
        }
      }
    }
  }

  private String getDocumentFileDatastreamId(DigitalObject foxml, DocumentFile documentFile) {
    List<DatastreamType> fileDatastreams = this.getFileDatastreams(foxml);
    for (DatastreamType datastreamType : fileDatastreams) {
      DatastreamVersionType datastreamVersionType = this.fedora3Storage.getLastVersionOfDatastream(foxml, datastreamType.getID());
      if (datastreamVersionType.getSIZE() == documentFile.getFileSize()) {
        URI fileUri = documentFile.getFinalData() != null ? documentFile.getFinalData() : documentFile.getSourceData();
        if (documentFile.getFileName().equals(datastreamVersionType.getLABEL())
                || (fileUri != null && fileUri.toString().endsWith("/" + datastreamType.getID() + "/content"))) {
          return datastreamType.getID();
        }
      }
    }
    return null;
  }

  private List<DatastreamType> getFileDatastreams(DigitalObject foxml) {
    return foxml.getDatastream().stream().filter(ds -> {
      for (String prefix : Fedora3Storage.FILE_DATASTREAM_PREFIXES) {
        if (ds.getID().startsWith(prefix)) {
          return true;
        }
      }
      return false;
    }).collect(Collectors.toList());
  }

  private String downloadFileAndComputeChecksum(DocumentFile documentFile) throws IOException, NoSuchAlgorithmException {
    Path tmpFilePath = this.documentFileService.getLocalTempFilePath(documentFile);
    if (!Files.exists(tmpFilePath) || FileTool.getSize(tmpFilePath) != documentFile.getFileSize()) {
      FileTool.ensureFolderExists(tmpFilePath.getParent());

      try (ReadableByteChannel readableByteChannel = Channels.newChannel(documentFile.getSourceData().toURL().openStream());
              FileOutputStream fileOutputStream = new FileOutputStream(tmpFilePath.toString())) {
        fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        log.info("document file downloaded to {}", tmpFilePath);
      }
    }
    String checksum = ChecksumTool.computeChecksum(
            new BufferedInputStream(new FileInputStream(tmpFilePath.toString()), SolidifyConstants.BUFFER_SIZE), ChecksumAlgorithm.SHA256);
    documentFile.setChecksum(checksum);
    FileTool.deleteFile(tmpFilePath);
    return checksum;
  }
}
