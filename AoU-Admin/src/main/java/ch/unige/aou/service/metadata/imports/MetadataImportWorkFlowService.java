/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataImportWorkFlowService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouRuntimeException;
import ch.unige.aou.exception.ErrorCodes;
import ch.unige.aou.model.ExistingPublicationInfo;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.DuplicateService;

@Service
@ConditionalOnBean(AdminController.class)
public class MetadataImportWorkFlowService {
  private static final Logger log = LoggerFactory.getLogger(MetadataImportWorkFlowService.class);

  private final DoiCrossrefImportService doiCrossrefImportService;
  private final ArxivImportService arxivImportService;
  private final PmidImportService pmidImportService;
  private final InspireHepImportService inspireHepImportService;
  private final EuropePmcImportService europePmcImportService;
  private final DataCiteImportService dataCiteImportService;
  private final UnpaywallImportService unpaywallImportService;
  private final MetadataService metadataService;
  private final GoogleIsbnImportService googleIsbnImportService;
  private final SwisscoveryImportService swisscoveryImportService;
  private final CongressLibraryIsbnImportService congressLibraryIsbnImportService;
  private final OrcidImportService orcidImportService;

  private final MessageService messageService;
  private final DuplicateService duplicateService;
  private final boolean checkForDuplicates;

  public MetadataImportWorkFlowService(DoiCrossrefImportService doiCrossrefImportService, ArxivImportService arxivImportService,
          PmidImportService pmidImportService, InspireHepImportService inspireHepImportService, EuropePmcImportService europePmcImportService,
          DataCiteImportService dataCiteImportService, UnpaywallImportService unpaywallImportService, MetadataService metadataService,
          GoogleIsbnImportService googleIsbnImportService, SwisscoveryImportService swisscoveryImportService,
          CongressLibraryIsbnImportService congressLibraryIsbnImportService,
          OrcidImportService orcidImportService, MessageService messageService, AouProperties aouProperties, DuplicateService duplicateService) {
    this.doiCrossrefImportService = doiCrossrefImportService;
    this.arxivImportService = arxivImportService;
    this.pmidImportService = pmidImportService;
    this.inspireHepImportService = inspireHepImportService;
    this.europePmcImportService = europePmcImportService;
    this.dataCiteImportService = dataCiteImportService;
    this.unpaywallImportService = unpaywallImportService;
    this.metadataService = metadataService;
    this.googleIsbnImportService = googleIsbnImportService;
    this.swisscoveryImportService = swisscoveryImportService;
    this.congressLibraryIsbnImportService = congressLibraryIsbnImportService;
    this.orcidImportService = orcidImportService;
    this.messageService = messageService;
    this.duplicateService = duplicateService;
    this.checkForDuplicates = aouProperties.getMetadata().getSearch().getDuplicates().isCheckForDuplicates();
  }

  public String getMetadataFromDoi(String doi, boolean isXmlFormat) {
    String cleanedDoi = CleanTool.trim(doi);

    if (ValidationTool.isValidShortDOI(cleanedDoi)) {
      try {
        // In case of shortDOI, its corresponding DOI must be retrieved
        cleanedDoi = DOIResolver.getDoiFromShortDOI(cleanedDoi);
      } catch (WebClientResponseException exception) {
        throw new SolidifyRuntimeException(this.messageService.get("deposit.error.identifiers.shortdoi.invalid", new Object[] { cleanedDoi }));
      }
    }
    if (!ValidationTool.isValidDOI(cleanedDoi)) {
      throw new SolidifyRuntimeException(this.messageService.get("deposit.error.identifiers.doi.invalid", new Object[] { cleanedDoi }));
    }

    this.checkForDuplicate(Publication.ImportSource.DOI, cleanedDoi);

    DepositDoc depositDoc = new DepositDoc();
    try {
      depositDoc = this.inspireHepImportService.searchInspireHep(cleanedDoi, false, depositDoc);
      //complete metadata in crossref when it has been imported by InspireHep
      depositDoc = this.doiCrossrefImportService.searchDoi(cleanedDoi, depositDoc);

    } catch (SolidifyRuntimeException e) {
      try {
        depositDoc = this.doiCrossrefImportService.searchDoi(cleanedDoi, depositDoc);
        // Check that first step properties have been recovered.
        if (this.isDepositDataNull(depositDoc)) {
          throw new SolidifyRuntimeException("The doi has not been found neither in crossref.");
        }
      } catch (SolidifyRuntimeException exception) {
        try {
          depositDoc = this.dataCiteImportService.searchDoi(cleanedDoi, depositDoc);
        } catch (SolidifyRuntimeException ex) {
          throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
                  this.messageService.get("deposit.error.identifiers.doi.not_found", new Object[] { cleanedDoi }));
        }
      }
    }

    try {
      depositDoc = this.pmidImportService.searchDoi(cleanedDoi, depositDoc);
      String pmcId = depositDoc.getIdentifiers().getPmcid();
      if (!StringTool.isNullOrEmpty(pmcId)) {
        depositDoc = this.europePmcImportService.searchEuropePmcId(pmcId, depositDoc);
      }
    } catch (SolidifyResourceNotFoundException e) {
      log.warn("Unable to import from PMID with the DOI '{}'", cleanedDoi);
    } catch (Exception e) {
      log.warn("Unable to import from PMID with the DOI '{}'", cleanedDoi, e);
    }

    // Check that first step properties have been recovered.
    if (this.isDepositDataNull(depositDoc)) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
              this.messageService.get("deposit.error.identifiers.doi.not_found", new Object[] { cleanedDoi }));
    }

    this.checkForIndirectDuplicate(Publication.ImportSource.DOI, depositDoc);

    if (isXmlFormat) {
      return this.doiCrossrefImportService.transformDepositDocToMetadata(depositDoc);
    } else {
      return this.doiCrossrefImportService.transformDepositDocToFormData(depositDoc);
    }
  }

  public String getMetadataFromPmid(String pmid, boolean isXmlFormat) {
    final String cleanedPmid = CleanTool.trim(pmid); // clean the PMID
    if (!ValidationTool.isValidPMID(cleanedPmid)) {
      throw new SolidifyRuntimeException(this.messageService.get("deposit.error.identifiers.pmid.invalid", new Object[] { cleanedPmid }));
    }

    this.checkForDuplicate(Publication.ImportSource.PMID, cleanedPmid);

    DepositDoc depositDoc = new DepositDoc();
    depositDoc = this.pmidImportService.searchPmid(cleanedPmid, depositDoc);

    String pmcId = depositDoc.getIdentifiers().getPmcid();
    if (!StringTool.isNullOrEmpty(pmcId)) {
      depositDoc = this.europePmcImportService.searchEuropePmcId(pmcId, depositDoc);
    }

    this.checkForIndirectDuplicate(Publication.ImportSource.PMID, depositDoc);

    if (isXmlFormat) {
      return this.pmidImportService.transformDepositDocToMetadata(depositDoc);
    } else {
      return this.pmidImportService.transformDepositDocToFormData(depositDoc);
    }
  }

  public String getMetadataFromArxiv(String arxivId, boolean isXmlFormat) {
    if (!ValidationTool.isValidArxivId(arxivId)) {
      throw new SolidifyRuntimeException(this.messageService.get("deposit.error.identifiers.arxiv.invalid", new Object[] { arxivId }));
    }

    this.checkForDuplicate(Publication.ImportSource.ARXIV, arxivId);

    DepositDoc depositDoc = new DepositDoc();
    try {
      depositDoc = this.inspireHepImportService.searchInspireHep(arxivId, true, depositDoc);
    } catch (SolidifyRuntimeException e) {
      try {
        depositDoc = this.arxivImportService.searchArxiv(arxivId, depositDoc);
      } catch (SolidifyRuntimeException ex) {
        throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
                this.messageService.get("deposit.error.identifiers.arxiv.not_found", new Object[] { arxivId }));
      }
    }
    if (isXmlFormat) {
      return this.inspireHepImportService.transformDepositDocToMetadata(depositDoc);
    } else {
      return this.inspireHepImportService.transformDepositDocToFormData(depositDoc);
    }
  }

  public String getMetadataFromOrcidPutCode(BigInteger putCode, Person creator, boolean isXmlFormat) {
    if (Boolean.TRUE.equals(creator.isVerifiedOrcid()) && creator.getOrcidToken() != null) {
      DepositDoc depositDoc = this.orcidImportService.searchOrcidWork(putCode, creator);
      if (isXmlFormat) {
        return this.orcidImportService.transformDepositDocToMetadata(depositDoc);
      } else {
        return this.orcidImportService.transformDepositDocToFormData(depositDoc);
      }
    } else {
      throw new SolidifyRuntimeException("Person " + creator.getResId() + " (" + creator.getFullName() + ") doesn't have any ORCID token");
    }
  }

  public void importFilesFromServices(Publication publication) throws IOException, NoSuchAlgorithmException {
    //import from arxiv if needed
    if (publication.getImportSource().equals(Publication.ImportSource.ARXIV)
            || publication.getImportSource().equals(Publication.ImportSource.ARXIV_ORCID)) {
      try {
        this.arxivImportService.createDocumentFileFromArxivMetadata(publication);
      } catch (SolidifyRuntimeException e) {
        this.inspireHepImportService.createDocumentFileFromInspireHepMetadata(publication);
      }
    } else if (publication.getImportSource().equals(Publication.ImportSource.DOI)
            || publication.getImportSource().equals(Publication.ImportSource.DOI_ORCID)) {
      this.doiCrossrefImportService.createDocumentFileFromCrossrefMetadata(publication);
      try {
        this.unpaywallImportService.createDocumentFileFromUnpaywall(publication);
      } catch (RuntimeException ex) {
        log.warn("unable to get metadata correctly from unpaywall, {}", ex.getMessage());
      }
      if (publication.getDocumentFiles().isEmpty() && publication.getDoi().toLowerCase().contains("arxiv")) {
        this.arxivImportService.createDocumentFileFromDoiArxiv(publication);
      }
    } else if (publication.getImportSource().equals(Publication.ImportSource.PMID)
            || publication.getImportSource().equals(Publication.ImportSource.PMID_ORCID)) {
      //check if there is pmcid, if so import files from (Europe)PMC
      String pmcId = this.getPmcIdFromPublicationMetadata(publication);
      if (!StringTool.isNullOrEmpty(pmcId)) {
        this.europePmcImportService.createDocumentFileFromEuropePmc(publication);
      }
      this.unpaywallImportService.createDocumentFileFromUnpaywall(publication);
    } else if (publication.getImportSource().equals(Publication.ImportSource.ISBN)
            || publication.getImportSource().equals(Publication.ImportSource.ISBN_ORCID)) {
      this.unpaywallImportService.createDocumentFileFromUnpaywall(publication);
    }
  }

  private String getPmcIdFromPublicationMetadata(Publication publication) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    return metadataExtractor.getPmcIdFromPublication(publication);
  }

  public String getMetadataFromIsbn(String isbn, boolean isXmlFormat) {
    if (!ValidationTool.isValidISBN(isbn)) {
      throw new SolidifyRuntimeException(this.messageService.get("deposit.error.identifiers.isbn.invalid", new Object[] { isbn }));
    }

    this.checkForDuplicate(Publication.ImportSource.ISBN, isbn);

    DepositDoc depositDoc = new DepositDoc();
    try {
      depositDoc = this.googleIsbnImportService.searchInGoogle(isbn, depositDoc);
    } catch (SolidifyRuntimeException e) {
      try {
        depositDoc = this.swisscoveryImportService.searchIsbn(isbn, depositDoc);
      } catch (SolidifyRuntimeException ex) {
        try {
          depositDoc = this.congressLibraryIsbnImportService.searchInCongressLibrary(isbn, depositDoc);
        } catch (SolidifyRuntimeException exp){
          throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
                  this.messageService.get("deposit.error.identifiers.isbn.not_found", new Object[] { isbn }));
        }

      }
    }

    this.checkForIndirectDuplicate(Publication.ImportSource.ISBN, depositDoc);

    if (isXmlFormat) {
      return this.googleIsbnImportService.transformDepositDocToMetadata(depositDoc);
    } else {
      return this.googleIsbnImportService.transformDepositDocToFormData(depositDoc);
    }
  }

  private void checkForDuplicate(Publication.ImportSource source, String value) {
    if (this.checkForDuplicates) {
      ExistingPublicationInfo existingPublicationInfo = null;
      String message = null;
      switch (source) {
        case DOI:
          existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByDoi(value);
          message = this.messageService.get("deposit.error.identifiers.doi.already_exists", new Object[] { value });
          break;
        case PMID:
          existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByPmid(value);
          message = this.messageService.get("deposit.error.identifiers.pmid.already_exists", new Object[] { value });
          break;
        case ARXIV:
          existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByArxivId(value);
          message = this.messageService.get("deposit.error.identifiers.arxiv.already_exists", new Object[] { value });
          break;
        case ISBN:
          existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByIsbn(value);
          message = this.messageService.get("deposit.error.identifiers.isbn.already_exists", new Object[] { value });
          break;
        default:
          break;
      }

      this.throwExistingPublicationInfoExceptionIfExists(existingPublicationInfo, message);
    }
  }

  private void checkForIndirectDuplicate(Publication.ImportSource source, DepositDoc depositDoc) {
    switch (source) {
      case PMID:
        this.checkIndirectDoi(depositDoc);
        break;
      case DOI:
        this.checkIndirectPmid(depositDoc);
        break;
      case ISBN:
        this.checkIndirectDoi(depositDoc);
        this.checkIndirectPmid(depositDoc);
        break;
      default:
        break;
    }
  }

  private void checkIndirectDoi(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getIdentifiers().getDoi())) {
      String doi = depositDoc.getIdentifiers().getDoi();
      ExistingPublicationInfo existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByDoi(doi);
      if (existingPublicationInfo != null) {
        this.throwExistingPublicationInfoExceptionIfExists(existingPublicationInfo,
                this.messageService.get("deposit.error.identifiers.indirect_already_exists"));
      }
    }
  }

  private void checkIndirectPmid(DepositDoc depositDoc) {
    if (depositDoc.getIdentifiers().getPmid() != null) {
      String pmid = depositDoc.getIdentifiers().getPmid().toString();
      ExistingPublicationInfo existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByPmid(pmid);
      if (existingPublicationInfo != null) {
        this.throwExistingPublicationInfoExceptionIfExists(existingPublicationInfo,
                this.messageService.get("deposit.error.identifiers.indirect_already_exists"));
      }
    }
  }

  private boolean isDepositDataNull(DepositDoc depositDoc) {
    return depositDoc.getTitle() == null
            && depositDoc.getType() == null
            && depositDoc.getOriginalTitle() == null
            && depositDoc.getSubtype() == null
            && depositDoc.getSubsubtype() == null;
  }


  private void throwExistingPublicationInfoExceptionIfExists(ExistingPublicationInfo existingPublicationInfo, String message) {
    final Map<Object, Object> params = this.duplicateService.getParamsFromExistingPublicationInfo(existingPublicationInfo);
    if (params != null) {
      throw new AouRuntimeException(HttpStatus.BAD_REQUEST, message, ErrorCodes.IDENTIFIER_ALREADY_EXISTS.value(), params);
    }
  }

}
