/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataValidatorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.MetadataClassification;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataCorrection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.MetadataFunding;
import ch.unige.aou.model.publication.MetadataLink;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeContributorRole;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileType;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.model.rest.UnigePersonDTO;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.metadata.imports.MetadataImportService;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.service.rest.UnigePersonSearchService;

public abstract class MetadataValidatorService implements MetadataValidator {

  protected static final String FORM_STEP_TYPE_PREFIX = Publication.DepositFormStep.TYPE.name().toLowerCase() + ".";
  protected static final String FORM_STEP_FILES_PREFIX = Publication.DepositFormStep.FILES.name().toLowerCase() + ".";
  protected static final String FORM_STEP_CONTRIBUTORS_PREFIX = Publication.DepositFormStep.CONTRIBUTORS.name().toLowerCase() + ".";
  protected static final String FORM_STEP_DESCRIPTION_PREFIX = Publication.DepositFormStep.DESCRIPTION.name().toLowerCase() + ".";

  private static final String ERROR_TYPE_EMPTY = "deposit.error.type.empty";
  private static final String ERROR_TYPE_INVALID_NAME = "deposit.error.type.invalid_name";
  private static final String ERROR_SUBTYPE_EMPTY = "deposit.error.subtype.empty";
  private static final String ERROR_SUBTYPE_INVALID_NAME = "deposit.error.subtype.invalid_name";
  private static final String ERROR_SUBSUBTYPE_INVALID_NAME = "deposit.error.subSubtype.invalid_name";
  private static final String ERROR_INVALID_SUBTYPE_FOR_TYPE = "deposit.error.subtype.invalid_type";

  protected static final String ERROR_INVALID_ISSN = "deposit.error.identifiers.issn.invalid";

  protected static final String ERROR_INVALID_DOI = "deposit.error.identifiers.doi.invalid";
  protected static final String ERROR_DOI_ALREADY_EXISTS = "deposit.error.identifiers.doi.already_exists";

  protected static final String ERROR_INVALID_PMID = "deposit.error.identifiers.pmid.invalid";
  protected static final String ERROR_PMID_ALREADY_EXISTS = "deposit.error.identifiers.pmid.already_exists";

  protected static final String ERROR_INVALID_ARXIV_ID = "deposit.error.identifiers.arxiv.invalid";
  protected static final String ERROR_ARXIV_ID_ALREADY_EXISTS = "deposit.error.identifiers.arxiv.already_exists";

  protected static final String ERROR_INVALID_ISBN = "deposit.error.identifiers.isbn.invalid";
  protected static final String ERROR_ISBN_ALREADY_EXISTS = "deposit.error.identifiers.isbn.already_exists";

  protected static final String ERROR_INVALID_URN = "deposit.error.identifiers.urn.invalid";

  private static final String ERROR_CONTRIBUTOR_AT_LEAST_ONE_IS_MANDATORY = "deposit.error.contributors.publication_has_no_contributor";
  private static final String ERROR_CONTRIBUTOR_APPEARS_MORE_THAN_ONCE = "deposit.error.contributors.appears_more_than_once";
  private static final String ERROR_CONTRIBUTOR_NAME_UPPERCASE = "deposit.error.contributors.uppercase_name";
  private static final String ERROR_CONTRIBUTOR_INVALID_ROLE = "deposit.error.contributors.invalid_role";

  private static final String ERROR_CONTRIBUTOR_FIRSTNAME_CHANGED = "deposit.error.contributors.firstname_changed";
  private static final String ERROR_CONTRIBUTOR_LASTNAME_CHANGED = "deposit.error.contributors.lastname_changed";

  private static final String ERROR_MISSING_AUTHOR_IN_CONTRIBUTORS = "deposit.error.contributors.missing_author_in_contributors";
  private static final String ERROR_MISSING_DIRECTOR_IN_CONTRIBUTORS = "deposit.error.contributors.missing_director_in_contributors";
  private static final String ERROR_ONLY_ONE_AUTHOR_IN_CONTRIBUTORS = "deposit.error.contributors.only_one_author_in_contributors";
  protected static final String ERROR_FIELD_CANNOT_BE_EMPTY = "deposit.error.field_cannot_be_empty";
  protected static final String ERROR_FIELD_INVALID_VALUE = "deposit.error.field_invalid_value";
  private static final String ERROR_STRUCTURE_INVALID_ID = "deposit.error.structure.invalid_id";
  private static final String ERROR_STRUCTURE_INCLUDED_IN_ANOTHER_ONE = "deposit.error.structure.included_in_another_one";
  private static final String ERROR_RESEARCH_GROUP_INVALID_ID = "deposit.error.researchGroup.invalid_id";
  private static final String ERROR_RESEARCH_GROUP_NOT_VALID = "deposit.error.researchGroup.not_valid";
  private static final String ERROR_INVALID_DATE = "deposit.error.date.invalid_date";
  private static final String ERROR_DATE_MANDATORY = "deposit.error.date.at_least_one_date_is_mandatory";
  private static final String ERROR_DATE_IN_THE_FUTURE = "deposit.error.date.cannot_be_in_the_future";
  protected static final String ERROR_LANGUAGE_APPEARS_MORE_THAN_ONCE = "deposit.error.language.appears_more_than_once";
  protected static final String ERROR_LANGUAGE_MANDATORY = "deposit.error.language.at_least_one_value_mandatory";
  protected static final String ERROR_ABSTRACT_MAXIMUM_SIZE_EXCEEDED = "deposit.error.abstract.maximum_size_exceeded";
  protected static final String ERROR_ABSTRACT_MANDATORY_FOR_DIPLOMA = "deposit.error.abstract.mandatory_for_diploma";
  protected static final String ERROR_FILES_IMPRIMATUR_MANDATORY_FOR_THESIS = "deposit.error.files.imprimatur_mandatory_for_thesis";
  protected static final String ERROR_FILES_PUBLICATION_MODE_MANDATORY = "deposit.error.files.publication_mode_mandatory";
  protected static final String ERROR_FILES_TYPE_NOT_ALLOWED_WITH_PUBLICATION_SUBTYPE = "deposit.error.files.file_type_not_allowed_with_publication_type";
  protected static final String ERROR_FILE_IS_NOT_SURE_FOR_LICENSE = "deposit.error.files.not_sure_for_license";
  protected static final String ERROR_FILES_EMPTY = "deposit.error.files.empty";
  protected static final String ERROR_FILES_EMPTY_AFTER_UPDATE = "deposit.error.files.empty_after_update";
  protected static final String ERROR_INVALID_JEL = "deposit.error.description.invalid_jel";

  protected final MetadataExtractor metadataExtractor;
  protected final MessageService messageService;
  private final PublicationTypeService publicationTypeService;
  private final PublicationSubtypeService publicationSubtypeService;
  private final PublicationSubSubtypeService publicationSubSubtypeService;
  private final ResearchGroupService researchGroupService;
  private final StructureService structureService;
  protected DuplicateService duplicateService;
  private final UnigePersonSearchService unigePersonSearchService;
  private final DocumentFileService documentFileService;
  private final HistoryService historyService;

  private final String[] metadataDateFormat;
  protected boolean checkForDuplicates;
  private final List<String> structuresRequiringPublicationMode;
  private final int maxSizeForAbstract;

  public MetadataValidatorService(AouProperties aouProperties, MetadataExtractor metadataExtractor, MessageService messageService,
          PublicationTypeService publicationTypeService, PublicationSubtypeService publicationSubtypeService,
          PublicationSubSubtypeService publicationSubSubtypeService, ResearchGroupService researchGroupService,
          StructureService structureService, DuplicateService duplicateService, UnigePersonSearchService unigePersonSearchService,
          DocumentFileService documentFileService, HistoryService historyService) {
    this.metadataExtractor = metadataExtractor;
    this.messageService = messageService;
    this.publicationTypeService = publicationTypeService;
    this.publicationSubtypeService = publicationSubtypeService;
    this.publicationSubSubtypeService = publicationSubSubtypeService;
    this.structureService = structureService;
    this.researchGroupService = researchGroupService;
    this.duplicateService = duplicateService;
    this.historyService = historyService;

    this.metadataDateFormat = aouProperties.getParameters().getMetadataDateFormat();
    this.checkForDuplicates = aouProperties.getMetadata().getSearch().getDuplicates().isCheckForDuplicates();
    this.structuresRequiringPublicationMode = List.of(
            aouProperties.getMetadata().getValidation().getStructureIdsRequiringPublicationModeForThesis());
    this.maxSizeForAbstract = aouProperties.getMetadata().getValidation().getMaxSizeForAbstract();
    this.unigePersonSearchService = unigePersonSearchService;
    this.documentFileService = documentFileService;
  }

  @Override
  public void validate(Publication publication, BindingResult errors) {
    if (publication.getMetadataValidationType() != null) {
      Object depositDocObj = this.metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
      switch (publication.getMetadataValidationType()) {
        case CURRENT_STEP:
          this.validateCurrentFormStep(publication, depositDocObj, errors);
          break;
        case GLOBAL:
          this.validateAll(publication, depositDocObj, errors);
          break;
        case GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION:
          this.validateAll(publication, depositDocObj, errors);
          this.validateRulesForValidators(publication, depositDocObj, errors);
          break;
        case NONE:
          break;
      }
    } else {
      throw new SolidifyRuntimeException("MetadataValidationType cannot be null");
    }
  }

  /******************************************************************************************************************/

  /**
   * Validate the metadata for the current publication form step
   *
   * @param publication
   * @param errors
   */
  protected void validateCurrentFormStep(Publication publication, Object depositDocObj, BindingResult errors) {
    switch (publication.getFormStep()) {
      case TYPE:
        this.validateFormTypeStep(publication, depositDocObj, errors);
        break;
      case FILES:
        this.validateFormFilesStep(publication, depositDocObj, errors);
        break;
      case CONTRIBUTORS:
        this.validateFormContributorsStep(publication, depositDocObj, errors);
        break;
      case DESCRIPTION:
        this.validateFormDescriptionStep(publication, depositDocObj, errors);
        break;
      case SUMMARY:
        break;
    }
  }

  /**
   * Validate metadata for all form steps
   *
   * @param publication
   * @param errors
   */
  protected void validateAll(Publication publication, Object depositDocObj, BindingResult errors) {
    this.validateFormTypeStep(publication, depositDocObj, errors);
    this.validateFormFilesStep(publication, depositDocObj, errors);
    this.validateFormContributorsStep(publication, depositDocObj, errors);
    this.validateFormDescriptionStep(publication, depositDocObj, errors);
    this.validatePublicationModeForThesis(publication, depositDocObj, errors);
    this.validateImprimaturForThesis(publication, depositDocObj, errors);
    this.validateFilesNotMissing(publication, depositDocObj, errors);
  }

  public void validateRulesForValidators(Publication publication, Object depositDocObj, BindingResult errors) {
    this.validateUnigeContributorsName(publication, depositDocObj, errors);
    this.validateNotSureForLicenseFiles(publication, depositDocObj, errors);
  }

  /******************************************************************************************************************/

  protected void validateFormTypeStep(Publication publication, Object depositDocObj, BindingResult errors) {
    this.validateTypes(publication, depositDocObj, errors);
    this.validateTitle(publication, depositDocObj, errors);
    this.validateLanguage(publication, depositDocObj, errors);
  }

  protected void validateFormFilesStep(Publication publication, Object depositDocObj, BindingResult errors) {
    this.validateFileTypeAllowedForPublicationType(publication, depositDocObj, errors);
  }

  protected void validateFormContributorsStep(Publication publication, Object depositDocObj, BindingResult errors) {
    this.validateHasContributor(publication, depositDocObj, errors);
    this.validateContributorsProperties(publication, depositDocObj, errors);
    this.validateContributorsAppearOnce(publication, depositDocObj, errors);
    this.validateAuthorAndDirectorAppearsOnceForMaster(publication, depositDocObj, errors);
    this.validateContributorTypesAllowedForPublicationType(publication, depositDocObj, errors);

    this.validateResearchGroups(publication, depositDocObj, errors);
    this.validateStructuresExist(publication, depositDocObj, errors);
    this.validateStructuresHierarchyDuplicates(publication, depositDocObj, errors);
  }

  protected void validateFormDescriptionStep(Publication publication, Object depositDocObj, BindingResult errors) {
    this.validateTypes(publication, depositDocObj, errors);
    this.validateTitle(publication, depositDocObj, errors);
    this.validateOriginalTitle(publication, depositDocObj, errors);
    this.validateDates(publication, depositDocObj, errors, false);
    this.validateISBN(publication, depositDocObj, errors);
    this.validateArxivId(publication, depositDocObj, errors);
    this.validateURN(publication, depositDocObj, errors);
    this.validateAbstracts(publication, depositDocObj, errors);
    this.validateCollection(publication, depositDocObj, errors);
    this.validateLinks(publication, depositDocObj, errors);
    this.validateLanguage(publication, depositDocObj, errors);
    this.validateDatasets(publication, depositDocObj, errors);
    this.validateFundings(publication, depositDocObj, errors);
    this.validateCorrections(publication, depositDocObj, errors);
    this.validateJELCode(publication, depositDocObj, errors);
  }

  // Validate Jel Code for WORKING PAPER only
  protected void validateJELCode(Publication publication, Object depositDocObj, BindingResult errors) {
    if (publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_ID)) {
      List<MetadataClassification> classificationList = this.metadataExtractor.getClassifications(depositDocObj);
      for (MetadataClassification classification : classificationList) {
        if (classification.getCode().equals("JEL") && !ValidationTool.isValidJEL(classification.getItem())) {
          errors.addError(
                  new FieldError(publication.getClass().getSimpleName(),
                          FORM_STEP_DESCRIPTION_PREFIX + "jel." + classification.getItem() + ".code",
                          this.messageService.get(ERROR_INVALID_JEL, new Object[] { classification.getItem() })));
        }
      }
    }
  }

  /******************************************************************************************************************/

  protected void validateTitle(Publication publication, Object depositDocObj, BindingResult errors) {
    String title = this.metadataExtractor.getTitleContent(depositDocObj);
    if (StringTool.isNullOrEmpty(title)) {
      errors.addError(
              new FieldError(publication.getClass().getSimpleName(), FORM_STEP_TYPE_PREFIX + "title",
                      this.messageService.get(ERROR_FIELD_CANNOT_BE_EMPTY)));
    }
  }

  protected void validateOriginalTitle(Publication publication, Object depositDocObj, BindingResult errors) {
    String originalTitle = this.metadataExtractor.getOriginalTitleContent(depositDocObj);
    if (!StringTool.isNullOrEmpty(originalTitle)) {
      // if original title exists, validate it has a language
      String lang = this.metadataExtractor.getOriginalTitleLang(depositDocObj);
      if (StringTool.isNullOrEmpty(lang)) {
        errors.addError(
                new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "originalTitle.lang",
                        this.messageService.get(ERROR_FIELD_CANNOT_BE_EMPTY)));
      }
    }
  }

  protected void validateFileTypeAllowedForPublicationType(Publication publication, Object depositDocObj, BindingResult errors) {
    List<MetadataFile> files = this.metadataExtractor.getFiles(depositDocObj);
    if (!CollectionUtils.isEmpty(files)) {

      // list of allowed file types for the publication subtype
      PublicationSubtype publicationSubtype = this.publicationSubtypeService.findOne(publication.getSubtype().getResId());
      List<PublicationSubtypeDocumentFileType> publicationSubtypeDocumentFileTypes = publicationSubtype.getPublicationSubtypeDocumentFileTypes();
      List<String> allowedDocumentFileTypeValues = publicationSubtypeDocumentFileTypes.stream()
              .map(psdft -> psdft.getDocumentFileType().getValue()).collect(Collectors.toList());

      int index = 0;
      for (MetadataFile metadataFile : files) {
        String fileType = metadataFile.getType();
        if (!allowedDocumentFileTypeValues.contains(fileType) && !AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE.equals(fileType)) {
          errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_FILES_PREFIX + "files." + index + ".type",
                  this.messageService.get(ERROR_FILES_TYPE_NOT_ALLOWED_WITH_PUBLICATION_SUBTYPE,
                          new Object[] { fileType, publicationSubtype.getName() })));
        }
        index++;
      }
    }
  }

  private void validateNotSureForLicenseFiles(Publication publication, Object depositDocObj, BindingResult errors) {
    List<MetadataFile> files = this.metadataExtractor.getFiles(depositDocObj);
    if (!CollectionUtils.isEmpty(files)) {
      int index = 0;
      for (MetadataFile metadataFile : files) {
        // metadataFile created with a previous version of metadata or thumbnails may have an empty resId
        if (!StringTool.isNullOrEmpty(metadataFile.getResId())) {
          DocumentFile documentFile = this.documentFileService.findOne(metadataFile.getResId());
          if (documentFile.getNotSureForLicense()) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_FILES_PREFIX + "files." + index + ".license",
                    this.messageService.get(ERROR_FILE_IS_NOT_SURE_FOR_LICENSE, new Object[] { metadataFile.getName() })));
          }
        }
        index++;
      }
    }
  }

  protected void validateImprimaturForThesis(Publication publication, Object depositDocObj, BindingResult errors) {
    String subtypeName = this.metadataExtractor.getSubtype(depositDocObj);
    if (AouConstants.DEPOSIT_SUBTYPE_THESE_NAME.equals(subtypeName)) {
      boolean imprimaturExists = false;
      List<MetadataFile> files = this.metadataExtractor.getFiles(depositDocObj);
      if (files != null && !files.isEmpty()) {
        imprimaturExists = files.stream().anyMatch(f -> f.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE));
      }

      // Check if the publication has a METADATA structure. If not, it is probably a thesis made in another university
      // --> Imprimatur is not mandatory
      if (!imprimaturExists && publication.getPublicationStructures().stream()
              .anyMatch(ps -> ps.getLinkType().equals(PublicationStructure.LinkType.METADATA))
              && !this.historyService.hasBeenCompleted(publication.getResId())
              && !this.metadataExtractor.isBefore2010orBeforeAuthorJoiningUnige(depositDocObj)) {
        errors.addError(
                new FieldError(publication.getClass().getSimpleName(), FORM_STEP_FILES_PREFIX + "files",
                        this.messageService.get(ERROR_FILES_IMPRIMATUR_MANDATORY_FOR_THESIS)));
      }
    }
  }

  protected void validatePublicationModeForThesis(Publication publication, Object depositDocObj, BindingResult errors) {
    String subtypeName = this.metadataExtractor.getSubtype(depositDocObj);
    if (AouConstants.DEPOSIT_SUBTYPE_THESE_NAME.equals(subtypeName)) {
      Structure structure = this.getFirstStructureRequiringPublicationMode(publication);
      if (structure != null) {
        List<MetadataFile> files = this.metadataExtractor.getFiles(depositDocObj);
        if ((CollectionUtils.isEmpty(files)
                || files.stream().noneMatch(f -> f.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE)))
                && !this.historyService.hasBeenCompleted(publication.getResId())
                && !this.metadataExtractor.isBefore2010orBeforeAuthorJoiningUnige(depositDocObj)) {
          errors.addError(
                  new FieldError(publication.getClass().getSimpleName(), FORM_STEP_FILES_PREFIX + "files",
                          this.messageService.get(ERROR_FILES_PUBLICATION_MODE_MANDATORY, new Object[] { structure.getName() })));
        }
      }
    }
  }

  protected void validateTypes(Publication publication, Object depositDocObj, BindingResult errors) {

    String typeName = this.metadataExtractor.getType(depositDocObj);
    String subtypeName = this.metadataExtractor.getSubtype(depositDocObj);
    String subSubtypeName = this.metadataExtractor.getSubSubtype(depositDocObj);

    /**
     * validate type
     */
    PublicationType publicationType = null;
    if (StringTool.isNullOrEmpty(typeName)) {
      errors.addError(
              new FieldError(publication.getClass().getSimpleName(), FORM_STEP_TYPE_PREFIX + "type",
                      this.messageService.get(ERROR_TYPE_EMPTY)));
    } else {
      publicationType = this.publicationTypeService.findByName(typeName);
      if (publicationType == null) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_TYPE_PREFIX + "type",
                this.messageService.get(ERROR_TYPE_INVALID_NAME)));
      }
    }

    /**
     * Validate subtype
     */
    if (StringTool.isNullOrEmpty(subtypeName)) {
      errors.addError(
              new FieldError(publication.getClass().getSimpleName(), FORM_STEP_TYPE_PREFIX + "subtype",
                      this.messageService.get(ERROR_SUBTYPE_EMPTY)));
    } else {
      Optional<PublicationSubtype> publicationSubtypeOpt = this.publicationSubtypeService.findByName(subtypeName);
      if (publicationSubtypeOpt.isEmpty()) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_TYPE_PREFIX + "subtype",
                this.messageService.get(ERROR_SUBTYPE_INVALID_NAME)));
      } else {
        PublicationSubtype publicationSubtype = publicationSubtypeOpt.get();

        /**
         * Validate that subtype's parent is the given type
         */
        if (publicationType != null && !publicationSubtype.getPublicationType().getResId()
                .equals(publicationType.getResId())) {
          errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_TYPE_PREFIX + "subtype",
                  this.messageService.get(ERROR_INVALID_SUBTYPE_FOR_TYPE)));
        }

        /**
         * Validate subsubtype
         */
        if (!StringTool.isNullOrEmpty(subSubtypeName)) {
          PublicationSubSubtype publicationSubSubtype = this.publicationSubSubtypeService
                  .findByNameAndPublicationSubType(subSubtypeName, publicationSubtype);
          if (publicationSubSubtype == null) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_TYPE_PREFIX + "subsubtype",
                    this.messageService.get(ERROR_SUBSUBTYPE_INVALID_NAME)));
          }
        }
      }
    }
  }

  protected void validateHasContributor(Publication publication, Object depositDocObj, BindingResult errors) {
    List<AbstractContributor> contributors = this.metadataExtractor.getContributors(depositDocObj);
    if (!contributors.stream().anyMatch(abstractContributor -> abstractContributor instanceof ContributorDTO)) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_CONTRIBUTORS_PREFIX + "contributors",
              this.messageService.get(ERROR_CONTRIBUTOR_AT_LEAST_ONE_IS_MANDATORY)));
    }
  }

  protected void validateContributorsProperties(Publication publication, Object depositDocObj, BindingResult errors) {
    List<AbstractContributor> contributors = this.metadataExtractor.getContributors(depositDocObj);
    String contributorPrefix = "contributors.";
    IntStream.range(0, contributors.size()).forEach(index -> {
      if (contributors.get(index) instanceof ContributorDTO) {
        // Case of contributors
        ContributorDTO contributorDTO = (ContributorDTO) contributors.get(index);
        this.checkStringFieldIsNotEmpty(publication, contributorDTO.getLastName(),
                FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".lastname", errors);
        this.checkStringFieldIsNotEmpty(publication, contributorDTO.getRole(),
                FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".role", errors);
        if ("unige".equals(contributorDTO.getInstitution())) {
          this.checkStringFieldIsNotEmpty(publication, contributorDTO.getCnIndividu(),
                  FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".cnIndividu", errors);
        }

        if (!StringTool.isNullOrEmpty(contributorDTO.getFirstName())) {
          if (MetadataImportService.UNDEFINED_NAME_VALUE.equals(contributorDTO.getFirstName())) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(),
                    FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".firstname",
                    this.messageService.get(ERROR_FIELD_INVALID_VALUE)));
          }
          if (!ValidationTool.isValidName(contributorDTO.getFirstName())) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(),
                    FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".firstname",
                    this.messageService.get(ERROR_CONTRIBUTOR_NAME_UPPERCASE)));
          }
        }

        if (!StringTool.isNullOrEmpty(contributorDTO.getLastName())) {
          if (MetadataImportService.UNDEFINED_NAME_VALUE.equals(contributorDTO.getLastName())) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(),
                    FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".lastname",
                    this.messageService.get(ERROR_FIELD_INVALID_VALUE)));
          }
          if (!ValidationTool.isValidName(contributorDTO.getLastName())) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(),
                    FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".lastname",
                    this.messageService.get(ERROR_CONTRIBUTOR_NAME_UPPERCASE)));
          }
        }
      } else {
        // Case of collaborations
        this.checkStringFieldIsNotEmpty(publication, ((CollaborationDTO) contributors.get(index)).getName(),
                FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".name", errors);
      }
    });
  }

  protected void validateContributorsAppearOnce(Publication publication, Object depositDocObj, BindingResult errors) {
    List<AbstractContributor> contributors = this.metadataExtractor.getContributors(depositDocObj);
    List<String> cnIndividus = new ArrayList<>();
    int index = 0;
    for (AbstractContributor contributor : contributors) {
      if (contributor instanceof ContributorDTO && !StringTool.isNullOrEmpty(((ContributorDTO) contributor).getCnIndividu())) {
        if (!cnIndividus.contains(((ContributorDTO) contributor).getCnIndividu())) {
          cnIndividus.add(((ContributorDTO) contributor).getCnIndividu());
        } else {
          errors.addError(
                  new FieldError(publication.getClass().getSimpleName(),
                          FORM_STEP_CONTRIBUTORS_PREFIX + "contributors." + index + ".cnIndividu",
                          this.messageService.get(ERROR_CONTRIBUTOR_APPEARS_MORE_THAN_ONCE)));
        }
      }
      index++;
    }
  }

  protected void validateAuthorAndDirectorAppearsOnceForMaster(Publication publication, Object depositDocObj, BindingResult errors) {
    if (this.publicationSubtypeService.isThesisOrProfessionalThesisOrMaster(publication.getSubtype().getName())) {
      List<AbstractContributor> contributors = this.metadataExtractor.getContributors(depositDocObj);
      boolean isThereAuthorInContributors = contributors.stream().filter(c -> c instanceof ContributorDTO)
              .anyMatch(c -> ((ContributorDTO) c).getRole().equals(AouConstants.CONTRIBUTOR_ROLE_AUTHOR));
      if (!isThereAuthorInContributors) {
        errors.addError(
                new FieldError(publication.getClass().getSimpleName(),
                        FORM_STEP_CONTRIBUTORS_PREFIX + "contributors", this.messageService.get(ERROR_MISSING_AUTHOR_IN_CONTRIBUTORS,
                        new Object[] { publication.getSubtype().getName() })));
      }
      boolean isThereDirectorInContributors = contributors.stream().filter(c -> c instanceof ContributorDTO)
              .anyMatch(c -> ((ContributorDTO) c).getRole().equals(AouConstants.CONTRIBUTOR_ROLE_DIRECTOR));
      if (!isThereDirectorInContributors && !this.metadataExtractor.isBefore2010orBeforeAuthorJoiningUnige(depositDocObj)) {
        errors.addError(
                new FieldError(publication.getClass().getSimpleName(),
                        FORM_STEP_CONTRIBUTORS_PREFIX + "contributors", this.messageService.get(ERROR_MISSING_DIRECTOR_IN_CONTRIBUTORS,
                        new Object[] { publication.getSubtype().getName() })));
      }

      //Check there is only one author for thesis and thesis professional subTypes
      if (this.publicationSubtypeService.isThesisOrThesisProfessional(publication.getSubtype().getName())) {
        long numberOfAuthors = contributors.stream().filter(c -> c instanceof ContributorDTO).
                filter(c -> ((ContributorDTO) c).getRole().equals(AouConstants.CONTRIBUTOR_ROLE_AUTHOR)).count();
        if (numberOfAuthors != 1) {
          errors.addError(
                  new FieldError(publication.getClass().getSimpleName(),
                          FORM_STEP_CONTRIBUTORS_PREFIX + "contributors", this.messageService.get(ERROR_ONLY_ONE_AUTHOR_IN_CONTRIBUTORS,
                          new Object[] { publication.getSubtype().getName() })));
        }
      }
    }
  }

  protected void validateContributorTypesAllowedForPublicationType(Publication publication, Object depositDocObj, BindingResult errors) {
    List<AbstractContributor> contributors = this.metadataExtractor.getContributors(depositDocObj);
    if (!CollectionUtils.isEmpty(contributors)) {

      // list of allowed contributor roles for the publication subtype
      PublicationSubtype publicationSubtype = this.publicationSubtypeService.findOne(publication.getSubtype().getResId());
      List<PublicationSubtypeContributorRole> publicationSubtypeContributorRoles = publicationSubtype.getPublicationSubtypeContributorRoles();
      List<String> allowedContributorRoleValues = publicationSubtypeContributorRoles.stream()
              .map(pscr -> pscr.getContributorRole().getValue()).collect(Collectors.toList());

      String contributorPrefix = "contributors.";

      int index = 0;
      for (AbstractContributor abstractContributor : contributors) {
        if (abstractContributor instanceof ContributorDTO) {
          ContributorDTO contributorDTO = (ContributorDTO) abstractContributor;
          String role = contributorDTO.getRole();
          if (!allowedContributorRoleValues.contains(role)) {
            errors.addError(
                    new FieldError(publication.getClass().getSimpleName(), FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".role",
                            this.messageService.get(ERROR_CONTRIBUTOR_INVALID_ROLE, new Object[] { role, publicationSubtype.getName() })));
          }
        }
        index++;
      }
    }
  }

  protected void validateResearchGroups(Publication publication, Object depositDocObj, BindingResult errors) {
    List<ResearchGroup> researchGroups = this.metadataExtractor.getResearchGroups(depositDocObj);

    int index = 0;
    for (ResearchGroup metadataResearchGroup : researchGroups) {
      String fieldName = FORM_STEP_CONTRIBUTORS_PREFIX + "groups." + index++;
      this.checkStringFieldIsNotEmpty(publication, metadataResearchGroup.getResId(), fieldName, errors);
      if (!this.researchGroupService.existsById(metadataResearchGroup.getResId())) {
        // research group id doesn't exist
        errors.addError(new FieldError(publication.getClass().getSimpleName(), fieldName,
                this.messageService.get(ERROR_RESEARCH_GROUP_INVALID_ID, new Object[] { metadataResearchGroup.getResId() })));
      } else if (publication.getMetadataValidationType() == Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION) {
        // for the publication to be validated, its eventual research groups must be validated
        ResearchGroup existingResearchGroup = this.researchGroupService.findOne(metadataResearchGroup.getResId());
        if (Boolean.FALSE.equals(existingResearchGroup.isValidated())) {
          errors.addError(new FieldError(publication.getClass().getSimpleName(), fieldName, this.messageService
                  .get(ERROR_RESEARCH_GROUP_NOT_VALID,
                          new Object[] { metadataResearchGroup.getName() + " (" + metadataResearchGroup.getResId() + ")" })));
        }
      }
    }
  }

  protected void validateStructuresExist(Publication publication, Object depositDocObj, BindingResult errors) {
    List<Structure> structures = this.metadataExtractor.getStructures(depositDocObj);
    for (Structure st : structures) {
      Structure structureFound = this.structureService.findById(st.getResId()).orElse(null);
      if (structureFound == null) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), "structure",
                this.messageService.get(ERROR_STRUCTURE_INVALID_ID, new Object[] { st.getResId() })));
      }
    }
  }

  /**
   * Validate that a selected structure is not implicitly included in another structure
   * <p>
   * e.g: if "Département de biologie moléculaire" is selected, which corresponds to "Faculté des sciences; Section de biologie; Département de biologie moléculaire"
   * then "Section de biologie" cannot be selected as it is already implicitely included
   *
   * @param publication
   * @param depositDocObj
   * @param errors
   */
  protected void validateStructuresHierarchyDuplicates(Publication publication, Object depositDocObj, BindingResult errors) {
    List<Structure> structures = this.metadataExtractor.getStructures(depositDocObj);
    List<String> leafStructureIds = structures.stream().map(Structure::getResId).collect(Collectors.toList());
    List<String> duplicatedStructureIds = this.structureService.getDuplicatedStructureIdsInHierarchy(leafStructureIds);
    duplicatedStructureIds.forEach(leafStructureId -> {
      Structure duplicatedStructure = this.structureService.findOne(leafStructureId);
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_CONTRIBUTORS_PREFIX + "academicStructures",
              this.messageService.get(ERROR_STRUCTURE_INCLUDED_IN_ANOTHER_ONE, new Object[] { duplicatedStructure.getName() })));
    });
  }

  protected void validateFundings(Publication publication, Object depositDocObj, BindingResult errors) {
    final List<MetadataFunding> fundings = this.metadataExtractor.getFundings(depositDocObj);
    IntStream.range(0, fundings.size()).forEach(index -> {
      this.checkStringFieldIsNotEmpty(publication, fundings.get(index).getFunder(),
              FORM_STEP_CONTRIBUTORS_PREFIX + "fundings." + index + ".funder", errors);
    });
  }

  protected void validateCorrections(Publication publication, Object depositDocObj, BindingResult errors) {
    List<MetadataCorrection> metadataCorrections = this.metadataExtractor.getCorrections(depositDocObj);
    int index = 0;
    for (MetadataCorrection metadataCorrection : metadataCorrections) {
      if (!StringTool.isNullOrEmpty(metadataCorrection.getDoi()) || !StringTool.isNullOrEmpty(metadataCorrection.getPmid())) {
        this.checkStringFieldIsNotEmpty(publication, metadataCorrection.getNote(),
                FORM_STEP_DESCRIPTION_PREFIX + "corrections." + index + ".note", errors);
      }
      index++;
    }
  }

  protected void validateISBN(Publication publication, Object depositDocObj, BindingResult errors) {
    String isbn = this.metadataExtractor.getISBN(depositDocObj);
    if (!StringTool.isNullOrEmpty(isbn)) {
      if (!ValidationTool.isValidISBN(isbn)) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.isbn",
                this.messageService.get(ERROR_INVALID_ISBN, new Object[] { isbn })));
      }
      if (this.checkForDuplicates && publication.getSubtype().getName().equals(AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME)
              && this.duplicateService.bookAlreadyUsedByAnotherPublication(isbn, publication.getResId())) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.isbn",
                this.messageService.get(ERROR_ISBN_ALREADY_EXISTS, new Object[] { isbn })));
      }
    }
  }

  protected void validateArxivId(Publication publication, Object depositDocObj, BindingResult errors) {
    String arxivId = this.metadataExtractor.getArxivId(depositDocObj);
    if (!StringTool.isNullOrEmpty(arxivId)) {
      if (!ValidationTool.isValidArxivId(arxivId)) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.arxivid",
                this.messageService.get(ERROR_INVALID_ARXIV_ID, new Object[] { arxivId })));
      }
      if (this.checkForDuplicates && this.duplicateService.arxivIdAlreadyUsedByAnotherPublication(arxivId, publication.getResId())) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.arxivid",
                this.messageService.get(ERROR_ARXIV_ID_ALREADY_EXISTS, new Object[] { arxivId })));
      }
    }
  }

  protected void validateURN(Publication publication, Object depositDocObj, BindingResult errors) {
    String urn = this.metadataExtractor.getURN(depositDocObj);
    if (!StringTool.isNullOrEmpty(urn) && !ValidationTool.isValidURN(urn)) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.urn",
              this.messageService.get(ERROR_INVALID_URN, new Object[] { urn })));
    }
  }

  protected void validateDates(Publication publication, Object depositDocObj, BindingResult errors, boolean isFirstStep) {
    String subtypeName = this.metadataExtractor.getSubtype(depositDocObj);
    if (AouConstants.DEPOSIT_SUBTYPE_THESE_NAME.equals(subtypeName) || !isFirstStep) {
      List<MetadataDates> dates = this.metadataExtractor.getDates(depositDocObj);
      int index = 0;
      boolean hasOneDate = false;
      for (MetadataDates date : dates) {
        if (!StringTool.isNullOrEmpty(date.getContent())) {
          hasOneDate = true;
          if (!ValidationTool.isValidDate(date.getContent(), this.metadataDateFormat, true)) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "dates." + date.getType(),
                    this.messageService.get(ERROR_INVALID_DATE)));
          }
          if (date.getType().equals(AouConstants.DATE_TYPE_FIRST_ONLINE) || date.getType().equals(AouConstants.DATE_TYPE_DEFENSE)) {
            if (!ValidationTool.isValidDate(date.getContent(), this.metadataDateFormat, false)) {
              errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "dates." + date.getType(),
                      this.messageService.get(ERROR_DATE_IN_THE_FUTURE)));
            }
          }
        }
        index++;
      }

      if (!hasOneDate) {
        if (!dates.isEmpty()) {
          for (MetadataDates date : dates) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "dates." + date.getType(),
                    this.messageService.get(ERROR_DATE_MANDATORY)));
          }
        } else {
          errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "dates",
                  this.messageService.get(ERROR_DATE_MANDATORY)));
        }
      }
    }
  }

  protected void validateAbstracts(Publication publication, Object depositDocObj, BindingResult errors) {
    List<MetadataTextLang> abstracts = this.metadataExtractor.getAbstracts(depositDocObj);
    List<String> languages = new ArrayList<>(); // this list is used to verify if there is a duplicate language

    // At least one abstract is mandatory for diploma type
    if (abstracts.isEmpty() && publication.getSubtype().getType().getResId().equals(AouConstants.DEPOSIT_TYPE_DIPLOME_ID)) {
      String langFieldName = FORM_STEP_DESCRIPTION_PREFIX + "abstracts.0.lang";
      String textFieldName = FORM_STEP_DESCRIPTION_PREFIX + "abstracts.0.text";

      errors.addError(new FieldError(publication.getClass().getSimpleName(), langFieldName,
              this.messageService.get(ERROR_ABSTRACT_MANDATORY_FOR_DIPLOMA)));
      errors.addError(new FieldError(publication.getClass().getSimpleName(), textFieldName,
              this.messageService.get(ERROR_ABSTRACT_MANDATORY_FOR_DIPLOMA)));
    }

    IntStream.range(0, abstracts.size()).forEach(index -> {
      String langFieldName = FORM_STEP_DESCRIPTION_PREFIX + "abstracts." + index + ".lang";
      String textFieldName = FORM_STEP_DESCRIPTION_PREFIX + "abstracts." + index + ".text";

      this.checkStringFieldIsNotEmpty(publication, abstracts.get(index).getContent(), textFieldName, errors);

      //Check that size of the abstract that not exceed the maximum size
      if (!StringTool.isNullOrEmpty(abstracts.get(index).getContent()) && abstracts.get(index).getContent().length() > this.maxSizeForAbstract) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(),
                textFieldName, this.messageService.get(ERROR_ABSTRACT_MAXIMUM_SIZE_EXCEEDED, new Object[] { this.maxSizeForAbstract })));
      }

      if (abstracts.size() > 1 && StringTool.isNullOrEmpty(abstracts.get(index).getLang())) {
        this.checkStringFieldIsNotEmpty(publication, abstracts.get(index).getLang(), langFieldName, errors);
      } else {
        languages.add(abstracts.get(index).getLang()); // init the language list only if there is a valid value
      }

      // if many abstracts are given, they must have a language and languages must be different
      if (index > 0) {
        // check that lang is not empty
        this.checkStringFieldIsNotEmpty(publication, abstracts.get(index).getLang(), langFieldName, errors);
        // check that lang doesn't exist twice
        if (this.hasDuplicate(languages)) {
          errors.addError(new FieldError(publication.getClass().getSimpleName(), langFieldName,
                  this.messageService.get(ERROR_LANGUAGE_APPEARS_MORE_THAN_ONCE)));
        }
      }
    });

  }

  protected void validateLinks(Publication publication, Object depositDocObj, BindingResult errors) {
    List<MetadataLink> links = this.metadataExtractor.getLinks(depositDocObj);
    IntStream.range(0, links.size()).forEach(index -> {
      String targetField = FORM_STEP_DESCRIPTION_PREFIX + "links." + index + ".target";
      String typeField = FORM_STEP_DESCRIPTION_PREFIX + "links." + index + ".type";
      this.checkStringFieldIsNotEmpty(publication, links.get(index).getTarget(), targetField, errors);
      if (links.get(index).getType() == null) {
        errors.addError(
                new FieldError(publication.getClass().getSimpleName(), typeField, this.messageService.get(ERROR_FIELD_CANNOT_BE_EMPTY)));
      }
    });
  }

  protected void validateCollection(Publication publication, Object depositDocObj, BindingResult errors) {
    List<MetadataCollection> collections = this.metadataExtractor.getCollections(depositDocObj);
    IntStream.range(0, collections.size()).forEach(element -> {
      String nameField = FORM_STEP_DESCRIPTION_PREFIX + "collections." + element + ".name";
      this.checkStringFieldIsNotEmpty(publication, collections.get(element).getName(), nameField, errors);
    });
  }

  protected void validateLanguage(Publication publication, Object depositDocObj, BindingResult errors) {
    List<String> languages = this.metadataExtractor.getLanguages(depositDocObj);

    if (languages.size() > 1) {
      List<String> duplicateLangList = new ArrayList<>(); // this list is used to verify if there is a duplicate language
      IntStream.range(0, languages.size()).forEach(index -> {
        String langField = FORM_STEP_DESCRIPTION_PREFIX + "languages." + index + ".language";
        if (languages.size() > 1 && StringTool.isNullOrEmpty(languages.get(index))) {
          this.checkStringFieldIsNotEmpty(publication, languages.get(index), langField, errors);
        } else {
          duplicateLangList.add(languages.get(index));
        }
        // if many languages are given, they must must be different
        if (index > 0) {
          // check that lang is not empty
          this.checkStringFieldIsNotEmpty(publication, languages.get(index), langField, errors);
          // check that lang doesn't exist twice
          if (this.hasDuplicate(duplicateLangList)) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(), langField,
                    this.messageService.get(ERROR_LANGUAGE_APPEARS_MORE_THAN_ONCE)));
          }
        }
      });
    } else if (languages.isEmpty() || StringTool.isNullOrEmpty(languages.get(0))) {
      // validate there is at least one language
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "languages.0.language",
              this.messageService.get(ERROR_FIELD_CANNOT_BE_EMPTY)));
    }
  }

  protected void validateFilesNotMissing(Publication publication, Object depositDocObj, BindingResult errors) {
    Publication.PublicationStatus status = publication.getStatus();
    List<MetadataFile> files = this.metadataExtractor.getFiles(depositDocObj);
    boolean hasFile = files.stream().anyMatch(f -> !AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE.equals(f.getType()));

    boolean hasFileBeforeUpdate = false;
    if (!StringTool.isNullOrEmpty(publication.getMetadataBackup())) {
      Object depositDocObjBefore = this.metadataExtractor.createDepositDocObjectFromXml(publication.getMetadataBackup());
      List<MetadataFile> filesBeforeUpdate = this.metadataExtractor.getFiles(depositDocObjBefore);
      hasFileBeforeUpdate = filesBeforeUpdate.stream().anyMatch(f -> !AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE.equals(f.getType()));
    }

    if ((status == Publication.PublicationStatus.IN_PROGRESS ||
            (status == Publication.PublicationStatus.FEEDBACK_REQUIRED && !this.historyService.hasBeenCompleted(publication.getResId()))
    ) && !hasFile) {
      // Publication is being created --> a file is required for new publications
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_FILES_PREFIX + "files",
              this.messageService.get(ERROR_FILES_EMPTY)));
    } else if ((status == Publication.PublicationStatus.IN_EDITION ||
            (status == Publication.PublicationStatus.FEEDBACK_REQUIRED && this.historyService.hasBeenCompleted(publication.getResId()))
    ) && !hasFile && hasFileBeforeUpdate) {
      // Publication is being updated --> publications without any file can be updated, but only if there was already no file before the update
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_FILES_PREFIX + "files",
              this.messageService.get(ERROR_FILES_EMPTY_AFTER_UPDATE)));
    }
  }

  private void validateDatasets(Publication publication, Object depositDocObj, BindingResult errors) {
    List<String> datasets = this.metadataExtractor.getDatasets(depositDocObj);
    IntStream.range(0, datasets.size()).forEach(index -> {
      String urlField = FORM_STEP_DESCRIPTION_PREFIX + "datasets." + index + ".url";
      this.checkStringFieldIsNotEmpty(publication, datasets.get(index), urlField, errors);
    });
  }

  private void validateUnigeContributorsName(Publication publication, Object depositDocObj, BindingResult errors) {
    List<AbstractContributor> contributors = this.metadataExtractor.getContributors(depositDocObj);
    String contributorPrefix = "contributors.";
    IntStream.range(0, contributors.size()).forEach(index -> {
      if (contributors.get(index) instanceof ContributorDTO) {
        ContributorDTO contributorDTO = (ContributorDTO) contributors.get(index);
        String cnIndividu = contributorDTO.getCnIndividu();
        if (!StringTool.isNullOrEmpty(cnIndividu)) {
          Optional<UnigePersonDTO> unigeContributor = this.unigePersonSearchService.searchByCnIndividu(cnIndividu);
          if (unigeContributor.isPresent()) {

            if (!unigeContributor.get().getLastname().equals(contributorDTO.getLastName())
                    && !Objects.equals(contributorDTO.getLastName(), this.getContributorOtherLastname(cnIndividu, publication))) {
              errors.addError(new FieldError(publication.getClass().getSimpleName(),
                      FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".lastname",
                      this.messageService.get(ERROR_CONTRIBUTOR_LASTNAME_CHANGED, new Object[] { contributorDTO.getLastName() })));
            }

            if (!unigeContributor.get().getFirstname().equals(contributorDTO.getFirstName())
                    && !Objects.equals(contributorDTO.getFirstName(), this.getContributorOtherFirstname(cnIndividu, publication))) {
              errors.addError(new FieldError(publication.getClass().getSimpleName(),
                      FORM_STEP_CONTRIBUTORS_PREFIX + contributorPrefix + index + ".firstname",
                      this.messageService.get(ERROR_CONTRIBUTOR_FIRSTNAME_CHANGED, new Object[] { contributorDTO.getFirstName() })));
            }
          }
        }
      }
    });
  }

  private String getContributorOtherFirstname(String cnIndividu, Publication publication) {
    if (!publication.getContributorOtherNames().isEmpty()) {
      for (ContributorOtherName contributorOtherName : publication.getContributorOtherNames()) {
        if (contributorOtherName.getContributor().getCnIndividu().equals(cnIndividu)) {
          return contributorOtherName.getFirstName();
        }
      }
    }
    return null;
  }

  private String getContributorOtherLastname(String cnIndividu, Publication publication) {
    if (!publication.getContributorOtherNames().isEmpty()) {
      for (ContributorOtherName contributorOtherName : publication.getContributorOtherNames()) {
        if (contributorOtherName.getContributor().getCnIndividu().equals(cnIndividu)) {
          return contributorOtherName.getLastName();
        }
      }
    }
    return null;
  }

  /******************************************************************************************************************/

  private void checkStringFieldIsNotEmpty(Publication publication, String value, String fieldName, BindingResult errors) {
    if (StringTool.isNullOrEmpty(value)) {
      errors.addError(
              new FieldError(publication.getClass().getSimpleName(), fieldName, this.messageService.get(ERROR_FIELD_CANNOT_BE_EMPTY)));
    }
  }

  private <T> boolean hasDuplicate(Iterable<T> all) {  //TODO :  this code should probably be in solidify
    Set<T> set = new HashSet<>();
    // Set#add returns false if the set does not change, which
    // indicates that a duplicate element has been added.
    for (T each : all)
      if (!set.add(each))
        return true;
    return false;
  }

  /**
   * Return the first metadata structure linked to the publication (or one of its parents) requiring a 'Mode de publication' document file
   *
   * @param publication
   * @return
   */
  public Structure getFirstStructureRequiringPublicationMode(Publication publication) {
    List<String> structureIdsRequiringPublicationMode = this.getStructureIdsRequiringPublicationMode(publication);
    if (!structureIdsRequiringPublicationMode.isEmpty()) {
      return this.structureService.findOne(structureIdsRequiringPublicationMode.get(0));
    }
    return null;
  }

  private List<String> getStructureIdsRequiringPublicationMode(Publication publication) {
    // get all structures linked to the publication
    List<PublicationStructure> publicationStructures = publication.getPublicationStructures();

    // compare only structures that are METADATA structures
    List<PublicationStructure> metadataPublicationStructures = publicationStructures.stream()
            .filter(ps -> ps.getLinkType().equals(PublicationStructure.LinkType.METADATA)).collect(Collectors.toList());

    List<String> matchingStructureIds = new ArrayList<>();
    for (PublicationStructure publicationStructure : metadataPublicationStructures) {
      // get all parent structure ids of the structure linked to the publication
      List<String> ancestorStructureIds = this.structureService.getAncestorStructureIds(publicationStructure.getStructure().getResId());
      // add the structure id itself to check it as well
      ancestorStructureIds.add(publicationStructure.getStructure().getResId());

      // check if one of them require publication mode
      List<String> matchingIds = ancestorStructureIds.stream()
              .filter(this.structuresRequiringPublicationMode::contains)
              .collect(Collectors.toList());
      matchingStructureIds.addAll(matchingIds);
    }
    return matchingStructureIds;
  }
}
