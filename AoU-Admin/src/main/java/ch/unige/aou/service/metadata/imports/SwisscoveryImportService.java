/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - SwisscoveryImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class SwisscoveryImportService extends MetadataImportService {

  private final String apiUrl;

  private static final String XPATH_ERROR_SWISSCOVERY = "/searchRetrieveResponse[./numberOfRecords/text()='0']";
  private static final String XPATH_SWISSCOVERY = "/searchRetrieveResponse/records/record[./recordPosition/text()='1']//recordData/record";

  public SwisscoveryImportService(AouProperties aouProperties, MessageService messageService, MetadataService metadataService,
          ContributorService contributorService, JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService) {
    super(aouProperties, messageService, metadataService, contributorService, journalTitleRemoteService, licenseService);
    this.apiUrl = aouProperties.getMetadata().getImports().getIsbn().getSwisscoveryUrl();
  }

  public DepositDoc searchIsbn(String isbn, DepositDoc depositDoc) {
    try {
      Document swisscoveryDoc = this.getXmlFromSwisscovery(isbn);
      this.fillDepositDoc(isbn, depositDoc, swisscoveryDoc);
      return depositDoc;
    } catch (final XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching ISBN '" + isbn + "' on Swisscovery", e);
    }
  }

  public void fillDepositDoc(String isbn, DepositDoc depositDoc, Document swisscoveryDoc) throws XPathExpressionException {

    final XPath xpath = XMLTool.buildXPath();

    Node errorNode = (Node) xpath.compile(SwisscoveryImportService.XPATH_ERROR_SWISSCOVERY).evaluate(swisscoveryDoc, XPathConstants.NODE);
    if (errorNode != null) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
              this.messageService.get("deposit.error.identifiers.isbn.not_found_by_swisscovery", new Object[] { isbn }));
    }

    depositDoc.setType(AouConstants.DEPOSIT_TYPE_LIVRE_ID);
    depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME);

    this.fillTitle(XPATH_SWISSCOVERY + "/datafield[@tag='245']/subfield[@code='a']",
            XPATH_SWISSCOVERY + "/datafield[@tag='245']/subfield[@code='b']", depositDoc, swisscoveryDoc);
    this.fillPublicationYear(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='264']/subfield[@code='c']");
    this.fillPaging(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='300']/subfield[@code='a']");
    this.fillNote(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='500']/subfield[@code='a']");
    this.fillPublisherPlace(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='264']/subfield[@code='a']");
    this.fillPublisherName(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='264']/subfield[@code='b']");
    this.fillLanguage(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/controlfield[@tag='008']");
    this.fillAbstracts(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='520']/subfield[@code='a']", null);
    this.fillKeywords(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='650']/subfield[@code='a']");
    this.fillIssn(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='022']/subfield[@code='a']");
    this.fillIsbn(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='020']/subfield[@code='a']");
    this.fillCommercialUrl(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='856']/subfield[@code='u']");
    this.fillContributors(depositDoc, swisscoveryDoc, XPATH_SWISSCOVERY + "/datafield[@tag='700']/subfield[@code='a']",
            XPATH_SWISSCOVERY + "/datafield[@tag='100']/subfield[@code='a']");

    // if the ISBN fetched from metadata is different from the one used to retrieve metadata, replace it
    this.ensureDepositIdentifiersExist(depositDoc);
    if (StringTool.isNullOrEmpty(depositDoc.getIdentifiers().getIsbn()) || !depositDoc.getIdentifiers().getIsbn().equals(isbn)) {
      depositDoc.getIdentifiers().setIsbn(isbn);
    }
  }

  /****************************************************************************************/

  private Document getXmlFromSwisscovery(String isbn) {
    if (!StringTool.isNullOrEmpty(this.apiUrl)) {
      try {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

        Map<String, String> parameters = new HashMap<>();
        parameters.put("id", isbn);

        final String xmlString = client.getForObject(this.apiUrl, String.class, parameters);
        return this.parseXML(xmlString);
      } catch (HttpClientErrorException e) {
        throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
                this.messageService.get("deposit.error.identifiers.isbn.not_found_by_swisscovery", new Object[] { isbn }));
      } catch (IOException | ParserConfigurationException | SAXException e) {
        throw new SolidifyRuntimeException("An error occurred when searching ISBN '" + isbn + "' on Swisscovery", e);
      }
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  /****************************************************************************************/

  protected void fillPublicationYear(DepositDoc depositDoc, Document doc, String xpathQueryYear) {
    String year = XMLTool.getFirstTextContent(doc, xpathQueryYear);
    if (!StringTool.isNullOrEmpty(year)) {
      this.ensureDatesExist(depositDoc);
      depositDoc.getDates().getDate().add(this.getDateWithType(DateTypes.PUBLICATION, year));
    }
  }

  protected void fillTitle(String xpathQuery1, String xpathQuery2, DepositDoc depositDoc, Document swisscoveryDoc) {
    String title = XMLTool.getFirstTextContent(swisscoveryDoc, xpathQuery1, Collections.emptyMap());
    String subTitle = XMLTool.getFirstTextContent(swisscoveryDoc, xpathQuery2, Collections.emptyMap());
    if (!StringTool.isNullOrEmpty(title)) {
      if (!StringTool.isNullOrEmpty(subTitle)) {
        depositDoc.setTitle(this.getText(title + " : " + subTitle));
      } else {
        depositDoc.setTitle(this.getText(title));
      }
    }
  }

  private void fillPaging(DepositDoc depositDoc, Document swisscoveryDoc, String xpathQueryYear) {
    String paging = XMLTool.getFirstTextContent(swisscoveryDoc, xpathQueryYear);
    if (!StringTool.isNullOrEmpty(paging)) {
      this.ensurePagesExist(depositDoc);
      this.setPagingOrOther(depositDoc, paging);
    }
  }

  private void fillLanguage(DepositDoc depositDoc, Document swisscoveryDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(swisscoveryDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      //Language are in the characters at position 35-37
      String language = value.substring(35, 38);
      this.ensureLanguagesExist(depositDoc);
      String languageCode = CleanTool.getLanguageCode(language);
      depositDoc.getLanguages().getLanguage().add(languageCode);
      if (depositDoc.getTitle() != null) {
        depositDoc.getTitle().setLang(languageCode);
      }
    }
  }

  private void fillContributors(DepositDoc depositDoc, Document swisscoveryDoc, String xpathQuery, String xpathQueryFirstAuthor) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      //The first author is not always present
      Node firstAuthor = (Node) xpath.compile(xpathQueryFirstAuthor).evaluate(swisscoveryDoc, XPathConstants.NODE);
      if (firstAuthor != null) {
        this.ensureContributorsExist(depositDoc);
        Contributor contributor = this.createContributor(firstAuthor);
        depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
      }

      NodeList authorsList = (NodeList) xpath.compile(xpathQuery).evaluate(swisscoveryDoc, XPathConstants.NODESET);

      if (authorsList.getLength() > 0) {
        this.ensureContributorsExist(depositDoc);
      }
      for (int i = 0; i < this.getNumberMaxContributor(authorsList.getLength()); i++) {
        Contributor contributor = this.createContributor(authorsList.item(i));
        depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
      }
    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill contributors", e);
    }
  }

  private Contributor createContributor(Node authorNode) {
    String authorName = authorNode.getTextContent();
    // Cut the author name on the last comma, assuming the last word is the firstname
    String firstName = null;
    String lastName = null;
    if (!StringTool.isNullOrEmpty(authorName)) {
      authorName = authorName.trim();
      int lastCommaIndex = authorName.lastIndexOf(',');
      if (lastCommaIndex > -1) {
        lastName = authorName.substring(0, lastCommaIndex);
        firstName = authorName.substring(lastCommaIndex + 1).trim();
      } else {
        lastName = authorName;
      }
    }

    Contributor contributor = new Contributor();
    contributor.setLastname(CleanTool.capitalizeString(lastName));
    contributor.setFirstname(CleanTool.capitalizeString(firstName));
    return contributor;
  }
}
