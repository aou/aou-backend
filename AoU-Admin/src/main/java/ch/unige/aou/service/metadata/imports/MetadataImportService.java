/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.rest.JournalTitleDTO;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.ObjectFactory;
import ch.unige.aou.model.xml.deposit.v2_4.Text;
import ch.unige.aou.model.xml.deposit.v2_4.TextLang;
import ch.unige.aou.service.AouService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

public abstract class MetadataImportService extends AouService {

  private static final List<String> ORCID_PREFIXES = List.of("https://orcid.org/", "http://orcid.org/");

  public static final String UNDEFINED_NAME_VALUE = "???";

  protected MetadataService metadataService;
  protected ContributorService contributorService;
  protected JournalTitleRemoteService journalTitleRemoteService;
  protected String tempFolder;
  protected LicenseService licenseService;

  protected int maxContributorsLimit;

  protected ObjectFactory factory = new ObjectFactory();

  protected MetadataImportService(AouProperties aouProperties, MessageService messageService, MetadataService metadataService,
          ContributorService contributorService, JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService) {
    super(messageService);

    this.metadataService = metadataService;
    this.contributorService = contributorService;
    this.journalTitleRemoteService = journalTitleRemoteService;
    this.licenseService = licenseService;

    this.tempFolder = aouProperties.getTempLocation(aouProperties.getPublicationsFolder());
    this.maxContributorsLimit = aouProperties.getMetadata().getImports().getLimitMaxContributors();
  }

  protected String transformDepositDocToFormData(DepositDoc depositDoc) {
    MetadataExtractor extractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
    this.verifyISSNInfo(depositDoc);
    extractor.setDefaultContributorsRoles(depositDoc);
    extractor.replaceTextValuesByIds(depositDoc);
    String xml = extractor.serializeDepositDocToXml(depositDoc);
    return extractor.transformMetadataToFormData(xml);
  }

  protected String transformDepositDocToMetadata(DepositDoc depositDoc) {
    MetadataExtractor extractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
    this.verifyISSNInfo(depositDoc);
    extractor.setDefaultContributorsRoles(depositDoc);
    extractor.replaceIdsByTextValues(depositDoc);
    return extractor.serializeDepositDocToXml(depositDoc);
  }

  public void verifyISSNInfo(DepositDoc depositDoc) {
    if (depositDoc.getIdentifiers() != null && !StringTool.isNullOrEmpty(depositDoc.getIdentifiers().getIssn())) {
      this.importInformationFromISSN(depositDoc);
    }
  }

  public void importInformationFromISSN(DepositDoc depositDoc) {
    String issn = depositDoc.getIdentifiers().getIssn();
    RestCollection<JournalTitleDTO> journalCollection = this.journalTitleRemoteService.getJournalTitlesByISSN(issn);
    if (journalCollection.getData() != null && journalCollection.getData().iterator().hasNext()) {
      //replace the journal title in metadata by the mainTitle in ISSN values

      final JournalTitleDTO journalTitleDTO = journalCollection.getData().iterator().next();
      depositDoc.getContainer().setTitle(journalTitleDTO.getMainTitle());
      if (!StringTool.isNullOrEmpty(journalTitleDTO.getIssnl())) {
        depositDoc.getIdentifiers().setIssn(journalTitleDTO.getIssnl());
      }
    }
  }

  public Document parseXML(String xmlString) throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
    documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

    documentBuilderFactory.setExpandEntityReferences(false);

    return documentBuilderFactory.newDocumentBuilder()
            .parse(new InputSource(new ByteArrayInputStream(xmlString.getBytes(StandardCharsets.UTF_8))));
  }

  /****************************************************************************************/

  protected boolean isBigInteger(String value) {
    if (StringTool.isNullOrEmpty(value)) {
      return false;
    }

    try {
      new BigInteger(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  protected int getNumberMaxContributor(int contributorLength) {
    return Math.min(contributorLength, this.maxContributorsLimit);
  }

  protected String getDateString(String year, String month, String day) {
    String dateStr = null;
    if (!StringTool.isNullOrEmpty(year)) {
      dateStr = year;
      if (!StringTool.isNullOrEmpty(month)) {
        if (month.matches("\\d+")) {
          dateStr += "-" + this.get2DigitsNumber(month);
        } else {
          dateStr += "-" + this.getMonthNumber(month);
        }
        if (!StringTool.isNullOrEmpty(day)) {
          dateStr += "-" + this.get2DigitsNumber(day);
        }
      }
    }
    return dateStr;
  }

  private String getMonthNumber(String month) {
    DateTimeFormatter parser = DateTimeFormatter.ofPattern("MMM").withLocale(Locale.ENGLISH);
    TemporalAccessor accessor = parser.parse(month);
    String monthNumber = String.valueOf(accessor.get(ChronoField.MONTH_OF_YEAR));
    return this.get2DigitsNumber(monthNumber);
  }

  protected String get2DigitsNumber(String numberStr) {
    if (!StringTool.isNullOrEmpty(numberStr)) {
      try {
        NumberFormat formatter = new DecimalFormat("00");
        int number = Integer.parseInt(numberStr);
        return formatter.format(number);
      } catch (NumberFormatException e) {
        return numberStr;
      }
    }
    return numberStr;
  }

  protected String getDatesRange(String startYear, String startMonth, String startDay, String endYear, String endMonth, String endDay) {
    String startDateStr = this.getDateString(startYear, startMonth, startDay);
    String endDateStr = this.getDateString(endYear, endMonth, endDay);

    String datesRange = "";
    if (!StringTool.isNullOrEmpty(startDateStr)) {
      datesRange += startDateStr;
    }
    if (!StringTool.isNullOrEmpty(endDateStr) && !endDateStr.equals(startDateStr)) {
      datesRange += " - " + endDateStr;
      datesRange = datesRange.trim();
    }

    return datesRange;
  }

  protected TextLang getTextLang(String content) {
    TextLang text = new TextLang();
    text.setContent(content);
    return text;
  }

  protected TextLang getTextLang(String content, String language) {
    TextLang text = new TextLang();
    text.setContent(content);
    text.setLang(language);
    return text;
  }

  protected Text getText(String content) {
    Text text = new Text();
    text.setContent(content);
    return text;
  }

  protected Text getText(String content, String language) {
    Text text = new Text();
    text.setContent(content);
    text.setLang(language);
    return text;
  }

  protected DateWithType getDateWithType(DateTypes dateType, String content) {
    DateWithType dateWithType = new DateWithType();
    dateWithType.setType(dateType);
    dateWithType.setContent(content);
    return dateWithType;
  }

  protected String cleanOrcid(String orcid) {
    if (!StringTool.isNullOrEmpty(orcid)) {
      for (String orcidPrefix : MetadataImportService.ORCID_PREFIXES) {
        orcid = orcid.replace(orcidPrefix, "");
      }
      if (ValidationTool.isValidOrcid(orcid)) {
        return orcid;
      }
    }
    // if empty or invalid return null
    return null;
  }

  protected String getNameOrUndefinedValue(String name) {
    if (name != null) {
      name = name.trim();
      if (!StringTool.isNullOrEmpty(name)) {
        return name;
      }
    }

    return MetadataImportService.UNDEFINED_NAME_VALUE;
  }

  protected String cleanText(String value) {
    return value.replaceAll("\\s+", " ");
  }

  protected String getStringOrNull(JSONObject jsonObject, String propertyName) {
    if (jsonObject != null && jsonObject.has(propertyName) && !jsonObject.isNull(propertyName)) {
      return jsonObject.getString(propertyName);
    }
    return null;
  }

  protected JSONArray getArrayOrNull(JSONObject jsonObject, String propertyName) {
    try {
      if (jsonObject != null && jsonObject.has(propertyName) && !jsonObject.isNull(propertyName)) {
        return jsonObject.getJSONArray(propertyName);
      }
    } catch (JSONException e) {
      return null;
    }
    return null;
  }

  /****************************************************************************************/

  protected void fillPublicationDate(DepositDoc depositDoc, Document doc, String xpathQueryYear, String xpathQueryMonth,
          String xpathQueryDay) {
    this.fillDate(depositDoc, doc, xpathQueryYear, xpathQueryMonth, xpathQueryDay, DateTypes.PUBLICATION);
  }

  protected void fillFirstOnlineDate(DepositDoc depositDoc, Document doc, String xpathQueryYear, String xpathQueryMonth,
          String xpathQueryDay) {
    this.fillDate(depositDoc, doc, xpathQueryYear, xpathQueryMonth, xpathQueryDay, DateTypes.FIRST_ONLINE);
  }

  protected void fillDate(DepositDoc depositDoc, Document doc, String xpathQueryYear, String xpathQueryMonth,
          String xpathQueryDay, DateTypes type) {
    String year = XMLTool.getFirstTextContent(doc, xpathQueryYear);
    String month = XMLTool.getFirstTextContent(doc, xpathQueryMonth);
    String day = XMLTool.getFirstTextContent(doc, xpathQueryDay);

    String dateStr = this.getDateString(year, month, day);
    if (!StringTool.isNullOrEmpty(dateStr)) {
      this.ensureDatesExist(depositDoc);
      depositDoc.getDates().getDate().add(this.getDateWithType(type, dateStr));
    }
  }

  protected void fillContainerTitle(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setTitle(value);
    }
  }

  protected void fillTitle(DepositDoc depositDoc, Document doc, String xpathQuery) {
    this.fillTitle(depositDoc, doc, xpathQuery, Collections.emptyMap());
  }

  protected void fillTitle(DepositDoc depositDoc, Document doc, String xpathQuery, Map<String, String> namespaces) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery, namespaces);
    if (!StringTool.isNullOrEmpty(value)) {
      depositDoc.setTitle(this.getText(value));
    }
  }

  protected void fillIssn(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDepositIdentifiersExist(depositDoc);
      if (ValidationTool.isValidISSN(value)) {
        depositDoc.getIdentifiers().setIssn(value);
      } else {
        depositDoc.getIdentifiers().setIssn(this.addChar(value, '-', 4)); //add hyphen
      }
    }
  }

  protected void fillCommercialUrl(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      depositDoc.setPublisherVersionUrl(value);
    }
  }

  protected void fillIsbn(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDepositIdentifiersExist(depositDoc);
      depositDoc.getIdentifiers().setIsbn(value);
    }
  }

  protected void fillDoi(DepositDoc depositDoc, Document doc, String xpathQuery) {
    this.fillDoi(depositDoc, doc, xpathQuery, Collections.emptyMap());
  }

  protected void fillDoi(DepositDoc depositDoc, Document doc, String xpathQuery, Map<String, String> namespaces) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery, namespaces);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDepositIdentifiersExist(depositDoc);
      depositDoc.getIdentifiers().setDoi(value);
    }
  }

  protected void fillNote(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      depositDoc.setNote(value);
    }
  }

  protected void fillPublisherName(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensurePublisherExist(depositDoc);
      depositDoc.getPublisher().getNameOrPlace().add(this.factory.createPublisherName(value));
    }
  }

  protected void fillPublisherPlace(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensurePublisherExist(depositDoc);
      depositDoc.getPublisher().getNameOrPlace().add(this.factory.createPublisherPlace(value));
    }
  }

  protected void fillAbstracts(DepositDoc depositDoc, Document doc, String xpathQuery, String xpathLangQuery) {
    String value = XMLTool.getFirstNodeInnerRawContent(doc, xpathQuery);
    String language = null;
    if (xpathLangQuery != null) {
      language = XMLTool.getFirstTextContent(doc, xpathLangQuery);
    }
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureAbstractsExist(depositDoc);
      Text text = this.getText(value);
      if (!StringTool.isNullOrEmpty(language)) {
        text.setLang(CleanTool.getLanguageCode(language));
      }
      depositDoc.getAbstracts().getAbstract().add(text);
    }
  }

  protected void fillKeywords(DepositDoc depositDoc, Document doc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList keywordsList = (NodeList) xpath.compile(xpathQuery).evaluate(doc, XPathConstants.NODESET);

      if (keywordsList.getLength() > 0) {
        this.ensureKeywordsExist(depositDoc);
      }
      for (int i = 0; i < keywordsList.getLength(); i++) {
        String keyword = keywordsList.item(i).getTextContent();
        depositDoc.getKeywords().getKeyword().add(keyword);
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill keywords", e);
    }
  }

  /***g
   * Set value to pages/other,specially if contains hyphen, set the value to pages/paging if value contains 3* characters
   * and not hyphen
   * @param depositDoc
   * @param value
   */
  protected void setPagingOrOther(DepositDoc depositDoc, String value) {
    if (value.contains("-") || value.length() < 3) {
      depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesPaging(value));
    } else {
      depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesOther(value));
    }
  }

  /****************************************************************************************/

  protected void ensureDepositFundingAgencyExist(DepositDoc depositDoc) {
    if (depositDoc.getFundings() == null) {
      depositDoc.setFundings(this.factory.createFundings());
    }
  }

  protected void ensureAbstractsExist(DepositDoc depositDoc) {
    if (depositDoc.getAbstracts() == null) {
      depositDoc.setAbstracts(this.factory.createAbstracts());
    }
  }

  protected void ensureDepositIdentifiersExist(DepositDoc depositDoc) {
    if (depositDoc.getIdentifiers() == null) {
      depositDoc.setIdentifiers(this.factory.createDepositIdentifiers());
    }
  }

  protected void ensureContributorsExist(DepositDoc depositDoc) {
    if (depositDoc.getContributors() == null) {
      depositDoc.setContributors(this.factory.createContributors());
    }
  }

  protected void ensureDatesExist(DepositDoc depositDoc) {
    if (depositDoc.getDates() == null) {
      depositDoc.setDates(this.factory.createDates());
    }
  }

  protected void ensureContainerExist(DepositDoc depositDoc) {
    if (depositDoc.getContainer() == null) {
      depositDoc.setContainer(this.factory.createContainer());
    }
  }

  protected void ensureLanguagesExist(DepositDoc depositDoc) {
    if (depositDoc.getLanguages() == null) {
      depositDoc.setLanguages(this.factory.createLanguages());
    }
  }

  protected void ensureKeywordsExist(DepositDoc depositDoc) {
    if (depositDoc.getKeywords() == null) {
      depositDoc.setKeywords(this.factory.createKeywords());
    }
  }

  protected void ensurePagesExist(DepositDoc depositDoc) {
    if (depositDoc.getPages() == null) {
      depositDoc.setPages(this.factory.createPages());
    }
  }

  protected void ensurePublisherExist(DepositDoc depositDoc) {
    if (depositDoc.getPublisher() == null) {
      depositDoc.setPublisher(this.factory.createPublisher());
    }
  }

  protected void ensureFundingsExist(DepositDoc depositDoc) {
    if (depositDoc.getFundings() == null) {
      depositDoc.setFundings(this.factory.createFundings());
    }
  }

  protected void ensureDepositPagesExist(DepositDoc depositDoc) {
    if (depositDoc.getPages() == null) {
      depositDoc.setPages(this.factory.createPages());
    }
  }

  protected void ensureDepositLinkExist(DepositDoc depositDoc) {
    if (depositDoc.getLinks() == null) {
      depositDoc.setLinks(this.factory.createLinks());
    }
  }

  private String addChar(String str, char ch, int position) {
    return str.substring(0, position) + ch + str.substring(position);
  }

}
