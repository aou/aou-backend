/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - AoUProdResearchGroupSearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.rest.ResearchGroupDTO;

@Service
@ConditionalOnBean(AdminController.class)
public class AoUProdResearchGroupSearchService implements ResearchGroupSearch {

  private String apiUrl;

  private String apiKey;

  public AoUProdResearchGroupSearchService(AouProperties aouProperties) {
    this.apiUrl = aouProperties.getMetadata().getSearch().getResearchGroup().getUrl();
    this.apiKey = aouProperties.getMetadata().getSearch().getResearchGroup().getApiKey();
  }

  @Override
  public List<ResearchGroupDTO> search(LocalDateTime createdOrModifiedSince) {
    if (!StringTool.isNullOrEmpty(this.apiUrl) && !StringTool.isNullOrEmpty(this.apiKey)) {

      try {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

        final String jsonString = client.postForObject(this.apiUrl, this.getRequest(createdOrModifiedSince), String.class);

        final ObjectMapper mapper = new ObjectMapper();
        final JavaType listOfMaps = mapper.getTypeFactory().constructParametricType(List.class, Map.class);
        List<Map<String, String>> results = mapper.readValue(jsonString, listOfMaps);

        List<ResearchGroupDTO> resultList = new ArrayList<>();

        results.stream().forEach(map -> {
          ResearchGroupDTO researchGroupDTO = new ResearchGroupDTO();
          researchGroupDTO.setId(this.getStringOrNull(map, "id"));

          researchGroupDTO.setName(this.getStringOrNull(map, "title"));
          String displayTitle = this.getStringOrNull(map, "display_title");
          if (!StringTool.isNullOrEmpty(displayTitle)) {
            researchGroupDTO.setName(displayTitle);
          }

          researchGroupDTO.setCode(this.getStringOrNull(map, "external_code"));
          researchGroupDTO.setDeleted(Boolean.parseBoolean(this.getStringOrNull(map, "deleted")));
          try {
            String createdStr = this.getStringOrNull(map, "created");
            createdStr = createdStr.replace(" ", "T");
            researchGroupDTO.setCreated(LocalDateTime.parse(createdStr));

            String modifiedStr = this.getStringOrNull(map, "modified");
            if (!StringTool.isNullOrEmpty(modifiedStr)) {
              modifiedStr = modifiedStr.replace(" ", "T");
              researchGroupDTO.setModified(LocalDateTime.parse(modifiedStr));
            } else {
              researchGroupDTO.setModified(researchGroupDTO.getCreated());
            }
          } catch (DateTimeParseException e) {
            // if an error occurs, we consider the source data as old in order to not erase updates made in database
            LocalDateTime lastUpdate = LocalDateTime.of(2000, 1, 1, 0, 0);
            researchGroupDTO.setCreated(lastUpdate);
            researchGroupDTO.setModified(lastUpdate);
          }

          resultList.add(researchGroupDTO);
        });

        return resultList;
      } catch (final IOException e) {
        throw new SolidifyRuntimeException(e.getMessage(), e);
      }
    } else {
      throw new SolidifyRuntimeException("incomplete configuration");
    }
  }

  private HttpEntity<MultiValueMap<String, String>> getRequest(LocalDateTime createdOrModifiedSince) {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringTool.DATE_FORMAT);
    String sinceStr = createdOrModifiedSince.format(formatter);

    MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
    parameters.add("since", sinceStr);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.set("X-API-KEY", this.apiKey);

    return new HttpEntity<>(parameters, headers);
  }

  private String getStringOrNull(Map<String, String> map, String propertyName) {
    Object value = map.getOrDefault(propertyName, "");
    if (value != null) {
      return value.toString();
    }
    return null;
  }
}
