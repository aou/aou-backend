/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - UnigePersonSearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.model.rest.UnigePersonDTO;
import ch.unige.aou.service.AouCacheNames;

public abstract class UnigePersonSearchService implements UnigePersonSearch {

  private final static String NAME_SEPARATOR = "___";

  @Override
  public RestCollection<UnigePersonDTO> search(String firstname, String lastname, Pageable pageable) {

    if (StringTool.isNullOrEmpty(lastname) && StringTool.isNullOrEmpty(firstname)) {
      throw new SolidifyValidationException(new ValidationError("Firstname or lastname must be given to search for a person"));
    }

    RestCollection<UnigePersonDTO> results = this.getResultsFromExternalService(firstname, lastname, pageable);
    this.discardNotNecessaryYearsOfBirth(results);
    return results;
  }

  @Override
  @Cacheable(AouCacheNames.UNIGE_PERSON_CN_INDIVIDU)
  public Optional<UnigePersonDTO> searchByCnIndividu(String cnIndividu) {
    if (!StringTool.isNullOrEmpty(cnIndividu)) {
      Optional<UnigePersonDTO> unigeContributor = this.getResultsFromExternalService(cnIndividu);
      unigeContributor.ifPresent(unigePersonDTO -> unigePersonDTO.setYearOfBirth(null));
      return unigeContributor;
    } else {
      throw new SolidifyRuntimeException("cnIndividu is mandatory to search for a person");
    }
  }

  /**
   * Years of birth may be used to differentiate homonyms. If a person name appears only once in the list,
   * the info is not necessary and is removed from results for privacy considerations.
   *
   * @param results
   */
  protected void discardNotNecessaryYearsOfBirth(RestCollection<UnigePersonDTO> results) {
    List<UnigePersonDTO> unigePersonDTOs = results.getData();
    Map<String, Long> fullnameOccurrences = unigePersonDTOs.stream()
            .collect(groupingBy(unigePersonDTO -> unigePersonDTO.getFirstname() + NAME_SEPARATOR + unigePersonDTO.getLastname(), counting()));

    unigePersonDTOs.forEach(unigePersonDTO -> {
      String fullname = unigePersonDTO.getFirstname() + NAME_SEPARATOR + unigePersonDTO.getLastname();
      if (fullnameOccurrences.get(fullname) == 1) {
        unigePersonDTO.setYearOfBirth(null);
      }
    });
  }

  protected abstract Optional<UnigePersonDTO> getResultsFromExternalService(String cnIndividu);

  protected abstract RestCollection<UnigePersonDTO> getResultsFromExternalService(String firstname, String lastname, Pageable pageable);
}
