/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - EmailJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.EmailMessage;

@Service
@ConditionalOnBean(AdminController.class)
@Profile("email-service")
public class EmailJmsListener extends MessageProcessor<EmailMessage> {

  private static final Logger log = LoggerFactory.getLogger(EmailJmsListener.class);

  private final EmailProcessingService emailProcessingService;

  public EmailJmsListener(AouProperties aouProperties, EmailProcessingService emailProcessingService) {
    super(aouProperties);
    this.emailProcessingService = emailProcessingService;
  }

  @JmsListener(destination = "${aou.queue.emails}")
  @Override
  public void receiveMessage(EmailMessage emailMessage) {
    this.sendForParallelProcessing(emailMessage);
  }

  @Override
  public void processMessage(EmailMessage emailMessage) {
    log.info("Reading message {}", emailMessage);
    this.emailProcessingService.processMessage(emailMessage);
    log.info("Message {} processed", emailMessage);
  }
}
