/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataValidatorV1.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.util.List;
import java.util.stream.IntStream;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.service.rest.UnigePersonSearchService;

@Service
@ConditionalOnBean(AdminController.class)
public class MetadataValidatorV1 extends MetadataValidatorService {

  public MetadataValidatorV1(AouProperties aouProperties, MetadataExtractorV1 metadataExtractor, MessageService messageService,
          PublicationTypeService publicationTypeService, PublicationSubtypeService publicationSubtypeService,
          PublicationSubSubtypeService publicationSubSubtypeService, ResearchGroupService researchGroupService,
          StructureService structureService, DuplicateService duplicateService, UnigePersonSearchService unigePersonSearchService,
          DocumentFileService documentFileService, HistoryService historyService) {
    super(aouProperties, metadataExtractor, messageService, publicationTypeService, publicationSubtypeService, publicationSubSubtypeService,
            researchGroupService, structureService, duplicateService, unigePersonSearchService, documentFileService, historyService);
  }

  @Override
  protected void validateFormDescriptionStep(Publication publication, Object depositDocObj, BindingResult errors) {
    super.validateFormDescriptionStep(publication, depositDocObj, errors);

    this.validateLanguage(publication, depositDocObj, errors);
    this.validateContainerISSN(publication, depositDocObj, errors);
    this.validateContainerISBN(publication, depositDocObj, errors);
    this.validateDOIs(publication, depositDocObj, errors);
  }

  @Override
  protected void validateLanguage(Publication publication, Object depositDocObj, BindingResult errors) {
    String language = ((MetadataExtractorV1) this.metadataExtractor).getLanguage(depositDocObj);
    if (StringTool.isNullOrEmpty(language)) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "language",
              this.messageService.get(ERROR_FIELD_CANNOT_BE_EMPTY)));
    }
  }

  protected void validateContainerISSN(Publication publication, Object depositDocObj, BindingResult errors) {
    String issn = ((MetadataExtractorV1) this.metadataExtractor).getContainerISSN(depositDocObj);
    if (!StringTool.isNullOrEmpty(issn) && !ValidationTool.isValidISSN(issn)) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.issn",
              this.messageService.get(ERROR_INVALID_ISSN)));
    }
  }

  protected void validateContainerISBN(Publication publication, Object depositDocObj, BindingResult errors) {
    String isbn = ((MetadataExtractorV1) this.metadataExtractor).getContainerISBN(depositDocObj);
    if (!StringTool.isNullOrEmpty(isbn) && !ValidationTool.isValidISBN(isbn)) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.isbn",
              this.messageService.get(ERROR_INVALID_ISBN)));
    }
  }

  protected void validateDOIs(Publication publication, Object depositDocObj, BindingResult errors) {
    List<String> dois = ((MetadataExtractorV1) this.metadataExtractor).getDOIs(depositDocObj);
    if (dois != null) {
      IntStream.range(0, dois.size()).forEach(index -> {
        String doi = dois.get(index);
        // TODO : check that doi doesn't already exist in existing deposits. Use ElasticSearch for that ?
        if (!StringTool.isNullOrEmpty(doi) && !ValidationTool.isValidDOI(doi)) {
          errors.addError(
                  new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.dois.doi." + index + ".",
                          this.messageService.get(ERROR_INVALID_DOI)));
        }
      });
    }
  }
}
