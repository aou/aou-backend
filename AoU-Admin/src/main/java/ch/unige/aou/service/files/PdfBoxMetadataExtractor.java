/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PdfBoxMetadataExtractor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.files;

import java.io.IOException;
import java.net.URL;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;

/**
 * FileMetadataExtractor implementation that supports only PDF files
 */
@Service
@ConditionalOnBean(AdminController.class)
public class PdfBoxMetadataExtractor implements FileMetadataExtractor {

  @Override
  public boolean isValidFile(DocumentFile pdfDocumentFile) {
    try (PDDocument doc = PDDocument.load(new URL(pdfDocumentFile.getFinalData().toString()).openStream())) {
      return true;
    } catch (InvalidPasswordException invalidPasswordException) {
      return true;
    } catch (IOException e) {
      return false;
    }
  }

  @Override
  public boolean isPasswordProtected(DocumentFile pdfDocumentFile) throws IOException {
    try (PDDocument doc = PDDocument.load(new URL(pdfDocumentFile.getFinalData().toString()).openStream())) {
      return false;
    } catch (InvalidPasswordException invalidPasswordException) {
      return true;
    }
  }

  @Override
  public String getText(DocumentFile pdfDocumentFile) throws IOException {
    if (MediaType.APPLICATION_PDF_VALUE.equals(pdfDocumentFile.getMimetype())) {
      if (!this.isPasswordProtected(pdfDocumentFile)) {
        try (PDDocument doc = PDDocument.load(new URL(pdfDocumentFile.getFinalData().toString()).openStream())) {
          return new PDFTextStripper().getText(doc);
        }
      }
    }
    return null;
  }
}
