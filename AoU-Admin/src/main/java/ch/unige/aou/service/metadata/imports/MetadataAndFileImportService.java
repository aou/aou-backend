/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataAndFileImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.service.MessageService;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.xml.deposit.v2_4.AccessLevel;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

public abstract class MetadataAndFileImportService extends MetadataImportService {

  private static final Logger log = LoggerFactory.getLogger(MetadataAndFileImportService.class);

  protected DocumentFileService documentFileService;
  protected DocumentFileTypeService documentFileTypeService;

  protected List<String> ignoredFilesSourceUrlPatterns;

  protected MetadataAndFileImportService(AouProperties aouProperties, MessageService messageService, MetadataService metadataService,
          ContributorService contributorService, DocumentFileService documentFileService, DocumentFileTypeService documentFileTypeService,
          JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService) {
    super(aouProperties, messageService, metadataService, contributorService, journalTitleRemoteService, licenseService);

    this.documentFileService = documentFileService;
    this.documentFileTypeService = documentFileTypeService;

    this.ignoredFilesSourceUrlPatterns = List.of(aouProperties.getMetadata().getImports().getIgnoredFilesSourceUrlPatterns());
  }

  protected DocumentFileType convertFileTypeDocToDocumentFileType(FileType type) {
    return this.documentFileTypeService.findByValue(type.value());
  }

  public DocumentFileType convertFileTypeDocToDocumentFileType(String typeName) {
    return this.documentFileTypeService.findByValue(typeName);
  }

  protected DocumentFile.AccessLevel convertAccessLevel(AccessLevel accessLevel) {
    return switch (accessLevel) {
      case PUBLIC -> DocumentFile.AccessLevel.PUBLIC;
      case RESTRICTED -> DocumentFile.AccessLevel.RESTRICTED;
      case PRIVATE -> DocumentFile.AccessLevel.PRIVATE;
      default -> null;
    };
  }

  protected void ensureFileExists(DepositDoc depositDoc) {
    if (depositDoc.getFiles() == null) {
      depositDoc.setFiles(this.factory.createFiles());
    }
  }

  /**
   * Check if the URL must be ignored when fetching document files from external services
   *
   * @param fileSourceUri URL of the file to download
   * @return
   */
  private boolean ignoreDocumentFileSourceDataUrl(URI fileSourceUri) {
    return fileSourceUri == null
            || this.ignoredFilesSourceUrlPatterns.stream().anyMatch(ignoredUrl -> fileSourceUri.toString().matches(ignoredUrl));
  }

  protected DocumentFile saveDocumentFileIfSourceDataValid(DocumentFile documentFile) {
    if (!this.ignoreDocumentFileSourceDataUrl(documentFile.getSourceData())) {
      return this.documentFileService.save(documentFile);
    } else {
      log.info("DocumentFile sourceData '{}' belongs to ignored file source urls. DocumentFile won't be created", documentFile.getSourceData());
      return null;
    }
  }
}
