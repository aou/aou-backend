/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DocumentFileProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.NoTokenRestClientService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouDownloadException;
import ch.unige.aou.message.DocumentFileMessage;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFile.DocumentFileStatus;
import ch.unige.aou.model.tool.CleanTool;

@Service
@ConditionalOnBean(AdminController.class)
public class DocumentFileProcessingService extends AouService {

  private static final String DOCUMENT_FILE_PREFIX = "{DocumentFile} ";

  private static final Logger log = LoggerFactory.getLogger(DocumentFileProcessingService.class);

  private final DocumentFileService documentFileService;
  private final NoTokenRestClientService restClientService;
  private final PublicationService publicationService;
  private final DownloadService downloadService;

  private final int fileNameLengthLimit;

  public DocumentFileProcessingService(AouProperties aouProperties, MessageService messageService, DocumentFileService documentFileService,
          NoTokenRestClientService restClientService, PublicationService publicationService, DownloadService downloadService) {
    super(messageService);
    this.documentFileService = documentFileService;
    this.restClientService = restClientService;
    this.publicationService = publicationService;
    this.downloadService = downloadService;
    this.fileNameLengthLimit = aouProperties.getParameters().getFileNameLengthLimit();
  }

  /**
   * Return true if a new DocumentFileMessage must be sent to continue the DocumentFile processing
   *
   * @param documentFileMessage document file message object
   * @return boolean
   */
  @Transactional
  public boolean processDocumentFile(DocumentFileMessage documentFileMessage) {

    DocumentFile documentFile;
    try {
      documentFile = this.documentFileService.findOne(documentFileMessage.getResId());
    } catch (NoSuchElementException e) {
      log.error("DocumentFile " + documentFileMessage.getResId() + " not found. Stopping DocumentFile processing workflow", e);
      return false;
    }

    int initialHashCode = documentFile.hashCode();

    DocumentFileStatus currentStatus = documentFile.getStatus();

    this.logDocumentFileMessage(LogLevel.INFO, documentFile, "will be processed");

    try {
      switch (currentStatus) {
        case RECEIVED:
          this.processReceivedDocumentFile(documentFile);
          break;
        case TO_PROCESS:
          this.processToProcessDocumentFile(documentFile);
          break;
        case PROCESSED:
          this.processProcessedDocumentFile(documentFile);
          break;
        case CLEANING:
          this.processCleaningDocumentFile(documentFile);
          break;
        default:
          this.logDocumentFileMessage(LogLevel.INFO, documentFile, "has a status that does not require any processing");
          break;
      }
    } catch (AouDownloadException e) {
      documentFile.setStatusWithMessage(DocumentFileStatus.IMPOSSIBLE_TO_DOWNLOAD, e.getMessage());
      this.logDocumentFileMessage(LogLevel.WARN, documentFile, "Unable to download the document file", e);
    } catch (Exception e) {
      documentFile.setStatusWithMessage(DocumentFileStatus.IN_ERROR, e.getMessage());
    }

    boolean mustSendNewMessage = false;

    try {
      if (initialHashCode != documentFile.hashCode()) {
        this.documentFileService.save(documentFile);
        this.logDocumentFileMessage(LogLevel.TRACE, documentFile, "saved in database");

        /*
         * If document file status has been modified, the process must go on --> send a new
         * DocumentFileMessage
         */
        if (documentFile.getStatus() != currentStatus && documentFile.getStatus() != DocumentFileStatus.READY
                && documentFile.getStatus() != DocumentFileStatus.CONFIRMED && documentFile.getStatus() != DocumentFileStatus.TO_BE_CONFIRMED) {
          this.logDocumentFileMessage(LogLevel.DEBUG, documentFile, "a new DocumentFileMessage must be sent to continue processing");
          mustSendNewMessage = true;
        }
      } else {
        this.logDocumentFileMessage(LogLevel.TRACE, documentFile, "has not been modified during its processing --> process stops now");
      }
    } catch (final Exception e) {
      this.logDocumentFileMessage(LogLevel.ERROR, documentFile, "processing error", e);
    }

    return mustSendNewMessage;
  }

  private void processReceivedDocumentFile(DocumentFile documentFile) {
    if (documentFile.getFinalData() != null) {
      documentFile.setStatus(DocumentFileStatus.PROCESSED);
    } else if (this.checkSource(documentFile.getSourceData())) {
      this.calculateFileName(documentFile);
      documentFile.setStatus(DocumentFileStatus.TO_PROCESS);
    } else {
      documentFile.setStatusWithMessage(DocumentFileStatus.IN_ERROR, this.messageService.get("documentFile.error.check"));
    }
  }

  private void processToProcessDocumentFile(DocumentFile documentFile) {
    Path folderPath = this.publicationService.getPublicationFilesFolderPath(documentFile.getPublication());
    Path filePath = folderPath.resolve(documentFile.getFileName());
    this.getData(documentFile, filePath);
    documentFile.setStatus(DocumentFileStatus.PROCESSED);
  }

  private void processProcessedDocumentFile(DocumentFile documentFile) {
    if (documentFile.getFileSize() == null || documentFile.getFileSize() == 0L) {
      // set file size
      long size = FileTool.getSize(documentFile.getFinalData());
      documentFile.setFileSize(size);
    }

    // set mimetype
    if (StringTool.isNullOrEmpty(documentFile.getMimetype())) {
      String mimetype = FileTool.getContentType(documentFile.getFinalData()).toString();
      documentFile.setMimetype(mimetype);
    }

    if (!documentFile.isStoredOutside()) {
      this.calculateChecksum(documentFile);
    }

    documentFile.setStatus(DocumentFileStatus.READY);

    // check document file quality
    this.logDocumentFileMessage(LogLevel.INFO, documentFile, "quality of file will be evaluated");
    this.documentFileService.checkFileValidity(documentFile);
  }

  private void processCleaningDocumentFile(DocumentFile documentFile) {
    throw new SolidifyRuntimeException("Not implemented yet");
  }

  /****************************************************************/

  private void calculateChecksum(DocumentFile documentFile) {
    try {
      String checksumDf = ChecksumTool.computeChecksum(new BufferedInputStream(new FileInputStream(new File(documentFile.getFinalData())),
              SolidifyConstants.BUFFER_SIZE), ChecksumAlgorithm.SHA256);
      documentFile.setChecksum(checksumDf);
    } catch (NoSuchAlgorithmException | IOException e) {
      throw new SolidifyProcessingException(DOCUMENT_FILE_PREFIX + " Error in calculating checksum: finalData=" + documentFile.getFinalData());
    }
  }

  private void calculateFileName(DocumentFile documentFile) {
    if (StringTool.isNullOrEmpty(documentFile.getFileName())) {

      String fileName = StringTool.decodeUrl(
              documentFile.getSourceData().toString().substring(documentFile.getSourceData().toString().lastIndexOf('/') + 1));

      if (!StringTool.isNullOrEmpty(fileName)) {
        fileName = this.cleanFileName(documentFile, fileName);
        documentFile.setFileName(fileName);
      } else {
        throw new SolidifyRuntimeException("unable to calculate filename");
      }
    }
  }

  private boolean checkSource(URI sourceData) {
    final String[] schemes = { "http", "https", "file" };
    final UrlValidator urlValidator = new UrlValidator(schemes, UrlValidator.ALLOW_LOCAL_URLS);
    return urlValidator.isValid(sourceData.toString());
  }

  private void getData(DocumentFile documentFile, Path destinationPath) {
    final URI srcUri = documentFile.getSourceData();
    try {
      switch (srcUri.getScheme()) {
        case "http":
        case "https":
          // Need to verify it the urls is from fedora
          if (documentFile.isStoredOutside()) {
            this.logDocumentFileMessage(LogLevel.INFO, documentFile,
                    "Document file from fedora " + documentFile.getSourceData());
            documentFile.setFinalData(documentFile.getSourceData());
            this.logDocumentFileMessage(LogLevel.INFO, documentFile, "file successfully linked to fedora");
          } else {
            this.logDocumentFileMessage(LogLevel.INFO, documentFile,
                    "will be downloaded from " + documentFile.getSourceData() + " to " + destinationPath);

            // check if file can be downloaded or copied
            this.checkAvailableDiskSpace(srcUri, destinationPath);

            this.downloadService.download(srcUri, destinationPath);

            documentFile.setFinalData(destinationPath.toUri());
            this.logDocumentFileMessage(LogLevel.INFO, documentFile, "file successfully downloaded");
          }
          break;
        case "file":
          this.logDocumentFileMessage(LogLevel.INFO, documentFile, "will be moved from " + Paths.get(srcUri) + " to " + destinationPath);
          this.checkAvailableDiskSpace(srcUri, destinationPath);
          if (Paths.get(srcUri).compareTo(destinationPath) != 0) {
            if (FileTool.moveFile(Paths.get(srcUri), destinationPath)) {
              this.logDocumentFileMessage(LogLevel.INFO, documentFile, "file successfully moved");
            }
          }
          documentFile.setFinalData(destinationPath.toUri());
          break;
        default:
          throw new SolidifyProcessingException(DOCUMENT_FILE_PREFIX + " Protocol '" + srcUri.getScheme() + "' not supported");
      }
    } catch (final FileAlreadyExistsException e) {
      this.logDocumentFileMessage(LogLevel.ERROR, documentFile, "Error in getting data", e);
      final String filename = Paths.get(e.getFile()).getFileName().toString();
      throw new SolidifyProcessingException(this.messageService.get("documentFile.error.already_exists", new Object[] { filename }));
    } catch (final IOException e) {
      this.logDocumentFileMessage(LogLevel.ERROR, documentFile, "Error in getting data", e);
      throw new SolidifyProcessingException(DOCUMENT_FILE_PREFIX + " Error in getting data: source=" + srcUri + " target=" + destinationPath);
    } catch (final ResourceAccessException e) {
      this.logDocumentFileMessage(LogLevel.ERROR, documentFile, "Error in getting data", e);
      if (e.getCause() instanceof FileAlreadyExistsException) {
        final String filePath = ((FileAlreadyExistsException) e.getCause()).getFile();
        final String filename = Paths.get(filePath).getFileName().toString();
        throw new SolidifyProcessingException(this.messageService.get("documentFile.error.already_exists", new Object[] { filename }));
      } else {
        throw new SolidifyProcessingException(DOCUMENT_FILE_PREFIX + " Error in getting data: source=" + srcUri + " target=" + destinationPath);
      }
    }
  }

  protected void logDocumentFileMessage(LogLevel logLevel, DocumentFile documentFile, String message, Exception... e) {
    String publicationId = documentFile.getPublication().getResId();
    message += " (Publication: " + publicationId + ")";
    this.logObjectWithStatusMessage(logLevel, documentFile.getClass().getSimpleName(), documentFile.getResId(),
            documentFile.getStatus().toString(), documentFile.getFileName(), message, e);
  }

  private void checkAvailableDiskSpace(URI srcUri, Path destPath) {
    try {
      long fileSize = this.getFileSize(srcUri);
      if (destPath.toFile().getParentFile().exists()) {
        long usableSpace = destPath.toFile().getParentFile().getUsableSpace();
        if (usableSpace < fileSize) {
          log.error("There isn't enough disk space to save file from {} to {} (file size: {}, space available: {})", srcUri, destPath,
                  StringTool.formatSmartSize(fileSize), StringTool.formatSmartSize(usableSpace));
          throw new SolidifyProcessingException(this.messageService.get("documentFile.error.insufficient_space"));
        } else {
          log.debug("There is enough disk space to save file from {} to {} (file size: {}, space available: {})", srcUri, destPath,
                  StringTool.formatSmartSize(fileSize), StringTool.formatSmartSize(usableSpace));
        }
      } else {
        throw new SolidifyProcessingException(
                this.messageService.get("documentFile.error.missing_folder", new Object[] { destPath.toFile().getParentFile() }));
      }
    } catch (SolidifyProcessingException e) {
      throw e;
    } catch (Exception e) {
      log.warn("unable to check available disk space to save file from {} to {}: {}", srcUri, destPath, e.getMessage());
    }
  }

  private long getFileSize(URI srcUri) {
    switch (srcUri.getScheme()) {
      case "file":
        return FileTool.getSize(Paths.get(srcUri));
      case "http":
      case "https":
        return this.restClientService.headContentLength(srcUri.toString(), TokenUsage.WITHOUT_TOKEN);
      default:
        throw new SolidifyProcessingException(DOCUMENT_FILE_PREFIX + " Protocol '" + srcUri.getScheme() + "' not supported");
    }
  }

  /**
   * Deduplicate filenames for the same Publication and update filename if it contains characters that are ISO_8859_1 compatible.
   * <p>
   * ISO_8859_1 is for instance required by the frontend to be able to download the file. The frontend set HTTP headers when making an in memory
   * download and only ISO_8859_1 are allowed.
   *
   * @param fileName
   * @return
   */
  private String cleanFileName(DocumentFile documentFile, String fileName) {
    String name = fileName;
    String dotExtension = null;
    if (fileName.contains(".")) {
      name = fileName.substring(0, fileName.lastIndexOf("."));
      dotExtension = fileName.substring(fileName.lastIndexOf("."));
    }

    // If the filename isn't ISO_8859_1 compatible, rename it "file"
    CharsetEncoder iso8859Encoder = StandardCharsets.ISO_8859_1.newEncoder();
    if (!iso8859Encoder.canEncode(fileName)) {
      name = AouConstants.FILE_NAME_REPLACEMENT;
    }

    // Shorten fileName if too long
    if (name.length() > this.fileNameLengthLimit) {
      name = name.substring(0, this.fileNameLengthLimit);
    }

    if (dotExtension != null) {
      fileName = name + dotExtension;
    }
    //Replace blank spaces and + characters with underscore
    fileName = CleanTool.cleanFileName(fileName);

    // Deduplicate filename with a suffix number if it is already used by another documentFile
    List<String> otherDocumentFilenames = documentFile.getPublication().getDocumentFiles().stream()
            .filter(df -> !df.getResId().equals(documentFile.getResId())).map(DocumentFile::getFileName).collect(Collectors.toList());
    int i = 1;
    while (otherDocumentFilenames.contains(fileName)) {
      fileName = name + i;
      if (dotExtension != null) {
        fileName = fileName + dotExtension;
      }
      i++;
    }

    return fileName;
  }
}
