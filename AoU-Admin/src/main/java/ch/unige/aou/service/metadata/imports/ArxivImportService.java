/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ArxivImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileType;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.AccessLevel;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.File;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class ArxivImportService extends MetadataAndFileImportService {
  private static final Logger log = LoggerFactory.getLogger(ArxivImportService.class);

  private static final String ARXIV = "arxiv";
  private static final String ARXIV_PDF_URL = "https://arxiv.org/pdf/";
  private static final String ARXIV_FILE_NAME = "_from_arxiv.pdf";
  private static final String XPATH_ARXIV = "/default:feed/default:entry";
  private static final String XPATH_ERROR_VALUE = "Error";
  private static final String XPATH_TITLE_NODE = ArxivImportService.XPATH_ARXIV + "/default:title";

  private static final String XPATH_DEFAULT_ROOT = ArxivImportService.XPATH_ARXIV + "/default";
  private static final String XPATH_ARXIV_ROOT = ArxivImportService.XPATH_ARXIV + "/arxiv";

  private final String apiUrl;
  private final PublicationSubtypeService publicationSubtypeService;

  public ArxivImportService(AouProperties aouProperties, MessageService messageService,
          MetadataService metadataService, ContributorService contributorService, DocumentFileService documentFileService,
          DocumentFileTypeService documentFileTypeService, JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService,
          PublicationSubtypeService publicationSubtypeService) {
    super(aouProperties, messageService, metadataService, contributorService, documentFileService, documentFileTypeService,
            journalTitleRemoteService, licenseService);
    this.apiUrl = aouProperties.getMetadata().getImports().getArxiv().getUrl();
    this.publicationSubtypeService = publicationSubtypeService;
  }

  public DepositDoc createDocumentFileFromArxivMetadata(Publication publication) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());

    DepositDoc depositDoc = (DepositDoc) metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    String arxivId = depositDoc.getIdentifiers().getArxiv();
    Document arxivDoc = this.getXmlFromArxiv(arxivId);

    //Fill file metadata
    this.fillFile(depositDoc, arxivDoc, ArxivImportService.XPATH_DEFAULT_ROOT + ":link[@title='pdf']");

    //get file name
    String filePath = this.getFileNameFromArxivDoc(arxivDoc);
    String fileNameWithoutExtension = filePath.substring(filePath.lastIndexOf('/') + 1);
    File firstFile = depositDoc.getFiles().getFile().stream().filter(f -> f.getName().equals(fileNameWithoutExtension)).findFirst()
            .orElseThrow();
    DocumentFileType documentFileType = this.convertFileTypeDocToDocumentFileType(firstFile.getType());
    DocumentFile.AccessLevel accessLevel = this.convertAccessLevel(firstFile.getAccessLevel());
    String mimeType = firstFile.getMimetype();

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setAccessLevel(accessLevel);
    documentFile.setDocumentFileType(documentFileType);
    documentFile.setMimetype(mimeType);
    documentFile.setIsImported(true);
    documentFile.setFileName(CleanTool.cleanFileName(publication.getSubtype().getName() + ARXIV_FILE_NAME));

    try {
      documentFile.setSourceData(new URI(filePath));
    } catch (URISyntaxException e) {
      throw new SolidifyRuntimeException("Unable to get file URL from arXiv", e);
    }

    this.saveDocumentFileIfSourceDataValid(documentFile);

    return depositDoc;
  }

  public void createDocumentFileFromDoiArxiv(Publication publication) {
    String input = publication.getDoi().toLowerCase();
    input = input.substring(input.indexOf(ARXIV) + ARXIV.length() + 1);
    String urlToPdf = ARXIV_PDF_URL + input;

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);
    documentFile.setMimetype("application/pdf");
    PublicationSubtype publicationSubType = this.publicationSubtypeService.findByCode(publication.getSubtype().getCode());
    PublicationSubtypeDocumentFileType publicationSubtypeDocumentFileType = publicationSubType.getPublicationSubtypeDocumentFileTypes().stream()
            .filter(df -> df.getLevel().equals(DocumentFileType.FileTypeLevel.PRINCIPAL))
            .min(Comparator.comparingInt(PublicationSubtypeDocumentFileType::getSortValue)).orElse(null);
    if (publicationSubtypeDocumentFileType != null) {
      documentFile.setDocumentFileType(publicationSubtypeDocumentFileType.getDocumentFileType());
    } else {
      throw new SolidifyRuntimeException("Unable to get document file type of the file from arXiv URL " + urlToPdf);
    }
    documentFile.setFileName(CleanTool.cleanFileName(publication.getSubtype().getName() + ARXIV_FILE_NAME));
    documentFile.setIsImported(true);

    try {
      documentFile.setSourceData(new URI(urlToPdf));
    } catch (URISyntaxException e) {
      throw new SolidifyRuntimeException("Unable to get pdf file from arXiv URL", e);
    }

    this.saveDocumentFileIfSourceDataValid(documentFile);

  }
  /****************************************************************************************/

  private Document getXmlFromArxiv(String arxivId) {
    try {
      RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
      RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

      Map<String, String> parameters = new HashMap<>();
      parameters.put("arxivId", arxivId);

      final String xmlString = client.getForObject(this.apiUrl, String.class, parameters);
      return XMLTool.parseXML(xmlString);
    } catch (HttpClientErrorException e) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
              this.messageService.get("deposit.error.identifiers.arxiv.not_found_by_arxiv", new Object[] { arxivId }));
    } catch (IOException | ParserConfigurationException | SAXException e) {
      throw new SolidifyRuntimeException("An error occurred when searching identifier '" + arxivId + "' on Arxiv", e);
    }
  }

  public DepositDoc searchArxiv(String arxivId, DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(this.apiUrl)) {
      try {
        Document arxivDoc = this.getXmlFromArxiv(arxivId);
        this.fillDepositDoc(arxivId, depositDoc, arxivDoc);

        return depositDoc;
      } catch (XPathExpressionException e) {
        throw new SolidifyRuntimeException("An error occurred when searching identifier '" + arxivId + "' on Arxiv", e);
      }
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  public void fillDepositDoc(String arxivId, DepositDoc depositDoc, Document arxivDoc) throws XPathExpressionException {
    final XPath xpath = this.getXPathWithNamespaces();

    Node titleNode = (Node) xpath.compile(ArxivImportService.XPATH_TITLE_NODE).evaluate(arxivDoc, XPathConstants.NODE);

    if (titleNode == null || titleNode.getTextContent().equals(XPATH_ERROR_VALUE)) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
              this.messageService.get("deposit.error.identifiers.arxiv.not_found_by_arxiv", new Object[] { arxivId }));
    }

    depositDoc.setType(AouConstants.DEPOSIT_TYPE_ARTICLE_ID);
    depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);

    String title = titleNode.getTextContent();
    title = this.cleanText(title);
    depositDoc.setTitle(this.getText(title));

    this.fillAbstracts(depositDoc, arxivDoc, ArxivImportService.XPATH_DEFAULT_ROOT + ":summary");
    this.fillPublicationDate(depositDoc, arxivDoc, ArxivImportService.XPATH_DEFAULT_ROOT + ":published");
    this.fillDoi(depositDoc, arxivDoc, ArxivImportService.XPATH_ARXIV_ROOT + ":doi");
    this.fillContributors(depositDoc, arxivDoc, ArxivImportService.XPATH_DEFAULT_ROOT + ":author");
    this.fillNote(depositDoc, arxivDoc, ArxivImportService.XPATH_ARXIV_ROOT + ":comment");
    this.fillContainerTitle(depositDoc, arxivDoc, ArxivImportService.XPATH_ARXIV_ROOT + ":journal_ref");
    this.fillArxivId(depositDoc, arxivDoc, ArxivImportService.XPATH_DEFAULT_ROOT + ":id");
  }

  private void fillArxivId(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    String value = this.getValueFromXPathQuery(arxivDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      value = value.replace("http://arxiv.org/abs/", "");
      this.ensureDepositIdentifiersExist(depositDoc);
      depositDoc.getIdentifiers().setArxiv(value);
    }
  }

  @Override
  protected void fillContainerTitle(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    String value = this.getValueFromXPathQuery(arxivDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setTitle(value);
    }
  }

  private void fillFile(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    final XPath xpath = this.getXPathWithNamespaces();
    try {
      NodeList filesList = (NodeList) xpath.compile(xpathQuery).evaluate(arxivDoc, XPathConstants.NODESET);
      if (filesList.getLength() > 0) {
        this.ensureFileExists(depositDoc);
      }

      String filePath = this.getFileNameFromArxivDoc(arxivDoc);

      File file = new File();
      //If there is journal_ref tag, the file's type is ARTICLE_SUBMITTED_VERSION, if not, REPORT-> PREPRINT.
      if (this.isJournalRefPresent(arxivDoc)) {
        file.setType(FileType.ARTICLE_SUBMITTED_VERSION);
      } else {
        file.setType(FileType.PREPRINT);
      }
      file.setAccessLevel(AccessLevel.PUBLIC);
      file.setMimetype("application/pdf");
      file.setName(filePath.substring(filePath.lastIndexOf('/') + 1));
      depositDoc.getFiles().getFile().add(file);

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill files", e);
    }
  }

  private boolean isJournalRefPresent(Document arxivDoc) {
    String value = this.getValueFromXPathQuery(arxivDoc, ArxivImportService.XPATH_ARXIV_ROOT + ":journal_ref");
    return !StringTool.isNullOrEmpty(value);
  }

  private void fillContributors(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    final XPath xpath = this.getXPathWithNamespaces();
    try {
      NodeList authorsList = (NodeList) xpath.compile(xpathQuery).evaluate(arxivDoc, XPathConstants.NODESET);

      if (authorsList.getLength() > 0) {
        this.ensureContributorsExist(depositDoc);
      }

      for (int i = 0; i < this.getNumberMaxContributor(authorsList.getLength()); i++) {
        Contributor contributor = this.createContributorFromNode(authorsList.item(i));
        depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
      }
      // add authors that have an affiliation with unige, geneve, or hug
      NodeList authorsListWithAffiliation = (NodeList) xpath.compile(xpathQuery
                      + "[contains(./AffiliationInfo/Affiliation, 'Geneva') or contains(./AffiliationInfo/Affiliation, 'Genève')  or contains(./AffiliationInfo/Affiliation, 'Genéve')"
                      + "or contains(./AffiliationInfo/Affiliation, 'Unige') or contains(./AffiliationInfo/Affiliation, 'HUG')]")
              .evaluate(arxivDoc, XPathConstants.NODESET);
      for (int i = 0; i < authorsListWithAffiliation.getLength(); i++) {
        Contributor contributor = this.createContributorFromNode(authorsListWithAffiliation.item(i));
        //check if it was already added
        if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                .noneMatch(c -> ((Contributor) c).getLastname().equals(contributor.getLastname())
                        && ((Contributor) c).getFirstname().equals(contributor.getFirstname()) && ((Contributor) c).getInstitution()
                        .equals(contributor.getInstitution()))) {
          depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
        }
      }
    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill contributors", e);
    }
  }

  @Override
  protected void fillNote(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    String value = this.getValueFromXPathQuery(arxivDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      depositDoc.setNote(value);
    }
  }

  @Override
  protected void fillDoi(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    String value = this.getValueFromXPathQuery(arxivDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDepositIdentifiersExist(depositDoc);
      depositDoc.getIdentifiers().setDoi(value);
    }
  }

  private void fillPublicationDate(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    String value = this.getValueFromXPathQuery(arxivDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDatesExist(depositDoc);
      try {
        DateWithType date = new DateWithType();
        date.setType(DateTypes.PUBLICATION);
        //Transform format date from arxiv yyyy-MM-ddTHH:mm:ssZ to yyyy-MM-dd
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(value, inputFormatter);
        String formattedDate = outputFormatter.format(localDate);
        date.setContent(formattedDate);
        depositDoc.getDates().getDate().add(date);
      } catch (DateTimeParseException e) {
        log.error("Error when transforming date: {}", value, e);
      }
    }
  }

  private void fillAbstracts(DepositDoc depositDoc, Document arxivDoc, String xpathQuery) {
    String value = XMLTool.getFirstNodeInnerRawContent(arxivDoc, xpathQuery, this.getArxivNamespaces());
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureAbstractsExist(depositDoc);
      value = this.cleanText(value);
      depositDoc.getAbstracts().getAbstract().add(this.getText(value));
    }
  }

  private String getFileNameFromArxivDoc(Document arxivDoc) {
    String fileXPathQuery = ArxivImportService.XPATH_DEFAULT_ROOT + ":link[@title='pdf']";
    return this.getValueFromXPathQuery(arxivDoc, fileXPathQuery + "/@href");
  }

  /****************************************************************************************/

  private String getValueFromXPathQuery(Document arxivDoc, String xpathQuery) {
    return XMLTool.getFirstTextContent(arxivDoc, xpathQuery, this.getArxivNamespaces());
  }

  private HashMap<String, String> getArxivNamespaces() {
    HashMap<String, String> namespaces = new HashMap<>();
    namespaces.put("default", "http://www.w3.org/2005/Atom");
    namespaces.put("arxiv", "http://arxiv.org/schemas/atom");
    return namespaces;
  }

  private XPath getXPathWithNamespaces() {
    return XMLTool.buildXPath(this.getArxivNamespaces());
  }

  private Contributor createContributorFromNode(Node author) throws XPathExpressionException {
    final XPath xpath = XMLTool.buildXPath();

    String authorName = XMLTool.getFirstTextContent(author, "./default:name", this.getArxivNamespaces());

    // Cut the author name on the last space, assuming the last word is the lastname
    String firstName = null;
    String lastName = null;
    if (!StringTool.isNullOrEmpty(authorName)) {
      authorName = authorName.trim();
      int lastSpaceIndex = authorName.lastIndexOf(' ');
      if (lastSpaceIndex > -1) {
        firstName = authorName.substring(0, lastSpaceIndex);
        lastName = authorName.substring(lastSpaceIndex + 1);
      } else {
        lastName = authorName;
      }
    }

    firstName = this.getNameOrUndefinedValue(firstName);
    lastName = this.getNameOrUndefinedValue(lastName);

    Contributor contributor = new Contributor();
    contributor.setLastname(CleanTool.capitalizeString(lastName));
    contributor.setFirstname(CleanTool.capitalizeString(firstName));

    //check if there are institutions
    NodeList institutionList = (NodeList) xpath.compile("./arxiv:affiliation").evaluate(author, XPathConstants.NODESET);
    if (institutionList.getLength() > 0) {
      String institution = "";
      for (int j = 0; j < institutionList.getLength(); j++) {
        institution = (institution.isEmpty()) ?
                institution.concat(institutionList.item(j).getTextContent()) :
                institution.concat("," + institutionList.item(j).getTextContent());
      }
      contributor.setInstitution(institution);
    }
    return contributor;
  }
}
