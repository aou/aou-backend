/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MonitorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;

import ch.unige.solidify.model.xml.orcid.v3_0.work.WorkSummary;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.OrcidSynchronizationService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouRuntimeException;
import ch.unige.aou.model.ExistingPublicationInfo;
import ch.unige.aou.model.orcid.ExternalIdentifiers;
import ch.unige.aou.model.orcid.ImportedIdentifiers;
import ch.unige.aou.model.orcid.OrcidSynchronization;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.metadata.imports.MetadataImportWorkFlowService;

@Service
@ConditionalOnBean(AdminController.class)
public class OrcidSynchronizerService {

  static final Logger log = LoggerFactory.getLogger(OrcidSynchronizerService.class);

  private final String publicationUrlTemplate;
  private final String storagePrefix;

  private final MetadataService metadataService;
  private final PublicationService publicationService;
  private final MetadataImportWorkFlowService metadataImportWorkFlowService;
  private final OrcidSynchronizationService orcidSynchronizationService;

  public OrcidSynchronizerService(AouProperties properties, MetadataService metadataService, PublicationService publicationService,
          MetadataImportWorkFlowService metadataImportWorkFlowService, @Lazy OrcidSynchronizationService orcidSynchronizationService) {
    this.metadataService = metadataService;
    this.publicationService = publicationService;
    this.metadataImportWorkFlowService = metadataImportWorkFlowService;
    this.orcidSynchronizationService = orcidSynchronizationService;
    this.publicationUrlTemplate = properties.getParameters().getPublicationUrlTemplate();
    this.storagePrefix = properties.getStorage().getPrefix();
  }

  @Transactional
  public OrcidSynchronization importOrcidWorkWithoutExternalId(BigInteger putCode, WorkSummary workSummary, Person person) {
    log.info("Person {}, putCode: {}, workSummary: {}", person.getFullName(), putCode, workSummary.getTitle().getTitle());

    Optional<OrcidSynchronization> orcidSynchronizationOpt = this.orcidSynchronizationService.findByPersonResIdAndPutCode(person.getResId(),
            putCode);
    if (orcidSynchronizationOpt.isPresent()) {
      // ORCID Work is already known as an AoU Publication
      log.info("Work {} is already linked to Publication {}", putCode, orcidSynchronizationOpt.get().getPublication().getResId());
    } else {
      // ORCID Work is not known yet --> create it
      // Note: no need to check for duplicates as the Work doesn't have any identifier
      String xmlMetadata = this.metadataImportWorkFlowService.getMetadataFromOrcidPutCode(putCode, person, true);

      // Is the publication source from AoU itself ? If this is the case, we don't try to import it again to not create a duplicate
      Optional<Publication> existingPublicationOpt = this.getExistingPublicationByUrl(xmlMetadata);
      if (existingPublicationOpt.isPresent()) {
        // This a publication from AoU --> create a corresponding OrcidSynchronization
        // Note: do not set any download date: as it already exists it is not downloaded from ORCID
        String publicationId = existingPublicationOpt.get().getResId();
        OrcidSynchronization savedOrcidSync = this.orcidSynchronizationService.storeOrcidSynchronization(person, putCode, publicationId, null,
                null);
        List<OrcidSynchronization> orcidSynchronizations = this.orcidSynchronizationService.findByPersonResIdAndPublicationResId(
                person.getResId(), publicationId);
        if (orcidSynchronizations.size() == 1) {
          // This was the first link between ORCID profile and an AoU publication --> user must be notified
          return savedOrcidSync;
        }
      } else {
        // Create Publication
        Publication savedPublication = this.storePublication(xmlMetadata, Publication.ImportSource.ORCID, person);
        // Create corresponding OrcidSynchronization
        return this.orcidSynchronizationService.storeOrcidSynchronization(person, putCode, savedPublication.getResId(), OffsetDateTime.now(),
                null);
      }
    }
    return null;
  }

  @Transactional
  public OrcidSynchronization importOrcidWorkWithExternalId(BigInteger putCode, ExternalIdentifiers externalIdentifiers, Person person,
          ImportedIdentifiers alreadyImportedIdentifiers) {
    log.info("Person {}, putCode: {}}, identifiers: {}", person.getFullName(), putCode, externalIdentifiers);

    // Check if the person has already a link to the ORCID putCode
    Optional<OrcidSynchronization> orcidSynchronizationOpt = this.orcidSynchronizationService.findByPersonResIdAndPutCode(person.getResId(),
            putCode);
    if (orcidSynchronizationOpt.isPresent()) {
      // ORCID Work is already known as a AoU Publication
      log.info("Work {} is already linked to Publication {}", putCode, orcidSynchronizationOpt.get().getPublication().getResId());
    } else {
      // ORCID Work is not known yet -> check by its identifier(s) if it already exists in AoU Publications
      // or if one of its identifiers has just been imported
      // Note: checking for duplicates may be not sufficient as the indexation may be too slow
      boolean publicationAlreadyImported = alreadyImportedIdentifiers.containsOneIdentifier(externalIdentifiers);
      ExistingPublicationInfo existingPublicationInfo = null;
      if (!publicationAlreadyImported) {
        existingPublicationInfo = this.orcidSynchronizationService.getExistingPublicationInfo(externalIdentifiers);
      }

      if (existingPublicationInfo != null || publicationAlreadyImported) {
        // A publication with the same identifier already exists in AoU --> create a corresponding OrcidSynchronization
        // Note: do not set any download date: as it already exists it is not downloaded from ORCID
        String publicationId;
        if (existingPublicationInfo != null) {
          publicationId = existingPublicationInfo.getResId();
        } else {
          publicationId = alreadyImportedIdentifiers.getPublicationId(externalIdentifiers);
        }
        OrcidSynchronization savedOrcidSync = this.orcidSynchronizationService.storeOrcidSynchronization(person, putCode, publicationId, null,
                null);
        List<OrcidSynchronization> orcidSynchronizations = this.orcidSynchronizationService.findByPersonResIdAndPublicationResId(
                person.getResId(), publicationId);
        if (orcidSynchronizations.size() == 1) {
          // This was the first link between ORCID profile and an AoU publication --> user must be notified
          return savedOrcidSync;
        }
      } else {
        // The Work doesn't exist in AoU as a Publication yet --> import it
        try {
          OrcidSynchronization savedOrcidSync = this.saveOrcidWorkWithExternalId(person, putCode, externalIdentifiers);
          if (savedOrcidSync != null) {
            alreadyImportedIdentifiers.addExternalIdentifiers(savedOrcidSync.getPublication().getResId(), externalIdentifiers);
            return savedOrcidSync;
          } else {
            log.warn("Unable to import putCode {} from ORCID. Its externalIds ({}) may be unrecognized by the import system ?", putCode,
                    externalIdentifiers);
          }
        } catch (Exception e) {
          log.warn("Unable to import putCode {} from ORCID. Its externalIds ({}) may be unrecognized by the import system ? {}", putCode,
                  externalIdentifiers, e.getMessage());
        }
      }
    }
    return null;
  }

  private Optional<Publication> getExistingPublicationByUrl(String xmlMetadata) {
    Object depositDoc = this.metadataService.getDepositDoc(xmlMetadata);
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
    String url = metadataExtractor.getPublisherVersionUrl(depositDoc);
    if (!StringTool.isNullOrEmpty(url)) {
      String publicationUrlPrefix = this.publicationUrlTemplate.replace("{archiveId}", "");
      if (url.startsWith(publicationUrlPrefix)) {
        String suffix = StringTool.removePrefix(url, publicationUrlPrefix);
        if (suffix.startsWith(this.storagePrefix + ":")) {
          return this.publicationService.findByArchiveId(suffix);
        }
      }
    }
    return Optional.empty();
  }

  private Publication storePublication(String xmlMetadata, Publication.ImportSource importSource, Person person) {
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setImportSource(importSource);
    publication.setNotifyFilesFromExternalSource(true);
    publication.setNotifyContributorsFromExternalSource(true);
    publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
    publication.setCreator(person);
    return this.publicationService.save(publication);
  }

  private OrcidSynchronization saveOrcidWorkWithExternalId(Person person, BigInteger putCode, ExternalIdentifiers externalIdentifiers) {

    String xmlMetadata = null;
    Publication.ImportSource importSource = null;
    try {
      if (!StringTool.isNullOrEmpty(externalIdentifiers.getPmid())) {
        xmlMetadata = this.metadataImportWorkFlowService.getMetadataFromPmid(externalIdentifiers.getPmid(), true);
        importSource = Publication.ImportSource.PMID_ORCID;
      } else if (!StringTool.isNullOrEmpty(externalIdentifiers.getDoi())) {
        xmlMetadata = this.metadataImportWorkFlowService.getMetadataFromDoi(externalIdentifiers.getDoi(), true);
        importSource = Publication.ImportSource.DOI_ORCID;
      } else if (!StringTool.isNullOrEmpty(externalIdentifiers.getArxivId())) {
        xmlMetadata = this.metadataImportWorkFlowService.getMetadataFromArxiv(externalIdentifiers.getArxivId(), true);
        importSource = Publication.ImportSource.ARXIV_ORCID;
      } else if (!StringTool.isNullOrEmpty(externalIdentifiers.getIsbn())) {
        xmlMetadata = this.metadataImportWorkFlowService.getMetadataFromIsbn(externalIdentifiers.getIsbn(), true);
        importSource = Publication.ImportSource.ISBN_ORCID;
      }
    } catch (AouRuntimeException e) {
      log.error("An error occurred while trying to get Publication metadata for ORCID Work with putCode " + putCode + " for person "
              + person.getFullName() + "(" + person.getOrcid() + ")", e);
    }

    if (!StringTool.isNullOrEmpty(xmlMetadata)) {
      // Create Publication
      Publication savedPublication = this.storePublication(xmlMetadata, importSource, person);
      // Store corresponding OrcidSynchronization
      OrcidSynchronization savedOrcidSync = this.orcidSynchronizationService.storeOrcidSynchronization(person, putCode,
              savedPublication.getResId(), OffsetDateTime.now(),
              null);

      // try to import file if possible
      try {
        this.metadataImportWorkFlowService.importFilesFromServices(savedPublication);
      } catch (IOException | NoSuchAlgorithmException | HttpStatusCodeException e) {
        log.warn("Unable to download publication's files", e);
      }

      return savedOrcidSync;
    }

    return null;
  }
}
