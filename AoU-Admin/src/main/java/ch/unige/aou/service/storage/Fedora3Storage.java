/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - Fedora3Storage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.storage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.AouXmlNamespacePrefixMapper;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationThumbnail;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.model.xml.fedora.foxml.v1.DatastreamType;
import ch.unige.aou.model.xml.fedora.foxml.v1.DatastreamVersionType;
import ch.unige.aou.model.xml.fedora.foxml.v1.DigitalObject;
import ch.unige.aou.model.xml.fedora.repository.DatastreamProfile;
import ch.unige.aou.model.xml.fedora.repository.FedoraRepository;
import ch.unige.aou.service.MetadataService;

@Profile("storage-fedora3")
@Service
@ConditionalOnBean(AdminController.class)
public class Fedora3Storage extends StorageService {

  private static final Logger log = LoggerFactory.getLogger(Fedora3Storage.class);

  private static final String ADMIN_DATASTREAM_NAME = "ADMIN";

  private static final String ADMIN_NAME_XSL = "admin";

  public static final List<String> FILE_DATASTREAM_PREFIXES = List.of("ATTACHMENT", "THESIS");
  private static final String XPATH_FILENAME = "./FileName";
  private static final String XPATH_SIZE = "./Size";
  private static final String XPATH_DATASTREAM_ID = "./Datastream_ID";
  private static final String XPATH_DESCRIPTION = "./Description";
  private static final String XPATH_DESCRIPTION_COMPLEMENT = "./DescriptionComplement";
  private static final String XPATH_AVAILABILITY = "./Availability";
  private static final String XPATH_EMBARGO_ENDDATE = "./EmbargoEndDate";
  private static final String XPATH_EMBARGO = "./Embargo";
  private static final String XPATH_PERIOD = "./Period";

  private static final String XPATH_ADMIN_ATTACHMENT = "/admin/deposit/Attachment";
  private static final String XPATH_ADMIN_SUBMISSION_DATE = "/admin/deposit/SubmissionDate";
  private static final String XPATH_LICENCE = "./Licence";

  private static final String REGEX_ID = "[^0-9]";

  private final String fedoraUrl;
  private final String fedoraUser;
  private final String fedoraPassword;
  private final String fedoraPrefix;
  private final JAXBContext jaxbContext;
  private final Map<String, String> xlstList = new HashMap<>();

  protected String tempFolder;

  private final StructureService structureService;
  private final DocumentFileService documentFileService;
  private final DocumentFileTypeService documentFileTypeService;
  private final LicenseService licenseService;

  public Fedora3Storage(AouProperties aouProperties, MessageService messageService, PublicationService publicationService,
          UserService userService, MetadataService metadataService, StructureService structureService, DocumentFileService documentFileService,
          DocumentFileTypeService documentFileTypeService, LicenseService licenseService) {
    super(messageService, publicationService, userService, metadataService);

    this.documentFileService = documentFileService;
    this.structureService = structureService;
    this.documentFileTypeService = documentFileTypeService;
    this.licenseService = licenseService;

    // Check configuration
    String fedoraUrlValue = aouProperties.getStorage().getUrl();
    if (!fedoraUrlValue.endsWith("/")) {
      fedoraUrlValue = fedoraUrlValue + "/";
    }
    this.fedoraUrl = fedoraUrlValue;
    this.fedoraUser = aouProperties.getStorage().getUser();
    this.fedoraPassword = aouProperties.getStorage().getPassword();
    this.fedoraPrefix = aouProperties.getStorage().getPrefix();

    this.tempFolder = aouProperties.getTempLocation(aouProperties.getPublicationsFolder());

    // Check Prefix
    if (StringTool.isNullOrEmpty(this.fedoraPrefix)) {
      throw new SolidifyRuntimeException("Missing Fedora prefix");
    }

    // Complete preloading configuration
    try {
      // Jaxb
      this.jaxbContext = JAXBContext.newInstance(DepositDoc.class);
      // Load XSL
      this.loadXsl();
    } catch (JAXBException | IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }

    this.checkFedoraServer();
  }

  @Override
  public void generateExportMetadata(Publication publication) {
    // Generate attachmentIdsString as string that contains each index as document files are in the publication separated by ;
    // It should be depending of number of document files and thumbnails as: 1;2;3;
    StringBuilder stringBuilder = new StringBuilder();
    AtomicInteger index = new AtomicInteger();
    publication.getDocumentFiles().forEach(documentFile -> stringBuilder.append(index.incrementAndGet()).append(";"));
    if (publication.getThumbnail() != null) {
      stringBuilder.append(index.incrementAndGet()).append(";");
    }
    this.generateMetadataXMLs(publication, stringBuilder.toString());
  }

  @Override
  public void updateExportMetadata(Publication publication) {
    // Create a string of ids checking if the already attachments exits or not already in Fedora.
    // If the document file does not exist in fedora, we will generate a new id using the last index already used.
    // If the document file exists in Fedora, we used the one already used in AttachmentID
    String attachmentsIdsString = this.generateStringIdsUsedForAttachmentsIds(publication);
    this.generateMetadataXMLs(publication, attachmentsIdsString);
  }

  private String generateStringIdsUsedForAttachmentsIds(Publication publication) {
    StringBuilder stringBuilder = new StringBuilder();

    List<DatastreamType> filesDatastreams = this.getAttachmentsDatastreams(publication);

    int maxIndex = 0;
    if (!filesDatastreams.isEmpty()) {
      // Attachments ids from history are made as ATTACHMENTX.NUM_VERSION or ATTACHMENTX
      maxIndex = filesDatastreams.stream().map(DatastreamType::getID)
              .map(id -> {
                if (id.contains(".")) {
                  return Integer.parseInt(id.substring(0, id.indexOf(".")).replaceAll(REGEX_ID, ""));
                } else {
                  return Integer.parseInt(id.replaceAll(REGEX_ID, ""));
                }
              })
              .collect(Collectors.summarizingInt(Integer::intValue))
              .getMax();
    }

    for (DocumentFile documentFile : publication.getDocumentFiles()) {
      // check if already exits in fedora DataStreams
      int datastreamIndex = this.getMatchingOrNewDatastreamIndex(publication, maxIndex, documentFile.getFileName(), documentFile.getFileSize(),
              documentFile.getChecksum(), filesDatastreams);
      stringBuilder.append(datastreamIndex).append(";");
      if (datastreamIndex > maxIndex) {
        maxIndex = datastreamIndex;
      }
    }

    // check also the thumbnail
    if (publication.getThumbnail() != null) {
      int thumbnailIndex = this.getMatchingOrNewDatastreamIndex(publication, maxIndex, publication.getThumbnail().getFileName(),
              publication.getThumbnail().getFileSize(), null, filesDatastreams);
      stringBuilder.append(thumbnailIndex).append(";");
    }

    return stringBuilder.toString();
  }

  private int getMatchingOrNewDatastreamIndex(Publication publication, int maxIndex, String fileName, long fileSize, String localFileChecksum,
          List<DatastreamType> filesDatastreams) {
    String dsIdToReturn = null;

    // Compare by using filename and filesize
    Optional<DatastreamType> dsBeforeUpdateOpt = filesDatastreams.stream()
            .filter(dS -> this.getLastVersionOfDatastream(dS).getLABEL().equals(fileName)
                    && this.getLastVersionOfDatastream(dS).getSIZE() == fileSize)
            .findFirst();
    if (dsBeforeUpdateOpt.isPresent()) {
      // datastream with same name and size found --> we assume they are identical
      dsIdToReturn = dsBeforeUpdateOpt.get().getID();
    } else {

      // Some old deposits in Fedora do not contain file names.
      // -> If no file was found above, we try to find it in Fedora by using its file size only
      dsBeforeUpdateOpt = filesDatastreams.stream()
              .filter(dS -> this.getLastVersionOfDatastream(dS).getSIZE() == fileSize)
              .findFirst();

      if (dsBeforeUpdateOpt.isPresent()) {
        String dsId = dsBeforeUpdateOpt.get().getID();
        // Datastream with same size found in Fedora
        if (!StringTool.isNullOrEmpty(localFileChecksum)) {
          try {
            // As we know the file checksum, download the datastream content from Fedora to compare checksums to ensure it is the same file
            // This is a slow process, but this is done only during update of old publications
            Path tmpFolder = Paths.get(this.tempFolder).resolve(publication.getResId());
            Path filePath = Path.of(tmpFolder.toString(), fileName);
            File tmpFilePath = this.getAttachmentFromFedora(publication.getArchiveId(), dsId, filePath);
            String dsChecksum = ChecksumTool.computeChecksum(
                    new BufferedInputStream(new FileInputStream(tmpFilePath.toString()), SolidifyConstants.BUFFER_SIZE),
                    ChecksumAlgorithm.SHA256);
            if (dsChecksum.equals(localFileChecksum)) {
              // It is indeed the same file
              dsIdToReturn = dsId;
            }
          } catch (NoSuchAlgorithmException | IOException e) {
            log.error("unable to download datastream content to compare checksums", e);
          }
        } else {
          // Local file has no checksum. We assume that if files have the same size they are identical
          dsIdToReturn = dsId;
        }
      }
    }

    if (!StringTool.isNullOrEmpty(dsIdToReturn)) {
      return dsIdToReturn.contains(".") ?
              Integer.parseInt(dsIdToReturn.substring(0, dsIdToReturn.indexOf(".")).replaceAll(REGEX_ID, "")) :
              Integer.parseInt(dsIdToReturn.replaceAll(REGEX_ID, ""));
    }

    return maxIndex + 1;
  }

  /**
   * Method that creates each metadata xml
   *
   * @param publication
   * @param attachmentsIdsString string with the order of attachment ids as they should be defined in ADMIN xml separated by ;. (example:
   *                             1;3;2;4;).
   */
  private void generateMetadataXMLs(Publication publication, String attachmentsIdsString) {

    // Check that attachmentsIdsString doesn't contain twice the same index
    this.checkAttachmentIdsString(attachmentsIdsString);

    Path metadataFolder = this.getMetadataFolder(publication.getResId());
    if (!FileTool.ensureFolderExists(metadataFolder)) {
      throw new SolidifyProcessingException("Problem with " + metadataFolder.toString());
    }
    // Generate Deposit metadata
    try {
      DepositDoc depositDoc = (DepositDoc) this.deserialize(publication.getMetadata());
      this.saveToFile(metadataFolder.resolve(AouConstants.AOU + SolidifyConstants.XML_EXT), this.serialize(depositDoc));
    } catch (JAXBException | IOException e) {
      throw new SolidifyProcessingException("Error in generation " + AouConstants.AOU + SolidifyConstants.XML_EXT + " metadata", e);
    }

    String completedPivotMetadata = this.getCompletedPivotMetadataForTransformation(publication);

    // Generate other metadata
    for (Entry<String, String> xslt : this.xlstList.entrySet()) {
      try {
        log.info("Generating metadata for {} ({})", xslt.getKey(), publication.getResId());
        String metadata;
        if (xslt.getKey().contains(ADMIN_NAME_XSL)) {
          // For admin, we need to set the variable for the attachments IDs that would be used to create the ATTACHMENT ID
          Map<String, String> parameters = new HashMap<>();
          parameters.put("position_ids", attachmentsIdsString);
          metadata = XMLTool.transform(completedPivotMetadata, xslt.getValue(), parameters);
        } else {
          metadata = XMLTool.transform(completedPivotMetadata, xslt.getValue());
        }
        this.saveToFile(metadataFolder.resolve(xslt.getKey() + SolidifyConstants.XML_EXT), metadata);

      } catch (IOException e) {
        throw new SolidifyProcessingException("Error in generation " + xslt.getKey() + SolidifyConstants.XML_EXT + " metadata", e);
      }
    }
  }

  private void checkAttachmentIdsString(String attachmentsIdsString) {
    HashSet<String> foundIndexes = new HashSet<>();
    String[] indexes = attachmentsIdsString.split(";");
    for (String index : indexes) {
      if (!foundIndexes.contains(index)) {
        foundIndexes.add(index);
      } else {
        throw new IllegalStateException(
                "Index value " + index + " found twice. Stopping process to not loose data when storing files in Fedora datastreams.");
      }
    }
  }

  @Override
  public String getNextArchiveId(Publication publication) {
    Fedora3Client fedoraClient = this.getFedoraClient();
    try {
      return fedoraClient.getNextPID(this.fedoraPrefix);
    } catch (JAXBException e) {
      throw new SolidifyRuntimeException("Unable to get next archive id", e);
    }
  }

  @Override
  public void storePublication(Publication publication) {
    try {
      Fedora3Client fedoraClient = this.getFedoraClient();
      this.setArchiveIdIfEmpty(fedoraClient, publication);
      this.createArchiveIfNotExist(fedoraClient, publication);
      Document adminDatastreamDocument = null;
      // Check admin datastream before stock it to be able to clean after the attachments not used anymore
      try {
        adminDatastreamDocument = this.getAdminDataStreamDocument(fedoraClient, publication);
      } catch (SolidifyResourceNotFoundException e) {
        // It is a new publication
      }
      this.saveMetadata(fedoraClient, publication);
      this.ingestAttachments(fedoraClient, publication);
      this.cleanAttachmentsFromFedora(fedoraClient, publication, adminDatastreamDocument);
      this.checkObject(publication);
    } catch (Exception e) {
      throw new SolidifyProcessingException(e.getMessage(), e);
    }
  }

  private void checkObject(Publication publication) {
    DigitalObject foxml = this.getFoxml(publication.getArchiveId());
    List<DatastreamType> datastreams = foxml.getDatastream();
    if (!datastreams.stream().anyMatch(datastreamType -> datastreamType.getID().equals(ADMIN_DATASTREAM_NAME))) {
      throw new SolidifyRuntimeException(
              "Fedora object '" + publication.getArchiveId() + "' doesn't contain any " + ADMIN_DATASTREAM_NAME + " datastream");
    }
    if (!datastreams.stream().anyMatch(datastreamType -> datastreamType.getID().equals(AouConstants.AOU.toUpperCase()))) {
      throw new SolidifyRuntimeException(
              "Fedora object '" + publication.getArchiveId() + "' doesn't contain any " + AouConstants.AOU.toUpperCase() + " datastream");
    }
  }

  /**
   * It will take the DigitalObject of the publication from Fedora and create each DocumentFile accordingly.
   *
   * @param publication publication to check in fedora
   */
  @Override
  public void revertDocumentFilesAndThumbnailFromStorageInfo(Publication publication) {
    Fedora3Client fedoraClient = this.getFedoraClient();
    try {
      Document adminDatastreamDocument = this.getAdminDataStreamDocument(fedoraClient, publication);
      String submissionDate = XMLTool.getFirstTextContent(adminDatastreamDocument, XPATH_ADMIN_SUBMISSION_DATE);
      XPath xpath = XMLTool.buildXPath();
      NodeList attachmentList = (NodeList) xpath.compile(XPATH_ADMIN_ATTACHMENT)
              .evaluate(adminDatastreamDocument, XPathConstants.NODESET);

      for (int i = 0; i < attachmentList.getLength(); i++) {
        // check if the attachment already exist in publication document files, if not created.
        final String fileName = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_FILENAME);
        final String size = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_SIZE);
        String description = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_DESCRIPTION);
        String datastreamId = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_DATASTREAM_ID);
        String embargoAvailability = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_EMBARGO);
        String period = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_PERIOD);
        Integer embargoPeriod = (period != null) ? Integer.parseInt(period) : null;
        String embargoEndDate = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_EMBARGO_ENDDATE);
        String availability = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_AVAILABILITY);
        String licence = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_LICENCE);

        if (!description.equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)) {

          Optional<DocumentFile> documentFileOpt;
          if (!StringTool.isNullOrEmpty(fileName) && !StringTool.isNullOrEmpty(size)) {
            documentFileOpt = publication.getDocumentFiles().stream()
                    .filter(df -> df.getFileSize().toString().equals(size) && df.getFileName().equals(fileName)).findFirst();
          } else {
            // Case of old publication with incomplete ADMIN datastream --> we search the document file by its size only
            String dsSize = this.getDatastreamSize(publication, datastreamId);
            documentFileOpt = publication.getDocumentFiles().stream()
                    .filter(df -> df.getFileSize().toString().equals(dsSize)).findFirst();
          }

          if (documentFileOpt.isEmpty()) {
            // DocumentFile has been removed locally --> restore it with info from Fedora
            // create Document file with info from Admin
            DatastreamProfile attachmentStreamProfile = fedoraClient.getDataStreamProfile(publication.getArchiveId(), datastreamId);

            DocumentFile documentFile = this.setDocumentFile(publication, datastreamId, fileName,
                    attachmentStreamProfile.getDsSize().longValue(),
                    description, XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_DESCRIPTION_COMPLEMENT),
                    availability, licence, attachmentStreamProfile.getDsMIME(), embargoAvailability,
                    embargoEndDate, embargoPeriod, submissionDate, attachmentStreamProfile.getDsCreateDate(),
                    (i + 1) * 10);
            this.documentFileService.save(documentFile);
          } else {
            // Check if the order of the document file is correct as well as the license, accesslevel, embargo and documentFileType has not
            // changed.
            DocumentFile documentFile = this.setDocumentFilesProperties(documentFileOpt.get(), licence, (i + 1) * 10, availability,
                    embargoAvailability, embargoEndDate, embargoPeriod, description, submissionDate);
            this.documentFileService.save(documentFile);
          }
        } else {
          if (publication.getThumbnail() == null || !publication.getThumbnail().getFileName().equals(fileName) || !publication.getThumbnail()
                  .getFileSize().toString().equals(size)) {
            // if publication does not contain thumbnail, create it and if it is not the same it should overwrite it.
            DatastreamProfile attachmentStreamProfile = fedoraClient.getDataStreamProfile(publication.getArchiveId(), datastreamId);
            this.setThumbnail(publication, datastreamId, attachmentStreamProfile.getDsMIME(),
                    attachmentStreamProfile.getDsSize().longValue(),
                    fileName);
          }
        }
      }

      // Clean all document files that are not in admin
      if (!publication.getDocumentFiles().isEmpty()) {
        for (Iterator<DocumentFile> iterator = publication.getDocumentFiles().iterator(); iterator.hasNext(); ) {
          DocumentFile df = iterator.next();
          boolean deleteFile = true;
          for (int i = 0; i < attachmentList.getLength(); i++) {
            Node attachmentNode = attachmentList.item(i);
            String attachmentFileName = XMLTool.getFirstTextContent(attachmentNode, XPATH_FILENAME);
            String attachmentSize = XMLTool.getFirstTextContent(attachmentNode, XPATH_SIZE);
            String datastreamId = XMLTool.getFirstTextContent(attachmentList.item(i), XPATH_DATASTREAM_ID);

            if (!StringTool.isNullOrEmpty(attachmentFileName) && !StringTool.isNullOrEmpty(attachmentSize)) {
              if (df.getFileName().equals(attachmentFileName) && df.getFileSize().toString().equals(attachmentSize)) {
                deleteFile = false;
                break;
              }
            } else {
              // Case of old publication with incomplete ADMIN datastream. We compare only on file size
              String dsSize = this.getDatastreamSize(publication, datastreamId);
              if (df.getFileSize().toString().equals(dsSize)) {
                deleteFile = false;
                break;
              }
            }
          }

          if (deleteFile) {
            this.documentFileService.delete(df.getResId());
            iterator.remove();
          }
        }
      }

      // Clean thumbnail if there is one in local but not in fedora, otherwise that would have been updated before
      if (publication.getThumbnail() != null && IntStream.range(0, attachmentList.getLength()).mapToObj(attachmentList::item)
              .noneMatch(att -> XMLTool.getFirstTextContent(att, XPATH_DESCRIPTION).equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE))) {
        publication.setThumbnail(null);
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException(
              "Unable to get attachement list from ADMIN datastream for object '" + publication.getArchiveId() + "'", e);
    }
  }

  private void setArchiveIdIfEmpty(Fedora3Client fedora3Client, Publication publication) throws JAXBException {
    if (StringTool.isNullOrEmpty(publication.getArchiveId())) {
      // Get next PID (~identifier)
      String publicationPid = fedora3Client.getNextPID(this.fedoraPrefix);
      publication.setArchiveId(publicationPid);
      log.info("Fedora publication PID: {}", publicationPid);
    }
  }

  private void createArchiveIfNotExist(Fedora3Client fedora3Client, Publication publication) throws JAXBException {
    if (fedora3Client.check(publication.getArchiveId()) == HttpStatus.NOT_FOUND) {
      String result = fedora3Client.ingest(publication.getArchiveId(), publication.getTitle());
      if (!result.equals(publication.getArchiveId())) {
        throw new SolidifyProcessingException("Wrong PID: " + publication.getArchiveId() + " != " + result);
      }
      log.info("Fedora object {} created", publication.getArchiveId());
    }
  }

  private Document getAdminDataStreamDocument(Fedora3Client fedora3Client, Publication publication) {
    return fedora3Client.getDataStreamContentAsXml(publication.getArchiveId(), ADMIN_DATASTREAM_NAME);
  }

  private void cleanAttachmentsFromFedora(Fedora3Client fedora3Client, Publication publication, Document oldAdminDatastreamDocument) {
    // Delete all Attachment and thumbnails datastreams not declared in adminDataStream
    if (oldAdminDatastreamDocument != null) {
      try {
        XPath xpath = XMLTool.buildXPath();
        NodeList oldAttachmentList = (NodeList) xpath.compile(XPATH_ADMIN_ATTACHMENT)
                .evaluate(oldAdminDatastreamDocument, XPathConstants.NODESET);

        Document currentAttachmentListDoc = fedora3Client.getDataStreamContentAsXml(publication.getArchiveId(), ADMIN_DATASTREAM_NAME);
        NodeList currentAttachmentList = (NodeList) xpath.compile(XPATH_ADMIN_ATTACHMENT)
                .evaluate(currentAttachmentListDoc, XPathConstants.NODESET);

        IntStream.range(0, oldAttachmentList.getLength()).mapToObj(oldAttachmentList::item).forEach(oldNode -> {
          String oldFileName = XMLTool.getFirstTextContent(oldNode, XPATH_FILENAME);
          final String oldSize = XMLTool.getFirstTextContent(oldNode, XPATH_SIZE);
          String oldAttachmentId = XMLTool.getFirstTextContent(oldNode, XPATH_DATASTREAM_ID);

          boolean deleteDs = true;

          for (int i = 0; i < currentAttachmentList.getLength(); i++) {
            Node n = currentAttachmentList.item(i);
            String nodeFilename = XMLTool.getFirstTextContent(n, XPATH_FILENAME);
            String nodeSize = XMLTool.getFirstTextContent(n, XPATH_SIZE);

            if (!StringTool.isNullOrEmpty(oldFileName) && !StringTool.isNullOrEmpty(oldSize)) {
              if (nodeFilename.equals(oldFileName) && nodeSize.equals(oldSize)) {
                deleteDs = false;
                break;
              } else {
                // Some data are wrong in ADMIN datastream -> we do another check to prevent deleting files while it should not.
                // Instead of getting the size in ADMIN datastream, we get the real size of the file in the datastream itself
                String currentDsSize = this.getDatastreamSize(publication, oldAttachmentId);
                if (nodeFilename.equals(oldFileName) && nodeSize.equals(currentDsSize)) {
                  // file names match. Size in the new ADMIN
                  deleteDs = false;
                  break;
                } else if (nodeSize.equals(currentDsSize)) {
                  // Size is the same, but filename is different. It is maybe another file with the same size.
                  // We delete the file
                } else {
                  // filename and size are different
                  // We delete the file
                }
              }
            } else {
              // Case of old publication with incomplete ADMIN datastream --> we delete a datastream only if no one with the same size exists anymore
              String dsOldSize = this.getDatastreamSize(publication, oldAttachmentId);
              if (nodeSize.equals(dsOldSize)) {
                deleteDs = false;
                break;
              }
            }
          }

          if (deleteDs) {
            // If no datastream with same size anymore -> delete it
            fedora3Client.deleteManagedDataStream(publication.getArchiveId(), oldAttachmentId, publication.getTitle());
            log.info("Datastream '{}' deleted for object {} ({})", oldAttachmentId, publication.getArchiveId(), publication.getResId());
          }
        });

      } catch (XPathExpressionException e) {
        throw new SolidifyRuntimeException(
                "Unable to get attachement list from ADMIN datastream for object '" + publication.getArchiveId() + "'",
                e);
      }
    }
  }

  private void ingestAttachments(Fedora3Client fedora3Client, Publication publication) {
    try {
      // get the attachments infos present in ADMIN datastream to ensure they correspond to the ATTACHMENTs to create
      Document adminDatastreamDocument = fedora3Client.getDataStreamContentAsXml(publication.getArchiveId(), ADMIN_DATASTREAM_NAME);
      XPath xpath = XMLTool.buildXPath();
      NodeList attachmentsList = (NodeList) xpath.compile(XPATH_ADMIN_ATTACHMENT).evaluate(adminDatastreamDocument, XPathConstants.NODESET);

      int index = 0;
      index = this.ingestAttachmentsForDocumentFiles(fedora3Client, publication, attachmentsList, index);
      this.ingestAttachmentsForThumbnail(fedora3Client, publication, attachmentsList, index);

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to get Attachment list from ADMIN datastream for object '" + publication.getArchiveId() + "'",
              e);
    }
  }

  private int ingestAttachmentsForDocumentFiles(Fedora3Client fedora3Client, Publication publication, NodeList attachmentsList, int index) {
    for (DocumentFile documentFile : publication.getDocumentFiles()) {

      Node attachmentNode = attachmentsList.item(index);
      String datastreamIdInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_DATASTREAM_ID);
      String fileNameInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_FILENAME);
      String sizeInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_SIZE);
      String descriptionInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_DESCRIPTION);

      index++;
      String fileName = documentFile.getFileName();
      String size = documentFile.getFileSize().toString();
      String fileType = documentFile.getDocumentFileType().getValue();

      if (fileName.equals(fileNameInAdmin) && sizeInAdmin.equals(size) && descriptionInAdmin
              .equals(fileType)) {
        HttpStatus status = fedora3Client.checkDatastream(publication.getArchiveId(), datastreamIdInAdmin);
        // If the attachment already exits in Fedora, do nothing.
        if (status == HttpStatus.NOT_FOUND) {
          log.info("Datastream '{}' will be created for object {} ({}) with the content of file '{}' ({})", datastreamIdInAdmin,
                  publication.getArchiveId(), publication.getResId(), fileName, documentFile.getFinalData());
          this.addManagedDataStream(fedora3Client, publication.getArchiveId(), datastreamIdInAdmin, fileName, documentFile.getFinalData());
        }
      } else {
        String reason = "";
        if (!fileName.equals(fileNameInAdmin)) {
          reason += "(filename: " + fileName + " != " + fileNameInAdmin + ")";
        }
        if (!sizeInAdmin.equals(size)) {
          reason += "(size: " + sizeInAdmin + " != " + size + ")";
        }
        if (!descriptionInAdmin.equals(fileType)) {
          reason += "(type: " + descriptionInAdmin + " != " + fileType + ")";
        }

        throw new SolidifyRuntimeException(
                "'" + datastreamIdInAdmin + "' described in ADMIN datastream does not correspond to the attachment '" + datastreamIdInAdmin
                        + "' to ingest " + reason);
      }
    }
    return index;
  }

  private void ingestAttachmentsForThumbnail(Fedora3Client fedora3Client, Publication publication, NodeList attachmentsList, int index) {
    if (publication.getThumbnail() != null) {

      Node attachmentNode = attachmentsList.item(index);
      String datastreamIdInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_DATASTREAM_ID);
      String fileNameInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_FILENAME);
      String sizeInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_SIZE);
      String descriptionInAdmin = XMLTool.getFirstTextContent(attachmentNode, XPATH_DESCRIPTION);

      String fileName = publication.getThumbnail().getFileName();
      String size = publication.getThumbnail().getFileSize().toString();
      String fileType = FileType.VIGNETTE.value();

      if (fileName.equals(fileNameInAdmin) && sizeInAdmin.equals(size) && descriptionInAdmin
              .equals(fileType)) {
        ByteArrayResource content = new ByteArrayResource(publication.getThumbnail().getFileContent()) {
          @Override
          public String getFilename() {
            return fileName; // Filename has to be returned in order to be able to be posted
          }
        };
        HttpStatus status = fedora3Client.checkDatastream(publication.getArchiveId(), datastreamIdInAdmin);
        if (status == HttpStatus.NOT_FOUND) {
          log.info("Datastream '{}' will be created for object {} ({}) with the content of file '{}' ", datastreamIdInAdmin,
                  publication.getArchiveId(), publication.getResId(), fileName);
          this.addManagedDataStream(fedora3Client, publication.getArchiveId(), datastreamIdInAdmin, fileName, content);
        }
      } else {
        throw new SolidifyRuntimeException(
                "'" + datastreamIdInAdmin + "' described in ADMIN datastream does not correspond to the attachment '" + datastreamIdInAdmin
                        + "' to ingest '");
      }
    }
  }

  private void saveMetadata(Fedora3Client fedora3Client, Publication publication) {
    Path metadataFolder = this.getMetadataFolder(publication.getResId());
    for (Path xmlFile : FileTool.scanFolder(metadataFolder, Files::isRegularFile)) {
      if (xmlFile.getFileName().toString().endsWith(SolidifyConstants.XML_EXT)) {
        String dsId = xmlFile.getFileName().toString().substring(0, xmlFile.getFileName().toString().lastIndexOf(".")).toUpperCase();
        HttpStatus status = fedora3Client.checkDatastream(publication.getArchiveId(), dsId);
        if (status == HttpStatus.OK) {
          this.updateXmlDataStream(fedora3Client, publication.getArchiveId(), dsId, publication.getTitle(), xmlFile);
        } else if (status == HttpStatus.NOT_FOUND) {
          this.addXmlDataStream(fedora3Client, publication.getArchiveId(), dsId, publication.getTitle(), xmlFile);
        }
      }
    }
  }

  /************************************************************************************************/

  private void addXmlDataStream(Fedora3Client fedora3Client, String archiveId, String dsId, String title, Path xmlFile) {
    DatastreamProfile dsProfile = fedora3Client
            .addXmlDataStream(archiveId, dsId, title, xmlFile.toUri());
    log.info("Fedora XML datastream {} {} ({}) added (size={}, mimeType={})", archiveId, dsId, title, dsProfile.getDsSize(),
            dsProfile.getDsMIME());
  }

  private void addManagedDataStream(Fedora3Client fedora3Client, String archiveId, String attachmentId, String fileName,
          ByteArrayResource finalData) {
    DatastreamProfile dsProfile = fedora3Client
            .addManagedDataStream(archiveId, attachmentId, fileName, finalData);
    log.info("Fedora Managed datastream {} {} ({}) added (size={}, mimeType={})", archiveId, attachmentId, fileName, dsProfile.getDsSize(),
            dsProfile.getDsMIME());
  }

  private void addManagedDataStream(Fedora3Client fedora3Client, String archiveId, String attachmentId, String fileName, URI finalData) {
    DatastreamProfile dsProfile = fedora3Client
            .addManagedDataStream(archiveId, attachmentId, fileName, finalData);
    log.info("Fedora Managed datastream {} {} ({}) added (size={}, mimeType={})", archiveId, attachmentId, fileName, dsProfile.getDsSize(),
            dsProfile.getDsMIME());
  }

  private void updateXmlDataStream(Fedora3Client fedora3Client, String archiveId, String dsId, String fileName, Path xmlFile) {
    DatastreamProfile dsProfile = fedora3Client
            .updateXmlDataStream(archiveId, dsId, fileName, xmlFile);
    log.info("Fedora XML datastream {} {} ({}) updated (size={}, mimeType={})", archiveId, dsId, fileName, dsProfile.getDsSize(),
            dsProfile.getDsMIME());
  }

  private void checkFedoraServer() {
    // Server info
    try {
      Fedora3Client fedoraClient = this.getFedoraClient();
      FedoraRepository repo = fedoraClient.getServerInfo();
      log.info("Fedora server version: {}", repo.getRepositoryVersion());
    } catch (Exception e) {
      throw new SolidifyRuntimeException("Error in connecting to Fedora server", e);
    }
  }

  private Fedora3Client getFedoraClient() {
    return new Fedora3Client(this.fedoraUrl, this.fedoraUser, this.fedoraPassword,
            this.metadataService.getAouXmlNamespacePrefixMapper(AouMetadataVersion.getDefaultVersion()));
  }

  private Path getMetadataFolder(String resId) {
    Path publicationPath = this.publicationService.getPublicationFilesFolderPath(resId);
    return Paths.get(publicationPath.toString(), AouConstants.METADATA_FOLDER);
  }

  private Marshaller getJaxbMarshaller() throws JAXBException {
    Marshaller marshaller = this.jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    return marshaller;
  }

  private Object deserialize(String xmlString) throws JAXBException {
    final StringReader newMd = new StringReader(xmlString);
    return this.jaxbContext.createUnmarshaller().unmarshal(newMd);
  }

  private String serialize(Object xmlObject) throws JAXBException {
    StringWriter sw = new StringWriter();
    Marshaller marshaller = this.getJaxbMarshaller();
    marshaller.marshal(xmlObject, sw);
    return sw.toString();
  }

  private void saveToFile(Path filePath, String fileContent) throws IOException {
    try (FileOutputStream file = new FileOutputStream(filePath.toFile())) {
      file.write(fileContent.getBytes());
    }
  }

  private void loadXsl() throws IOException {
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(this.getClass().getClassLoader());
    for (final org.springframework.core.io.Resource xsl : resolver
            .getResources("classpath*:/" + AouConstants.XSL_HOME + "/" + AouConstants.AOU.toLowerCase() + "2*.xsl")) {
      String fileName = xsl.getFilename();
      if (fileName == null) {
        throw new SolidifyRuntimeException("Resource " + xsl.getURI() + " has no filename");
      }
      log.info("Finding XML transformation: {}", fileName);
      this.xlstList.put(fileName.substring(fileName.indexOf("2") + 1, fileName.indexOf(".")),
              StreamUtils.copyToString(xsl.getInputStream(), Charset.defaultCharset()));
    }
  }

  /**
   * Generate XML completed with metadata required by the current public interface in production
   *
   * @param publication
   * @return
   */
  private String getCompletedPivotMetadataForTransformation(Publication publication) {
    try {
      Document document = XMLTool.parseXML(publication.getMetadata());
      this.completeDepositorInfos(publication, document);
      this.completePublicationDates(publication, document);
      this.completeEmbargoPeriods(publication, document);
      this.completeStructuresNamesWithAncestorsNames(publication, document);
      return XMLTool.documentToString(document);
    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException | TransformerException e) {
      throw new SolidifyRuntimeException(e.getMessage());
    }
  }

  /**
   * Add depositor informations
   *
   * @param publication
   * @param doc
   * @throws XPathExpressionException
   */
  private void completeDepositorInfos(Publication publication, Document doc) throws XPathExpressionException {
    XPath xpath = this.getDepositDocXpath(publication);
    Node rootNode = (Node) xpath.compile("./aou_deposit:deposit_doc").evaluate(doc, XPathConstants.NODE);

    String personId = publication.getCreatorId();
    User user = this.userService.findByPersonResId(personId).get(0);

    String ns = this.metadataService.getAouXmlNamespacePrefixMapper(publication.getMetadataVersion()).getNamespacePrefixes()
            .get(AouConstants.XML_NAMESPACE_DEPOSIT_PREFIX);
    XMLTool.addChildNodeNS(rootNode, ns, "depositor_last_name", user.getLastName());
    XMLTool.addChildNodeNS(rootNode, ns, "depositor_first_name", user.getFirstName());
    XMLTool.addChildNodeNS(rootNode, ns, "depositor_unique_id", user.getUniqueId());
    XMLTool.addChildNodeNS(rootNode, ns, "depositor_email", user.getEmail());
  }

  /**
   * Add deposit creation and validation dates
   *
   * @param publication
   * @param doc
   * @throws XPathExpressionException
   */
  private void completePublicationDates(Publication publication, Document doc) throws XPathExpressionException {
    XPath xpath = this.getDepositDocXpath(publication);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    Node rootNode = (Node) xpath.compile("./aou_deposit:deposit_doc").evaluate(doc, XPathConstants.NODE);
    String ns = this.metadataService.getAouXmlNamespacePrefixMapper(publication.getMetadataVersion()).getNamespacePrefixes()
            .get(AouConstants.XML_NAMESPACE_DEPOSIT_PREFIX);
    XMLTool.addChildNodeNS(rootNode, ns, "creation_date", publication.getCreationTime().format(formatter));
    XMLTool.addChildNodeNS(rootNode, ns, "validation_date", OffsetDateTime.now().format(formatter));
  }

  /**
   * Calculate and add eventual embargoes durations in months from the embargo end date
   *
   * @param doc
   * @throws XPathExpressionException
   */
  private void completeEmbargoPeriods(Publication publication, Document doc) throws XPathExpressionException {
    XPath xpath = this.getDepositDocXpath(publication);
    NodeList fileNodes = (NodeList) xpath.compile("/aou_deposit:deposit_doc/aou_deposit:files/aou_deposit:file")
            .evaluate(doc, XPathConstants.NODESET);
    for (int i = 0; i < fileNodes.getLength(); i++) {
      Node fileNode = fileNodes.item(i);
      Node embargoNode = (Node) xpath.compile("./aou_deposit:embargo").evaluate(fileNode, XPathConstants.NODE);
      if (embargoNode != null) {
        String endDateStr = (String) xpath.compile("./aou_deposit:end_date").evaluate(embargoNode, XPathConstants.STRING);
        int months = this.calculateMonthsDifferenceFromToday(endDateStr);
        String ns = this.metadataService.getAouXmlNamespacePrefixMapper(publication.getMetadataVersion()).getNamespacePrefixes()
                .get(AouConstants.XML_NAMESPACE_DEPOSIT_PREFIX);
        XMLTool.addChildNodeNS(embargoNode, ns, "duration_in_months", String.valueOf(months));
      }
    }
  }

  /**
   * For each metadata structure, prepend the structures path from top structure to metadata structure. e.g: "Département de biologie
   * moléculaire" --> "Faculté des sciences; Section de biologie; Département de biologie moléculaire"
   *
   * @param publication
   * @param doc
   * @throws XPathExpressionException
   */
  private void completeStructuresNamesWithAncestorsNames(Publication publication, Document doc) throws XPathExpressionException {
    XPath xpath = this.getDepositDocXpath(publication);
    NodeList academicStructuresNodes = (NodeList) xpath
            .compile("/aou_deposit:deposit_doc/aou_deposit:academic_structures/aou_deposit:academic_structure")
            .evaluate(doc, XPathConstants.NODESET);
    for (int i = 0; i < academicStructuresNodes.getLength(); i++) {
      Node structureNode = academicStructuresNodes.item(i);
      String structureId = (String) xpath.compile("./aou_deposit:resId").evaluate(structureNode, XPathConstants.STRING);
      List<Structure> ancestorStructures = this.structureService.getAncestorStructures(structureId);
      StringBuilder fullPathNameBuilder = new StringBuilder();
      ListIterator<Structure> li = ancestorStructures.listIterator(ancestorStructures.size());
      while (li.hasPrevious()) {
        fullPathNameBuilder.append(li.previous().getName()).append("; ");
      }
      String structureName = (String) xpath.compile("./aou_deposit:name").evaluate(structureNode, XPathConstants.STRING);
      fullPathNameBuilder.append(structureName);
      Node structureNameNode = (Node) xpath.compile("./aou_deposit:name").evaluate(structureNode, XPathConstants.NODE);
      structureNameNode.setTextContent(fullPathNameBuilder.toString());
    }
  }

  private int calculateMonthsDifferenceFromToday(String endDateStr) {
    LocalDate endDate = LocalDate.parse(endDateStr).withDayOfMonth(1);

    // add one month to ensure the end date has passed
    endDate = endDate.plusMonths(1);

    LocalDate todayDate = LocalDate.now().withDayOfMonth(1);
    int months = Period.between(todayDate, endDate).getMonths();
    int years = Period.between(todayDate, endDate).getYears();
    return months + 12 * years;
  }

  private XPath getDepositDocXpath(Publication publication) {
    AouXmlNamespacePrefixMapper prefixMapper = this.metadataService.getAouXmlNamespacePrefixMapper(publication.getMetadataVersion());
    return XMLTool.buildXPath(prefixMapper);
  }

  private String getDatastreamSize(Publication publication, String attachmentId) {
    Fedora3Client fedora3Client = this.getFedoraClient();
    DatastreamProfile dsProfile = fedora3Client.getDataStreamProfile(publication.getArchiveId(), attachmentId);
    return dsProfile.getDsSize().toString();
  }

  /************************************************************************************************/

  public File getAttachmentFromFedora(String fedoraId, String dataStreamName, Path path) {
    Fedora3Client fedoraClient = this.getFedoraClient();
    return fedoraClient.getAttachment(fedoraId, dataStreamName, path);
  }

  public DigitalObject getFoxml(String pid) {
    Fedora3Client fedoraClient = this.getFedoraClient();
    return fedoraClient.getFoxml(pid);
  }

  public URI getDatastreamContentUrl(String pid, String datastreamName) {
    return URI.create(this.fedoraUrl + "objects/" + pid + "/datastreams/" + datastreamName + "/content");
  }

  public String getDataStreamContent(String pid, String dsId) {
    Fedora3Client fedoraClient = this.getFedoraClient();
    return fedoraClient.getDataStreamContent(pid, dsId);
  }

  public DatastreamVersionType getLastVersionOfDatastream(DigitalObject foxml, String datastreamName) {
    for (DatastreamType ds : foxml.getDatastream()) {
      if (ds.getID().equals(datastreamName)) {
        return this.getLastVersionOfDatastream(ds);
      }
    }
    throw new SolidifyRuntimeException("'" + datastreamName + "' datastream not found for object " + foxml.getPID());
  }

  public DatastreamVersionType getLastVersionOfDatastream(DatastreamType ds) {
    return ds.getDatastreamVersion().get(ds.getDatastreamVersion().size() - 1);
  }

  public List<DatastreamType> getAttachmentsDatastreams(Publication publication) {
    DigitalObject foxml = this.getFoxml(publication.getArchiveId());
    List<DatastreamType> datastreams = foxml.getDatastream();

    return datastreams.stream().filter(ds -> {
      for (String prefix : FILE_DATASTREAM_PREFIXES) {
        if (ds.getID().startsWith(prefix)) {
          return true;
        }
      }
      return false;
    }).toList();

  }

  /************************************************************************************************/

  public String getTempFolder() {
    return this.tempFolder;
  }

  public DocumentFile setDocumentFile(Publication publication, String datastreamId, String fileName, Long fileSize, String description,
          String descriptionComplement, String availability, String license, String mimeType, String embargoAvailability,
          String embargoEndDate, Integer embargoPeriod, String submissionDate, OffsetDateTime creationDate, int sortValue) {

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setFileName(fileName);
    documentFile.setFileSize(fileSize);
    documentFile.setLabel(descriptionComplement);
    documentFile.setMimetype(mimeType);

    documentFile = this.setDocumentFilesProperties(documentFile, license, sortValue, availability, embargoAvailability, embargoEndDate,
            embargoPeriod, description, submissionDate);

    URI fileUri = this.getDatastreamContentUrl(publication.getArchiveId(), datastreamId);
    documentFile.setSourceData(fileUri);
    documentFile.getCreation().setWhen(creationDate);
    return documentFile;
  }

  public DocumentFile setDocumentFilesProperties(DocumentFile documentFile, String license, int sortValue, String availability,
          String embargoAvailability, String embargoEndDate, Integer embargoPeriod, String description, String submissionDate) {

    if (documentFile.getSortValue() == null || !documentFile.getSortValue().equals(sortValue)) {
      documentFile.setSortValue(sortValue);
    }
    DocumentFileType documentFileType = this.convertFileTypeDocToDocumentFileType(description);
    if (documentFile.getDocumentFileType() != documentFileType) {
      documentFile.setDocumentFileType(documentFileType);
    }
    DocumentFile.AccessLevel accessLevel = this.getAccessLevel(availability);
    if (documentFile.getAccessLevel() != accessLevel) {
      documentFile.setAccessLevel(accessLevel);
    }
    License licence = this.licenseService.getLicenseFromType(license);
    if (documentFile.getLicense() != licence) {
      documentFile.setLicense(licence);
    }

    if (!StringTool.isNullOrEmpty(embargoAvailability)) {
      DocumentFile.AccessLevel embargoAccessLevel = this.getAccessLevel(embargoAvailability);
      if (documentFile.getEmbargoAccessLevel() != embargoAccessLevel) {
        documentFile.setEmbargoAccessLevel(embargoAccessLevel);
      }
      if (!StringTool.isNullOrEmpty(embargoEndDate)) {
        if (documentFile.getEmbargoEndDate() != LocalDate.parse(embargoEndDate)) {
          documentFile.setEmbargoEndDate(LocalDate.parse(embargoEndDate));
        }
      } else if (embargoPeriod != null && !StringTool.isNullOrEmpty(submissionDate)) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDate endDate = LocalDate.parse(submissionDate, inputFormatter).plusMonths(embargoPeriod);
        if (documentFile.getEmbargoEndDate() != endDate) {
          documentFile.setEmbargoEndDate(endDate);
        }
      }
    }

    return documentFile;
  }

  public void setThumbnail(Publication publication, String datastreamId, String mimeType, Long size, String fileName) {

    if (StringTool.isNullOrEmpty(fileName)) {
      String extension = this.getFileExtension(mimeType);
      if (!StringTool.isNullOrEmpty(extension)) {
        fileName = "thumbnail." + extension;
      } else {
        fileName = "thumbnail";
      }
    }

    PublicationThumbnail thumbnail = new PublicationThumbnail();
    thumbnail.setFileName(fileName);
    thumbnail.setFileSize(size);
    thumbnail.setMimeType(mimeType);

    Path tmpFolder = Paths.get(this.getTempFolder()).resolve(publication.getResId());
    if (FileTool.ensureFolderExists(tmpFolder)) {
      Path tmpFile = tmpFolder.resolve(Objects.requireNonNull(fileName));
      File fileContent2 = this.getAttachmentFromFedora(publication.getArchiveId(), datastreamId, tmpFile);
      try {
        thumbnail.setFileContent(Files.readAllBytes(fileContent2.toPath()));
      } catch (IOException e) {
        throw new SolidifyRuntimeException("Unable to read thumbnail for object" + publication.getArchiveId());
      }
    }

    publication.setThumbnail(thumbnail);
  }

  public String getFileExtension(String mimetype) {
    if (!StringTool.isNullOrEmpty(mimetype)) {
      return switch (mimetype) {
        case "application/pdf" -> "pdf";
        case "image/gif" -> "gif";
        case "image/jpg", "image/jpeg" -> "jpg";
        case "image/bmp" -> "bmp";
        case "application/msword" -> "doc";
        case "application/vnd.openxmlformats-officedocument.wordprocessingml.document" -> "docx";
        case "application/msexcel" -> "xls";
        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" -> "xlsx";
        case "application/mspowerpoint" -> "ppt";
        case "application/vnd.openxmlformats-officedocument.presentationml.presentation" -> "pptx";
        case "text/csv" -> "csv";
        case "text/plain" -> "txt";
        case "application/xml" -> "xml";
        default -> null;
      };
    }
    return null;
  }

  public DocumentFile.AccessLevel getAccessLevel(String availabilityInAdmin) {
    if (!StringTool.isNullOrEmpty(availabilityInAdmin)) {
      return switch (availabilityInAdmin) {
        case "Non diffusé" -> DocumentFile.AccessLevel.PRIVATE;
        case "Intranet" -> DocumentFile.AccessLevel.RESTRICTED;
        default -> DocumentFile.AccessLevel.PUBLIC;
      };
    } else {
      return DocumentFile.AccessLevel.PUBLIC;
    }
  }

  public DocumentFileType convertFileTypeDocToDocumentFileType(String typeName) {
    DocumentFileType documentFileType = this.documentFileTypeService.convertFileTypeToDocumentFileType(typeName);
    if (documentFileType == null) {
      throw new SolidifyRuntimeException("No DocumentFileType could be found for type '" + typeName + "'");
    }
    return documentFileType;
  }

}
