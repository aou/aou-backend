/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataDifferencesService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import static java.util.stream.Collectors.groupingBy;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.util.ReflectionUtils;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataCorrection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataDifference;
import ch.unige.aou.model.publication.MetadataEmbargo;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.MetadataFunding;
import ch.unige.aou.model.publication.MetadataLink;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

public abstract class MetadataDifferencesService implements MetadataComparator {

  private final MetadataExtractor metadataExtractor;
  private final MessageService messageService;

  public MetadataDifferencesService(MetadataExtractor metadataExtractor, MessageService messageService) {
    this.metadataExtractor = metadataExtractor;
    this.messageService = messageService;
  }

  @Override
  public Map<String, Map<String, List<MetadataDifference>>> getPendingUpdates(Publication publication, boolean simplifiedVersion) {
    Map<String, Map<String, List<MetadataDifference>>> fieldDifferences = new HashMap<>();
    final Object depositDocBefore = this.metadataExtractor.createDepositDocObjectFromXml(publication.getMetadataBackup());
    final Object depositDocAfter = this.metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());

    this.checkOriginalTitle(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkTitle(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkTypes(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkDates(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkIdentifiers(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkPages(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkPublisherVersionUrl(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkNote(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkDoctor(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkLocalNumber(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkDiscipline(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkMandator(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkEdition(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkAward(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkAouCollection(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkPublisher(depositDocBefore, depositDocAfter, fieldDifferences);
    this.checkContainer(depositDocBefore, depositDocAfter, fieldDifferences);

    if (!simplifiedVersion) {
      this.checkLanguages(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkContributors(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkAbstracts(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkCorrections(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkFunding(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkDatasets(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkKeywords(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkFiles(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkLinks(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkGroups(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkStructures(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkCollections(depositDocBefore, depositDocAfter, fieldDifferences);
    } else {
      this.checkLanguagesSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkContributorsSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkAbstractsSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkCorrectionsSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkFundingSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkFilesSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkDatasetsSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkKeywordsSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkLinksSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkGroupsSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkStructuresSimplified(depositDocBefore, depositDocAfter, fieldDifferences);
      this.checkCollectionsSimplified(depositDocBefore, depositDocAfter, fieldDifferences);

    }

    return fieldDifferences;
  }

  private void checkContainer(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    // Check each container field: title, editor, volume...
    List<MetadataDifference> listDifferences = new ArrayList<>();

    String containerTitleBefore = this.metadataExtractor.getContainerTitle(depositDocBefore);
    String containerTitleAfter = this.metadataExtractor.getContainerTitle(depositDocAfter);

    if ((containerTitleBefore != null && !containerTitleBefore.equals(containerTitleAfter)) || (containerTitleAfter != null
            && !containerTitleAfter.equals(containerTitleBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("title"), containerTitleBefore, containerTitleAfter);
      listDifferences.add(difference);
    }

    String containerEditorBefore = this.metadataExtractor.getContainerEditor(depositDocBefore);
    String containerEditorAfter = this.metadataExtractor.getContainerEditor(depositDocAfter);

    if ((containerEditorBefore != null && !containerEditorBefore.equals(containerEditorAfter)) || (containerEditorAfter != null
            && !containerEditorAfter.equals(containerEditorBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("editor"), containerEditorBefore, containerEditorAfter);
      listDifferences.add(difference);
    }

    String containerVolumeBefore = this.metadataExtractor.getContainerVolume(depositDocBefore);
    String containerVolumeAfter = this.metadataExtractor.getContainerVolume(depositDocAfter);

    if ((containerVolumeBefore != null && !containerVolumeBefore.equals(containerVolumeAfter)) || (containerVolumeAfter != null
            && !containerVolumeAfter.equals(containerVolumeBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("volume"), containerVolumeBefore, containerVolumeAfter);
      listDifferences.add(difference);
    }

    String containerIssueBefore = this.metadataExtractor.getContainerIssue(depositDocBefore);
    String containerIssueAfter = this.metadataExtractor.getContainerIssue(depositDocAfter);

    if ((containerIssueBefore != null && !containerIssueBefore.equals(containerIssueAfter)) || (containerIssueAfter != null
            && !containerIssueAfter.equals(containerIssueBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("issue"), containerIssueBefore, containerIssueAfter);
      listDifferences.add(difference);
    }

    String containerSpecialIssueBefore = this.metadataExtractor.getContainerSpecialIssue(depositDocBefore);
    String containerSpecialIssueAfter = this.metadataExtractor.getContainerSpecialIssue(depositDocAfter);

    if ((containerSpecialIssueBefore != null && !containerSpecialIssueBefore.equals(containerSpecialIssueAfter)) || (
            containerSpecialIssueAfter != null && !containerSpecialIssueAfter.equals(containerSpecialIssueBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("specialIssue"), containerSpecialIssueBefore,
              containerSpecialIssueAfter);
      listDifferences.add(difference);
    }

    String conferenceSubtitleBefore = this.metadataExtractor.getContainerConferenceSubtitle(depositDocBefore);
    String conferenceSubtitleAfter = this.metadataExtractor.getContainerConferenceSubtitle(depositDocAfter);

    if ((conferenceSubtitleBefore != null && !conferenceSubtitleBefore.equals(conferenceSubtitleAfter)) || (conferenceSubtitleAfter != null
            && !conferenceSubtitleAfter.equals(conferenceSubtitleBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("conferenceSubtitle"), conferenceSubtitleBefore,
              conferenceSubtitleAfter);
      listDifferences.add(difference);
    }

    String conferenceDateBefore = this.metadataExtractor.getContainerConferenceDate(depositDocBefore);
    String conferenceDateAfter = this.metadataExtractor.getContainerConferenceDate(depositDocAfter);

    if ((conferenceDateBefore != null && !conferenceDateBefore.equals(conferenceDateAfter)) || (conferenceDateAfter != null
            && !conferenceDateAfter.equals(conferenceDateBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("conferenceDate"), conferenceDateBefore,
              conferenceDateAfter);
      listDifferences.add(difference);
    }

    String conferencePlaceBefore = this.metadataExtractor.getContainerConferencePlace(depositDocBefore);
    String conferencePlaceAfter = this.metadataExtractor.getContainerConferencePlace(depositDocAfter);

    if ((conferencePlaceBefore != null && !conferencePlaceBefore.equals(conferencePlaceAfter)) || (conferencePlaceAfter != null
            && !conferencePlaceAfter.equals(conferencePlaceBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("conferencePlace"), conferencePlaceBefore,
              conferencePlaceAfter);
      listDifferences.add(difference);
    }
    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("container"), listDifferences);
      fieldDifferences.put(this.messageService.get("container"), differencesMap);
    }
  }

  private void checkStructures(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<Structure> structureListBefore = this.metadataExtractor.getStructures(depositDocBefore);
    List<Structure> structuresListAfter = this.metadataExtractor.getStructures(depositDocAfter);

    if (!structureListBefore.isEmpty() || !structuresListAfter.isEmpty()) {
      int maxLength = Math.max(structureListBefore.size(), structuresListAfter.size());
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        Structure before = structureListBefore.isEmpty() || structureListBefore.size() < i + 1 ? new Structure() : structureListBefore.get(i);
        Structure after = structuresListAfter.isEmpty() || structuresListAfter.size() < i + 1 ? new Structure() : structuresListAfter.get(i);
        if (!before.equals(after)) {
          this.addIfNotEqual(before.getName(), after.getName(), "name", listDifferences);
          this.addIfNotEqual(before.getCodeStruct(), after.getCodeStruct(), "code", listDifferences);
          differencesMap.put(this.messageService.get("structure") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("structures"), differencesMap);
      }
    }
  }

  private void checkGroups(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<ResearchGroup> groupsListBefore = this.metadataExtractor.getResearchGroups(depositDocBefore);
    List<ResearchGroup> groupsListeAfter = this.metadataExtractor.getResearchGroups(depositDocAfter);

    if (!groupsListBefore.isEmpty() || !groupsListeAfter.isEmpty()) {
      int maxLength = Math.max(groupsListBefore.size(), groupsListeAfter.size());
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        ResearchGroup before = groupsListBefore.isEmpty() || groupsListBefore.size() < i + 1 ? new ResearchGroup() : groupsListBefore.get(i);
        ResearchGroup after = groupsListeAfter.isEmpty() || groupsListeAfter.size() < i + 1 ? new ResearchGroup() : groupsListeAfter.get(i);
        if (!before.equals(after)) {
          this.addIfNotEqual(before.getName(), after.getName(), "name", listDifferences);
          this.addIfNotEqual(before.getCode(), after.getCode(), "code", listDifferences);
          differencesMap.put(this.messageService.get("group") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("groups"), differencesMap);
      }
    }
  }

  private void checkPublisher(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String publisherNameBefore = this.metadataExtractor.getPublisherName(depositDocBefore);
    String publisherNameAfter = this.metadataExtractor.getPublisherName(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((publisherNameBefore != null && !publisherNameBefore.equals(publisherNameAfter)) || (publisherNameAfter != null && !publisherNameAfter
            .equals(publisherNameBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("publisherName"), publisherNameBefore, publisherNameAfter);
      listDifferences.add(difference);
    }
    String publisherPlaceBefore = this.metadataExtractor.getPublisherPlace(depositDocBefore);
    String publisherPlaceAfter = this.metadataExtractor.getPublisherPlace(depositDocAfter);

    if ((publisherPlaceBefore != null && !publisherPlaceBefore.equals(publisherPlaceAfter)) || (publisherPlaceAfter != null
            && !publisherPlaceAfter.equals(publisherPlaceBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("publisherPlace"), publisherPlaceBefore,
              publisherPlaceAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("publisher"), listDifferences);
      fieldDifferences.put(this.messageService.get("publisher"), differencesMap);
    }
  }

  private void checkLinks(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataLink> linksListBefore = this.metadataExtractor.getLinks(depositDocBefore);
    List<MetadataLink> linksListAfter = this.metadataExtractor.getLinks(depositDocAfter);

    if (!linksListBefore.isEmpty() || !linksListAfter.isEmpty()) {
      int maxLength = Math.max(linksListBefore.size(), linksListAfter.size());
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        MetadataLink before = linksListBefore.isEmpty() || linksListBefore.size() < i + 1 ? new MetadataLink() : linksListBefore.get(i);
        MetadataLink after = linksListAfter.isEmpty() || linksListAfter.size() < i + 1 ? new MetadataLink() : linksListAfter.get(i);
        if (!before.equals(after)) {
          this.addIfNotEqual(before.getTarget(), after.getTarget(), "target", listDifferences);
          this.addIfNotEqual(this.getFieldValueOrNull(MetadataLink.LinkType.class, "type", before.getType()),
                  this.getFieldValueOrNull(MetadataLink.LinkType.class, "type", after.getType()),
                  "type", listDifferences);

          MetadataTextLang descriptionBefore = before.getDescription();
          MetadataTextLang descriptionAfter = after.getDescription();
          this.addIfNotEqual(this.getFieldValueOrNull(MetadataTextLang.class, "content", descriptionBefore),
                  this.getFieldValueOrNull(MetadataTextLang.class, "content", descriptionAfter),
                  "description.content", listDifferences);
          this.addIfNotEqual(this.getFieldValueOrNull(MetadataTextLang.class, "lang", descriptionBefore),
                  this.getFieldValueOrNull(MetadataTextLang.class, "lang", descriptionAfter),
                  "description.lang", listDifferences);
          differencesMap.put(this.messageService.get("link") + "." + (i + 1), listDifferences);
        }
      }

      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("corrections"), differencesMap);
      }
    }
  }

  private void checkCollections(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataCollection> collectionsBefore = this.metadataExtractor.getCollections(depositDocBefore);
    List<MetadataCollection> collectionsAfter = this.metadataExtractor.getCollections(depositDocAfter);

    if (!collectionsBefore.isEmpty() || !collectionsAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      int maxLength = Math.max(collectionsBefore.size(), collectionsAfter.size());

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        MetadataCollection before =
                collectionsBefore.isEmpty() || collectionsBefore.size() < i + 1 ? new MetadataCollection() : collectionsBefore.get(i);
        MetadataCollection after =
                collectionsAfter.isEmpty() || collectionsAfter.size() < i + 1 ? new MetadataCollection() : collectionsAfter.get(i);

        if (!before.equals(after)) {
          this.addIfNotEqual(before.getName(), after.getName(), "name", listDifferences);
          this.addIfNotEqual(before.getNumber(), after.getNumber(), "collectionNumber", listDifferences);
          differencesMap.put(this.messageService.get("collection") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("collections"), differencesMap);
      }
    }
  }

  private void checkAouCollection(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String aouCollectionBefore = this.metadataExtractor.getAouCollection(depositDocBefore);
    String aouCollectionAfter = this.metadataExtractor.getAouCollection(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((aouCollectionBefore != null && !aouCollectionBefore.equals(aouCollectionAfter)) || (aouCollectionAfter != null && !aouCollectionAfter
            .equals(aouCollectionBefore))) {
      MetadataDifference difference = new MetadataDifference(null, aouCollectionBefore, aouCollectionAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("aouCollection"), listDifferences);
      fieldDifferences.put(this.messageService.get("aouCollection"), differencesMap);
    }
  }

  private void checkAward(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String awardBefore = this.metadataExtractor.getAward(depositDocBefore);
    String awardAfter = this.metadataExtractor.getAward(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((awardBefore != null && !awardBefore.equals(awardAfter)) || (awardAfter != null && !awardAfter.equals(awardBefore))) {
      MetadataDifference difference = new MetadataDifference(null, awardBefore, awardAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("award"), listDifferences);
      fieldDifferences.put(this.messageService.get("award"), differencesMap);
    }
  }

  private void checkEdition(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String editionBefore = this.metadataExtractor.getEdition(depositDocBefore);
    String editionAfter = this.metadataExtractor.getEdition(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((editionBefore != null && !editionBefore.equals(editionAfter)) || (editionAfter != null && !editionAfter.equals(editionBefore))) {
      MetadataDifference difference = new MetadataDifference(null, editionBefore, editionAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("edition"), listDifferences);
      fieldDifferences.put(this.messageService.get("edition"), differencesMap);
    }
  }

  private void checkMandator(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String mandatorBefore = this.metadataExtractor.getMandator(depositDocBefore);
    String mandatorAfter = this.metadataExtractor.getMandator(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((mandatorBefore != null && !mandatorBefore.equals(mandatorAfter)) || (mandatorAfter != null && !mandatorAfter.equals(mandatorBefore))) {
      MetadataDifference difference = new MetadataDifference(null, mandatorBefore, mandatorAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("mandator"), listDifferences);
      fieldDifferences.put(this.messageService.get("mandator"), differencesMap);
    }
  }

  private void checkDiscipline(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String disciplineBefore = this.metadataExtractor.getDiscipline(depositDocBefore);
    String disciplineAfter = this.metadataExtractor.getDiscipline(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((disciplineBefore != null && !disciplineBefore.equals(disciplineAfter)) || (disciplineAfter != null && !disciplineAfter
            .equals(disciplineBefore))) {
      MetadataDifference difference = new MetadataDifference(null, disciplineBefore, disciplineAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("discipline"), listDifferences);
      fieldDifferences.put(this.messageService.get("discipline"), differencesMap);
    }
  }

  private void checkFiles(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataFile> fileListBefore = this.metadataExtractor.getFiles(depositDocBefore);
    List<MetadataFile> fileListAfter = this.metadataExtractor.getFiles(depositDocAfter);

    if (!fileListBefore.isEmpty() || !fileListAfter.isEmpty()) {
      int maxLength = Math.max(fileListBefore.size(), fileListAfter.size());
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        MetadataFile before = fileListBefore.isEmpty() || fileListBefore.size() < i + 1 ? new MetadataFile() : fileListBefore.get(i);
        MetadataFile after = fileListAfter.isEmpty() || fileListAfter.size() < i + 1 ? new MetadataFile() : fileListAfter.get(i);
        if (!before.equals(after)) {
          this.addIfNotEqual(before.getType(), after.getType(), "type", listDifferences);
          this.addIfNotEqual(before.getName(), after.getName(), "name", listDifferences);
          this.addIfNotEqual(this.convertBigIntegerToStringOrNull(before.getSize()), this.convertBigIntegerToStringOrNull(after.getSize()),
                  "size", listDifferences);
          this.addIfNotEqual(before.getMimeType(), after.getMimeType(), "mimeType", listDifferences);
          this.addIfNotEqual(before.getLicense(), after.getLicense(), "license", listDifferences);
          this.addIfNotEqual(before.getLabel(), after.getLabel(), "label", listDifferences);
          this.addIfNotEqual(before.getAccessLevel(), after.getAccessLevel(), "accessLevel", listDifferences);

          //Check embargo
          MetadataEmbargo embargoBefore = before.getEmbargo();
          MetadataEmbargo embargoAfter = after.getEmbargo();
          if (embargoBefore != null || embargoAfter != null) {
            this.addIfNotEqual(this.getFieldValueOrNull(MetadataEmbargo.class, "accessLevel", embargoBefore),
                    this.getFieldValueOrNull(MetadataEmbargo.class, "accessLevel", embargoAfter),
                    "embargoAccessLevel", listDifferences);
            this.addIfNotEqual(this.getFieldValueOrNull(MetadataEmbargo.class, "endDate", embargoBefore),
                    this.getFieldValueOrNull(MetadataEmbargo.class, "endDate", embargoAfter),
                    "embargoEndDate", listDifferences);
          }
          differencesMap.put(this.messageService.get("file") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("files"), differencesMap);
      }
    }
  }

  private void checkKeywords(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<String> keywordsBefore = this.metadataExtractor.getKeywords(depositDocBefore);
    List<String> keywordsAfter = this.metadataExtractor.getKeywords(depositDocAfter);

    if (!keywordsBefore.isEmpty() || !keywordsAfter.isEmpty()) {
      int maxLength = Math.max(keywordsBefore.size(), keywordsAfter.size());
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        String before = keywordsBefore == null || keywordsBefore.size() < i + 1 ? null : keywordsBefore.get(i);
        String after = keywordsAfter == null || keywordsAfter.size() < i + 1 ? null : keywordsAfter.get(i);
        if ((before != null || after != null) && !Objects.equals(before, after)) {
          MetadataDifference dataset = new MetadataDifference(null, before, after);
          listDifferences.add(dataset);
          differencesMap.put(this.messageService.get("keyword") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("keywords"), differencesMap);
      }
    }
  }

  private void checkDatasets(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<String> datasetsBefore = this.metadataExtractor.getDatasets(depositDocBefore);
    List<String> datasetAfter = this.metadataExtractor.getDatasets(depositDocAfter);

    if (!datasetsBefore.isEmpty() || !datasetAfter.isEmpty()) {
      int maxLength = Math.max(datasetsBefore.size(), datasetAfter.size());
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        String before = datasetsBefore.isEmpty() || datasetsBefore.size() < i + 1 ? null : datasetsBefore.get(i);
        String after = datasetAfter.isEmpty() || datasetAfter.size() < i + 1 ? null : datasetAfter.get(i);
        if ((before != null || after != null) && !Objects.equals(before, after)) {
          MetadataDifference dataset = new MetadataDifference(null, before, after);
          listDifferences.add(dataset);
          differencesMap.put(this.messageService.get("dataset") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("datasets"), differencesMap);
      }
    }
  }

  private void checkFunding(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataFunding> fundingBefore = this.metadataExtractor.getFundings(depositDocBefore);
    List<MetadataFunding> fundingAfter = this.metadataExtractor.getFundings(depositDocAfter);

    if (!fundingBefore.isEmpty() || !fundingAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      int maxLength = Math.max(fundingBefore.size(), fundingAfter.size());

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        MetadataFunding before = fundingBefore.isEmpty() || fundingBefore.size() < i + 1 ? new MetadataFunding() : fundingBefore.get(i);
        MetadataFunding after = fundingAfter.isEmpty() || fundingAfter.size() < i + 1 ? new MetadataFunding() : fundingAfter.get(i);
        if (!before.equals(after)) {
          this.addIfNotEqual(before.getFunder(), after.getFunder(), "funder", listDifferences);
          this.addIfNotEqual(before.getName(), after.getName(), "name", listDifferences);
          this.addIfNotEqual(before.getAcronym(), after.getAcronym(), "acronym", listDifferences);
          this.addIfNotEqual(before.getCode(), after.getCode(), "code", listDifferences);
          differencesMap.put(this.messageService.get("funding") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("fundings"), differencesMap);
      }
    }
  }

  private void checkCorrections(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataCorrection> correctionsBefore = this.metadataExtractor.getCorrections(depositDocBefore);
    List<MetadataCorrection> correctionsAfter = this.metadataExtractor.getCorrections(depositDocAfter);

    if (!correctionsBefore.isEmpty() || !correctionsAfter.isEmpty()) {
      int maxLength = Math.max(correctionsBefore.size(), correctionsAfter.size());
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        MetadataCorrection before =
                correctionsBefore.isEmpty() || correctionsBefore.size() < i + 1 ? new MetadataCorrection() : correctionsBefore.get(i);
        MetadataCorrection after =
                correctionsAfter.isEmpty() || correctionsAfter.size() < i + 1 ? new MetadataCorrection() : correctionsAfter.get(i);
        if (!before.equals(after)) {
          listDifferences.clear();
          this.addIfNotEqual(before.getDoi(), after.getDoi(), "doi", listDifferences);
          this.addIfNotEqual(before.getPmid(), after.getPmid(), "pmid", listDifferences);
          this.addIfNotEqual(before.getNote(), after.getNote(), "note", listDifferences);
          differencesMap.put(this.messageService.get("correction") + "." + (i + 1), listDifferences);
        }
      }
      if (differencesMap.size() > 0) {
        fieldDifferences.put(this.messageService.get("corrections"), differencesMap);
      }
    }
  }

  private void checkDoctor(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataDifference> listDifferences = new ArrayList<>();

    String doctorAddressBefore = this.metadataExtractor.getDoctorAddress(depositDocBefore);
    String doctorAddressAfter = this.metadataExtractor.getDoctorAddress(depositDocAfter);

    if ((doctorAddressBefore != null && !doctorAddressBefore.equals(doctorAddressAfter)) || (doctorAddressAfter != null && !doctorAddressAfter
            .equals(doctorAddressBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("address"), doctorAddressBefore, doctorAddressAfter);
      listDifferences.add(difference);
    }

    String doctorEmailBefore = this.metadataExtractor.getDoctorEmail(depositDocBefore);
    String doctorEmailAfter = this.metadataExtractor.getDoctorEmail(depositDocAfter);

    if ((doctorEmailBefore != null && !doctorEmailBefore.equals(doctorEmailAfter)) || (doctorEmailAfter != null && !doctorEmailAfter
            .equals(doctorEmailBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("email"), doctorEmailBefore, doctorEmailAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("doctor"), listDifferences);
      fieldDifferences.put(this.messageService.get("doctor"), differencesMap);
    }
  }

  private void checkOriginalTitle(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String titleBefore = this.metadataExtractor.getOriginalTitleContent(depositDocBefore);
    String titleAfter = this.metadataExtractor.getOriginalTitleContent(depositDocAfter);

    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((titleBefore != null && !titleBefore.equals(titleAfter)) || (titleAfter != null && !titleAfter.equals(titleBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("content"), titleBefore, titleAfter);
      listDifferences.add(difference);
    }

    String titleLangBefore = this.metadataExtractor.getOriginalTitleLang(depositDocBefore);
    String titleLangAfter = this.metadataExtractor.getOriginalTitleLang(depositDocBefore);

    if ((titleLangBefore != null && !titleLangBefore.equals(titleLangAfter)) || (titleLangAfter != null && !titleLangAfter
            .equals(titleLangBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("lang"), titleLangBefore, titleLangAfter);
      listDifferences.add(difference);
    }
    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("originalTitle"), listDifferences);
      fieldDifferences.put(this.messageService.get("originalTitle"), differencesMap);
    }
  }

  private void checkTypes(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataDifference> listDifferences = new ArrayList<>();

    String typeNameBefore = this.metadataExtractor.getType(depositDocBefore);
    String subtypeNameBefore = this.metadataExtractor.getSubtype(depositDocBefore);
    String subSubtypeNameBefore = this.metadataExtractor.getSubSubtype(depositDocBefore);

    String typeNameAfter = this.metadataExtractor.getType(depositDocAfter);
    String subtypeNameAfter = this.metadataExtractor.getSubtype(depositDocAfter);
    String subSubtypeNameAfter = this.metadataExtractor.getSubSubtype(depositDocAfter);

    if (!typeNameBefore.equals(typeNameAfter)) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("type"), typeNameBefore, typeNameAfter);
      listDifferences.add(difference);
    }

    if (!subtypeNameBefore.equals(subtypeNameAfter)) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("subType"), subtypeNameBefore, subtypeNameAfter);
      listDifferences.add(difference);
    }

    if ((StringTool.isNullOrEmpty(subSubtypeNameBefore) && !StringTool.isNullOrEmpty(subSubtypeNameAfter)) ||
            (!StringTool.isNullOrEmpty(subSubtypeNameBefore) && StringTool.isNullOrEmpty(subSubtypeNameAfter)) ||
            (!StringTool.isNullOrEmpty(subSubtypeNameBefore) && !StringTool.isNullOrEmpty(subSubtypeNameAfter) &&
                    !subSubtypeNameBefore.equals(subSubtypeNameAfter))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("subSubType"), subSubtypeNameBefore, subSubtypeNameAfter);
      listDifferences.add(difference);
    }
    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("types"), listDifferences);
      fieldDifferences.put(this.messageService.get("types"), differencesMap);
    }
  }

  private void checkTitle(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataDifference> listDifferences = new ArrayList<>();

    String titleBefore = this.metadataExtractor.getTitleContent(depositDocBefore);
    String titleAfter = this.metadataExtractor.getTitleContent(depositDocAfter);

    if (!titleBefore.equals(titleAfter)) {
      MetadataDifference difference = new MetadataDifference(null, titleBefore, titleAfter);
      listDifferences.add(difference);
    }
    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("title"), listDifferences);
      fieldDifferences.put(this.messageService.get("title"), differencesMap);
    }
  }

  private void checkLanguages(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<String> languagesListBefore = this.metadataExtractor.getLanguages(depositDocBefore);
    List<String> languagesListAfter = this.metadataExtractor.getLanguages(depositDocAfter);

    List<MetadataDifference> listDifferences = new ArrayList<>();

    if (!languagesListBefore.isEmpty() || !languagesListAfter.isEmpty()) {
      languagesListBefore.stream().filter(element -> !languagesListAfter.contains(element)).forEach(e -> {
        MetadataDifference difference = new MetadataDifference(this.messageService.get(e), e, null);
        listDifferences.add(difference);
      });
      languagesListAfter.stream().filter(element -> !languagesListBefore.contains(element)).forEach(e -> {
        MetadataDifference difference = new MetadataDifference(this.messageService.get(e), null, e);
        listDifferences.add(difference);
      });
      if (!listDifferences.isEmpty()) {
        Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
        differencesMap.put(this.messageService.get("languages"), listDifferences);
        fieldDifferences.put(this.messageService.get("languages"), differencesMap);
      }
    }
  }

  private void checkDates(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataDates> metadataDatesListBefore = this.metadataExtractor.getDates(depositDocBefore);
    List<MetadataDates> metadataDatesListAfter = this.metadataExtractor.getDates(depositDocAfter);

    if (!metadataDatesListBefore.isEmpty() || !metadataDatesListAfter.isEmpty()) {
      // Check by type of date
      List<MetadataDifference> listDifferences = new ArrayList<>();
      List<String> typesList = Stream.concat(metadataDatesListBefore.stream(), metadataDatesListAfter.stream())
              .collect(groupingBy(MetadataDates::getType))
              .entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
      for (String type : typesList) {
        String dateBefore = metadataDatesListBefore.stream().filter(e -> e.getType().equals(type)).map(MetadataDates::getContent).findFirst()
                .orElse("");
        String dateAfter = metadataDatesListAfter.stream().filter(e -> e.getType().equals(type)).map(MetadataDates::getContent).findFirst()
                .orElse("");
        if (!dateBefore.equals(dateAfter)) {
          MetadataDifference difference = new MetadataDifference(this.messageService.get(type), dateBefore, dateAfter);
          listDifferences.add(difference);
        }
      }
      if (!listDifferences.isEmpty()) {
        Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
        differencesMap.put(this.messageService.get("date"), listDifferences);
        fieldDifferences.put(this.messageService.get("date"), differencesMap);
      }
    }

  }

  private void checkIdentifiers(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    // Check each identifier: doi, pmid, pmcid, isbn..
    List<MetadataDifference> listDifferences = new ArrayList<>();
    String doiBefore = this.metadataExtractor.getDOI(depositDocBefore);
    String doiAfter = this.metadataExtractor.getDOI(depositDocAfter);

    if ((doiBefore != null && !doiBefore.equals(doiAfter)) || (doiAfter != null && !doiAfter.equals(doiBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("doi"), doiBefore, doiAfter);
      listDifferences.add(difference);
    }

    String pmidBefore = (this.metadataExtractor.getPmid(depositDocBefore) == null) ?
            null :
            this.metadataExtractor.getPmid(depositDocBefore).toString();
    String pmidAfter = (this.metadataExtractor.getPmid(depositDocAfter) == null) ?
            null : this.metadataExtractor.getPmid(depositDocAfter).toString();

    if ((pmidBefore != null && !pmidBefore.equals(pmidAfter)) || (pmidAfter != null && !pmidAfter.equals(pmidBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("pmid"), pmidBefore, pmidAfter);
      listDifferences.add(difference);
    }

    String isbnBefore = this.metadataExtractor.getISBN(depositDocBefore);
    String isbnAfter = this.metadataExtractor.getISBN(depositDocAfter);

    if ((isbnBefore != null && !isbnBefore.equals(isbnAfter)) || (isbnAfter != null && !isbnAfter.equals(isbnBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("isbn"), isbnBefore, isbnAfter);
      listDifferences.add(difference);
    }

    String issnBefore = this.metadataExtractor.getISSN(depositDocBefore);
    String issnAfter = this.metadataExtractor.getISSN(depositDocAfter);

    if ((issnBefore != null && !issnBefore.equals(issnAfter)) || (issnAfter != null && !issnAfter.equals(issnBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("issn"), issnBefore, issnAfter);
      listDifferences.add(difference);
    }

    String arxivIdBefore = this.metadataExtractor.getArxivId(depositDocBefore);
    String arxivIdAfter = this.metadataExtractor.getArxivId(depositDocAfter);

    if ((arxivIdBefore != null && !arxivIdBefore.equals(arxivIdAfter)) || (arxivIdAfter != null && !arxivIdAfter.equals(arxivIdBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("arxiv"), arxivIdBefore, arxivIdAfter);
      listDifferences.add(difference);
    }

    String mmsidBefore = this.metadataExtractor.getMmsid(depositDocBefore);
    String mmsidAfter = this.metadataExtractor.getMmsid(depositDocAfter);

    if ((mmsidBefore != null && !mmsidBefore.equals(mmsidAfter)) || (mmsidAfter != null && !mmsidAfter.equals(mmsidBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("mmsid"), mmsidBefore, mmsidAfter);
      listDifferences.add(difference);
    }

    String repecBefore = this.metadataExtractor.getRepec(depositDocBefore);
    String repecAfter = this.metadataExtractor.getRepec(depositDocAfter);

    if ((repecBefore != null && !repecBefore.equals(repecAfter)) || (repecAfter != null && !repecAfter.equals(repecBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("repec"), repecBefore, repecAfter);
      listDifferences.add(difference);
    }

    String dbplBefore = this.metadataExtractor.getDblp(depositDocBefore);
    String dbplAfter = this.metadataExtractor.getDblp(depositDocAfter);

    if ((dbplBefore != null && !dbplBefore.equals(dbplAfter)) || (dbplAfter != null && !dbplAfter.equals(dbplBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("dbpl"), dbplBefore, dbplAfter);
      listDifferences.add(difference);
    }

    String urnBefore = this.metadataExtractor.getURN(depositDocBefore);
    String urnAfter = this.metadataExtractor.getURN(depositDocAfter);

    if ((urnBefore != null && !urnBefore.equals(urnAfter)) || (urnAfter != null && !urnAfter.equals(urnBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("urn"), urnBefore, urnAfter);
      listDifferences.add(difference);
    }

    String pmcidBefore = this.metadataExtractor.getPmcId(depositDocBefore);
    String pmcidAfter = this.metadataExtractor.getPmcId(depositDocAfter);

    if ((pmcidBefore != null && !pmcidBefore.equals(pmcidAfter)) || (pmcidAfter != null && !pmcidAfter.equals(pmcidBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("pmcid"), pmcidBefore, pmcidAfter);
      listDifferences.add(difference);
    }
    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("identifiers"), listDifferences);
      fieldDifferences.put(this.messageService.get("identifiers"), differencesMap);
    }
  }

  private void checkAbstracts(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataTextLang> metadataTextLangListBefore = this.metadataExtractor.getAbstracts(depositDocBefore);
    List<MetadataTextLang> metadataTextLangListAfter = this.metadataExtractor.getAbstracts(depositDocAfter);

    List<MetadataDifference> listDifferences = new ArrayList<>();
    if (!metadataTextLangListBefore.isEmpty() || !metadataTextLangListAfter.isEmpty()) {
      List<String> abstractLanguagesList = Stream.concat(metadataTextLangListBefore.stream(), metadataTextLangListAfter.stream())
              .filter(mtl -> mtl.getLang() != null)
              .collect(groupingBy(MetadataTextLang::getLang))
              .entrySet().stream().map(Map.Entry::getKey).collect(Collectors.toList());
      for (String lang : abstractLanguagesList) {
        String abstractBefore = metadataTextLangListBefore.stream().filter(e -> e.getLang() == null || e.getLang().equals(lang))
                .map(MetadataTextLang::getContent)
                .findFirst()
                .orElse("");
        String abstractAfter = metadataTextLangListAfter.stream().filter(e -> e.getLang() == null || e.getLang().equals(lang))
                .map(MetadataTextLang::getContent)
                .findFirst()
                .orElse("");
        if (!abstractBefore.equals(abstractAfter)) {
          MetadataDifference difference = new MetadataDifference(this.messageService.get(lang), abstractBefore, abstractAfter);
          listDifferences.add(difference);
        }
      }
      if (!listDifferences.isEmpty()) {
        Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
        differencesMap.put(this.messageService.get("abstract"), listDifferences);
        fieldDifferences.put(this.messageService.get("abstract"), differencesMap);
      }
    }
  }

  private void checkContributors(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    //Check first all contributors and then all collaborations
    List<AbstractContributor> listContributorBefore = this.metadataExtractor.getContributors(depositDocBefore).stream()
            .filter(d -> d instanceof ContributorDTO)
            .collect(Collectors.toList());
    List<AbstractContributor> listContributorAfter = this.metadataExtractor.getContributors(depositDocAfter).stream()
            .filter(d -> d instanceof ContributorDTO)
            .collect(Collectors.toList());
    Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

    if (!listContributorBefore.isEmpty() || !listContributorAfter.isEmpty()) {
      int maxLength = Math.max(listContributorBefore.size(), listContributorAfter.size());

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        ContributorDTO contributorBeforeUpdate = listContributorBefore.isEmpty() || listContributorBefore.size() < i + 1 ? new ContributorDTO() :
                (ContributorDTO) listContributorBefore.get(i);
        ContributorDTO contributorAfterUpdate = listContributorAfter.isEmpty() || listContributorAfter.size() < i + 1 ? new ContributorDTO() :
                (ContributorDTO) listContributorAfter.get(i);
        if (this.contributorHasMetadataUpdate(contributorBeforeUpdate, contributorAfterUpdate)) {
          this.addIfNotEqual(contributorBeforeUpdate.getFirstName(), contributorAfterUpdate.getFirstName(), "firstname", listDifferences);
          this.addIfNotEqual(contributorBeforeUpdate.getLastName(), contributorAfterUpdate.getLastName(), "lastname", listDifferences);
          this.addIfNotEqual(contributorBeforeUpdate.getEmail(), contributorAfterUpdate.getEmail(), "email", listDifferences);
          this.addIfNotEqual(contributorBeforeUpdate.getInstitution(), contributorAfterUpdate.getInstitution(), "institution", listDifferences);
          this.addIfNotEqual(contributorBeforeUpdate.getRole(), contributorAfterUpdate.getRole(), "role", listDifferences);
          this.addIfNotEqual(contributorBeforeUpdate.getOrcid(), contributorAfterUpdate.getOrcid(), "orcid", listDifferences);
          this.addIfNotEqual(contributorBeforeUpdate.getCnIndividu(), contributorAfterUpdate.getCnIndividu(), "cnIndividu", listDifferences);
          this.addIfNotEqual(contributorBeforeUpdate.getStructure(), contributorAfterUpdate.getStructure(), "structure", listDifferences);
          differencesMap.put(this.messageService.get("contributor") + "." + (i + 1), listDifferences);
        }
      }
    }

    List<AbstractContributor> listCollaborationsBefore = this.metadataExtractor.getContributors(depositDocBefore).stream()
            .filter(CollaborationDTO.class::isInstance).collect(Collectors.toList());
    List<AbstractContributor> listCollaborationsAfter = this.metadataExtractor.getContributors(depositDocAfter).stream()
            .filter(CollaborationDTO.class::isInstance).collect(Collectors.toList());

    if (!listCollaborationsBefore.isEmpty() || !listCollaborationsAfter.isEmpty()) {
      int maxLength = Math.max(listCollaborationsBefore.size(), listCollaborationsAfter.size());

      for (int i = 0; i < maxLength; i++) {
        List<MetadataDifference> listDifferences = new ArrayList<>();
        CollaborationDTO collaborationBefore =
                listCollaborationsBefore.isEmpty() || listCollaborationsBefore.size() < i + 1 ? new CollaborationDTO() :
                        (CollaborationDTO) listCollaborationsBefore.get(i);
        CollaborationDTO collaborationAfter =
                listCollaborationsAfter.isEmpty() || listCollaborationsAfter.size() < i + 1 ? new CollaborationDTO() :
                        (CollaborationDTO) listCollaborationsAfter.get(i);
        if (!collaborationBefore.equals(collaborationAfter)) {
          MetadataDifference name = new MetadataDifference(this.messageService.get("name"), collaborationBefore.getName(),
                  collaborationAfter.getName());

          listDifferences.add(name);
          differencesMap.put(this.messageService.get("collaboration") + "." + (i + 1), listDifferences);
        }
      }
    }
    if (differencesMap.size() > 0) {
      fieldDifferences.put(this.messageService.get("contributors"), differencesMap);
    }
  }

  private boolean contributorHasMetadataUpdate(ContributorDTO contributor, ContributorDTO comparedContributor) {
    return !Objects.equals(contributor.getFirstName(), comparedContributor.getFirstName())
            || !Objects.equals(contributor.getLastName(), comparedContributor.getLastName())
            || !Objects.equals(contributor.getEmail(), comparedContributor.getEmail())
            || !Objects.equals(contributor.getInstitution(), comparedContributor.getInstitution())
            || !Objects.equals(contributor.getRole(), comparedContributor.getRole())
            || !Objects.equals(contributor.getOrcid(), comparedContributor.getOrcid())
            || !Objects.equals(contributor.getCnIndividu(), comparedContributor.getCnIndividu())
            || !Objects.equals(contributor.getStructure(), comparedContributor.getStructure());
  }

  private void checkPages(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String pagingBefore = this.metadataExtractor.getPaging(depositDocBefore);
    String pagingAfter = this.metadataExtractor.getPaging(depositDocAfter);

    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((pagingBefore != null && !pagingBefore.equals(pagingAfter)) || (pagingAfter != null && !pagingAfter.equals(pagingBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("paging"), pagingBefore, pagingAfter);
      listDifferences.add(difference);
    }

    String pagingOtherBefore = this.metadataExtractor.getPagingOther(depositDocBefore);
    String pagingOtherAfter = this.metadataExtractor.getPagingOther(depositDocAfter);

    if ((pagingOtherBefore != null && !pagingOtherBefore.equals(pagingOtherAfter)) || (pagingOtherAfter != null && !pagingOtherAfter
            .equals(pagingOtherBefore))) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get("pagingOther"), pagingOtherBefore, pagingOtherAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("pages"), listDifferences);
      fieldDifferences.put(this.messageService.get("pages"), differencesMap);
    }
  }

  private void checkPublisherVersionUrl(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String publisherVersionUrlBefore = this.metadataExtractor.getPublisherVersionUrl(depositDocBefore);
    String publisherVersionUrlAfter = this.metadataExtractor.getPublisherVersionUrl(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((publisherVersionUrlBefore != null && !publisherVersionUrlBefore.equals(publisherVersionUrlAfter)) ||
            (publisherVersionUrlAfter != null && !publisherVersionUrlAfter.equals(publisherVersionUrlBefore))) {
      MetadataDifference difference = new MetadataDifference(null, publisherVersionUrlBefore, publisherVersionUrlAfter);
      listDifferences.add(difference);
    }
    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("publisherVersionUrl"), listDifferences);
      fieldDifferences.put(this.messageService.get("publisherVersionUrl"), differencesMap);
    }
  }

  private void checkNote(Object depositDocBefore, Object depositDocAfter, Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String noteBefore = this.metadataExtractor.getNote(depositDocBefore);
    String noteAfter = this.metadataExtractor.getNote(depositDocAfter);

    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((noteBefore != null && !noteBefore.equals(noteAfter)) || (noteAfter != null && !noteAfter.equals(noteBefore))) {
      MetadataDifference difference = new MetadataDifference(null, noteBefore, noteAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("note"), listDifferences);
      fieldDifferences.put(this.messageService.get("note"), differencesMap);
    }
  }

  private void checkLocalNumber(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    String localNumberBefore = this.metadataExtractor.getLocalNumber(depositDocBefore);
    String localNumberAfter = this.metadataExtractor.getLocalNumber(depositDocAfter);
    List<MetadataDifference> listDifferences = new ArrayList<>();

    if ((localNumberBefore != null && !localNumberBefore.equals(localNumberAfter)) || (localNumberAfter != null && !localNumberAfter
            .equals(localNumberBefore))) {
      MetadataDifference difference = new MetadataDifference(null, localNumberBefore, localNumberAfter);
      listDifferences.add(difference);
    }

    if (!listDifferences.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new HashMap<>();
      differencesMap.put(this.messageService.get("localNumber"), listDifferences);
      fieldDifferences.put(this.messageService.get("localNumber"), differencesMap);
    }
  }

  private void checkFilesSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataFile> fileListBefore = this.metadataExtractor.getFiles(depositDocBefore);
    List<MetadataFile> fileListAfter = this.metadataExtractor.getFiles(depositDocAfter);
    if (!fileListBefore.isEmpty() || !fileListAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<MetadataFile> filesDeleted = fileListBefore.stream().filter(f -> !fileListAfter.contains(f)).collect(Collectors.toList());
      List<MetadataFile> filesAdded = fileListAfter.stream().filter(f -> !fileListBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < filesDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("fileDeleted", new Object[] { fileListBefore.indexOf(filesDeleted.get(i)) + 1, filesDeleted.get(i).getName() }), null);
      }
      for (int i = 0; i < filesAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("fileAdded", new Object[] { fileListAfter.indexOf(filesAdded.get(i)) + 1, filesAdded.get(i).getName() }), null);
      }
      if (filesAdded.isEmpty() && filesDeleted.isEmpty() && !fileListBefore.equals(fileListAfter)) {
        differencesMap.put(this.messageService
                .get("filesChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("files"), differencesMap);
      }
    }
  }

  private void checkFundingSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataFunding> fundingBefore = this.metadataExtractor.getFundings(depositDocBefore);
    List<MetadataFunding> fundingAfter = this.metadataExtractor.getFundings(depositDocAfter);

    if (!fundingBefore.isEmpty() || !fundingAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<MetadataFunding> fundingsDeleted = fundingBefore.stream().filter(f -> !fundingAfter.contains(f)).collect(Collectors.toList());
      List<MetadataFunding> fundingsAdded = fundingAfter.stream().filter(f -> !fundingBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < fundingsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                        .get("fundingDeleted", new Object[] { fundingBefore.indexOf(fundingsDeleted.get(i)) + 1, fundingsDeleted.get(i).getName() }),
                null);
      }
      for (int i = 0; i < fundingsAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("fundingAdded", new Object[] { fundingAfter.indexOf(fundingsAdded.get(i)) + 1, fundingsAdded.get(i).getName() }), null);
      }
      if (fundingsAdded.isEmpty() && fundingsDeleted.isEmpty() && !fundingBefore.equals(fundingAfter)) {
        differencesMap.put(this.messageService
                .get("fundingsChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("fundings"), differencesMap);
      }
    }
  }

  private void checkCorrectionsSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataCorrection> correctionsBefore = this.metadataExtractor.getCorrections(depositDocBefore);
    List<MetadataCorrection> correctionsAfter = this.metadataExtractor.getCorrections(depositDocAfter);

    if (!correctionsBefore.isEmpty() || !correctionsAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<MetadataCorrection> correctionsDeleted = correctionsBefore.stream().filter(f -> !correctionsAfter.contains(f))
              .collect(Collectors.toList());
      List<MetadataCorrection> correctionsAdded = correctionsAfter.stream().filter(f -> !correctionsBefore.contains(f))
              .collect(Collectors.toList());

      for (int i = 0; i < correctionsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("correctionDeleted",
                        new Object[] { correctionsBefore.indexOf(correctionsDeleted.get(i)) + 1, correctionsDeleted.get(i).getNote(),
                                correctionsDeleted.get(i).getDoi(), correctionsDeleted.get(i).getPmid() }), null);
      }
      for (int i = 0; i < correctionsAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("correctionAdded", new Object[] { correctionsAfter.indexOf(correctionsAdded.get(i)) + 1, correctionsAdded.get(i).getNote(),
                        correctionsAdded.get(i).getDoi(), correctionsAdded.get(i).getPmid() }), null);
      }
      if (correctionsAdded.isEmpty() && correctionsDeleted.isEmpty() && !correctionsBefore.equals(correctionsAfter)) {
        differencesMap.put(this.messageService
                .get("correctionsChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("corrections"), differencesMap);
      }
    }
  }

  private void checkLanguagesSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<String> languagesListBefore = this.metadataExtractor.getLanguages(depositDocBefore);
    List<String> languagesListAfter = this.metadataExtractor.getLanguages(depositDocAfter);

    if (!languagesListBefore.isEmpty() || !languagesListAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<String> languagesDeleted = languagesListBefore.stream().filter(f -> !languagesListAfter.contains(f)).collect(Collectors.toList());
      List<String> languagesAdded = languagesListAfter.stream().filter(f -> !languagesListBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < languagesDeleted.size(); i++) {
        differencesMap.put(this.messageService
                        .get("languageDeleted", new Object[] { languagesListBefore.indexOf(languagesDeleted.get(i)) + 1, languagesDeleted.get(i) }),
                null);
      }
      for (int i = 0; i < languagesAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("languageAdded", new Object[] { languagesListAfter.indexOf(languagesAdded.get(i)) + 1, languagesAdded.get(i) }), null);
      }
      if (languagesAdded.isEmpty() && languagesDeleted.isEmpty() && !languagesListBefore.equals(languagesListAfter)) {
        differencesMap.put(this.messageService
                .get("languageChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("languages"), differencesMap);
      }
    }
  }

  private void checkAbstractsSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataTextLang> metadataTextLangListBefore = this.metadataExtractor.getAbstracts(depositDocBefore);
    List<MetadataTextLang> metadataTextLangListAfter = this.metadataExtractor.getAbstracts(depositDocAfter);

    if (!metadataTextLangListBefore.isEmpty() || !metadataTextLangListAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<MetadataTextLang> abstractsDeleted = metadataTextLangListBefore.stream().filter(f -> !metadataTextLangListAfter.contains(f))
              .collect(Collectors.toList());
      List<MetadataTextLang> abstractsAdded = metadataTextLangListAfter.stream().filter(f -> !metadataTextLangListBefore.contains(f))
              .collect(Collectors.toList());

      for (int i = 0; i < abstractsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                        .get("abstractDeleted",
                                new Object[] { metadataTextLangListBefore.indexOf(abstractsDeleted.get(i)) + 1, abstractsDeleted.get(i).getContent() }),
                null);
      }
      for (int i = 0; i < abstractsAdded.size(); i++) {
        differencesMap.put(this.messageService
                        .get("abstractAdded",
                                new Object[] { metadataTextLangListAfter.indexOf(abstractsAdded.get(i)) + 1, abstractsAdded.get(i).getContent() }),
                null);
      }
      if (abstractsAdded.isEmpty() && abstractsDeleted.isEmpty() && !metadataTextLangListBefore.equals(metadataTextLangListAfter)) {
        differencesMap.put(this.messageService
                .get("abstractChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("abstract"), differencesMap);
      }
    }
  }

  private void checkDatasetsSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<String> datasetsBefore = this.metadataExtractor.getDatasets(depositDocBefore);
    List<String> datasetAfter = this.metadataExtractor.getDatasets(depositDocAfter);

    if (!datasetsBefore.isEmpty() || !datasetAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<String> datasetsDeleted = datasetsBefore.stream().filter(f -> !datasetAfter.contains(f)).collect(Collectors.toList());
      List<String> datasetsAdded = datasetAfter.stream().filter(f -> !datasetsBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < datasetsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("datasetDeleted", new Object[] { datasetsBefore.indexOf(datasetsDeleted.get(i)) + 1, datasetsDeleted.get(i) }), null);
      }
      for (int i = 0; i < datasetsAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("datasetAdded", new Object[] { datasetAfter.indexOf(datasetsAdded.get(i)) + 1, datasetsAdded.get(i) }), null);
      }
      if (datasetsAdded.isEmpty() && datasetsDeleted.isEmpty() && !datasetsBefore.equals(datasetAfter)) {
        differencesMap.put(this.messageService
                .get("datasetsChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("datasets"), differencesMap);
      }
    }
  }

  private void checkKeywordsSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<String> keywordsBefore = this.metadataExtractor.getKeywords(depositDocBefore);
    List<String> keywordsAfter = this.metadataExtractor.getKeywords(depositDocAfter);

    if (!keywordsBefore.isEmpty() || !keywordsAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<String> keywordsDeleted = keywordsBefore.stream().filter(f -> !keywordsAfter.contains(f))
              .collect(Collectors.toList());
      List<String> keywordsAdded = keywordsAfter.stream().filter(f -> !keywordsBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < keywordsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("keywordDeleted", new Object[] { keywordsBefore.indexOf(keywordsDeleted.get(i)) + 1, keywordsDeleted.get(i) }), null);
      }
      for (int i = 0; i < keywordsAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("keywordAdded", new Object[] { keywordsAfter.indexOf(keywordsAdded.get(i)) + 1, keywordsAdded.get(i) }), null);
      }
      if (keywordsAdded.isEmpty() && keywordsDeleted.isEmpty() && !keywordsBefore.equals(keywordsAfter)) {
        differencesMap.put(this.messageService
                .get("keywordsChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("keywords"), differencesMap);
      }
    }
  }

  private void checkLinksSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataLink> linksListBefore = this.metadataExtractor.getLinks(depositDocBefore);
    List<MetadataLink> linksListAfter = this.metadataExtractor.getLinks(depositDocAfter);

    if (!linksListBefore.isEmpty() || !linksListAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<MetadataLink> linksDeleted = linksListBefore.stream().filter(f -> !linksListAfter.contains(f)).collect(Collectors.toList());
      List<MetadataLink> linksAdded = linksListAfter.stream().filter(f -> !linksListBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < linksDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("linkDeleted",
                        new Object[] { linksListBefore.indexOf(linksDeleted.get(i)) + 1, linksDeleted.get(i).getDescription().getLang(),
                                linksDeleted.get(i).getDescription().getContent() }), null);
      }
      for (int i = 0; i < linksAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("linkAdded", new Object[] { linksListAfter.indexOf(linksAdded.get(i)) + 1, linksAdded.get(i).getDescription().getLang(),
                        linksAdded.get(i).getDescription().getContent() }), null);
      }
      if (linksAdded.isEmpty() && linksDeleted.isEmpty() && !linksListBefore.equals(linksListAfter)) {
        differencesMap.put(this.messageService
                .get("linksChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("links"), differencesMap);
      }
    }
  }

  private void checkGroupsSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<ResearchGroup> groupsListBefore = this.metadataExtractor.getResearchGroups(depositDocBefore);
    List<ResearchGroup> groupsListeAfter = this.metadataExtractor.getResearchGroups(depositDocAfter);

    if (!groupsListBefore.isEmpty() || !groupsListeAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<ResearchGroup> groupsDeleted = groupsListBefore.stream().filter(f -> !groupsListeAfter.contains(f)).collect(Collectors.toList());
      List<ResearchGroup> groupsAdded = groupsListeAfter.stream().filter(f -> !groupsListBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < groupsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("groupDeleted", new Object[] { groupsListBefore.indexOf(groupsDeleted.get(i)) + 1, groupsDeleted.get(i).getName() }), null);
      }
      for (int i = 0; i < groupsAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("groupAdded", new Object[] { groupsListeAfter.indexOf(groupsAdded.get(i)) + 1, groupsAdded.get(i).getName() }), null);
      }
      if (groupsAdded.isEmpty() && groupsDeleted.isEmpty() && !groupsListBefore.equals(groupsListeAfter)) {
        differencesMap.put(this.messageService
                .get("groupsChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("groups"), differencesMap);
      }
    }
  }

  private void checkStructuresSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<Structure> structureListBefore = this.metadataExtractor.getStructures(depositDocBefore);
    List<Structure> structuresListAfter = this.metadataExtractor.getStructures(depositDocAfter);

    if (!structureListBefore.isEmpty() || !structuresListAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<Structure> structuresDeleted = structureListBefore.stream().filter(f -> !structuresListAfter.contains(f))
              .collect(Collectors.toList());
      List<Structure> structuresAdded = structuresListAfter.stream().filter(f -> !structureListBefore.contains(f)).collect(Collectors.toList());

      for (int i = 0; i < structuresDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("structureDeleted",
                        new Object[] { structureListBefore.indexOf(structuresDeleted.get(i)) + 1, structuresDeleted.get(i).getName() }), null);
      }
      for (int i = 0; i < structuresAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("structureAdded",
                        new Object[] { structuresListAfter.indexOf(structuresAdded.get(i)) + 1, structuresAdded.get(i).getName() }), null);
      }
      if (structuresAdded.isEmpty() && structuresDeleted.isEmpty() && !structureListBefore.equals(structuresListAfter)) {
        differencesMap.put(this.messageService
                .get("structuresChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("structures"), differencesMap);
      }
    }
  }

  private void checkCollectionsSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    List<MetadataCollection> collectionsBefore = this.metadataExtractor.getCollections(depositDocBefore);
    List<MetadataCollection> collectionsAfter = this.metadataExtractor.getCollections(depositDocAfter);

    if (!collectionsBefore.isEmpty() || !collectionsAfter.isEmpty()) {
      Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();
      List<MetadataCollection> collectionsDeleted = collectionsBefore.stream().filter(f -> !collectionsAfter.contains(f))
              .collect(Collectors.toList());
      List<MetadataCollection> collectionsAdded = collectionsAfter.stream().filter(f -> !collectionsBefore.contains(f))
              .collect(Collectors.toList());

      for (int i = 0; i < collectionsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("collectionDeleted",
                        new Object[] { collectionsBefore.indexOf(collectionsDeleted.get(i)) + 1, collectionsDeleted.get(i).getName(),
                                collectionsDeleted.get(i).getNumber() }), null);
      }
      for (int i = 0; i < collectionsAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("collectionAdded", new Object[] { collectionsAfter.indexOf(collectionsAdded.get(i)) + 1, collectionsAdded.get(i).getName(),
                        collectionsAdded.get(i).getNumber() }), null);
      }
      if (collectionsAdded.isEmpty() && collectionsDeleted.isEmpty() && !collectionsBefore.equals(collectionsAfter)) {
        differencesMap.put(this.messageService
                .get("collectionsChangeOrder"), null);
      }
      if (!differencesMap.isEmpty()) {
        fieldDifferences.put(this.messageService.get("collections"), differencesMap);
      }
    }
  }

  private void checkContributorsSimplified(Object depositDocBefore, Object depositDocAfter,
          Map<String, Map<String, List<MetadataDifference>>> fieldDifferences) {
    //Check first all contributors, all collaborators and then all collaborations
    List<ContributorDTO> listAllContributorsBefore = this.metadataExtractor.getContributors(depositDocBefore).stream()
            .filter(ContributorDTO.class::isInstance)
            .map(c -> (ContributorDTO) c)
            .collect(Collectors.toList());
    List<ContributorDTO> listAllContributorsAfter = this.metadataExtractor.getContributors(depositDocAfter).stream()
            .filter(ContributorDTO.class::isInstance)
            .map(c -> (ContributorDTO) c)
            .collect(Collectors.toList());
    Map<String, List<MetadataDifference>> differencesMap = new LinkedHashMap<>();

    List<ContributorDTO> listContributorsBefore = listAllContributorsBefore.stream()
            .filter(c -> !c.getRole().equals(AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR))
            .collect(Collectors.toList());
    List<ContributorDTO> listContributorsAfter = listAllContributorsAfter.stream()
            .filter(c -> !c.getRole().equals(AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR))
            .collect(Collectors.toList());

    List<ContributorDTO> listCollaboratorsBefore = listAllContributorsBefore.stream()
            .filter(c -> c.getRole().equals(AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR))
            .collect(Collectors.toList());
    List<ContributorDTO> listCollaboratorsAfter = listAllContributorsAfter.stream()
            .filter(c -> c.getRole().equals(AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR))
            .collect(Collectors.toList());

    // Contributors
    if (!listContributorsBefore.isEmpty() || !listContributorsAfter.isEmpty()) {
      this.compareContributorsList(listContributorsBefore, listContributorsAfter, differencesMap, "contributor");
    }

    // Collaborators
    if (!listCollaboratorsBefore.isEmpty() || !listCollaboratorsAfter.isEmpty()) {
      this.compareContributorsList(listCollaboratorsBefore, listCollaboratorsAfter, differencesMap, "collaborator");
    }

    // Collaborations
    List<AbstractContributor> listCollaborationsBefore = this.metadataExtractor.getContributors(depositDocBefore).stream()
            .filter(CollaborationDTO.class::isInstance).collect(Collectors.toList());
    List<AbstractContributor> listCollaborationsAfter = this.metadataExtractor.getContributors(depositDocAfter).stream()
            .filter(CollaborationDTO.class::isInstance).collect(Collectors.toList());

    if (!listCollaborationsBefore.isEmpty() || !listCollaborationsAfter.isEmpty()) {
      List<AbstractContributor> collaborationsDeleted = listCollaborationsBefore.stream().filter(f -> !listCollaborationsAfter.contains(f))
              .collect(Collectors.toList());
      List<AbstractContributor> collaborationsAdded = listCollaborationsAfter.stream().filter(f -> !listCollaborationsBefore.contains(f))
              .collect(Collectors.toList());

      for (int i = 0; i < collaborationsDeleted.size(); i++) {
        differencesMap.put(this.messageService
                .get("collaborationDeleted", new Object[] { listCollaborationsBefore.indexOf(collaborationsDeleted.get(i)) + 1,
                        ((CollaborationDTO) collaborationsDeleted.get(i)).getName() }), null);
      }
      for (int i = 0; i < collaborationsAdded.size(); i++) {
        differencesMap.put(this.messageService
                .get("collaborationAdded", new Object[] { listCollaborationsAfter.indexOf(collaborationsAdded.get(i)) + 1,
                        ((CollaborationDTO) collaborationsAdded.get(i)).getName() }), null);
      }
      if (collaborationsAdded.isEmpty() && collaborationsDeleted.isEmpty() && !listCollaborationsBefore.equals(listCollaborationsAfter)) {
        differencesMap.put(this.messageService
                .get("collaborationsChangeOrder"), null);
      }
    }

    if (!differencesMap.isEmpty()) {
      fieldDifferences.put(this.messageService.get("contributors"), differencesMap);
    }
  }

  private void compareContributorsList(List<ContributorDTO> contributorsBefore, List<ContributorDTO> contributorsAfter,
          Map<String, List<MetadataDifference>> differencesMap, String messagePrefix) {
    List<AbstractContributor> contributorsDeleted = contributorsBefore.stream().filter(f -> !contributorsAfter.contains(f))
            .collect(Collectors.toList());
    List<AbstractContributor> contributorsAdded = contributorsAfter.stream().filter(f -> !contributorsBefore.contains(f))
            .collect(Collectors.toList());

    for (int i = 0; i < contributorsDeleted.size(); i++) {
      differencesMap.put(this.messageService
              .get(messagePrefix + "Deleted", new Object[] {
                      ((ContributorDTO) contributorsDeleted.get(i)).getFirstName(),
                      ((ContributorDTO) contributorsDeleted.get(i)).getLastName() }), null);
    }
    for (int i = 0; i < contributorsAdded.size(); i++) {
      differencesMap.put(this.messageService
                      .get(messagePrefix + "Added", new Object[] {
                              ((ContributorDTO) contributorsAdded.get(i)).getFirstName(),
                              ((ContributorDTO) contributorsAdded.get(i)).getLastName() }),
              null);
    }
    if (contributorsAdded.isEmpty() && contributorsDeleted.isEmpty() && !contributorsBefore.equals(contributorsAfter)) {
      differencesMap.put(this.messageService.get(messagePrefix + "sChangeOrder"), null);
    }
  }

  private String convertBigIntegerToStringOrNull(BigInteger number) {
    return (number != null ? number.toString() : null);
  }

  private String getFieldValueOrNull(Class<?> clazz, String fieldName, Object fieldWithValue) {
    if (fieldWithValue != null) {
      Field field = ReflectionUtils.findField(clazz, fieldName);
      if (field == null) {
        return null;
      }
      field.setAccessible(true);
      Object typedValue = ReflectionUtils.getField(field, fieldWithValue);
      return typedValue == null ? null : typedValue.toString();
    }
    return null;
  }

  private void addIfNotEqual(String oldValue, String newValue, String messageLabel, List<MetadataDifference> listDifferences) {
    if (!(oldValue == null && newValue == null) && !Objects.equals(oldValue, newValue)) {
      MetadataDifference difference = new MetadataDifference(this.messageService.get(messageLabel), oldValue, newValue);
      listDifferences.add(difference);
    }
  }

}
