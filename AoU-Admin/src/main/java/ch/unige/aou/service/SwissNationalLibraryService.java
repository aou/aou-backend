/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - SwissNationalLibraryService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import jakarta.transaction.Transactional;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.SwissNationalLibraryUploadService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.config.AouProperties.SendMode;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.schedule.SwissNationalLibraryUpload;
import ch.unige.aou.service.metadata.MetadataExtractor;

@Service
@ConditionalOnBean(AdminController.class)
public class SwissNationalLibraryService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(SwissNationalLibraryService.class);

  private final PublicationService publicationService;
  private final DocumentFileService documentFileService;
  private final MetadataService metadataService;
  private final SwissNationalLibraryUploadService swissNationalLibraryUploadService;

  private final String sftpHost;
  private final String sftpPath;
  private final String sftpUser;
  private final String sftpPassword;
  private final SendMode sendMode;

  public SwissNationalLibraryService(AouProperties aouProperties, MessageService messageService, PublicationService publicationService,
          DocumentFileService documentFileService, MetadataService metadataService,
          SwissNationalLibraryUploadService swissNationalLibraryUploadService) {
    super(messageService);
    this.publicationService = publicationService;
    this.documentFileService = documentFileService;
    this.metadataService = metadataService;
    this.swissNationalLibraryUploadService = swissNationalLibraryUploadService;

    this.sftpHost = aouProperties.getSwissNationalLibrary().getFtpServer().getHost();
    String path = aouProperties.getSwissNationalLibrary().getFtpServer().getPath();
    if (!StringTool.isNullOrEmpty(path) && !path.endsWith("/")) {
      path = path + "/";
    }
    this.sftpPath = path;
    this.sftpUser = aouProperties.getSwissNationalLibrary().getFtpServer().getUser();
    this.sftpPassword = aouProperties.getSwissNationalLibrary().getFtpServer().getPassword();
    this.sendMode = aouProperties.getSwissNationalLibrary().getSendMode();
  }

  @Transactional
  public List<Publication> getThesesToSend(OffsetDateTime searchFrom) {
    List<Publication> lastUpdatedTheses = this.publicationService.findThesisLastUpdatedAfter(searchFrom);
    log.info("{} theses have been updated since {}", lastUpdatedTheses.size(), searchFrom);
    List<Publication> matchingCriteriaTheses = lastUpdatedTheses.stream().filter(this::matchCriteriaToBeSent).collect(Collectors.toList());
    log.info("{} theses match criteria to be sent", matchingCriteriaTheses.size());
    List<Publication> thesesToSend = matchingCriteriaTheses.stream().filter(p -> !this.allFilesAlreadySynchronized(p))
            .collect(Collectors.toList());
    log.info("{} theses must be sent as they have not been sent yet", thesesToSend.size());
    return thesesToSend;
  }

  @Transactional
  public void sendNonPublicFiles(Publication publication) {
    List<DocumentFile> files = this.getNonPublicDocumentFiles(publication);
    for (DocumentFile documentFile : files) {
      //Check if file has already been sent to Swiss National Library
      if (!this.fileAlreadySynchronized(publication, documentFile)) {
        this.sendNonPublicFile(publication, documentFile);
      }
    }
  }

  private void sendNonPublicFile(Publication publication, DocumentFile documentFile) {
    // Store SwissNationalLibraryUpload with current documentFile checksum
    SwissNationalLibraryUpload upload = new SwissNationalLibraryUpload();
    upload.setPublication(publication);
    upload.setDocumentFileChecksum(documentFile.getChecksum());
    upload.setDocumentFileName(documentFile.getFileName());
    upload.setDocumentFileType(documentFile.getDocumentFileType().getValue());
    upload.setUploadDate(OffsetDateTime.now());
    this.swissNationalLibraryUploadService.save(upload);

    try {

      // download file in local
      Path tmpFilePath = this.documentFileService.downloadFileInTempFolder(documentFile);

      if (this.sendMode == SendMode.SFTP) {
        // Send file to sFTP server
        this.sendFileWithFtp(publication, documentFile, tmpFilePath);
      } else {
        this.sendFileWithHttp(publication, documentFile, tmpFilePath);
      }

      log.info("Document file {} for publication {} sent to Swiss National Library sFTP server", documentFile.getResId(),
              publication.getResId());

      // delete tmp thesis file from disk
      FileTool.deleteFile(tmpFilePath);

    } catch (IOException e) {
      throw new SolidifyRuntimeException("An error occurred while trying to send thesis to Swiss National Library with sFTP", e);
    }
  }

  private void sendFileWithFtp(Publication publication, DocumentFile documentFile, Path tmpFilePath) {
    try (SSHClient sshClient = new SSHClient()) {
      sshClient.addHostKeyVerifier(new PromiscuousVerifier());
      sshClient.connect(this.sftpHost);
      sshClient.authPassword(this.sftpUser, this.sftpPassword);

      // send file to sFTP server
      try (SFTPClient sftpClient = sshClient.newSFTPClient()) {
        sftpClient.put(tmpFilePath.toString(), this.sftpPath + this.getRemoteFileName(publication, documentFile));
      }

    } catch (IOException e) {
      throw new SolidifyRuntimeException("An error occurred while trying to send thesis to Swiss National Library with sFTP", e);
    }
  }

  private void sendFileWithHttp(Publication publication, DocumentFile documentFile, Path tmpFilePath) {
    try {
      MediaType mediaType;
      try {
        mediaType = MediaType.parseMediaType(documentFile.getContentType());
      } catch (InvalidMediaTypeException e) {
        mediaType = MediaType.APPLICATION_OCTET_STREAM;
      }

      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(mediaType);
      headers.setBasicAuth(this.sftpUser, this.sftpPassword);

      HttpEntity<byte[]> request = new HttpEntity<>(Files.readAllBytes(tmpFilePath), headers);

      String path = this.sftpPath;
      if (!StringTool.isNullOrEmpty(path) && !path.startsWith("/")) {
        path = "/" + path;
      }
      String url = this.sftpHost + path + this.getRemoteFileName(publication, documentFile);
      if (!url.startsWith("https://")) {
        url = "https://" + url;
      }

      // Send file to National Library
      final RestTemplate restTemplate = new RestTemplate();
      ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PUT, request, String.class);
      if (response.getStatusCode() != HttpStatus.CREATED) {
        throw new SolidifyRuntimeException("HTTP request did not return a 201 code (Created)");
      }

    } catch (IOException e) {
      throw new SolidifyRuntimeException("An error occurred while trying to send thesis to Swiss National Library with HTTP", e);
    }
  }

  /**
   * AND date:[2000 TO *]
   * <p>
   * AND institution.marc:Université\ de\ Genève
   * sous.type.objet.marc:Thèse
   * AND urn.marc:*
   * AND metadata.fulltext:true
   * AND has any file not PUBLIC
   *
   * @param publication
   * @return
   */
  private boolean matchCriteriaToBeSent(Publication publication) {

    // Publication must be completed
    if (publication.getStatus() != Publication.PublicationStatus.COMPLETED) {
      log.info("Publication {} does not match criteria as it is not COMPLETED but {}", publication.getArchiveId(), publication.getStatus());
      return false;
    }

    // Thesis has an UNIGE URN
    if (!this.metadataService.hasUnigeURN(publication)) {
      log.info("Publication {} does not match criteria as it doesn't have any URN", publication.getArchiveId());
      return false;
    }

    // Thesis has a THESIS file
    Optional<DocumentFile> thesisFileOpt = publication.getDocumentFiles().stream()
            .filter(df -> df.getDocumentFileType().getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_THESIS)).findFirst();
    if (thesisFileOpt.isEmpty()) {
      log.info("Publication {} does not match criteria as it doesn't have any thesis file", publication.getArchiveId());
      return false;
    }

    // Thesis has some file that must be sent that is not public
    List<DocumentFile> nonPublicFiles = this.getNonPublicDocumentFiles(publication);
    if (nonPublicFiles.isEmpty()) {
      log.info("Publication {} does not match criteria as it doesn't have any file that is not public", publication.getArchiveId());
      return false;
    }

    MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());

    // Thesis must be a UNIGE thesis
    if (metadataExtractor.isBeforeUnige(depositDoc)) {
      log.info("Publication {} does not match criteria as it is not an UNIGE thesis", publication.getArchiveId());
      return false;
    }

    // Thesis must date from after the year 2000
    boolean isAfter2000 = false;
    for (MetadataDates metadataDate : metadataExtractor.getDates(depositDoc)) {
      Integer year = this.metadataService.getYear(metadataDate.getContent());
      if (year != null && year > 2000) {
        isAfter2000 = true;
        break;
      }
    }

    if (!isAfter2000) {
      log.info("Publication {} does not match criteria as it is not after year 2000", publication.getArchiveId());
    }

    return isAfter2000;
  }

  private boolean allFilesAlreadySynchronized(Publication publication) {
    List<DocumentFile> nonPublicFiles = this.getNonPublicDocumentFiles(publication);

    for (DocumentFile documentFile : nonPublicFiles) {

      // For old deposits, files may not have any checksum yet
      if (StringTool.isNullOrEmpty(documentFile.getChecksum())) {
        try {
          this.documentFileService.completeChecksumByDownloadingFile(documentFile);
        } catch (IOException | NoSuchAlgorithmException e) {
          log.error("Unable to complete checksum for document file {}", documentFile.getResId(), e);
          return false;
        }
      }

      // Check if file has already been sent to Swiss National Library
      if (!this.fileAlreadySynchronized(publication, documentFile)) {
        return false;
      }
    }
    return true;
  }

  private boolean fileAlreadySynchronized(Publication publication, DocumentFile documentFile) {
    List<SwissNationalLibraryUpload> uploads = publication.getSwissNationalLibraryUploads();
    return uploads.stream().anyMatch(u -> u.getDocumentFileChecksum().equals(documentFile.getChecksum()));
  }

  /**
   * Return all files that are not public and that must be sent to Swiss National Library
   *
   * @param publication
   * @return
   */
  private List<DocumentFile> getNonPublicDocumentFiles(Publication publication) {
    return publication.getDocumentFiles().stream()
            .filter(df -> !df.getDocumentFileType().getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE)
                    && !df.getDocumentFileType().getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE)
                    && !df.getDocumentFileType().getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE)
                    && df.getCurrentAccessLevel() != DocumentFile.AccessLevel.PUBLIC)
            .collect(Collectors.toList());
  }

  private String getRemoteFileName(Publication publication, DocumentFile documentFile) {
    String urn = this.metadataService.getUrn(publication);
    String filename = documentFile.getFileName();
    return urn.replace(":", "-") + "_" + filename;
  }
}
