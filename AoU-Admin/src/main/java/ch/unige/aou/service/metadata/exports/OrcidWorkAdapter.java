/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - OrcidWorkAdapter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.exports;

import static ch.unige.aou.AouConstants.DATE_TYPE_DEFENSE;
import static ch.unige.aou.AouConstants.DATE_TYPE_FIRST_ONLINE;
import static ch.unige.aou.AouConstants.DATE_TYPE_IMPRIMATUR;
import static ch.unige.aou.AouConstants.DATE_TYPE_PUBLICATION;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME;

import java.util.List;
import java.util.Optional;
import javax.xml.namespace.QName;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import jakarta.xml.bind.JAXBElement;

import ch.unige.solidify.model.xml.orcid.v3_0.common.CreditName;
import ch.unige.solidify.model.xml.orcid.v3_0.common.ExternalId;
import ch.unige.solidify.model.xml.orcid.v3_0.common.ExternalIds;
import ch.unige.solidify.model.xml.orcid.v3_0.common.FuzzyDate;
import ch.unige.solidify.model.xml.orcid.v3_0.common.OrcidId;
import ch.unige.solidify.model.xml.orcid.v3_0.common.TranslatedTitle;
import ch.unige.solidify.model.xml.orcid.v3_0.common.Url;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Contributor;
import ch.unige.solidify.model.xml.orcid.v3_0.work.ContributorAttributes;
import ch.unige.solidify.model.xml.orcid.v3_0.work.ContributorEmail;
import ch.unige.solidify.model.xml.orcid.v3_0.work.ContributorSequence;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Work;
import ch.unige.solidify.model.xml.orcid.v3_0.work.WorkContributors;
import ch.unige.solidify.model.xml.orcid.v3_0.work.WorkTitle;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.orcid.OrcidIdentifierType;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;

@Service
@ConditionalOnBean(AdminController.class)
public class OrcidWorkAdapter {

  private static final String ORCID_COMMON_NS = "http://www.orcid.org/ns/common";
  private static final String ORCID_PART_OF_RELATION = "part-of";
  private static final String ORCID_SELF_RELATION = "self";

  private final MetadataService metadataService;

  private final String publicationUrlTemplate;

  public OrcidWorkAdapter(AouProperties aouProperties, MetadataService metadataService) {
    this.metadataService = metadataService;
    this.publicationUrlTemplate = aouProperties.getParameters().getPublicationUrlTemplate();
  }

  public void fillWork(Object depositDoc, Work work, MetadataExtractor metadataExtractor, String contributorRole, String archiveId) {
    this.fillTitles(depositDoc, work, metadataExtractor);
    this.fillType(depositDoc, work, metadataExtractor, contributorRole);
    this.fillContributors(depositDoc, work, metadataExtractor);
    this.fillDates(depositDoc, work, metadataExtractor);
    this.fillLanguage(depositDoc, work, metadataExtractor);
    this.fillAbstract(depositDoc, work, metadataExtractor);
    this.fillContainerTitle(depositDoc, work, metadataExtractor);
    this.fillCommercialUrl(depositDoc, work, metadataExtractor);
    this.fillExternalIds(depositDoc, work, metadataExtractor, archiveId);
  }

  private void fillExternalIds(Object depositDoc, Work work, MetadataExtractor metadataExtractor, String archiveId) {
    this.addExternalId(work, OrcidIdentifierType.DOI, metadataExtractor.getDOI(depositDoc));
    if (metadataExtractor.getPmid(depositDoc) != null) {
      this.addExternalId(work, OrcidIdentifierType.PMID, String.valueOf(metadataExtractor.getPmid(depositDoc)));
    }

    String subtype = metadataExtractor.getSubtype(depositDoc);
    if (subtype.equals(DEPOSIT_SUBTYPE_LIVRE_NAME)
            || subtype.equals(DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME)
            || subtype.equals(DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME)) {
      // These types may have an ISBN for themselves
      this.addExternalId(work, OrcidIdentifierType.ISBN, metadataExtractor.getISBN(depositDoc));
    } else {
      // otherwise if an ISBN is present, the current publication is part of the object having an ISBN
      this.addExternalId(work, OrcidIdentifierType.ISBN, metadataExtractor.getISBN(depositDoc), ORCID_PART_OF_RELATION);
    }

    this.addExternalId(work, OrcidIdentifierType.ISSN, metadataExtractor.getISSN(depositDoc), ORCID_PART_OF_RELATION);
    this.addExternalId(work, OrcidIdentifierType.ARXIV, metadataExtractor.getArxivId(depositDoc));
    this.addExternalId(work, OrcidIdentifierType.URN, metadataExtractor.getURN(depositDoc));
    this.addExternalId(work, OrcidIdentifierType.PMC, metadataExtractor.getPmcId(depositDoc));

    // Identifier types not recognized by ORCID are uploaded with a type 'other-id'
    this.addOtherExternalId(work, "mmsid", metadataExtractor.getMmsid(depositDoc), null);
    this.addOtherExternalId(work, "repec", metadataExtractor.getRepec(depositDoc), null);
    this.addOtherExternalId(work, "dblp", metadataExtractor.getDblp(depositDoc), null);
    String url = this.publicationUrlTemplate.replace("{archiveId}", archiveId);
    this.addOtherExternalId(work, null, archiveId, url);
  }

  private void addExternalId(Work work, OrcidIdentifierType type, String value) {
    this.addExternalId(work, type, value, ORCID_SELF_RELATION, null);
  }

  private void addOtherExternalId(Work work, String type, String value, String url) {
    if (!StringTool.isNullOrEmpty(value)) {
      if (!StringTool.isNullOrEmpty(type)) {
        value = type + ":" + value;
      }
      this.addExternalId(work, OrcidIdentifierType.OTHER_ID, value, ORCID_SELF_RELATION, url);
    }
  }

  private void addExternalId(Work work, OrcidIdentifierType type, String value, String relationShip) {
    this.addExternalId(work, type, value, relationShip, null);
  }

  private void addExternalId(Work work, OrcidIdentifierType type, String value, String relationShip, String url) {
    if (!StringTool.isNullOrEmpty(value)) {
      ExternalId externalId = new ExternalId();
      externalId.setExternalIdValue(value);
      externalId.setExternalIdType(type.getName());
      externalId.setExternalIdRelationship(relationShip);
      if (!StringTool.isNullOrEmpty(url)) {
        externalId.setExternalIdUrl(url);
      }
      if (work.getExternalIds() == null) {
        work.setExternalIds(new ExternalIds());
      }
      work.getExternalIds().getExternalId().add(externalId);
    }
  }

  private void fillCommercialUrl(Object depositDoc, Work work, MetadataExtractor metadataExtractor) {
    String url = metadataExtractor.getPublisherVersionUrl(depositDoc);
    if (!StringTool.isNullOrEmpty(url)) {
      Url workUrl = new Url();
      workUrl.setValue(url);
      work.setUrl(workUrl);
    }
  }

  private void fillContainerTitle(Object depositDoc, Work work, MetadataExtractor metadataExtractor) {
    String containerTitle = metadataExtractor.getContainerTitle(depositDoc);
    if (!StringTool.isNullOrEmpty(containerTitle)) {
      work.setJournalTitle(containerTitle);
    }
  }

  private void fillAbstract(Object depositDoc, Work work, MetadataExtractor metadataExtractor) {
    List<MetadataTextLang> abstracts = metadataExtractor.getAbstracts(depositDoc);
    if (!abstracts.isEmpty()) {
      String description = CleanTool.removeAllHtmlTags(abstracts.get(0).getContent());
      work.setShortDescription(description);
    }
  }

  private void fillLanguage(Object depositDoc, Work work, MetadataExtractor metadataExtractor) {
    List<String> languageCodes = metadataExtractor.getLanguages(depositDoc);
    if (!languageCodes.isEmpty()) {
      work.setLanguageCode(this.getLanguageCode(languageCodes.get(0)));
    }
  }

  private String getLanguageCode(String code) {
    switch (code) {
      case "fr", "fre", "fra":
        return "fr";
      case "en", "eng":
        return "en";
      case "de", "ger", "deu":
        return "de";
      case "ara", "ar":
        return "ar";
      case "chi", "zho", "zh":
        // according to https://github.com/ORCID/orcid-model/blob/master/src/main/java/org/orcid/jaxb/model/common/LanguageCode.java
        return "zh_CN";
      case "spa", "es":
        return "es";
      case "gre", "ell", "el":
        return "el";
      case "ita", "it":
        return "it";
      case "jpn", "ja":
        return "ja";
      case "dut", "nld", "nl":
        return "nl";
      case "por", "pt":
        return "pt";
      case "roh", "rm":
        return "rm";
      case "rus", "ru":
        return "ru";
      case "tur", "tr":
        return "tr";
      default:
        return null;
    }
  }

  private void fillDates(Object depositDoc, Work work, MetadataExtractor metadataExtractor) {

    List<MetadataDates> metadataDates = metadataExtractor.getDates(depositDoc);

    MetadataDates dateToExport = null;
    Optional<MetadataDates> publicationDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_PUBLICATION.equals(d.getType())).findFirst();
    if (publicationDateOpt.isPresent()) {
      dateToExport = publicationDateOpt.get();
    }
    if (dateToExport == null) {
      Optional<MetadataDates> firstOnlineDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_FIRST_ONLINE.equals(d.getType())).findFirst();
      if (firstOnlineDateOpt.isPresent()) {
        dateToExport = firstOnlineDateOpt.get();
      }
    }
    if (dateToExport == null) {
      Optional<MetadataDates> imprimaturDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_IMPRIMATUR.equals(d.getType())).findFirst();
      if (imprimaturDateOpt.isPresent()) {
        dateToExport = imprimaturDateOpt.get();
      }
    }
    if (dateToExport == null) {
      Optional<MetadataDates> defenseDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_DEFENSE.equals(d.getType())).findFirst();
      if (defenseDateOpt.isPresent()) {
        dateToExport = defenseDateOpt.get();
      }
    }

    if (dateToExport != null) {
      work.setPublicationDate(new FuzzyDate());
      String dateStr = dateToExport.getContent();

      // Year
      Integer year = this.metadataService.getYear(dateStr);
      FuzzyDate.Year fuzzyDateYear = new FuzzyDate.Year();
      fuzzyDateYear.setValue(year);
      work.getPublicationDate().setYear(fuzzyDateYear);

      // Month
      Integer month = this.metadataService.getMonth(dateStr);
      if (month != null) {
        FuzzyDate.Month fuzzyDateMonth = new FuzzyDate.Month();
        fuzzyDateMonth.setValue(month);
        work.getPublicationDate().setMonth(fuzzyDateMonth);
      }

      // Day
      Integer day = this.metadataService.getDay(dateStr);
      if (day != null) {
        FuzzyDate.Day fuzzyDateDay = new FuzzyDate.Day();
        fuzzyDateDay.setValue(day);
        work.getPublicationDate().setDay(fuzzyDateDay);
      }
    }
  }

  private void fillContributors(Object depositDoc, Work work, MetadataExtractor metadataExtractor) {
    List<AbstractContributor> contributors = metadataExtractor.getContributors(depositDoc);

    boolean firstAuthor = true;
    for (AbstractContributor abstractContributor : contributors) {
      Contributor workContributor = new Contributor();
      if (abstractContributor instanceof ContributorDTO contributorDTO) {

        // Contributor name
        CreditName creditName = new CreditName();
        creditName.setValue(contributorDTO.getFirstName() + " " + contributorDTO.getLastName());
        workContributor.setCreditName(creditName);

        // Contributor ORCID
        if (!StringTool.isNullOrEmpty(contributorDTO.getOrcid())) {
          OrcidId orcidId = new OrcidId();
          JAXBElement<String> uri = new JAXBElement<>(new QName(ORCID_COMMON_NS, "uri"), String.class,
                  "https://orcid.org/" + contributorDTO.getOrcid());
          orcidId.getContent().add(uri);

          JAXBElement<String> path = new JAXBElement<>(new QName(ORCID_COMMON_NS, "path"), String.class,
                  contributorDTO.getOrcid());
          orcidId.getContent().add(path);

          JAXBElement<String> host = new JAXBElement<>(new QName(ORCID_COMMON_NS, "host"), String.class, "orcid.org");
          orcidId.getContent().add(host);
          workContributor.setContributorOrcid(orcidId);
        }

        // Role
        String role = contributorDTO.getRole();
        String workRole;
        if (role.equals(AouConstants.CONTRIBUTOR_ROLE_EDITOR)) {
          workRole = "editor";
        } else if (role.equals(AouConstants.CONTRIBUTOR_ROLE_DIRECTOR)) {
          workRole = "http://credit.niso.org/contributor-roles/supervision/";
        } else if (role.equals(AouConstants.CONTRIBUTOR_ROLE_TRANSLATOR)) {
          workRole = "chair-or-translator";
        } else if (role.equals(AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR)) {
          workRole = "co-investigator";
        } else {
          workRole = "author";
        }

        workContributor.setContributorAttributes(new ContributorAttributes());
        workContributor.getContributorAttributes().setContributorRole(workRole);
        if (firstAuthor) {
          workContributor.getContributorAttributes().setContributorSequence(ContributorSequence.FIRST);
          firstAuthor = false;
        }

        // Email
        if (!StringTool.isNullOrEmpty(contributorDTO.getEmail())) {
          ContributorEmail contributorEmail = new ContributorEmail();
          contributorEmail.setValue(contributorDTO.getEmail());
          workContributor.setContributorEmail(contributorEmail);
        }
      } else if (abstractContributor instanceof CollaborationDTO collaborationDTO) {
        // Case of collaborations
        // collaboration name
        CreditName creditName = new CreditName();
        creditName.setValue(collaborationDTO.getName());
        workContributor.setCreditName(creditName);

        // role
        workContributor.setContributorAttributes(new ContributorAttributes());
        workContributor.getContributorAttributes().setContributorRole("author");
      }

      // Add contributor to the list
      if (work.getContributors() == null) {
        work.setContributors(new WorkContributors());
      }
      work.getContributors().getContributor().add(workContributor);
    }
  }

  private void fillType(Object depositDoc, Work work, MetadataExtractor metadataExtractor, String contributorRole) {
    // Eventually adapt Work type according to contributor role
    if (AouConstants.CONTRIBUTOR_ROLE_DIRECTOR.equals(contributorRole)) {
      work.setType("supervised-student-publication");
    } else if (AouConstants.CONTRIBUTOR_ROLE_TRANSLATOR.equals(contributorRole)) {
      work.setType("translation");
    } else {

      String subSubtype = metadataExtractor.getSubSubtype(depositDoc);
      if (!StringTool.isNullOrEmpty(subSubtype)) {
        if (AouConstants.DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME.equals(subSubtype)) {
          work.setType("book-review");
          return;
        }
        if (AouConstants.DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME.equals(subSubtype)) {
          work.setType("review");
          return;
        }
      }

      String subtype = metadataExtractor.getSubtype(depositDoc);
      String workType;
      switch (subtype) {
        case AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME -> workType = "journal-article";
        case AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME -> workType = "magazine-article";
        case AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME -> workType = "newspaper-article";
        case AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME -> workType = "journal-issue";
        case AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME -> workType = "book";
        case AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME,
                AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME -> workType = "edited-book";
        case AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME,
                AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME -> workType = "book-chapter";
        case AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME -> workType = "encyclopedia-entry";
        case AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME -> workType = "lecture-speech";
        case AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME -> workType = "conference-poster";
        case AouConstants.DEPOSIT_SUBTYPE_THESE_NAME,
                AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME,
                AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME,
                AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME,
                AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME -> workType = "dissertation-thesis";
        case AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME,
                AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME -> workType = "report";
        case AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME -> workType = "working-paper";
        case AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME -> workType = "preprint";
        default -> workType = null;
      }
      work.setType(workType);
    }
  }

  private void fillTitles(Object depositDoc, Work work, MetadataExtractor metadataExtractor) {
    // Title
    String title = metadataExtractor.getTitleContent(depositDoc);
    title = CleanTool.removeAllHtmlTags(title);
    WorkTitle workTitle = new WorkTitle();
    workTitle.setTitle(title);
    work.setTitle(workTitle);

    // Original title
    String originalTitleContent = metadataExtractor.getOriginalTitleContent(depositDoc);
    if (!StringTool.isNullOrEmpty(originalTitleContent)) {
      work.getTitle().setTranslatedTitle(new TranslatedTitle());
      work.getTitle().getTranslatedTitle().setValue(originalTitleContent);

      String originalTitleLang = metadataExtractor.getOriginalTitleLang(depositDoc);
      if (!StringTool.isNullOrEmpty(originalTitleLang)) {
        work.getTitle().getTranslatedTitle().setLanguageCode(this.getLanguageCode(originalTitleLang));
      }
    }
  }
}
