/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - GoogleIsbnImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class GoogleIsbnImportService extends MetadataImportService {
  private static final Logger log = LoggerFactory.getLogger(GoogleIsbnImportService.class);

  private final String apiUrl;
  private static final String TOTAL_ITEMS = "totalItems";
  private static final String ITEMS = "items";
  private static final String VOLUME_INFO = "volumeInfo";
  private static final String DESCRIPTION = "description";
  private static final String TITLE = "title";
  private static final String PUBLICATION_DATE = "publishedDate";
  private static final String PUBLISHER = "publisher";
  private static final String AUTHORS = "authors";
  private static final String PAGE_COUNT = "pageCount";
  private static final String LANGUAGE = "language";
  private static final String CATEGORIES = "categories";
  private static final String PRINT_TYPE = "printType";

  public GoogleIsbnImportService(AouProperties aouProperties, MessageService messageService,
          MetadataService metadataService, ContributorService contributorService, JournalTitleRemoteService journalTitleRemoteService,
          LicenseService licenseService) {
    super(aouProperties, messageService, metadataService, contributorService, journalTitleRemoteService, licenseService);
    this.apiUrl = aouProperties.getMetadata().getImports().getIsbn().getGoogleUrl();
  }

  public DepositDoc searchInGoogle(String isbn, DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(this.apiUrl)) {
      String googleJson = this.getJsonFromGoogle(isbn);
      JSONObject jsonObject = new JSONObject(googleJson);
      this.fillDepositDoc(isbn, depositDoc, jsonObject);

      return depositDoc;
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  public void fillDepositDoc(String isbn, DepositDoc depositDoc, JSONObject googleJson) {
    //check total_items to see if there is any result before getting all the info
    int totalItems = googleJson.getInt(TOTAL_ITEMS);
    if (totalItems != 1) {
      throw new SolidifyRuntimeException("There is none or more than one result for the isbn specified");
    }
    JSONObject googleBookJson = googleJson.getJSONArray(ITEMS).getJSONObject(0);

    this.fillTitle(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), TITLE);
    this.fillAbstract(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), DESCRIPTION);
    this.fillPublicationDate(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), PUBLICATION_DATE);
    this.fillPublisher(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), PUBLISHER);
    this.fillContributors(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), AUTHORS);
    this.fillPagination(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), PAGE_COUNT);
    this.fillKeywords(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), CATEGORIES);
    this.fillLanguage(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), LANGUAGE);
    this.fillPublicationType(depositDoc, googleBookJson.getJSONObject(VOLUME_INFO), PRINT_TYPE);
    this.fillIsbn(depositDoc, isbn);

  }

  private void fillPublicationType(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    String value = this.getStringOrNull(jsonObject, property);
    if (!StringTool.isNullOrEmpty(value)) {
      //There are two different values: book or magazine
      String subType = this.getPublicationSubType(value);
      String type = this.getPublicationType(value);
      depositDoc.setSubtype(subType);
      depositDoc.setType(type);
    }
  }

  private String getPublicationType(String value) {
    return switch (value) {
      case "BOOK" -> AouConstants.DEPOSIT_TYPE_LIVRE_NAME;
      case "MAGAZINE" -> AouConstants.DEPOSIT_TYPE_JOURNAL_NAME;
      default -> null;
    };
  }

  private String getPublicationSubType(String value) {
    return switch (value) {
      case "BOOK" -> AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME;
      case "MAGAZINE" -> AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME;
      default -> null;
    };
  }

  private void fillTitle(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    String value = this.getStringOrNull(jsonObject, property);
    if (!StringTool.isNullOrEmpty(value)) {
      depositDoc.setTitle(this.getText(value));
    }
  }

  private void fillAbstract(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    String value = this.getStringOrNull(jsonObject, property);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureAbstractsExist(depositDoc);
      depositDoc.getAbstracts().getAbstract().add(this.getText(value));
    }
  }

  private void fillPublicationDate(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    String value = this.getStringOrNull(jsonObject, property);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDatesExist(depositDoc);
      depositDoc.getDates().getDate().add(this.getDateWithType(DateTypes.PUBLICATION, value));
    }
  }

  private void fillPublisher(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    String value = this.getStringOrNull(jsonObject, property);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensurePublisherExist(depositDoc);
      depositDoc.getPublisher().getNameOrPlace().add(this.factory.createPublisherName(value));
    }
  }

  private void fillContributors(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    JSONArray contributorArray = this.getArrayOrNull(jsonObject, property);
    if (contributorArray != null && !contributorArray.isEmpty()) {
      this.ensureContributorsExist(depositDoc);
      for (int i = 0; i < this.getNumberMaxContributor(contributorArray.length()); i++) {
        String authorName = contributorArray.getString(i);
        // Cut the author name on the last space, assuming the last word is the lastName
        String firstName = null;
        String lastName = null;
        if (!StringTool.isNullOrEmpty(authorName)) {
          int commaIndex = authorName.lastIndexOf(' ');
          if (commaIndex > -1) {
            firstName = authorName.substring(0, commaIndex);
            lastName = authorName.substring(commaIndex + 1);
          } else {
            lastName = authorName;
          }
          firstName = this.getNameOrUndefinedValue(firstName);
          lastName = this.getNameOrUndefinedValue(lastName);

          Contributor contributor = new Contributor();
          contributor.setLastname(CleanTool.capitalizeString(lastName));
          contributor.setFirstname(CleanTool.capitalizeString(firstName));
          depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
        }
      }
    }
  }

  private void fillPagination(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    if (jsonObject != null && jsonObject.has(property) && !jsonObject.isNull(property)) {
      String value = Integer.toString(jsonObject.getInt(property));
      if (!StringTool.isNullOrEmpty(value)) {
        this.ensurePagesExist(depositDoc);
        this.setPagingOrOther(depositDoc, value);
      }
    }
  }

  private void fillKeywords(DepositDoc depositDoc, JSONObject jsonObject, String property) {
    JSONArray keywordArray = this.getArrayOrNull(jsonObject, property);
    if (keywordArray != null && !keywordArray.isEmpty()) {
      this.ensureKeywordsExist(depositDoc);
      for (int i = 0; i < keywordArray.length(); i++) {
        String value = keywordArray.getString(i);
        if (!StringTool.isNullOrEmpty(value)) {
          depositDoc.getKeywords().getKeyword().add(value);
        }
      }
    }
  }

  private void fillLanguage(DepositDoc depositDoc, JSONObject doc, String property) {
    String value = this.getStringOrNull(doc, property);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureLanguagesExist(depositDoc);
      depositDoc.getLanguages().getLanguage().add(CleanTool.getLanguageCode(value));
    }
  }

  private void fillIsbn(DepositDoc depositDoc, String isbnValue) {
    this.ensureDepositIdentifiersExist(depositDoc);
    depositDoc.getIdentifiers().setIsbn(isbnValue);
  }

  private String getJsonFromGoogle(String isbn) {
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    RestTemplate client = restTemplateBuilder.build();
    return client.getForObject(this.apiUrl, String.class, Map.of("id", isbn));
  }

}
