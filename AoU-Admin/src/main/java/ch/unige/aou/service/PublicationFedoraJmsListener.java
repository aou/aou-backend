/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationFedoraJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.PublicationFromFedoraMessage;

@Profile("storage-fedora3")
@Service
@ConditionalOnBean(AdminController.class)
public class PublicationFedoraJmsListener extends MessageProcessor<PublicationFromFedoraMessage> {

  private static final Logger log = LoggerFactory.getLogger(PublicationFedoraJmsListener.class);

  private final PublicationFedoraProcessingService publicationFedoraProcessingService;

  public PublicationFedoraJmsListener(AouProperties aouProperties, PublicationFedoraProcessingService publicationFedoraProcessingService) {
    super(aouProperties);
    this.publicationFedoraProcessingService = publicationFedoraProcessingService;
  }

  @JmsListener(destination = "${aou.queue.publicationsFromFedora}")
  @Override
  public void receiveMessage(PublicationFromFedoraMessage publicationFromFedoraMessage) {
    this.sendForParallelProcessing(publicationFromFedoraMessage);
  }

  @Override
  public void processMessage(PublicationFromFedoraMessage publicationFromFedoraMessage) {
    log.info("Reading message {} from pid {}", publicationFromFedoraMessage, publicationFromFedoraMessage.getPid());
    this.publicationFedoraProcessingService.processMessage(publicationFromFedoraMessage);
    log.info("Message {} processed from pid {}", publicationFromFedoraMessage, publicationFromFedoraMessage.getPid());
  }
}
