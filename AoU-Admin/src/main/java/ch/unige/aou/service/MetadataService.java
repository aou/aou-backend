/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.json.JSONObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.AouXmlNamespacePrefixMapper;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.LabeledLanguageService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.tool.MetadataTool;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.metadata.MetadataComparatorV1;
import ch.unige.aou.service.metadata.MetadataComparatorV2;
import ch.unige.aou.service.metadata.MetadataDifferencesService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.metadata.MetadataExtractorV1;
import ch.unige.aou.service.metadata.MetadataExtractorV2;
import ch.unige.aou.service.metadata.MetadataValidator;
import ch.unige.aou.service.metadata.MetadataValidatorV1;
import ch.unige.aou.service.metadata.MetadataValidatorV2;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.rest.UnigePersonSearchService;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileTypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedLicenseRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStructureRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(AdminController.class)
public class MetadataService extends CommonMetadataService {

  private final AouProperties aouProperties;
  private final PublicationTypeService publicationTypeService;
  private final PublicationSubtypeService publicationSubtypeService;
  private final PublicationSubSubtypeService publicationSubSubtypeService;
  private final StructureService structureService;
  private final ResearchGroupService researchGroupService;
  private final ContributorService contributorService;
  private final LabeledLanguageService labeledLanguageService;
  private final JournalTitleRemoteService journalTitleRemoteService;
  private final DuplicateService duplicateService;
  private final PersonService personService;
  private final UnigePersonSearchService unigePersonSearchService;
  private final DocumentFileService documentFileService;
  private final HistoryService historyService;

  private final String swissNationalLibraryUrnPrefix;
  private final String unigeDoiPrefix;
  private final int minimumYearForUrn;

  public MetadataService(AouProperties aouProperties, MessageService messageService, PublicationTypeService publicationTypeService,
          PublicationSubtypeService publicationSubtypeService,
          PublicationSubSubtypeService publicationSubSubtypeService, StructureService structureService,
          ResearchGroupService researchGroupService, ContributorService contributorService, LabeledLanguageService labeledLanguageService,
          JournalTitleRemoteService journalTitleRemoteService, DuplicateService duplicateService, PersonService personService,
          UnigePersonSearchService unigePersonSearchService, TrustedStructureRemoteResourceService remoteStructureService,
          @Lazy DocumentFileService documentFileService, TrustedPublicationRemoteResourceService remotePublicationService,
          TrustedDocumentFileRemoteResourceService remoteDocumentFileService, HistoryService historyService,
          TrustedPublicationSubtypeRemoteResourceService remotePublicationSubtypeService,
          TrustedDocumentFileTypeRemoteResourceService remoteDocumentFileTypeService,
          TrustedContributorRemoteResourceService remoteContributorService,
          TrustedLicenseRemoteResourceService remoteLicenseService,
          TrustedUserRemoteResourceService remoteUserService) {

    super(aouProperties, messageService, remoteStructureService, remotePublicationService, remoteDocumentFileService,
            remotePublicationSubtypeService, remoteDocumentFileTypeService, remoteContributorService, remoteLicenseService, remoteUserService);

    this.aouProperties = aouProperties;
    this.publicationTypeService = publicationTypeService;
    this.publicationSubtypeService = publicationSubtypeService;
    this.publicationSubSubtypeService = publicationSubSubtypeService;
    this.structureService = structureService;
    this.researchGroupService = researchGroupService;
    this.contributorService = contributorService;
    this.labeledLanguageService = labeledLanguageService;
    this.journalTitleRemoteService = journalTitleRemoteService;
    this.duplicateService = duplicateService;
    this.personService = personService;
    this.unigePersonSearchService = unigePersonSearchService;
    this.documentFileService = documentFileService;
    this.historyService = historyService;

    this.swissNationalLibraryUrnPrefix = aouProperties.getMetadata().getSwissNationalLibrary().getUrnPrefix();
    this.unigeDoiPrefix = aouProperties.getMetadata().getUnigeDoiPrefix();
    this.minimumYearForUrn = aouProperties.getMetadata().getSwissNationalLibrary().getMinimumYearForUrn();
  }

  public MetadataExtractor getMetadataExtractor(AouMetadataVersion version) {
    return switch (version) {
      case V1_0 -> new MetadataExtractorV1(this.aouProperties, this.publicationTypeService, this.publicationSubtypeService,
              this.publicationSubSubtypeService, this.structureService, this.researchGroupService, this.contributorService,
              this.labeledLanguageService, this.journalTitleRemoteService, this.personService, this.messageService,
              this.unigePersonSearchService);
      case V2_0, V2_1, V2_2, V2_3, V2_4 ->
              new MetadataExtractorV2(this.aouProperties, this.publicationTypeService, this.publicationSubtypeService,
                      this.publicationSubSubtypeService, this.structureService, this.researchGroupService, this.contributorService,
                      this.labeledLanguageService, this.journalTitleRemoteService, this.personService, this.messageService,
                      this.unigePersonSearchService);
      default -> throw new SolidifyProcessingException(this.messageService.get("metadata.error.version", new Object[] { version }));
    };
  }

  public MetadataValidator getMetadataValidator(AouMetadataVersion version) {
    return switch (version) {
      case V1_0 -> new MetadataValidatorV1(this.aouProperties, (MetadataExtractorV1) this.getMetadataExtractor(version), this.messageService,
              this.publicationTypeService, this.publicationSubtypeService, this.publicationSubSubtypeService, this.researchGroupService,
              this.structureService, this.duplicateService, this.unigePersonSearchService, this.documentFileService, this.historyService);
      case V2_0, V2_1, V2_2, V2_3, V2_4 ->
              new MetadataValidatorV2(this.aouProperties, (MetadataExtractorV2) this.getMetadataExtractor(version), this.messageService,
                      this.publicationTypeService, this.publicationSubtypeService, this.publicationSubSubtypeService, this.researchGroupService,
                      this.structureService, this.duplicateService, this.unigePersonSearchService, this.documentFileService,
                      this.historyService);
      default -> throw new SolidifyProcessingException(this.messageService.get("metadata.error.version", new Object[] { version }));
    };
  }

  public MetadataDifferencesService getMetadataComparator(Publication publication) {
    AouMetadataVersion version = publication.getMetadataVersion();
    return switch (version) {
      case V1_0 -> new MetadataComparatorV1((MetadataExtractorV1) this.getMetadataExtractor(version), this.messageService);
      case V2_0, V2_1, V2_2, V2_3, V2_4 ->
              new MetadataComparatorV2((MetadataExtractorV2) this.getMetadataExtractor(version), this.messageService);
      default -> throw new SolidifyProcessingException(this.messageService.get("metadata.error.version", new Object[] { version }));
    };
  }

  public String upgradeMetadataFormat(String metadata) {
    AouMetadataVersion startVersion = this.detectVersionFromXmlMetadata(metadata);
    AouMetadataVersion targetVersion = AouMetadataVersion.getDefaultVersion();

    if (startVersion == AouMetadataVersion.V1_0 && targetVersion == AouMetadataVersion.V2_0) {
      return this.transformFromV1ToV2_0(metadata);
    } else if (startVersion == AouMetadataVersion.V1_0 && targetVersion == AouMetadataVersion.V2_1) {
      return this.transformFromV1ToV2_1(metadata);
    } else if (startVersion == AouMetadataVersion.V1_0 && targetVersion == AouMetadataVersion.V2_2) {
      return this.transformFromV1ToV2_2(metadata);
    } else if (startVersion == AouMetadataVersion.V1_0 && targetVersion == AouMetadataVersion.V2_3) {
      return this.transformFromV1ToV2_3(metadata);
    } else if (startVersion == AouMetadataVersion.V1_0 && targetVersion == AouMetadataVersion.V2_4) {
      return this.transformFromV1ToV2_4(metadata);
    } else if (startVersion == AouMetadataVersion.V2_0 && targetVersion == AouMetadataVersion.V2_1) {
      return this.transformFromV2_0ToV2_1(metadata);
    } else if (startVersion == AouMetadataVersion.V2_0 && targetVersion == AouMetadataVersion.V2_2) {
      return this.transformFromV2_0ToV2_2(metadata);
    } else if (startVersion == AouMetadataVersion.V2_1 && targetVersion == AouMetadataVersion.V2_2) {
      return this.transformFromV2_1ToV2_2(metadata);
    } else if (startVersion == AouMetadataVersion.V2_0 && targetVersion == AouMetadataVersion.V2_3) {
      return this.transformFromV2_0ToV2_3(metadata);
    } else if (startVersion == AouMetadataVersion.V2_1 && targetVersion == AouMetadataVersion.V2_3) {
      return this.transformFromV2_1ToV2_3(metadata);
    } else if (startVersion == AouMetadataVersion.V2_2 && targetVersion == AouMetadataVersion.V2_3) {
      return this.transformFromV2_2ToV2_3(metadata);
    } else if (startVersion == AouMetadataVersion.V2_2 && targetVersion == AouMetadataVersion.V2_4) {
      return this.transformFromV2_2ToV2_4(metadata);
    } else if (startVersion == AouMetadataVersion.V2_3 && targetVersion == AouMetadataVersion.V2_4) {
      return this.transformFromV2_3ToV2_4(metadata);
    } else {
      throw new SolidifyRuntimeException("Upgrading metadata version from " + startVersion + " to " + targetVersion + " is not supported");
    }
  }

  public String transformFromV1ToV2_0(String metadataV1) {

    try {
      final ClassPathResource metadataV1ToV2XSLResource = new ClassPathResource(
              AouConstants.XSL_HOME + "/" + AouConstants.METADATA_V1_TO_V2_XSL);

      String xsl = FileTool.toString(metadataV1ToV2XSLResource.getInputStream());
      return XMLTool.transform(metadataV1, xsl);

    } catch (IOException e) {
      throw new SolidifyRuntimeException("unable to transform metadata from V1 to V2", e);
    }
  }

  /**
   * Difference is in the way structures are managed in Json: in 2.1, their values in an array of resId in Json
   * and the resId property must be filled in XML metadata
   *
   * @param metadataV2_0
   * @return
   */
  public String transformFromV2_0ToV2_1(String metadataV2_0) {
    AouXmlNamespacePrefixMapper aouXmlNamespacePrefixMapper = this.getAouXmlNamespacePrefixMapper(AouMetadataVersion.V2_0);
    XPath xpath = XMLTool.buildXPath(aouXmlNamespacePrefixMapper);
    try {
      Document document = XMLTool.parseXML(metadataV2_0);
      NodeList structuresList = (NodeList) xpath
              .compile("/aou_deposit:deposit_doc/aou_deposit:academic_structures/aou_deposit:academic_structure")
              .evaluate(document, XPathConstants.NODESET);
      for (int i = 0; i < structuresList.getLength(); i++) {
        Node structureNode = structuresList.item(i);
        Node codeNode = (Node) xpath.compile("./aou_deposit:code").evaluate(structureNode, XPathConstants.NODE);
        String codeStruct = codeNode.getTextContent();
        Structure structure = this.structureService.findByCodeStruct(codeStruct);
        Node resIdNode = document.createElementNS(AouConstants.METADATA_XML_NAMESPACE_V2, "resId");
        resIdNode.setTextContent(structure.getResId());
        structureNode.appendChild(resIdNode);
      }

      // update xmlns for V2.1
      String metadata = XMLTool.documentToString(document);
      metadata = metadata.replace("\"" + AouConstants.METADATA_XML_NAMESPACE_V2 + "\"",
              "\"" + AouConstants.METADATA_XML_NAMESPACE_V2_1 + "\"");
      return metadata;

    } catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException | TransformerException e) {
      throw new SolidifyRuntimeException("Unable to transform metadata from V2.0 to V2.1", e);
    }
  }

  public String transformFromV2_1ToV2_2(String metadataV2_1) {
    AouXmlNamespacePrefixMapper aouXmlNamespacePrefixMapper = this.getAouXmlNamespacePrefixMapper(AouMetadataVersion.V2_1);
    XPath xpath = XMLTool.buildXPath(aouXmlNamespacePrefixMapper);
    try {
      Document document = XMLTool.parseXML(metadataV2_1);

      // transform pages
      Node pagesNode = (Node) xpath.compile("/aou_deposit:deposit_doc/aou_deposit:pages").evaluate(document, XPathConstants.NODE);
      if (pagesNode != null) {
        Node pagesStartNode = (Node) xpath.compile("/aou_deposit:deposit_doc/aou_deposit:pages/aou_deposit:start")
                .evaluate(document, XPathConstants.NODE);
        Node pagesEndNode = (Node) xpath.compile("/aou_deposit:deposit_doc/aou_deposit:pages/aou_deposit:end")
                .evaluate(document, XPathConstants.NODE);
        String pagesStartValue = this.getTextContentIfNotNull(pagesStartNode);
        String pagesEndValue = this.getTextContentIfNotNull(pagesEndNode);
        String paging = "";

        if (!StringTool.isNullOrEmpty(pagesStartValue)) {
          paging = pagesStartValue;
        }

        if (!StringTool.isNullOrEmpty(pagesEndValue)) {
          if (!StringTool.isNullOrEmpty(paging)) {
            paging += "-";
          }
          paging += pagesEndValue;
        }

        // create new 'paging' node
        if (!StringTool.isNullOrEmpty(paging)) {
          Node pagingNode = document.createElementNS(AouConstants.METADATA_XML_NAMESPACE_V2_1, "paging");
          pagingNode.setTextContent(paging);
          pagesNode.appendChild(pagingNode);
        }

        // remove obsolete nodes 'start' and 'end'
        if (pagesStartNode != null) {
          pagesNode.removeChild(pagesStartNode);
        }
        if (pagesEndNode != null) {
          pagesNode.removeChild(pagesEndNode);
        }
      }

      // update xmlns for V2.2
      String metadata = XMLTool.documentToString(document);
      metadata = metadata.replace("\"" + AouConstants.METADATA_XML_NAMESPACE_V2_1 + "\"",
              "\"" + AouConstants.METADATA_XML_NAMESPACE_V2_2 + "\"");
      return metadata;

    } catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException | TransformerException e) {
      throw new SolidifyRuntimeException("Unable to transform metadata from V2.1 to V2.2", e);
    }
  }

  public String transformFromV1ToV2_1(String metadataV1) {
    String metadataV2_0 = this.transformFromV1ToV2_0(metadataV1);
    return this.transformFromV2_0ToV2_1(metadataV2_0);
  }

  public String transformFromV1ToV2_2(String metadataV1) {
    String metadataV2_0 = this.transformFromV1ToV2_0(metadataV1);
    String metadataV2_1 = this.transformFromV2_0ToV2_1(metadataV2_0);
    return this.transformFromV2_1ToV2_2(metadataV2_1);
  }

  public String transformFromV1ToV2_3(String metadataV1) {
    String metadataV2_0 = this.transformFromV1ToV2_0(metadataV1);
    String metadataV2_1 = this.transformFromV2_0ToV2_1(metadataV2_0);
    String metadataV2_2 = this.transformFromV2_1ToV2_2(metadataV2_1);
    return this.transformFromV2_2ToV2_3(metadataV2_2);
  }

  public String transformFromV1ToV2_4(String metadataV1) {
    String metadataV2_3 = this.transformFromV1ToV2_3(metadataV1);
    return this.transformFromV2_3ToV2_4(metadataV2_3);
  }

  public String transformFromV2_0ToV2_2(String metadataV2_0) {
    String metadataV2_1 = this.transformFromV2_0ToV2_1(metadataV2_0);
    return this.transformFromV2_1ToV2_2(metadataV2_1);
  }

  public String transformFromV2_0ToV2_3(String metadataV2_0) {
    String metadataV2_1 = this.transformFromV2_0ToV2_1(metadataV2_0);
    String metadataV2_2 = this.transformFromV2_1ToV2_2(metadataV2_1);
    return this.transformFromV2_2ToV2_3(metadataV2_2);
  }

  public String transformFromV2_1ToV2_3(String metadataV2_1) {
    String metadataV2_2 = this.transformFromV2_1ToV2_2(metadataV2_1);
    return this.transformFromV2_2ToV2_3(metadataV2_2);
  }

  public String transformFromV2_2ToV2_3(String metadataV2_2) {
    AouXmlNamespacePrefixMapper aouXmlNamespacePrefixMapper = this.getAouXmlNamespacePrefixMapper(AouMetadataVersion.V2_1);
    XPath xpath = XMLTool.buildXPath(aouXmlNamespacePrefixMapper);
    try {
      Document document = XMLTool.parseXML(metadataV2_2);

      // transform container conference_title into conference_subtitle
      Node containerNode = (Node) xpath.compile("/aou_deposit:deposit_doc/aou_deposit:container").evaluate(document, XPathConstants.NODE);
      Node conferenceTitleNode = (Node) xpath.compile("/aou_deposit:deposit_doc/aou_deposit:container/aou_deposit:conference_title")
              .evaluate(document, XPathConstants.NODE);
      String conferenceTitle = this.getTextContentIfNotNull(conferenceTitleNode);

      if (containerNode != null && conferenceTitleNode != null && !StringTool.isNullOrEmpty(conferenceTitle)) {
        // create new Node
        Node conferenceSubtitleNode = document.createElementNS(AouConstants.METADATA_XML_NAMESPACE_V2_2, "conference_subtitle");
        conferenceSubtitleNode.setTextContent(conferenceTitle);
        containerNode.appendChild(conferenceSubtitleNode);

        // remove obsolete Node
        containerNode.removeChild(conferenceTitleNode);
      }

      // update xmlns for V2.3
      String metadata = XMLTool.documentToString(document);
      metadata = metadata.replace("\"" + AouConstants.METADATA_XML_NAMESPACE_V2_2 + "\"",
              "\"" + AouConstants.METADATA_XML_NAMESPACE_V2_3 + "\"");
      return metadata;

    } catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException | TransformerException e) {
      throw new SolidifyRuntimeException("Unable to transform metadata from V2.2 to V2.3", e);
    }
  }

  public String transformFromV2_2ToV2_4(String metadataV2_2) {
    String metadataV2_3 = this.transformFromV2_2ToV2_3(metadataV2_2);
    return this.transformFromV2_3ToV2_4(metadataV2_3);
  }

  public String transformFromV2_3ToV2_4(String metadataV2_3) {
    AouXmlNamespacePrefixMapper aouXmlNamespacePrefixMapper = this.getAouXmlNamespacePrefixMapper(AouMetadataVersion.V2_3);
    XPath xpath = XMLTool.buildXPath(aouXmlNamespacePrefixMapper);
    try {
      Document document = XMLTool.parseXML(metadataV2_3);

      // transform document file types
      Map<String, String> fileTypeUpgradeMapping = new HashMap<>();
      fileTypeUpgradeMapping.put("Master", "Master thesis");
      fileTypeUpgradeMapping.put("Report (Published version)", AouConstants.DOCUMENT_FILE_TYPE_REPORT);
      fileTypeUpgradeMapping.put("Report (Accepted version)", AouConstants.DOCUMENT_FILE_TYPE_REPORT);
      fileTypeUpgradeMapping.put("Report (Submitted version)", AouConstants.DOCUMENT_FILE_TYPE_REPORT);
      fileTypeUpgradeMapping.put("Working paper (Published version)", AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER);
      fileTypeUpgradeMapping.put("Working paper (Accepted version)", AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER);
      fileTypeUpgradeMapping.put("Working paper (Submitted version)", AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER);

      NodeList filesNode = (NodeList) xpath.compile("/aou_deposit:deposit_doc/aou_deposit:files/aou_deposit:file")
              .evaluate(document, XPathConstants.NODESET);
      for (int i = 0; i < filesNode.getLength(); i++) {
        Node fileNode = filesNode.item(i);

        Node fileTypeNode = (Node) xpath.compile("./aou_deposit:type").evaluate(fileNode, XPathConstants.NODE);
        String fileType = fileTypeNode.getTextContent();
        if (fileTypeUpgradeMapping.containsKey(fileType)) {
          fileTypeNode.setTextContent(fileTypeUpgradeMapping.get(fileType));
        }
      }

      // update xmlns for V2.4
      String metadata = XMLTool.documentToString(document);
      metadata = metadata.replace("\"" + AouConstants.METADATA_XML_NAMESPACE_V2_3 + "\"",
              "\"" + AouConstants.METADATA_XML_NAMESPACE_V2_4 + "\"");
      return metadata;

    } catch (XPathExpressionException | ParserConfigurationException | SAXException | IOException | TransformerException e) {
      throw new SolidifyRuntimeException("Unable to transform metadata from V2.3 to V2.4", e);
    }
  }

  private String getTextContentIfNotNull(Node node) {
    if (node != null && !StringTool.isNullOrEmpty(node.getTextContent())) {
      return node.getTextContent();
    }
    return null;
  }

  /******************************************************************************************************/

  /**
   * Return true if the publication metadata have been updated by adding or removing UNIGE URN according to conditions based on metadata
   * Note: publications already sent to Swiss National Library keep their URN even if conditions do not match anymore.
   *
   * @param publication
   * @return
   */
  public boolean updateUnigeUrnIfRequired(Publication publication) {
    if (this.fillConditionsForUnigeURN(publication) && !this.hasUnigeURN(publication)) {
      this.completeUnigeUrn(publication);
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      return true;
    } else if (!this.fillConditionsForUnigeURN(publication) && this.hasUnigeURN(publication)
            && publication.getSwissNationalLibraryUploads().isEmpty()) {
      this.removeUnigeUrn(publication);
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      return true;
    }
    return false;
  }

  private void completeUnigeUrn(Publication publication) {
    String urn = MetadataTool.generateSwissNationalLibraryUrn(this.swissNationalLibraryUrnPrefix, publication.getArchiveId());
    this.setUrn(publication, urn);
  }

  private void removeUnigeUrn(Publication publication) {
    this.setUrn(publication, null);
  }

  private void setUrn(Publication publication, String urn) {
    final MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDocObj = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    metadataExtractor.setURN(depositDocObj, urn);
    String metadata = metadataExtractor.serializeDepositDocToXml(depositDocObj);
    publication.setMetadata(metadata);
  }

  private boolean fillConditionsForUnigeURN(Publication publication) {
    // Thesis only
    if (!publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID)) {
      return false;
    }

    // Status must be IN_PREPARATION or COMPLETED or publication must have already been completed once
    if (publication.getStatus() != Publication.PublicationStatus.SUBMITTED
            && publication.getStatus() != Publication.PublicationStatus.COMPLETED
            && !this.historyService.hasBeenCompleted(publication.getResId())) {
      return false;
    }

    // Must already have an archive id
    if (StringTool.isNullOrEmpty(publication.getArchiveId())) {
      return false;
    }

    final MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());

    // Defense date >= 2002
    Optional<MetadataDates> defenseDateOpt = metadataExtractor.getDefenseDate(depositDoc);
    if (defenseDateOpt.isEmpty() || this.defenseDateValueTooOldForUrn(defenseDateOpt.get().getContent())) {
      return false;
    }

    // Must be UNIGE
    if (metadataExtractor.isBeforeUnige(depositDoc)) {
      return false;
    }

    // Contains at least one file
    return publication.getDocumentFiles() != null && !publication.getDocumentFiles().isEmpty();
  }

  private boolean defenseDateValueTooOldForUrn(String defenseDate) {
    Integer defenseYear = this.getYear(defenseDate);
    return defenseYear == null || defenseYear < this.minimumYearForUrn;
  }

  /******************************************************************************************************/

  /**
   * Return true if the publication metadata have been updated by adding or removing UNIGE DOI according to conditions based on metadata
   *
   * @param publication
   * @return
   */
  public boolean updateUnigeDoiIfRequired(Publication publication) {
    if (this.fillConditionsForUnigeDOI(publication) && !this.hasUnigeDOI(publication)) {
      this.completeUnigeDoi(publication);
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      return true;
    }
    return false;
  }

  private void completeUnigeDoi(Publication publication) {
    this.setDoi(publication, this.unigeDoiPrefix + publication.getArchiveId());
  }

  private void setDoi(Publication publication, String doi) {
    final MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDocObj = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    metadataExtractor.setDOI(depositDocObj, doi);
    String metadata = metadataExtractor.serializeDepositDocToXml(depositDocObj);
    publication.setMetadata(metadata);
  }

  private boolean fillConditionsForUnigeDOI(Publication publication) {
    // Thesis or privat-docent thesis only
    if (!publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID)
            && !publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_ID)) {
      return false;
    }

    // Status must be IN_PREPARATION or COMPLETED or publication must have already been completed once
    if (publication.getStatus() != Publication.PublicationStatus.SUBMITTED
            && publication.getStatus() != Publication.PublicationStatus.COMPLETED
            && !this.historyService.hasBeenCompleted(publication.getResId())) {
      return false;
    }

    // Must already have an archive id
    if (StringTool.isNullOrEmpty(publication.getArchiveId())) {
      return false;
    }

    final MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());

    // Must be UNIGE
    if (metadataExtractor.isBeforeUnige(depositDoc)) {
      return false;
    }

    return true;
  }

  public boolean isBefore2010orBeforeAuthorJoiningUnige(Publication publication) {
    final MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    return metadataExtractor.isBefore2010orBeforeAuthorJoiningUnige(depositDoc);
  }

  /******************************************************************************************************/

  public boolean hasUnigeDOI(Publication publication) {
    final MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    String doi = metadataExtractor.getDOI(depositDoc);
    return !StringTool.isNullOrEmpty(doi) && doi.startsWith(this.unigeDoiPrefix);
  }

  public String getUnigeDoiToPreserve(Publication publication) {
    if ((publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID)
            || publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_ID))
            && this.hasUnigeDOI(publication)) {
      MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
      DepositDoc depositDoc = (DepositDoc) metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
      return metadataExtractor.getDOI(depositDoc);
    }
    return null;
  }

  public void restoreUnigeDoiToPreserve(Publication publication, String unigeDoiToPreserve) {
    if (publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID)
            || publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_ID)) {
      if (publication.isMetadataDirty()) {
        MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
        Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
        metadataExtractor.setDOI(depositDoc, unigeDoiToPreserve);
        String metadata = metadataExtractor.serializeDepositDocToXml(depositDoc);
        publication.setMetadata(metadata);
      }
      if (publication.isFormDataDirty()) {
        JSONObject jsonObject = new JSONObject(publication.getFormData());
        this.ensureDescriptionIdentifiersExits(jsonObject);
        jsonObject.getJSONObject("description").getJSONObject("identifiers").put("doi", unigeDoiToPreserve);
        publication.setFormData(jsonObject.toString());
      }
    }
  }

  public boolean hasUnigeURN(Publication publication) {
    String urn = this.getUrn(publication);
    return !StringTool.isNullOrEmpty(urn) && urn.startsWith(this.swissNationalLibraryUrnPrefix);
  }

  public String getUrn(Publication publication) {
    final MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    return metadataExtractor.getURN(depositDoc);
  }

  /**
   * For UNIGE thesis, an URN value is computed when deposit is submitted. This value may not be posted back by the frontend if the thesis
   * is edited, but it has to be preserved.
   *
   * @param publication
   * @return
   */
  public String getUnigeUrnToPreserve(Publication publication) {
    if (publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID) && this.hasUnigeURN(publication)) {
      MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
      DepositDoc depositDoc = (DepositDoc) metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
      return metadataExtractor.getURN(depositDoc);
    }
    return null;
  }

  /**
   * If there was an URN to preserve and patching the resource has removed it, restore it
   *
   * @param publication
   * @param unigeUrnToPreserve
   */
  public void restoreUnigeUrnToPreserve(Publication publication, String unigeUrnToPreserve) {
    if (publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID)) {
      if (publication.isMetadataDirty()) {
        MetadataExtractor metadataExtractor = this.getMetadataExtractor(publication.getMetadataVersion());
        Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
        metadataExtractor.setURN(depositDoc, unigeUrnToPreserve);
        String metadata = metadataExtractor.serializeDepositDocToXml(depositDoc);
        publication.setMetadata(metadata);
      }
      if (publication.isFormDataDirty()) {
        JSONObject jsonObject = new JSONObject(publication.getFormData());
        this.ensureDescriptionIdentifiersExits(jsonObject);
        jsonObject.getJSONObject("description").getJSONObject("identifiers").put("urn", unigeUrnToPreserve);
        publication.setFormData(jsonObject.toString());
      }
    }
  }

  private void ensureDescriptionIdentifiersExits(JSONObject jsonObject) {
    if (!jsonObject.has("description") || jsonObject.isNull("description")) {
      jsonObject.put("description", new JSONObject());
    }
    if (!jsonObject.getJSONObject("description").has("identifiers") || jsonObject.getJSONObject("description").isNull("identifiers")) {
      jsonObject.getJSONObject("description").put("identifiers", new JSONObject());
    }
  }

}
