/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DataCiteImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.Text;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class DataCiteImportService extends MetadataImportService {

  private final String apiUrl;

  public DataCiteImportService(AouProperties aouProperties, MessageService messageService,
          MetadataService metadataService, ContributorService contributorService,
          JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService) {
    super(aouProperties, messageService, metadataService, contributorService, journalTitleRemoteService, licenseService);
    this.apiUrl = aouProperties.getMetadata().getImports().getDataCite().getUrl();
  }

  public DepositDoc searchDoi(String doi, DepositDoc depositDoc) {
    final JsonNode parentNode = this.getJsonFromDataCite(doi);
    final JsonNode dataNode = parentNode.get("data");
    this.fillDepositDoc(depositDoc, dataNode);
    return depositDoc;
  }

  private JsonNode getJsonFromDataCite(String doi) {
    try {
      RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
      RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();
      Map<String, String> parameters = new HashMap<>();
      parameters.put("doi", doi);
      final String jsonString = client.getForObject(this.apiUrl, String.class, parameters);
      return new ObjectMapper().readTree(jsonString);
    } catch (HttpClientErrorException e) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND,
              this.messageService.get("deposit.error.identifiers.doi.not_found_by_datacite", new Object[] { doi }));
    } catch (IOException e) {
      throw new SolidifyRuntimeException("An error occurred when searching DOI '" + doi + "' on DataCite", e);
    }
  }

  public void fillDepositDoc(DepositDoc depositDoc, JsonNode dataNode) {
    final JsonNode attributesNode = dataNode.get("attributes");
    this.fillDoi(depositDoc, dataNode);
    this.fillTypes(depositDoc, attributesNode);
    this.fillTitle(depositDoc, attributesNode);
    this.fillLanguage(depositDoc, attributesNode);
    this.fillContributors(depositDoc, attributesNode);
    this.fillPublicationYear(depositDoc, attributesNode);
    this.fillAbstracts(depositDoc, attributesNode);
    this.fillContainer(depositDoc, attributesNode);
    this.fillKeywords(depositDoc, attributesNode);
  }

  private void fillDoi(DepositDoc depositDoc, JsonNode dataNode) {
    String doi = dataNode.get("id").textValue();
    this.ensureDepositIdentifiersExist(depositDoc);
    depositDoc.getIdentifiers().setDoi(doi);
  }

  private void fillTypes(DepositDoc depositDoc, JsonNode attributesNode) {
    JsonNode typesNode = attributesNode.get("types");
    if (typesNode.has("resourceTypeGeneral")) {
      JsonNode typeNode = typesNode.get("resourceTypeGeneral");
      String type = this.getPublicationType(typeNode.textValue());
      depositDoc.setType(type);
      String subtype = this.getPublicationSubType(typeNode.textValue());
      depositDoc.setSubtype(subtype);
    } else {
      throw new SolidifyRuntimeException("Unable to match type and subtype");
    }
  }

  private void fillTitle(DepositDoc depositDoc, JsonNode attributesNode) {
    JsonNode titlesNode = attributesNode.get("titles");
    if (titlesNode.isArray()) {
      for (JsonNode titleNode : titlesNode) {
        String titleValue = null;
        String titleLangCode = null;

        if (titleNode.has("title")) {
          titleValue = titleNode.get("title").textValue();
        }
        if (titleNode.has("lang")) {
          titleLangCode = CleanTool.getLanguageCode(titleNode.get("lang").textValue());
        }

        if (!StringTool.isNullOrEmpty(titleValue)) {
          String mainLanguage = this.getMainLanguage(attributesNode);
          if (!StringTool.isNullOrEmpty(mainLanguage) && !StringTool.isNullOrEmpty(titleLangCode) && !mainLanguage.equals(titleLangCode)) {
            depositDoc.setOriginalTitle(this.getTextLang(titleValue, titleLangCode));
          } else {
            depositDoc.setTitle(this.getText(titleValue, mainLanguage));
          }
        }
      }
    }
  }

  private void fillLanguage(DepositDoc depositDoc, JsonNode attributesNode) {
    String mainLanguage = this.getMainLanguage(attributesNode);
    if (!StringTool.isNullOrEmpty(mainLanguage)) {
      this.ensureLanguagesExist(depositDoc);
      depositDoc.getLanguages().getLanguage().add(CleanTool.getLanguageCode(mainLanguage));
    }
  }

  private void fillContributors(DepositDoc depositDoc, JsonNode attributesNode) {
    JsonNode creatorsNode = attributesNode.get("creators");
    if (creatorsNode.isArray()) {
      for (JsonNode creatorNode : creatorsNode) {
        // Contributor name
        String givenName = null;
        String familyName = null;
        String name = null;

        if (creatorNode.has("givenName")) {
          givenName = creatorNode.get("givenName").textValue();
        }
        if (creatorNode.has("familyName")) {
          familyName = creatorNode.get("familyName").textValue();
        }
        if (creatorNode.has("name")) {
          name = creatorNode.get("name").textValue();
        }

        if (StringTool.isNullOrEmpty(givenName) && StringTool.isNullOrEmpty(familyName) && name != null && name.contains(",")) {
          familyName = name.split(",")[0].trim();
          givenName = name.split(",")[1].trim();
        }

        familyName = this.getNameOrUndefinedValue(familyName);
        givenName = this.getNameOrUndefinedValue(givenName);

        // ORCID
        String orcid = null;
        if (creatorNode.has("nameIdentifiers")) {
          JsonNode nameIdentifiersNode = creatorNode.get("nameIdentifiers");
          if (nameIdentifiersNode.isArray()) {
            for (JsonNode nameIdentifierNode : nameIdentifiersNode) {
              if (nameIdentifierNode.has("nameIdentifierScheme")) {
                String scheme = nameIdentifierNode.get("nameIdentifierScheme").textValue();
                if ("ORCID".equals(scheme)) {
                  orcid = nameIdentifierNode.get("nameIdentifier").textValue();
                }
              }
            }
          }
        }

        // Affiliation
        List<String> affiliationsList = new ArrayList<>();
        String affiliations = null;
        if (creatorNode.has("affiliation")) {
          JsonNode affiliationsNode = creatorNode.get("affiliation");
          if (affiliationsNode.isArray()) {
            for (JsonNode affiliationNode : affiliationsNode) {
              affiliationsList.add(affiliationNode.textValue());
            }
          }
          if (!affiliationsList.isEmpty()) {
            affiliations = "";
            for (String affiliation : affiliationsList) {
              affiliations += affiliation + ", ";
            }
            affiliations = affiliations.substring(0, affiliations.length() - 2);
          }
        }

        Contributor contributor = new Contributor();
        contributor.setLastname(CleanTool.capitalizeString(familyName));
        contributor.setFirstname(CleanTool.capitalizeString(givenName));
        contributor.setOrcid(orcid);
        contributor.setInstitution(affiliations);
        this.ensureContributorsExist(depositDoc);
        depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
      }
    }
  }

  private void fillPublicationYear(DepositDoc depositDoc, JsonNode attributesNode) {
    if (attributesNode.has("publicationYear")) {
      String publicationYearStr;
      if (attributesNode.get("publicationYear").isNumber()) {
        publicationYearStr = String.valueOf(attributesNode.get("publicationYear").intValue());
      } else {
        publicationYearStr = attributesNode.get("publicationYear").textValue();
      }
      this.ensureDatesExist(depositDoc);
      depositDoc.getDates().getDate().add(this.getDateWithType(DateTypes.PUBLICATION, publicationYearStr));
    }
  }

  private void fillAbstracts(DepositDoc depositDoc, JsonNode attributesNode) {
    if (attributesNode.has("descriptions")) {
      JsonNode descriptionsNode = attributesNode.get("descriptions");
      if (descriptionsNode.isArray()) {
        for (JsonNode descriptionNode : descriptionsNode) {
          if (descriptionNode.has("descriptionType") && descriptionNode.get("descriptionType").textValue().equals("Abstract")
                  && descriptionNode.has("description")) {
            this.ensureAbstractsExist(depositDoc);
            Text text = this.getText(descriptionNode.get("description").textValue());
            depositDoc.getAbstracts().getAbstract().add(text);
            break;
          }

        }
      }
    }
  }

  private void fillContainer(DepositDoc depositDoc, JsonNode attributesNode) {
    if (attributesNode.has("container") && attributesNode.get("container").has("title")) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setTitle(attributesNode.get("container").get("title").textValue());
    }
  }

  private void fillKeywords(DepositDoc depositDoc, JsonNode attributesNode) {
    if (attributesNode.has("subjects")) {
      JsonNode subjectsNode = attributesNode.get("subjects");
      if (subjectsNode.isArray()) {
        Set<String> subjects = new HashSet<>();
        for (JsonNode subjectNode : subjectsNode) {
          if (subjectNode.has("subject")) {
            subjects.add(subjectNode.get("subject").textValue());
          }
        }
        if (!subjects.isEmpty()) {
          this.ensureKeywordsExist(depositDoc);
          for (String subject : subjects) {
            depositDoc.getKeywords().getKeyword().add(subject);
          }
        }
      }
    }
  }

  private String getMainLanguage(JsonNode attributesNode) {
    if (attributesNode.has("language")) {
      JsonNode languageNode = attributesNode.get("language");
      return CleanTool.getLanguageCode(languageNode.textValue());
    }
    return null;
  }

  private String getPublicationType(String value) {
    return switch (value) {
      case "Book", "BookChapter" -> AouConstants.DEPOSIT_TYPE_LIVRE_NAME;
      case "ConferencePaper", "ConferenceProceeding", "Presentation" -> AouConstants.DEPOSIT_TYPE_CONFERENCE_NAME;
      case "Dissertation" -> AouConstants.DEPOSIT_TYPE_DIPLOME_NAME;
      case "Journal" -> AouConstants.DEPOSIT_TYPE_JOURNAL_NAME;
      case "Preprint", "Report" -> AouConstants.DEPOSIT_TYPE_RAPPORT_NAME;
      default -> AouConstants.DEPOSIT_TYPE_ARTICLE_NAME;
    };
  }

  private String getPublicationSubType(String value) {
    return switch (value) {
      case "Book" -> AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME;
      case "BookChapter" -> AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME;
      case "ConferencePaper" -> AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME;
      case "ConferenceProceeding", "Presentation" -> AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME;
      case "Dissertation" -> AouConstants.DEPOSIT_SUBTYPE_THESE_NAME;
      case "Journal" -> AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME;
      case "Preprint" -> AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME;
      case "Report" -> AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME;
      default -> AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME;
    };
  }
}
