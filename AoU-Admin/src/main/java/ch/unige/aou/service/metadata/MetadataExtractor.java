/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataExtractor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LabeledLanguageService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.display.StructureValidationRight;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.publication.MetadataClassification;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataCorrection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.MetadataFunding;
import ch.unige.aou.model.publication.MetadataLink;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.model.rest.JournalTitleDTO;
import ch.unige.aou.model.settings.LabeledLanguage;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.service.metadata.imports.DOIResolver;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.rest.UnigePersonSearchService;

public abstract class MetadataExtractor {

  private static final Logger log = LoggerFactory.getLogger(MetadataExtractor.class);

  private static final String INCORRECT_DATE = "yyyy-mm";

  private static final String ERROR_VALIDATION_STRUCTURE_NOT_AUTOMATICALLY_SET = "deposit.error.structure.validation_structure_not_automatically_set";

  protected final DepositDocAdapter depositDocAdapter;

  protected PublicationTypeService publicationTypeService;
  protected PublicationSubtypeService publicationSubtypeService;
  protected PublicationSubSubtypeService publicationSubSubtypeService;
  protected StructureService structureService;
  protected ResearchGroupService researchGroupService;
  protected ContributorService contributorService;
  protected LabeledLanguageService labeledLanguageService;
  protected JournalTitleRemoteService journalTitleRemoteService;
  protected PersonService personService;
  protected final MessageService messageService;
  protected final UnigePersonSearchService unigePersonSearchService;

  private final String[] metadataOutputDateFormat;
  private final String[] frontendEnterDateFormat;
  private final String[] frontendOutputDateFormat;
  private final Map<String, String> defaultContributorRolesByDepositSubtype;
  private final String[] metadataDateFormat;

  protected MetadataExtractor(AouProperties aouProperties, DepositDocAdapter depositDocAdapter, PublicationTypeService publicationTypeService,
          PublicationSubtypeService publicationSubtypeService,
          PublicationSubSubtypeService publicationSubSubtypeService,
          StructureService structureService,
          ResearchGroupService researchGroupService,
          ContributorService contributorService,
          LabeledLanguageService labeledLanguageService,
          JournalTitleRemoteService journalTitleRemoteService, PersonService personService, MessageService messageService,
          UnigePersonSearchService unigePersonSearchService) {

    this.depositDocAdapter = depositDocAdapter;

    this.publicationTypeService = publicationTypeService;
    this.publicationSubtypeService = publicationSubtypeService;
    this.publicationSubSubtypeService = publicationSubSubtypeService;
    this.structureService = structureService;
    this.researchGroupService = researchGroupService;
    this.contributorService = contributorService;
    this.labeledLanguageService = labeledLanguageService;
    this.journalTitleRemoteService = journalTitleRemoteService;
    this.personService = personService;
    this.messageService = messageService;
    this.unigePersonSearchService = unigePersonSearchService;

    this.metadataOutputDateFormat = aouProperties.getParameters().getMetadataOutputDateFormat();
    this.frontendEnterDateFormat = aouProperties.getParameters().getFrontendEnterDateFormat();
    this.frontendOutputDateFormat = aouProperties.getParameters().getFrontendOutputDateFormat();
    this.defaultContributorRolesByDepositSubtype = aouProperties.getMetadata().getDefault().getContributorRolesByDepositSubtype();
    this.metadataDateFormat = aouProperties.getParameters().getMetadataDateFormat();
  }

  /*******************************************************************************************/

  public void setMetadataFromFormData(Publication publication) {
    String xmlMetadata = this.transformFormDataToMetadata(publication.getFormData());
    publication.setMetadata(xmlMetadata);
    publication.setMetadataVersion(AouMetadataVersion.getDefaultVersion());
  }

  public void setFormDataFromMetadata(Publication publication) {
    String formData = this.transformMetadataToFormData(publication.getMetadata());
    publication.setFormData(formData);
  }

  /**
   * Replace ids values by their text value (useful to be stored in pivotal metadata)
   *
   * @param depositDocObj Pivotal metadata in memory object
   */
  public void replaceIdsByTextValues(Object depositDocObj) {

    /*
     * Type
     */
    String typeStr = this.getType(depositDocObj);
    if (!StringTool.isNullOrEmpty(typeStr)) {
      Optional<PublicationType> type = this.publicationTypeService.findById(typeStr);
      type.ifPresent(publicationType -> this.setType(depositDocObj, publicationType.getName()));
    }

    /*
     * Subtype
     */
    String subtypeStr = this.getSubtype(depositDocObj);
    if (!StringTool.isNullOrEmpty(subtypeStr)) {
      Optional<PublicationSubtype> subType = this.publicationSubtypeService.findById(subtypeStr);
      subType.ifPresent(publicationSubtype -> this.setSubtype(depositDocObj, publicationSubtype.getName()));
    }

    /*
     * SubSubtype
     */
    String subSubtypeStr = this.getSubSubtype(depositDocObj);
    if (!StringTool.isNullOrEmpty(subSubtypeStr)) {
      Optional<PublicationSubSubtype> subSubType = this.publicationSubSubtypeService.findById(subSubtypeStr);
      subSubType.ifPresent(publicationSubSubtype -> this.setSubSubtype(depositDocObj, publicationSubSubtype.getName()));
    }

    /*
     * Title's language
     */
    String titleLangStr = this.getTitleLang(depositDocObj);
    if (!StringTool.isNullOrEmpty(titleLangStr)) {
      Optional<LabeledLanguage> language = this.labeledLanguageService.findById(titleLangStr);
      language.ifPresent(lang -> this.setTitleLang(depositDocObj, lang.getCode()));
    }

    /*
     * Original Title's language
     */
    String originalTitleLangStr = this.getOriginalTitleLang(depositDocObj);
    if (!StringTool.isNullOrEmpty(originalTitleLangStr)) {
      Optional<LabeledLanguage> language = this.labeledLanguageService.findById(originalTitleLangStr);
      language.ifPresent(lang -> this.setOriginalTitleLang(depositDocObj, lang.getCode()));
    }

    /*
     * Abstracts' languages
     */
    List<MetadataTextLang> abstracts = this.getAbstracts(depositDocObj);
    if (!abstracts.isEmpty()) {
      for (MetadataTextLang abstractText : abstracts) {
        if (!StringTool.isNullOrEmpty(abstractText.getLang())) {
          Optional<LabeledLanguage> language = this.labeledLanguageService.findById(abstractText.getLang());
          language.ifPresent(lang -> abstractText.setLang(lang.getCode()));
        }
      }
      this.setAbstracts(depositDocObj, abstracts);
    }

    /*
     * Links descriptions language
     */
    List<MetadataLink> links = this.getLinks(depositDocObj);
    if (!links.isEmpty()) {
      for (MetadataLink link : links) {
        if (link.getDescription() != null && !StringTool.isNullOrEmpty(link.getDescription().getLang())) {
          Optional<LabeledLanguage> language = this.labeledLanguageService.findById(link.getDescription().getLang());
          language.ifPresent(lang -> link.getDescription().setLang(lang.getCode()));
        }
      }
      this.setLinks(depositDocObj, links);
    }
  }

  /**
   * Replace text values by their ids (useful for the frontend form that uses ids for select values)
   *
   * @param depositDocObj Pivotal metadata in memory object
   */
  public void replaceTextValuesByIds(Object depositDocObj) {

    /*
     * Type
     */
    String typeStr = this.getType(depositDocObj);
    if (!StringTool.isNullOrEmpty(typeStr)) {
      PublicationType type = this.publicationTypeService.findByName(typeStr);
      if (type != null) {
        this.setType(depositDocObj, type.getResId());
      }
    }

    /*
     * Subtype
     */
    String subtypeStr = this.getSubtype(depositDocObj);
    if (!StringTool.isNullOrEmpty(subtypeStr)) {
      Optional<PublicationSubtype> subtypeOpt = this.publicationSubtypeService.findByName(subtypeStr);
      if (subtypeOpt.isPresent()) {
        this.setSubtype(depositDocObj, subtypeOpt.get().getResId());

        /*
         * SubSubtype
         */
        String subSubtypeStr = this.getSubSubtype(depositDocObj);
        if (!StringTool.isNullOrEmpty(subSubtypeStr)) {
          PublicationSubSubtype subSubtype = this.publicationSubSubtypeService.findByNameAndPublicationSubType(subSubtypeStr, subtypeOpt.get());
          if (subSubtype != null) {
            this.setSubSubtype(depositDocObj, subSubtype.getResId());
          }
        }
      }
    }

    /*
     * Title's language
     */
    String titleLangStr = this.getTitleLang(depositDocObj);
    if (!StringTool.isNullOrEmpty(titleLangStr)) {
      LabeledLanguage language = this.labeledLanguageService.findByCode(titleLangStr);
      if (language != null) {
        this.setTitleLang(depositDocObj, language.getResId());
      }
    }

    /*
     * Original title's language
     */
    String originalTitleLangStr = this.getOriginalTitleLang(depositDocObj);
    if (!StringTool.isNullOrEmpty(originalTitleLangStr)) {
      LabeledLanguage language = this.labeledLanguageService.findByCode(originalTitleLangStr);
      if (language != null) {
        this.setOriginalTitleLang(depositDocObj, language.getResId());
      }
    }

    /*
     * Abstracts' languages
     */
    List<MetadataTextLang> abstracts = this.getAbstracts(depositDocObj);
    if (!abstracts.isEmpty()) {
      for (MetadataTextLang abstractText : abstracts) {
        if (!StringTool.isNullOrEmpty(abstractText.getLang())) {
          LabeledLanguage language = this.labeledLanguageService.findByCode(abstractText.getLang());
          if (language != null) {
            abstractText.setLang(language.getResId());
          }
        }
      }
      this.setAbstracts(depositDocObj, abstracts);
    }

    /*
     * Links descriptions language
     */
    List<MetadataLink> links = this.getLinks(depositDocObj);
    if (!links.isEmpty()) {
      for (MetadataLink link : links) {
        if (link.getDescription() != null && !StringTool.isNullOrEmpty(link.getDescription().getLang())) {
          LabeledLanguage language = this.labeledLanguageService.findByCode(link.getDescription().getLang());
          if (language != null) {
            link.getDescription().setLang(language.getResId());
          }
        }
      }
      this.setLinks(depositDocObj, links);
    }
  }

  /**
   * Replace fields values that are different between front form data and pivotal metadata
   *
   * @param depositDocObj
   */
  public void replaceFormValuesByMetadataValues(Object depositDocObj) {
    this.replaceIdsByTextValues(depositDocObj);
    this.formatDepositDocDatesToMetadataFormat(depositDocObj);
  }

  /**
   * Replace fields values that are different between front form data and pivotal metadata
   *
   * @param depositDocObj
   */
  public void replaceMetadataValuesByFormValues(Object depositDocObj) {
    this.replaceTextValuesByIds(depositDocObj);
    this.formatDepositDocDatesToFrontendFormat(depositDocObj);
  }

  public void replaceISSNByISSNL(Publication publication) {
    Object depositDocObj = this.createDepositDocObjectFromXml(publication.getMetadata());

    String issn = this.getISSN(depositDocObj);
    if (!StringTool.isNullOrEmpty(issn)) {
      //Find issn-l related with the issn using journalService
      RestCollection<JournalTitleDTO> journalTitles = this.journalTitleRemoteService.getJournalTitlesByISSN(issn);
      if (journalTitles.getData() != null && journalTitles.getData().iterator().hasNext()) {
        JournalTitleDTO journalTitleDTO = journalTitles.getData().iterator().next();
        String issnl = journalTitleDTO.getIssnl();
        if (!issn.equals(issnl)) {
          this.setISSN(depositDocObj, issnl);
        }
      }
      String metadata = this.serializeDepositDocToXml(depositDocObj);
      publication.setMetadata(metadata);
    }
  }

  public void replaceShortDOIbyDOI(Publication publication) {
    Object depositDocObj = this.createDepositDocObjectFromXml(publication.getMetadata());
    String doi = this.getDOI(depositDocObj);

    if (ValidationTool.isValidShortDOI(doi)) {
      // In case of shortDOI, its corresponding DOI must be retrieved
      try {
        doi = DOIResolver.getDoiFromShortDOI(doi);
        this.setDOI(depositDocObj, doi);
      } catch (WebClientResponseException exception) {
        this.setDOI(depositDocObj, doi);
      }

      String metadata = this.serializeDepositDocToXml(depositDocObj);
      publication.setMetadata(metadata);
    }
  }

  /**
   * Set the publication properties that exists in metadata with the values in metadata
   *
   * @param publication
   */
  public void setPropertiesFromMetadata(Publication publication) {

    Object depositDocObj = this.createDepositDocObjectFromXml(publication.getMetadata());

    // Title
    String titleContent = this.getTitleContent(depositDocObj);
    if (!StringTool.isNullOrEmpty(titleContent)) {
      publication.setTitle(titleContent);
    }

    // Subtype
    PublicationSubtype subtype = null;
    String subtypeStr = this.getSubtype(depositDocObj);
    if (!StringTool.isNullOrEmpty(subtypeStr)) {
      Optional<PublicationSubtype> subtypeOpt = this.publicationSubtypeService.findByName(subtypeStr);
      if (subtypeOpt.isPresent()) {
        subtype = subtypeOpt.get();
        publication.setSubtype(subtype);
      }
    }

    // Subsubtype
    String subSubtypeStr = this.getSubSubtype(depositDocObj);
    if (subtype != null && !StringTool.isNullOrEmpty(subSubtypeStr)) {
      PublicationSubSubtype subSubtype = this.publicationSubSubtypeService.findByNameAndPublicationSubType(subSubtypeStr, subtype);
      if (subSubtype != null) {
        publication.setSubSubtype(subSubtype);
      }
    } else {
      publication.setSubSubtype(null);
    }

    // DOI
    String doi = this.getDOI(depositDocObj);
    if (!StringTool.isNullOrEmpty(doi)) {
      publication.setDoi(doi);
    } else {
      publication.setDoi(null);
    }

    // PMID
    BigInteger pmid = this.getPmid(depositDocObj);
    if (pmid != null) {
      publication.setPmid(pmid.toString());
    } else {
      publication.setPmid(null);
    }
  }

  public void setRelationsFromMetadata(Publication publication) {

    Object depositDocObj = this.createDepositDocObjectFromXml(publication.getMetadata());

    /*
     * Structures
     */
    List<Structure> structures = this.getStructures(depositDocObj);
    if (structures != null) {
      this.syncPublicationStructuresList(publication, structures);
    }

    /*
     * Research groups
     */
    List<ResearchGroup> groups = this.getResearchGroups(depositDocObj);
    if (groups != null) {
      this.syncResearchGroupsList(publication, groups);
    }

    /*
     * Contributors and links between Publication and ContributorOtherName
     */
    List<AbstractContributor> abstractContributors = this.getContributors(depositDocObj);
    if (abstractContributors != null) {
      this.syncContributorsAndPublicationContributorOtherNameList(publication, abstractContributors);
    }
  }

  public JSONArray transformAcademicStructuresToJson(List<Structure> structures) {
    JSONArray structuresArray = new JSONArray();
    for (Structure st : structures) {
      structuresArray.put(new JSONObject()
              .put("name", st.getName())
              .put("code", st.getCodeStruct())
              .put("oldCode", st.getCnStructC())
              .put("resId", st.getResId())
      );
    }
    return structuresArray;
  }

  public JSONArray transformGroupsToJson(List<ResearchGroup> researchGroups) {
    JSONArray groupsArray = new JSONArray();
    for (ResearchGroup rg : researchGroups) {
      groupsArray.put(new JSONObject()
              .put("name", rg.getName())
              .put("code", rg.getCode())
              .put("resId", rg.getResId())
      );
    }
    return groupsArray;
  }

  public void setDefaultContributorsRoles(Object depositDocObj) {

    String subtype = this.getSubtype(depositDocObj);
    Optional<PublicationSubtype> publicationSubtypeOpt = this.publicationSubtypeService.findByName(subtype);
    if (publicationSubtypeOpt.isPresent()) {
      AuthorRole defaultRole = AuthorRole.fromValue(this.defaultContributorRolesByDepositSubtype.get(publicationSubtypeOpt.get().getResId()));

      List<AbstractContributor> contributors = this.getContributors(depositDocObj);

      contributors.stream()
              .filter(c -> c instanceof ContributorDTO && StringTool.isNullOrEmpty(((ContributorDTO) c).getRole()))
              .map(ContributorDTO.class::cast)
              .forEach(c -> c.setRole(defaultRole.value()));

      this.setContributors(depositDocObj, contributors);
    } else {
      throw new SolidifyRuntimeException(String.format("PublicationSubtype '%s' not found", subtype));
    }
  }

  /*******************************************************************************************/

  /**
   * Transform metadata from the format used by the frontend form to the format used to store metadata
   *
   * @param formData
   * @return
   */
  public abstract String transformFormDataToMetadata(String formData);

  /**
   * Transform metadata from the format used to store metadata to the format used by the frontend form
   *
   * @param metadata
   * @return
   */
  public abstract String transformMetadataToFormData(String metadata);

  /**
   * Serialize a object representing the pivotal metadata to string
   *
   * @param depositDocObj
   * @return
   */
  public abstract String serializeDepositDocToXml(Object depositDocObj);

  public abstract void completeMetadataWithFiles(Publication publication);

  public abstract void resetUnigeContributorsDisplayName(Publication publication);

  public abstract void replaceUnigeContributorsDisplayName(Publication publication);

  public abstract void completeClassificationsMetadataWithDeweyCodes(Publication publication);

  public abstract void completeStructureResId(Publication publication);

  public abstract void completeResearchGroupResId(Publication publication);

  public abstract void completeContributorsCnIndividuFromOrcid(Publication publication);

  protected abstract void formatDepositDocDatesToFrontendFormat(Object depositDocObj);

  protected abstract void formatDepositDocDatesToMetadataFormat(Object depositDocObj);

  public abstract Object createDepositDocObjectFromXml(String xmlData);

  public abstract Object createDepositDocFromJsonFormData(String jsonData);

  public abstract String getPmcIdFromPublication(Publication publication);

  public abstract void addResearchGroup(ResearchGroup researchGroup, Publication publication);

  public abstract void addStructure(Structure structure, Publication publication);

  public abstract void removeStructure(Structure structure, Publication publication);

  public abstract void addLanguage(LabeledLanguage labeledLanguage, Publication publication);

  public abstract boolean isCnIndividuInContributors(Publication publication, String cnIndividu);

  public abstract void removeCnIndividuFromContributors(Publication publication, String cnIndividu);

  public abstract void updatePublicationListWithContributorDisplayName(Publication publication, String cnIndividu, String lastname,
          String firstname, String[] publicationIds);

  public abstract void cleanMetadata(Publication publication);

  public abstract void cleanMetadataToClone(Publication publication);

  /*******************************************************************************************/

  public String getType(Object depositDocObj) {
    return this.depositDocAdapter.getType(depositDocObj);
  }

  public void setType(Object depositDocObj, String value) {
    this.depositDocAdapter.setType(depositDocObj, value);
  }

  public String getSubtype(Object depositDocObj) {
    return this.depositDocAdapter.getSubtype(depositDocObj);
  }

  public void setSubtype(Object depositDocObj, String value) {
    this.depositDocAdapter.setSubtype(depositDocObj, value);
  }

  public String getSubSubtype(Object depositDocObj) {
    return this.depositDocAdapter.getSubSubtype(depositDocObj);
  }

  public void setSubSubtype(Object depositDocObj, String value) {
    this.depositDocAdapter.setSubSubtype(depositDocObj, value);
  }

  public String getTitleContent(Object depositDocObj) {
    return this.depositDocAdapter.getTitleContent(depositDocObj);
  }

  public String getTitleLang(Object depositDocObj) {
    return this.depositDocAdapter.getTitleLang(depositDocObj);
  }

  public void setTitleLang(Object depositDocObj, String value) {
    this.depositDocAdapter.setTitleLang(depositDocObj, value);
  }

  public String getOriginalTitleContent(Object depositDocObj) {
    return this.depositDocAdapter.getOriginalTitleContent(depositDocObj);
  }

  public String getOriginalTitleLang(Object depositDocObj) {
    return this.depositDocAdapter.getOriginalTitleLang(depositDocObj);
  }

  public void setOriginalTitleLang(Object depositDocObj, String value) {
    this.depositDocAdapter.setOriginalTitleLang(depositDocObj, value);
  }

  public String getISBN(Object depositDocObj) {
    return this.depositDocAdapter.getISBN(depositDocObj);
  }

  public String getISSN(Object depositDocObj) {
    return this.depositDocAdapter.getISSN(depositDocObj);
  }

  public void setISSN(Object depositDocObj, String value) {
    this.depositDocAdapter.setISSN(depositDocObj, value);
  }

  public String getDOI(Object depositDocObj) {
    return this.depositDocAdapter.getDOI(depositDocObj);
  }

  public void setDOI(Object depositDocObj, String value) {
    this.depositDocAdapter.setDOI(depositDocObj, value);
  }

  public BigInteger getPmid(Object depositDocObj) {
    return this.depositDocAdapter.getPmid(depositDocObj);
  }

  public String getArxivId(Object depositDocObj) {
    return this.depositDocAdapter.getArxivId(depositDocObj);
  }

  public String getURN(Object depositDocObj) {
    return this.depositDocAdapter.getURN(depositDocObj);
  }

  public void setURN(Object depositDocObj, String value) {
    this.depositDocAdapter.setURN(depositDocObj, value);
  }

  public String getPmcId(Object depositDocObj) {
    return this.depositDocAdapter.getPmcId(depositDocObj);
  }

  public String getMmsid(Object depositDocObj) {
    return this.depositDocAdapter.getMmsid(depositDocObj);
  }

  public String getRepec(Object depositDocObj) {
    return this.depositDocAdapter.getRepec(depositDocObj);
  }

  public String getDblp(Object depositDocObj) {
    return this.depositDocAdapter.getDblp(depositDocObj);
  }

  public List<AbstractContributor> getContributors(Object depositDocObj) {
    return this.depositDocAdapter.getContributors(depositDocObj);
  }

  public Optional<ContributorDTO> getFirstAuthor(Object depositDocObj) {
    return this.depositDocAdapter.getContributors(depositDocObj).stream()
            .filter(c -> c instanceof ContributorDTO && ((ContributorDTO) c).getRole().equals(AouConstants.CONTRIBUTOR_ROLE_AUTHOR))
            .map(c -> (ContributorDTO) c)
            .findFirst();
  }

  public void setContributors(Object depositDocObj, List<AbstractContributor> abstractContributors) {
    this.depositDocAdapter.setContributors(depositDocObj, abstractContributors);
  }

  public List<ResearchGroup> getResearchGroups(Object depositDocObj) {
    return this.depositDocAdapter.getResearchGroups(depositDocObj);
  }

  public List<Structure> getStructures(Object depositDocObj) {
    return this.depositDocAdapter.getStructures(depositDocObj);
  }

  public List<MetadataDates> getDates(Object depositDocObj) {
    return this.depositDocAdapter.getDates(depositDocObj);
  }

  public MetadataDates getDateForGlobalYear(Object depositDocObj) {
    return this.depositDocAdapter.getDateForGlobalYear(depositDocObj);
  }

  public Integer getGlobalYear(Object depositDocObj) {
    MetadataDates globalYearDate = this.getDateForGlobalYear(depositDocObj);
    if (globalYearDate != null) {
      return CleanTool.extractYearFromDate(globalYearDate.getContent(), this.metadataDateFormat);
    }
    return null;
  }

  public Optional<MetadataDates> getDefenseDate(Object depositDocObj) {
    return this.depositDocAdapter.getDates(depositDocObj).stream()
            .filter(d -> d.getType().equals(AouConstants.DATE_TYPE_DEFENSE))
            .findFirst();
  }

  public List<MetadataTextLang> getAbstracts(Object depositDocObj) {
    return this.depositDocAdapter.getAbstracts(depositDocObj);
  }

  public void setAbstracts(Object depositDocObj, List<MetadataTextLang> abstracts) {
    this.depositDocAdapter.setAbstracts(depositDocObj, abstracts);
  }

  public List<MetadataLink> getLinks(Object depositDocObj) {
    return this.depositDocAdapter.getLinks(depositDocObj);
  }

  public void setLinks(Object depositDocObj, List<MetadataLink> links) {
    this.depositDocAdapter.setLinks(depositDocObj, links);
  }

  public List<MetadataCollection> getCollections(Object depositDocObj) {
    return this.depositDocAdapter.getCollections(depositDocObj);
  }

  public List<String> getLanguages(Object depositDocObj) {
    return this.depositDocAdapter.getLanguages(depositDocObj);
  }

  public void setLanguages(Object depositDocObj, List<String> languagesList) {
    this.depositDocAdapter.setLanguages(depositDocObj, languagesList);
  }

  public List<MetadataFunding> getFundings(Object depositDocObj) {
    return this.depositDocAdapter.getFundings(depositDocObj);
  }

  public List<String> getDatasets(Object depositDocObj) {
    return this.depositDocAdapter.getDatasets(depositDocObj);
  }

  public List<MetadataFile> getFiles(Object depositDocObj) {
    return this.depositDocAdapter.getFiles(depositDocObj);
  }

  public List<MetadataCorrection> getCorrections(Object depositDocObj) {
    return this.depositDocAdapter.getCorrections(depositDocObj);
  }

  public String getDoctorAddress(Object depositDocObj) {
    return this.depositDocAdapter.getDoctorAddress(depositDocObj);
  }

  public String getDoctorEmail(Object depositDocObj) {
    return this.depositDocAdapter.getDoctorEmail(depositDocObj);
  }

  public String getLocalNumber(Object depositDocObj) {
    return this.depositDocAdapter.getLocalNumber(depositDocObj);
  }

  public List<MetadataClassification> getClassifications(Object depositDocObj) {
    return this.depositDocAdapter.getClassifications(depositDocObj);
  }

  public String getPublisherVersionUrl(Object depositDocObj) {
    return this.depositDocAdapter.getPublisherVersionUrl(depositDocObj);
  }

  public String getNote(Object depositDocObj) {
    return this.depositDocAdapter.getNote(depositDocObj);
  }

  public String getPagingOther(Object depositDocObj) {
    return this.depositDocAdapter.getPagingOther(depositDocObj);
  }

  public String getPaging(Object depositDocObj) {
    return this.depositDocAdapter.getPaging(depositDocObj);
  }

  public List<String> getKeywords(Object depositDocObj) {
    return this.depositDocAdapter.getKeywords(depositDocObj);
  }

  public String getAward(Object depositDocObj) {
    return this.depositDocAdapter.getAward(depositDocObj);
  }

  public String getDiscipline(Object depositDocObj) {
    return this.depositDocAdapter.getDiscipline(depositDocObj);
  }

  public String getEdition(Object depositDocObj) {
    return this.depositDocAdapter.getEdition(depositDocObj);
  }

  public String getMandator(Object depositDocObj) {
    return this.depositDocAdapter.getMandator(depositDocObj);
  }

  public String getAouCollection(Object depositDocObj) {
    return this.depositDocAdapter.getAouCollection(depositDocObj);
  }

  public String getPublisherName(Object depositDocObj) {
    return this.depositDocAdapter.getPublisherName(depositDocObj);
  }

  public String getPublisherPlace(Object depositDocObj) {
    return this.depositDocAdapter.getPublisherPlace(depositDocObj);
  }

  public String getContainerTitle(Object depositDocObj) {
    return this.depositDocAdapter.getContainerTitle(depositDocObj);
  }

  public String getContainerVolume(Object depositDocObj) {
    return this.depositDocAdapter.getContainerVolume(depositDocObj);
  }

  public String getContainerIssue(Object depositDocObj) {
    return this.depositDocAdapter.getContainerIssue(depositDocObj);
  }

  public String getContainerSpecialIssue(Object depositDocObj) {
    return this.depositDocAdapter.getContainerSpecialIssue(depositDocObj);
  }

  public String getContainerEditor(Object depositDocObj) {
    return this.depositDocAdapter.getContainerEditor(depositDocObj);
  }

  public String getContainerConferencePlace(Object depositDocObj) {
    return this.depositDocAdapter.getContainerConferencePlace(depositDocObj);
  }

  public String getContainerConferenceDate(Object depositDocObj) {
    return this.depositDocAdapter.getContainerConferenceDate(depositDocObj);
  }

  public String getContainerConferenceSubtitle(Object depositDocObj) {
    return this.depositDocAdapter.getContainerConferenceSubtitle(depositDocObj);
  }

  public boolean isBeforeUnige(Object depositDocObject) {
    return this.depositDocAdapter.isBeforeUnige(depositDocObject);
  }

  public boolean isBefore2010orBeforeAuthorJoiningUnige(Object depositDocObject) {
    boolean isBeforeUnige = this.isBeforeUnige(depositDocObject);
    boolean isBefore2010 = this.getDates(depositDocObject).stream().anyMatch(d -> {
      try {
        if (CleanTool.extractYearFromDate(d.getContent(), this.metadataDateFormat) <= 2010) {
          return true;
        }
      } catch (NullPointerException ex) {
        log.debug("can't extract a year from a date: {}", ex.getCause());
      }
      return false;
    });
    return isBeforeUnige || isBefore2010;
  }
  /*******************************************************************************************/

  /**
   * Manage links between a publication and their structures according to the given list of academic structures: add structures that exist in
   * the academic structures list but not in the publication's structures list. Remove structures from the publication's structures that are not
   * in the academic structures list.
   *
   * @param publication
   * @param academicStructures
   */
  public void syncPublicationStructuresList(Publication publication, List<Structure> academicStructures) {
    List<PublicationStructure> structures = publication.getPublicationStructures();

    // Create a list of existing PublicationStructure objects to be able to reuse the same ones below (required for JPA / Hibernate)
    List<PublicationStructure> publicationStructuresPointers = structures.stream().toList();

    List<String> existingResIds;
    if (academicStructures.isEmpty()) {
      // if there is no structure in metadata, do not remove existing validation structure
      existingResIds = structures.stream().filter(pubStruct -> pubStruct.getLinkType() == PublicationStructure.LinkType.METADATA)
              .map(pubStruct -> pubStruct.getStructure().getResId()).toList();
    } else {
      // if there is one metadata structure, validation structure may be cleared as it is not useful anymore
      existingResIds = structures.stream().map(pubStruct -> pubStruct.getStructure().getResId()).toList();
    }

    List<String> resIdsToPersist = academicStructures.stream().map(ResourceNormalized::getResId).toList();
    List<String> resIdsToAdd = resIdsToPersist.stream().filter(element -> !existingResIds.contains(element)).toList();
    List<String> resIdsToRemove = existingResIds.stream().filter(element -> !resIdsToPersist.contains(element)).toList();
    List<PublicationStructure> publicationStructuresToRemove = structures.stream()
            .filter(pubStruct -> resIdsToRemove.contains(pubStruct.getStructure().getResId())).toList();

    // remove structures that are not present anymore
    structures.removeAll(publicationStructuresToRemove);

    // add structures not in list yet
    for (String resIdToAdd : resIdsToAdd) {
      // Add new Structure
      Structure structure = this.structureService.findById(resIdToAdd).orElse(null);
      if (structure != null) {
        PublicationStructure pStructure = new PublicationStructure();
        pStructure.setStructure(structure);
        pStructure.setPublication(publication);
        pStructure.setLinkType(PublicationStructure.LinkType.METADATA);

        structures.add(pStructure);
      } else {
        log.warn("No structure with resId '{}' found", resIdToAdd);
      }
    }

    // Manage specific case of an existing VALIDATION structure that is replaced by the same structure in METADATA
    // In this particular case, the logic above preserve the PublicationStructure in the structures list so its linkType must be updated
    for (PublicationStructure publicationStructure : structures) {
      if (publicationStructure.getLinkType() == PublicationStructure.LinkType.VALIDATION && resIdsToPersist.contains(
              publicationStructure.getStructure().getResId())) {
        publicationStructure.setLinkType(PublicationStructure.LinkType.METADATA);
        log.info("link type between structure '{}' and publication '{}' automatically updated to METADATA",
                publicationStructure.getStructure().getName(), publication.getResId());
      }
    }

    // If there isn't any structure anymore on the publication, it disappears from validator's list
    // --> in this case we take the first current user structure on which he has validation rights and we use it as VALIDATION structure
    if (structures.isEmpty() && publication.getStatus() == Publication.PublicationStatus.IN_VALIDATION
            && !this.addCurrentUserValidationStructure(publication, existingResIds, structures, publicationStructuresPointers)) {

      // We build the validation error here (instead of MetadataValidatorService) because validation has already been done at this step
      final BindingResult errors = new BeanPropertyBindingResult(publication, publication.getClass().getName());
      errors.addError(new FieldError(publication.getClass().getSimpleName(), "structure",
              this.messageService.get(ERROR_VALIDATION_STRUCTURE_NOT_AUTOMATICALLY_SET)));
      throw new SolidifyValidationException(new ValidationError(errors));
    }
  }

  private boolean addCurrentUserValidationStructure(Publication publication, List<String> existingResIdsBeforeUpdate,
          List<PublicationStructure> structuresThatWillBeStored, List<PublicationStructure> publicationStructuresPointers) {
    if (this.transformOneMetadataStructureIntoValidationStructure(publication, existingResIdsBeforeUpdate, structuresThatWillBeStored,
            publicationStructuresPointers)) {
      return true;
    } else {
      return this.addCurrentUserFirstValidationStructureForSubtype(publication, structuresThatWillBeStored);
    }
  }

  /**
   * Check if one the structures before update matches the validation structures of the current user to keep it specifically instead of
   * arbitrary selecting the first one.
   *
   * @param publication
   * @param existingResIdsBeforeUpdate
   * @param structuresThatWillBeStored
   * @param publicationStructuresPointers JPA / Hibernate raises an Exception if we try to save a Publication with a PublicationStructure
   *                                      that has the same key but is not the same object. This list allows to find the same object.
   * @return
   */
  private boolean transformOneMetadataStructureIntoValidationStructure(Publication publication, List<String> existingResIdsBeforeUpdate,
          List<PublicationStructure> structuresThatWillBeStored, List<PublicationStructure> publicationStructuresPointers) {
    // Get validation structures for the current user
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String personId = this.personService.getLinkedPersonId(authentication);

    Map<String, List<String>> fullStructuresValidationRights = this.personService.getValidablePublicationSubtypesByStructure(personId);
    for (String structureId : existingResIdsBeforeUpdate) {
      if (fullStructuresValidationRights.containsKey(structureId)) {
        if (fullStructuresValidationRights.get(structureId).contains(publication.getSubtype().getResId())) {

          // Find the same instance of PublicationStructure that was present beforeUpdate and make it a VALIDATION structure
          for (PublicationStructure existingPublicationStructure : publicationStructuresPointers) {
            if (existingPublicationStructure.getStructure().getResId().equals(structureId) && existingPublicationStructure.getPublication()
                    .getResId().equals(publication.getResId())) {
              existingPublicationStructure.setLinkType(PublicationStructure.LinkType.VALIDATION);
              structuresThatWillBeStored.add(existingPublicationStructure);
              log.info("metadata structure '{}' automatically updated to validation structure a on publication '{}'",
                      existingPublicationStructure.getStructure().getName(), publication.getResId());
              return true;
            }
          }
        }
      }
    }
    return false;
  }

  private boolean addCurrentUserFirstValidationStructureForSubtype(Publication publication,
          List<PublicationStructure> structuresThatWillBeStored) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String personId = this.personService.getLinkedPersonId(authentication);
    List<StructureValidationRight> structureValidationRights = this.personService.getStructureValidationRights(personId);
    for (StructureValidationRight structureValidationRight : structureValidationRights) {
      if (this.addStructureIfUserCanValidatePublicationSubtype(publication, structureValidationRight, structuresThatWillBeStored)) {
        return true;
      }
    }

    log.warn("Unable to automatically add a validation structure on publication '{}'", publication.getResId());
    return false;
  }

  private boolean addStructureIfUserCanValidatePublicationSubtype(Publication publication, StructureValidationRight structureValidationRight,
          List<PublicationStructure> structuresThatWillBeStored) {
    List<String> subtypesCurrentUserCanValidate = structureValidationRight.getPublicationSubtypes()
            .stream()
            .map(PublicationSubtype::getResId)
            .toList();
    if (subtypesCurrentUserCanValidate.contains(publication.getSubtype().getResId())) {
      Structure structure = this.structureService.findById(structureValidationRight.getResId()).orElse(null);
      if (structure != null) {
        PublicationStructure pStructure = new PublicationStructure();
        pStructure.setStructure(structure);
        pStructure.setPublication(publication);
        pStructure.setLinkType(PublicationStructure.LinkType.VALIDATION);
        pStructure.getCreation().setWhen(OffsetDateTime.now());
        structuresThatWillBeStored.add(pStructure);
        log.info("validation structure '{}' automatically added on publication '{}'", structure.getName(), publication.getResId());
        return true;
      }
    }
    return false;
  }

  /**
   * Manage links between a publication and their research groups according to the given list of groups: add research groups that exist in the
   * groups list but not in the publication's research groups list. Remove research groups from the publication's research groups that are not
   * in the groups list.
   *
   * @param publication
   * @param groups
   */
  private void syncResearchGroupsList(Publication publication, List<ResearchGroup> groups) {
    Set<ResearchGroup> researchGroups = publication.getResearchGroups();

    List<String> existingResIds = researchGroups.stream().map(ResourceNormalized::getResId).toList();
    List<String> resIdsToPersist = groups.stream().map(ResourceNormalized::getResId).toList();
    List<String> resIdsToAdd = resIdsToPersist.stream().filter(element -> !existingResIds.contains(element)).toList();
    List<String> resIdsToRemove = existingResIds.stream().filter(element -> !resIdsToPersist.contains(element)).toList();
    List<ResearchGroup> researchGroupsToRemove = researchGroups.stream().filter(group -> resIdsToRemove.contains(group.getResId())).toList();

    // remove research groups that are not present anymore
    researchGroupsToRemove.forEach(researchGroups::remove);

    // add research group not in list yet
    for (String resId : resIdsToAdd) {
      // Add new research group
      ResearchGroup researchGroup = this.researchGroupService.findById(resId).orElse(null);
      if (researchGroup != null) {
        researchGroups.add(researchGroup);
      } else {
        log.warn("Unable to synchronize research group with resId {}", resId);
      }
    }
  }

  private void syncContributorsAndPublicationContributorOtherNameList(Publication publication, List<AbstractContributor> abstractContributors) {
    Set<Contributor> currentContributors = publication.getContributors();
    Set<ContributorOtherName> contributorOtherNames = publication.getContributorOtherNames();

    // remove all contributors from list
    currentContributors.clear();

    // remove all contributors other names from list
    contributorOtherNames.clear();

    for (AbstractContributor abstractContributor : abstractContributors) {
      if (abstractContributor instanceof ContributorDTO contributorDTO) {
        // find or create contributor posted in metadata
        Contributor contributor = this.contributorService.findOrCreate(contributorDTO);
        currentContributors.add(contributor);

        if (!StringTool.isNullOrEmpty(contributor.getCnIndividu()) &&
                (!Objects.equals(contributor.getFirstName(), contributorDTO.getFirstName())
                        || !Objects.equals(contributor.getLastName(), contributorDTO.getLastName()))) {
          // Form of the UNIGE contributor name is one of its other names --> link publication with it
          Optional<ContributorOtherName> contributorOtherNameOpt = contributor.getOtherNames().stream()
                  .filter(con -> contributorDTO.getLastName().equals(con.getLastName())
                          && Objects.equals(contributorDTO.getFirstName(), con.getFirstName())).findFirst();
          if (contributorOtherNameOpt.isPresent()) {
            if (!contributorOtherNames.contains(contributorOtherNameOpt.get())) {
              contributorOtherNames.add(contributorOtherNameOpt.get());
            }
          } else {
            throw new SolidifyRuntimeException("ContributorOtherName not found");
          }
        }
      }
    }
  }

  protected String updateDateFromFrontendToMetadataFormat(String dateStr) {
    return this.updateDateFormat(dateStr, this.frontendEnterDateFormat, this.metadataOutputDateFormat);
  }

  protected String updateDateFromMetadataToFrontendFormat(String dateStr) {
    return this.updateDateFormat(dateStr, this.metadataOutputDateFormat, this.frontendOutputDateFormat);
  }

  /**
   * Update a date from a format to another. The first source format is going to be checked with the first output format,
   * the second source Format with the second output format and so on, to avoid to check format that does not contain
   * the same info (i.e MM-yyyy with yyyy-MM-dd). They are both defined as the same order for an specific type of format.
   *
   * @param dateStr
   * @param sourceFormat
   * @param outputFormat
   * @return
   */
  private String updateDateFormat(String dateStr, String[] sourceFormat, String[] outputFormat) {
    int counter = 0;
    for (String source : sourceFormat) {
      try {
        DateTimeFormatter formatterFront = new DateTimeFormatterBuilder()
                .appendPattern(source)
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                .toFormatter();
        LocalDate localDate = LocalDate.parse(dateStr, formatterFront);
        DateTimeFormatter formatterMetadata = DateTimeFormatter.ofPattern(outputFormat[counter]);
        return localDate.format(formatterMetadata);
      } catch (DateTimeParseException e) {
        counter++;
      }
    }
    // return an incorrect date if it cannot be transformed, since returning the date as it is could have problems later on when validating
    // since it could be in a correct format for the backend but not for the frontend. (i.e date 2021-05 not possible to transform here but it's
    // valid date for metadata
    return MetadataExtractor.INCORRECT_DATE;
  }

}

