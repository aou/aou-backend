/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationFedoraProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.PublicationFromFedoraMessage;
import ch.unige.aou.model.fedora.AdminDatastreamDto;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.xml.fedora.foxml.v1.DigitalObject;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.metadata.imports.FedoraImportService;

@Profile("storage-fedora3")
@Service
@ConditionalOnBean(AdminController.class)
public class PublicationFedoraProcessingService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(PublicationFedoraProcessingService.class);

  private String logFolder;

  private final FedoraImportService fedoraImportService;
  private final MetadataService metadataService;

  public PublicationFedoraProcessingService(MessageService messageService, FedoraImportService fedoraImportService,
          MetadataService metadataService, AouProperties aouProperties) {
    super(messageService);
    this.fedoraImportService = fedoraImportService;
    this.metadataService = metadataService;

    this.logFolder = aouProperties.getLogsFolder();
  }

  public void processMessage(PublicationFromFedoraMessage publicationsFromFedoraMessage) {

    String pid = publicationsFromFedoraMessage.getPid();

    log.info("=== Start importing object '{}' from Fedora ====", pid);

    try {

      DigitalObject foxml = this.fedoraImportService.getFoxml(pid);

      //Get DepositDoc according to pid from the message
      String xmlMetadata = this.fedoraImportService.getDepositDocFromFedora(foxml);
      log.debug("xmlMetadata fetched for object '{}'", pid);

      AdminDatastreamDto adminDS = this.fedoraImportService.getAdminDatastreamDto(foxml);
      log.debug("adminDS fetched for object '{}'", pid);

      Optional<Person> creatorOpt = this.fedoraImportService.getExistingCreator(publicationsFromFedoraMessage, adminDS);
      log.debug("existing creator fetched for object '{}'", pid);

      Person creator;
      if (!creatorOpt.isPresent()) {
        // Ensure deposit creator exists in database (thread safe)
        creator = this.fedoraImportService.getOrCreateCreatorPerson(publicationsFromFedoraMessage, adminDS);
        log.info("new Person {} created", creator.getFullName());
      } else {
        creator = creatorOpt.get();
      }

      //Ensure all contributors exists in db (thread safe)
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
      log.debug("metadataExtractor found for object '{}'", pid);

      List<AbstractContributor> contributors = metadataExtractor.getContributors(metadataExtractor.createDepositDocObjectFromXml(xmlMetadata));
      log.debug("AbstractContributors found for object '{}'", pid);

      for (AbstractContributor contributor : contributors) {
        if (contributor instanceof ContributorDTO) {
          ContributorDTO contributorDTO = (ContributorDTO) contributor;
          if ((!StringTool.isNullOrEmpty(contributorDTO.getCnIndividu())
                  && this.fedoraImportService.getExistingContributorByCnIndividu(contributorDTO) == null)
                  ||
                  (StringTool.isNullOrEmpty(contributorDTO.getCnIndividu())
                          && this.fedoraImportService.getExistingContributorByFirstNameAndLastNameAndCnIndividuIsNull(contributorDTO) == null)) {
            this.fedoraImportService.getOrCreateContributor(contributorDTO);
            log.info("new Contributor {} created", contributorDTO);
          } else {
            log.debug("No need to create new Contributor '{}'", contributorDTO);
          }
        }
      }

      // Import Object as a new Publication
      Publication publication = this.fedoraImportService.importObjectFromFedora(foxml, xmlMetadata, creator, adminDS);

      // Manually manage first completed date in status history
      // Note: seems to be necessary now while it was not during migration in production (regression ?)
      this.fedoraImportService.updateFirstCompletedDateInStatusHistory(publication);

      this.logFedoraImportMessage(pid, "object successfully imported from Fedora (" + publicationsFromFedoraMessage.getLogMessage() + ")");

      log.debug("object {} successfully imported from Fedora", pid);
    } catch (Exception e) {
      log.error("An error occurred while trying to import document {} from Fedora", pid, e);
      this.logFedoraImportMessage(pid, e);

      // rethrow Exception to rollback the import thanks to @Transactional annotation
      throw new SolidifyRuntimeException("An error occurred while trying to import document " + pid + " from Fedora", e);
    }
  }

  private void logFedoraImportMessage(String pid, Object obj) {
    if (obj != null) {
      try {
        Path logFile = Paths.get(this.logFolder).resolve("fedora-import-error.log");
        if (!FileTool.isFile(logFile)) {
          Files.createFile(logFile);
        }

        if (obj instanceof Exception) {
          Exception ex = (Exception) obj;
          StringWriter sw = new StringWriter();
          PrintWriter pw = new PrintWriter(sw);
          ex.printStackTrace(pw);
          String stackTrace = sw.toString();

          StringBuilder errorMessageSB = new StringBuilder("Exception: " + ex.getMessage());
          if (ex.getCause() != null) {
            errorMessageSB.append(" | " + ex.getCause().getMessage());

            if (ex.getCause().getCause() != null) {
              errorMessageSB.append(" | " + ex.getCause().getCause().getMessage());
            }
          }

          String message = errorMessageSB.append("\n\n").append(stackTrace).toString();

          Files.write(logFile, this.getLogLine(pid, message), StandardOpenOption.APPEND);
        } else {
          Files.write(logFile, this.getLogLine(pid, obj.toString()), StandardOpenOption.APPEND);
        }
      } catch (IOException e) {
        log.error("unable to write to fedora-import-error.log file", e);
      }
    }
  }

  private byte[] getLogLine(String pid, String lineContent) {
    LocalDateTime now = LocalDateTime.now();
    String line = now + "\t" + pid + "\t" + lineContent + "\n";
    return line.getBytes();
  }
}
