/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - Fedora3Client.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.storage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouXmlNamespacePrefixMapper;
import ch.unige.aou.model.xml.fedora.foxml.v1.DigitalObject;
import ch.unige.aou.model.xml.fedora.foxml.v1.ObjectPropertiesType;
import ch.unige.aou.model.xml.fedora.foxml.v1.PropertyType;
import ch.unige.aou.model.xml.fedora.foxml.v1.StateType;
import ch.unige.aou.model.xml.fedora.repository.DatastreamProfile;
import ch.unige.aou.model.xml.fedora.repository.FedoraRepository;
import ch.unige.aou.model.xml.fedora.repository.PidList;

public class Fedora3Client {

  private static final String FEDORA_OBJETS = "/objects/";
  private static final String FEDORA_DATASTREAM = "/datastreams/";
  private static final String FEDORA_CONTENT = "/content";
  private static final int FEDORA_MAX_LABEL_SIZE = 255;

  private final RestTemplate restTemplate;
  private final JAXBContext jaxbContext;
  private final AouXmlNamespacePrefixMapper aouXmlNamespacePrefixMapper;

  public Fedora3Client(String fedoraUrl, String fedoraUser, String fedoraPassword, AouXmlNamespacePrefixMapper aouXmlNamespacePrefixMapper) {
    this.restTemplate = new RestTemplateBuilder().rootUri(fedoraUrl).basicAuthentication(fedoraUser, fedoraPassword).build();
    // read datastreams IN UTF-8 by default
    this.restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

    this.aouXmlNamespacePrefixMapper = aouXmlNamespacePrefixMapper;
    try {
      this.jaxbContext = JAXBContext.newInstance(DigitalObject.class, FedoraRepository.class, PidList.class, DatastreamProfile.class);
    } catch (JAXBException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  /**
   * Server Info
   *
   * @return repositionInfo
   * @throws JAXBException
   */
  public FedoraRepository getServerInfo() throws JAXBException {
    return (FedoraRepository) this.getObjectXmlString(this.restTemplate.getForObject("/describe?xml=true", String.class));
  }

  /**
   * Next available PID
   *
   * @param fedoraPrefix
   * @return nextPid
   * @throws JAXBException
   */
  public String getNextPID(String fedoraPrefix) throws JAXBException {
    PidList pidList = (PidList) this.getObjectXmlString(
            this.restTemplate.postForObject(FEDORA_OBJETS + "nextPID?format=xml&namespace=" + fedoraPrefix, null, String.class));
    return pidList.getPid().get(0);
  }

  /**
   * Check if document exists
   *
   * @param pid
   * @return httpStatus
   */
  public HttpStatus check(String pid) {
    try {
      this.restTemplate.exchange(FEDORA_OBJETS + pid, HttpMethod.HEAD, null, String.class);
    } catch (final HttpClientErrorException e) {
      return (HttpStatus) e.getStatusCode();
    }
    return HttpStatus.OK;
  }

  /**
   * Check if datastream exists
   *
   * @param pid
   * @param dsId
   * @return httpStatus
   */
  public HttpStatus checkDatastream(String pid, String dsId) {
    try {
      this.restTemplate.exchange(FEDORA_OBJETS + pid + FEDORA_DATASTREAM + dsId, HttpMethod.HEAD, null, String.class);
    } catch (final HttpClientErrorException e) {
      return (HttpStatus) e.getStatusCode();
    }
    return HttpStatus.OK;
  }

  /**
   * Get document
   *
   * @param pid
   * @return foxml document
   * @throws JAXBException
   */
  public DigitalObject getFoxml(String pid) {
    try {
      return (DigitalObject) this
              .getObjectXmlString(this.restTemplate.getForObject(FEDORA_OBJETS + pid + "/objectXML", String.class));
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        throw new SolidifyResourceNotFoundException(pid);
      }
      throw new SolidifyRestException("Cannot get Fedora document " + pid, e);
    } catch (Exception e) {
      throw new SolidifyRuntimeException("Error in getting Fedora document " + pid, e);
    }
  }

  /**
   * Ingest a new document
   *
   * @param pid
   * @param label
   * @return pid
   * @throws JAXBException
   */
  public String ingest(String pid, String label) throws JAXBException {
    DigitalObject foxml = this.getFoxml(pid, label);
    String foxmlSting = this.convertFoxml(foxml);
    // Header
    final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML_VALUE + ";charset=" + StandardCharsets.UTF_8.name());
    final HttpEntity<String> httpEntity = new HttpEntity<>(foxmlSting, headers);
    try {
      ResponseEntity<String> response = this.restTemplate.exchange(FEDORA_OBJETS + pid, HttpMethod.POST, httpEntity, String.class);
      return response.getBody();
    } catch (final HttpClientErrorException e) {
      throw new SolidifyRestException("Ingest error", e);
    }
  }

  /**
   * Delete XML content, ControlGroup=X
   *
   * @param pid
   * @param dsId
   * @param dsLabel
   * @return datastream info
   */
  public void deleteManagedDataStream(String pid, String dsId, String dsLabel) {
    this.deleteDatastream(pid, dsId, "X", dsLabel);
  }

  /**
   * Add XML content, ControlGroup=X
   *
   * @param pid
   * @param dsId
   * @param dsLabel
   * @param dsContent
   * @return datastream info
   */
  public DatastreamProfile addXmlDataStream(String pid, String dsId, String dsLabel, URI dsContent) {
    return this.addDatastream(pid, dsId, "X", dsLabel, new FileSystemResource(Paths.get(dsContent)));
  }

  /**
   * Add managed content (~binary), ControlGroup=M
   *
   * @param pid
   * @param dsId
   * @param dsLabel
   * @param dsContent
   * @return datastream info
   */
  public DatastreamProfile addManagedDataStream(String pid, String dsId, String dsLabel, URI dsContent) {
    return this.addDatastream(pid, dsId, "M", dsLabel, new FileSystemResource(Paths.get(dsContent)));
  }

  /**
   * Add managed content (~binary), ControlGroup=M
   *
   * @param pid
   * @param dsId
   * @param dsLabel
   * @param dsContent
   * @return datastream info
   */
  public DatastreamProfile addManagedDataStream(String pid, String dsId, String dsLabel, ByteArrayResource dsContent) {
    return this.addDatastream(pid, dsId, "M", dsLabel, dsContent);
  }

  /**
   * Update XML content, ControlGroup=X
   *
   * @param pid
   * @param dsId
   * @param dsLabel
   * @param path
   * @return datastream info
   */
  public DatastreamProfile updateXmlDataStream(String pid, String dsId, String dsLabel, Path path) {
    try {
      String content = Files.readString(path);
      return this.updateDatastream(pid, dsId, "X", dsLabel, MediaType.TEXT_XML_VALUE, content);
    } catch (IOException e) {
      throw new SolidifyRestException(e.getMessage(), e);
    }
  }

  /**
   * Get datastream
   *
   * @param pid
   * @param dsId
   * @return datastream info
   */
  public DatastreamProfile getDataStreamProfile(String pid, String dsId) {
    try {
      return (DatastreamProfile) this
              .getObjectXmlString(this.restTemplate.getForObject(FEDORA_OBJETS + pid + FEDORA_DATASTREAM + dsId + "?format=xml", String.class));
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        throw new SolidifyResourceNotFoundException(pid);
      }
      throw new SolidifyRestException("Cannot get Fedora datastream " + pid + "-" + dsId, e);
    } catch (Exception e) {
      throw new SolidifyRuntimeException("Error in getting Fedora datastream " + pid + "-" + dsId, e);
    }
  }

  public String getDataStreamContent(String pid, String dsId) {
    try {
      return this.restTemplate.getForObject(FEDORA_OBJETS + pid + FEDORA_DATASTREAM + dsId + FEDORA_CONTENT, String.class);
    } catch (HttpClientErrorException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        throw new SolidifyResourceNotFoundException(pid);
      }
      throw new SolidifyRestException("Cannot get Fedora datastream content " + pid + "-" + dsId, e);
    } catch (Exception e) {
      throw new SolidifyRuntimeException("Error in getting Fedora datastream content " + pid + "-" + dsId, e);
    }
  }

  public Document getDataStreamContentAsXml(String pid, String dsId) {
    try {
      String xmlContent = this.getDataStreamContent(pid, dsId);
      return XMLTool.parseXML(xmlContent);
    } catch (ParserConfigurationException | SAXException | IOException e) {
      throw new SolidifyRuntimeException("Error in getting Fedora datastream content as XML " + pid + "-" + dsId, e);
    }
  }

  // *********************
  // ** Private methods **
  // *********************

  /**
   * Add a datastream
   *
   * @param pid
   * @param dsId
   * @param dsGroup
   * @param dsLabel
   * @param dsContent
   * @return datastream info
   */
  private DatastreamProfile addDatastream(String pid, String dsId, String dsGroup, String dsLabel, Object dsContent) {
    // Url parameters
    String url = this.getDatastreamUrl(pid, dsId, dsGroup, dsLabel);
    // Header
    final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    headers.add(HttpHeaders.CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA.toString());
    // Body
    MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
    body.add("file", dsContent);
    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
    try {
      final ResponseEntity<String> responseEntity = this.restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
      return (DatastreamProfile) this.getObjectXmlString(responseEntity.getBody());
    } catch (final Exception e) {
      throw new SolidifyRestException(e.getMessage(), e);
    }
  }

  /**
   * Update a datastream
   *
   * @param pid
   * @param dsId
   * @param dsGroup
   * @param dsLabel
   * @param content
   * @return datastream info
   */
  private DatastreamProfile updateDatastream(String pid, String dsId, String dsGroup, String dsLabel, String mimeType, String content) {
    // Url parameters
    String url = this.getDatastreamUrl(pid, dsId, dsGroup, dsLabel);
    // Header
    final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
    headers.add(HttpHeaders.CONTENT_TYPE, mimeType);
    try {
      final HttpEntity<String> requestEntity = new HttpEntity<>(content, headers);
      final ResponseEntity<String> responseEntity = this.restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
      return (DatastreamProfile) this.getObjectXmlString(responseEntity.getBody());
    } catch (final Exception e) {
      throw new SolidifyRestException(e.getMessage(), e);
    }
  }

  /**
   * Delete a datastream
   *
   * @param pid
   * @param dsId
   * @param dsGroup
   * @param dsLabel
   * @return datastream info
   */
  private void deleteDatastream(String pid, String dsId, String dsGroup, String dsLabel) {
    // Url parameters
    String url = this.getDatastreamUrl(pid, dsId, dsGroup, dsLabel);

    try {
      this.restTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<>(pid), String.class);
    } catch (final Exception e) {
      throw new SolidifyRestException(e.getMessage(), e);
    }
  }

  private String getDatastreamUrl(String pid, String dsId, String dsGroup, String dsLabel) {
    dsLabel = StringTool.truncateOnSpaceWithEllipsis(dsLabel, FEDORA_MAX_LABEL_SIZE);
    dsLabel = dsLabel.replaceAll("\\{", "_");
    dsLabel = dsLabel.replaceAll("}", "_");

    StringBuilder urlBuilder = new StringBuilder();
    urlBuilder.append(FEDORA_OBJETS).append(pid).append(FEDORA_DATASTREAM).append(dsId);
    urlBuilder.append("?controlGroup=").append(dsGroup);
    urlBuilder.append("&dsLabel=").append(dsLabel);
    return urlBuilder.toString();
  }

  public File getAttachment(String pid, String dsId, Path path) {
    String url = FEDORA_OBJETS + pid + FEDORA_DATASTREAM + dsId + FEDORA_CONTENT;
    try {
      return this.restTemplate.execute(url, HttpMethod.GET, null, clientHttpResponse -> {
        File ret = new File(String.valueOf(path));
        StreamUtils.copy(clientHttpResponse.getBody(), new FileOutputStream(ret));
        return ret;
      });
    } catch (final Exception e) {
      throw new SolidifyRestException(e.getMessage(), e);
    }
  }

  private String convertFoxml(DigitalObject foxml) throws JAXBException {
    StringWriter sw = new StringWriter();
    Marshaller marshaller = this.getJaxbMarshaller();
    marshaller.marshal(foxml, sw);
    return sw.toString();
  }

  private Marshaller getJaxbMarshaller() throws JAXBException {
    Marshaller marshaller = this.jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.setProperty(AouConstants.XML_NAMESPACE_PREFIX_MAPPER, this.aouXmlNamespacePrefixMapper);
    return marshaller;
  }

  private Object getObjectXmlString(String xmlString) throws JAXBException {
    final StringReader newMd = new StringReader(xmlString);
    return this.jaxbContext.createUnmarshaller().unmarshal(newMd);
  }

  private DigitalObject getFoxml(String pid, String label) {
    label = StringTool.truncateOnSpaceWithEllipsis(label, FEDORA_MAX_LABEL_SIZE);
    DigitalObject foxml = new DigitalObject();
    foxml.setVERSION("1.1");
    // Add PID
    foxml.setPID(pid);
    // Object properties
    ObjectPropertiesType properties = new ObjectPropertiesType();
    foxml.setObjectProperties(properties);
    // Add 'State' property
    PropertyType stateProperty = new PropertyType();
    stateProperty.setNAME("info:fedora/fedora-system:def/model#state");
    stateProperty.setVALUE(StateType.A.toString());
    foxml.getObjectProperties().getProperty().add(stateProperty);
    // Add 'Label' property
    PropertyType labelProperty = new PropertyType();
    labelProperty.setNAME("info:fedora/fedora-system:def/model#label");
    labelProperty.setVALUE(label);
    foxml.getObjectProperties().getProperty().add(labelProperty);
    return foxml;
  }

}
