/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - UserSynchronizationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.auth.service.UserInfoLocalService;
import ch.unige.solidify.service.AbstractUserSynchronizationService;
import ch.unige.solidify.service.TrustedRestClientService;
import ch.unige.solidify.util.TrustedRestClientTool;

import ch.unige.aou.controller.AdminController;

@Service
@ConditionalOnBean(AdminController.class)
public class UserSynchronizationService extends AbstractUserSynchronizationService {
  public UserSynchronizationService(UserInfoLocalService userInfoLocalService, TrustedRestClientTool trustedRestClientTool,
          TrustedRestClientService trustedRestClientService, AuthorizationClientProperties authProperties) {
    super(userInfoLocalService, trustedRestClientTool, trustedRestClientService, authProperties.getAuthorizationServerUrl());
  }

}
