/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataValidatorV2.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.service.rest.UnigePersonSearchService;

@Service
@ConditionalOnBean(AdminController.class)
public class MetadataValidatorV2 extends MetadataValidatorService {

  public MetadataValidatorV2(AouProperties aouProperties, MetadataExtractorV2 metadataExtractor, MessageService messageService,
          PublicationTypeService publicationTypeService, PublicationSubtypeService publicationSubtypeService,
          PublicationSubSubtypeService publicationSubSubtypeService, ResearchGroupService researchGroupService,
          StructureService structureService, DuplicateService duplicateService, UnigePersonSearchService unigePersonSearchService,
          DocumentFileService documentFileService, HistoryService historyService) {
    super(aouProperties, metadataExtractor, messageService, publicationTypeService, publicationSubtypeService, publicationSubSubtypeService,
            researchGroupService, structureService, duplicateService, unigePersonSearchService, documentFileService, historyService);
  }

  @Override
  protected void validateFormDescriptionStep(Publication publication, Object depositDocObj, BindingResult errors) {
    super.validateFormDescriptionStep(publication, depositDocObj, errors);

    this.validateLanguages(publication, depositDocObj, errors);
    this.validateISSN(publication, depositDocObj, errors);
    this.validateDOI(publication, depositDocObj, errors);
    this.validatePMID(publication, depositDocObj, errors);
  }

  protected void validateLanguages(Publication publication, Object depositDocObj, BindingResult errors) {

    List<String> languagesList = this.metadataExtractor.getLanguages(depositDocObj);

    String langFieldName = FORM_STEP_DESCRIPTION_PREFIX + "languages";

    if (languagesList == null || languagesList.isEmpty()) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), langFieldName, this.messageService.get(ERROR_LANGUAGE_MANDATORY)));
    } else {
      List<String> foundLanguages = new ArrayList<>();
      for (String lang : languagesList) {
        if (foundLanguages.contains(lang)) {
          errors.addError(new FieldError(publication.getClass().getSimpleName(), langFieldName,
                  this.messageService.get(ERROR_LANGUAGE_APPEARS_MORE_THAN_ONCE)));
        } else {
          foundLanguages.add(lang);
        }
      }
    }
  }

  protected void validateISSN(Publication publication, Object depositDocObj, BindingResult errors) {
    String issn = this.metadataExtractor.getISSN(depositDocObj);
    if (!StringTool.isNullOrEmpty(issn) && !ValidationTool.isValidISSN(issn)) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.issn",
              this.messageService.get(ERROR_INVALID_ISSN, new Object[] { issn })));
    }
  }

  protected void validateDOI(Publication publication, Object depositDocObj, BindingResult errors) {
    String doi = this.metadataExtractor.getDOI(depositDocObj);
    if (!StringTool.isNullOrEmpty(doi)) {
      if (!ValidationTool.isValidDOI(doi)) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.doi",
                this.messageService.get(ERROR_INVALID_DOI, new Object[] { doi })));
      }
      if (this.checkForDuplicates && this.duplicateService.doiAlreadyUsedByAnotherPublication(doi, publication.getResId())) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.doi",
                this.messageService.get(ERROR_DOI_ALREADY_EXISTS, new Object[] { doi })));
      }
    }
  }

  protected void validatePMID(Publication publication, Object depositDocObj, BindingResult errors) {
    BigInteger pmid = this.metadataExtractor.getPmid(depositDocObj);
    if (pmid != null) {
      if (!ValidationTool.isValidPMID(String.valueOf(pmid))) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.pmid",
                this.messageService.get(ERROR_INVALID_PMID, new Object[] { pmid })));
      }
      if (this.checkForDuplicates && this.duplicateService.pmidAlreadyUsedByAnotherPublication(String.valueOf(pmid), publication.getResId())) {
        errors.addError(new FieldError(publication.getClass().getSimpleName(), FORM_STEP_DESCRIPTION_PREFIX + "identifiers.pmid",
                this.messageService.get(ERROR_PMID_ALREADY_EXISTS, new Object[] { pmid })));
      }
    }
  }
}
