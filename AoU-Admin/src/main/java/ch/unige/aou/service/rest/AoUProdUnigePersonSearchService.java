/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - AoUProdUnigePersonSearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.rest.UnigePersonDTO;

@Service
@Profile("person-search-aou-prod")
@ConditionalOnBean(AdminController.class)
public class AoUProdUnigePersonSearchService extends UnigePersonSearchService {

  private static final int PAGE_SIZE = 10;

  private final String apiUrl;

  private final String apiKey;

  public AoUProdUnigePersonSearchService(AouProperties aouProperties) {
    this.apiUrl = aouProperties.getMetadata().getSearch().getPerson().getUrl();
    this.apiKey = aouProperties.getMetadata().getSearch().getPerson().getApiKey();
  }

  @Override
  protected RestCollection<UnigePersonDTO> getResultsFromExternalService(String firstname, String lastname, Pageable pageable) {
    if (!StringTool.isNullOrEmpty(this.apiUrl) && !StringTool.isNullOrEmpty(this.apiKey)) {

      try {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

        final String jsonString = client.postForObject(this.apiUrl, this.getRequest(firstname, lastname), String.class);

        final ObjectMapper mapper = new ObjectMapper();
        final JavaType listOfMaps = mapper.getTypeFactory().constructParametricType(List.class, Map.class);
        List<Map> results = mapper.readValue(jsonString, listOfMaps);

        List<UnigePersonDTO> resultList = new ArrayList<>();

        results.forEach(map -> {
          String cnIndividu = this.getStringOrNull(map, "cn");
          String mapFirstname = this.getStringOrNull(map, "firstname");
          String mapLastname = this.getStringOrNull(map, "lastname");
          String yearOfBirthStr = this.getStringOrNull(map, "birthday");
          Integer yearOfBirth = null;
          if (!StringTool.isNullOrEmpty(yearOfBirthStr)) {
            yearOfBirth = Integer.parseInt(yearOfBirthStr);
          }
          String function = this.getStringOrNull(map, "fonction");
          String structure = this.getStringOrNull(map, "structure");

          UnigePersonDTO unigePersonDTO = new UnigePersonDTO(cnIndividu, mapFirstname, mapLastname, yearOfBirth, function, structure, null);
          resultList.add(unigePersonDTO);
        });
        RestCollectionPage collectionPage = new RestCollectionPage(0, PAGE_SIZE, 1, resultList.size());
        return new RestCollection<>(resultList, collectionPage);
      } catch (final IOException e) {
        throw new SolidifyRuntimeException(e.getMessage());
      }
    } else {
      throw new SolidifyRuntimeException("incomplete configuration");
    }
  }

  @Override
  protected Optional<UnigePersonDTO> getResultsFromExternalService(String cnIndividu) {
    // Not implemented because this is not used in Prod and will be removed
    return Optional.empty();
  }

  private HttpEntity<MultiValueMap<String, String>> getRequest(String firstname, String lastname) {
    MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
    parameters.add("firstname", firstname);
    parameters.add("lastname", lastname);

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    headers.set("X-API-KEY", this.apiKey);

    return new HttpEntity<>(parameters, headers);
  }

  private String getStringOrNull(Map map, String propertyName) {
    String value = map.getOrDefault(propertyName, "").toString();
    if (StringTool.isNullOrEmpty(value)) {
      return null;
    } else {
      return value;
    }
  }
}
