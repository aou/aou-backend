/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ContactService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.business.PublicationService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouMetadataValidationException;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.model.contact.ContactableContributor;
import ch.unige.aou.model.contact.PublicationContactMessage;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.rest.UnigePersonDTO;
import ch.unige.aou.service.AouService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.rest.UnigePersonSearchService;

@Service
@ConditionalOnBean(AdminController.class)
public class ContactService extends AouService {

  private static final String CONTACT_MESSAGE_SENDER_NAME_NOT_NULL = "contact_message.sender_name_missing";
  private static final String CONTACT_MESSAGE_SENDER_EMAIL_NOT_NULL = "contact_message.sender_email_missing";
  private static final String CONTACT_MESSAGE_RECIPIENT_NOT_NULL = "contact_message.recipient_missing";
  private static final String CONTACT_MESSAGE_EMPTY_MESSAGE = "contact_message.empty_message";

  private final PublicationService publicationService;
  private final UnigePersonSearchService unigePersonSearchService;
  private final MetadataService metadataService;
  private final String adminEmail;

  public ContactService(MessageService messageService, PublicationService publicationService,
          UnigePersonSearchService unigePersonSearchService, MetadataService metadataService, AouProperties aouProperties) {
    super(messageService);
    this.publicationService = publicationService;
    this.unigePersonSearchService = unigePersonSearchService;
    this.metadataService = metadataService;
    this.adminEmail = aouProperties.getAdminEmail();
  }

  /**
   * Returns the list of contributors for whom an email can be found in the people search service.
   *
   * @param publicationId
   * @return
   */
  public List<ContactableContributor> getContributorsContactableByEmail(String publicationId) {
    Publication publication = this.publicationService.findOne(publicationId);
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    List<AbstractContributor> abstractContributors = metadataExtractor.getContributors(depositDoc);
    List<ContributorDTO> contributorDTOs = abstractContributors.stream().filter(ContributorDTO.class::isInstance).map(ContributorDTO.class::cast)
            .collect(Collectors.toList());

    List<ContactableContributor> contactableContributors = new ArrayList<>();
    for (ContributorDTO contributorDTO : contributorDTOs) {
      if (!StringTool.isNullOrEmpty(contributorDTO.getCnIndividu())) {
        Optional<UnigePersonDTO> unigePersonDtoOpt = this.unigePersonSearchService.searchByCnIndividu(contributorDTO.getCnIndividu());
        if (unigePersonDtoOpt.isPresent() && !StringTool.isNullOrEmpty(unigePersonDtoOpt.get().getEmail())) {
          contactableContributors.add(
                  new ContactableContributor(contributorDTO.getCnIndividu(), contributorDTO.getFirstName(), contributorDTO.getLastName()));
        }
      }
    }
    return contactableContributors;
  }

  public void sendContactMessage(String publicationId, PublicationContactMessage contactMessage) {
    final BindingResult errors = new BeanPropertyBindingResult(contactMessage, contactMessage.getClass().getName());
    this.validateContactMessage(contactMessage, errors, ValidationType.CONTACT_MESSAGE);
    if (errors.hasErrors()) {
      throw new AouMetadataValidationException(new ValidationError(errors));
    }

    Optional<UnigePersonDTO> unigePersonDtoOpt = this.unigePersonSearchService.searchByCnIndividu(contactMessage.getRecipientCnIndividu());
    if (unigePersonDtoOpt.isPresent() && !StringTool.isNullOrEmpty(unigePersonDtoOpt.get().getEmail())) {
      Publication publication = this.publicationService.findOne(publicationId);
      Map<String, Object> parameters = getParametersMap(contactMessage, publication.getResId());
      SolidifyEventPublisher.getPublisher().publishEvent(
              new EmailMessage(unigePersonDtoOpt.get().getEmail(), EmailMessage.EmailTemplate.CONTACT_MESSAGE_TO_CONTRIBUTOR, parameters));
    }
  }


  public void sendContactMessage(PublicationContactMessage contactMessage) {
    final BindingResult errors = new BeanPropertyBindingResult(contactMessage, contactMessage.getClass().getName());
    this.validateContactMessage(contactMessage, errors, ValidationType.CONTACT_MESSAGE);
    if (errors.hasErrors()) {
      throw new AouMetadataValidationException(new ValidationError(errors));
    }

    Optional<UnigePersonDTO> unigePersonDtoOpt = this.unigePersonSearchService.searchByCnIndividu(contactMessage.getRecipientCnIndividu());
    if (unigePersonDtoOpt.isPresent() && !StringTool.isNullOrEmpty(unigePersonDtoOpt.get().getEmail())) {
      Map<String, Object> parameters = getParametersMap(contactMessage, null);
      SolidifyEventPublisher.getPublisher().publishEvent(
              new EmailMessage(unigePersonDtoOpt.get().getEmail(), EmailMessage.EmailTemplate.CONTACT_MESSAGE_TO_CONTRIBUTOR, parameters));
    }
  }


  public void sendAskCorrectionMessage(String publicationId, PublicationContactMessage contactMessage) {
    final Publication publication = this.publicationService.findOne(publicationId);
    final BindingResult errors = new BeanPropertyBindingResult(contactMessage, contactMessage.getClass().getName());

    this.validateContactMessage(contactMessage, errors, ValidationType.UPDATE_REQUEST_MESSAGE);
    if (errors.hasErrors()) {
      throw new AouMetadataValidationException(new ValidationError(errors));
    }
    Map<String, Object> parameters = getParametersMap(contactMessage, publication.getResId());
    SolidifyEventPublisher.getPublisher().publishEvent(
            new EmailMessage(this.adminEmail, EmailMessage.EmailTemplate.ASK_CORRECTION_FOR_PUBLICATION, parameters));
  }

  private Map<String, Object> getParametersMap(PublicationContactMessage contactMessage, String publicationId) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("senderEmail", contactMessage.getSenderEmail());
    parameters.put("senderName", contactMessage.getSenderName());
    parameters.put("messageContent", contactMessage.getMessageContent());
    parameters.put("publicationId", publicationId);
    return parameters;
  }

  private void validateContactMessage(PublicationContactMessage contactMessage, BindingResult errors, ValidationType validationType) {
    if (StringTool.isNullOrEmpty(contactMessage.getSenderName())) {
      errors.addError(new FieldError(contactMessage.getClass().getSimpleName(), "senderName",
              this.messageService.get(CONTACT_MESSAGE_SENDER_NAME_NOT_NULL)));
    }
    if (StringTool.isNullOrEmpty(contactMessage.getSenderEmail())) {
      errors.addError(new FieldError(contactMessage.getClass().getSimpleName(), "senderEmail",
              this.messageService.get(CONTACT_MESSAGE_SENDER_EMAIL_NOT_NULL)));
    }
    if (StringTool.isNullOrEmpty(contactMessage.getRecipientCnIndividu()) && validationType == ValidationType.CONTACT_MESSAGE) {
      errors.addError(new FieldError(contactMessage.getClass().getSimpleName(), "recipientCnIndividu",
              this.messageService.get(CONTACT_MESSAGE_RECIPIENT_NOT_NULL)));
    }
    if (StringTool.isNullOrEmpty(contactMessage.getMessageContent())) {
      errors.addError(new FieldError(contactMessage.getClass().getSimpleName(), "messageContent",
              this.messageService.get(CONTACT_MESSAGE_EMPTY_MESSAGE)));
    }
  }

  // @formatter:off
  public enum ValidationType {
    CONTACT_MESSAGE,
    UPDATE_REQUEST_MESSAGE
  }
}
