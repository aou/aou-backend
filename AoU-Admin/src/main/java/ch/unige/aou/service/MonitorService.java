/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MonitorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.ModuleList;

@Service
@ConditionalOnBean(AdminController.class)
public class MonitorService extends AouService {

  private final String adminModulePublicUrl;
  private final String accessModulePublicUrl;
  private final String authModulePublicUrl;
  private final String indexModulePublicUrl;
  private final String oaiModulePublicUrl;

  public MonitorService(AouProperties aouProperties, AuthorizationClientProperties authClientProperties, MessageService messageService) {
    super(messageService);
    this.adminModulePublicUrl = aouProperties.getModule().getAdmin().getPublicUrl();
    this.accessModulePublicUrl = aouProperties.getModule().getAccess().getPublicUrl();
    this.authModulePublicUrl = authClientProperties.getPublicAuthorizationServerUrl();
    this.indexModulePublicUrl = this.getLinkedModuleUrl(this.adminModulePublicUrl, IndexConstants.INDEX_MODULE);
    this.oaiModulePublicUrl = this.getLinkedModuleUrl(this.accessModulePublicUrl, OAIConstants.OAI_MODULE);
  }

  public ModuleList getModuleUrls() {
    return new ModuleList(this.authModulePublicUrl, this.adminModulePublicUrl, this.accessModulePublicUrl, this.indexModulePublicUrl,
            this.oaiModulePublicUrl);
  }

  private String getLinkedModuleUrl(String mainModuleUrl, String linkedModuleName) {
    return mainModuleUrl.substring(0, mainModuleUrl.lastIndexOf("/") + 1) + linkedModuleName;
  }
}
