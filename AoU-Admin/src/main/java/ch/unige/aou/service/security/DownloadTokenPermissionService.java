/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DownloadTokenPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.security;

import org.springframework.stereotype.Service;

import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.service.DownloadTokenService;
import ch.unige.solidify.service.security.SolidifyDownloadTokenPermissionService;

import ch.unige.aou.model.security.DownloadTokenType;

@Service
public class DownloadTokenPermissionService extends SolidifyDownloadTokenPermissionService<DownloadTokenType> {

  public DownloadTokenPermissionService(DownloadTokenService downloadTokenService) {
    super(downloadTokenService);
  }



}
