/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DOIResolver.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.net.URI;

import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

import ch.unige.solidify.util.StringTool;

public class DOIResolver {

  private static final String DOI_URL = "https://doi.org/";

  public static String getDoiFromShortDOI(String shortDoi) {
    final WebClient client = WebClient.create();
    URI location = URI.create(DOI_URL + shortDoi);

    ResponseEntity<Void> response = client.get()
            .uri(location)
            .retrieve()
            .toBodilessEntity()
            .block();

    return DOIResolver.extractDOIFromResponse(response);
  }

  private static String extractDOIFromResponse(ResponseEntity response) {
    // check if the response is a redirect, if so, get the header 'Location' that contains the long DOI
    if (response.getStatusCode().is3xxRedirection()) {
      String location =  response.getHeaders().getLocation().toString();
      if (location.startsWith(DOI_URL)) {
        return StringTool.isNullOrEmpty(location) ? null : location.replace(DOI_URL, "");
      } else {
        return null;
      }
    } else {
      return null; // in case DOI has not been found.
    }
  }
}
