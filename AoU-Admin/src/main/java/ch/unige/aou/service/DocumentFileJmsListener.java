/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DocumentFileJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.DocumentFileMessage;

@Service
@ConditionalOnBean(AdminController.class)
public class DocumentFileJmsListener extends MessageProcessor<DocumentFileMessage> {

  private static final Logger log = LoggerFactory.getLogger(DocumentFileJmsListener.class);

  private DocumentFileProcessingService documentFileProcessingService;
  private DocumentFileJmsEmitter documentFileJmsEmitter;

  public DocumentFileJmsListener(AouProperties aouProperties, DocumentFileProcessingService documentFileProcessingService,
          DocumentFileJmsEmitter documentFileJmsEmitter) {
    super(aouProperties);
    this.documentFileProcessingService = documentFileProcessingService;
    this.documentFileJmsEmitter = documentFileJmsEmitter;
  }

  @Override
  @JmsListener(destination = "${aou.queue.documentFiles}")
  public void receiveMessage(DocumentFileMessage documentFileMessage) {
    this.sendForParallelProcessing(documentFileMessage);
  }

  @Override
  public void processMessage(DocumentFileMessage documentFileMessage) {

    log.info("Received DocumentFileMessage: " + documentFileMessage.toString());

    final boolean mustSendNewMessage = this.documentFileProcessingService.processDocumentFile(documentFileMessage);

    /*
     * Sending a new message is done outside processDocumentFile() to ensure Transaction is committed
     * when the message is sent
     */
    if (mustSendNewMessage) {
      log.info("----> DocumentFileJmsListener will send a DocumentFileMessage for DocumentFile '{}'", documentFileMessage.getResId());
      this.documentFileJmsEmitter.sendDocumentFileMessageToQueue(new DocumentFileMessage(documentFileMessage.getResId()));
    }
  }
}
