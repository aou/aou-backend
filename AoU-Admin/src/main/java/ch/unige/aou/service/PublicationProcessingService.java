/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.PublicationUpdateMessage;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.Publication.PublicationStatus;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.storage.StorageService;

@Service
@ConditionalOnBean(AdminController.class)
public class PublicationProcessingService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(PublicationProcessingService.class);

  private final PublicationService publicationService;
  private final StorageService storageService;
  private final HistoryService historyService;
  private final MetadataService metadataService;

  private final Path nfsSharePath;

  public PublicationProcessingService(MessageService messageService, PublicationService publicationService, StorageService storageService,
          MetadataService metadataService, AouProperties aouProperties, HistoryService historyService) {
    super(messageService);
    this.publicationService = publicationService;
    this.storageService = storageService;
    this.metadataService = metadataService;
    this.historyService = historyService;

    this.nfsSharePath = Paths.get(URI.create(aouProperties.getThesisNfsSharePath()));
    if (!FileTool.checkFile(this.nfsSharePath)) {
      throw new SolidifyRuntimeException("NFS share location (" + this.nfsSharePath + ") does not exist");
    }
  }

  /**
   * Return the Publication saved after the processing
   *
   * @param publicationId Id of the Publication to process
   * @return boolean
   */
  @Transactional
  public Publication processPublication(String publicationId) {

    final Publication publication = this.publicationService.findOne(publicationId);

    this.logPublicationMessage(LogLevel.INFO, publication, "will be processed");

    try {
      switch (publication.getStatus()) {
        case SUBMITTED:
          this.processSubmittedPublication(publication);
          break;
        case CHECKED:
          this.processCheckedPublication(publication);
          break;
        case IN_PREPARATION:
          this.processInPreparationPublication(publication);
          break;
        case CANCEL_EDITION:
          this.revertPublicationChanges(publication);
          break;
        case UPDATE_IN_BATCH:
          this.updateInBatchPublication(publication);
        default:
          break;
      }
    } catch (final SolidifyCheckingException e) {
      if (e.getCause() != null) {
        this.inError(publication, e.getCause().getMessage());
      } else {
        this.inError(publication, e.getMessage());
      }
    } catch (final SolidifyProcessingException e) {
      this.inError(publication, e.getMessage());
    } catch (final Exception e) {
      this.logPublicationMessage(LogLevel.ERROR, publication, "Processing error", e);
      this.inError(publication, e.getMessage());
    }

    return this.publicationService.save(publication);
  }

  private void updateInBatchPublication(Publication publication) {
    publication.setStatus(PublicationStatus.CHECKED);
  }

  private void revertPublicationChanges(Publication publication) {
    if (this.publicationService.areThereChangesInDocumentFiles(publication)) {
      // Take the document files from fedora or file system
      this.storageService.revertDocumentFilesAndThumbnailFromStorageInfo(publication);
    }
    publication.setMetadata(publication.getMetadataBackup());
    // Do not validate metadata when canceling update. It allows to cancel the edition of a publication with an old document file type.
    publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
    publication.setMetadataBackup(null);
    publication.setStatus(PublicationStatus.COMPLETED);
    publication.setStatusMessage(null);
  }

  private void processSubmittedPublication(Publication publication) throws IOException {

    // Get next available archiveId to be ready to calculate URN before storing object
    if (StringTool.isNullOrEmpty(publication.getArchiveId())) {
      publication.setArchiveId(this.storageService.getNextArchiveId(publication));
    }

    if (this.metadataService.updateUnigeDoiIfRequired(publication)) {
      log.info("DOI automatically computed for Publication {}", publication.getArchiveId());
    }

    if (this.metadataService.updateUnigeUrnIfRequired(publication)) {
      log.info("URN automatically computed for Publication {}", publication.getArchiveId());
    }

    this.storageService.checkMetadata(publication);
    publication.setStatus(PublicationStatus.CHECKED);
  }

  private void processCheckedPublication(Publication publication) {
    if (this.historyService.hasBeenUpdatingMetadata(publication.getResId())
            || this.historyService.hasBeenUpdatingMetadataInBatch(publication.getResId())) {
      this.storageService.updateExportMetadata(publication);
    } else {
      this.storageService.generateExportMetadata(publication);
    }
    publication.setStatus(PublicationStatus.IN_PREPARATION);
  }

  private void processInPreparationPublication(Publication publication) throws IOException {
    this.storageService.storePublication(publication);
    publication.setStatus(PublicationStatus.COMPLETED);

    //clean metadataBackup property if needed
    publication.setMetadataBackup(null);
    if (!this.historyService.hasBeenUpdatingMetadata(publication.getResId())
            && !this.historyService.hasBeenUpdatingMetadataInBatch(publication.getResId())
            && !this.historyService.hasBeenCompleted(publication.getResId())) {
      this.generateDoctorInfoFile(publication);
    } else {
      // Send a message that will be used to clear Publication related cache on Access module (such as downloaded files or citations)
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationUpdateMessage(publication.getResId()));
    }
  }

  /*************************************************************************************************************/

  @Transactional
  public Publication processMetadataToUpgradePublication(String publicationId) {
    Publication publication = this.publicationService.findOne(publicationId);

    // metadata is automatically upgraded after a find --> we only need to save the Publication to persist the upgrade
    // note: if metadata have already been updated, the field is not dirty here and we don't need to save the publication again.
    if (publication.isMetadataDirty()) {
      this.logPublicationMessage(LogLevel.INFO, publication, "will be processed for metadata version upgrade");
      publication = this.publicationService.simpleSave(publication);
    }
    return publication;
  }

  /*************************************************************************************************************/

  private void logPublicationMessage(LogLevel logLevel, Publication publication, String message, Exception... e) {
    this.logObjectWithStatusMessage(logLevel, publication.getClass().getSimpleName(), publication.getResId(),
            publication.getStatus().toString(), publication.getTitle(), message, e);
  }

  private void inError(Publication publication, String message) {
    publication.setErrorStatusWithMessage(message);
    this.logPublicationMessage(LogLevel.ERROR, publication, message);
  }

  //Generate info txt file
  public void generateDoctorInfoFile(Publication publication) throws IOException {
    // For Thesis create a file on the share drive containing doctor's address
    if (publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID)
            && !this.metadataService.isBefore2010orBeforeAuthorJoiningUnige(publication)) {
      // Check if there is already an author
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
      final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
      String thesisRef = metadataExtractor.getLocalNumber(depositDoc);
      thesisRef = CleanTool.cleanHyphens(thesisRef, true).replaceAll("\\s", "").replaceAll("\\/", "");
      String doctorEmail = metadataExtractor.getDoctorEmail(depositDoc);
      String doctorAddress = metadataExtractor.getDoctorAddress(depositDoc);

      final Optional<ContributorDTO> authorOpt = metadataExtractor.getFirstAuthor(depositDoc);
      if (authorOpt.isPresent()) {
        String fileName = authorOpt.get().getLastName() + "_" + authorOpt.get().getFirstName() + "_" + thesisRef + AouConstants.TXT_EXT;
        fileName = CleanTool.cleanFileName(fileName, true);

        Files.createFile(this.nfsSharePath.resolve(fileName));
        //write data to the file
        try (BufferedWriter bufferedWriter = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(this.nfsSharePath.resolve(fileName).toFile()), StandardCharsets.UTF_8))) {
          bufferedWriter.write("Email du/de la doctorant-e");
          bufferedWriter.newLine();
          bufferedWriter.write("  " + doctorEmail);
          bufferedWriter.newLine();
          bufferedWriter.write("Adresse postale du/de la doctorant-e");
          bufferedWriter.newLine();
          bufferedWriter.write("  " + doctorAddress.replaceAll("\\r|\\n", " ")); // remove all break lines
        }
      } else {
        throw new SolidifyRuntimeException("First author cannot be found for publication " + publication.getResId());
      }
    }
  }
}
