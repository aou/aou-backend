/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PmidImportService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.imports;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.Funding;
import ch.unige.aou.model.xml.deposit.v2_4.Link;
import ch.unige.aou.model.xml.deposit.v2_4.LinkTypes;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Service
@ConditionalOnBean(AdminController.class)
public class PmidImportService extends MetadataImportService {

  /***
   * Here is the explanation of the mapping of some fields between PMID and the pivot format of the archive-ouverte:
   *
   *  article subtype (editorial, letter, etc...) -> in PMID there several types but only one in archive-ouverte.
   *    Take the order in which they appear and convert them as following:
   *      Book review       ->      Compte rendu de livre
   *      Case reports      ->      Rapport de cas
   *      Comment           ->      Commentaire
   *      Dataset           ->      Article de données
   *      Editorial         ->      Editorial
   *      Letter            ->      Lettre
   *      Meta-Analysis     ->      Méta-analyse
   *      Review            ->      Revue de littérature
   *      Video-Audio Media ->      Check if there is any other publication type, if not, Article video
   *      other values      ->      Article
   *  NCT link -> link with type web
   *  Pagination ->  Pages -> field other
   */
  private static final String XPATH_PMIDREF = "/PubmedArticleSet/PubmedArticle/MedlineCitation";
  private static final String XPATH_PMID_ARTICLE_REF = PmidImportService.XPATH_PMIDREF + "/Article";
  private static final String XPATH_PMID_ARTICLE_NUMBER = PmidImportService.XPATH_PMIDREF + "/Article/ELocationID[@EIdType='pii']";
  private static final String XPATH_ERROR_NODE = "/error";
  private static final String XPATH_DOI_PMID_COUNT = "/eSearchResult/Count";
  private static final String XPATH_DOI_PMID = "/eSearchResult/IdList/Id";
  private static final String XPATH_DOI_PMID_ERROR = "/eSearchResult/ErrorList";
  private static final String XPATH_PMID_PMC = "PubmedArticleSet/PubmedArticle/PubmedData/ArticleIdList/ArticleId[@IdType='pmc']";
  private static final String XPATH_PMID_PMID = PmidImportService.XPATH_PMIDREF + "/PMID";
  private static final String XPATH_PMID_KEYWORDS = PmidImportService.XPATH_PMIDREF + "/KeywordList/Keyword";
  private static final String XPATH_PMID_PUBLICATION_TYPE = PmidImportService.XPATH_PMIDREF + "/Article/PublicationTypeList/PublicationType";
  private static final String XPATH_PMID_PAGINATION = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Pagination/MedlinePgn";
  private static final String XPATH_PMID_NCTLINK =
          PmidImportService.XPATH_PMID_ARTICLE_REF + "/DataBankList/DataBank/AccessionNumberList/AccessionNumber";
  private static final String XPATH_PMID_FUNDING_AGENCY = PmidImportService.XPATH_PMIDREF + "/Article/GrantList/Grant";
  private static final String XPATH_PMID_LANGUAGES = PmidImportService.XPATH_PMIDREF + "/Article/Language";
  private static final String XPATH_PMID_CONTRIBUTOR = PmidImportService.XPATH_PMIDREF + "/Article/AuthorList/Author";
  private static final String XPATH_PMID_COLLABORATOR = PmidImportService.XPATH_PMIDREF + "/InvestigatorList/Investigator";
  private static final String XPATH_PMID_PUBLICATION_DAY = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Journal/JournalIssue/PubDate/Day";
  private static final String XPATH_PMID_PUBLICATION_MONTH = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Journal/JournalIssue/PubDate/Month";
  private static final String XPATH_PMID_PUBLICATION_YEAR = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Journal/JournalIssue/PubDate/Year";
  private static final String XPATH_PMID_JOURNAL_ISSUE = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Journal/JournalIssue/Issue";
  private static final String XPATH_PMID_TITLE = PmidImportService.XPATH_PMID_ARTICLE_REF + "/ArticleTitle";
  private static final String XPATH_PMID_JOURNAL_TITLE = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Journal/Title";
  private static final String XPATH_PMID_ISSN = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Journal/ISSN";
  private static final String XPATH_PMID_ABSTRACT = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Abstract/AbstractText";
  private static final String XPATH_PMID_DOI = "/PubmedArticleSet/PubmedArticle/PubmedData/ArticleIdList/ArticleId[@IdType='doi']";
  private static final String XPATH_PMID_JOURNAL_VOLUME = PmidImportService.XPATH_PMID_ARTICLE_REF + "/Journal/JournalIssue/Volume";
  private static final String XPATH_PMID_MESH_KEYWORDS = PmidImportService.XPATH_PMIDREF + "/MeshHeadingList/MeshHeading";
  private static final String XPATH_PMID_OTHER_ABSTRACT = PmidImportService.XPATH_PMIDREF + "/OtherAbstract";
  private static final String XPATH_PMID_VERNACULAR_TITLE = PmidImportService.XPATH_PMID_ARTICLE_REF + "/VernacularTitle";
  private static final String XPATH_PMID_FIRST_ONLINE_DATE_YEAR = PmidImportService.XPATH_PMID_ARTICLE_REF + "/ArticleDate/Year";
  private static final String XPATH_PMID_FIRST_ONLINE_DATE_MONTH = PmidImportService.XPATH_PMID_ARTICLE_REF + "/ArticleDate/Month";
  private static final String XPATH_PMID_FIRST_ONLINE_DATE_DAY = PmidImportService.XPATH_PMID_ARTICLE_REF + "/ArticleDate/Day";
  private static final String XPATH_PMID_GRANTID = "./GrantID";
  private static final String XPATH_PMID_AGENCY = "./Agency";
  private static final String XPATH_PMID_COUNTRY = "./Country";

  private static final String DOI_NOT_FOUND_BY_PUBMED_MESSAGE = "The DOI '%s' was not found by Pubmed";
  private static final String PMID_NOT_FOUND_BY_PUBMED_MESSAGE = "The PMID '%s' was not found by Pubmed";

  private final String apiUrl;
  private final String apiWithDoiUrl;

  private final int millisecondsBetweenPubmedImports;

  public PmidImportService(AouProperties aouProperties, MessageService messageService, MetadataService metadataService,
          ContributorService contributorService, JournalTitleRemoteService journalTitleRemoteService, LicenseService licenseService) {
    super(aouProperties, messageService, metadataService, contributorService, journalTitleRemoteService, licenseService);
    this.apiUrl = aouProperties.getMetadata().getImports().getPmid().getUrl();
    this.apiWithDoiUrl = aouProperties.getMetadata().getImports().getPmid().getDoiUrl();
    this.millisecondsBetweenPubmedImports = aouProperties.getMetadata().getImports().getPmid().getMillisecondsBetweenPubmedImports();
  }

  public DepositDoc searchDoi(String doi, DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(this.apiWithDoiUrl)) {
      try {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate client = restTemplateBuilder.rootUri(this.apiWithDoiUrl).build();

        Map<String, String> parameters = new HashMap<>();
        parameters.put("doi", doi);

        final String xmlString = client.getForObject(this.apiWithDoiUrl, String.class, parameters);
        Document doiPmidDoc = this.parseXML(xmlString);

        //Verify that there is no error and only one result
        final XPath xpath = XMLTool.buildXPath();
        Node errorNode = (Node) xpath.compile(PmidImportService.XPATH_DOI_PMID_ERROR).evaluate(doiPmidDoc, XPathConstants.NODE);
        if (errorNode != null && errorNode.getChildNodes().getLength() > 0) {

          throw new SolidifyResourceNotFoundException(String.format(DOI_NOT_FOUND_BY_PUBMED_MESSAGE, doi));
        }
        Node countNode = (Node) xpath.compile(PmidImportService.XPATH_DOI_PMID_COUNT).evaluate(doiPmidDoc, XPathConstants.NODE);
        if (countNode == null || !countNode.getTextContent().equals("1")) {
          throw new SolidifyResourceNotFoundException(String.format(DOI_NOT_FOUND_BY_PUBMED_MESSAGE, doi));
        }
        //get PMID from the url queried by doi
        String pmid = XMLTool.getFirstTextContent(doiPmidDoc, XPATH_DOI_PMID);
        if (!StringTool.isNullOrEmpty(pmid)) {
          //find by PMID into pubmed
          Document pmidDoc = this.getDocumentFromPmid(pmid);
          this.fillDepositDocByDoi(doi, depositDoc, pmidDoc);
        }

        return depositDoc;

      } catch (final HttpClientErrorException.NotFound e) {
        throw new SolidifyResourceNotFoundException(String.format(DOI_NOT_FOUND_BY_PUBMED_MESSAGE, doi));
      } catch (HttpClientErrorException | IOException | ParserConfigurationException | SAXException | XPathExpressionException e) {
        throw new SolidifyRuntimeException("An error occurred when searching DOI '" + doi + "' on Pubmed", e);
      }
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  public DepositDoc searchPmid(String pmid, DepositDoc depositDoc) {
    try {
      Document pmidDoc = this.getDocumentFromPmid(pmid);
      this.fillDepositDoc(pmid, depositDoc, pmidDoc);

      return depositDoc;
    } catch (HttpClientErrorException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching PMID '" + pmid + "' on Pubmed", e);
    }
  }

  private synchronized Document getDocumentFromPmid(String pmid) {
    if (!StringTool.isNullOrEmpty(this.apiUrl)) {

      try {
        SolidifyTime.waitInMilliSeconds(this.millisecondsBetweenPubmedImports);
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

        Map<String, String> parameters = new HashMap<>();
        parameters.put("pmid", pmid);

        final String xmlString = client.getForObject(this.apiUrl, String.class, parameters);
        return this.parseXML(xmlString);
      } catch (final HttpClientErrorException.NotFound e) {
        throw new SolidifyResourceNotFoundException(String.format(PMID_NOT_FOUND_BY_PUBMED_MESSAGE, pmid));
      } catch (HttpClientErrorException | IOException | ParserConfigurationException | SAXException e) {
        throw new SolidifyRuntimeException("An error occurred when searching PMID '" + pmid + "' on Pubmed", e);
      }
    } else {
      throw new SolidifyRuntimeException("Incomplete configuration");
    }
  }

  public void fillDepositDocByDoi(String doi, DepositDoc depositDoc, Document pmidDoc) throws XPathExpressionException {
    final XPath xpath = XMLTool.buildXPath();

    Node errorNode = (Node) xpath.compile(PmidImportService.XPATH_ERROR_NODE).evaluate(pmidDoc, XPathConstants.NODE);
    Node medlineCitationNode = XMLTool.getFirstNode(pmidDoc, PmidImportService.XPATH_PMIDREF);
    if ((errorNode != null && errorNode.getChildNodes().getLength() > 0) || medlineCitationNode == null) {
      throw new SolidifyResourceNotFoundException(String.format(DOI_NOT_FOUND_BY_PUBMED_MESSAGE, doi));
    }

    this.fillPmc(depositDoc, pmidDoc, XPATH_PMID_PMC);
    this.fillPmid(depositDoc, pmidDoc, XPATH_PMID_PMID);
    this.fillKeywords(depositDoc, pmidDoc, XPATH_PMID_KEYWORDS);
    this.fillPublicationType(depositDoc, pmidDoc, XPATH_PMID_PUBLICATION_TYPE);
    this.fillAbstract(depositDoc, pmidDoc, XPATH_PMID_ABSTRACT);

    // replace the whole contributor list by Pubmed metadata that contains generally more metadata (such as affiliation)
    depositDoc.getContributors().getContributorOrCollaboration().clear();
    this.fillContributorsAndCollaborations(depositDoc, pmidDoc, XPATH_PMID_CONTRIBUTOR, XPATH_PMID_COLLABORATOR);
  }

  public void fillDepositDoc(String pmid, DepositDoc depositDoc, Document pmidDoc) throws XPathExpressionException {
    final XPath xpath = XMLTool.buildXPath();

    Node errorNode = (Node) xpath.compile(PmidImportService.XPATH_ERROR_NODE).evaluate(pmidDoc, XPathConstants.NODE);
    Node medlineCitationNode = XMLTool.getFirstNode(pmidDoc, PmidImportService.XPATH_PMIDREF);
    if ((errorNode != null && errorNode.getChildNodes().getLength() > 0) || medlineCitationNode == null) {
      throw new SolidifyResourceNotFoundException(String.format(PMID_NOT_FOUND_BY_PUBMED_MESSAGE, pmid));
    }

    this.fillTitle(depositDoc, pmidDoc, XPATH_PMID_TITLE, XPATH_PMID_VERNACULAR_TITLE);
    this.fillContainerTitle(depositDoc, pmidDoc, XPATH_PMID_JOURNAL_TITLE);
    this.fillIssn(depositDoc, pmidDoc, XPATH_PMID_ISSN);
    this.fillAbstract(depositDoc, pmidDoc, XPATH_PMID_ABSTRACT);
    this.fillDoi(depositDoc, pmidDoc, XPATH_PMID_DOI);
    this.fillPmc(depositDoc, pmidDoc, XPATH_PMID_PMC);
    this.fillPmid(depositDoc, pmidDoc, XPATH_PMID_PMID);
    this.fillContainerVolume(depositDoc, pmidDoc, XPATH_PMID_JOURNAL_VOLUME);
    this.fillContainerIssue(depositDoc, pmidDoc, XPATH_PMID_JOURNAL_ISSUE);
    this.fillPublicationDate(depositDoc, pmidDoc, XPATH_PMID_PUBLICATION_YEAR, XPATH_PMID_PUBLICATION_MONTH, XPATH_PMID_PUBLICATION_DAY);
    this.fillDate(depositDoc, pmidDoc, XPATH_PMID_FIRST_ONLINE_DATE_YEAR, XPATH_PMID_FIRST_ONLINE_DATE_MONTH, XPATH_PMID_FIRST_ONLINE_DATE_DAY,
            DateTypes.FIRST_ONLINE);
    this.fillContributorsAndCollaborations(depositDoc, pmidDoc, XPATH_PMID_CONTRIBUTOR, XPATH_PMID_COLLABORATOR);
    this.fillLanguages(depositDoc, pmidDoc, XPATH_PMID_LANGUAGES);
    this.fillKeywords(depositDoc, pmidDoc, XPATH_PMID_KEYWORDS);
    this.fillAbstractInOtherLanguages(depositDoc, pmidDoc, XPATH_PMID_OTHER_ABSTRACT);
    this.fillMeshKeywords(depositDoc, pmidDoc, XPATH_PMID_MESH_KEYWORDS);
    this.fillNCTLink(depositDoc, pmidDoc, XPATH_PMID_NCTLINK);
    this.fillFundingAgency(depositDoc, pmidDoc, XPATH_PMID_FUNDING_AGENCY);
    this.fillPublicationType(depositDoc, pmidDoc, XPATH_PMID_PUBLICATION_TYPE);
    this.fillPagination(depositDoc, pmidDoc, XPATH_PMID_PAGINATION);
  }

  protected void fillTitle(DepositDoc depositDoc, Document doc, String xpathQueryTitle, String xpathQueryVernacular) {
    String vernacularTitle = XMLTool.getFirstTextContent(doc, xpathQueryVernacular);
    //For title we take vernacularTitle and originalTitle as title, if there is no vernaculartitle, we take the title as main title.
    if (!StringTool.isNullOrEmpty(vernacularTitle)) {
      depositDoc.setTitle(this.getText(vernacularTitle));
      String title = XMLTool.getFirstTextContent(doc, xpathQueryTitle);
      if (!StringTool.isNullOrEmpty(title)) {
        depositDoc.setOriginalTitle(this.getTextLang(title));
      }
    } else {
      String title = XMLTool.getFirstTextContent(doc, xpathQueryTitle);
      if (!StringTool.isNullOrEmpty(title)) {
        depositDoc.setTitle(this.getText(title));
      }
    }
  }

  private void fillPagination(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList pagesList = (NodeList) xpath.compile(xpathQuery).evaluate(pmidDoc, XPathConstants.NODESET);
      String articleNumber = XMLTool.getFirstTextContent(pmidDoc, XPATH_PMID_ARTICLE_NUMBER);

      if (pagesList.getLength() > 0 || !StringTool.isNullOrEmpty(articleNumber)) {
        this.ensureDepositPagesExist(depositDoc);
      }

      // if there is no MedlinePgn we take the <ELocationID EIdType="pii> as article number
      if (pagesList.getLength() == 0 && !StringTool.isNullOrEmpty(articleNumber)) {
        this.setPagingOrOther(depositDoc, articleNumber);
        return; // no need to continue cause there is no pagination
      }

      for (int i = 0; i < pagesList.getLength(); i++) {
        String pages = pagesList.item(i).getTextContent();
        if (pages.contains("-") || pages.length() < 3) {
          depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesPaging(pages));
          if (!StringTool.isNullOrEmpty(articleNumber)) {
            depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesOther(pages));
          }
        } else if (!StringTool.isNullOrEmpty(articleNumber) && pages.equals(articleNumber)) {
          depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesOther(pages));
        } else if (!StringTool.isNullOrEmpty(articleNumber) && !pages.equals(articleNumber)) {
          depositDoc.getPages().getPagingOrOther().add(this.factory.createPagesOther(articleNumber));
        }
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill pages", e);
    }
  }

  private void fillPublicationType(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    try {
      final XPath xpath = XMLTool.buildXPath();
      NodeList publicationTypeList = (NodeList) xpath.compile(xpathQuery).evaluate(pmidDoc, XPathConstants.NODESET);
      if (publicationTypeList.getLength() > 0) {

        // Since in pubmed, there could be multiple publication types, we have to check the priorities: (from higher to lower)
        // Review  > Meta-analyse > Case report >Editorial > Comment > Lettre
        String subSubType = this.checkPriorityBetweenSubTypes(publicationTypeList);

        //check if there is another node for the case of video
        if (subSubType.equals(AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_VIDEO) && publicationTypeList.getLength() > 1) {
          String publicationType2 = publicationTypeList.item(1).getTextContent();
          subSubType = this.transformPubMedPublicationType(publicationType2);
        }

        depositDoc.setSubsubtype(subSubType);
        depositDoc.setSubtype(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
        depositDoc.setType(AouConstants.DEPOSIT_TYPE_ARTICLE_NAME);
      }
    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill funding agencies", e);
    }
  }

  private void fillFundingAgency(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList fundingList = (NodeList) xpath.compile(xpathQuery).evaluate(pmidDoc, XPathConstants.NODESET);

      if (fundingList.getLength() > 0) {
        this.ensureFundingsExist(depositDoc);
      }
      for (int i = 0; i < fundingList.getLength(); i++) {
        String projectNumber = XMLTool.getFirstTextContent(fundingList.item(i), XPATH_PMID_GRANTID);
        String funder = XMLTool.getFirstTextContent(fundingList.item(i), XPATH_PMID_AGENCY);
        String jurisdiction = XMLTool.getFirstTextContent(fundingList.item(i), XPATH_PMID_COUNTRY);

        Funding funding = new Funding();
        funding.setFunder(funder);
        funding.setCode(projectNumber);
        funding.setJurisdiction(jurisdiction);
        depositDoc.getFundings().getFunding().add(funding);
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill funding agencies", e);
    }
  }

  private void fillNCTLink(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(pmidDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value) && value.startsWith("NCT")) {
      this.ensureDepositLinkExist(depositDoc);
      Link link = new Link();
      link.setType(LinkTypes.WEB);
      link.setTarget("NCT");
      link.setDescription(this.getText(value));
      depositDoc.getLinks().getLink().add(link);
    }
  }

  protected void fillPmc(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(pmidDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDepositIdentifiersExist(depositDoc);
      depositDoc.getIdentifiers().setPmcid(value);
    }
  }

  private void fillMeshKeywords(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList keywordsList = (NodeList) xpath.compile(xpathQuery).evaluate(pmidDoc, XPathConstants.NODESET);

      if (keywordsList.getLength() > 0) {
        this.ensureKeywordsExist(depositDoc);
      }
      for (int i = 0; i < keywordsList.getLength(); i++) {
        String keywordBase = XMLTool.getFirstTextContent(keywordsList.item(i), "./DescriptorName");
        NodeList qualifierNameList = (NodeList) xpath.compile("./QualifierName")
                .evaluate(keywordsList.item(i), XPathConstants.NODESET);
        if (qualifierNameList.getLength() > 0) {
          //Create a keyword with each Qualifier Name using / as separator
          for (int j = 0; j < qualifierNameList.getLength(); j++) {
            String keyword = keywordBase + " / " + qualifierNameList.item(j).getTextContent();
            depositDoc.getKeywords().getKeyword().add(keyword);
          }
        } else {
          depositDoc.getKeywords().getKeyword().add(keywordBase);
        }
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill keywords", e);
    }
  }

  private void fillLanguages(DepositDoc depositDoc, Document crossrefDoc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(crossrefDoc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureLanguagesExist(depositDoc);
      String languageCode = CleanTool.getLanguageCode(value);
      depositDoc.getLanguages().getLanguage().add(languageCode);
      if (depositDoc.getTitle() != null) {
        depositDoc.getTitle().setLang(languageCode);
      }
    }
  }

  private void fillContributorsAndCollaborations(DepositDoc depositDoc, Document pmidDoc, String xpathQuery, String collaboratorXPathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList authorsList = (NodeList) xpath.compile(xpathQuery).evaluate(pmidDoc, XPathConstants.NODESET);

      if (authorsList.getLength() > 0) {
        this.ensureContributorsExist(depositDoc);
      }

      // Get until the max limit of contributors + ones that are affiliated with UNIGE
      for (int i = 0; i < this.getNumberMaxContributor(authorsList.getLength()); i++) {
        //check first if the contributor is not a collaboration
        if (StringTool.isNullOrEmpty(XMLTool.getFirstTextContent(authorsList.item(i), "./CollectiveName"))) {
          Contributor contributor = this.createContributorFromNode(authorsList.item(i));
          depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
        }
      }
      // add authors that have an affiliation with unige, geneve, or hug
      NodeList authorsListWithAffiliation = (NodeList) xpath.compile(xpathQuery
                      + "[contains(./AffiliationInfo/Affiliation, 'Geneva') or contains(./AffiliationInfo/Affiliation, 'Genève')  or contains(./AffiliationInfo/Affiliation, 'Genéve')"
                      + "or contains(./AffiliationInfo/Affiliation, 'Unige') or contains(./AffiliationInfo/Affiliation, 'HUG')]")
              .evaluate(pmidDoc, XPathConstants.NODESET);
      for (int i = 0; i < authorsListWithAffiliation.getLength(); i++) {
        //check first if the contributor is not a collaboration
        if (StringTool.isNullOrEmpty(XMLTool.getFirstTextContent(authorsListWithAffiliation.item(i), "./CollectiveName"))) {
          Contributor contributor = this.createContributorFromNode(authorsListWithAffiliation.item(i));
          //check if it was already added
          if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                  .noneMatch(c -> ((Contributor) c).getLastname().equals(contributor.getLastname())
                          && ((Contributor) c).getFirstname().equals(contributor.getFirstname()) && ((Contributor) c).getInstitution()
                          .equals(contributor.getInstitution()))) {
            depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
          }
        }
      }

      NodeList collaborationsList = (NodeList) xpath.compile(xpathQuery + "[CollectiveName]").evaluate(pmidDoc, XPathConstants.NODESET);
      // Get all collaboration names
      for (int i = 0; i < collaborationsList.getLength(); i++) {
        Collaboration collaboration = new Collaboration();
        collaboration.setName(XMLTool.getFirstTextContent(collaborationsList.item(i), "./CollectiveName"));
        depositDoc.getContributors().getContributorOrCollaboration().add(collaboration);
      }

      // Get all collaborators until reaching the limit + ones that are affiliated with UNIGE
      NodeList collaboratorsList = (NodeList) xpath.compile(collaboratorXPathQuery).evaluate(pmidDoc, XPathConstants.NODESET);
      if (collaboratorsList.getLength() > 0) {
        this.ensureContributorsExist(depositDoc);
      }
      for (int i = 0; i < this.getNumberMaxContributor(collaboratorsList.getLength()); i++) {
        //check if it was already added
        Contributor collaborator = this.createContributorFromNode(collaboratorsList.item(i));
        if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                .filter(c -> c instanceof Contributor)
                .noneMatch(c -> ((Contributor) c).getLastname().equals(collaborator.getLastname())
                        && ((Contributor) c).getFirstname().equals(collaborator.getFirstname()))) {
          collaborator.setRole(AuthorRole.COLLABORATOR);
          depositDoc.getContributors().getContributorOrCollaboration().add(collaborator);
        }
      }

      // add authors that have an affiliation with unige, geneve, or hug
      NodeList collaboratorsListWithAffiliation = (NodeList) xpath.compile(collaboratorXPathQuery
                      + "[contains(./AffiliationInfo/Affiliation, 'Geneva') or contains(./AffiliationInfo/Affiliation, 'Genève')  or contains(./AffiliationInfo/Affiliation, 'Genéve')"
                      + "or contains(./AffiliationInfo/Affiliation, 'Unige')]")
              .evaluate(pmidDoc, XPathConstants.NODESET);
      for (int i = 0; i < collaboratorsListWithAffiliation.getLength(); i++) {
        Contributor collaborator = this.createContributorFromNode(collaboratorsListWithAffiliation.item(i));
        //check if it was already added as a contributor or collaborator
        if (depositDoc.getContributors().getContributorOrCollaboration().stream()
                .filter(c -> c instanceof Contributor)
                .noneMatch(c -> ((Contributor) c).getLastname().equals(collaborator.getLastname())
                        && ((Contributor) c).getFirstname().equals(collaborator.getFirstname()))) {
          collaborator.setRole(AuthorRole.COLLABORATOR);
          depositDoc.getContributors().getContributorOrCollaboration().add(collaborator);
        }

      }
    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill contributors", e);
    }
  }

  private void fillAbstract(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList abstractList = (NodeList) xpath.compile(xpathQuery).evaluate(pmidDoc, XPathConstants.NODESET);

      if (abstractList.getLength() > 0) {
        this.ensureAbstractsExist(depositDoc);

        // If an abstract was already imported (probably from Crossref), delete it first to keep only the one from Pubmed
        // which is most of the time better
        if (!depositDoc.getAbstracts().getAbstract().isEmpty()) {
          depositDoc.getAbstracts().getAbstract().clear();
        }
      }
      //the abstract is structured in different categories: background, objectives, methods, results, conclusions and we need to construct just
      //one abstract with all these texts.
      StringBuilder abstractTextBuilder = new StringBuilder();
      for (int i = 0; i < abstractList.getLength(); i++) {
        String labelStr = null;
        Node attributeLabelNode = abstractList.item(i).getAttributes().getNamedItem("Label");
        if (attributeLabelNode != null) {
          labelStr = StringTool.capitalize(attributeLabelNode.getNodeValue());
        }
        if (abstractList.getLength() > 1) {
          if (StringTool.isNullOrEmpty(labelStr)) {
            abstractTextBuilder.append("<p>").append(XMLTool.getFirstNodeInnerRawContent(abstractList.item(i), ".", true)).append("</p>\n");
          } else {
            abstractTextBuilder.append("<p>").append("<b>").append(labelStr).append(": </b>")
                    .append(XMLTool.getFirstNodeInnerRawContent(abstractList.item(i), ".", true)).append("</p>\n");
          }
        } else {
          if (StringTool.isNullOrEmpty(labelStr)) {
            abstractTextBuilder.append(XMLTool.getFirstNodeInnerRawContent(abstractList.item(i), ".", true));
          } else {
            abstractTextBuilder.append("<b>").append(labelStr).append(": </b>")
                    .append(XMLTool.getFirstNodeInnerRawContent(abstractList.item(i), ".", true));
          }
        }
      }
      if (abstractList.getLength() > 0) {
        depositDoc.getAbstracts().getAbstract().add(this.getText(abstractTextBuilder.toString(), AouConstants.LANG_CODE_ENGLISH));
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill abstracts", e);
    }
  }

  private void fillAbstractInOtherLanguages(DepositDoc depositDoc, Document pmidDoc, String xpathQuery) {
    final XPath xpath = XMLTool.buildXPath();
    try {
      NodeList abstractList = (NodeList) xpath.compile(xpathQuery).evaluate(pmidDoc, XPathConstants.NODESET);

      if (abstractList.getLength() > 0) {
        this.ensureAbstractsExist(depositDoc);
      }
      for (int i = 0; i < abstractList.getLength(); i++) {
        String abstractText = abstractList.item(i).getTextContent();
        String language = XMLTool.getFirstTextContent(abstractList.item(i), xpathQuery + "/@Language");
        depositDoc.getAbstracts().getAbstract().add(this.getText(abstractText, language));
      }

    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("Unable to fill abstracts", e);
    }
  }

  protected void fillPmid(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureDepositIdentifiersExist(depositDoc);
      depositDoc.getIdentifiers().setPmid(new BigInteger(value));
    }
  }

  protected void fillContainerVolume(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setVolume(value);
    }
  }

  protected void fillContainerIssue(DepositDoc depositDoc, Document doc, String xpathQuery) {
    String value = XMLTool.getFirstTextContent(doc, xpathQuery);
    if (!StringTool.isNullOrEmpty(value)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setIssue(value);
    }
  }

  /****************************************************************************************/

  private String checkPriorityBetweenSubTypes(NodeList publicationTypeList) {
    Stream<Node> nodeStream = IntStream.range(0, publicationTypeList.getLength())
            .mapToObj(publicationTypeList::item);
    List<Node> nodes = nodeStream.sorted(Comparator.comparingInt(node -> this.getSubTypePriority(node.getTextContent())))
            .collect(Collectors.toList());
    String publicationType = nodes.get(0).getTextContent();
    return this.transformPubMedPublicationType(publicationType);
  }

  private int getSubTypePriority(String pubmedType) {
    switch (pubmedType) {
      case "Review":
        return 1;
      case "Meta-Analysis":
        return 2;
      case "Case Reports":
        return 3;
      case "Editorial":
        return 4;
      case "Comment":
        return 5;
      case "Letter":
        return 6;
      default:
        return 7;
    }
  }

  private String transformPubMedPublicationType(String pubmedType) {
    switch (pubmedType) {
      case "Book review":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME;
      case "Case Reports":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_RAPPORT_DU_CAS_NAME;
      case "Comment":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME;
      case "Dataset":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_DONNEES_NAME;
      case "Editorial":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_EDITORIAL_NAME;
      case "Letter":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_LETTRE_NAME;
      case "Meta-Analysis":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_META_ANALYSIS_NAME;
      case "Review":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_REVIEW_LITERATURE_NAME;
      case "Video-Audio Media":
        return AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_VIDEO;
      default:
        return AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME;
    }
  }

  private Contributor createContributorFromNode(Node author) throws XPathExpressionException {
    final XPath xpath = XMLTool.buildXPath();

    String lastName = XMLTool.getFirstTextContent(author, "./LastName");
    String firstName = XMLTool.getFirstTextContent(author, "./ForeName");
    String initials = XMLTool.getFirstTextContent(author, "./Initials");
    String suffix = XMLTool.getFirstTextContent(author, "./Suffix");

    lastName = this.getNameOrUndefinedValue(lastName);

    Contributor contributor = new Contributor();
    contributor.setLastname(CleanTool.capitalizeString(lastName));
    if (firstName == null) {
      initials = this.getNameOrUndefinedValue(initials);
      contributor.setFirstname(CleanTool.capitalizeString(initials));
    } else {
      contributor.setFirstname(suffix == null ?
              CleanTool.capitalizeString(firstName) :
              CleanTool.capitalizeString(firstName) + " (" + CleanTool.capitalizeString(suffix) + ")");
    }

    //check if there are institutions
    NodeList institutionList = (NodeList) xpath.compile("./AffiliationInfo/Affiliation")
            .evaluate(author, XPathConstants.NODESET);
    if (institutionList.getLength() > 0) {
      String institution = "";
      for (int j = 0; j < institutionList.getLength(); j++) {
        institution = (institution.isEmpty()) ?
                institution.concat(institutionList.item(j).getTextContent()) :
                institution.concat("," + institutionList.item(j).getTextContent());
      }
      contributor.setInstitution(institution);
    }

    //check if contains orcid
    Node orcidId = (Node) xpath.compile("./Identifier[@Source='ORCID']").evaluate(author, XPathConstants.NODE);
    if (orcidId != null) {
      String orcidWithUrl = orcidId.getTextContent();
      String orcid = this.cleanOrcid(orcidWithUrl);
      if (!StringTool.isNullOrEmpty(orcid)) {
        contributor.setOrcid(orcid);
      }
    }
    return contributor;
  }
}
