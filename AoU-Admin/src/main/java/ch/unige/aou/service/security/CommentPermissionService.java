/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - CommentPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.security;

import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.aou.business.CommentService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.controller.AouControllerAction;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.Publication;

@Service("commentPermissionService")
@ConditionalOnBean(AdminController.class)
public class CommentPermissionService extends AbstractAdminPermissionService {

  private CommentService commentService;
  private PublicationService publicationService;

  public CommentPermissionService(PersonService personService, CommentService commentService, PublicationService publicationService) {
    super(personService);
    this.commentService = commentService;
    this.publicationService = publicationService;
  }

  public boolean isAllowed(String targetResId, String actionString) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }

    Comment existingResource = this.getExistingResource(targetResId);
    if (existingResource == null) {
      throw new NoSuchElementException(targetResId);
    }

    String personId = this.getPersonAuthenticatedId();
    AouControllerAction action = this.getControllerAction(actionString);
    return this.isAllowedToPerformActionOnResource(personId, existingResource, action);
  }

  /******************************************************************************************************/

  private Comment getExistingResource(String resId) {
    return this.commentService.findOne(resId);
  }

  /******************************************************************************************************/

  private boolean isAllowedToPerformActionOnResource(String personId, Comment resourceToCheck, AouControllerAction action) {

    // the only action checked by CommentPermissionService so far is to get a comment
    if (action != AouControllerAction.GET_DEPOSIT_COMMENT) {
      return false;
    }

    Comment comment = this.commentService.findOne(resourceToCheck.getResId());
    Publication publication = this.publicationService.findOne(comment.getPublicationId());

    return this.userIsAllowedToReadComment(personId, comment, publication);
  }

  /**
   * Return true if the person can read the comment.
   * It is the case if:
   * 1) comment only for validators:
   * - the person is validator of the publication
   * <p>
   * 2) comment is not reserved to validators:
   * - person is creator of the publication
   * - person is contributor of the publication
   * - person is validator of the publication
   * - person has written the comment
   *
   * @param personId
   * @param comment
   * @param publication
   * @return
   */
  private boolean userIsAllowedToReadComment(String personId, Comment comment, Publication publication) {

    if (comment.isOnlyForValidators() && this.personService.isAllowedToValidate(personId, publication)) {
      return true;
    } else {
      // user is comment's author
      return this.publicationService.isPersonPublicationCreator(personId, publication.getResId()) // user is a contributor // user is
                                                                                                  // publication's depositor
              || this.personService.isAllowedToValidate(personId, publication) // user is validator
              || comment.getCreatorId().equals(personId);
    }
  }
}
