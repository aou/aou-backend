/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - IndexClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.service.IndexResourceService;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.rest.AouSearchCondition;

@Service
@ConditionalOnBean(AdminController.class)
public class IndexClientService {

  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;

  private final String indexPublicationsInProgressName;

  public IndexClientService(AouProperties aouProperties, IndexResourceService<String, PublicationIndexEntry> indexResourceService) {
    this.indexResourceService = indexResourceService;
    this.indexPublicationsInProgressName = aouProperties.getIndexing().getInProgressPublicationsIndexName();
  }

  public List<String> findPublicationIds(String query) {
    List<AouSearchCondition> conditionList = new ArrayList<>();
    AouSearchCondition identifierCondition = new AouSearchCondition();
    identifierCondition.setType(SearchConditionType.QUERY);
    identifierCondition.setValue(query);
    conditionList.add(identifierCondition);

    FieldsRequest fieldRequest = new FieldsRequest();
    fieldRequest.getIncludes().add(AouConstants.INDEX_FIELD_ID);

    List<String> publicationIds = new ArrayList<>();

    Pageable pageable = PageRequest.of(0, 50, Sort.by(AouConstants.INDEX_FIELD_ARCHIVE_ID_INT));
    Page<PublicationIndexEntry> page;
    do {
      page = this.indexResourceService.search(this.indexPublicationsInProgressName,
              conditionList.stream().map(SearchCondition.class::cast).collect(Collectors.toList()), null, pageable, fieldRequest);
      List<String> resIds = page.stream().map(NoSqlResource::getResId).collect(Collectors.toList());
      publicationIds.addAll(resIds);
      pageable = page.nextPageable();
    } while (page.hasNext());

    return publicationIds;
  }
}
