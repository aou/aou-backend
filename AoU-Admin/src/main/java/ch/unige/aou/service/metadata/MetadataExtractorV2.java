/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataExtractorV2.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.io.Serializable;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBElement;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LabeledLanguageService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.OtherNameDTO;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.rest.UnigePersonDTO;
import ch.unige.aou.model.settings.LabeledLanguage;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.Abstracts;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructure;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructures;
import ch.unige.aou.model.xml.deposit.v2_4.AccessLevel;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Classification;
import ch.unige.aou.model.xml.deposit.v2_4.Classifications;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Collection;
import ch.unige.aou.model.xml.deposit.v2_4.Collections;
import ch.unige.aou.model.xml.deposit.v2_4.Container;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.Contributors;
import ch.unige.aou.model.xml.deposit.v2_4.Correction;
import ch.unige.aou.model.xml.deposit.v2_4.Corrections;
import ch.unige.aou.model.xml.deposit.v2_4.Datasets;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.Dates;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v2_4.Embargo;
import ch.unige.aou.model.xml.deposit.v2_4.File;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.model.xml.deposit.v2_4.Files;
import ch.unige.aou.model.xml.deposit.v2_4.Funding;
import ch.unige.aou.model.xml.deposit.v2_4.Fundings;
import ch.unige.aou.model.xml.deposit.v2_4.Group;
import ch.unige.aou.model.xml.deposit.v2_4.Groups;
import ch.unige.aou.model.xml.deposit.v2_4.Keywords;
import ch.unige.aou.model.xml.deposit.v2_4.Languages;
import ch.unige.aou.model.xml.deposit.v2_4.Link;
import ch.unige.aou.model.xml.deposit.v2_4.Links;
import ch.unige.aou.model.xml.deposit.v2_4.ObjectFactory;
import ch.unige.aou.model.xml.deposit.v2_4.OtherName;
import ch.unige.aou.model.xml.deposit.v2_4.Pages;
import ch.unige.aou.model.xml.deposit.v2_4.Publisher;
import ch.unige.aou.model.xml.deposit.v2_4.Text;
import ch.unige.aou.model.xml.deposit.v2_4.TextLang;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.AbstractsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.AcademicsStructuresDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.ClassificationsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.CollectionsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.ContainerDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.ContributorsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.CorrectionsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.DatasetsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.DateWithTypeDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.DatesDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.DepositIdentifiersDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.FilesDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.FundingsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.GroupsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.KeywordsDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.LanguagesDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.LinksDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.PagesDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.PublisherDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.TextDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.deserializer.TextLangDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.AbstractsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.AcademicStructuresSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.AuthorRoleSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.ClassificationsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.CollectionsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.ContributorsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.CorrectionsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DatasetsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DateWithTypeSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DatesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DepositDocSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DepositIdentifiersSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.FilesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.FundingsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.GroupsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.KeywordsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.LanguagesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.LinksSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.PagesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.PublisherSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.TextLangSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.TextSerializer;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.rest.UnigePersonSearchService;

@Service
@ConditionalOnBean(AdminController.class)
public class MetadataExtractorV2 extends MetadataExtractor {

  private static final Logger log = LoggerFactory.getLogger(MetadataExtractorV2.class);

  ObjectFactory factory = new ObjectFactory();

  public MetadataExtractorV2(AouProperties aouProperties, PublicationTypeService publicationTypeService,
          PublicationSubtypeService publicationSubtypeService,
          PublicationSubSubtypeService publicationSubSubtypeService, StructureService structureService,
          ResearchGroupService researchGroupService, ContributorService contributorService, LabeledLanguageService labeledLanguageService,
          JournalTitleRemoteService journalTitleRemoteService, PersonService personService, MessageService messageService,
          UnigePersonSearchService unigePersonSearchService) {
    super(aouProperties, new DepositDocV2Adapter(), publicationTypeService, publicationSubtypeService, publicationSubSubtypeService,
            structureService, researchGroupService, contributorService, labeledLanguageService, journalTitleRemoteService, personService,
            messageService, unigePersonSearchService);
  }

  @Override
  public String transformFormDataToMetadata(String jsonData) {
    DepositDoc depositDoc = this.createDepositDocFromJsonFormData(jsonData);
    return this.serializeDepositDocToXml(depositDoc);
  }

  @Override
  public DepositDoc createDepositDocFromJsonFormData(String jsonData) {

    try {
      ObjectMapper mapper = this.getJsonFormDeserializerMapper();

      JsonNode root = mapper.readTree(jsonData);
      JsonNode typeNode = root.at("/type");
      JsonNode contributorsNode = root.at("/contributors");
      JsonNode descriptionNode = root.at("/description");

      DepositDoc depositDoc = mapper.readValue(typeNode.toString(), DepositDoc.class);
      ObjectReader updater = mapper.readerForUpdating(depositDoc);
      depositDoc = updater.readValue(contributorsNode.toString());
      updater = mapper.readerForUpdating(depositDoc);
      depositDoc = updater.readValue(descriptionNode.toString());

      this.replaceFormValuesByMetadataValues(depositDoc);

      return depositDoc;

    } catch (JsonProcessingException e) {
      if (e.getCause() instanceof SolidifyValidationException ex) {
        throw (SolidifyValidationException) ex.getCause();
      }
      throw new SolidifyRuntimeException("unable to parse json data", e);
    }
  }

  @Override
  public void replaceFormValuesByMetadataValues(Object depositDocObj) {
    super.replaceFormValuesByMetadataValues(depositDocObj);
    this.completeStructures(depositDocObj);
    this.completeResearchGroups(depositDocObj);
  }

  private void completeStructures(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<Structure> structuresList = this.getStructures(depositDocObj);
    if (!structuresList.isEmpty()) {
      AcademicStructures academicStructures = new AcademicStructures();
      for (Structure structure : structuresList) {
        if (!StringTool.isNullOrEmpty(structure.getResId())) {
          AcademicStructure academicStructure = new AcademicStructure();
          Structure structureDB = this.structureService.findById(structure.getResId()).orElse(null);
          academicStructure.setResId(structure.getResId());
          if (structureDB != null) {
            academicStructure.setCode(structureDB.getCodeStruct());
            academicStructure.setName(structureDB.getName());
          }
          academicStructures.getAcademicStructure().add(academicStructure);
        }
      }
      depositDoc.setAcademicStructures(academicStructures);
    }
  }

  private void completeResearchGroups(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<ResearchGroup> researchGroupList = this.getResearchGroups(depositDocObj);
    if (!researchGroupList.isEmpty()) {
      Groups groups = new Groups();
      for (ResearchGroup researchGroup : researchGroupList) {
        if (!StringTool.isNullOrEmpty(researchGroup.getResId())) {
          Group group = new Group();
          ResearchGroup researchGroupDb = this.researchGroupService.findById(researchGroup.getResId()).orElse(null);
          group.setResId(researchGroup.getResId());
          if (researchGroupDb != null) {
            group.setName(researchGroupDb.getName());
            group.setCode(!StringTool.isNullOrEmpty(researchGroupDb.getCode()) ? new BigInteger(researchGroupDb.getCode()) : null);
          }
          groups.getGroup().add(group);
        }
      }
      depositDoc.setGroups(groups);
    }
  }

  @Override
  public String transformMetadataToFormData(String xmlData) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(xmlData);

    this.replaceMetadataValuesByFormValues(depositDoc);

    try {
      ObjectMapper mapper = this.getJsonFormSerializerMapper();
      return mapper.writeValueAsString(depositDoc);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to transform XML data to Json form data", e);
    }
  }

  /**
   * 1) Save the document files properties into the metadata content 2) Save the eventual thumbnail into metadata
   */
  @Override
  public void completeMetadataWithFiles(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    Files files = new Files();

    // add DocumentFiles into metadata
    for (DocumentFile df : publication.getDocumentFiles()) {
      File xmlFile = this.convertDocumentFileToXmlFile(df);
      files.getFile().add(xmlFile);
    }

    // add Thumbnail into metadata as Vignette
    if (publication.getThumbnail() != null) {
      File thumbnailFile = new File();
      thumbnailFile.setType(FileType.VIGNETTE);
      thumbnailFile.setAccessLevel(AccessLevel.PUBLIC);
      if (publication.getThumbnail().getFileSize() != null) {
        thumbnailFile.setSize(BigInteger.valueOf(publication.getThumbnail().getFileSize()));
      }
      thumbnailFile.setMimetype(publication.getThumbnail().getMimeType());
      thumbnailFile.setName(publication.getThumbnail().getFileName());
      files.getFile().add(thumbnailFile);
    }

    if (!files.getFile().isEmpty()) {
      depositDoc.setFiles(files);
    } else {
      depositDoc.setFiles(null);
    }

    // add into the publication metadata the info from file
    String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(xmlMetadata);
  }

  @Override
  public void resetUnigeContributorsDisplayName(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    // First reset contributors name by the main Contributor name
    for (Serializable serializable : depositDoc.getContributors().getContributorOrCollaboration()) {
      if (serializable instanceof Contributor metadataContributor) {
        String metadataCnIndividu = metadataContributor.getCnIndividu();
        if (!StringTool.isNullOrEmpty(metadataCnIndividu)) {
          ch.unige.aou.model.publication.Contributor contributor = this.contributorService.findByCnIndividu(metadataCnIndividu);
          metadataContributor.setFirstname(contributor.getFirstName());
          metadataContributor.setLastname(contributor.getLastName());
          metadataContributor.setOtherNames(null);
        }
      }
    }

    String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(xmlMetadata);
  }

  @Override
  public void replaceUnigeContributorsDisplayName(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    // Then replace their names by any eventual link between publication and ContributorOtherName
    if (!publication.getContributorOtherNames().isEmpty()) {
      for (ContributorOtherName otherNameLinkedToPublication : publication.getContributorOtherNames()) {
        ch.unige.aou.model.publication.Contributor contributor = otherNameLinkedToPublication.getContributor();

        if (depositDoc.getContributors() != null) {
          for (Serializable serializable : depositDoc.getContributors().getContributorOrCollaboration()) {
            if (serializable instanceof Contributor metadataContributor) {
              String metadataCnIndividu = metadataContributor.getCnIndividu();
              if (contributor.getCnIndividu().equals(metadataCnIndividu)) {
                // Set main name in metadata with value form the linked other name
                metadataContributor.setFirstname(otherNameLinkedToPublication.getFirstName());
                metadataContributor.setLastname(otherNameLinkedToPublication.getLastName());

                // Complete other names in metadata
                metadataContributor.setOtherNames(this.factory.createOtherNames());
                // First by adding the main contributor name
                OtherName metadataOtherName = this.factory.createOtherName();
                metadataOtherName.setFirstname(contributor.getFirstName());
                metadataOtherName.setLastname(contributor.getLastName());
                metadataContributor.getOtherNames().getOtherName().add(metadataOtherName);
                for (ContributorOtherName cOtherName : contributor.getOtherNames()) {
                  if (!cOtherName.equals(otherNameLinkedToPublication)) {
                    metadataOtherName = this.factory.createOtherName();
                    metadataOtherName.setFirstname(cOtherName.getFirstName());
                    metadataOtherName.setLastname(cOtherName.getLastName());
                    metadataContributor.getOtherNames().getOtherName().add(metadataOtherName);
                  }
                }
              }
            }
          }
        }
      }
    }

    String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(xmlMetadata);
  }

  @Override
  public void completeClassificationsMetadataWithDeweyCodes(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    if (depositDoc.getAcademicStructures() != null && !depositDoc.getAcademicStructures().getAcademicStructure().isEmpty()) {
      Set<String> deweyCodes = new HashSet<>();
      for (AcademicStructure academicStructure : depositDoc.getAcademicStructures().getAcademicStructure()) {
        Structure structure = this.structureService.findByCodeStruct(academicStructure.getCode());
        if (structure != null && !StringTool.isNullOrEmpty(structure.getDewey())) {
          deweyCodes.add(structure.getDewey());
        }
      }

      if (!deweyCodes.isEmpty()) {
        if (depositDoc.getClassifications() == null) {
          depositDoc.setClassifications(new Classifications());
        }
        for (String deweyCode : deweyCodes) {
          Classification classification = new Classification();
          classification.setCode("Dewey");
          classification.setItem(deweyCode);
          boolean existingDewey = depositDoc.getClassifications().getClassification().stream().anyMatch(cl -> cl.getItem().equals(deweyCode));
          if (!existingDewey) {
            depositDoc.getClassifications().getClassification().add(classification);
          }
        }

        String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
        publication.setMetadata(xmlMetadata);
      }
    }
  }

  @Override
  public void completeStructureResId(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    if (depositDoc.getAcademicStructures() != null && !depositDoc.getAcademicStructures().getAcademicStructure().isEmpty()) {
      for (AcademicStructure academicStructure : depositDoc.getAcademicStructures().getAcademicStructure()) {
        if (academicStructure != null) {
          Structure structure = this.structureService.findByCodeStruct(academicStructure.getCode());
          if (structure != null) {
            academicStructure.setResId(structure.getResId());
            academicStructure.setName(structure.getName());
          }
        }
      }
      String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
      publication.setMetadata(xmlMetadata);
    }
  }

  @Override
  public void completeResearchGroupResId(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    if (depositDoc.getGroups() != null && !depositDoc.getGroups().getGroup().isEmpty()) {
      for (Group group : depositDoc.getGroups().getGroup()) {
        if (group != null) {
          if (StringTool.isNullOrEmpty(group.getResId())) {
            //TODO: if this message never appears in logs, the code below may be suppressed
            log.info("ResearchGroup doesn't contain any resId");

            // If resId is null try to find it
            List<ResearchGroup> rgList = this.researchGroupService.findByName(group.getName());
            if (!rgList.isEmpty() && rgList.get(0) != null) {
              group.setResId(rgList.get(0).getResId());
              if (!StringTool.isNullOrEmpty(rgList.get(0).getCode())) {
                group.setCode(new BigInteger(rgList.get(0).getCode()));
              }
            }
          } else {
            log.info("ResearchGroup already contains a resId");
            // If resId is already set, find Research group to complete its code
            Optional<ResearchGroup> researchGroupOpt = this.researchGroupService.findById(group.getResId());
            if (researchGroupOpt.isPresent()) {
              ResearchGroup researchGroup = researchGroupOpt.get();
              if (!StringTool.isNullOrEmpty(researchGroup.getCode())) {
                group.setCode(new BigInteger(researchGroup.getCode()));
              }
            }
          }
        }
      }
      String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
      publication.setMetadata(xmlMetadata);
    }
  }

  @Override
  public void completeContributorsCnIndividuFromOrcid(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
    if (depositDoc.getContributors() != null && !depositDoc.getContributors().getContributorOrCollaboration().isEmpty()) {
      for (Serializable serializable : depositDoc.getContributors().getContributorOrCollaboration()) {
        if (serializable instanceof Contributor metadataContributor) {
          String orcid = metadataContributor.getOrcid();
          if (!StringTool.isNullOrEmpty(orcid) && StringTool.isNullOrEmpty(metadataContributor.getCnIndividu())) {
            // Look in contributors table if a contributor with same ORCID already exists
            ch.unige.aou.model.publication.Contributor existingContributor = this.contributorService.findByOrcid(orcid);
            if (existingContributor != null) {
              // If such a contributor already exists, look in UNIGE person search service for the official form of its name as it is the version we store
              // Note: we store ORCID only for UNIGE contributors, so the cnIndividu always has a value at this point
              Optional<UnigePersonDTO> unigePersonOpt = this.unigePersonSearchService.searchByCnIndividu(existingContributor.getCnIndividu());
              if (!unigePersonOpt.isEmpty()) {
                metadataContributor.setCnIndividu(existingContributor.getCnIndividu());
                metadataContributor.setFirstname(unigePersonOpt.get().getFirstname());
                metadataContributor.setLastname(unigePersonOpt.get().getLastname());
                metadataContributor.setStructure(unigePersonOpt.get().getStructure());
              } else {
                log.warn(
                        "The ORCID " + orcid + " corresponds to the contributor " + existingContributor.getFullName() + " having the cnIndividu "
                                + existingContributor.getCnIndividu()
                                + ", but this cnIndividu cannot be found in the UNIGE person search service");
              }
            }
          }
        }
      }
      String xmlMetadata = this.serializeDepositDocToXml(depositDoc);
      publication.setMetadata(xmlMetadata);
    }
  }

  @Override
  protected void formatDepositDocDatesToFrontendFormat(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getDates() != null && depositDoc.getDates().getDate() != null) {
      List<DateWithType> dates = depositDoc.getDates().getDate();
      for (DateWithType date : dates) {
        if (!StringTool.isNullOrEmpty(date.getContent())) {
          String dateStr = this.updateDateFromMetadataToFrontendFormat(date.getContent());
          date.setContent(dateStr);
        }
      }
    }
  }

  @Override
  protected void formatDepositDocDatesToMetadataFormat(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getDates() != null && depositDoc.getDates().getDate() != null) {
      List<DateWithType> dates = depositDoc.getDates().getDate();
      for (DateWithType date : dates) {
        if (!StringTool.isNullOrEmpty(date.getContent())) {
          String dateStr = this.updateDateFromFrontendToMetadataFormat(date.getContent());
          date.setContent(dateStr);
        }
      }
    }
  }

  @Override
  public String serializeDepositDocToXml(Object depositDocObject) {
    return this.depositDocAdapter.serializeDepositDocToXml(depositDocObject);
  }

  @Override
  public Object createDepositDocObjectFromXml(String xmlData) {
    if (xmlData == null) {
      throw new SolidifyValidationException(new ValidationError("XmlData is null. Unable to deserialize XML to DepositDoc."));
    }
    return JAXB.unmarshal(new StringReader(xmlData), DepositDoc.class);
  }

  @Override
  public void replaceIdsByTextValues(Object depositDocObj) {
    super.replaceIdsByTextValues(depositDocObj);

    /**
     * Languages
     */
    List<String> languageIds = this.getLanguages(depositDocObj);
    if (languageIds != null) {
      List<String> languageCodes = new ArrayList<>();
      for (String languageId : languageIds) {
        if (!StringTool.isNullOrEmpty(languageId)) {
          Optional<LabeledLanguage> language = this.labeledLanguageService.findById(languageId);
          if (language.isPresent()) {
            languageCodes.add(language.get().getCode());
          } else {
            languageCodes.add(languageId);
          }
        } else {
          languageCodes.add(""); // add empty string, sometimes the front can send empty language
        }
      }
      this.setLanguages(depositDocObj, languageCodes);
    }
  }

  @Override
  public void replaceTextValuesByIds(Object depositDocObj) {
    super.replaceTextValuesByIds(depositDocObj);

    /**
     * Languages
     */
    List<String> languageStrList = this.getLanguages(depositDocObj);
    if (languageStrList != null) {
      List<String> languageIds = new ArrayList<>();
      for (String languageStr : languageStrList) {
        LabeledLanguage language = this.labeledLanguageService.findByCode(languageStr);
        if (language != null) {
          languageIds.add(language.getResId());
        } else {
          languageIds.add(languageStr);
        }
      }
      this.setLanguages(depositDocObj, languageIds);
    }
  }

  @Override
  public String getPmcIdFromPublication(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
    return this.getPmcId(depositDoc);
  }

  @Override
  public void addResearchGroup(ResearchGroup researchGroup, Publication publication) {
    DepositDoc depositDoc = this.createDepositDocFromJsonFormData(publication.getFormData());
    Group group = new Group();
    group.setResId(researchGroup.getResId());
    group.setName(researchGroup.getName());
    group.setCode(researchGroup.getCode() != null ? new BigInteger(researchGroup.getCode()) : null);

    this.ensureGroupExists(depositDoc);
    List<Group> groups = depositDoc.getGroups().getGroup();
    if (groups.stream().noneMatch(g -> g.getResId().equals(researchGroup.getResId()))) {
      groups.add(group);
    }

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void addStructure(Structure structure, Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    // create a new AcademicStructure object
    AcademicStructure academicStructure = new AcademicStructure();
    academicStructure.setResId(structure.getResId());
    academicStructure.setCode(structure.getCodeStruct());
    academicStructure.setName(structure.getName());

    this.ensureAcademicStructuresExists(depositDoc);

    List<AcademicStructure> academicStructures = depositDoc.getAcademicStructures().getAcademicStructure();
    if (academicStructures.stream().noneMatch(acst -> acst.getResId().equals(structure.getResId()))) {
      academicStructures.add(academicStructure);
    }

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void removeStructure(Structure structure, Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    List<AcademicStructure> academicStructures = depositDoc.getAcademicStructures().getAcademicStructure();
    List<AcademicStructure> academicStructuresToRemove = new ArrayList<>();
    for (AcademicStructure academicStructure : academicStructures) {
      if (academicStructure.getResId().equals(structure.getResId())) {
        academicStructuresToRemove.add(academicStructure);
      }
    }

    for (AcademicStructure academicStructureToRemove : academicStructuresToRemove) {
      academicStructures.remove(academicStructureToRemove);
    }

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void addLanguage(LabeledLanguage labeledLanguage, Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    this.ensureLanguagesExists(depositDoc);

    List<String> languages = depositDoc.getLanguages().getLanguage();
    if (languages.stream().noneMatch(lang -> lang.equals(labeledLanguage.getCode()))) {
      depositDoc.getLanguages().getLanguage().add(labeledLanguage.getCode());
    }

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public boolean isCnIndividuInContributors(Publication publication, String cnIndividu) {
    if (StringTool.isNullOrEmpty(cnIndividu)) {
      return false;
    }

    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
    List<AbstractContributor> contributors = this.getContributors(depositDoc);
    return contributors.stream().anyMatch(abstractContributor -> abstractContributor instanceof ContributorDTO ctd
            && cnIndividu.equals(ctd.getCnIndividu()));
  }

  @Override
  public void removeCnIndividuFromContributors(Publication publication, String cnIndividu) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    // loop on all contributors and replace cnIndividu by null if it matches
    List<AbstractContributor> contributors = this.getContributors(depositDoc);
    for (AbstractContributor abstractContributor : contributors) {
      if (abstractContributor instanceof ContributorDTO contributorDTO) {
        if (contributorDTO.getCnIndividu() != null && contributorDTO.getCnIndividu().equals(cnIndividu)) {
          contributorDTO.setCnIndividu(null);
        }
      }
    }
    this.setContributors(depositDoc, contributors);
    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void updatePublicationListWithContributorDisplayName(Publication publication, String cnIndividu, String lastname, String firstname,
          String[] publicationIds) {
    Optional<UnigePersonDTO> unigePersonOpt = this.unigePersonSearchService.searchByCnIndividu(cnIndividu);
    if (unigePersonOpt.isEmpty()) {
      throw new SolidifyRuntimeException("unable to find UNIGE contributor with cnIndividu " + cnIndividu);
    }
    UnigePersonDTO unigePersonDTO = unigePersonOpt.get();

    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());
    List<AbstractContributor> contributors = this.getContributors(depositDoc);
    for (AbstractContributor abstractContributor : contributors) {
      if (abstractContributor instanceof ContributorDTO contributorDTO) {
        if (contributorDTO.getCnIndividu() != null && contributorDTO.getCnIndividu().equals(cnIndividu)) {
          // update display names if different
          if (!Objects.equals(contributorDTO.getFirstName(), firstname)) {
            contributorDTO.setFirstName(firstname);
          }
          if (!Objects.equals(contributorDTO.getLastName(), lastname)) {
            contributorDTO.setLastName(lastname);
          }

          // Check if the current form of the name in UNIGE database is already in other names. If not add it.
          boolean mustAddCurrentUnigeName = true;
          for (OtherNameDTO otherNameDTO : contributorDTO.getOtherNames()) {
            if (Objects.equals(otherNameDTO.getFirstName(), unigePersonDTO.getFirstname()) && Objects.equals(otherNameDTO.getLastName(),
                    unigePersonDTO.getLastname())) {
              mustAddCurrentUnigeName = false;
              break;
            }
          }

          if (mustAddCurrentUnigeName) {
            contributorDTO.getOtherNames().add(new OtherNameDTO(unigePersonDTO.getFirstname(), unigePersonDTO.getLastname()));
          }
        }
      }
    }
    this.setContributors(depositDoc, contributors);
    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  private void ensureGroupExists(DepositDoc depositDoc) {
    if (depositDoc.getGroups() == null) {
      depositDoc.setGroups(this.factory.createGroups());
    }
  }

  private void ensureAcademicStructuresExists(DepositDoc depositDoc) {
    if (depositDoc.getAcademicStructures() == null) {
      depositDoc.setAcademicStructures(this.factory.createAcademicStructures());
    }
  }

  private void ensureLanguagesExists(DepositDoc depositDoc) {
    if (depositDoc.getLanguages() == null) {
      depositDoc.setLanguages(this.factory.createLanguages());
    }
  }

  /*******************************************************************************************/

  @Override
  public void cleanMetadata(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    this.cleanTitle(depositDoc);
    this.cleanOriginalTitle(depositDoc);
    this.cleanMandator(depositDoc);
    this.cleanPublisher(depositDoc);
    this.cleanEdition(depositDoc);
    this.cleanAward(depositDoc);
    this.cleanAouCollection(depositDoc);
    this.cleanDoctorEmail(depositDoc);
    this.cleanDoctorAddress(depositDoc);
    this.cleanPublisherVersionUrl(depositDoc);
    this.cleanIdentifiers(depositDoc);
    this.cleanAbstracts(depositDoc);
    this.cleanNote(depositDoc);
    this.cleanKeywords(depositDoc);
    this.cleanCollections(depositDoc);
    this.cleanFundings(depositDoc);
    this.cleanDatasets(depositDoc);
    this.cleanDiscipline(depositDoc);
    this.cleanLinks(depositDoc);
    this.cleanCorrections(depositDoc);
    this.cleanPages(depositDoc);
    this.cleanContainerValues(depositDoc);
    this.cleanContributors(depositDoc);
    this.cleanClassifications(depositDoc);

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  @Override
  public void cleanMetadataToClone(Publication publication) {
    DepositDoc depositDoc = (DepositDoc) this.createDepositDocObjectFromXml(publication.getMetadata());

    this.clearIdentifiers(depositDoc);
    this.clearFiles(depositDoc);

    String metadata = this.serializeDepositDocToXml(depositDoc);
    publication.setMetadata(metadata);
  }

  private void cleanTitle(DepositDoc depositDoc) {
    if (depositDoc.getTitle() != null && !StringTool.isNullOrEmpty(depositDoc.getTitle().getContent())) {
      String value = depositDoc.getTitle().getContent();
      value = CleanTool.cleanTitle(value);
      depositDoc.getTitle().setContent(value);
    }
  }

  private void cleanOriginalTitle(DepositDoc depositDoc) {
    if (depositDoc.getOriginalTitle() != null && !StringTool.isNullOrEmpty(depositDoc.getOriginalTitle().getContent())) {
      String value = depositDoc.getOriginalTitle().getContent();
      value = CleanTool.cleanTitle(value);
      depositDoc.getOriginalTitle().setContent(value);
    }
  }

  private void cleanMandator(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getMandator())) {
      String value = depositDoc.getMandator();
      value = CleanTool.cleanString(value);
      depositDoc.setMandator(value);
    }
  }

  private void cleanPublisher(DepositDoc depositDoc) {
    if (depositDoc.getPublisher() != null) {
      Publisher publisher = depositDoc.getPublisher();
      for (JAXBElement<String> element : publisher.getNameOrPlace()) {
        element.setValue(CleanTool.cleanString(element.getValue()));
      }
    }
  }

  private void cleanContainerValues(DepositDoc depositDoc) {
    if (depositDoc.getContainer() != null) {
      this.cleanContainerTitle(depositDoc);
      this.cleanEditor(depositDoc);
      this.cleanVolume(depositDoc);
      this.cleanIssue(depositDoc);
      this.cleanSpecialIssue(depositDoc);
      this.cleanConferenceSubtitle(depositDoc);
      this.cleanConferenceDate(depositDoc);
      this.cleanConferencePlace(depositDoc);
    }
  }

  private void cleanContainerTitle(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getTitle())) {
      depositDoc.getContainer().setTitle(CleanTool.cleanString(depositDoc.getContainer().getTitle()));
    }
  }

  private void cleanEditor(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getEditor())) {
      depositDoc.getContainer().setEditor(CleanTool.cleanString(depositDoc.getContainer().getEditor()));
    }
  }

  private void cleanVolume(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getVolume())) {
      depositDoc.getContainer().setVolume(CleanTool.cleanString(depositDoc.getContainer().getVolume()));
    }
  }

  private void cleanIssue(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getIssue())) {
      depositDoc.getContainer().setIssue(CleanTool.cleanString(depositDoc.getContainer().getIssue()));
    }
  }

  private void cleanSpecialIssue(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getSpecialIssue())) {
      depositDoc.getContainer().setSpecialIssue(CleanTool.cleanString(depositDoc.getContainer().getSpecialIssue()));
    }
  }

  private void cleanConferenceSubtitle(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getConferenceSubtitle())) {
      depositDoc.getContainer().setConferenceSubtitle(CleanTool.cleanString(depositDoc.getContainer().getConferenceSubtitle()));
    }
  }

  private void cleanConferenceDate(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getConferenceDate())) {
      depositDoc.getContainer().setConferenceDate(CleanTool.cleanString(depositDoc.getContainer().getConferenceDate()));
    }
  }

  private void cleanConferencePlace(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getContainer().getConferencePlace())) {
      depositDoc.getContainer().setConferencePlace(CleanTool.cleanString(depositDoc.getContainer().getConferencePlace()));
    }
  }

  private void cleanEdition(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getEdition())) {
      String value = depositDoc.getEdition();
      value = CleanTool.cleanString(value);
      depositDoc.setEdition(value);
    }
  }

  private void cleanAward(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getAward())) {
      String value = depositDoc.getAward();
      value = CleanTool.cleanString(value);
      depositDoc.setAward(value);
    }
  }

  private void cleanAouCollection(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getAouCollection())) {
      String value = depositDoc.getAouCollection();
      value = CleanTool.cleanString(value);
      depositDoc.setAouCollection(value);
    }
  }

  private void cleanDoctorEmail(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getDoctorEmail())) {
      String value = depositDoc.getDoctorEmail();
      value = CleanTool.cleanString(value);
      depositDoc.setDoctorEmail(value);
    }
  }

  private void cleanDoctorAddress(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getDoctorAddress())) {
      String value = depositDoc.getDoctorAddress();
      value = CleanTool.trim(value); // remove starting and trailing whitespace
      value = CleanTool.removeDoubleSpaces(value);
      depositDoc.setDoctorAddress(value);
    }
  }

  private void cleanPublisherVersionUrl(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getPublisherVersionUrl())) {
      String value = depositDoc.getPublisherVersionUrl();
      value = CleanTool.cleanString(value);
      depositDoc.setPublisherVersionUrl(value);
    }
  }

  private void cleanIdentifiers(DepositDoc depositDoc) {
    if (depositDoc.getIdentifiers() != null) {
      depositDoc.getIdentifiers().setDoi(CleanTool.cleanString(depositDoc.getIdentifiers().getDoi()));
      depositDoc.getIdentifiers().setArxiv(CleanTool.cleanString(depositDoc.getIdentifiers().getArxiv()));
      depositDoc.getIdentifiers().setIssn(CleanTool.cleanString(depositDoc.getIdentifiers().getIssn()));
      depositDoc.getIdentifiers().setDblp(CleanTool.cleanString(depositDoc.getIdentifiers().getDblp()));
      depositDoc.getIdentifiers().setIsbn(CleanTool.cleanStringAndHyphens(depositDoc.getIdentifiers().getIsbn(), true));
      depositDoc.getIdentifiers().setLocalNumber(CleanTool.cleanString(depositDoc.getIdentifiers().getLocalNumber()));
      depositDoc.getIdentifiers().setMmsid(CleanTool.cleanString(depositDoc.getIdentifiers().getMmsid()));
      depositDoc.getIdentifiers().setPmcid(CleanTool.cleanString(depositDoc.getIdentifiers().getPmcid()));
      depositDoc.getIdentifiers().setRepec(CleanTool.cleanString(depositDoc.getIdentifiers().getRepec()));
      depositDoc.getIdentifiers().setUrn(CleanTool.cleanString(depositDoc.getIdentifiers().getUrn()));
    }
  }

  private void cleanAbstracts(DepositDoc depositDoc) {
    if (depositDoc.getAbstracts() != null) {
      for (Text depositAbstract : depositDoc.getAbstracts().getAbstract()) {
        depositAbstract.setContent(CleanTool.cleanAbstract(depositAbstract.getContent()));
      }
    }
  }

  private void cleanNote(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getNote())) {
      String value = depositDoc.getNote();
      value = CleanTool.cleanString(value);
      depositDoc.setNote(value);
    }
  }

  private void cleanKeywords(DepositDoc depositDoc) {
    if (depositDoc.getKeywords() != null) {
      int i = 0;
      for (String keyword : depositDoc.getKeywords().getKeyword()) {
        depositDoc.getKeywords().getKeyword().set(i++, CleanTool.cleanString(keyword));
      }
    }
  }

  private void cleanCollections(DepositDoc depositDoc) {
    if (depositDoc.getCollections() != null) {
      for (Collection collection : depositDoc.getCollections().getCollection()) {
        collection.setName(CleanTool.cleanString(collection.getName()));
        collection.setNumber(CleanTool.cleanString(collection.getNumber()));
      }
    }
  }

  private void cleanFundings(DepositDoc depositDoc) {
    if (depositDoc.getFundings() != null) {
      for (Funding funding : depositDoc.getFundings().getFunding()) {
        funding.setFunder(CleanTool.cleanString(funding.getFunder()));
        funding.setName(CleanTool.cleanString(funding.getName()));
        funding.setCode(CleanTool.cleanString(funding.getCode()));
        funding.setAcronym(CleanTool.cleanString(funding.getAcronym()));
        funding.setJurisdiction(CleanTool.cleanString(funding.getJurisdiction()));
        funding.setProgram(CleanTool.cleanString(funding.getProgram()));
      }
    }
  }

  private void cleanDatasets(DepositDoc depositDoc) {
    if (depositDoc.getDatasets() != null) {
      int i = 0;
      for (String url : depositDoc.getDatasets().getUrl()) {
        depositDoc.getDatasets().getUrl().set(i++, CleanTool.cleanString(url));
      }
    }
  }

  private void cleanDiscipline(DepositDoc depositDoc) {
    if (!StringTool.isNullOrEmpty(depositDoc.getDiscipline())) {
      depositDoc.setDiscipline(CleanTool.trim(depositDoc.getDiscipline()));
    }
  }

  private void cleanLinks(DepositDoc depositDoc) {
    if (depositDoc.getLinks() != null) {
      for (Link link : depositDoc.getLinks().getLink()) {
        link.getDescription().setContent(CleanTool.cleanString(link.getDescription().getContent()));
        link.setTarget(CleanTool.cleanString(link.getTarget()));
      }
    }
  }

  private void cleanCorrections(DepositDoc depositDoc) {
    if (depositDoc.getCorrections() != null) {
      for (Correction correction : depositDoc.getCorrections().getCorrection()) {
        correction.setNote(CleanTool.cleanString(correction.getNote()));
        correction.setDoi(CleanTool.cleanString(correction.getDoi()));
        correction.setPmid(CleanTool.cleanString(correction.getPmid()));
      }
    }
  }

  private void cleanPages(DepositDoc depositDoc) {
    if (depositDoc.getPages() != null) {
      for (JAXBElement<String> element : depositDoc.getPages().getPagingOrOther()) {
        element.setValue(CleanTool.cleanString(element.getValue()));
        if (element.getName().getLocalPart().equals("paging")) {
          element.setValue(CleanTool.cleanPaging(element.getValue()));
        }
      }
    }
  }

  private void cleanContributors(DepositDoc depositDoc) {
    if (depositDoc.getContributors() != null) {
      for (Serializable serializable : depositDoc.getContributors().getContributorOrCollaboration()) {
        if (serializable instanceof Contributor contributor) {
          contributor.setFirstname(CleanTool.cleanString(contributor.getFirstname()));
          contributor.setLastname(CleanTool.cleanString(contributor.getLastname()));
          contributor.setEmail(CleanTool.cleanString(contributor.getEmail()));
          contributor.setInstitution(CleanTool.cleanString(contributor.getInstitution()));
          contributor.setOrcid(CleanTool.cleanString(contributor.getOrcid()));
          contributor.setCnIndividu(CleanTool.cleanString(contributor.getCnIndividu()));
          if (contributor.getOtherNames() != null) {
            for (OtherName otherName : contributor.getOtherNames().getOtherName()) {
              otherName.setFirstname(CleanTool.cleanString(otherName.getFirstname()));
              otherName.setLastname(CleanTool.cleanString(otherName.getLastname()));
            }
          }
        } else {
          Collaboration collaboration = (Collaboration) serializable;
          collaboration.setName(CleanTool.cleanString(collaboration.getName()));
        }
      }
    }
  }

  private void cleanClassifications(DepositDoc depositDoc) {
    if (depositDoc.getClassifications() != null) {
      for (Classification classification : depositDoc.getClassifications().getClassification()) {
        if ("JEL".equals(classification.getCode())) {
          classification.setItem(classification.getItem().toUpperCase());
        }
      }
    }
  }

  private void clearIdentifiers(DepositDoc depositDoc) {
    if (depositDoc.getIdentifiers() != null) {
      depositDoc.getIdentifiers().setDoi(null);
      depositDoc.getIdentifiers().setArxiv(null);
      depositDoc.getIdentifiers().setPmid(null);
      depositDoc.getIdentifiers().setPmcid(null);
    }
  }

  private void clearFiles(DepositDoc depositDoc) {
    if (depositDoc.getFiles() != null && depositDoc.getFiles().getFile() != null) {
      depositDoc.getFiles().getFile().clear();
      depositDoc.setFiles(null);
    }
  }

  /*******************************************************************************************/

  private ObjectMapper getJsonFormDeserializerMapper() {
    SimpleModule module = new SimpleModule();
    module.addDeserializer(Abstracts.class, new AbstractsDeserializer());
    module.addDeserializer(AcademicStructures.class, new AcademicsStructuresDeserializer());
    module.addDeserializer(Classifications.class, new ClassificationsDeserializer());
    module.addDeserializer(Collections.class, new CollectionsDeserializer());
    module.addDeserializer(Container.class, new ContainerDeserializer());
    module.addDeserializer(Contributors.class, new ContributorsDeserializer());
    module.addDeserializer(Corrections.class, new CorrectionsDeserializer());
    module.addDeserializer(Datasets.class, new DatasetsDeserializer());
    module.addDeserializer(Dates.class, new DatesDeserializer());
    module.addDeserializer(DateWithType.class, new DateWithTypeDeserializer());
    module.addDeserializer(DepositIdentifiers.class, new DepositIdentifiersDeserializer());
    module.addDeserializer(Files.class, new FilesDeserializer());
    module.addDeserializer(Fundings.class, new FundingsDeserializer());
    module.addDeserializer(Groups.class, new GroupsDeserializer());
    module.addDeserializer(Keywords.class, new KeywordsDeserializer());
    module.addDeserializer(Languages.class, new LanguagesDeserializer());
    module.addDeserializer(Links.class, new LinksDeserializer());
    module.addDeserializer(Pages.class, new PagesDeserializer());
    module.addDeserializer(Publisher.class, new PublisherDeserializer());
    module.addDeserializer(TextLang.class, new TextLangDeserializer());
    module.addDeserializer(Text.class, new TextDeserializer());

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(module);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper;
  }

  private ObjectMapper getJsonFormSerializerMapper() {
    SimpleModule module = new SimpleModule();
    module.addSerializer(Abstracts.class, new AbstractsSerializer());
    module.addSerializer(AcademicStructures.class, new AcademicStructuresSerializer());
    module.addSerializer(AuthorRole.class, new AuthorRoleSerializer());
    module.addSerializer(Classifications.class, new ClassificationsSerializer());
    module.addSerializer(Collections.class, new CollectionsSerializer());
    module.addSerializer(Contributors.class, new ContributorsSerializer());
    module.addSerializer(Corrections.class, new CorrectionsSerializer());
    module.addSerializer(Datasets.class, new DatasetsSerializer());
    module.addSerializer(Dates.class, new DatesSerializer());
    module.addSerializer(DateWithType.class, new DateWithTypeSerializer());
    module.addSerializer(DepositDoc.class, new DepositDocSerializer());
    module.addSerializer(DepositIdentifiers.class, new DepositIdentifiersSerializer());
    module.addSerializer(Files.class, new FilesSerializer());
    module.addSerializer(Fundings.class, new FundingsSerializer());
    module.addSerializer(Groups.class, new GroupsSerializer());
    module.addSerializer(Keywords.class, new KeywordsSerializer());
    module.addSerializer(Languages.class, new LanguagesSerializer());
    module.addSerializer(Links.class, new LinksSerializer());
    module.addSerializer(Pages.class, new PagesSerializer());
    module.addSerializer(Publisher.class, new PublisherSerializer());
    module.addSerializer(Text.class, new TextSerializer());
    module.addSerializer(TextLang.class, new TextLangSerializer());

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(module);
    mapper.registerModule(new JavaTimeModule());
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    return mapper;
  }

  private File convertDocumentFileToXmlFile(DocumentFile documentFile) {
    File file = new File();
    file.setResId(documentFile.getResId());
    file.setType(FileType.fromValue(documentFile.getDocumentFileType().getValue()));
    file.setAccessLevel(AccessLevel.fromValue(
            documentFile.getAccessLevel().toString().substring(0, 1).toUpperCase() + documentFile.getAccessLevel().toString().substring(1)
                    .toLowerCase()));
    file.setName(documentFile.getFileName());
    if (documentFile.getFileSize() != null) {
      file.setSize(BigInteger.valueOf(documentFile.getFileSize()));
    }
    file.setChecksum(documentFile.getChecksum());
    file.setMimetype(documentFile.getMimetype());
    if (!StringTool.isNullOrEmpty(documentFile.getLabel())) {
      file.setLabel(documentFile.getLabel());
    }
    if (documentFile.getLicense() != null) {
      file.setLicense(documentFile.getLicense().getOpenLicenseId());
    }
    if (documentFile.getEmbargoEndDate() != null) {
      Embargo embargo = new Embargo();
      embargo.setAccessLevel(AccessLevel.fromValue(
              documentFile.getEmbargoAccessLevel().toString().substring(0, 1).toUpperCase() + documentFile.getEmbargoAccessLevel().toString()
                      .substring(1).toLowerCase()));
      embargo.setEndDate(documentFile.getEmbargoEndDate());
      file.setEmbargo(embargo);
    }
    return file;
  }
}
