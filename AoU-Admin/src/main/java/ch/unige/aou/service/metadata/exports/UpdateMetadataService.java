/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - UpdateMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata.exports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouMetadataValidationException;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.message.PublicationMessage;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.xml.list_deposits.Publications;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.metadata.PublicationsAdapter;

@Service
@ConditionalOnBean(AdminController.class)
public class UpdateMetadataService {
  private static final Logger log = LoggerFactory.getLogger(UpdateMetadataService.class);

  private final String exportFolder;
  private final Integer numberOfErrorsToSendForEmailOfBatchCorrections;
  private final PublicationService publicationService;
  private final MetadataService metadataService;
  private final PublicationsAdapter publicationsAdapter;
  private final String tempFolder;

  public UpdateMetadataService(AouProperties aouProperties, PublicationService publicationService, MetadataService metadataService,
          PublicationsAdapter publicationsAdapter) {
    this.publicationService = publicationService;
    this.metadataService = metadataService;
    this.publicationsAdapter = publicationsAdapter;
    this.exportFolder = aouProperties.getExportPublicationsFolder();
    this.numberOfErrorsToSendForEmailOfBatchCorrections = aouProperties.getBatchCorrections().getNumberOfErrorToSendForEmail();
    this.tempFolder = aouProperties.getTempLocation(aouProperties.getPublicationsFolder());
  }

  @Async
  public void exportMetadata(List<String> ids, String fileId) {
    Publications publications = new Publications();
    for (String archiveId : ids) {
      Optional<Publication> publicationOpt = this.publicationService.findByArchiveId(archiveId);
      if (publicationOpt.isPresent()) {
        Publication publication = publicationOpt.get();
        final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
        final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
        this.publicationsAdapter.addPublication(publications, depositDoc, archiveId, publication.getUpdateTime());
      }
    }
    String metadata = this.publicationsAdapter.serializePublicationsToXml(publications);
    String xmlPath = this.getFilePath(fileId);
    //write the file in file system
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(xmlPath))) {
      writer.write(metadata);
    } catch (IOException e) {
      log.error("Error writing Archives xml to file system");
    }
  }

  public Path checkIfFilePathExists(String id) {
    String filePathStr = this.getFilePath(id);
    Path filePath = Paths.get(filePathStr);
    if (!FileTool.checkFile(filePath)) {
      throw new SolidifyRuntimeException("Export file with id: " + id + " is not ready");
    }
    return filePath;
  }

  public String getFilePath(String id) {
    return this.exportFolder + File.separator + id + SolidifyConstants.XML_EXT;
  }

  @Transactional
  @Async
  public void importPublications(String xmlString, User userImporting) {
    // First validate xml and validation rules for each publication
    // In case of errors, they should be sent by email
    try {
      Map<String, String> publicationsErrors = this.verifyPublicationsFromXml(xmlString);
      if (!publicationsErrors.isEmpty()) {
        this.sendEmailWithListError(publicationsErrors, userImporting.getEmail());
        log.info("Email with {} validation errors has been send", publicationsErrors.size());
        return;
      }
      // Change each metadata from publications and save them
      int total = this.updatePublicationsMetadata(xmlString, userImporting.getExternalUid());
      this.sendEmailWithListPublicationsUpdated(total, userImporting.getEmail());
      log.info("Email with {} publication changed", total);

    } catch (IOException | SolidifyRuntimeException e) {
      this.sendEmailWithExceptionMessage(String.valueOf(e.getCause()), userImporting.getEmail());
      log.info("Exception error: {}", e.getMessage());
      log.info("Email with validation error from xml {}", String.valueOf(e.getCause()));
    }
  }

  private int updatePublicationsMetadata(String xmlString, String personImporting) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
    Publications publications = this.publicationsAdapter.createPublicationsFromXml(xmlString);
    for (ch.unige.aou.model.xml.list_deposits.Publication publicationXml : publications.getPublication()) {
      Optional<Publication> publicationOpt = this.publicationService.findByArchiveId(publicationXml.getArchiveId());
      if (publicationOpt.isPresent()) {
        Publication publication = publicationOpt.get();
        // First check again if the publication has been modified since its export.
        // In case of large batch update file, the process can take a long time, increasing the risk of this happening.
        if (!publicationXml.getLastUpdate().isBefore(publication.getUpdateTime())) {
          Publication.PublicationStatus currentStatus = publication.getStatus();
          String publicationMetadata = metadataExtractor.serializeDepositDocToXml(publicationXml.getDepositDoc());
          publication.setMetadata(publicationMetadata);
          publication.getLastUpdate().setWho(personImporting);

          switch (currentStatus) {
            case COMPLETED -> {
              publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL);
              publication.setStatus(Publication.PublicationStatus.UPDATE_IN_BATCH);
              this.publicationService.save(publication);
              SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMessage(publication.getResId()));
            }
            default -> {
              publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
              publication.setStatus(Publication.PublicationStatus.UPDATE_IN_BATCH);
              this.publicationService.save(publication);
              publication.setStatus(currentStatus);
              this.publicationService.save(publication);
            }
          }
          log.info("Metadata of publication with resId {} ({}) has been successfully modified", publication.getResId(),
                  publication.getArchiveId());
        } else {
          log.warn("Metadata of publication with resId {} ({}) has not been modified because it has been modified since it was exported",
                  publication.getResId(), publication.getArchiveId());
        }
      } else {
        throw new SolidifyRuntimeException("Publication with archiveId " + publicationXml.getArchiveId() + " not found");
      }
    }
    return publications.getPublication().size();
  }

  public Map<String, String> verifyPublicationsFromXml(String xmlString) throws IOException {
    // First validate XML
    log.info("Validation of xml in progress");
    this.publicationsAdapter.validate(xmlString, this.tempFolder);
    log.info("Validation of xml done");

    // Check validation rules from each publication
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
    Publications publications = this.publicationsAdapter.createPublicationsFromXml(xmlString);

    Map<String, String> validationErrors = new HashMap<>();

    for (ch.unige.aou.model.xml.list_deposits.Publication publicationXml : publications.getPublication()) {
      Optional<Publication> publicationOpt = this.publicationService.findByArchiveId(publicationXml.getArchiveId());
      if (publicationOpt.isEmpty()) {
        throw new SolidifyRuntimeException("The archiveID " + publicationXml.getArchiveId() + " does not exits in the database");
      }
      String publicationMetadata = metadataExtractor.serializeDepositDocToXml(publicationXml.getDepositDoc());
      Publication publication = new Publication();
      publication.setResId(publicationOpt.get().getResId()); // Needed to set it to avoid duplicates validation rules
      publication.setMetadata(publicationMetadata);
      publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL);
      metadataExtractor.setPropertiesFromMetadata(publication);

      // First check if Publication has been updated since it has been exported
      if (publicationXml.getLastUpdate().isBefore(publicationOpt.get().getUpdateTime())) {
        // Publication has been updated after it was exported -> we don't update it to not lose changes made in the meantime
        validationErrors.put(publicationXml.getArchiveId(), "The publication has been modified after it was exported");
        log.info("Publication with resId {} ({}) has been updated after it was exported for batch update (exported with: {}, current value: {})",
                publication.getResId(), publicationXml.getArchiveId(), publicationXml.getLastUpdate(), publicationOpt.get().getUpdateTime());
      } else {
        // If the check of last update is fine, check metadata content
        try {
          this.publicationService.validateMetadata(publication);
          log.info("Metadata of publication with resId {} ({}) has been successfully validated", publication.getResId(),
                  publicationXml.getArchiveId());

        } catch (AouMetadataValidationException ex) {
          validationErrors.put(publicationXml.getArchiveId(), ex.toString());
          log.info("Publication with resId {} ({}) contains the following validation error {}", publication.getResId(),
                  publicationXml.getArchiveId(), ex.toString());
        }
      }
      // if max error is reached, stop the verification here
      if (validationErrors.size() >= this.numberOfErrorsToSendForEmailOfBatchCorrections) {
        break;
      }
    }
    return validationErrors;
  }

  private void sendEmailWithExceptionMessage(String exceptionMessage, String email) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("exception", exceptionMessage);
    this.sendEmailWithParameters(parameters, email);
  }

  private void sendEmailWithListError(Map<String, String> listErrors, String email) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("listError", listErrors);
    this.sendEmailWithParameters(parameters, email);
  }

  private void sendEmailWithListPublicationsUpdated(int total, String email) {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("total", total);
    this.sendEmailWithParameters(parameters, email);
  }

  public void sendEmailWithParameters(Map<String, Object> parameters, String email) {
    SolidifyEventPublisher.getPublisher().publishEvent(
            new EmailMessage(email, EmailMessage.EmailTemplate.BATCH_UPDATES_REPORT, parameters));

  }
}
