/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - RockUnigePersonSearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.FieldValidationError;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.rest.UnigePersonDTO;

@Service
@Profile("person-search-rock")
@ConditionalOnBean(AdminController.class)
public class RockUnigePersonSearchService extends UnigePersonSearchService {
  private static final Logger log = LoggerFactory.getLogger(RockUnigePersonSearchService.class);

  private static final int PAGE_SIZE = 10;
  private static final String ROCK_FUNCTION_FIELD = "publicUniFunctionCurrentLabel";
  private static final String ROCK_STRUCTURE_CODE_FIELD = "structureCurrentAlphaCode";
  private static final String ROCK_STRUCTURE_LABEL_FIELD = "structureCurrentEditedLabel";
  private static final String EMPLOYEE_INFO_ENDPOINT = "/persons/employee-info";
  private static final String PERSONS_SEARCH_ENDPOINT = "/persons-search";
  private static final String REQUEST_BODY_TEMPLATE = "client_id=%s&scope=openid%%20allatclaims&username=%s&password=%s&grant_type=password";

  private final String serviceUrl;
  private final String tokenUrl;
  private final String clientId;
  private final String username;
  private final String password;

  private final MessageService messageService;

  private String accessToken;

  public RockUnigePersonSearchService(AouProperties aouProperties, MessageService messageService) {
    final AouProperties.PersonUnige personUnige = aouProperties.getMetadata().getSearch().getPersonUnige();
    this.serviceUrl = personUnige.getUrl();
    this.tokenUrl = personUnige.getTokenUrl();
    this.clientId = personUnige.getClientId();
    this.username = personUnige.getUsername();
    this.password = personUnige.getPassword();

    this.messageService = messageService;
  }

  @Scheduled(fixedRateString = "${aou.metadata.search.personUnige.tokenRefreshDelay:480000}")
  public void refreshAccessToken() {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    final RestTemplate restTemplate = new RestTemplate();
    final String requestBody = String.format(REQUEST_BODY_TEMPLATE, URLEncoder.encode(this.clientId, StandardCharsets.UTF_8),
            URLEncoder.encode(this.username, StandardCharsets.UTF_8), URLEncoder.encode(this.password, StandardCharsets.UTF_8));
    final HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);
    final ResponseEntity<String> response = restTemplate.exchange(this.tokenUrl, HttpMethod.POST, requestEntity, String.class);
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      try {
        this.accessToken = new ObjectMapper().readTree(response.getBody()).get("access_token").textValue();
      } catch (JsonProcessingException e) {
        throw new SolidifyRuntimeException("Unable to process answer", e);
      }
    } else {
      throw new SolidifyRuntimeException("Failed to get Access Token. Status : " + response.getStatusCode()
              + " Response body : " + response.getBody());
    }
  }

  @Override
  public Optional<UnigePersonDTO> getResultsFromExternalService(String cnIndividu) {
    if (StringTool.isNullOrEmpty(cnIndividu)) {
      throw new SolidifyValidationException(new ValidationError("CnIndividu should have 1 or more than one character"));
    }
    final String url = this.serviceUrl + PERSONS_SEARCH_ENDPOINT + "?entityId=" + cnIndividu;
    return this.getUnigePersonDTO(url);
  }

  @Override
  protected RestCollection<UnigePersonDTO> getResultsFromExternalService(String firstname, String lastname, Pageable pageable) {
    this.validateLastnameAndFirstname(lastname, firstname);

    // Pass URL without encoding query parameters as they are encoded later by using the restTemplate.exchange(url, ...) method
    String url = this.serviceUrl + PERSONS_SEARCH_ENDPOINT + "?size=" + pageable.getPageSize() + "&page=" + pageable.getPageNumber();
    if (!StringTool.isNullOrEmpty(firstname)) {
      url += "&firstname=" + firstname;
    }
    if (!StringTool.isNullOrEmpty(lastname)) {
      url += "&lastname=" + lastname;
    }

    return this.getUnigePersonDTORestCollection(url);
  }

  private void validateLastnameAndFirstname(String lastname, String firstname) {
    ValidationError validationError = new ValidationError();
    if (firstname != null && firstname.length() == 1) {
      validationError.getErrors()
              .add(new FieldValidationError("firstname", this.messageService.get("validation.rockUnigePersonSearch.firstname.minimum2chars")));
    }

    if (lastname != null && lastname.length() == 1) {
      validationError.getErrors()
              .add(new FieldValidationError("lastname", this.messageService.get("validation.rockUnigePersonSearch.lastname.minimum2chars")));
    }

    if (!validationError.getErrors().isEmpty()) {
      throw new SolidifyValidationException(validationError);
    }
  }

  private RestCollection<UnigePersonDTO> getUnigePersonDTORestCollection(String url) {
    final ResponseEntity<String> response = this.makeRestCall(url);

    if (response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
      return new RestCollection<>(Collections.emptyList(), new RestCollectionPage(0, 0, 0, 0));
    } else {
      final ObjectMapper mapper = new ObjectMapper();
      RestCollectionPage collectionPage;
      List<UnigePersonDTO> personList = new ArrayList<>();
      try {
        ObjectNode node = mapper.readValue(response.getBody(), ObjectNode.class);
        node.get("_data").forEach(element -> personList.add(new UnigePersonDTO(element)));
        int currentPage = node.get("_page").get("currentPage").asInt();
        int pageSize = node.get("_page").get("sizePage").asInt();
        int totalItems = node.get("_page").get("totalItems").asInt();
        int totalPages = node.get("_page").get("totalPages").asInt();
        collectionPage = new RestCollectionPage(currentPage, pageSize, totalPages, totalItems);
      } catch (JsonProcessingException e) {
        throw new SolidifyRuntimeException("Unable to process JSON response", e);
      }

      this.completePersonList(personList);
      return new RestCollection<>(personList, collectionPage);
    }
  }

  /**
   * Complete a list of person by adding the function of each person, if it exists
   * Only employee have a function
   * Use parallel REST calls to avoid high latency
   */
  private List<UnigePersonDTO> completePersonList(List<UnigePersonDTO> personList) {
    List<UnigePersonDTO> employeeList = new ArrayList<>();
    List<GetEmployeeFunctionCallable> callerList = new ArrayList<>();
    ExecutorService executor = Executors.newFixedThreadPool(PAGE_SIZE);
    for (UnigePersonDTO person : personList) {
      if (person.isEmployee()) {
        employeeList.add(person);
        callerList.add(new GetEmployeeFunctionCallable(person.getCnIndividu()));
      }
    }
    try {
      List<Future<EmployeeDTO>> resultList = executor.invokeAll(callerList);
      executor.shutdown();
      Iterator<Future<EmployeeDTO>> resultListIterator = resultList.iterator();
      Iterator<UnigePersonDTO> employeeListIterator = employeeList.iterator();
      while (resultListIterator.hasNext() && employeeListIterator.hasNext()) {
        final EmployeeDTO employeeDTO = resultListIterator.next().get();
        final UnigePersonDTO unigePersonDTO = employeeListIterator.next();
        unigePersonDTO.setFunction(employeeDTO.getFunction());
        unigePersonDTO.setStructure(employeeDTO.getStructure());
      }
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();  // set interrupt flag
      log.error("Interrupted while launching REST calls for completing the person list");
    } catch (ExecutionException e) {
      throw new SolidifyRuntimeException("Error during execution", e);
    }
    return personList;
  }

  private ResponseEntity<String> makeRestCall(String url) {
    try {
      final RestTemplate restTemplate = new RestTemplate();
      final HttpHeaders headers = new HttpHeaders();
      headers.setBearerAuth(this.accessToken);
      final HttpEntity<String> requestEntity = new HttpEntity<>(headers);
      final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
      if (!response.getStatusCode().is2xxSuccessful()) {
        log.error("Error when calling url {}", url);
        throw new SolidifyRuntimeException("Unable to access person search service, http status code is " + response.getStatusCode());
      }
      return response;
    } catch (RestClientException e) {
      log.error("Error when calling url {}", url);
      throw new SolidifyRuntimeException("Error when calling person search service", e);
    }
  }

  private Optional<UnigePersonDTO> getUnigePersonDTO(String url) {
    final ResponseEntity<String> response = this.makeRestCall(url);
    List<UnigePersonDTO> personList = new ArrayList<>();
    if (response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
      return Optional.empty();
    } else {
      final ObjectMapper mapper = new ObjectMapper();
      try {
        ObjectNode node = mapper.readValue(response.getBody(), ObjectNode.class);
        node.get("_data").forEach(element -> personList.add(new UnigePersonDTO(element)));
      } catch (JsonProcessingException e) {
        throw new SolidifyRuntimeException("Unable to process JSON response", e);
      }

      if (personList.size() > 1) { // we should have only one element, otherwise there is a problem with the service returning the result
        throw new SolidifyRuntimeException("Error: We should have only one result for search by cnIndividu");
      }

      this.completePersonList(personList);
      return !personList.isEmpty() ? Optional.of(personList.get(0)) : Optional.empty();
    }
  }

  /**
   * Allow to make parallel REST calls to get the function of a person
   */
  private class GetEmployeeFunctionCallable implements Callable<EmployeeDTO> {

    private final String cnIndividu;

    public GetEmployeeFunctionCallable(String cnIndividu) {
      this.cnIndividu = cnIndividu;
    }

    @Override
    public EmployeeDTO call() {

      final String url = RockUnigePersonSearchService.this.serviceUrl + EMPLOYEE_INFO_ENDPOINT + "/" + this.cnIndividu;
      final ResponseEntity<String> response = RockUnigePersonSearchService.this.makeRestCall(url);

      if (response.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
        return new EmployeeDTO("", "");
      } else {
        final ObjectMapper mapper = new ObjectMapper();
        try {
          ObjectNode node = mapper.readValue(response.getBody(), ObjectNode.class);

          String function = null;
          JsonNode functionNode = node.get(ROCK_FUNCTION_FIELD);
          if (functionNode != null) {
            function = functionNode.textValue();
          }

          String structureLabel = null;
          JsonNode structureLabelNode = node.get(ROCK_STRUCTURE_LABEL_FIELD);
          if (structureLabelNode != null) {
            structureLabel = structureLabelNode.textValue();
          }

          String structureCode = null;
          JsonNode structureCodeNode = node.get(ROCK_STRUCTURE_CODE_FIELD);
          if (structureCodeNode != null) {
            structureCode = structureCodeNode.textValue();
          }

          String structureCompleteLabel = structureLabel;
          if (!StringTool.isNullOrEmpty(structureCode)) {
            if (!StringTool.isNullOrEmpty(structureCompleteLabel)) {
              structureCompleteLabel += " (" + structureCode + ")";
            } else {
              structureCompleteLabel = structureCode;
            }
          }
          return new EmployeeDTO(function, structureCompleteLabel);
        } catch (JsonProcessingException e) {
          throw new SolidifyRuntimeException("Unable to process JSON response", e);
        }
      }
    }
  }

  private class EmployeeDTO {
    private final String function;
    private final String structure;

    public EmployeeDTO(String function, String structure) {
      this.function = function;
      this.structure = structure;
    }

    public String getFunction() {
      return this.function;
    }

    public String getStructure() {
      return this.structure;
    }
  }
}
