/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - EmailProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jakarta.mail.MessagingException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.EmailParameters;
import ch.unige.solidify.service.EmailService;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.ResearchGroup;

@Service
@ConditionalOnBean(AdminController.class)
@Profile("email-service")
public class EmailProcessingService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(EmailProcessingService.class);

  private final EmailService emailService;
  private final PublicationService publicationService;
  private final ResearchGroupService researchGroupService;

  private final String homePage;
  private final int delaySinceLastCheck;
  private final String emailsPrefix;

  public EmailProcessingService(MessageService messageService, AouProperties aouProperties, EmailService emailService,
          PublicationService publicationService, ResearchGroupService researchGroupService) {
    super(messageService);
    this.emailService = emailService;
    this.publicationService = publicationService;
    this.researchGroupService = researchGroupService;
    this.homePage = aouProperties.getParameters().getHomepage();
    this.delaySinceLastCheck = aouProperties.getDelaySinceLastCheck();
    this.emailsPrefix = aouProperties.getParameters().getEmailsPrefix();
  }

  @Transactional
  public void processMessage(EmailMessage emailMessage) {
    Map<String, Object> parameterList = new HashMap<>();
    try {
      switch (emailMessage.getTemplate()) {
        case NEW_DEPOSITS_TO_VALIDATE:
        case MY_INDIRECT_PUBLICATION_VALIDATED:
        case MY_PUBLICATIONS_VALIDATED:
        case MY_PUBLICATIONS_REJECTED:
        case NEW_COMMENT_FOR_VALIDATORS_DEPOSIT:
        case NEW_COMMENT_FOR_CONTRIBUTORS_DEPOSIT:
        case NEW_COMMENT_FOR_DEPOSITOR_DEPOSIT:
        case NEW_PUBLICATIONS_IMPORTED_FROM_ORCID:
        case NEW_PUBLICATIONS_EXPORTED_TO_ORCID:
          this.sendEmailWithPublicationsList(emailMessage);
          break;
        case MY_DEPOSIT_REQUIRES_FEEDBACK:
        case NEW_PUBLICATION_VALIDATED_DOCTOR_THESIS:
          this.sendEmailToDepositor(emailMessage);
          break;
        case NEW_SUBSCRIPTION:
        case REMOVE_SUBSCRIPTION:
          parameterList.put("notificationTypes", emailMessage.getParameters().get(AouConstants.NOTIFICATION_IDS));
          this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(),
                  Collections.singletonList(emailMessage.getTo()), this.getEmailSubject(emailMessage), parameterList);
          break;
        case FORGOTTEN_PUBLICATIONS_TO_VALIDATE:
        case FORGOTTEN_PUBLICATIONS_IN_PROGRESS:
          this.sendEmailWithPublicationsList(emailMessage, this.delaySinceLastCheck);
          break;
        case SYNCHRONIZED_RESEARCH_GROUPS:
          this.sendEmailWithSynchronizedResearchGroups(emailMessage);
          break;
        case NEW_RESEARCH_GROUPS_TO_VALIDATE:
          this.sendEmailWithResearchGroups(emailMessage);
          break;
        case CONTACT_MESSAGE_TO_CONTRIBUTOR:
        case ASK_CORRECTION_FOR_PUBLICATION:
          this.sendEmailPublicationContactMessage(emailMessage);
          break;
        case BATCH_UPDATES_REPORT:
          this.sendEmailWithCorrectionReport(emailMessage);
          break;
        default:
          throw new SolidifyRuntimeException("Email template " + emailMessage.getTemplate() + " not supported");
      }

    } catch (Exception e) {
      log.error("Cannot send email ({}): {}", this.getEmailSubject(emailMessage), e.getMessage(), e);
    }
  }

  private void sendEmailWithCorrectionReport(EmailMessage emailMessage) throws MessagingException {
    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            this.getEmailSubject(emailMessage), emailMessage.getParameters());
  }

  private void sendEmailWithPublicationsList(EmailMessage emailMessage) throws MessagingException {
    List<Publication> publications = new ArrayList<>();
    for (String publicationId : emailMessage.getParameters().keySet()) {
      publications.add(this.publicationService.findOne(publicationId));
    }

    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("publications", publications);

    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            this.getEmailSubject(emailMessage), parameters);
  }

  private void sendEmailWithPublicationsList(EmailMessage emailMessage, int delay) throws MessagingException {
    List<Publication> publications = new ArrayList<>();
    for (String publicationId : emailMessage.getParameters().keySet()) {
      publications.add(this.publicationService.findOne(publicationId));
    }

    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("publications", publications);
    parameters.put("delay", delay);
    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            this.getEmailSubject(emailMessage), parameters);
  }

  private void sendEmailWithOnePublication(EmailMessage emailMessage) throws MessagingException {
    String publicationId = emailMessage.getParameters().keySet().iterator().next();
    Publication publication = this.publicationService.findOne(publicationId);
    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.put("publication", publication);

    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            this.getEmailSubject(emailMessage), parameters);
  }

  private void sendEmailToDepositor(EmailMessage emailMessage) throws MessagingException {
    this.sendEmailWithOnePublication(emailMessage);
  }

  private void sendEmailPublicationContactMessage(EmailMessage emailMessage) throws MessagingException {
    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.putAll(emailMessage.getParameters());

    if (emailMessage.getParameters().get("publicationId") != null) {
      Publication publication = this.publicationService.findOne(emailMessage.getParameters().get("publicationId").toString());
      parameters.put("publication", publication);
    } else {
      parameters.put("publication", null);
    }

    // Clean message content to prevent XSS
    String messageContent = emailMessage.getParameters().get("messageContent").toString();
    messageContent = messageContent.replace("\n", "<br/>");
    messageContent = Jsoup.clean(messageContent, new Safelist().addTags("br"));
    parameters.put("messageContent", messageContent);

    String senderEmail = emailMessage.getParameters().get("senderEmail").toString();

    EmailParameters emailParameters = new EmailParameters().setReplyTo(senderEmail).setToList(Collections.singletonList(emailMessage.getTo()))
            .setSubject(this.getEmailSubject(emailMessage)).setTemplate(emailMessage.getTemplate().toString().toLowerCase())
            .setTemplateParameters(parameters);
    this.emailService.sendEmailWithTemplate(emailParameters);
  }

  private void sendEmailWithSynchronizedResearchGroups(EmailMessage emailMessage) throws MessagingException {
    Map<String, Object> parameters = this.getEmailDefaultParameters();
    parameters.putAll(emailMessage.getParameters());

    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            this.getEmailSubject(emailMessage), parameters);
  }

  private void sendEmailWithResearchGroups(EmailMessage emailMessage) throws MessagingException {
    Map<String, Object> parameters = this.getEmailDefaultParameters();

    List<ResearchGroup> researchGroupList = new ArrayList<>();
    for (String researchGroupId : emailMessage.getParameters().keySet()) {
      researchGroupList.add(this.researchGroupService.findOne(researchGroupId));
    }

    parameters.put("researchGroups", researchGroupList);

    this.emailService.sendEmailWithTemplate(emailMessage.getTemplate().toString().toLowerCase(), Collections.singletonList(emailMessage.getTo()),
            this.getEmailSubject(emailMessage), parameters);
  }

  private Map<String, Object> getEmailDefaultParameters() {
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("homePage", this.homePage);
    return parameters;
  }

  private String getEmailSubject(EmailMessage emailMessage) {
    String subject = emailMessage.getTemplate().getSubject();
    if (!StringTool.isNullOrEmpty(this.emailsPrefix)) {
      subject = this.emailsPrefix.trim() + " " + subject;
    }
    return subject;
  }
}
