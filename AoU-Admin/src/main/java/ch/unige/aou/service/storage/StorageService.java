/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - StorageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.storage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.service.AouService;
import ch.unige.aou.service.MetadataService;

public abstract class StorageService extends AouService implements StorageResourceTransferInterface {

  protected PublicationService publicationService;
  protected UserService userService;
  protected MetadataService metadataService;

  public StorageService(MessageService messageService, PublicationService publicationService, UserService userService,
          MetadataService metadataService) {
    super(messageService);
    this.publicationService = publicationService;
    this.userService = userService;
    this.metadataService = metadataService;
  }

  // **************************
  // ** Methods to implement **
  // **************************

  // Generate additional metadata
  public abstract void generateExportMetadata(Publication publication);

  // Update current metadata
  public abstract void updateExportMetadata(Publication publication);

  public abstract String getNextArchiveId(Publication publication);
  
  // store publication & its metadata
  public abstract void storePublication(Publication publication) throws IOException;

  public abstract void revertDocumentFilesAndThumbnailFromStorageInfo(Publication publication);

  // *********************
  // ** Default Methods **
  // *********************

  // Validate publication metadata
  public void checkMetadata(Publication publication) throws IOException {
    String schema = FileTool.toString(this.getResourceSchema(publication.getMetadataVersion()).getInputStream());
    XMLTool.validate(schema, publication.getMetadata());
  }

  @Override
  public InputStream getInputStream(URI uri) throws IOException {
    return uri.toURL().openStream();
  }
}
