/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - WorkIdentifiers.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.model.orcid;

import ch.unige.solidify.util.StringTool;

public class ExternalIdentifiers {
  private String doi;
  private String pmid;
  private String arxivId;
  private String isbn;

  public void addValue(String identifierType, String identifierValue) {
    switch (identifierType) {
      case "pmid" -> this.setPmid(identifierValue);
      case "doi" -> this.setDoi(identifierValue);
      case "isbn" -> this.setIsbn(identifierValue);
      case "arxiv" -> this.setArxivId(identifierValue);
    }
  }

  public boolean hasValue() {
    return !StringTool.isNullOrEmpty(this.doi)
            || !StringTool.isNullOrEmpty(this.pmid)
            || !StringTool.isNullOrEmpty(this.arxivId)
            || !StringTool.isNullOrEmpty(this.isbn);
  }

  public String getDoi() {
    return this.doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getPmid() {
    return this.pmid;
  }

  public void setPmid(String pmid) {
    this.pmid = pmid;
  }

  public String getArxivId() {
    return this.arxivId;
  }

  public void setArxivId(String arxivId) {
    this.arxivId = arxivId;
  }

  public String getIsbn() {
    return this.isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    if (!StringTool.isNullOrEmpty(this.doi)) {
      sb.append("doi=").append(this.doi);
    }
    if (!StringTool.isNullOrEmpty(this.pmid)) {
      if (sb.length() > 0) {
        sb.append(";");
      }
      sb.append("pmid=").append(this.pmid);
    }
    if (!StringTool.isNullOrEmpty(this.arxivId)) {
      if (sb.length() > 0) {
        sb.append(";");
      }
      sb.append("arxivId=").append(this.arxivId);
    }
    if (!StringTool.isNullOrEmpty(this.isbn)) {
      if (sb.length() > 0) {
        sb.append(";");
      }
      sb.append("isbn=").append(this.isbn);
    }
    return sb.toString();
  }
}
