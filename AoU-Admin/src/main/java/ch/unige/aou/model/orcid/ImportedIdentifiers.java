/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ImportedIdentifiers.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.model.orcid;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import ch.unige.solidify.util.StringTool;

public class ImportedIdentifiers {
  HashMap<String, ExternalIdentifiers> imports = new HashMap<>();

  public void addExternalIdentifiers(String publicationId, ExternalIdentifiers externalIdentifiers) {
    this.imports.put(publicationId, externalIdentifiers);
  }

  public boolean containsOneIdentifier(ExternalIdentifiers externalIdentifiers) {
    return (!StringTool.isNullOrEmpty(externalIdentifiers.getDoi()) && this.containsDoi(externalIdentifiers.getDoi()))
            ||
            (!StringTool.isNullOrEmpty(externalIdentifiers.getPmid()) && this.containsPmid(externalIdentifiers.getPmid()))
            ||
            (!StringTool.isNullOrEmpty(externalIdentifiers.getIsbn()) && this.containsIsbn(externalIdentifiers.getIsbn()))
            ||
            (!StringTool.isNullOrEmpty(externalIdentifiers.getArxivId()) && this.containsArxivId(externalIdentifiers.getArxivId()));
  }

  public String getPublicationId(ExternalIdentifiers externalIdentifiers) {
    Optional<String> publicationIdOpt = Optional.empty();
    if (!StringTool.isNullOrEmpty(externalIdentifiers.getPmid())) {
      publicationIdOpt = this.getPublicationIdByPmid(externalIdentifiers.getPmid());
    }

    if (publicationIdOpt.isEmpty() && !StringTool.isNullOrEmpty(externalIdentifiers.getDoi())) {
      publicationIdOpt = this.getPublicationIdByDoi(externalIdentifiers.getDoi());
    }

    if (publicationIdOpt.isEmpty() && !StringTool.isNullOrEmpty(externalIdentifiers.getIsbn())) {
      publicationIdOpt = this.getPublicationIdByIsbn(externalIdentifiers.getIsbn());
    }

    if (publicationIdOpt.isEmpty() && !StringTool.isNullOrEmpty(externalIdentifiers.getArxivId())) {
      publicationIdOpt = this.getPublicationIdByArxivId(externalIdentifiers.getArxivId());
    }

    return publicationIdOpt.orElse(null);
  }

  public boolean containsDoi(String doi) {
    return this.getPublicationIdByDoi(doi).isPresent();
  }

  public Optional<String> getPublicationIdByDoi(String doi) {
    return this.imports.entrySet().stream().filter(entry -> doi.equals(entry.getValue().getDoi())).map(Map.Entry::getKey).findFirst();
  }

  public boolean containsPmid(String pmid) {
    return this.getPublicationIdByPmid(pmid).isPresent();
  }

  public Optional<String> getPublicationIdByPmid(String pmid) {
    return this.imports.entrySet().stream().filter(entry -> pmid.equals(entry.getValue().getPmid())).map(Map.Entry::getKey).findFirst();
  }

  public boolean containsArxivId(String arxivId) {
    return this.getPublicationIdByArxivId(arxivId).isPresent();
  }

  public Optional<String> getPublicationIdByArxivId(String arxivId) {
    return this.imports.entrySet().stream().filter(entry -> arxivId.equals(entry.getValue().getArxivId())).map(Map.Entry::getKey).findFirst();
  }

  public boolean containsIsbn(String isbn) {
    return this.getPublicationIdByIsbn(isbn).isPresent();
  }

  public Optional<String> getPublicationIdByIsbn(String isbn) {
    return this.imports.entrySet().stream().filter(entry -> isbn.equals(entry.getValue().getIsbn())).map(Map.Entry::getKey).findFirst();
  }
}
