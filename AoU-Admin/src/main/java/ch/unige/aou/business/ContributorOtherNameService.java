/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ContributorOtherNameService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.Objects;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.repository.ContributorOtherNameRepository;
import ch.unige.aou.specification.ContributorOtherNameSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ContributorOtherNameService extends AouResourceService<ContributorOtherName> {

  private static final String ERROR_NAME_ALREADY_EXISTS_FOR_CONTRIBUTOR = "contributor.error.other_name_already_exists";
  private static final String ERROR_NAME_IDENTICAL_TO_CONTRIBUTOR = "contributor.error.other_name_identical_to_contributor";

  @Override
  public SolidifySpecification<ContributorOtherName> getSpecification(ContributorOtherName otherName) {
    return new ContributorOtherNameSpecification(otherName);
  }

  public Optional<ContributorOtherName> findByContributorIdAndFirstNameAndLastName(String contributorId, String firstName, String lastName) {
    return ((ContributorOtherNameRepository) this.itemRepository).findByContributorResIdAndFirstNameAndLastName(contributorId, firstName,
            lastName);
  }

  @Override
  public ContributorOtherName save(ContributorOtherName otherName) {
    ContributorOtherName savedOtherName = super.save(otherName);

    if (savedOtherName.getContributor() != null) {
      SolidifyEventPublisher.getPublisher().publishEvent(savedOtherName.getContributor().getCacheMessage());
    }

    return savedOtherName;
  }

  @Override
  protected void validateItemSpecificRules(ContributorOtherName otherName, BindingResult errors) {

    Optional<ContributorOtherName> otherNameOpt = this.findByContributorIdAndFirstNameAndLastName(otherName.getContributor().getResId(),
            otherName.getFirstName(), otherName.getLastName());

    if (otherNameOpt.isPresent() && !otherNameOpt.get().getResId().equals(otherName.getResId())) {
      String errorMessage = this.messageService.get(ERROR_NAME_ALREADY_EXISTS_FOR_CONTRIBUTOR, new Object[] { otherName.getFullName() });
      errors.addError(new FieldError(ContributorOtherName.class.getSimpleName(), "firstName", errorMessage));
      errors.addError(new FieldError(ContributorOtherName.class.getSimpleName(), "lastName", errorMessage));
    }

    // Check that the other name is not the same as the contributor
    if (Objects.equals(otherName.getContributor().getLastName(), otherName.getLastName())
            && Objects.equals(otherName.getContributor().getFirstName(), otherName.getFirstName())) {
      String errorMessage = this.messageService.get(ERROR_NAME_IDENTICAL_TO_CONTRIBUTOR, new Object[] { otherName.getFullName() });
      errors.addError(new FieldError(ContributorOtherName.class.getSimpleName(), "firstName", errorMessage));
      errors.addError(new FieldError(ContributorOtherName.class.getSimpleName(), "lastName", errorMessage));
    }
  }
}
