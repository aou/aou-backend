/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - RoleService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.security.Role;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.repository.RoleRepository;
import ch.unige.aou.specification.RoleSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class RoleService extends AouResourceService<Role> {
  private static final Logger log = LoggerFactory.getLogger(RoleService.class);

  private RoleRepository roleRepository;
  private LabeledLanguageService labeledLanguageService;

  public RoleService(RoleRepository roleRepository, LabeledLanguageService labeledLanguageService) {
    this.roleRepository = roleRepository;
    this.labeledLanguageService = labeledLanguageService;
  }

  @Override
  public RoleSpecification getSpecification(Role resource) {
    return new RoleSpecification(resource);
  }

  @Override
  public Optional<Integer> getMaxSortValue(Sortable item) {
    return ((RoleRepository) this.itemRepository).findMaxSortValue();
  }

  @Override
  protected void beforeValidate(Role item) {
    this.setDefaultSortValue(item);
  }

  public void initDefaultData() {
  }
}
