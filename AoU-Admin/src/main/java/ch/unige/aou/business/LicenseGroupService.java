/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - LicenseGroupService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.io.IOException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import jakarta.transaction.Transactional;

import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.LicenseGroup;
import ch.unige.aou.repository.LicenseGroupRepository;
import ch.unige.aou.specification.LicenseGroupSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class LicenseGroupService extends AouResourceService<LicenseGroup> {
  static final Logger log = LoggerFactory.getLogger(LicenseGroupService.class);

  public static final String AOU_LICENSES_GROUPS_FILE = "aou-licenses-groups.json";

  private LabeledLanguageService labeledLanguageService;

  public LicenseGroupService(LabeledLanguageService labeledLanguageService) {
    this.labeledLanguageService = labeledLanguageService;
  }

  public LicenseGroup findByName(String name) {
    return ((LicenseGroupRepository) this.itemRepository).findByName(name);
  }

  @Override
  public SolidifySpecification<LicenseGroup> getSpecification(LicenseGroup resource) {
    return new LicenseGroupSpecification(resource);
  }

  @Override
  protected void beforeValidate(LicenseGroup item) {
    this.setDefaultSortValue(item);
  }

  @Override
  protected void validateItemSpecificRules(LicenseGroup licenseGroup, BindingResult errors) {
    // check name uniqueness
    LicenseGroup existingLicenseGroup = this.findByName(licenseGroup.getName());
    if (existingLicenseGroup != null && !existingLicenseGroup.getResId().equals(licenseGroup.getResId())) {
      errors.addError(new FieldError(licenseGroup.getClass().getSimpleName(), "name", "This value already exists"));
    }
  }

  @Override
  public Optional<Integer> getMaxSortValue(Sortable item) {
    return ((LicenseGroupRepository) this.itemRepository).findMaxSortValue();
  }

  @Transactional
  public void initDefaultData() {
    try {
      ObjectMapper mapper = new ObjectMapper();
      final ArrayNode licensesGroupsArray = mapper
              .readValue(FileTool.toString(new ClassPathResource(AOU_LICENSES_GROUPS_FILE).getInputStream()), ArrayNode.class);
      for (final JsonNode item : licensesGroupsArray) {
        String resId = item.get("resId").textValue();
        if (!this.existsById(resId)) {
          LicenseGroup licenseGroup = new LicenseGroup();
          licenseGroup.setResId(resId);
          licenseGroup.setName(item.get("name").textValue());
          licenseGroup.setVisible(true);
          if (item.has("labels")) {
            JsonNode labelsNode = item.get("labels");
            if (labelsNode.has(AouConstants.LANG_CODE_FRENCH)) {
              licenseGroup.getLabels().add(new Label(labelsNode.get(AouConstants.LANG_CODE_FRENCH).textValue(),
                      this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));
            }
            if (labelsNode.has(AouConstants.LANG_CODE_ENGLISH)) {
              licenseGroup.getLabels().add(new Label(labelsNode.get(AouConstants.LANG_CODE_ENGLISH).textValue(),
                      this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
            }
          }
          licenseGroup = this.save(licenseGroup);
        }
      }
    } catch (IOException e) {
      log.error("Unable to initialize licenses groups from Json file", e);
    }
  }

}
