/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ResearchGroupService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.rest.ResearchGroupDTO;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.repository.ResearchGroupRepository;
import ch.unige.aou.service.rest.ResearchGroupSearch;
import ch.unige.aou.specification.ResearchGroupSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ResearchGroupService extends AouResourceService<ResearchGroup> {
  private static final Logger log = LoggerFactory.getLogger(ResearchGroupService.class);

  private static final String JSON_FILE_FIELD_ID = "id";
  private static final String JSON_FILE_FIELD_TITLE = "title";
  private static final String JSON_FILE_FIELD_DISPLAY_TITLE = "display_title";
  private static final String JSON_FILE_FIELD_EXTERNAL_CODE = "external_code";
  private static final String JSON_FILE_FIELD_DELETED = "deleted";

  private static final String ERROR_RESEARCH_GROUP_IDENTICAL_NAME_ACTIVE_STATUS = "research_groups.error.sameNameActiveStatus";
  private static final String ERROR_RESEARCH_GROUP_IDENTICAL_NAME_INACTIVE_STATUS = "research_groups.error.sameNameInactiveStatus";

  private final ResearchGroupRepository researchGroupRepository;
  private final ResearchGroupSearch researchGroupSearch;

  public ResearchGroupService(ResearchGroupRepository researchGroupRepository, ResearchGroupSearch researchGroupSearch) {
    this.researchGroupRepository = researchGroupRepository;
    this.researchGroupSearch = researchGroupSearch;
  }

  public List<ResearchGroup> findByName(String name) {
    return this.researchGroupRepository.findByName(name);
  }

  /**
   * Return the list of research groups that a user has selected in his profile
   *
   * @param externalUid
   * @param pageable
   * @return
   */
  public Page<ResearchGroup> findByUserExternalUid(String externalUid, Pageable pageable) {
    return this.researchGroupRepository.findByUserExternalUid(externalUid, pageable);
  }

  public ResearchGroup validate(String researchGroupId) {
    ResearchGroup researchGroup = this.findOne(researchGroupId);
    researchGroup.setValidated(true);
    return this.save(researchGroup);
  }

  @Override
  protected void validateItemSpecificRules(ResearchGroup item, BindingResult errors) {
    /**
     * Check name + active uniqueness
     */
    List<ResearchGroup> existingResearchGroups = this.findByName(item.getName());
    if (!existingResearchGroups.isEmpty()) {
      for (ResearchGroup existingResearchGroup : existingResearchGroups) {
        if (!existingResearchGroup.getResId().equals(item.getResId()) && existingResearchGroup.isActive().equals(item.isActive())) {
          String messageKey = ERROR_RESEARCH_GROUP_IDENTICAL_NAME_ACTIVE_STATUS;
          if (Boolean.FALSE.equals(item.isActive())) {
            messageKey = ERROR_RESEARCH_GROUP_IDENTICAL_NAME_INACTIVE_STATUS;
          }
          errors.addError(new FieldError(item.getClass().getSimpleName(), "name",
                  this.messageService.get(messageKey, new Object[] { item.getName(), item.isActive() })));
        }
      }
    }
    /**
     * Check that research group code is a positive integer or zero
     */
    if (!StringTool.isNullOrEmpty(item.getCode()) && !item.getCode().matches("\\d+")) {
      errors.addError(new FieldError(item.getClass().getSimpleName(), "code", "Research group code must be a positive integer or zero"));
    }
  }

  @Override
  public ResearchGroupSpecification getSpecification(ResearchGroup resource) {
    return new ResearchGroupSpecification(resource);
  }

  @Transactional
  public void createResearchGroupsFromJsonFile(String jsonFilePath) throws IOException {
    final JSONArray structuresArray = new JSONArray(FileTool.toString(new ClassPathResource(jsonFilePath).getInputStream()));
    for (final Object obj : structuresArray) {
      if (obj instanceof JSONObject) {
        JSONObject item = (JSONObject) obj;
        String id = item.getString(JSON_FILE_FIELD_ID);
        String title = item.getString(JSON_FILE_FIELD_TITLE);
        if (!item.isNull(JSON_FILE_FIELD_DISPLAY_TITLE)) {
          title = item.getString(JSON_FILE_FIELD_DISPLAY_TITLE);
        }
        String externalCode = null;
        if (!item.isNull(JSON_FILE_FIELD_EXTERNAL_CODE)) {
          externalCode = item.getString(JSON_FILE_FIELD_EXTERNAL_CODE);
        }
        String deletedStr = item.getString(JSON_FILE_FIELD_DELETED);
        boolean active = !deletedStr.equals("1");

        this.createResearchGroup(id, title, externalCode, active);
      }
    }
    log.info("All research groups initialized");
  }

  private void createResearchGroup(String resId, String name, String code, boolean active) {
    if (!this.existsById(resId)) {
      ResearchGroup group = new ResearchGroup();
      group.setResId(resId);
      group.setName(name);
      if (!StringTool.isNullOrEmpty(code)) {
        group.setCode(code);
      }
      group.setActive(active);
      this.save(group);
      log.info("Research group '{}' created", name);
    } else {
      log.debug("Research group '{}: {}' already exists", resId, name);
    }
  }

  @Transactional
  public List<ResearchGroupDTO> synchronizeFromUnigeReferential(LocalDateTime createdOrModifiedSince) {
    List<ResearchGroupDTO> researchGroupDTOS = this.researchGroupSearch.search(createdOrModifiedSince);
    log.info("{} research groups will be checked for synchronization", researchGroupDTOS.size());

    List<ResearchGroupDTO> synchronizedResearchGroups = new ArrayList<>();
    for (final ResearchGroupDTO researchGroupDTO : researchGroupDTOS) {
      ResearchGroupDTO syncResearchGroupDTO = this.syncResearchGroup(researchGroupDTO);
      if (syncResearchGroupDTO != null) {
        synchronizedResearchGroups.add(syncResearchGroupDTO);
      }
    }
    log.info("{} research groups have been synchronized", synchronizedResearchGroups.size());
    return synchronizedResearchGroups;
  }

  private ResearchGroupDTO syncResearchGroup(ResearchGroupDTO researchGroupDTO) {
    String resId = researchGroupDTO.getId();
    String name = researchGroupDTO.getName();
    String code = researchGroupDTO.getCode();
    boolean active = !researchGroupDTO.isDeleted();

    boolean sync = false;

    Optional<ResearchGroup> existingResearchGroupOpt = this.findById(resId);
    if (existingResearchGroupOpt.isPresent()) {
      log.debug("Research group '{}: {}' already exists", resId, name);
      ResearchGroup existingResearchGroup = existingResearchGroupOpt.get();

      // eventually update the existing research group if:
      //   - the source has been updated after the record in database
      //   - its name has changed
      //   - its active status has changed
      ZoneOffset offset = existingResearchGroup.getLastUpdate().getWhen().getOffset();
      OffsetDateTime sourceLastUpdate = OffsetDateTime.of(researchGroupDTO.getModified(), offset);
      if (sourceLastUpdate.isAfter(existingResearchGroup.getLastUpdate().getWhen()) && (
              !existingResearchGroup.getName().equals(name) || (existingResearchGroup.isActive() && researchGroupDTO.isDeleted())
      )) {
        String oldName = existingResearchGroup.getName();
        if (!existingResearchGroup.getName().equals(name)) {
          researchGroupDTO.setOldName(oldName);
          researchGroupDTO.setAction(ResearchGroupDTO.Action.MODIFIED);
        } else {
          researchGroupDTO.setAction(ResearchGroupDTO.Action.DELETED);
        }
        existingResearchGroup.setName(name);
        existingResearchGroup.setActive(active);
        this.save(existingResearchGroup);
        sync = true;
        log.info("Existing research group '{}' updated with new name '{}' and isActive '{}' (resId={})", oldName,
                existingResearchGroup.getName(), existingResearchGroup.isActive(), resId);
      }
    } else {
      ResearchGroup group = new ResearchGroup();
      group.setResId(resId);
      group.setName(name);
      if (!StringTool.isNullOrEmpty(code)) {
        group.setCode(code);
      }
      group.setActive(active);
      group.setValidated(true);
      this.save(group);
      researchGroupDTO.setAction(ResearchGroupDTO.Action.ADDED);
      sync = true;
      log.info("New research group '{}' (resId={}) created", name, resId);
    }
    if (sync) {
      return researchGroupDTO;
    } else {
      return null;
    }
  }
}
