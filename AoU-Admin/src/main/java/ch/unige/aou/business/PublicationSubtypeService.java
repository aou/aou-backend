/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationSubtypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import jakarta.transaction.Transactional;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.ValueWithTranslations;
import ch.unige.aou.repository.PublicationSubtypeRepository;
import ch.unige.aou.specification.PublicationSubtypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PublicationSubtypeService extends AouResourceService<PublicationSubtype> {

  private static final String SUBTYPE_ARTICLE_SCIENTIFIQUE_EN = "Scientific Article";
  private static final String SUBTYPE_ARTICLE_PROFESSIONNEL_NAME_EN = "Professional Article";
  private static final String SUBTYPE_AUTRE_ARTICLE_NAME_EN = "Newspaper Article";
  private static final String SUBTYPE_ACTES_DE_CONFERENCE_NAME_EN = "Proceedings (set of individual contributions)";
  private static final String SUBTYPE_ACTES_DE_CONFERENCE_NAME_FR = "Actes (ensemble de contributions individuelles)";
  private static final String SUBTYPE_PRESENTATION_INTERVENTION_NAME_EN = "Conference presentation";
  private static final String SUBTYPE_POSTER_NAME_EN = "Poster";
  private static final String SUBTYPE_CHAPITRE_D_ACTES_NAME_EN = "Chapter / Article (individual contribution to proceedings)";
  private static final String SUBTYPE_CHAPITRE_D_ACTES_NAME_FR = "Chapitre / Article (contribution parue dans des actes)";
  private static final String SUBTYPE_THESE_NAME_EN = "Doctoral Thesis";
  private static final String SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME_EN = "Master of advanced Studies";
  private static final String SUBTYPE_MASTER_NAME_EN = "Master";
  private static final String SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME_EN = "Privat-docent Thesis";
  private static final String SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME_FR = "Thèse de doctorat professionnel de FC (DAPS)";
  private static final String SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME_EN = "Doctoral thesis of advanced professional studies (DAPS)";
  private static final String SUBTYPE_NUMERO_DE_REVUE_NAME_EN = "Journal Issue";
  private static final String SUBTYPE_LIVRE_NAME_EN = "Book";
  private static final String SUBTYPE_OUVRAGE_COLLECTIF_NAME_EN = "Edited Book";
  private static final String SUBTYPE_CHAPITRE_DE_LIVRE_NAME_EN = "Book Chapter";
  private static final String SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME_EN = "Dictionary / Encyclopedia contribution";
  private static final String SUBTYPE_RAPPORT_DE_RECHERCHE_NAME_EN = "Report";
  private static final String SUBTYPE_RAPPORT_TECHNIQUE_NAME_EN = "Technical Report";
  private static final String SUBTYPE_WORKING_PAPER_NAME_EN = "Data paper";
  private static final String SUBTYPE_PREPRINT_NAME_EN = "Preprint";

  private static final Logger log = LoggerFactory.getLogger(PublicationSubtypeService.class);

  private static final String DEFAULT_DESCRIPTIONS_FILE = "publication-subtypes-descriptions.json";

  private final PublicationTypeService publicationTypeService;
  private final PublicationSubtypeRepository publicationSubtypeRepository;
  private final LabeledLanguageService labeledLanguageService;

  public PublicationSubtypeService(PublicationSubtypeRepository publicationSubtypeRepository, PublicationTypeService publicationTypeService,
          LabeledLanguageService labeledLanguageService) {
    this.publicationSubtypeRepository = publicationSubtypeRepository;
    this.publicationTypeService = publicationTypeService;
    this.labeledLanguageService = labeledLanguageService;
  }

  @Override
  public PublicationSubtypeSpecification getSpecification(PublicationSubtype resource) {
    return new PublicationSubtypeSpecification(resource);
  }

  public Optional<PublicationSubtype> findByName(String name) {
    return this.publicationSubtypeRepository.findByName(name);
  }

  public PublicationSubtype findByCode(String code) {
    return this.publicationSubtypeRepository.findByCode(code);
  }

  public List<String> getAllIds() {
    Page<PublicationSubtype> list = this.findAll(Pageable.unpaged());
    return list.getContent().stream().map(PublicationSubtype::getResId).collect(Collectors.toList());
  }

  @Override
  protected void beforeValidate(PublicationSubtype item) {

    // Calculates a default sort value if no one is given
    if (!this.itemRepository.existsById(item.getResId()) && item.getSortValue() == null && item.getPublicationType() != null) {
      Optional<Integer> maxValueOpt = this.publicationSubtypeRepository.findMaxSortValueByPublicationType(item.getPublicationType().getResId());
      int newOrder = maxValueOpt.isPresent() ? maxValueOpt.get() + AouConstants.ORDER_INCREMENT : AouConstants.ORDER_INCREMENT;
      item.setSortValue(newOrder);
    }

    // Calculates a default sort value for bibliographies if no one is given
    if (!this.itemRepository.existsById(item.getResId()) && item.getBibliographySortValue() == null) {
      Optional<Integer> maxValueOpt = this.publicationSubtypeRepository.findMaxBibliographySortValue();
      int newOrder = maxValueOpt.isPresent() ? maxValueOpt.get() + AouConstants.ORDER_INCREMENT : AouConstants.ORDER_INCREMENT;
      item.setBibliographySortValue(newOrder);
    }
  }

  /********************
   ** Initialization **
   ********************/

  @Transactional
  public void initDefaultData() {
    // @formatter:off
    int sort = 0;
    String type = AouConstants.DEPOSIT_TYPE_ARTICLE_NAME;
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME,                   AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID,  (sort = sort + 10), 10);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME,                  AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_ID, (sort = sort + 10), 20);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME,                          AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_ID,         (sort = sort + 10), 30);

    type = AouConstants.DEPOSIT_TYPE_CONFERENCE_NAME;
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME,                       AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_ID,          (sort = sort + 10), 90);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME,              AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_ID, (sort = sort + 10), 110);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME,                                 AouConstants.DEPOSIT_SUBTYPE_POSTER_ID,                    (sort = sort + 10), 140);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME,                    AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_ID, 	     (sort = sort + 10), 70);

    type = AouConstants.DEPOSIT_TYPE_DIPLOME_NAME;
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_THESE_NAME,                                  AouConstants.DEPOSIT_SUBTYPE_THESE_ID,                    (sort = sort + 10), 170);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME,               AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_ID, (sort = sort + 10), 200);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME,                                 AouConstants.DEPOSIT_SUBTYPE_MASTER_ID,                   (sort = sort + 10), 210);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME,                 AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_ID,   (sort = sort + 10), 180);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME,            AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_ID,   (sort = sort + 10), 190);

    type = AouConstants.DEPOSIT_TYPE_JOURNAL_NAME;
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME,                        AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_ID, (sort = sort + 10), 40);

    type = AouConstants.DEPOSIT_TYPE_LIVRE_NAME;
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME,                                  AouConstants.DEPOSIT_SUBTYPE_LIVRE_ID,                                  (sort = sort + 10), 50);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME,                      AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_ID,                      (sort = sort + 10), 60);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME,                      AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_ID,                      (sort = sort + 10), 80);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME, AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_ID, (sort = sort + 10), 100);

    type = AouConstants.DEPOSIT_TYPE_RAPPORT_NAME;
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME,                   AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_ID, (sort = sort + 10), 130);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME    ,                  AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_ID,    (sort = sort + 10), 150);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME,                          AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_ID,        (sort = sort + 10), 160);
    this.createPublicationSubtype(type, AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME,                               AouConstants.DEPOSIT_SUBTYPE_PREPRINT_ID,             (sort = sort + 10), 120);
    // @formatter:on
  }

  public void createPublicationSubtype(String typeName, String name, String code, int sort, int bibliographySortValue) {
    PublicationType type = this.publicationTypeService.findByName(typeName);
    if (type != null) {
      PublicationSubtype subtype = this.findByCode(code);
      if (subtype == null) {
        subtype = new PublicationSubtype();
        subtype.setResId(code);
        subtype.setPublicationType(type);
        subtype.setName(name);
        subtype.setCode(code);
        subtype.setSortValue(sort);
        subtype.setBibliographySortValue(bibliographySortValue);
        this.addDefaultDescriptions(subtype);
        this.addLabels(subtype);
        this.save(subtype);
        log.info("PublicationSubtype '{}' created", name);
      } else {
        log.debug("PublicationSubtype '{}' already exists", name);
      }
    } else {
      log.warn("unable to create PublicationSubtype {} as PublicationType {} does not exist", name, typeName);
    }
  }

  private void addDefaultDescriptions(PublicationSubtype subtype) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      final ArrayNode licensesArray = mapper
              .readValue(FileTool.toString(new ClassPathResource(DEFAULT_DESCRIPTIONS_FILE).getInputStream()), ArrayNode.class);
      JsonNode subtypeItem = null;
      for (final JsonNode item : licensesArray) {
        String code = item.get("code").textValue();
        if (code.equals(subtype.getCode())) {
          subtypeItem = item;
          break;
        }
      }

      if (subtypeItem != null && subtypeItem.has("descriptions")) {
        JsonNode descriptionsNode = subtypeItem.get("descriptions");
        if (descriptionsNode.has(AouConstants.LANG_CODE_FRENCH)) {
          subtype.getDescriptions().add(new Label(descriptionsNode.get(AouConstants.LANG_CODE_FRENCH).textValue(),
                  this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));
        }
        if (descriptionsNode.has(AouConstants.LANG_CODE_ENGLISH)) {
          subtype.getDescriptions().add(new Label(descriptionsNode.get(AouConstants.LANG_CODE_ENGLISH).textValue(),
                  this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
        }
      }

    } catch (IOException e) {
      log.error("Unable to initialize publication subtypes descriptions from Json file", e);
    }
  }

  private void addLabels(PublicationSubtype subtype) {
    ValueWithTranslations valueWithTranslations;
    switch (subtype.getName()) {
      case AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, SUBTYPE_ARTICLE_SCIENTIFIQUE_EN,
                        AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Wissenschaftlicher Artikel");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Artículo científico");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME, SUBTYPE_ARTICLE_PROFESSIONNEL_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Fachartikel");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Artículo profesional");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME, SUBTYPE_AUTRE_ARTICLE_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Sonstiger Artikel");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Otro artículo");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME, SUBTYPE_ACTES_DE_CONFERENCE_NAME_EN,
                        SUBTYPE_ACTES_DE_CONFERENCE_NAME_FR);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Tagungsband");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Acta de congreso");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME,
                SUBTYPE_PRESENTATION_INTERVENTION_NAME_EN, AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Konferezpräsentation");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Presentación / Comunicación");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME, SUBTYPE_POSTER_NAME_EN,
                AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Konferenzposter");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Póster");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME, SUBTYPE_CHAPITRE_D_ACTES_NAME_EN,
                        SUBTYPE_CHAPITRE_D_ACTES_NAME_FR);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Konferenzbeitrag");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Artículo de acta");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_THESE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_THESE_NAME, SUBTYPE_THESE_NAME_EN,
                AouConstants.DEPOSIT_SUBTYPE_THESE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Dissertation");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Tesis doctoral");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME,
                SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME_EN, AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Master of advanced studies");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Trabajo de máster de estudios avanzados");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME, SUBTYPE_MASTER_NAME_EN,
                AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Masterarbeit");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Tesina de máster");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME, SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Habilitation");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Tesis \"privat-docent\"");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME,
                        SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME_EN, SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME_FR);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Doctoral thesis of advanced professional studies (DAPS)");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Tesis doctoral profesional en FC (DAPS)");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME, SUBTYPE_NUMERO_DE_REVUE_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Zeitschrift");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Número de revista");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME, SUBTYPE_LIVRE_NAME_EN,
                AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Monografie");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Libro");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME, SUBTYPE_OUVRAGE_COLLECTIF_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Sammelband");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Obra colectiva");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME, SUBTYPE_CHAPITRE_DE_LIVRE_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Buchkapitel");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Capítulo de libro");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME,
                SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME_EN,
                AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Wörterbuch-/Lexikonbeitrag");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Artículo de diccionario / enciclopedia");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME, SUBTYPE_RAPPORT_DE_RECHERCHE_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Forschungsbericht");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Informe de investigación");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME, SUBTYPE_RAPPORT_TECHNIQUE_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Technischer Bericht");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Informe técnico");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME:
        valueWithTranslations = this
                .newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME, SUBTYPE_WORKING_PAPER_NAME_EN,
                        AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Arbeitspapier");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Working paper");
        break;
      case AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME, SUBTYPE_PREPRINT_NAME_EN,
                AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME);
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_GERMAN, "Preprint");
        valueWithTranslations.addTranslation(AouConstants.LANG_CODE_SPANISH, "Artículo preliminar");
        break;
      default:
        return;
    }
    subtype.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_ENGLISH),
            this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
    subtype.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_FRENCH),
            this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));
    subtype.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_GERMAN),
            this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_GERMAN)));
    subtype.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_SPANISH),
            this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_SPANISH)));
  }

  public boolean isThesisOrProfessionalThesisOrMaster(String subtype) {
    return !StringTool.isNullOrEmpty(subtype) &&
            (subtype.equals(AouConstants.DEPOSIT_SUBTYPE_THESE_NAME) ||
                    subtype.equals(AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME) ||
                    subtype.equals(AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME) ||
                    subtype.equals(AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME));
  }

  public boolean isThesisOrThesisProfessional(String subtype) {
    return !StringTool.isNullOrEmpty(subtype) &&
            (subtype.equals(AouConstants.DEPOSIT_SUBTYPE_THESE_NAME) ||
                    subtype.equals(AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME));
  }

}
