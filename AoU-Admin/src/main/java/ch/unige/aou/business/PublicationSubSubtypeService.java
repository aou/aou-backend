/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationSubSubtypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.io.IOException;
import java.text.Normalizer;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import jakarta.transaction.Transactional;

import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.ValueWithTranslations;
import ch.unige.aou.repository.PublicationSubSubtypeRepository;
import ch.unige.aou.specification.PublicationSubSubtypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PublicationSubSubtypeService extends AouResourceService<PublicationSubSubtype> {
  private static final Logger log = LoggerFactory.getLogger(PublicationSubSubtypeService.class);

  private static final String DEFAULT_DESCRIPTIONS_FILE = "publication-sub-subtypes-descriptions.json";

  private PublicationSubtypeService publicationSubtypeService;
  private PublicationSubSubtypeRepository publicationSubSubtypeRepository;
  private LabeledLanguageService labeledLanguageService;

  public PublicationSubSubtypeService(PublicationSubSubtypeRepository publicationSubSubtypeRepository,
          PublicationSubtypeService publicationSubtypeService, LabeledLanguageService labeledLanguageService) {
    this.publicationSubSubtypeRepository = publicationSubSubtypeRepository;
    this.publicationSubtypeService = publicationSubtypeService;
    this.labeledLanguageService = labeledLanguageService;
  }

  @Override
  public SolidifySpecification<PublicationSubSubtype> getSpecification(PublicationSubSubtype resource) {
    return new PublicationSubSubtypeSpecification(resource);
  }

  public PublicationSubSubtype findByNameAndPublicationSubType(String name, PublicationSubtype subtype) {
    return this.publicationSubSubtypeRepository.findByNameAndPublicationSubtype(name, subtype);
  }

  public Optional<Integer> getMaxSortValueByPublicationSubtype(String typeId) {
    return this.publicationSubSubtypeRepository.findMaxSortValueByPublicationSubtype(typeId);
  }

  @Override
  protected void beforeValidate(PublicationSubSubtype item) {

    // Calculates a default sort value if no one is given
    if (!this.itemRepository.existsById(item.getResId()) && item.getSortValue() == null && item.getPublicationSubtype() != null) {
      Optional<Integer> maxValueOpt = this.getMaxSortValueByPublicationSubtype(item.getPublicationSubtype().getResId());
      int newOrder = maxValueOpt.isPresent() ? maxValueOpt.get() + AouConstants.ORDER_INCREMENT : AouConstants.ORDER_INCREMENT;
      item.setSortValue(newOrder);
    }

    // Calculates a default sort value for bibliographies if no one is given
    if (!this.itemRepository.existsById(item.getResId()) && item.getBibliographySortValue() == null) {
      Optional<Integer> maxValueOpt = this.publicationSubSubtypeRepository.findMaxBibliographySortValue();
      int newOrder = maxValueOpt.isPresent() ? maxValueOpt.get() + AouConstants.ORDER_INCREMENT : AouConstants.ORDER_INCREMENT;
      item.setBibliographySortValue(newOrder);
    }
  }

  /********************
   ** Initialization **
   ********************/

  @Transactional
  public void initDefaultData() {
    // @formatter:off
    int sort = 0;
    String subtype = AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME;
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME,            (sort = sort + 10), 10);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_DONNEES_NAME,    (sort = sort + 10), 90);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_VIDEO,           (sort = sort + 10), 100);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME,        (sort = sort + 10), 60);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME, (sort = sort + 10), 70);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_EDITORIAL_NAME,          (sort = sort + 10), 40);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_LETTRE_NAME,             (sort = sort + 10), 50);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_META_ANALYSIS_NAME,      (sort = sort + 10), 20);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_RAPPORT_DU_CAS_NAME,     (sort = sort + 10), 80);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_REVIEW_LITERATURE_NAME,  (sort = sort + 10), 30);

    subtype = AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME;
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME,            (sort = sort + 10), 10);
    this.createPublicationSubSubtype(subtype, AouConstants.DEPOSIT_SUB_SUBTYPE_BILLET_DE_BLOG_NAME,     (sort = sort + 10), 20);
    // @formatter:on
  }

  public void createPublicationSubSubtype(String subtypeName, String name, int sort, int bibliographySort) {
    Optional<PublicationSubtype> subtypeOptional = this.publicationSubtypeService.findByName(subtypeName);
    if (subtypeOptional.isPresent()) {
      PublicationSubtype subtype = subtypeOptional.get();
      PublicationSubSubtype subSubtype = this.findByNameAndPublicationSubType(name, subtype);
      if (subSubtype == null) {
        subSubtype = new PublicationSubSubtype();
        String nameNormalized = Normalizer.normalize(name.toUpperCase(), Normalizer.Form.NFD);
        String nameWithoutAccentNormalized = nameNormalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        subSubtype.setResId(subtype.getCode() + "-" + nameWithoutAccentNormalized.replace(" ", "-"));
        subSubtype.setName(name);
        subSubtype.setSortValue(sort);
        subSubtype.setBibliographySortValue(bibliographySort);
        subSubtype.setPublicationSubtype(subtype);
        this.addDefaultDescriptions(subSubtype);
        this.addLabels(subSubtype);
        this.save(subSubtype);
        log.info("PublicationSubSubtype '{}' created", name);
      } else {
        log.debug("PublicationSubSubtype '{}' already exists", name);
      }
    } else {
      log.warn("unable to create PublicationSubSubtype {} as PublicationSubtype {} does not exist", name, subtypeName);
    }
  }

  private void addDefaultDescriptions(PublicationSubSubtype subSubtype) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      final ArrayNode licensesArray = mapper
              .readValue(FileTool.toString(new ClassPathResource(DEFAULT_DESCRIPTIONS_FILE).getInputStream()), ArrayNode.class);
      JsonNode subSubtypeItem = null;
      for (final JsonNode item : licensesArray) {
        String resId = item.get("resId").textValue();
        if (resId.equals(subSubtype.getResId())) {
          subSubtypeItem = item;
          break;
        }
      }

      if (subSubtypeItem != null && subSubtypeItem.has("descriptions")) {
        JsonNode descriptionsNode = subSubtypeItem.get("descriptions");
        if (descriptionsNode.has(AouConstants.LANG_CODE_FRENCH)) {
          subSubtype.getDescriptions().add(new Label(descriptionsNode.get(AouConstants.LANG_CODE_FRENCH).textValue(),
                  this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));
        }
        if (descriptionsNode.has(AouConstants.LANG_CODE_ENGLISH)) {
          subSubtype.getDescriptions().add(new Label(descriptionsNode.get(AouConstants.LANG_CODE_ENGLISH).textValue(),
                  this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
        }
      }

    } catch (IOException e) {
      log.error("Unable to initialize publication subtypes descriptions from Json file", e);
    }
  }

  private void addLabels(PublicationSubSubtype subSubtype) {
    ValueWithTranslations valueWithTranslations;
    switch (subSubtype.getName()) {
      case AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME, "Article",
                AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_DONNEES_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_DONNEES_NAME, "Data paper",
                AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_DONNEES_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_VIDEO:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_VIDEO, "Video article",
                AouConstants.DEPOSIT_SUB_SUBTYPE_ARTICLE_VIDEO);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME, "Comment",
                AouConstants.DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME, "Book review",
                AouConstants.DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_EDITORIAL_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_EDITORIAL_NAME, "Editorial",
                AouConstants.DEPOSIT_SUB_SUBTYPE_EDITORIAL_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_LETTRE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_LETTRE_NAME, "Letter",
                AouConstants.DEPOSIT_SUB_SUBTYPE_LETTRE_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_META_ANALYSIS_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_META_ANALYSIS_NAME, "Meta-analysis",
                AouConstants.DEPOSIT_SUB_SUBTYPE_META_ANALYSIS_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_RAPPORT_DU_CAS_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_RAPPORT_DU_CAS_NAME, "Case report",
                AouConstants.DEPOSIT_SUB_SUBTYPE_RAPPORT_DU_CAS_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_REVIEW_LITERATURE_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_REVIEW_LITERATURE_NAME, "Review",
                AouConstants.DEPOSIT_SUB_SUBTYPE_REVIEW_LITERATURE_NAME);
        break;
      case AouConstants.DEPOSIT_SUB_SUBTYPE_BILLET_DE_BLOG_NAME:
        valueWithTranslations = this.newValueWithEngFreTranslations(AouConstants.DEPOSIT_SUB_SUBTYPE_BILLET_DE_BLOG_NAME, "Blog post",
                AouConstants.DEPOSIT_SUB_SUBTYPE_BILLET_DE_BLOG_NAME);
        break;
      default:
        return;
    }
    subSubtype.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_ENGLISH),
            this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
    subSubtype.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_FRENCH),
            this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));

  }

  public List<PublicationSubSubtype> findByPublicationSubtypes(String publicationSubtypeId) {
    return this.publicationSubSubtypeRepository.findByPublicationSubtypes(publicationSubtypeId);
  }
}
