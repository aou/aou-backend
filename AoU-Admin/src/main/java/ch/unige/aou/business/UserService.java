/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - UserService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import static ch.unige.aou.AouConstants.NO_REPLY_PREFIX;
import static ch.unige.aou.AouRestFields.APPLICATION_ROLE_FIELD;
import static ch.unige.aou.AouRestFields.RES_ID_FIELD;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.auth.client.service.AuthorizationClientProperties;
import ch.unige.solidify.auth.client.service.UserCreatorService;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.AuthUserDto;
import ch.unige.solidify.auth.model.PersonInfo;
import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.auth.service.UserInfoLocalService;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.OrcidToken;
import ch.unige.solidify.model.SolidifyApplicationRole;
import ch.unige.solidify.service.HttpRequestInfoProvider;
import ch.unige.solidify.util.PropagateRestClientTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.TrustedRestClientTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.display.StructureValidationRight;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.PersonAvatar;
import ch.unige.aou.model.settings.ValidationRight;
import ch.unige.aou.repository.ApplicationRoleRepository;
import ch.unige.aou.repository.NotificationTypeRepository;
import ch.unige.aou.repository.UserRepository;
import ch.unige.aou.specification.UserSpecification;

@Service
@Primary
@ConditionalOnBean(AdminController.class)
public class UserService extends AouResourceService<User> implements UserCreatorService, UserInfoLocalService {
  static final Logger log = LoggerFactory.getLogger(UserService.class);

  private static final String USERS_PATH = "/users/";
  private static final String DELETE_TOKEN_PATH = "/delete-token";
  private static final String CHANGE_ROLE_PATH = "/change-role";
  private static final String CHANGE_ORCID_ENDPOINT = "/change-orcid";

  private final UserRepository userRepository;
  private final PersonService personService;
  private final ApplicationRoleRepository applicationRoleRepository;
  private final NotificationTypeRepository notificationTypeRepository;
  private final HttpRequestInfoProvider httpRequestInfoProvider;
  private final TrustedRestClientTool trustedRestClientTool;
  private final PropagateRestClientTool propagateRestClientTool;
  private final PublicationService publicationService;
  private final EventService eventService;
  private final NotificationService notificationService;
  private final CommentService commentService;
  private final PersonValidationRightStructureService personValidationRightStructureService;
  private final StoredSearchService storedSearchService;
  private final OrcidSynchronizationService orcidSynchronizationService;

  private final String authModuleUrl;
  private final String[] institutionsAuthorized;

  public UserService(AouProperties aouProperties, UserRepository userRepository, @Lazy PersonService personService,
          ApplicationRoleRepository applicationRoleRepository,
          NotificationTypeRepository notificationTypeRepository, TrustedRestClientTool trustedRestClientTool,
          HttpRequestInfoProvider httpRequestInfoProvider, AuthorizationClientProperties authProperties,
          PropagateRestClientTool propagateRestClientTool, @Lazy PublicationService publicationService, @Lazy EventService eventService,
          @Lazy NotificationService notificationService, @Lazy CommentService commentService,
          PersonValidationRightStructureService personValidationRightStructureService, @Lazy StoredSearchService storedSearchService,
          @Lazy OrcidSynchronizationService orcidSynchronizationService) {
    this.userRepository = userRepository;
    this.personService = personService;
    this.applicationRoleRepository = applicationRoleRepository;
    this.notificationTypeRepository = notificationTypeRepository;
    this.httpRequestInfoProvider = httpRequestInfoProvider;
    this.trustedRestClientTool = trustedRestClientTool;
    this.propagateRestClientTool = propagateRestClientTool;
    this.publicationService = publicationService;
    this.authModuleUrl = authProperties.getAuthorizationServerUrl();
    this.eventService = eventService;
    this.notificationService = notificationService;
    this.commentService = commentService;
    this.personValidationRightStructureService = personValidationRightStructureService;
    this.storedSearchService = storedSearchService;
    this.institutionsAuthorized = aouProperties.getParameters().getAuthorizedInstitutions();
    this.orcidSynchronizationService = orcidSynchronizationService;
  }

  @Override
  public UserSpecification getSpecification(User resource) {
    return new UserSpecification(resource);
  }

  @Override
  public User findByExternalUid(String externalUid) {
    User user = this.userRepository.findByExternalUid(externalUid);
    if (user == null) {
      throw new NoSuchElementException("No user with external id " + externalUid);
    }
    return user;
  }

  public boolean existsByExternalUid(String externalUid) {
    return this.userRepository.existsByExternalUid(externalUid);
  }

  public List<User> findByPersonResId(String personId) {
    return this.userRepository.findByPersonResId(personId);
  }

  public List<User> findAdminOrRootUsers() {
    return this.userRepository.findAdminOrRootUsers();
  }

  public void revokeAccessAndRefreshTokens(User user) {
    RestTemplate restTemplate = this.trustedRestClientTool.getClient();
    restTemplate.delete(this.authModuleUrl + USERS_PATH + user.getExternalUid() + DELETE_TOKEN_PATH);
  }

  @Override
  public User save(User user) {
    if (user.getApplicationRole() == null) {
      user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
    }
    return super.save(user);
  }

  public void createTestUser(AouProperties.Test.User user, Person person) {
    String name = user.getName().toUpperCase();
    String mail = NO_REPLY_PREFIX + name + "@unige.ch";
    User userDb = this.userRepository.findByEmail(mail);
    if (userDb == null) {
      User newUser = new User();
      newUser.setResId(user.getName().toLowerCase());
      newUser.setFirstName(AouConstants.PERMANENT_TEST_DATA_LABEL + name + "_firstName");
      newUser.setLastName(AouConstants.PERMANENT_TEST_DATA_LABEL + name + "_lastName");
      newUser.setEmail(mail);
      newUser.setHomeOrganization(name + "_homeOrganization");
      newUser.setExternalUid(name + AouConstants.TEST_EXTERNAL_UID_SUFFIX);
      newUser.setPerson(person);
      if (user.getRole().equalsIgnoreCase(AuthApplicationRole.ADMIN.getName())) {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
      } else if (user.getRole().equalsIgnoreCase(AuthApplicationRole.ROOT.getName())) {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
      } else if (user.getRole().equalsIgnoreCase(AuthApplicationRole.TRUSTED_CLIENT.getName())) {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.TRUSTED_CLIENT));
      } else {
        newUser.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
      }
      User createdUser = this.save(newUser);
      log.info("Test user '{}' created", createdUser.getResId());
    }
  }

  @Override
  @Transactional
  public synchronized String createNewUser(String externalUid) {
    if (!this.userRepository.existsByExternalUid(externalUid)) {
      final AuthUserDto authUserDto = this.getAuthenticatedUser(externalUid);
      // Create a new user
      final User newUser = new User(authUserDto);
      // Update Last Login
      this.updateLastLogin(newUser, authUserDto);
      //Check if there is a person with the same orcid
      if (authUserDto.getOrcid() != null && authUserDto.isVerifiedOrcid()) {
        Person existingPerson = this.personService.findByOrcid(authUserDto.getOrcid());
        if (existingPerson != null && existingPerson.getOrcid().equals(authUserDto.getOrcid()) && existingPerson.isVerifiedOrcid()) {
          // Link new user to the existing person
          newUser.setPerson(existingPerson);
          // Save user
          this.userRepository.save(newUser);
          return externalUid;
        }
      }
      // Create Person
      Person newPerson = new Person();
      newPerson.setFirstName(newUser.getFirstName());
      newPerson.setLastName(newUser.getLastName());
      // Set ORCID for the person if the user has a verified one
      if (authUserDto.getOrcid() != null && authUserDto.isVerifiedOrcid()) {
        newPerson.setOrcid(authUserDto.getOrcid());
        newPerson.setVerifiedOrcid(true);
      }
      this.addNotificationType(newPerson, NotificationType.MY_PUBLICATION_VALIDATED.getResId());
      this.addNotificationType(newPerson, NotificationType.MY_INDIRECT_PUBLICATION_VALIDATED.getResId());
      this.addNotificationType(newPerson, NotificationType.MY_INDIRECT_PUBLICATION_COMMENTED.getResId());
      this.addNotificationType(newPerson, NotificationType.PUBLICATION_FORGOTTEN_IN_PROGRESS.getResId());
      this.addNotificationType(newPerson, NotificationType.PUBLICATION_FORGOTTEN_TO_VALIDATE.getResId());
      this.addNotificationType(newPerson, NotificationType.MY_PUBLICATION_FEEDBACK_REQUIRED.getResId());
      this.addNotificationType(newPerson, NotificationType.MY_PUBLICATION_COMMENTED.getResId());
      this.addNotificationType(newPerson, NotificationType.MY_PUBLICATION_REJECTED.getResId());
      this.addNotificationType(newPerson, NotificationType.PUBLICATION_TO_VALIDATE.getResId());
      this.addNotificationType(newPerson, NotificationType.PUBLICATION_TO_VALIDATE_COMMENTED.getResId());
      this.addNotificationType(newPerson, NotificationType.PUBLICATION_DETECTED_ON_ORCID.getResId());
      this.addNotificationType(newPerson, NotificationType.PUBLICATION_EXPORTED_TO_ORCID.getResId());

      this.personService.save(newPerson);
      newUser.setPerson(newPerson);
      // Save user
      this.userRepository.save(newUser);
    }
    return externalUid;
  }

  public User getCurrentUser() {
    final Principal principal = this.httpRequestInfoProvider.getPrincipal();
    return this.findByExternalUid(principal.getName());
  }

  public void updateUser(User user) {
    final AuthUserDto authUserDto = this.getAuthenticatedUser(user.getExternalUid());
    this.updateLastLogin(user, authUserDto);
    this.updateApplicationRole(user, authUserDto);
    this.updateOrcid(user, authUserDto);
  }

  public void updateUserRole(String externalUid, String role) {
    RestTemplate restTemplate = this.propagateRestClientTool.getClient();
    try {
      restTemplate.postForObject(this.authModuleUrl + USERS_PATH + externalUid + CHANGE_ROLE_PATH, role, AuthUserDto.class);
    } catch (HttpClientErrorException.Forbidden e) {
      throw new AccessDeniedException("Forbidden to set role " + role + " for user " + externalUid);
    } catch (RestClientException e) {
      throw new SolidifyRuntimeException("Unable to change role " + role + " for user " + externalUid);
    }
  }

  public String getApplicationRoleId(Map<String, Object> updateMap) {
    if (!this.containsApplicationRoleId(updateMap)) {
      return null;
    }
    return (String) ((Map<String, Object>) updateMap.get(APPLICATION_ROLE_FIELD)).get(RES_ID_FIELD);
  }

  public boolean containsApplicationRoleId(Map<String, Object> updateMap) {
    if (!updateMap.containsKey(APPLICATION_ROLE_FIELD) ||
            !(updateMap.get(APPLICATION_ROLE_FIELD) instanceof Map)) {
      return false;
    }
    final Map<String, Object> applicationRoleMap = (Map<String, Object>) updateMap.get(APPLICATION_ROLE_FIELD);
    return applicationRoleMap.containsKey(RES_ID_FIELD)
            && ((applicationRoleMap.get(RES_ID_FIELD)) instanceof String || applicationRoleMap.get(RES_ID_FIELD) == null);
  }

  @Transactional
  public boolean mergeUsersAndOrcid(String userIdToKeep, String userIdToMerge) {
    User userToKeep = this.findOne(userIdToKeep);
    User userToMerge = this.findOne(userIdToMerge);

    String orcidFromUserToMerge = userToMerge.getPerson().getOrcid();
    Boolean verifiedOrcidFromUserToMerge = userToMerge.getPerson().isVerifiedOrcid();
    OrcidToken orcidTokenFromUserToMerge = userToMerge.getPerson().getOrcidToken();

    if (this.mergeUsers(userToKeep, userToMerge)) {
      // If the person kept doesn't have any verified ORCID nor token, but person deleted got one, keep it.
      if (Boolean.TRUE.equals(verifiedOrcidFromUserToMerge) && orcidTokenFromUserToMerge != null
              && (!userToKeep.getPerson().isVerifiedOrcid() || userToKeep.getPerson().getOrcidToken() == null)) {
        userToKeep.getPerson().setOrcid(orcidFromUserToMerge);
        userToKeep.getPerson().setVerifiedOrcid(true);
        userToKeep.getPerson().setOrcidToken(this.cloneOrcidToken(orcidTokenFromUserToMerge));
        this.personService.save(userToKeep.getPerson());
      }
      return true;
    }
    return false;
  }

  private OrcidToken cloneOrcidToken(OrcidToken token) {
    OrcidToken clonedToken = new OrcidToken();
    clonedToken.setAccessToken(token.getAccessToken());
    clonedToken.setOrcid(token.getOrcid());
    clonedToken.setRefreshToken(token.getRefreshToken());
    clonedToken.setName(token.getName());
    clonedToken.setScope(token.getScope());
    clonedToken.setExpiresIn(token.getExpiresIn());
    return clonedToken;
  }

  @Transactional
  public boolean mergeUsers(User userToKeep, User userToMerge) {
    if (!userToKeep.getPerson().getResId().equals(userToMerge.getPerson().getResId())) {
      // Need to update all the deposits from person linked to userToMerge to the person linked to user
      this.publicationService.changePublicationCreator(userToKeep.getPerson(), userToMerge.getPerson());
      // Need to update all the notifications and events related to the userToMerge
      this.eventService.changeTriggerByPerson(userToKeep.getPerson(), userToMerge.getPerson());
      this.notificationService.changeNotificationRecipient(userToKeep.getPerson(), userToMerge.getPerson());
      this.commentService.changeCommentsPerson(userToKeep.getPerson(), userToMerge.getPerson());
      this.storedSearchService.changeStoredSearchCreator(userToKeep.getPerson(), userToMerge.getPerson());
      this.orcidSynchronizationService.changePersonId(userToKeep.getPerson(), userToMerge.getPerson());

      // Merge of profiles
      Person personToDelete = userToMerge.getPerson();
      Person personToKeep = userToKeep.getPerson();
      personToDelete.getStructures().forEach(personToKeep::addStructure);
      personToDelete.getResearchGroups().forEach(personToKeep::addResearchGroup);
      personToDelete.getSubscribedNotifications().forEach(personToKeep::addNotificationType);
      personToDelete.getFavoritePublications().forEach(personToKeep::addFavoritePublication);
      if (personToKeep.getAvatar() == null && personToDelete.getAvatar() != null) {
        personToKeep.setAvatar(new PersonAvatar(personToDelete.getAvatar()));
      }
      this.personService.save(personToKeep);

      List<StructureValidationRight> structureValidationRightList = this.personService.getStructureValidationRights(personToDelete.getResId());
      structureValidationRightList.stream().forEach(svr -> {
        List<ValidationRight> validationRightList = this.personValidationRightStructureService.findAllRelations(personToDelete.getResId(),
                svr.getResId());
        validationRightList.stream().forEach(validationRight -> {
          if (!this.personValidationRightStructureService.relationExists(personToKeep.getResId(), svr.getResId(),
                  validationRight.getPublicationSubtype().getResId())) {
            this.personValidationRightStructureService.saveRelation(personToKeep.getResId(), svr.getResId(),
                    validationRight.getPublicationSubtype().getResId(), this.personValidationRightStructureService.getDefaultJoinResource());
          }
        });
      });

      // change role from both users to the higher one(lower level) in case it is not the same
      if (!userToKeep.getApplicationRole().getResId().equals(userToMerge.getApplicationRole().getResId())) {
        if (userToKeep.getApplicationRole().getLevel() < userToMerge.getApplicationRole().getLevel()) {
          this.mergeUserRoles(userToMerge, userToKeep.getApplicationRole().getResId());
        } else {
          this.mergeUserRoles(userToKeep, userToMerge.getApplicationRole().getResId());
        }
      }

      // Change person linked to userToMerge to person linked to user
      userToMerge.setPerson(userToKeep.getPerson());
      this.save(userToMerge);

      // Deleting the person below seems to be not sufficient to prevent a duplicate entry exception on the ORCID value, even if
      // the person is deleted within the transaction (maybe related to https://github.com/spring-projects/spring-data-jpa/issues/1100)
      // Setting the ORCID to null and saving the person before deleting it solves the problem.
      personToDelete.setOrcid(null);
      this.personService.save(personToDelete);

      this.personService.delete(personToDelete.getResId());
      return true;
    } else {
      return false;
    }
  }

  public AuthUserDto getAuthenticatedUser(String externalUid) {
    RestTemplate restTemplate = this.trustedRestClientTool.getClient();
    AuthUserDto authUser = restTemplate.getForEntity(this.authModuleUrl + USERS_PATH + externalUid, AuthUserDto.class).getBody();
    if (authUser == null) {
      throw new IllegalStateException();
    }
    return authUser;
  }

  private void updateOrcid(User user, AuthUserDto authUserDto) {
    Person person = user.getPerson();
    if (!StringTool.isNullOrEmpty(authUserDto.getOrcid())
            && (StringTool.isNullOrEmpty(person.getOrcid()) || authUserDto.isVerifiedOrcid() && !person.isVerifiedOrcid())) {
      // If ORCID in application is null or if it is not verified while auth server knows a verified one -> set ORCID in local db
      person.setOrcid(authUserDto.getOrcid());
      person.setVerifiedOrcid(authUserDto.isVerifiedOrcid());
    }
  }

  private void addNotificationType(Person person, String notificationTypeId) {
    Optional<NotificationType> notificationTypeOptional = this.notificationTypeRepository.findById(notificationTypeId);
    if (notificationTypeOptional.isPresent()) {
      person.addNotificationType(notificationTypeOptional.get());
    } else {
      throw new NoSuchElementException("No NotificationType with id " + notificationTypeId);
    }
  }

  private void updateLastLogin(User user, AuthUserDto authUserDto) {
    if (authUserDto.getLastLogin() != null) {
      user.setLastLoginIpAddress(authUserDto.getLastLogin().getLoginIpAddress());
      user.setLastLoginTime(authUserDto.getLastLogin().getLoginTime());
    } else if (authUserDto.getFirstLogin() != null) {
      user.setLastLoginIpAddress(authUserDto.getFirstLogin().getLoginIpAddress());
      user.setLastLoginTime(authUserDto.getFirstLogin().getLoginTime());
    }
  }

  private void updateApplicationRole(User user, AuthUserDto authUserDto) {
    if (authUserDto.getApplicationRole() == null) {
      throw new SolidifyRuntimeException("User DTO (" + authUserDto.getExternalUid() + ") has a null application role");
    }
    if (!this.haveSameRole(user, authUserDto)) {
      Optional<SolidifyApplicationRole> role = this.applicationRoleRepository.findById(authUserDto.getApplicationRole().getResId());
      if (role.isPresent()) {
        user.setApplicationRole(role.get());
      } else {
        throw new SolidifyRuntimeException("Cannot find application role (" + authUserDto.getApplicationRole().getResId() + ")");
      }
    }
  }

  private boolean haveSameRole(User user, AuthUserDto authUserDto) {
    return user.getApplicationRole().getResId().equals(authUserDto.getApplicationRole().getResId());
  }

  private void mergeUserRoles(User userToUpdate, String applicationRoleId) {
    this.updateUserRole(userToUpdate.getExternalUid(), applicationRoleId);
    this.updateUser(userToUpdate);
  }

  public boolean isUserMemberOfAuthorizedInstitutions() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    final User user = this.findByExternalUid(authentication.getName());
    return Arrays.stream(this.institutionsAuthorized).distinct().anyMatch(u -> u.equals(user.getHomeOrganization()));
  }

  @Override
  public void saveAuthUserDto(UserInfo userInfo, AuthUserDto authUserDto) {
    User user = (User) userInfo;
    user.setAuthUserDto(authUserDto);
    this.save(user);
  }

  @Override
  public void saveUserPersonInfo(UserInfo userInfo, PersonInfo personInfo) {
    User user = (User) userInfo;
    Person person = user.getPerson();
    person.setOrcid(personInfo.getOrcid());
    person.setVerifiedOrcid(personInfo.isVerifiedOrcid());
    this.personService.save(person);

    // reindex person's publication to index its eventual ORCID
    this.personService.reindexUnigePersonsPublications(person.getResId());
  }
}
