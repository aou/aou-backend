/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - StoredSearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.search.StoredSearch;
import ch.unige.aou.model.search.StoredSearchCriteria;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.repository.StoredSearchRepository;
import ch.unige.aou.specification.StoredSearchSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class StoredSearchService extends AouResourceService<StoredSearch> {

  private static final String CRITERIA = "criteria";

  private static final String ERROR_CRITERIA_EMPTY = "stored_searches.error.empty_criteria_list";
  private static final String ERROR_NAME_ALREADY_EXISTS_FOR_PERSON = "stored_searches.error.name_already_exists_for_person";
  private static final String ERROR_FIELD_EMPTY = "stored_searches.error.criteria.field_empty";
  private static final String ERROR_TERM_EMPTY_VALUES = "stored_searches.error.criteria.term_empty_values";
  private static final String ERROR_TERM_BOTH_FILLED_VALUES = "stored_searches.error.criteria.term_both_values";
  private static final String ERROR_EMPTY_VALUE = "stored_searches.error.criteria.empty_value";
  private static final String ERROR_RANGE_EMPTY_VALUES = "stored_searches.error.criteria.range_empty_values";
  private static final String ERROR_NESTED_BOOLEAN_UNSUPPORTED = "stored_searches.error.criteria.nested_boolean_unsupported";

  private PersonService personService;

  private final ObjectMapper mapper = new ObjectMapper();

  public StoredSearchService(PersonService personService) {
    this.personService = personService;

    this.mapper.registerModule(new JavaTimeModule());
    this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  public Optional<StoredSearch> findByNameAndCreatorResId(String name, String personId) {
    return ((StoredSearchRepository) this.itemRepository).findByNameAndCreatorResId(name, personId);
  }

  /**
   * Update the Search and replace the criteria list by the given one (which is not done by patchResource() method).
   * <p>
   * This is done this way so we don't have to use an AssociationController to manage the criteria list.
   *
   * @param id
   * @param updateMap
   * @return
   */
  public StoredSearch updateWithCriteria(String id, Map<String, Object> updateMap) {
    StoredSearch storedSearch = this.findOne(id);

    if (!storedSearch.isModifiable()) {
      throw new SolidifyUnmodifiableException(
              this.messageService.get("validation.resource.unmodifiable",
                      new Object[] { storedSearch.getClass().getSimpleName() + ": " + id }));
    }

    this.patchResource(storedSearch, updateMap);

    if (updateMap.get(CRITERIA) != null) {
      try {
        storedSearch.getCriteria().clear();
        String mapJson = this.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(updateMap.get(CRITERIA));
        List<StoredSearchCriteria> newCriteria = this.mapper.readValue(mapJson, new TypeReference<>() {
        });
        storedSearch.getCriteria().addAll(newCriteria);
      } catch (JsonProcessingException e) {
        throw new SolidifyRuntimeException("unable to parse search criteria list", e);
      }
    }

    return this.save(storedSearch);
  }

  @Override
  protected void beforeValidate(StoredSearch storedSearch) {

    if (storedSearch.getCreator() == null) {
      Person person = this.personService.getLinkedPerson(SecurityContextHolder.getContext().getAuthentication());
      storedSearch.setCreator(person);
    }

    for (StoredSearchCriteria criteria : storedSearch.getCriteria()) {
      criteria.clean();
      criteria.setSearch(storedSearch);
    }
  }

  @Override
  protected void validateItemSpecificRules(StoredSearch storedSearch, BindingResult errors) {

    if (storedSearch.getCriteria().isEmpty()) {
      errors.addError(
              new FieldError(StoredSearch.class.getSimpleName(), CRITERIA,
                      this.messageService.get(ERROR_CRITERIA_EMPTY)));
    }

    Optional<StoredSearch> researchOpt = this.findByNameAndCreatorResId(storedSearch.getName(), storedSearch.getCreator().getResId());
    if (researchOpt.isPresent() && !researchOpt.get().getResId().equals(storedSearch.getResId())) {
      errors.addError(
              new FieldError(StoredSearch.class.getSimpleName(), "name",
                      this.messageService.get(ERROR_NAME_ALREADY_EXISTS_FOR_PERSON, new Object[] { storedSearch.getName() })));
    }

    this.validateCriteria(storedSearch, errors);
  }

  private void validateCriteria(StoredSearch storedSearch, BindingResult errors) {
    int index = 0;
    for (StoredSearchCriteria criteria : storedSearch.getCriteria()) {

      if (StringTool.isNullOrEmpty(criteria.getField())) {
        errors.addError(
                new FieldError(StoredSearch.class.getSimpleName(), CRITERIA + "." + index + ".field",
                        this.messageService.get(ERROR_FIELD_EMPTY)));
      }

      if (criteria.getType() == SearchConditionType.RANGE) {
        this.validateRangeCriteria(criteria, errors, index);
      } else if (criteria.getType() == SearchConditionType.TERM) {
        this.validateTermCriteria(criteria, errors, index);
      } else if (criteria.getType() == SearchConditionType.MATCH || criteria.getType() == SearchConditionType.QUERY
              || criteria.getType() == SearchConditionType.SIMPLE_QUERY) {
        this.validateCriteriaValue(criteria, errors, index);
      } else if (criteria.getType() == SearchConditionType.NESTED_BOOLEAN) {
        errors.addError(
                new FieldError(StoredSearch.class.getClass().getSimpleName(), CRITERIA + "." + index + ".type",
                        this.messageService.get(ERROR_NESTED_BOOLEAN_UNSUPPORTED)));
      }

      index++;
    }
  }

  private void validateRangeCriteria(StoredSearchCriteria criteria, BindingResult errors, int index) {
    if (StringTool.isNullOrEmpty(criteria.getLowerValue()) && StringTool.isNullOrEmpty(criteria.getUpperValue())) {
      errors.addError(
              new FieldError(StoredSearch.class.getClass().getSimpleName(), CRITERIA + "." + index + ".lowerValue",
                      this.messageService.get(ERROR_RANGE_EMPTY_VALUES)));
      errors.addError(
              new FieldError(StoredSearch.class.getClass().getSimpleName(), CRITERIA + "." + index + ".upperValue",
                      this.messageService.get(ERROR_RANGE_EMPTY_VALUES)));
    }
  }

  private void validateTermCriteria(StoredSearchCriteria criteria, BindingResult errors, int index) {
    // no search value given 
    if (StringTool.isNullOrEmpty(criteria.getValue()) && criteria.getTerms().isEmpty()) {
      errors.addError(
              new FieldError(StoredSearch.class.getSimpleName(), CRITERIA + "." + index + ".value",
                      this.messageService.get(ERROR_TERM_EMPTY_VALUES)));
      errors.addError(
              new FieldError(StoredSearch.class.getSimpleName(), CRITERIA + "." + index + ".terms",
                      this.messageService.get(ERROR_TERM_EMPTY_VALUES)));
    }
    // too many fields filled
    if (!StringTool.isNullOrEmpty(criteria.getValue()) && !criteria.getTerms().isEmpty()) {
      errors.addError(
              new FieldError(StoredSearch.class.getSimpleName(), CRITERIA + "." + index + ".value",
                      this.messageService.get(ERROR_TERM_BOTH_FILLED_VALUES)));
      errors.addError(
              new FieldError(StoredSearch.class.getSimpleName(), CRITERIA + "." + index + ".terms",
                      this.messageService.get(ERROR_TERM_BOTH_FILLED_VALUES)));
    }
  }

  private void validateCriteriaValue(StoredSearchCriteria criteria, BindingResult errors, int index) {
    if (StringTool.isNullOrEmpty(criteria.getValue())) {
      errors.addError(
              new FieldError(StoredSearch.class.getSimpleName(), CRITERIA + "." + index + ".value",
                      this.messageService.get(ERROR_EMPTY_VALUE)));
    }
  }

  @Override
  public SolidifySpecification<StoredSearch> getSpecification(StoredSearch storedSearch) {
    return new StoredSearchSpecification(storedSearch);
  }

  public void changeStoredSearchCreator(Person creatorToKeep, Person creatorToUpdate) {
    List<StoredSearch> storedSearchesList = ((StoredSearchRepository) this.itemRepository).findByCreatorResId(creatorToUpdate.getResId());
    for (StoredSearch storedSearch : storedSearchesList) {
      storedSearch.setCreator(creatorToKeep);
      this.itemRepository.save(storedSearch);
    }
  }
}
