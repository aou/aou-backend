/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - SwissNationalLibraryUploadService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.xml.orcid.v3_0.activities.WorkGroup;
import ch.unige.solidify.model.xml.orcid.v3_0.activities.Works;
import ch.unige.solidify.model.xml.orcid.v3_0.common.ExternalId;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Work;
import ch.unige.solidify.model.xml.orcid.v3_0.work.WorkSummary;
import ch.unige.solidify.service.OrcidService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.ExistingPublicationInfo;
import ch.unige.aou.model.exception.PublicationAlreadyExistsInOrcidProfileException;
import ch.unige.aou.model.exception.PublicationTooOldForOrcidException;
import ch.unige.aou.model.exception.PublicationWithoutExternalIdentifierException;
import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.model.orcid.ExternalIdentifiers;
import ch.unige.aou.model.orcid.ImportedIdentifiers;
import ch.unige.aou.model.orcid.OrcidSynchronization;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.repository.OrcidSynchronizationRepository;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.OrcidSynchronizerService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.metadata.exports.OrcidWorkAdapter;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.specification.OrcidSynchronizationSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class OrcidSynchronizationService extends AouResourceService<OrcidSynchronization> {

  static final Logger log = LoggerFactory.getLogger(OrcidSynchronizationService.class);

  private static final Integer PUBLICATION_LIMIT_DATE = 1900;

  private final PersonService personService;
  private final OrcidService orcidService;

  private final OrcidSynchronizerService orcidSynchronizerService;

  private final DuplicateService duplicateService;

  private final PublicationService publicationService;

  private final EventService eventService;

  private final MetadataService metadataService;

  private final OrcidWorkAdapter orcidWorkAdapter;

  private final List<String> rolesToSendToOrcidProfile;

  public OrcidSynchronizationService(AouProperties properties, PersonService personService, OrcidService orcidService,
          OrcidSynchronizerService orcidSynchronizerService, DuplicateService duplicateService, PublicationService publicationService,
          EventService eventService, MetadataService metadataService, OrcidWorkAdapter orcidWorkAdapter) {
    this.personService = personService;
    this.orcidService = orcidService;
    this.orcidSynchronizerService = orcidSynchronizerService;
    this.duplicateService = duplicateService;
    this.publicationService = publicationService;
    this.eventService = eventService;
    this.metadataService = metadataService;
    this.orcidWorkAdapter = orcidWorkAdapter;
    this.rolesToSendToOrcidProfile = List.of(properties.getOrcid().getRolesToSendToOrcidProfile());
  }

  @Override
  public OrcidSynchronizationSpecification getSpecification(OrcidSynchronization resource) {
    return new OrcidSynchronizationSpecification(resource);
  }

  public List<OrcidSynchronization> findByPersonResIdAndPublicationResId(String personId, String publicationId) {
    return ((OrcidSynchronizationRepository) this.itemRepository).findByPersonResIdAndPublicationResId(personId, publicationId);
  }

  public Optional<OrcidSynchronization> findByPersonResIdAndPutCode(String personId, BigInteger putCode) {
    return ((OrcidSynchronizationRepository) this.itemRepository).findByPersonResIdAndPutCode(personId, putCode);
  }

  public List<OrcidSynchronization> findByPersonResId(String personId) {
    return ((OrcidSynchronizationRepository) this.itemRepository).findByPersonResId(personId);
  }

  public void changePersonId(Person creatorToKeep, Person creatorToUpdate) {
    List<OrcidSynchronization> synchronizations = this.findByPersonResId(creatorToUpdate.getResId());
    for (OrcidSynchronization orcidSynchronization : synchronizations) {
      orcidSynchronization.setPerson(creatorToKeep);
      this.itemRepository.save(orcidSynchronization);
    }
  }

  public void synchronizeAllPeopleFromOrcidProfile(OffsetDateTime lastCheckDate) {
    List<Person> peopleWithOrcidToken = this.personService.findPeopleWithOrcidToken();
    ImportedIdentifiers importedIdentifiers = new ImportedIdentifiers();
    for (Person personWithOrcidToken : peopleWithOrcidToken) {
      this.synchronizeFromOrcidProfileForPerson(personWithOrcidToken, lastCheckDate, importedIdentifiers);
    }
  }

  @Async
  public void synchronizeAllFromOrcidProfileForPerson(Person person) {
    log.info("Full synchronization from ORCID profile will start for person {}", person);
    ImportedIdentifiers importedIdentifiers = new ImportedIdentifiers();
    OffsetDateTime lastCheckDate = OffsetDateTime.now().minusYears(100);
    this.synchronizeFromOrcidProfileForPerson(person, lastCheckDate, importedIdentifiers);
  }

  public List<OrcidSynchronization> synchronizeFromOrcidProfileForPerson(Person person, OffsetDateTime lastCheckDate,
          ImportedIdentifiers alreadyImportedIdentifiers) {
    if (person.getOrcidToken() != null) {

      Works works = this.orcidService.getWorks(person.getOrcidToken());
      List<OrcidSynchronization> orcidSynchronizationsByIdToNotify = this.importAllOrcidWorksWithExternalId(person, works, lastCheckDate,
              alreadyImportedIdentifiers);
      List<OrcidSynchronization> newPublicationsOrcidSynchronizationsWithoutId = this.importAllOrcidWorksWithoutExternalId(person, works,
              lastCheckDate);
      List<OrcidSynchronization> orcidSynchronizationsToNotify = new ArrayList<>();
      orcidSynchronizationsToNotify.addAll(orcidSynchronizationsByIdToNotify);
      orcidSynchronizationsToNotify.addAll(newPublicationsOrcidSynchronizationsWithoutId);

      this.notifyNewPublicationsOrcidSynchronizations(orcidSynchronizationsToNotify, person);
      return orcidSynchronizationsToNotify;
    } else {
      log.warn("Person {} ({}) doesn't have any ORCID token", person.getResId(), person.getFullName());
      return Collections.emptyList();
    }
  }

  private void notifyNewPublicationsOrcidSynchronizations(List<OrcidSynchronization> newPublicationsOrcidSynchronizations, Person person) {
    for (OrcidSynchronization orcidSynchronization : newPublicationsOrcidSynchronizations) {
      Publication publication = this.publicationService.findOne(orcidSynchronization.getPublication().getResId());
      EventType eventType = null;
      if (orcidSynchronization.getDownloadDate() != null) {
        eventType = EventType.PUBLICATION_DETECTED_ON_ORCID;
      } else if (orcidSynchronization.getUploadDate() != null) {
        eventType = EventType.PUBLICATION_EXPORTED_TO_ORCID;
      }
      if (eventType != null) {
        this.eventService.createEvent(publication, eventType, person);
      } else {
        log.error("OrcidSynchronization {} doesn't have any upload nor download date. A notification cannot be created for it",
                orcidSynchronization.getResId());
      }
    }
  }

  private List<OrcidSynchronization> importAllOrcidWorksWithExternalId(Person person, Works works, OffsetDateTime lastCheckDate,
          ImportedIdentifiers alreadyImportedIdentifiers) {
    HashMap<BigInteger, ExternalIdentifiers> worksToImportByExternalId = this.getWorksToImportByExternalId(works, lastCheckDate);

    List<OrcidSynchronization> orcidSynchronizationsToNotify = new ArrayList<>();
    worksToImportByExternalId.forEach((putCode, externalIdentifiers) -> {

      OrcidSynchronization newOrcidSynchronization = this.orcidSynchronizerService.importOrcidWorkWithExternalId(putCode, externalIdentifiers,
              person, alreadyImportedIdentifiers);
      if (newOrcidSynchronization != null) {
        orcidSynchronizationsToNotify.add(newOrcidSynchronization);
      }
    });

    return orcidSynchronizationsToNotify;
  }

  private List<OrcidSynchronization> importAllOrcidWorksWithoutExternalId(Person person, Works works, OffsetDateTime lastCheckDate) {
    Map<BigInteger, WorkSummary> workSummaries = this.getWorksToImportWithoutExternalId(works, lastCheckDate);

    List<OrcidSynchronization> newOrcidSynchronizations = new ArrayList<>();

    workSummaries.forEach((putCode, workSummary) -> {
      OrcidSynchronization newOrcidSynchronization = this.orcidSynchronizerService.importOrcidWorkWithoutExternalId(putCode, workSummary,
              person);
      if (newOrcidSynchronization != null) {
        newOrcidSynchronizations.add(newOrcidSynchronization);
      }
    });

    return newOrcidSynchronizations;
  }

  public OrcidSynchronization storeOrcidSynchronization(Person person, BigInteger putCode, String publicationId, OffsetDateTime downloadDate,
          OffsetDateTime uploadDate) {
    OrcidSynchronization orcidSync = new OrcidSynchronization();
    orcidSync.setPerson(person);
    Publication publication = this.publicationService.findOne(publicationId);
    orcidSync.setPublication(publication);
    orcidSync.setPutCode(putCode);
    orcidSync.setDownloadDate(downloadDate);
    orcidSync.setUploadDate(uploadDate);
    return this.save(orcidSync);
  }

  public ExistingPublicationInfo getExistingPublicationInfo(ExternalIdentifiers externalIdentifiers) {
    ExistingPublicationInfo existingPublicationInfo = null;

    if (!StringTool.isNullOrEmpty(externalIdentifiers.getPmid())) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByPmid(externalIdentifiers.getPmid());
    } else if (!StringTool.isNullOrEmpty(externalIdentifiers.getDoi())) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByDoi(externalIdentifiers.getDoi());
    } else if (!StringTool.isNullOrEmpty(externalIdentifiers.getIsbn())) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByIsbn(externalIdentifiers.getIsbn());
    } else if (!StringTool.isNullOrEmpty(externalIdentifiers.getArxivId())) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByArxivId(externalIdentifiers.getArxivId());
    }
    return existingPublicationInfo;
  }

  /**
   * Return a Map of Works having at least one external identifier and modified after the lastCheckDate.
   * The map contains each putCode as key, associated to its external identifiers (doi, pmid, ...)
   *
   * @param works
   * @param lastCheckDate
   * @return
   */
  private HashMap<BigInteger, ExternalIdentifiers> getWorksToImportByExternalId(Works works, OffsetDateTime lastCheckDate) {
    HashMap<BigInteger, ExternalIdentifiers> externalIdentifiersToImport = new HashMap<>();

    // Get identifier that can be imported
    for (WorkGroup workGroup : works.getGroup()) {
      for (WorkSummary workSummary : workGroup.getWorkSummary()) {
        OffsetDateTime lastModifiedDate = workSummary.getLastModifiedDate().getValue();
        if (lastModifiedDate.isAfter(lastCheckDate)) {
          ExternalIdentifiers externalIdentifiers = this.getSupportedExternalIdentifiers(workSummary);
          if (externalIdentifiers.hasValue()) {
            BigInteger putCode = workSummary.getPutCode();
            externalIdentifiersToImport.put(putCode, externalIdentifiers);
          }
        }
      }
    }
    return externalIdentifiersToImport;
  }

  /**
   * Return a Map of Works that don't have any supported external identifier and modified after the lastCheckDate
   *
   * @param works
   * @param lastCheckDate
   * @return
   */
  private Map<BigInteger, WorkSummary> getWorksToImportWithoutExternalId(Works works, OffsetDateTime lastCheckDate) {
    Map<BigInteger, WorkSummary> workSummaries = new HashMap<>();
    for (WorkGroup workGroup : works.getGroup()) {
      for (WorkSummary workSummary : workGroup.getWorkSummary()) {
        OffsetDateTime lastModifiedDate = workSummary.getLastModifiedDate().getValue();
        if (lastModifiedDate.isAfter(lastCheckDate)) {
          ExternalIdentifiers externalIdentifiers = this.getSupportedExternalIdentifiers(workSummary);
          if (!externalIdentifiers.hasValue()) {
            BigInteger putCode = workSummary.getPutCode();
            workSummaries.put(putCode, workSummary);
          }
        }
      }
    }
    return workSummaries;
  }

  private ExternalIdentifiers getSupportedExternalIdentifiers(WorkSummary workSummary) {
    ExternalIdentifiers externalIdentifiers = new ExternalIdentifiers();
    List<ExternalId> externalIds = workSummary.getExternalIds().getExternalId();
    for (ExternalId externalId : externalIds) {
      if (externalId.getExternalIdRelationship().equals("self")) {
        String identifierValue = externalId.getExternalIdValue();
        if (externalId.getExternalIdNormalized() != null) {
          identifierValue = externalId.getExternalIdNormalized().getValue();
        }
        String identifierType = externalId.getExternalIdType();
        externalIdentifiers.addValue(identifierType, identifierValue);
      }
    }
    return externalIdentifiers;
  }

  /***********************************************************************************************************************/

  @Async
  @Transactional
  public void synchronizeAllToOrcidProfileForPerson(Person person, boolean filterByContributorRole) {
    log.info("Full synchronization to ORCID profile will start for person {}", person);
    OffsetDateTime lastCheckDate = OffsetDateTime.now().minusYears(100);
    this.synchronizeToOrcidProfileForPerson(person, lastCheckDate, filterByContributorRole);
  }

  @Transactional
  public List<OrcidSynchronization> synchronizeToOrcidProfileForPerson(Person person, OffsetDateTime lastCheckDate,
          boolean filterByContributorRole) {
    List<OrcidSynchronization> orcidSynchronizationsToNotify = new ArrayList<>();
    if (person.getOrcidToken() != null) {
      // Reload Person to ensure object belongs to the current transaction (required for lazy loading)
      person = this.personService.findOne(person.getResId());
      Set<Publication> publications = this.publicationService.findCompletedPublicationsAsContributor(person, lastCheckDate);
      if (filterByContributorRole) {
        publications = this.filterByContributorRole(person, publications);
      }
      for (Publication publication : publications) {
        try {
          OrcidSynchronization newOrcidSynchronization = this.sendToOrcidProfile(person, publication.getResId());
          orcidSynchronizationsToNotify.add(newOrcidSynchronization);
        } catch (PublicationAlreadyExistsInOrcidProfileException e) {
          log.info("Publication '{}' ({}) wasn't sent to ORCID profile of {} ({}) (ORCID: {}) as it already exists", publication.getTitle(),
                  publication.getResId(), person.getFullName(), person.getResId(), person.getOrcid());
        } catch (PublicationWithoutExternalIdentifierException e) {
          log.info("Publication '{}' ({}) wasn't sent to ORCID profile of {} ({}) (ORCID: {}) as it doesn't contain any external identifier",
                  publication.getTitle(), publication.getResId(), person.getFullName(), person.getResId(), person.getOrcid());
        } catch (PublicationTooOldForOrcidException e) {
          log.info("Publication '{}' ({}) wasn't sent to ORCID profile of {} ({}) (ORCID: {}) because it is older than 1900",
                  publication.getTitle(), publication.getResId(), person.getFullName(), person.getResId(), person.getOrcid());
        }
      }
      this.notifyNewPublicationsOrcidSynchronizations(orcidSynchronizationsToNotify, person);
      return orcidSynchronizationsToNotify;
    } else {
      log.warn("Person {} ({}) doesn't have any ORCID token", person.getResId(), person.getFullName());
      return Collections.emptyList();
    }
  }

  private Set<Publication> filterByContributorRole(Person person, Set<Publication> publications) {
    Set<Publication> publicationsFilteredByRole = new HashSet<>();
    for (Publication publication : publications) {
      if (this.roleCanBeSentToOrcidProfile(person, publication)) {
        publicationsFilteredByRole.add(publication);
      }
    }
    return publicationsFilteredByRole;
  }

  private boolean roleCanBeSentToOrcidProfile(Person person, Publication publication) {
    String role = this.getContributorRole(person, publication);
    return this.rolesToSendToOrcidProfile.contains(role);
  }

  private String getContributorRole(Person person, Publication publication) {
    // A person may be linked to many users -> many cnIndividu
    List<String> cnIndividus = this.personService.getUnigeCnIndividus(person);

    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    Optional<ContributorDTO> contributorDtoOpt = metadataExtractor.getContributors(depositDoc).stream()
            .filter(abstractContributor -> abstractContributor instanceof ContributorDTO contributorDTO
                    && cnIndividus.contains(contributorDTO.getCnIndividu()))
            .map(abstractContributor -> (ContributorDTO) abstractContributor)
            .findFirst();
    return contributorDtoOpt.map(ContributorDTO::getRole).orElse(null);
  }

  public OrcidSynchronization sendToOrcidProfile(Person person, String publicationId) {
    if (person.getOrcidToken() != null) {
      // Check if the publication already exists in ORCID profile. If this is the case, we don't upload it again
      boolean doUpload = true;
      List<OrcidSynchronization> orcidSynchronizations = this.findByPersonResIdAndPublicationResId(person.getResId(), publicationId);
      // We check if one the synchronization still corresponds to an existing Work in profile
      // It may be not be the case if the user has deleted a Work in his ORCID profile, but wants to upload it again
      for (OrcidSynchronization orcidSynchronization : orcidSynchronizations) {
        try {
          this.orcidService.getWork(orcidSynchronization.getPutCode(), person.getOrcidToken());
          doUpload = false;
        } catch (SolidifyResourceNotFoundException e) {
          // If the user has deleted a Work in his ORCID profile, we delete the synchronization
          log.warn("Work with putCode {} doesn't exist anymore in ORCID profile of person {}", orcidSynchronization.getPutCode(), person);
          this.delete(orcidSynchronization.getResId());
          log.info("OrcidSynchronization corresponding to Work with putCode {} has been deleted", orcidSynchronization.getPutCode());
        }
      }

      final Publication publication = this.publicationService.findOne(publicationId);

      if (doUpload) {
        final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
        final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());

        Integer globalYear = metadataExtractor.getGlobalYear(depositDoc);
        if (globalYear == null || globalYear < PUBLICATION_LIMIT_DATE) {
          throw new PublicationTooOldForOrcidException();
        }

        String role = this.getContributorRole(person, publication);

        Work work = new Work();
        this.orcidWorkAdapter.fillWork(depositDoc, work, metadataExtractor, role, publication.getArchiveId());

        if (work.getExternalIds() != null && work.getExternalIds().getExternalId().stream()
                .anyMatch(externalId -> "self".equals(externalId.getExternalIdRelationship()))) {
          BigInteger putCode = this.orcidService.uploadWork(work, person.getOrcidToken());
          log.info("Publication '{}' ({}) has been uploaded to ORCID profile of person {} as Work with putCode {}", publication.getTitle(),
                  publicationId, person, putCode);
          return this.storeOrcidSynchronization(person, putCode, publicationId, null, OffsetDateTime.now());
        } else {
          log.info("Publication '{}' ({}) won't be sent to ORCID profile of {} ({}) (ORCID: {}) as it doesn't contain any external identifier",
                  publication.getTitle(), publicationId, person.getFullName(), person.getResId(), person.getOrcid());
          throw new PublicationWithoutExternalIdentifierException();
        }
      } else {
        log.info("Publication '{}' ({}) already exists in ORCID profile of {} ({}) (ORCID: {})", publication.getTitle(), publicationId,
                person.getFullName(), person.getResId(), person.getOrcid());
        throw new PublicationAlreadyExistsInOrcidProfileException();
      }
    } else {
      throw new SolidifyRuntimeException("Person " + person.getFullName() + " (" + person.getResId() + ") doesn't have any ORCID token");
    }
  }
}
