/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationStructureService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.JoinResource2TiersService;
import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.display.StructureWithJoinInfosDTO;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.repository.PublicationStructureRepository;
import ch.unige.aou.specification.PublicationStructureSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PublicationStructureService extends JoinResource2TiersService<Publication, Structure, PublicationStructure> {

  private PublicationStructureRepository structureRepository;

  public PublicationStructureService(PublicationStructureRepository structureRepository) {
    this.structureRepository = structureRepository;
  }

  @Override
  public Publication getEmptyParentResourceObject() {
    return new Publication();
  }

  @Override
  public Structure getEmptyChildResourceObject() {
    return new Structure();
  }

  @Override
  public Join2TiersSpecification<PublicationStructure> getJoinSpecification(PublicationStructure publicationStructure) {
    return new PublicationStructureSpecification(publicationStructure);
  }

  @Override
  public PublicationStructure getEmptyJoinResourceObject() {
    return new PublicationStructure();
  }

  @Override
  public void setParentResource(PublicationStructure publicationStructure, Publication publication) {
    publicationStructure.setPublication(publication);
  }

  @Override
  public void setChildResource(PublicationStructure publicationStructure, Structure structure) {
    publicationStructure.setStructure(structure);
  }

  @Override
  public Structure getChildResource(PublicationStructure publicationStructure) {
    return publicationStructure.getStructure();
  }

  @Override
  public PublicationStructure getDefaultJoinResource() {
    PublicationStructure publicationStructure = new PublicationStructure();

    // default linkType is VALIDATION as it is used in PublicationStructureController that allows to manage VALIDATION structures only
    publicationStructure.setLinkType(PublicationStructure.LinkType.VALIDATION);

    return publicationStructure;
  }

  @Override
  public String getChildPathFromJoinResource() {
    return PublicationStructure.PATH_TO_STRUCTURE;
  }

  @Override
  public StructureWithJoinInfosDTO getChildDTO(PublicationStructure joinResource) {
    return new StructureWithJoinInfosDTO(joinResource);
  }

  @Override
  public void deleteRelation(PublicationStructure joinResource) {
    joinResource.getStructure().getPublicationStructures().remove(joinResource);
    joinResource.getPublication().getPublicationStructures().remove(joinResource);
    super.deleteRelation(joinResource);
  }

  public List<PublicationStructure> findByPublication(String publicationId) {
      return structureRepository.findByPublicationId(publicationId);
  }
}
