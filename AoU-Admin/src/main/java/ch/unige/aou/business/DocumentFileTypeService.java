/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DocumentFileTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileType;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.ValueWithTranslations;
import ch.unige.aou.repository.DocumentFileTypeRepository;
import ch.unige.aou.specification.DocumentFileTypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class DocumentFileTypeService extends AouResourceService<DocumentFileType> {
  private static final Logger log = LoggerFactory.getLogger(DocumentFileTypeService.class);

  private DocumentFileTypeRepository documentFileTypeRepository;
  private PublicationSubtypeService publicationSubtypeService;
  private LabeledLanguageService labeledLanguageService;

  public DocumentFileTypeService(DocumentFileTypeRepository documentFileTypeRepository, PublicationSubtypeService publicationSubtypeService,
          LabeledLanguageService labeledLanguageService) {
    this.documentFileTypeRepository = documentFileTypeRepository;
    this.publicationSubtypeService = publicationSubtypeService;
    this.labeledLanguageService = labeledLanguageService;
  }

  @Override
  public DocumentFileTypeSpecification getSpecification(DocumentFileType resource) {
    return new DocumentFileTypeSpecification(resource);
  }

  public DocumentFileType findByValue(String name) {
    return this.documentFileTypeRepository.findByValue(name);
  }

  /********************
   ** Initialization **
   ********************/

  @Transactional
  public void initDefaultData() {

    /**
     * A1   Article scientifique
     * A2   Article professionnel
     * A3   Autre article
     */
    List<String> subtypeCodes = Arrays.asList("A1", "A2", "A3");
    // @formatter:off
    List<ValueWithTranslations> valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("Article (Published version)",                             AouConstants.DOCUMENT_PUBLISHED_VERSION_EN, AouConstants.DOCUMENT_PUBLISHED_VERSION_FR),
            this.newValueWithEngFreTranslations("Article (Accepted version)",                              AouConstants.DOCUMENT_ACCEPTED_VERSION_EN,  AouConstants.DOCUMENT_ACCEPTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_ARTICLE_SUBMITTED_VERSION, AouConstants.DOCUMENT_SUBMITTED_VERSION_EN, AouConstants.DOCUMENT_SUBMITTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_ARTICLE,                   AouConstants.UNDEFINED_VERSION_EN,          AouConstants.UNDEFINED_VERSION_FR)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     *  C1    Actes de conférence
     */
    subtypeCodes = Arrays.asList("C1");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("Proceedings (Published version)",                             AouConstants.DOCUMENT_PUBLISHED_VERSION_EN, AouConstants.DOCUMENT_PUBLISHED_VERSION_FR),
            this.newValueWithEngFreTranslations("Proceedings (Accepted version)",                              AouConstants.DOCUMENT_ACCEPTED_VERSION_EN,  AouConstants.DOCUMENT_ACCEPTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_PROCEEDINGS_SUBMITTED_VERSION, AouConstants.DOCUMENT_SUBMITTED_VERSION_EN, AouConstants.DOCUMENT_SUBMITTED_VERSION_FR)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     *  C2    Présentation / Intervention
     */
    subtypeCodes = Arrays.asList("C2");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_PRESENTATION,        AouConstants.DOCUMENT_FILE_TYPE_PRESENTATION,    "Présentation"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_RECORDING,           AouConstants.DOCUMENT_FILE_TYPE_RECORDING,       "Enregistrement")
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_APPENDIX,             AouConstants.DOCUMENT_FILE_TYPE_APPENDIX,             "Annexe"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_SUPPLEMENTAL_DATA,    "Supplement",           "Supplément"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_EXTRACT,              AouConstants.DOCUMENT_FILE_TYPE_EXTRACT,              "Extrait"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_SUMMARY,              AouConstants.DOCUMENT_FILE_TYPE_SUMMARY,              "Résumé"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_TRANSLATION,          AouConstants.DOCUMENT_FILE_TYPE_TRANSLATION,          "Traduction")
    );
    // @formatter:on
    List<DocumentFileType> documentFileTypes = new ArrayList<>();
    for (ValueWithTranslations valueWithTranslations : valuesWithTranslations) {
      documentFileTypes.add(this.createIfNotExists(valueWithTranslations));
    }
    this.createDocumentFileTypesForPublicationSubtypes(subtypeCodes, documentFileTypes, DocumentFileType.FileTypeLevel.SECONDARY);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * C3     Poster
     */
    subtypeCodes = Arrays.asList("C3");
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_POSTER, AouConstants.DOCUMENT_FILE_TYPE_POSTER,
                    AouConstants.DOCUMENT_FILE_TYPE_POSTER));
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * C4     Chapitre d'actes
     */
    subtypeCodes = Arrays.asList("C4");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("Proceedings chapter (Published version)",                             AouConstants.DOCUMENT_PUBLISHED_VERSION_EN, AouConstants.DOCUMENT_PUBLISHED_VERSION_FR),
            this.newValueWithEngFreTranslations("Proceedings chapter (Accepted version)",                              AouConstants.DOCUMENT_ACCEPTED_VERSION_EN,  AouConstants.DOCUMENT_ACCEPTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER_SUBMITTED_VERSION, AouConstants.DOCUMENT_SUBMITTED_VERSION_EN, AouConstants.DOCUMENT_SUBMITTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER,                   AouConstants.UNDEFINED_VERSION_EN,          AouConstants.UNDEFINED_VERSION_FR)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * D1     Thèse
     */
    subtypeCodes = Arrays.asList("D1");
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_THESIS, AouConstants.DOCUMENT_FILE_TYPE_THESIS, "Thèse")
    );

    List<ValueWithTranslations> additionalValuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE, "Imprimatur (signed)", "Imprimatur (signé)")
    );

    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes, additionalValuesWithTranslations);
    this.createCorrectiveFileTypes(subtypeCodes);

    additionalValuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE, "Mode de publication (signed)",
                    "Mode de publication (signé)")
    );

    this.createAdministrativeFileTypes(subtypeCodes, additionalValuesWithTranslations);

    /**
     * D4     Thèse de privat-docent
     * D5     Thèse professionnelle
     */
    subtypeCodes = Arrays.asList("D4", "D5");
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_THESIS, AouConstants.DOCUMENT_FILE_TYPE_THESIS, "Thèse"));
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * D2     Master d'études avancées
     * D3     Master
     */
    subtypeCodes = Arrays.asList("D2", "D3");
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_MASTER_THESIS, AouConstants.DOCUMENT_FILE_TYPE_MASTER_THESIS,
                    "Mémoire de master"));
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * J1     Numéro de revue
     */
    subtypeCodes = Arrays.asList("J1");
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("Issue (Published version)", "Issue (Published version)", "Numéro (Version publiée)")
    );
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * L1     Livre
     * L2     Ouvrage collectif
     */
    subtypeCodes = Arrays.asList("L1", "L2");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("Book (Published version)",                             AouConstants.DOCUMENT_PUBLISHED_VERSION_EN, AouConstants.DOCUMENT_PUBLISHED_VERSION_FR),
            this.newValueWithEngFreTranslations("Book (Accepted version)",                              AouConstants.DOCUMENT_ACCEPTED_VERSION_EN,  AouConstants.DOCUMENT_ACCEPTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_BOOK_SUBMITTED_VERSION, AouConstants.DOCUMENT_SUBMITTED_VERSION_EN, AouConstants.DOCUMENT_SUBMITTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_BOOK,                   AouConstants.UNDEFINED_VERSION_EN,          AouConstants.UNDEFINED_VERSION_FR)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * L3     Chapitre de livre
     */
    subtypeCodes = Arrays.asList("L3");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("Book chapter (Published version)",                             AouConstants.DOCUMENT_PUBLISHED_VERSION_EN, AouConstants.DOCUMENT_PUBLISHED_VERSION_FR),
            this.newValueWithEngFreTranslations("Book chapter (Accepted version)",                              AouConstants.DOCUMENT_ACCEPTED_VERSION_EN,  AouConstants.DOCUMENT_ACCEPTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_BOOK_CHAPTER_SUBMITTED_VERSION, AouConstants.DOCUMENT_SUBMITTED_VERSION_EN, AouConstants.DOCUMENT_SUBMITTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_BOOK_CHAPTER,                   AouConstants.UNDEFINED_VERSION_EN,          AouConstants.UNDEFINED_VERSION_FR)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * L5     Contribution à un dictionnaire / une encyclopédie
     */
    subtypeCodes = Arrays.asList("L5");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("Encyclopedia entry (Published version)",                             AouConstants.DOCUMENT_PUBLISHED_VERSION_EN, AouConstants.DOCUMENT_PUBLISHED_VERSION_FR),
            this.newValueWithEngFreTranslations("Encyclopedia entry (Accepted version)",                              AouConstants.DOCUMENT_ACCEPTED_VERSION_EN,  AouConstants.DOCUMENT_ACCEPTED_VERSION_FR),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION, AouConstants.DOCUMENT_SUBMITTED_VERSION_EN, AouConstants.DOCUMENT_SUBMITTED_VERSION_FR)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * R1     Rapport de recherche
     * R2     Rapport technique
     */
    subtypeCodes = Arrays.asList("R1", "R2");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_REPORT, AouConstants.DOCUMENT_FILE_TYPE_REPORT, "Rapport")
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * R3     Working paper
     */
    subtypeCodes = Arrays.asList("R3");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER, AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER, AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);

    /**
     * R4     Preprint
     */
    subtypeCodes = Arrays.asList("R4");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_PREPRINT, AouConstants.DOCUMENT_FILE_TYPE_PREPRINT, AouConstants.DOCUMENT_FILE_TYPE_PREPRINT)
    );
    // @formatter:on
    this.createPrincipalFileTypes(subtypeCodes, valuesWithTranslations);
    this.createSecondaryFileTypes(subtypeCodes);
    this.createCorrectiveFileTypes(subtypeCodes);
    this.createAdministrativeFileTypes(subtypeCodes);
  }

  private void createPrincipalFileTypes(List<String> subtypeCodes, List<ValueWithTranslations> valuesWithTranslations) {
    List<DocumentFileType> documentFileTypes = new ArrayList<>();
    for (ValueWithTranslations valueWithTranslations : valuesWithTranslations) {
      documentFileTypes.add(this.createIfNotExists(valueWithTranslations));
    }
    this.createDocumentFileTypesForPublicationSubtypes(subtypeCodes, documentFileTypes, DocumentFileType.FileTypeLevel.PRINCIPAL);
  }

  private void createSecondaryFileTypes(List<String> subtypeCodes) {
    this.createSecondaryFileTypes(subtypeCodes, null);
  }

  private void createSecondaryFileTypes(List<String> subtypeCodes, List<ValueWithTranslations> additionalValuesWithTranslations) {
    // @formatter:off
    List<ValueWithTranslations> valuesWithTranslations = new ArrayList<>(Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_APPENDIX,             AouConstants.DOCUMENT_FILE_TYPE_APPENDIX,             "Annexe"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_SUPPLEMENTAL_DATA,    AouConstants.DOCUMENT_FILE_TYPE_SUPPLEMENTAL_DATA,    "Données associées"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_RECORDING,            AouConstants.DOCUMENT_FILE_TYPE_RECORDING,            "Enregistrement"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_EXTRACT,              AouConstants.DOCUMENT_FILE_TYPE_EXTRACT,              "Extrait"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_SUMMARY,              AouConstants.DOCUMENT_FILE_TYPE_SUMMARY,              "Résumé"),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_TRANSLATION,          AouConstants.DOCUMENT_FILE_TYPE_TRANSLATION,          "Traduction")
    ));
    // @formatter:on

    if (additionalValuesWithTranslations != null) {
      valuesWithTranslations.addAll(0, additionalValuesWithTranslations);
    }

    List<DocumentFileType> documentFileTypes = new ArrayList<>();
    for (ValueWithTranslations valueWithTranslations : valuesWithTranslations) {
      documentFileTypes.add(this.createIfNotExists(valueWithTranslations));
    }
    this.createDocumentFileTypesForPublicationSubtypes(subtypeCodes, documentFileTypes, DocumentFileType.FileTypeLevel.SECONDARY);
  }

  private void createCorrectiveFileTypes(List<String> subtypeCodes) {
    // @formatter:off
    List<ValueWithTranslations> valuesWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_ADDENDUM,             AouConstants.DOCUMENT_FILE_TYPE_ADDENDUM,             AouConstants.DOCUMENT_FILE_TYPE_ADDENDUM),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_CORRIGENDUM,          AouConstants.DOCUMENT_FILE_TYPE_CORRIGENDUM,          AouConstants.DOCUMENT_FILE_TYPE_CORRIGENDUM),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_ERRATUM,              AouConstants.DOCUMENT_FILE_TYPE_ERRATUM,              AouConstants.DOCUMENT_FILE_TYPE_ERRATUM),
            this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_RETRACTION,           AouConstants.DOCUMENT_FILE_TYPE_RETRACTION,           "Rétraction")
    );
    // @formatter:on

    List<DocumentFileType> documentFileTypes = new ArrayList<>();
    for (ValueWithTranslations valueWithTranslations : valuesWithTranslations) {
      documentFileTypes.add(this.createIfNotExists(valueWithTranslations));
    }
    this.createDocumentFileTypesForPublicationSubtypes(subtypeCodes, documentFileTypes, DocumentFileType.FileTypeLevel.CORRECTIVE);
  }

  private void createAdministrativeFileTypes(List<String> subtypeCodes) {
    this.createAdministrativeFileTypes(subtypeCodes, null);
  }

  private void createAdministrativeFileTypes(List<String> subtypeCodes, List<ValueWithTranslations> additionalValuesWithTranslations) {

    // @formatter:off
    List<ValueWithTranslations> valuesWithTranslations = new ArrayList<>(Arrays.asList(
      this.newValueWithEngFreTranslations(AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE, AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE, "Contrat / Autorisation")
    ));
    // @formatter:on

    if (additionalValuesWithTranslations != null) {
      valuesWithTranslations.addAll(0, additionalValuesWithTranslations);
    }

    List<DocumentFileType> documentFileTypes = new ArrayList<>();
    for (ValueWithTranslations valueWithTranslations : valuesWithTranslations) {
      documentFileTypes.add(this.createIfNotExists(valueWithTranslations));
    }
    this.createDocumentFileTypesForPublicationSubtypes(subtypeCodes, documentFileTypes, DocumentFileType.FileTypeLevel.ADMINISTRATION);
  }

  private DocumentFileType createIfNotExists(ValueWithTranslations valueWithTranslations) {
    DocumentFileType dft = this.findByValue(valueWithTranslations.getValue());
    if (dft == null) {
      dft = new DocumentFileType();
      dft.setValue(valueWithTranslations.getValue());
      dft.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_ENGLISH),
              this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
      dft.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_FRENCH),
              this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));
      dft = this.save(dft);
      log.info("DocumentFileType '{}' created", valueWithTranslations.getValue());
    }
    return dft;
  }

  private void createDocumentFileTypesForPublicationSubtypes(List<String> subtypeCodes, List<DocumentFileType> documentFileTypes,
          DocumentFileType.FileTypeLevel level) {
    for (String subtypeCode : subtypeCodes) {
      this.createDocumentFileTypesForPublicationSubtype(subtypeCode, documentFileTypes, level);
    }
  }

  private void createDocumentFileTypesForPublicationSubtype(String subtypeCode, List<DocumentFileType> documentFileTypes,
          DocumentFileType.FileTypeLevel level) {
    // Do not create this relation for old types of document files: Article, Book, Book chapter, Proceedings chapter
    PublicationSubtype subtype = this.publicationSubtypeService.findByCode(subtypeCode);
    if (subtype != null) {
      int sortValue = AouConstants.ORDER_INCREMENT;
      boolean updated = false;
      for (DocumentFileType dft : documentFileTypes) {
        if (!dft.getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_ARTICLE)
                && !dft.getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_BOOK)
                && !dft.getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_BOOK_CHAPTER)
                && !dft.getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER)) {
          PublicationSubtypeDocumentFileType psDft = new PublicationSubtypeDocumentFileType();
          psDft.setPublicationSubtype(subtype);
          psDft.setDocumentFileType(dft);
          psDft.setLevel(level);
          psDft.setSortValue(sortValue);

          if (!subtype.getPublicationSubtypeDocumentFileTypes().contains(psDft)) {
            subtype.addPublicationSubtypeDocumentFileType(psDft);
            sortValue += AouConstants.ORDER_INCREMENT;
            updated = true;
          }
        }
      }
      if (updated) {
        this.publicationSubtypeService.save(subtype);
        log.info("DocumentFileTypes configured for PublicationSubtype {}", subtype.getName());
      }
    } else {
      log.warn("Unable to configure DocumentFileTypes for PublicationSubtype {} that does not exist", subtypeCode);
    }
  }

  public DocumentFileType convertFileTypeToDocumentFileType(String typeName) {
    DocumentFileType documentFileType = null;
    if (!StringTool.isNullOrEmpty(typeName)) {

      switch (typeName) {
        case "Report (Published version)":
        case "Report (Accepted version)":
        case "Report (Submitted version)":
          typeName = AouConstants.DOCUMENT_FILE_TYPE_REPORT;
          break;
        case "Working paper (Published version)":
        case "Working paper (Accepted version)":
        case "Working paper (Submitted version)":
          typeName = AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER;
          break;
        case "Master":
          typeName = AouConstants.DOCUMENT_FILE_TYPE_MASTER_THESIS;
          break;
        default:
          break;
      }

      documentFileType = this.findByValue(typeName);
    }

    return documentFileType;
  }
}
