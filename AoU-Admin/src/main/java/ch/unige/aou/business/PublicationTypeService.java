/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.repository.PublicationTypeRepository;
import ch.unige.aou.specification.PublicationTypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PublicationTypeService extends AouResourceService<PublicationType> {
  private static final Logger log = LoggerFactory.getLogger(PublicationTypeService.class);

  private PublicationTypeRepository publicationTypeRepository;

  public PublicationTypeService(PublicationTypeRepository publicationTypeRepository) {
    this.publicationTypeRepository = publicationTypeRepository;
  }

  @Override
  public PublicationTypeSpecification getSpecification(PublicationType resource) {
    return new PublicationTypeSpecification(resource);
  }

  public PublicationType findByName(String name) {
    return this.publicationTypeRepository.findByName(name);
  }

  @Override
  public Optional<Integer> getMaxSortValue(Sortable item) {
    return this.publicationTypeRepository.findMaxSortValue();
  }

  @Override
  protected void beforeValidate(PublicationType item) {
    this.setDefaultSortValue(item);
  }

  /********************
   ** Initialization **
   ********************/

  public void initDefaultData() {
    this.createPublicationType(AouConstants.DEPOSIT_TYPE_ARTICLE_ID, AouConstants.DEPOSIT_TYPE_ARTICLE_NAME, 10);
    this.createPublicationType(AouConstants.DEPOSIT_TYPE_CONFERENCE_ID, AouConstants.DEPOSIT_TYPE_CONFERENCE_NAME, 20);
    this.createPublicationType(AouConstants.DEPOSIT_TYPE_DIPLOME_ID, AouConstants.DEPOSIT_TYPE_DIPLOME_NAME, 30);
    this.createPublicationType(AouConstants.DEPOSIT_TYPE_JOURNAL_ID, AouConstants.DEPOSIT_TYPE_JOURNAL_NAME, 40);
    this.createPublicationType(AouConstants.DEPOSIT_TYPE_LIVRE_ID, AouConstants.DEPOSIT_TYPE_LIVRE_NAME, 50);
    this.createPublicationType(AouConstants.DEPOSIT_TYPE_RAPPORT_ID, AouConstants.DEPOSIT_TYPE_RAPPORT_NAME, 60);
  }

  public void createPublicationType(String resId, String name, int sort) {
    if (this.findByName(name) == null) {
      PublicationType type = new PublicationType();
      type.setResId(resId);
      type.setName(name);
      type.setSortValue(sort);
      this.save(type);
      log.info("PublicationType '{}' created", name);
    } else {
      log.debug("PublicationType '{}' already exists", name);
    }
  }
}
