/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - StructureService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.repository.StructureRepository;
import ch.unige.aou.specification.StructureSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class StructureService extends AouResourceService<Structure> {
  private static final Logger log = LoggerFactory.getLogger(StructureService.class);

  private static final String ERROR_STRUCTURE_INCLUDED_IN_ANOTHER_ONE = "deposit.error.structure.included_in_another_one";

  private static final String JSON_FILE = "structures.json";
  private static final String JSON_FILE_FIELD_ID = "id";
  private static final String JSON_FILE_FIELD_PARENT_ID = "parentId";
  private static final String JSON_FILE_FIELD_CN_STRUCT_C = "cnStructC";
  private static final String JSON_FILE_FIELD_C_STRUCT = "cStruct";
  private static final String JSON_FILE_FIELD_NAME = "name";
  private static final String JSON_FILE_FIELD_ACRONYM = "acronym";
  private static final String JSON_FILE_FIELD_DEWEY = "dewey";
  private static final String JSON_FILE_FIELD_STATUS = "status";
  private static final String JSON_FILE_FIELD_VALID_UNTIL = "validUntil";
  private static final String JSON_FILE_FIELD_SORT_VALUE = "sortValue";

  private StructureRepository structureRepository;

  public StructureService(StructureRepository structureRepository) {
    this.structureRepository = structureRepository;
  }

  @Override
  public StructureSpecification getSpecification(Structure structure) {
    return new StructureSpecification(structure);
  }

  public Structure findByCnStructC(String cnStructC) {
    return this.structureRepository.findByCnStructC(cnStructC);
  }

  public Structure findByCodeStruct(String cStruct) {
    return this.structureRepository.findByCodeStruct(cStruct);
  }

  public List<Structure> findByNameAndParentStructureResId(String name, String parentStructureId) {
    return this.structureRepository.findByNameAndParentStructureResId(name, parentStructureId);
  }

  /**
   * Return the list of structures that a user has selected in his profile
   *
   * @param externalUid
   * @param pageable
   * @return
   */
  public Page<Structure> findByUserExternalUid(String externalUid, Pageable pageable) {
    return this.structureRepository.findByUserExternalUid(externalUid, pageable);
  }

  public String getParentStructureResId(String resId) {
    // using a custom query in the repository prevents getting wrong data (already updated in memory) because of the JPA / Hibernate cache
    return this.structureRepository.getParentStructureResId(resId);
  }

  /**
   * Check that the childStructure can have the parentStructure as parent. It checks that it would not create orphan branches
   * in the structure tree.
   */
  public boolean isParentValid(Structure childStructure, Structure newParentStructure) {
    if (newParentStructure == null || StringTool.isNullOrEmpty(newParentStructure.getResId())) {
      /**
       * Moving at top is always allowed
       */
      return true;
    } else if (childStructure.getResId().equals(newParentStructure.getResId())) {
      /**
       * Moving under itself is not possible
       */
      return false;
    } else {
      /**
       * Moving under another node is only possible if the parent node has not the child node as parent itself
       *
       * A
       * |_B
       *   |_C
       *
       * In this case, B cannot be moved under C
       */
      List<String> parentAncestorStructureIds = this.getAncestorStructureIds(newParentStructure.getResId());
      return !parentAncestorStructureIds.contains(childStructure.getResId());
    }
  }

  /**
   * Return the list of structure ids that are ancestors of the given structure
   *
   * @param structureId
   * @return
   */
  public List<String> getAncestorStructureIds(String structureId) {
    List<String> ancestorStructureIds = new ArrayList<>();
    while (!StringTool.isNullOrEmpty(structureId)) {
      String parentStructureResId = this.getParentStructureResId(structureId);
      if (!StringTool.isNullOrEmpty(parentStructureResId)) {
        ancestorStructureIds.add(parentStructureResId);
      }
      structureId = parentStructureResId;
    }
    return ancestorStructureIds;
  }

  /**
   * Return the list of ancestor structures for the given structure.
   * Returned list is sorted from deeper child structure to top structure.
   *
   * @param structureId
   * @return
   */
  public List<Structure> getAncestorStructures(String structureId) {
    List<String> ancestorStructureIds = this.getAncestorStructureIds(structureId);
    List<Structure> structures = new ArrayList<>();
    for (String ancestorStructureId : ancestorStructureIds) {
      structures.add(this.findOne(ancestorStructureId));
    }
    return structures;
  }

  /**
   * Return the identifiers of all Structures that are descendant of the structure
   *
   * @param structureId
   * @return
   */
  public List<String> getAllChildStructureIds(String structureId) {
    List<String> childStructureIds = new ArrayList<>();
    this.fillChildStructureIds(structureId, childStructureIds);
    return childStructureIds;
  }

  private void fillChildStructureIds(String structureId, List<String> childStructureIds) {
    Structure structure = this.findOne(structureId);
    for (Structure childStructure : structure.getChildStructures()) {
      if (!childStructureIds.contains(childStructure.getResId())) {
        childStructureIds.add(childStructure.getResId());
      }
      this.fillChildStructureIds(childStructure.getResId(), childStructureIds);
    }
  }

  public void duplicatesInStructures(Person person, Structure structureToCheck) {
    //Validate if there are duplicate structureToCheck before adding a new one
    List<String> leafStructureIds = person.getStructures().stream().map(Structure::getResId).collect(Collectors.toList());
    leafStructureIds.add(structureToCheck.getResId());
    List<String> duplicatedStructureIds = this.getDuplicatedStructureIdsInHierarchy(leafStructureIds);
    duplicatedStructureIds.forEach(leafStructureId -> {
      Structure duplicatedStructure = this.findOne(leafStructureId);
      throw new SolidifyValidationException(new ValidationError(
              this.messageService.get(ERROR_STRUCTURE_INCLUDED_IN_ANOTHER_ONE, new Object[] { duplicatedStructure.getName() })));
    });
  }

  /**
   * Return a list of structures ids that can be considered as duplicates, because they are included in the ancestors of other structures
   *
   * @param leafStructureIds
   * @return
   */
  public List<String> getDuplicatedStructureIdsInHierarchy(List<String> leafStructureIds) {
    Set<String> duplicatedIds = new HashSet<>();
    for (String leafStructureId : leafStructureIds) {
      List<Structure> ancestorStructures = this.getAncestorStructures(leafStructureId);
      List<String> ancestorStructureIds = ancestorStructures.stream().map(Structure::getResId).collect(Collectors.toList());
      leafStructureIds.forEach(leafStructId -> {
        if (ancestorStructureIds.stream().anyMatch(ancestorId -> ancestorId.equals(leafStructId))) {
          // the leaf structure is included in another structure path
          duplicatedIds.add(leafStructId);
        }
      });
    }
    return new ArrayList<>(duplicatedIds);
  }

  public Optional<Integer> getMaxSortValueByParentStructure(String parentStructureId) {
    return this.structureRepository.findMaxSortValueByParentStructure(parentStructureId);
  }

  public Optional<Integer> getMaxSortValueWithoutParent() {
    return this.structureRepository.getMaxSortValue();
  }

  @Override
  public Optional<Integer> getMaxSortValue(Sortable item) {
    if (((Structure) item).getParentStructure() != null) {
      return this.getMaxSortValueByParentStructure(((Structure) item).getParentStructure().getResId());
    } else {
      return this.getMaxSortValueWithoutParent();
    }
  }

  @Override
  protected void beforeValidate(Structure item) {
    this.setDefaultSortValue(item);
  }

  @Override
  protected void validateItemSpecificRules(Structure item, BindingResult errors) {
    /**
     * Check that parent structure exists and is valid
     */
    if (item.getParentStructure() != null && item.getParentStructure().getResId() != null) {
      if (!this.existsById(item.getParentStructure().getResId())) {
        errors.addError(new FieldError(item.getClass().getSimpleName(), "parentStructure", "The parent structure does not exist"));
      }
      if (!this.isParentValid(item, item.getParentStructure())) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "parentStructure", "The parent structure is not valid for this structure"));
      }
    }

    /**
     * Check cnStructC uniqueness
     */
    Structure existingStructure = this.findByCnStructC(item.getCnStructC());
    if (existingStructure != null && !existingStructure.getResId().equals(item.getResId())) {
      errors.addError(new FieldError(item.getClass().getSimpleName(), "cnStructC", "This value already exists"));
    }

    /**
     * Check cStruct uniqueness
     */
    existingStructure = this.findByCodeStruct(item.getCodeStruct());
    if (existingStructure != null && !existingStructure.getResId().equals(item.getResId())) {
      errors.addError(new FieldError(item.getClass().getSimpleName(), "cStruct", "This value already exists"));
    }

    /**
     * Check dewey contains only digits
     */
    if (!StringTool.isNullOrEmpty(item.getDewey()) && !ValidationTool.isValidDeweyCode(item.getDewey())) {
      errors.addError(
              new FieldError(item.getClass().getSimpleName(), "dewey", "This value should contain only numbers, dots, dashes or slashes"));
    }

    /**
     * Check child name and status uniqueness for a parent structure
     * Note: obsolete structures are not verified as the same name can appear multiple times
     */
    if (item.getStatus() != Structure.StructureStatus.OBSOLETE && item.getParentStructure() != null
            && item.getParentStructure().getResId() != null) {
      List<Structure> existingStructures = this.findByNameAndParentStructureResId(item.getName(), item.getParentStructure().getResId());
      for (Structure existingStruct : existingStructures) {
        if (existingStruct.getStatus() != Structure.StructureStatus.OBSOLETE && !existingStruct.getResId().equals(item.getResId())) {
          errors.addError(new FieldError(item.getClass().getSimpleName(), "name", "This name already exists in the parent structure"));
          break;
        }
      }
    }

    /**
     * Check name and status uniqueness for top structures
     * Note: obsolete structures are not verified as the same name can appear multiple times
     */
    if (item.getStatus() != Structure.StructureStatus.OBSOLETE && item.getParentStructure() == null) {
      List<Structure> existingStructures = this.findByNameAndParentStructureResId(item.getName(), null);
      for (Structure existingStruct : existingStructures) {
        if (existingStruct.getStatus() != Structure.StructureStatus.OBSOLETE && !existingStruct.getResId().equals(item.getResId())) {
          errors.addError(new FieldError(item.getClass().getSimpleName(), "name", "This name already exists for a top structure"));
          break;
        }
      }
    }
  }

  @Transactional
  public void initDefaultData() {
    try {
      this.createDefaultStructuresFromJsonFile();
    } catch (IOException e) {
      log.error("Unable to initialize structures from Json file " + JSON_FILE, e);
    }
  }

  @Transactional
  public void createDefaultStructuresFromJsonFile() throws IOException {
    final JSONArray structuresArray = new JSONArray(FileTool.toString(new ClassPathResource(JSON_FILE).getInputStream()));
    for (final Object obj : structuresArray) {
      if (obj instanceof JSONObject) {
        JSONObject item = (JSONObject) obj;
        String resId = item.getString(JSON_FILE_FIELD_ID);

        String parentId = null;
        if (!item.isNull(JSON_FILE_FIELD_PARENT_ID)) {
          parentId = item.getString(JSON_FILE_FIELD_PARENT_ID);
        }
        String cnStructC = item.getString(JSON_FILE_FIELD_CN_STRUCT_C);
        String cStruct = item.getString(JSON_FILE_FIELD_C_STRUCT);
        String name = item.getString(JSON_FILE_FIELD_NAME);
        String acronym = null;
        if (!item.isNull(JSON_FILE_FIELD_ACRONYM)) {
          acronym = item.getString(JSON_FILE_FIELD_ACRONYM);
        }
        String dewey = item.getString(JSON_FILE_FIELD_DEWEY);
        Integer sortValue = item.getInt(JSON_FILE_FIELD_SORT_VALUE);
        Structure.StructureStatus status = Structure.StructureStatus.valueOf(item.getString(JSON_FILE_FIELD_STATUS));

        LocalDate validUntil = null;
        if (!item.isNull(JSON_FILE_FIELD_VALID_UNTIL)) {
          try {
            Integer year = item.getInt(JSON_FILE_FIELD_VALID_UNTIL);
            validUntil = LocalDate.of(year, 12, 31);
          } catch (JSONException e) {
            validUntil = LocalDate.parse(item.getString(JSON_FILE_FIELD_VALID_UNTIL));
          }
        }

        this.createStructure(resId, parentId, status, cnStructC, cStruct, name, acronym, dewey, validUntil, sortValue);
      }
    }
    log.info("All structures initialized");
  }

  public void createStructure(String resId, String parentId, Structure.StructureStatus status, String cnStructC, String cStructc, String name,
          String acronym, String dewey, LocalDate validUntil, Integer sortValue) {

    if (!this.existsById(resId)) {
      Structure structure = new Structure();
      structure.setResId(resId);
      if (!StringTool.isNullOrEmpty(parentId)) {
        Structure parentStructure = this.findOne(parentId);
        structure.setParentStructure(parentStructure);
      }
      structure.setStatus(status);
      structure.setCnStructC(cnStructC);
      structure.setCodeStruct(cStructc);
      structure.setName(name);
      structure.setAcronym(acronym);
      structure.setDewey(dewey);
      structure.setSortValue(sortValue);
      structure.setValidUntil(validUntil);
      this.save(structure);
      log.info("Structure '{}' created", name);
    } else {
      log.debug("Structure '{}: {}' already exists", resId, name);
    }
  }
}
