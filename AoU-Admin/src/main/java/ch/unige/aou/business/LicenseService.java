/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - LicenseService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import jakarta.transaction.Transactional;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.LicenseGroup;
import ch.unige.aou.repository.LicenseRepository;
import ch.unige.aou.specification.LicenseSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class LicenseService extends AouResourceService<License> {
  static final Logger log = LoggerFactory.getLogger(LicenseService.class);

  public static final String AOU_LICENSES_FILE = "aou-licenses.json";

  private LabeledLanguageService labeledLanguageService;
  private LicenseGroupService licenseGroupService;

  public LicenseService(LabeledLanguageService labeledLanguageService, LicenseGroupService licenseGroupService) {
    this.labeledLanguageService = labeledLanguageService;
    this.licenseGroupService = licenseGroupService;
  }

  public License findByOpenLicenseId(String openLicenseid) {
    return ((LicenseRepository) this.itemRepository).findByOpenLicenseId(openLicenseid);
  }

  public License findByTitle(String title) {
    return ((LicenseRepository) this.itemRepository).findByTitle(title);
  }

  public List<License> findByUrl(URL url) {
    return ((LicenseRepository) this.itemRepository).findByUrl(url);
  }

  public License findMoreSpecificLicenseByUrl(String licenseUrl) {
    try {
      List<License> licenses = this.findByUrl(new URL(licenseUrl));
      if (licenses.size() == 1) {
        return licenses.get(0);
      } else {
        for (License license : licenses) {
          if (Boolean.TRUE.equals(license.getVisible()) && !StringTool.isNullOrEmpty(license.getVersion())) {
            return license;
          }
        }
      }
    } catch (MalformedURLException e) {
      log.warn("Error in creating License URL from value {} ", licenseUrl);
    }
    return null;
  }

  @Override
  public LicenseSpecification getSpecification(License resource) {
    return new LicenseSpecification(resource);
  }

  @Transactional
  public void initDefaultData() {
    try {
      ObjectMapper mapper = new ObjectMapper();
      final ArrayNode licensesArray = mapper
              .readValue(FileTool.toString(new ClassPathResource(AOU_LICENSES_FILE).getInputStream()), ArrayNode.class);
      for (final JsonNode item : licensesArray) {
        String resId = item.get("resId").textValue();
        if (!this.existsById(resId)) {
          License license = new License();
          license.setResId(resId);

          LicenseGroup licenseGroup = this.licenseGroupService.findOne(item.get("groupId").textValue());
          license.setLicenseGroup(licenseGroup);

          license.setOpenLicenseId(item.get("openLicenseId").textValue());
          license.setTitle(item.get("title").textValue());
          if (item.has("version")) {
            license.setVersion(item.get("version").textValue());
          }
          if (item.has("url")) {
            String url = item.get("url").textValue();
            try {
              license.setUrl(new URL(url));
            } catch (MalformedURLException e) {
              log.warn("Error in creating license: Url {} is malformed", url);
            }
          }
          license.setSortValue(item.get("sortValue").asInt());
          license.setVisible(true);

          if (item.has("labels")) {
            JsonNode labelsNode = item.get("labels");
            if (labelsNode.has(AouConstants.LANG_CODE_FRENCH)) {
              license.getLabels().add(new Label(labelsNode.get(AouConstants.LANG_CODE_FRENCH).textValue(),
                      this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));
            }
            if (labelsNode.has(AouConstants.LANG_CODE_ENGLISH)) {
              license.getLabels().add(new Label(labelsNode.get(AouConstants.LANG_CODE_ENGLISH).textValue(),
                      this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
            }
          }

          this.save(license);
          log.info("License '{}' created", resId);
        }
      }
    } catch (IOException e) {
      log.error("Unable to initialize licenses from Json file", e);
    }
  }

  @Override
  protected void beforeValidate(License item) {
    this.setDefaultSortValue(item);
  }

  @Override
  protected void validateItemSpecificRules(License license, BindingResult errors) {

    // check LicenseGroup
    if (license.getLicenseGroup() == null || StringTool.isNullOrEmpty(license.getLicenseGroup().getResId())) {
      errors.addError(new FieldError(license.getClass().getSimpleName(), "licenseGroup", "The license group is mandatory"));
    } else if (!this.licenseGroupService.existsById(license.getLicenseGroup().getResId())) {
      errors.addError(new FieldError(license.getClass().getSimpleName(), "licenseGroup", "The license group does not exist"));
    }

    // check title uniqueness
    License existingLicense = this.findByTitle(license.getTitle());
    if (existingLicense != null && !existingLicense.getResId().equals(license.getResId())) {
      errors.addError(new FieldError(license.getClass().getSimpleName(), "title", "This value already exists"));
    }

    // check open license id uniqueness
    existingLicense = this.findByOpenLicenseId(license.getOpenLicenseId());
    if (existingLicense != null && !existingLicense.getResId().equals(license.getResId())) {
      errors.addError(new FieldError(license.getClass().getSimpleName(), "openLicenseId", "This value already exists"));
    }
  }

  @Override
  public Optional<Integer> getMaxSortValue(Sortable item) {
    return ((LicenseRepository) this.itemRepository).findMaxSortValue();
  }

  public License getLicenseFromType(String licenseName) {
    if (!StringTool.isNullOrEmpty(licenseName)) {
      licenseName = licenseName.toUpperCase();

      if (licenseName.equals("OTHER OA")) {
        licenseName = "OA OTHER";
      } else if (licenseName.equals("OA NATIONAL LICENCES")) {
        licenseName = "OA NATIONAL";
      }

      License license = this.findByOpenLicenseId(licenseName);
      if (license == null) {
        throw new SolidifyRuntimeException("No license found with OpenLicenceId '" + licenseName + "'");
      }
      return license;
    }
    return null;
  }
}
