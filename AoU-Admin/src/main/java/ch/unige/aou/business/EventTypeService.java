/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - EventTypeService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.repository.EventTypeRepository;
import ch.unige.aou.specification.EventTypeSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class EventTypeService extends AouResourceService<EventType> {
  private static final Logger log = LoggerFactory.getLogger(EventTypeService.class);

  private EventTypeRepository eventTypeRepository;

  public EventTypeService(EventTypeRepository eventTypeRepository) {
    this.eventTypeRepository = eventTypeRepository;
  }

  @Override
  public EventTypeSpecification getSpecification(EventType eventType) {
    return new EventTypeSpecification(eventType);
  }

  public void initDefaultData() {
    for (EventType eventType : EventType.getAllEventTypes()) {
      if (!this.eventTypeRepository.findById(eventType.getResId()).isPresent()) {
        this.eventTypeRepository.save(eventType);
        log.info("EventType {} created with name {}", eventType.getResId(), eventType.getName());
      }
    }
  }
}
