/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - CommentService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.repository.CommentRepository;
import ch.unige.aou.specification.CommentSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class CommentService extends AouResourceService<Comment> {
  private final CommentRepository commentRepository;
  private final PersonService personService;
  private final EventService eventService;

  private final PublicationService publicationService;

  public CommentService(CommentRepository commentRepository, PersonService personService, @Lazy EventService eventService,
          PublicationService publicationService) {
    this.commentRepository = commentRepository;
    this.personService = personService;
    this.eventService = eventService;
    this.publicationService = publicationService;
  }

  @Override
  public CommentSpecification getSpecification(Comment resource) {
    return new CommentSpecification(resource);
  }

  public List<Comment> findByPublication(String publicationId) {
    return this.commentRepository.findByPublicationId(publicationId);
  }

  @Override
  public Comment save(Comment comment) {
    final Comment saved = super.save(comment);

    // create an event for a new comment
    if (!comment.isSystem()) {
      if (comment.isOnlyForValidators()) {
        this.eventService.createEvent(comment, EventType.COMMENT_FOR_VALIDATORS_IN_PUBLICATION);
      } else {
        this.eventService.createEvent(comment, EventType.COMMENT_IN_PUBLICATION);
      }
    }

    return saved;
  }

  @Override
  protected void validateItemSpecificRules(Comment comment, BindingResult errors) {
    /**
     * Check that the author of the comment is allowed to validate the publication if the comment is only for validators.
     */
    if (!comment.isSystem() && comment.isOnlyForValidators() && !this.authenticatedUserHasPriviledgedRole()) {
      // reload publication to ensure its structures are filled
      Publication publication = this.publicationService.findOne(comment.getPublicationId());
      if (!this.personService.isAllowedToValidate(comment.getCreatorId(), publication)) {
        errors.addError(new FieldError(comment.getClass().getSimpleName(), "onlyForValidators", "This value is not permitted."));
      }
    }
  }

  public void changeCommentsPerson(Person creatorToKeep, Person creatorToUpdate) {
    List<Comment> commentList = ((CommentRepository) this.itemRepository).findByPersonResId(creatorToUpdate.getResId());
    for (Comment comment : commentList) {
      comment.setPerson(creatorToKeep);
      this.itemRepository.save(comment);
    }
  }
}
