/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ContributorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.HashTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.publication.OtherNameDTO;
import ch.unige.aou.model.search.ContributorSearchType;
import ch.unige.aou.model.security.User;
import ch.unige.aou.repository.ContributorRepository;
import ch.unige.aou.specification.ContributorSearchSpecification;
import ch.unige.aou.specification.ContributorSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ContributorService extends AouResourceService<Contributor> {

  private static final Logger log = LoggerFactory.getLogger(ContributorService.class);

  private static final String UNIQUE_KEY_SEPARATOR = "|||";

  private UserService userService;

  private final ContributorRepository contributorRepository;

  public ContributorService(ContributorRepository contributorRepository, UserService userService) {
    this.contributorRepository = contributorRepository;
    this.userService = userService;
  }

  @Override
  public SolidifySpecification<Contributor> getSpecification(Contributor resource) {
    return new ContributorSpecification(resource);
  }

  @Override
  protected void beforeValidate(Contributor contributor) {
    super.beforeValidate(contributor);
    contributor.setUniqueKey(this.computeUniqueKey(contributor));
  }

  /**
   * Compute a key based on the contributor cnIndividu, lastName and firstName that is unique in database.
   *
   * @param contributor
   * @return
   */
  private String computeUniqueKey(Contributor contributor) {
    StringBuilder sb = new StringBuilder();
    if (!StringTool.isNullOrEmpty(contributor.getCnIndividu())) {
      sb.append(contributor.getCnIndividu());
    }
    sb.append(UNIQUE_KEY_SEPARATOR);
    if (!StringTool.isNullOrEmpty(contributor.getLastName())) {
      sb.append(contributor.getLastName());
    }
    sb.append(UNIQUE_KEY_SEPARATOR);
    if (!StringTool.isNullOrEmpty(contributor.getFirstName())) {
      sb.append(contributor.getFirstName());
    }
    try {
      return HashTool.hash("SHA-1", sb.toString());
    } catch (NoSuchAlgorithmException e) {
      throw new SolidifyRuntimeException("Unable to compute Contributor unique key", e);
    }
  }

  @Transactional
  public Contributor findOrCreate(ContributorDTO contributorDTO) {
    Contributor existingContributor;

    if (StringTool.isNullOrEmpty(contributorDTO.getCnIndividu())) {
      existingContributor = this.contributorRepository
              .findByFirstNameAndLastNameAndCnIndividuIsNull(contributorDTO.getFirstName(), contributorDTO.getLastName());
    } else {
      existingContributor = this.contributorRepository.findByCnIndividu(contributorDTO.getCnIndividu());
    }

    if (existingContributor == null) {
      Contributor newContributor = new Contributor();
      newContributor.setFirstName(contributorDTO.getFirstName());
      newContributor.setLastName(contributorDTO.getLastName());
      newContributor.setCnIndividu(contributorDTO.getCnIndividu());
      existingContributor = this.save(newContributor);
    }

    if (!StringTool.isNullOrEmpty(existingContributor.getCnIndividu())) {
      // For UNIGE contributors, store eventual other forms of his/her name
      existingContributor = this.storeContributorOtherNames(existingContributor, contributorDTO);

      // For UNIGE contributors, store eventual ORCID
      existingContributor = this.storeContributorOrcid(existingContributor, contributorDTO);
    }

    return existingContributor;
  }

  private Contributor storeContributorOtherNames(Contributor existingContributor, ContributorDTO contributorDTO) {
    // List of all forms of name already existing in database
    List<OtherNameDTO> existingContributorNames = existingContributor.getOtherNames().stream()
            .map(con -> new OtherNameDTO(con.getFirstName(), con.getLastName())).collect(Collectors.toList());
    existingContributorNames.add(new OtherNameDTO(existingContributor.getFirstName(), existingContributor.getLastName()));

    // Compare with the list of forms to store
    List<OtherNameDTO> otherNameDTOs = contributorDTO.getOtherNames();
    otherNameDTOs.add(new OtherNameDTO(contributorDTO.getFirstName(), contributorDTO.getLastName()));

    List<OtherNameDTO> otherNamesDtoToStore = otherNameDTOs.stream().filter(otherNameDTO -> !existingContributorNames.contains(otherNameDTO))
            .collect(Collectors.toList());

    boolean otherNameAdded = false;
    for (OtherNameDTO otherNameDTO : otherNamesDtoToStore) {
      ContributorOtherName contributorOtherName = new ContributorOtherName();
      contributorOtherName.setContributor(existingContributor);
      contributorOtherName.setFirstName(otherNameDTO.getFirstName());
      contributorOtherName.setLastName(otherNameDTO.getLastName());

      existingContributor.getOtherNames().add(contributorOtherName);
      otherNameAdded = true;
      log.info("ContributorOtherName " + contributorOtherName + " created for Contributor " + existingContributor);
    }

    if (otherNameAdded) {
      return this.save(existingContributor);
    } else {
      return existingContributor;
    }
  }

  private Contributor storeContributorOrcid(Contributor contributor, ContributorDTO contributorDTO) {
    if (!StringTool.isNullOrEmpty(contributor.getCnIndividu())) {
      String fullCnIndividu = contributor.getCnIndividu() + AouConstants.UNIGE_EXTERNAL_UID_SUFFIX;
      if (this.userService.existsByExternalUid(fullCnIndividu)) {
        User user = this.userService.findByExternalUid(fullCnIndividu);
        if (user.getPerson() != null && !StringTool.isNullOrEmpty(user.getPerson().getOrcid())
                && !user.getPerson().getOrcid().equals(contributor.getOrcid())) {
          // First check if a corresponding Person with an ORCID already exists (if yes it is considered as more reliable than imported metadata value)
          contributor.setOrcid(user.getPerson().getOrcid());
        }
      } else if (!StringTool.isNullOrEmpty(contributorDTO.getOrcid()) && StringTool.isNullOrEmpty(contributor.getOrcid())) {
        // Otherwise if contributor doesn't have any ORCID yet, store the one found in metadata
        // Note: we don't override an existing value as it may have been added by a more reliable way such as editing the user/person (by an admin)
        //       or by the user himself in his profile
        contributor.setOrcid(contributorDTO.getOrcid());
      }
    }
    return contributor;
  }

  public Contributor findByCnIndividu(String cnIndividu) {
    return this.contributorRepository.findByCnIndividu(cnIndividu);
  }

  public Contributor findByOrcid(String orcid) {
    return this.contributorRepository.findByOrcid(orcid);
  }

  public Contributor findByFirstNameAndLastName(String firstName, String lastName) {
    return this.contributorRepository.findByFirstNameAndLastName(firstName, lastName);
  }

  public Contributor findByFirstNameAndLastNameAndCnIndividuIsNull(String firstName, String lastName) {
    return this.contributorRepository.findByFirstNameAndLastNameAndCnIndividuIsNull(firstName, lastName);
  }

  public Optional<Contributor> findByResIdAndPublicationsIsNull(String contributorId) {
    return this.contributorRepository.findByResIdAndPublicationsIsNull(contributorId);
  }

  public Page<Contributor> searchContributors(String search, ContributorSearchType searchType, Pageable pageable) {
    ContributorSearchSpecification specification = new ContributorSearchSpecification(search, searchType);
    Page<Contributor> contributors = this.findAll(specification, pageable);

    if (searchType == ContributorSearchType.ALL) {
      contributors = this.completeWithOtherNamesAsContributors(contributors);
    }

    return contributors;
  }

  /**
   * Complete Page content with eventual contributors' other names as new Contributor in the list
   *
   * @param contributors
   * @return
   */
  private Page<Contributor> completeWithOtherNamesAsContributors(Page<Contributor> contributors) {
    Set<String> fullNamesToAdd = new HashSet<>();
    for (Contributor contributor : contributors.getContent()) {
      if (!contributor.getOtherNames().isEmpty()) {
        for (ContributorOtherName con : contributor.getOtherNames()) {
          fullNamesToAdd.add(con.getFullName());
        }
        // Then clean other names to clear the displayName from other names as these other forms of names are added to the list
        // as distinct contributors
        contributor.getOtherNames().clear();
      }
    }

    if (fullNamesToAdd.isEmpty()) {
      return contributors;
    } else {
      List<Contributor> completedContributors = new ArrayList<>(contributors.getContent());
      int contributorsAdded = 0;
      for (String fullname : fullNamesToAdd) {
        Contributor contributor = new Contributor();
        if (fullname.contains(",")) {
          String[] splitFullname = fullname.split(",");
          contributor.setLastName(splitFullname[0].trim());
          contributor.setFirstName(splitFullname[1].trim());
        } else {
          contributor.setLastName(fullname);
        }

        if (!completedContributors.stream().anyMatch(c -> c.getFullName().equals(contributor.getFullName()))) {
          completedContributors.add(contributor);
          contributorsAdded++;
        }
      }

      completedContributors = completedContributors.stream().sorted(Comparator.comparing(Contributor::getFullName))
              .collect(Collectors.toList());

      return new PageImpl<>(completedContributors, Pageable.ofSize(completedContributors.size()),
              contributors.getTotalElements() + contributorsAdded);
    }
  }

  /**
   * Delete eventual contributors that are not linked to any Publication anymore.
   *
   * @param contributorIds The contributors to check
   */
  public void deleteContributorsIfOrphan(List<String> contributorIds) {
    for (String previousContributorId : contributorIds) {
      Optional<Contributor> orphanContributor = this.findByResIdAndPublicationsIsNull(previousContributorId);
      if (orphanContributor.isPresent()) {
        this.delete(orphanContributor.get().getResId());
      }
    }
  }
}
