/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Admin - LanguageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.model.Language;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.LanguageSpecification;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.repository.LanguageRepository;

@Service
@ConditionalOnBean(AdminController.class)
public class LanguageService extends ResourceService<Language> {

  Logger log = LoggerFactory.getLogger(LanguageService.class);

  @Override
  public LanguageSpecification getSpecification(Language resource) {
    return new LanguageSpecification(resource);
  }

  public void initDefaultData() {
    List<String> languages = new ArrayList<>();
    languages.add("en");
    languages.add("fr");

    languages.forEach(code -> {
      if (((LanguageRepository) this.itemRepository).findByCode(code) == null) {
        Language l = new Language();
        l.setResId(code.toUpperCase());
        l.setCode(code);
        this.save(l);
        this.log.info("Language '{}' created", code);
      }
    });
  }
}
