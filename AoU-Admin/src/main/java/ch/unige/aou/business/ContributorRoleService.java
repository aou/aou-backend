/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ContributorRoleService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.ContributorRole;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeContributorRole;
import ch.unige.aou.model.publication.PublicationSubtypeContributorRoleDTO;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.ValueWithTranslations;
import ch.unige.aou.repository.ContributorRoleRepository;
import ch.unige.aou.specification.ContributorRoleSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class ContributorRoleService extends AouResourceService<ContributorRole> {
  private static final Logger log = LoggerFactory.getLogger(ContributorRoleService.class);

  private static final String ROLE_AUTHOR_EN = "Author";
  private static final String ROLE_AUTHOR_FR = "Auteur-e";
  private static final String ROLE_COLLABORATOR_EN = "Collaborator";
  private static final String ROLE_COLLABORATOR_FR = "Collaborateur/trice";
  private static final String ROLE_DIRECTOR_EN = "Director";
  private static final String ROLE_DIRECTOR_FR = "Directeur/trice";
  private static final String ROLE_EDITOR_EN = "Scientific publisher";
  private static final String ROLE_EDITOR_FR = "Editeur scientifique";
  private static final String ROLE_PHOTOGRAPHER_EN = "Photographer";
  private static final String ROLE_PHOTOGRAPHER_FR = "Photographe";
  private static final String ROLE_TRANSLATOR_EN = "Translator";
  private static final String ROLE_TRANSLATOR_FR = "Traducteur/trice";
  private static final String ROLE_GUEST_EDITOR_EN = "Guest scientific publisher";
  private static final String ROLE_GUEST_EDITOR_FR = "Éditeur/trice invité-e";

  private final ContributorRoleRepository contributorRoleRepository;
  private final PublicationSubtypeService publicationSubtypeService;
  private final LabeledLanguageService labeledLanguageService;

  public ContributorRoleService(ContributorRoleRepository contributorRoleRepository, PublicationSubtypeService publicationSubtypeService,
          LabeledLanguageService labeledLanguageService) {
    this.contributorRoleRepository = contributorRoleRepository;
    this.publicationSubtypeService = publicationSubtypeService;
    this.labeledLanguageService = labeledLanguageService;
  }

  @Override
  public ContributorRoleSpecification getSpecification(ContributorRole resource) {
    return new ContributorRoleSpecification(resource);
  }

  public ContributorRole findByValue(String name) {
    return this.contributorRoleRepository.findByValue(name);
  }

  /********************
   ** Initialization **
   ********************/

  @Transactional
  public void initDefaultData() {

    ValueWithTranslations valueAuthor = this.newValueWithEngFreTranslations(AouConstants.CONTRIBUTOR_ROLE_AUTHOR, ROLE_AUTHOR_EN,
            ROLE_AUTHOR_FR);
    //    ValueWithTranslations valueCollaborator = this.newValueWithEngFreTranslations("collaborator", ROLE_COLLABORATOR_EN, ROLE_COLLABORATOR_FR);
    ValueWithTranslations valueDirector = this.newValueWithEngFreTranslations(AouConstants.CONTRIBUTOR_ROLE_DIRECTOR, ROLE_DIRECTOR_EN,
            ROLE_DIRECTOR_FR);
    ValueWithTranslations valueEditor = this.newValueWithEngFreTranslations(AouConstants.CONTRIBUTOR_ROLE_EDITOR, ROLE_EDITOR_EN,
            ROLE_EDITOR_FR);
    ValueWithTranslations valueGuest = this.newValueWithEngFreTranslations(AouConstants.CONTRIBUTOR_ROLE_GUEST_EDITOR, ROLE_GUEST_EDITOR_EN,
            ROLE_GUEST_EDITOR_FR);
    ValueWithTranslations valuePhotographer = this.newValueWithEngFreTranslations(AouConstants.CONTRIBUTOR_ROLE_PHOTOGRAPHER,
            ROLE_PHOTOGRAPHER_EN, ROLE_PHOTOGRAPHER_FR);
    ValueWithTranslations valueTranslator = this.newValueWithEngFreTranslations(AouConstants.CONTRIBUTOR_ROLE_TRANSLATOR, ROLE_TRANSLATOR_EN,
            ROLE_TRANSLATOR_FR);
    ValueWithTranslations valueCollaborator = this.newValueWithEngFreTranslations(AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR, ROLE_COLLABORATOR_EN,
            ROLE_COLLABORATOR_FR);


    /**
     * A1   Article scientifique
     * A2   Article professionnel
     * A3   Autre article
     */
    List<String> subtypeCodes = Arrays.asList("A1", "A2", "A3");
    // @formatter:off
    List<ValueWithTranslations> valuesWithTranslations = Arrays.asList(
            valueAuthor,
            valueTranslator,
            valuePhotographer,
            valueCollaborator
    );
    // @formatter:on
    this.createContributorRoles(subtypeCodes, valuesWithTranslations);

    /**
     * D1     Thèse de dictorat
     * D2     Master d'études avancées
     * D3     Master
     * D4     Thèse de privat-docent
     */
    subtypeCodes = Arrays.asList("D1", "D2", "D3", "D4", "D5");
    valuesWithTranslations = Arrays.asList(
            valueAuthor,
            valueDirector,
            valueCollaborator
    );
    this.createContributorRoles(subtypeCodes, valuesWithTranslations);

    /**
     * J1     Numéro de revue
     */
    subtypeCodes = Arrays.asList("J1");
    valuesWithTranslations = Arrays.asList(
            valueEditor,
            valueGuest,
            valueCollaborator
    );
    this.createContributorRoles(subtypeCodes, valuesWithTranslations);

    /**
     * L1     Livre
     * L3     Chapitre de livre
     * L5     Contribution à un dictionnaire / une encyclopédie
     * C1     Actes de conférence
     * C2     Présentation / Intervention
     * C3     Poster
     * C4     Chapitre d'actes
     * R1     Rapport de recherche
     * R2     Rapport technique
     * R3     Working paper
     * R4     Preprint
     */
    subtypeCodes = Arrays.asList("L1", "L3", "L5", "C1", "C2", "C3", "C4", "R1", "R2", "R3", "R4");
    // @formatter:off
    valuesWithTranslations = Arrays.asList(
            valueAuthor,
            valueEditor,
            valueTranslator,
            valuePhotographer,
            valueCollaborator
    );
    // @formatter:on
    this.createContributorRoles(subtypeCodes, valuesWithTranslations);

    /**
     * L2     Ouvrage collectif
     */
    subtypeCodes = Arrays.asList("L2");
    valuesWithTranslations = Arrays.asList(
            valueEditor,
            valueTranslator,
            valuePhotographer,
            valueCollaborator
    );
    this.createContributorRoles(subtypeCodes, valuesWithTranslations);
  }

  public Map<String, List<PublicationSubtypeContributorRoleDTO>> listContributorRoleByPublicationSubType() {
    Page<PublicationSubtype> publicationSubtypeServiceAll = publicationSubtypeService.findAll(Pageable.unpaged());
    Map<String, List<PublicationSubtypeContributorRoleDTO>> listMap = new HashMap<>();
    publicationSubtypeServiceAll.getContent().forEach(item -> {
      List<PublicationSubtypeContributorRole> publicationSubtypeContributorRoles = item.getPublicationSubtypeContributorRoles().stream()
              .filter(PublicationSubtypeContributorRole::getVisible)
              .collect(Collectors.toList());

      List<PublicationSubtypeContributorRoleDTO> dtoItems = publicationSubtypeContributorRoles.stream()
              .map(PublicationSubtypeContributorRoleDTO::new)
              .sorted(Comparator.comparingInt(PublicationSubtypeContributorRoleDTO::getSortValue))
              .collect(Collectors.toList());
      listMap.put(item.getResId(), dtoItems);
    });
    return listMap;
  }

  private void createContributorRoles(List<String> subtypeCodes, List<ValueWithTranslations> valuesWithTranslations) {
    List<ContributorRole> contributorRoles = new ArrayList<>();
    for (ValueWithTranslations valueWithTranslations : valuesWithTranslations) {
      contributorRoles.add(this.createIfNotExists(valueWithTranslations));
    }
    this.createContributorRolesForPublicationSubtypes(subtypeCodes, contributorRoles);
  }

  private ContributorRole createIfNotExists(ValueWithTranslations valueWithTranslations) {
    ContributorRole dft = this.findByValue(valueWithTranslations.getValue());
    if (dft == null) {
      dft = new ContributorRole();
      dft.setValue(valueWithTranslations.getValue());
      dft.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_ENGLISH),
              this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)));
      dft.getLabels().add(new Label(valueWithTranslations.getTranslation(AouConstants.LANG_CODE_FRENCH),
              this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)));
      dft = this.save(dft);
      log.info("ContributorRole '{}' created", valueWithTranslations.getValue());
    }
    return dft;
  }

  private void createContributorRolesForPublicationSubtypes(List<String> subtypeCodes, List<ContributorRole> contributorRoles) {
    for (String subtypeCode : subtypeCodes) {
      this.createContributorRolesForPublicationSubtype(subtypeCode, contributorRoles);
    }
  }

  private void createContributorRolesForPublicationSubtype(String subtypeCode, List<ContributorRole> contributorRoles) {
    PublicationSubtype subtype = this.publicationSubtypeService.findByCode(subtypeCode);
    if (subtype != null) {
      int sortValue = AouConstants.ORDER_INCREMENT;
      boolean updated = false;
      for (ContributorRole dft : contributorRoles) {
        PublicationSubtypeContributorRole psDft = new PublicationSubtypeContributorRole();
        psDft.setPublicationSubtype(subtype);
        psDft.setContributorRole(dft);
        psDft.setSortValue(sortValue);
        psDft.setVisible(!dft.getValue().equals(AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR));
        if (!subtype.getPublicationSubtypeContributorRoles().contains(psDft)) {
          subtype.addPublicationSubtypeContributorRole(psDft);
          sortValue += AouConstants.ORDER_INCREMENT;
          updated = true;
        }
      }
      if (updated) {
        this.publicationSubtypeService.save(subtype);
        log.info("ContributorRoles configured for PublicationSubtype {}", subtype.getName());
      }
    } else {
      log.warn("Unable to configure ContributorRoles for PublicationSubtype {} that does not exist", subtypeCode);
    }
  }
}
