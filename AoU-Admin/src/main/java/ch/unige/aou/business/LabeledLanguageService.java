/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - LabeledLanguageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.LabeledLanguage;
import ch.unige.aou.model.settings.ValueWithTranslations;
import ch.unige.aou.repository.LabeledLanguageRepository;
import ch.unige.aou.specification.LabeledLanguageSpecification;

@Service("labeledLanguageService")
@ConditionalOnBean(AdminController.class)
public class LabeledLanguageService extends AouResourceService<LabeledLanguage> {
  static final Logger log = LoggerFactory.getLogger(LabeledLanguageService.class);

  private final LabeledLanguageRepository labeledLanguageRepository;

  public LabeledLanguageService(LabeledLanguageRepository labeledLanguageRepository) {
    this.labeledLanguageRepository = labeledLanguageRepository;
  }

  @Override
  public Optional<LabeledLanguage> findById(String resId) {
    if (this.isUpperCase(resId)) {
      return super.findById(resId);
    }
    log.warn("LabeledLanguageService.findById() called with lower case value '" + resId + "'");
    return Optional.empty();
  }

  public LabeledLanguage findByCode(String code) {
    if (this.isUpperCase(code)) {
      log.warn("LabeledLanguageService.findByCode() called with upper case value '" + code + "'");
      return null;
    }
    return this.labeledLanguageRepository.findByCode(code);
  }

  private boolean isUpperCase(String str) {
    if (StringTool.isNullOrEmpty(str)) {
      return false;
    }
    return str.toUpperCase().equals(str);
  }

  @Override
  public LabeledLanguageSpecification getSpecification(LabeledLanguage resource) {
    return new LabeledLanguageSpecification(resource);
  }

  public void initDefaultData() {

    /**
     * Default languages using ISO 639-2 code which is the format devised for use by libraries, information services, and publishers
     * to indicate language in the exchange of information, especially in computerized systems.
     * See https://www.loc.gov/standards/iso639-2/faq.html#5
     */
    List<ValueWithTranslations> valueWithTranslations = Arrays.asList(
            this.newValueWithEngFreTranslations("fre", "French", "Français"),
            this.newValueWithEngFreTranslations("eng", "English", "Anglais"),
            this.newValueWithEngFreTranslations("ger", "German", "Allemand"),
            this.newValueWithEngFreTranslations("ara", "Arabic", "Arabe"),
            this.newValueWithEngFreTranslations("chi", "Chinese", "Chinois"),
            this.newValueWithEngFreTranslations("spa", "Spanish", "Espagnol"),
            this.newValueWithEngFreTranslations("gre", "Greek", "Grec"),
            this.newValueWithEngFreTranslations("ita", "Italian", "Italien"),
            this.newValueWithEngFreTranslations("jpn", "Japanese", "Japonais"),
            this.newValueWithEngFreTranslations("dut", "Dutch", "Néerlandais"),
            this.newValueWithEngFreTranslations("por", "Portuguese", "Portugais"),
            this.newValueWithEngFreTranslations("roh", "Romansh", "Romanche"),
            this.newValueWithEngFreTranslations("rus", "Russian", "Russe"),
            this.newValueWithEngFreTranslations("tur", "Turkish", "Turc"),
            this.newValueWithEngFreTranslations("mis", "Other", "Autre")
    );

    /**
     * First ensure all languages exist without labels
     */
    List<String> newLanguageCodes = new ArrayList<>();
    int sortValue = 0;
    for (ValueWithTranslations valueWithTranslation : valueWithTranslations) {
      String code = valueWithTranslation.getValue();
      if (this.labeledLanguageRepository.findByCode(code) == null) {
        sortValue = sortValue + AouConstants.ORDER_INCREMENT;
        LabeledLanguage language = new LabeledLanguage();
        language.setResId(code.toUpperCase());
        language.setCode(code);
        language.setSortValue(sortValue);
        this.save(language);
        log.info("Language '{}' created", code);
        newLanguageCodes.add(code);
      }
    }

    /**
     * Only then check if their labels exist, as label's languages must already exist to be created
     */
    for (ValueWithTranslations valueWithTranslation : valueWithTranslations) {
      String code = valueWithTranslation.getValue();
      if (newLanguageCodes.contains(code)) {
        LabeledLanguage language = this.findByCode(code);
        Set<Label> labels = new HashSet<>();
        labels.add(
                new Label(valueWithTranslation.getTranslation(AouConstants.LANG_CODE_FRENCH), this.findByCode(AouConstants.LANG_CODE_FRENCH)));
        labels.add(
                new Label(valueWithTranslation.getTranslation(AouConstants.LANG_CODE_ENGLISH), this.findByCode(AouConstants.LANG_CODE_ENGLISH)));
        language.setLabels(labels);
        this.save(language);
        log.info("Language's labels created for language '{}'", code);
      }
    }
  }
}
