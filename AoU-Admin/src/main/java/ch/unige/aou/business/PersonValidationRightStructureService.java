/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonValidationRightStructureService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.display.PublicationSubtypeValidationRightDTO;
import ch.unige.aou.model.display.StructureValidationRightDTO;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.settings.ValidationRight;
import ch.unige.aou.specification.StructureSpecification;
import ch.unige.aou.specification.ValidationRightSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PersonValidationRightStructureService extends JoinResource3TiersService<Person, Structure, PublicationSubtype, ValidationRight> {

  private PublicationSubtypeService publicationSubtypeService;

  public PersonValidationRightStructureService(PublicationSubtypeService publicationSubtypeService) {
    this.publicationSubtypeService = publicationSubtypeService;
  }

  @Override
  public List<String> getDefaultGrandChildIds() {
    return this.publicationSubtypeService.getAllIds();
  }

  /****************************************************************************************************
   * CRUD methods
   */

  @Override
  public Join3TiersSpecification<ValidationRight> getJoinSpecification(ValidationRight validationRight) {
    return new ValidationRightSpecification(validationRight);
  }

  @Override
  protected SolidifySpecification<Structure> getChildSpecification(Structure structure) {
    return new StructureSpecification(structure);
  }

  @Override
  protected String getChildToRelationPropertyName() {
    return ValidationRight.STRUCTURE_RELATION_PROPERTY_NAME;
  }

  @Override
  public Person getEmptyParentResourceObject() {
    return new Person();
  }

  @Override
  public Structure getEmptyChildResourceObject() {
    return new Structure();
  }

  @Override
  public PublicationSubtype getEmptyGrandChildResourceObject() {
    return new PublicationSubtype();
  }

  @Override
  public ValidationRight getEmptyJoinResourceObject() {
    return new ValidationRight();
  }

  @Override
  public void setParentResource(ValidationRight validationRight, Person person) {
    validationRight.setPerson(person);
  }

  @Override
  public void setChildResource(ValidationRight validationRight, Structure structure) {
    validationRight.setStructure(structure);
  }

  @Override
  public void setGrandChildResource(ValidationRight validationRight, PublicationSubtype publicationSubtype) {
    validationRight.setPublicationSubtype(publicationSubtype);
  }

  @Override
  public PublicationSubtype getGrandChildResource(ValidationRight validationRight) {
    return validationRight.getPublicationSubtype();
  }

  /****************************************************************************************************
   * DISPLAY
   */

  @Override
  public Relation3TiersChildDTO getChildDTO(Structure structure, List<ValidationRight> validationRights) {
    List<PublicationSubtypeValidationRightDTO> publicationSubtypesDTOList = new ArrayList<>();
    for (ValidationRight validationRight : validationRights) {
      publicationSubtypesDTOList.add(this.getGrandChildDTO(validationRight));
    }
    return new StructureValidationRightDTO(structure, publicationSubtypesDTOList);
  }

  @Override
  public PublicationSubtypeValidationRightDTO getGrandChildDTO(ValidationRight joinResource) {
    return new PublicationSubtypeValidationRightDTO(joinResource);
  }
}
