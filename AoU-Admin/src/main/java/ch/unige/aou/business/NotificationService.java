/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - NotificationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.notification.EventMessage;
import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.repository.NotificationRepository;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.specification.NotificationSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class NotificationService extends AouResourceService<Notification> {
  private static final Logger log = LoggerFactory.getLogger(NotificationService.class);

  private static final String MESSAGE_PUBLICATION_TO_VALIDATE = "notifications.message.new_deposit_to_validate";
  private static final String MESSAGE_MY_INDIRECT_PUBLICATION_VALIDATED = "notifications.message.indirect_deposit_validated";
  private static final String MESSAGE_MY_PUBLICATION_VALIDATED = "notifications.message.my_deposit_validated";
  private static final String MESSAGE_MY_PUBLICATION_REQUIRES_FEEDBACK = "notifications.message.deposit_requires_feedback";
  private static final String MESSAGE_MY_PUBLICATION_REJECTED = "notifications.message.my_deposit_rejected";
  private static final String MESSAGE_NEW_COMMENT_TO_VALIDATOR = "notifications.message.comment_deposit_validator";
  private static final String MESSAGE_NEW_COMMENT_TO_CONTRIBUTORS = "notifications.message.comment_deposit_contributor";
  private static final String MESSAGE_NEW_COMMENT_TO_DEPOSITOR = "notifications.message.comment_deposit_depositor";
  private static final String MESSAGE_NEW_PUBLICATION_IMPORTED_FROM_ORCID = "notifications.message.new_publication_imported_from_orcid";
  private static final String MESSAGE_NEW_PUBLICATION_EXPORTED_TO_ORCID = "notifications.message.new_publication_exported_to_orcid";

  private final UserService userService;
  private final PersonService personService;
  private final NotificationTypeService notificationTypeService;
  private final PublicationService publicationService;
  private final MetadataService metadataService;

  public NotificationService(UserService userService, PersonService personService, NotificationTypeService notificationTypeService,
          PublicationService publicationService, MetadataService metadataService) {
    this.userService = userService;
    this.personService = personService;
    this.notificationTypeService = notificationTypeService;
    this.publicationService = publicationService;
    this.metadataService = metadataService;
  }

  @Override
  public NotificationSpecification getSpecification(Notification notification) {
    return new NotificationSpecification(notification);
  }

  /**
   * Use an async event to create notifications for a new Event as the treatment may be time consuming
   * <p>
   * Note: Transactional is required as the Event contains a Publication instance with lazy loaded structures
   * --> a JPA session is required to retrieve them
   *
   * @param eventMessage
   */
  @TransactionalEventListener(fallbackExecution = true)
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  @Async
  public void listenToNewEventCreation(EventMessage eventMessage) {

    Event event = eventMessage.getEvent();

    switch (event.getEventType().getResId()) {
      case EventType.PUBLICATION_TO_VALIDATE_ID:
        this.createPublicationToValidateNotifications(event);
        break;
      case EventType.PUBLICATION_VALIDATED_ID:
        this.createPublicationValidatedNotifications(event);
        break;
      case EventType.PUBLICATION_FEEDBACK_REQUIRED_ID:
        this.createPublicationFeedbackRequiredNotification(event);
        break;
      case EventType.PUBLICATION_REJECTED_ID:
        this.createPublicationRejectedNotifications(event);
        break;
      case EventType.COMMENT_FOR_VALIDATORS_IN_PUBLICATION_ID:
        this.createValidatorCommentInPublicationNotification(event);
        break;
      case EventType.COMMENT_IN_PUBLICATION_ID:
        this.createValidatorCommentInPublicationNotification(event);
        this.createContributorsCommentInPublicationNotification(event);
        this.createDepositorCommentInPublicationNotification(event);
        break;
      case EventType.PUBLICATION_DETECTED_ON_ORCID_ID:
        this.createPublicationImportedFromOrcidNotifications(event);
        break;
      case EventType.PUBLICATION_EXPORTED_TO_ORCID_ID:
        this.createPublicationExportedToOrcidNotifications(event);
        break;
      default:
        break;
    }
  }

  /**
   * Create a notification for the depositor when a publication is rejected
   *
   * @param event The new event for which notifications must be created
   */
  private void createPublicationRejectedNotifications(Event event) {
    log.info("will create notifications for new Publications rejected");

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    this.createNotification(publication.getCreator(), NotificationType.MY_PUBLICATION_REJECTED.getResId(), event,
            this.messageService.get(MESSAGE_MY_PUBLICATION_REJECTED,
                    new Object[] { publication.getLastComment(false).getText(),
                            publication.getLastComment(false).getPerson().getFullName() }));

    log.info("notifications for new Publications rejected created");
  }

  /**
   * Create a notification for all users that can validate this publication
   *
   * @param event The new event for which notifications must be created
   */
  public void createPublicationToValidateNotifications(Event event) {
    log.info("will create notifications for new Publications to validate");

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    List<Person> validators = this.personService.getValidators(publication);

    for (Person validator : validators) {
      this.createNotification(validator, NotificationType.PUBLICATION_TO_VALIDATE.getResId(), event,
              this.messageService.get(MESSAGE_PUBLICATION_TO_VALIDATE, new Object[] { publication.getTitle() }));
    }

    log.info("notifications for new Publications to validate created");
  }

  /**
   * Create a notification for the depositor and for all UNIGE contributors (the ones having a cnIndividu)
   * when a Publication gets validated.
   * <p>
   * Notifications get created only if the publication has never been in COMPLETED status before, meaning updated publications don't generate
   * validation notifications.
   *
   * @param event
   */
  public void createPublicationValidatedNotifications(Event event) {
    long countOccurrences = this.publicationService.findStatusHistoryOrderByLastDate(
                    event.getPublication().getResId())
            .stream()
            .filter(s -> s.getStatus().equals(Publication.PublicationStatus.COMPLETED.name())).count();
    // check if the publication was in COMPLETED status more than once. see: PublicationProcessingService.processInPreparationPublication()
    // If countOccurrences == 1, this is the first validation --> create notifications
    if (countOccurrences <= 1) {
      this.createPublicationValidatedNotificationForDepositor(event);
      this.createPublicationValidatedNotificationsForContributors(event);
      this.sendEmailToDoctorIfApply(event);
    }
    this.markAsReadPreviousRelatedNotifications(event.getPublication());
  }

  private void markAsReadPreviousRelatedNotifications(Publication publication) {
    List<Notification> notificationsList = ((NotificationRepository) this.itemRepository).findByPublicationId(publication.getResId());

    //mark as read all notifications that are not from type MY_PUBLICATION_VALIDATED and MY_INDIRECT_PUBLICATION_VALIDATED
    notificationsList.stream().filter(n -> !n.getNotificationType().equals(NotificationType.MY_INDIRECT_PUBLICATION_VALIDATED) &&
                    !n.getNotificationType().equals(NotificationType.MY_PUBLICATION_VALIDATED))
            .forEach(n -> {
              n.setReadTime(OffsetDateTime.now());
              this.save(n);
            });
  }

  /**
   * Update the sentTime property of a Notification
   * Note: Transactional is required to allow the method to be called from a scheduled task context that runs outside of the main thread JPA session
   *
   * @param notificationId
   * @param sentTime
   * @return
   */
  @Transactional
  public Notification updateSentTime(String notificationId, OffsetDateTime sentTime) {
    Notification notif = this.findOne(notificationId);
    notif.setSentTime(sentTime);
    return this.save(notif);
  }

  private void createPublicationValidatedNotificationForDepositor(Event event) {
    log.info("will create notifications for validated Publications to depositors");

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    this.createNotification(publication.getCreator(), NotificationType.MY_PUBLICATION_VALIDATED.getResId(), event,
            this.messageService.get(MESSAGE_MY_PUBLICATION_VALIDATED, new Object[] { publication.getTitle() }));

    log.info("notifications for validated Publications to depositors created");
  }

  private void sendEmailToDoctorIfApply(Event event) {
    log.info("will send email for validated Thesis Publications to doctoral if different email from depositor email");

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());
    if (publication.getSubtype().getName().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_NAME)) {
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
      final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
      List<User> recipientUsers = this.userService.findByPersonResId(publication.getCreator().getResId());

      if (!this.metadataService.isBefore2010orBeforeAuthorJoiningUnige(publication)
              && recipientUsers.stream().noneMatch(u -> u.getEmail().equals(metadataExtractor.getDoctorEmail(depositDoc)))) {
        this.sendPublicationEmailToDoctoralStudent(publication, EmailMessage.EmailTemplate.NEW_PUBLICATION_VALIDATED_DOCTOR_THESIS,
                metadataExtractor.getDoctorEmail(depositDoc));
      }
    }

    log.info("email sent of validated Thesis Publications to doctoral created");
  }

  private void createValidatorCommentInPublicationNotification(Event event) {
    log.info("will create notifications to validators for a new comment on Publication '{}'", event.getPublication().getResId());

    // useful to prevent a lazy loading exception on publicationStructures when calling `this.personService.getValidators(publication)`
    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    List<Person> validators = this.personService.getValidators(publication);

    for (Person validator : validators) {
      this.createNotification(validator, NotificationType.PUBLICATION_TO_VALIDATE_COMMENTED.getResId(), event,
              this.messageService.get(MESSAGE_NEW_COMMENT_TO_VALIDATOR,
                      new Object[] { publication.getLastComment(true).getText(),
                              publication.getLastComment(true).getPerson().getFullName() }));
    }

    log.info("notifications for new validators comments on publication created");
  }

  private void createContributorsCommentInPublicationNotification(Event event) {
    log.info("will create notifications to contributors for a new comment on Publication '{}'", event.getPublication().getResId());

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    String message = this.messageService.get(MESSAGE_NEW_COMMENT_TO_CONTRIBUTORS,
            new Object[] { publication.getLastComment(false).getText(),
                    publication.getLastComment(false).getPerson().getFullName() });
    this.createNotificationForUnigeContributors(event, NotificationType.MY_INDIRECT_PUBLICATION_COMMENTED, message);

    log.info("notifications to contributors for a new comment on Publication created");
  }

  private void createDepositorCommentInPublicationNotification(Event event) {
    log.info("will create notification to depositor for a new comment on Publication '{}'", event.getPublication().getResId());

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    this.createNotification(publication.getCreator(), NotificationType.MY_PUBLICATION_COMMENTED.getResId(), event,
            this.messageService.get(MESSAGE_NEW_COMMENT_TO_DEPOSITOR,
                    new Object[] { publication.getLastComment(false).getText(),
                            publication.getLastComment(false).getPerson().getFullName() }));

    log.info("notification to depositor for a new comment on Publication created");
  }

  private void createPublicationValidatedNotificationsForContributors(Event event) {
    log.info("will create notifications to UNIGE contributors for the validated Publication '{}'", event.getPublication().getResId());

    String message = this.messageService.get(MESSAGE_MY_INDIRECT_PUBLICATION_VALIDATED, new Object[] { event.getPublication().getTitle() });
    this.createNotificationForUnigeContributors(event, NotificationType.MY_INDIRECT_PUBLICATION_VALIDATED, message);

    log.info("notifications to UNIGE contributors for the validated Publication '{}' created", event.getPublication().getResId());
  }

  private void createPublicationFeedbackRequiredNotification(Event event) {
    log.info("will create notification for publication requiring feedback");

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    this.createNotification(publication.getCreator(), NotificationType.MY_PUBLICATION_FEEDBACK_REQUIRED.getResId(), event,
            this.messageService.get(MESSAGE_MY_PUBLICATION_REQUIRES_FEEDBACK,
                    new Object[] { publication.getLastComment(false).getText(),
                            publication.getLastComment(false).getPerson().getFullName() }));

    log.info("notification created for publication requiring feedback");

    this.sendPublicationEmailToDepositor(publication, EmailMessage.EmailTemplate.MY_DEPOSIT_REQUIRES_FEEDBACK);
  }

  private void createPublicationImportedFromOrcidNotifications(Event event) {
    log.info("will create notification for publication imported from ORCID for person " + event.getTriggerBy());

    this.createNotification(event.getTriggerBy(), NotificationType.PUBLICATION_DETECTED_ON_ORCID.getResId(), event,
            this.messageService.get(MESSAGE_NEW_PUBLICATION_IMPORTED_FROM_ORCID, new Object[] { event.getPublication().getTitle() }));
  }

  private void createPublicationExportedToOrcidNotifications(Event event) {
    log.info("will create notification for publication exported to ORCID for person " + event.getTriggerBy());

    this.createNotification(event.getTriggerBy(), NotificationType.PUBLICATION_EXPORTED_TO_ORCID.getResId(), event,
            this.messageService.get(MESSAGE_NEW_PUBLICATION_EXPORTED_TO_ORCID, new Object[] { event.getPublication().getTitle() }));
  }

  private void sendPublicationEmailToDepositor(Publication publication, EmailMessage.EmailTemplate template) {
    List<User> recipientUsers = this.userService.findByPersonResId(publication.getCreator().getResId());
    HashMap<String, Object> parameters = new HashMap<>();
    parameters.put(publication.getResId(), null);

    for (User user : recipientUsers) {
      if (user.isEnabled()) {
        SolidifyEventPublisher.getPublisher()
                .publishEvent(new EmailMessage(user.getEmail(), template, parameters));
      }
    }
  }

  private void sendPublicationEmailToDoctoralStudent(Publication publication, EmailMessage.EmailTemplate template, String email) {
    HashMap<String, Object> parameters = new HashMap<>();
    parameters.put(publication.getResId(), null);

    SolidifyEventPublisher.getPublisher().publishEvent(new EmailMessage(email, template, parameters));
  }

  private void createNotification(Person person, String notificationTypeId, Event event, String message) {

    NotificationType notificationType = this.notificationTypeService.findOne(notificationTypeId);

    Notification notification = new Notification();
    notification.setRecipient(person);
    notification.setNotificationType(notificationType);
    notification.setEvent(event);
    notification.setMessage(message);
    this.save(notification);
  }

  private void createNotificationForUnigeContributors(Event event, NotificationType notificationType, String message) {

    Publication publication = this.publicationService.findOne(event.getPublication().getResId());

    // get contributors that are members of UNIGE
    List<Contributor> unigeContributors = publication.getContributors().stream()
            .filter(contributor -> !StringTool.isNullOrEmpty(contributor.getCnIndividu())).toList();

    //for each, check if they are user of the application
    for (Contributor unigeContributor : unigeContributors) {
      try {
        User user = this.userService.findByExternalUid(CleanTool.getFullUnigeExternalUid(unigeContributor.getCnIndividu()));
        this.createNotification(user.getPerson(), notificationType.getResId(), event, message);
      } catch (NoSuchElementException e) {
        log.debug("No user with cnIndividu {} found", unigeContributor.getCnIndividu());
      }
    }
  }

  public void changeNotificationRecipient(Person creatorToKeep, Person creatorToUpdate) {
    List<Notification> notificationList = ((NotificationRepository) this.itemRepository).findByRecipientResId(creatorToUpdate.getResId());
    for (Notification notification : notificationList) {
      notification.setRecipient(creatorToKeep);
      this.itemRepository.save(notification);
    }
  }
}
