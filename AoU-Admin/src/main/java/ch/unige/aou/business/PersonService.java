/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.PersonWithOrcid;
import ch.unige.solidify.service.PersonWithOrcidService;
import ch.unige.solidify.specification.JoinedTableInValues;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.ContributorUpdateMessage;
import ch.unige.aou.model.display.StructureValidationRight;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.settings.ValidationRight;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.repository.PersonRepository;
import ch.unige.aou.specification.PersonSpecification;
import ch.unige.aou.specification.PublicationSubtypeSpecification;
import ch.unige.aou.specification.StructureSpecification;
import ch.unige.aou.specification.UserSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class PersonService extends AouResourceService<Person> implements PersonWithOrcidService {
  static final Logger log = LoggerFactory.getLogger(PersonService.class);

  private static final String ORCID_TOKEN_MUST_BE_SET_TO_ENABLE_SYNC = "person.error.orcidTokenMustBeSetToEnableSynchronization";
  private static final String SYNC_TO_ORCID_AVAILABLE_TO_UNIGE_MEMBERS_ONLY = "person.error.syncToOrcidAvailableToUnigeMembersOnly";

  private final PersonRepository personRepository;
  private final StructureService structureService;
  private final PublicationSubtypeService publicationSubtypeService;
  private final UserService userService;
  private final PublicationService publicationService;
  private final ContributorService contributorService;

  private final String adminEmail;

  public PersonService(PersonRepository personRepository, StructureService structureService, PublicationSubtypeService publicationSubtypeService,
          UserService userService, @Lazy PublicationService publicationService, ContributorService contributorService,
          AouProperties aouProperties) {
    this.personRepository = personRepository;
    this.structureService = structureService;
    this.publicationSubtypeService = publicationSubtypeService;
    this.userService = userService;
    this.publicationService = publicationService;
    this.contributorService = contributorService;
    this.adminEmail = aouProperties.getAdminEmail();
  }

  /************************************/

  @Override
  public PersonSpecification getSpecification(Person resource) {
    return new PersonSpecification(resource);
  }

  @Override
  public Person save(Person person) {
    Person savedPerson = super.save(person);

    this.storeOrcidInContributor(person);

    if (!person.getLastName().contains(AouConstants.PERMANENT_TEST_DATA_LABEL)) {
      this.sendContributorUpdateMessages(person.getResId());
    }

    return savedPerson;
  }

  @Override
  public void patchResource(Person person, Map<String, Object> updateMap) {

    // reset verifiedOrcid if it gets updated (through admin for instance)
    if (!StringTool.isNullOrEmpty(person.getOrcid()) && Boolean.TRUE.equals(person.isVerifiedOrcid())
            && updateMap.containsKey("orcid") && !person.getOrcid().equals(updateMap.get("orcid"))) {
      updateMap.put("verifiedOrcid", false);
    }

    super.patchResource(person, updateMap);
  }

  @Override
  protected void validateItemSpecificRules(Person person, BindingResult errors) {
    if (Boolean.TRUE.equals(person.getSyncFromOrcidProfile()) && person.getOrcidToken() == null) {
      errors.addError(new FieldError(person.getClass().getSimpleName(), "syncFromOrcidProfile",
              this.messageService.get(ORCID_TOKEN_MUST_BE_SET_TO_ENABLE_SYNC)));
    }

    if (Boolean.TRUE.equals(person.getSyncToOrcidProfile())) {
      if (this.getUnigeCnIndividus(person).isEmpty()) {
        errors.addError(new FieldError(person.getClass().getSimpleName(), "syncToOrcidProfile",
                this.messageService.get(SYNC_TO_ORCID_AVAILABLE_TO_UNIGE_MEMBERS_ONLY, new Object[] { this.adminEmail })));
      }

      if (person.getOrcidToken() == null) {
        errors.addError(new FieldError(person.getClass().getSimpleName(), "syncToOrcidProfile",
                this.messageService.get(ORCID_TOKEN_MUST_BE_SET_TO_ENABLE_SYNC)));
      }
    }
  }

  /**
   * Return the Person id linked to the Authentication.
   * <p>
   * - It uses REST HTTP requests to get the Person linked to OAuth2 token (call is made on Admin)
   *
   * @param authentication
   * @return String the personId (existing in the Admin module)
   */
  public String getLinkedPersonId(Authentication authentication) {
    return this.authenticatedUserProvider.getPersonId(authentication);
  }

  /**
   * Check from each user linked to the person
   *
   * @return
   */
  public Optional<String> getAuthenticatedUserUnigeCnIndividu() {
    Person person = this.getLinkedPerson((SecurityContextHolder.getContext().getAuthentication()));
    List<User> userList = this.userService.findByPersonResId(person.getResId());
    User userUnige = userList.stream().filter(u -> u.getExternalUid().endsWith(AouConstants.UNIGE_EXTERNAL_UID_SUFFIX)
            || u.getExternalUid().endsWith(AouConstants.TEST_EXTERNAL_UID_SUFFIX)).findFirst().orElse(null);
    return userUnige != null ? this.externalUidToUnigeCnIndividu(userUnige.getExternalUid()) : Optional.empty();
  }

  public List<String> getUnigeCnIndividus(Person person) {
    return person.getUsers().stream().filter(user -> user.getExternalUid().endsWith(AouConstants.UNIGE_EXTERNAL_UID_SUFFIX))
            .map(user -> StringTool.substringBefore(user.getExternalUid(), "@")).toList();
  }

  public Person getLinkedPerson(Authentication authentication) {
    String resId = this.getLinkedPersonId(authentication);
    return this.findOne(resId);
  }

  public boolean isAllowedToValidate(String personId, Publication publication) {
    Map<String, List<String>> fullStructuresValidationRights = this.getValidablePublicationSubtypesByStructure(personId);

    List<String> publicationStructureIds = publication.getPublicationStructures().stream()
            .map(publicationStructure -> publicationStructure.getCompositeKey().getStructure().getResId()).collect(Collectors.toList());

    List<String> matchingStructureIds = fullStructuresValidationRights.keySet().stream()
            .filter(publicationStructureIds::contains).collect(Collectors.toList());

    for (String structureId : matchingStructureIds) {
      List<String> publicationSubtypeIds = fullStructuresValidationRights.get(structureId);
      if (publicationSubtypeIds.contains(publication.getSubtype().getResId())) {
        return true;
      }
    }
    return false;
  }

  public List<Person> getValidators(Publication publication) {

    List<Person> validators = new ArrayList<>();

    // get users with validation rights
    final UserSpecification spec = this.userService.getSpecification(new User());
    spec.setOnlyWithValidationRights(true);
    final List<User> validatorUsers = this.userService.findAll(spec, Pageable.unpaged()).toList();

    // for each user check if they can validate the publication
    for (User validatorUser : validatorUsers) {
      Person validatorPerson = validatorUser.getPerson();
      if (this.isAllowedToValidate(validatorPerson.getResId(), publication) && !validators.contains(validatorPerson)) {
        validators.add(validatorPerson);
      }
    }

    return validators;
  }

  /**
   * Return a list of StructureValidationRight which is the list of structures the person has validation rights on with their corresponding
   * publication subtypes that the person can validate
   *
   * @param personId
   * @return
   */
  public List<StructureValidationRight> getStructureValidationRights(String personId) {
    StructureSpecification structureSpecification = this.structureService.getSpecification(new Structure());
    /*
     * select a distinct list of structure with validation rights linked to the person
     */
    structureSpecification.addJoinTableInValues(new JoinedTableInValues(ValidationRight.PERSON_RELATION_PROPERTY_NAME,
            ValidationRight.PATH_TO_PERSON + "." + SolidifyConstants.DB_RES_ID, List.of(personId)));

    final Page<Structure> structuresPage = this.structureService.findAll(structureSpecification, Pageable.unpaged());

    final List<StructureValidationRight> structureValidationRights = new ArrayList<>();
    for (final Structure structure : structuresPage) {
      PublicationSubtypeSpecification publicationSubtypeSpecification = this.publicationSubtypeService
              .getSpecification(new PublicationSubtype());
      publicationSubtypeSpecification.addJoinTableInValues(new JoinedTableInValues(ValidationRight.STRUCTURE_RELATION_PROPERTY_NAME,
              ValidationRight.PATH_TO_PERSON + "." + SolidifyConstants.DB_RES_ID, List.of(personId)));
      publicationSubtypeSpecification.addJoinTableInValues(new JoinedTableInValues(ValidationRight.STRUCTURE_RELATION_PROPERTY_NAME,
              ValidationRight.PATH_TO_STRUCTURE + "." + SolidifyConstants.DB_RES_ID, List.of(structure.getResId())));

      List<PublicationSubtype> publicationSubtypes = this.publicationSubtypeService.findAll(publicationSubtypeSpecification, Pageable.unpaged())
              .getContent();

      StructureValidationRight structureValidationRight = new StructureValidationRight(structure);
      structureValidationRight.addPublicationSubtypes(publicationSubtypes);
      structureValidationRights.add(structureValidationRight);
    }

    return structureValidationRights;
  }

  /**
   * Return a map of [structureId ---> List of publication subtypes] representing the person validation rights Child structures are included
   *
   * @param personId
   * @return
   */
  public Map<String, List<String>> getValidablePublicationSubtypesByStructure(String personId) {

    // Get the list of validation rights that have been given to the user (named "parent structures" in the comments below)
    List<StructureValidationRight> structureValidationRights = this.getStructureValidationRights(personId);

    Map<String, List<String>> fullStructuresValidationRights = new HashMap<>();
    for (StructureValidationRight structureValidationRight : structureValidationRights) {
      fullStructuresValidationRights.put(structureValidationRight.getResId(),
              structureValidationRight.getPublicationSubtypes().stream().map(PublicationSubtype::getResId).collect(Collectors.toList()));
    }

    // list of parent structures ids
    List<String> parentStructureIds = new ArrayList<>(fullStructuresValidationRights.keySet());

    // Complete the map with all child structures as validation rights are inherited from parent structure
    for (String parentStructureId : parentStructureIds) {

      // get list of descendant structures
      List<String> childStructureIds = this.structureService.getAllChildStructureIds(parentStructureId);

      for (String childStructureId : childStructureIds) {

        if (!fullStructuresValidationRights.containsKey(childStructureId)) {
          // if the child structure is not in the list yet, add it with same publication subtypes ids as parent structure
          fullStructuresValidationRights.put(childStructureId, fullStructuresValidationRights.get(parentStructureId));
        } else {
          // if child structure is already in the list, complete the publication subtypes ids with the new parent structure's ones
          // but do not duplicate them (deduplication is done by using a Set)
          Set<String> set = new HashSet<>();
          set.addAll(fullStructuresValidationRights.get(childStructureId));
          set.addAll(fullStructuresValidationRights.get(parentStructureId));
          List<String> mergedSubtypeIds = new ArrayList<>(set);
          fullStructuresValidationRights.put(childStructureId, mergedSubtypeIds);
        }
      }
    }

    return fullStructuresValidationRights;
  }

  @Transactional
  public boolean hasSubscribedToNotificationType(String resId, NotificationType notificationType) {
    Person person = this.findOne(resId);
    return person.getSubscribedNotifications().stream()
            .anyMatch(subscribedNotificationType -> subscribedNotificationType.getResId().equals(notificationType.getResId()));
  }

  public List<Person> findTestPeople() {
    return this.personRepository.findByFirstNameStartingWith(AouConstants.PERMANENT_TEST_DATA_LABEL);
  }

  public Person initDefaultData(AouProperties.Test.User user) {
    final String permanentLabel = AouConstants.PERMANENT_TEST_DATA_LABEL;
    final String firstName = permanentLabel + user.getName() + " First Name";
    final String lastName = permanentLabel + user.getName() + " Last Name";
    Person testPerson = this.personRepository.findByFirstName(firstName);
    if (testPerson == null) {
      testPerson = new Person();
      testPerson.setResId(user.getName());
      testPerson.setFirstName(firstName);
      testPerson.setLastName(lastName);
      this.save(testPerson);
      log.info("Person {} created", testPerson.getFirstName());
    }
    return testPerson;
  }

  @Override
  public PersonWithOrcid getPerson(Authentication authentication) {
    final String authUserId = authentication.getName();
    final User user = this.userService.findByExternalUid(authUserId);
    if (user != null) {
      return user.getPerson();
    } else {
      throw new SolidifyRuntimeException("User referenced by " + authUserId + " is not in database");
    }
  }

  public Person findByOrcid(String orcid) {
    return this.personRepository.findByOrcid(orcid);
  }

  @Override
  public PersonWithOrcid save(PersonWithOrcid personWithOrcid) {
    if (personWithOrcid instanceof Person person) {
      Person savedPerson = this.save(person);
      this.reindexUnigePersonsPublications(savedPerson.getResId());
      return savedPerson;
    } else {
      throw new SolidifyRuntimeException("PersonService cannot save instance of " + personWithOrcid.getClass());
    }
  }

  @Override
  protected Person afterFind(Person person) {
    person.getUsers().forEach(u -> { //Avoid using the findFirst here otherwise we get error for generating Doc
      if (u.isEnabled()) {  // get the first user enabled
        person.setEmail(u.getEmail());
        return;   // break the forEach when we found the first user
      }
    });
    return person;
  }

  /**
   * Reindex all publications that are linked to this person by using the corresponding users' UNIGE cnIndividu
   *
   * @param personId
   */
  public void reindexUnigePersonsPublications(String personId) {
    List<String> unigeCnIndividus = this.getUnigeCnIndividus(personId);
    if (!unigeCnIndividus.isEmpty()) {
      StringBuilder queryBuilder = new StringBuilder();
      for (String cnIndividu : unigeCnIndividus) {
        if (queryBuilder.length() > 0) {
          queryBuilder.append(" OR ");
        }
        queryBuilder.append(AouConstants.INDEX_FIELD_CONTRIBUTORS_CN_INDIVIDU + ":" + StringTool.removeTrailing(cnIndividu,
                AouConstants.UNIGE_EXTERNAL_UID_SUFFIX));
      }
      String query = queryBuilder.toString();
      this.publicationService.reindexAll(query);
    }
  }

  private void sendContributorUpdateMessages(String personId) {
    List<String> unigeCnIndividus = this.getUnigeCnIndividus(personId);
    for (String cnIndividu : unigeCnIndividus) {
      SolidifyEventPublisher.getPublisher().publishEvent(new ContributorUpdateMessage(CleanTool.externalUidToCnIndividu(cnIndividu)));
    }
  }

  public void storeOrcidInContributor(Person person) {
    if (!StringTool.isNullOrEmpty(person.getOrcid())) {
      List<String> unigeCnIndividus = this.getUnigeCnIndividus(person.getResId());
      for (String cnIndividu : unigeCnIndividus) {
        Contributor contributor = this.contributorService.findByCnIndividu(
                StringTool.removeTrailing(cnIndividu, AouConstants.UNIGE_EXTERNAL_UID_SUFFIX));
        if (contributor != null && (StringTool.isNullOrEmpty(contributor.getOrcid()) || !person.getOrcid().equals(contributor.getOrcid()))) {
          // case 1) no orcid in contributor --> add it with the one from person table
          // case 2) contributor has an orcid, but different from the one in person --> override it
          contributor.setOrcid(person.getOrcid());
          this.contributorService.save(contributor);
        }
      }
    }
  }

  private List<String> getUnigeCnIndividus(String personId) {
    List<User> users = this.userService.findByPersonResId(personId);
    return users.stream()
            .filter(u -> u.getHomeOrganization().equals(AouConstants.UNIGE_HOME_ORGANIZATION_VALUE))
            .map(User::getExternalUid)
            .toList();
  }

  public List<Person> findPeopleWithOrcidToken() {
    return this.personRepository.findByOrcidTokenIsNotNull();
  }
}
