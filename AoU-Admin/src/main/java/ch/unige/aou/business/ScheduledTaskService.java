/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ScheduledTaskService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import static ch.unige.solidify.service.SchedulerService.SchedulingCancelType;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.support.CronExpression;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import jakarta.annotation.PostConstruct;

import ch.unige.solidify.scheduler.AbstractTaskRunnable;
import ch.unige.solidify.service.SchedulerService;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.schedule.ScheduledTask;
import ch.unige.aou.model.schedule.TaskType;
import ch.unige.aou.specification.ScheduledTaskSpecification;
import ch.unige.aou.task.CleanDepositsUpdatedWithoutChangesTask;
import ch.unige.aou.task.NotifyContributorsOnCommentsTask;
import ch.unige.aou.task.NotifyDepositorsOnCommentsTask;
import ch.unige.aou.task.NotifyForgottenPublicationsInProgress;
import ch.unige.aou.task.NotifyForgottenPublicationsToValidatorsTask;
import ch.unige.aou.task.NotifyPublicationsRejectedToDepositorTask;
import ch.unige.aou.task.NotifyPublicationsToValidateTask;
import ch.unige.aou.task.NotifyPublicationsValidatedToContributorsTask;
import ch.unige.aou.task.NotifyPublicationsValidatedToDepositorTask;
import ch.unige.aou.task.NotifyResearchGroupsToValidateTask;
import ch.unige.aou.task.NotifyValidatorsOnCommentsTask;
import ch.unige.aou.task.ReindexPublicationsWithEndingEmbargoTask;
import ch.unige.aou.task.SyncThesisToSwissNationalLibraryTask;
import ch.unige.aou.task.SynchronizeOrcidProfilesTask;
import ch.unige.aou.task.SynchronizeResearchGroupsTask;
import ch.unige.aou.task.TaskExecutorService;

@Service
@ConditionalOnBean(AdminController.class)
public class ScheduledTaskService extends AouResourceService<ScheduledTask> {

  private static final Logger log = LoggerFactory.getLogger(ScheduledTaskService.class);

  private static final String INVALID_CRON_EXPRESSION = "scheduledTask.error.invalid_cron_expression";

  private final SchedulerService<ScheduledTask> schedulerService;
  private final NotificationService notificationService;
  private final TaskExecutorService taskExecutorService;

  public ScheduledTaskService(AouProperties aouProperties, SchedulerService<ScheduledTask> schedulerService,
          NotificationService notificationService,
          TaskExecutorService taskExecutorService) {
    this.schedulerService = schedulerService;
    this.schedulerService.setAdditionalThreadsSize(aouProperties.getParameters().getSchedulerThreadPoolAdditionalSize());
    this.notificationService = notificationService;
    this.taskExecutorService = taskExecutorService;
  }

  @Override
  public SolidifySpecification<ScheduledTask> getSpecification(ScheduledTask scheduledTask) {
    return new ScheduledTaskSpecification(scheduledTask);
  }

  @Override
  protected void validateItemSpecificRules(ScheduledTask scheduledTask, BindingResult errors) {
    if (!StringTool.isNullOrEmpty(scheduledTask.getCronExpression())) {
      try {
        CronExpression.parse(scheduledTask.getCronExpression());
      } catch (IllegalArgumentException e) {
        errors.addError(new FieldError(scheduledTask.getClass().getSimpleName(), "cronExpression",
                this.messageService.get(INVALID_CRON_EXPRESSION, new Object[] { scheduledTask.getCronExpression() })));
      }
    }
  }

  @Override
  public ScheduledTask save(ScheduledTask scheduledTask) {
    ScheduledTask savedScheduledTask = super.save(scheduledTask);

    if (scheduledTask.isNameDirty() || scheduledTask.isTaskTypeDirty() || scheduledTask.isCronExpressionDirty()
            || scheduledTask.isEnabledDirty()) {

      // prevent dirty fields to be still dirty when lastExecution is updated from AbstractTaskRunnable
      scheduledTask.resetDirtyFields();

      this.applyTaskScheduling(savedScheduledTask);
    }

    return savedScheduledTask;
  }

  /**
   * Cancel task scheduling if it is running, then delete the task itself
   *
   * @param id
   */
  @Override
  public void delete(String id) {
    if (this.existsById(id)) {
      this.cancelTaskSchedulingIfScheduled(this.findOne(id), SchedulingCancelType.DONT_INTERRUPT_RUNNING_TASK);
    }
    super.delete(id);
  }

  /**
   * Cancel all task schedulings if they are running, then delete the tasks themselves
   *
   * @param scheduledTasks
   */
  @Override
  public void deleteAll(Iterable<? extends ScheduledTask> scheduledTasks) {
    for (ScheduledTask scheduledTask : scheduledTasks) {
      this.cancelTaskSchedulingIfScheduled(scheduledTask, SchedulingCancelType.DONT_INTERRUPT_RUNNING_TASK);
    }
    super.deleteAll(scheduledTasks);
  }

  /**
   * Get all tasks from database and (re)start scheduling for the ones that are enabled.
   * The tasks that are disabled get unscheduled.
   */
  @PostConstruct()
  public void scheduleEnabledTasks() {
    this.disableAllTasksScheduling();
    List<ScheduledTask> enabledScheduledTaskList = this.findAll(Pageable.unpaged()).toList().stream().filter(ScheduledTask::isEnabled)
            .collect(Collectors.toList());
    if (!enabledScheduledTaskList.isEmpty()) {
      for (ScheduledTask scheduledTask : enabledScheduledTaskList) {
        this.applyTaskScheduling(scheduledTask);
      }
    } else {
      log.info("There is no task to schedule");
    }
  }

  /**
   * Cancel all tasks scheduling. If a task is running it completes until the end.
   */
  public void disableAllTasksScheduling() {
    this.schedulerService.cancelAllTasksScheduling();
    log.info("All scheduled tasks cancelled");
  }

  /**
   * unschedule and/or (re)start a task depending on its 'enabled' property
   *
   * @param scheduledTask
   */
  public void applyTaskScheduling(ScheduledTask scheduledTask) {
    this.cancelTaskSchedulingIfScheduled(scheduledTask, SchedulingCancelType.DONT_INTERRUPT_RUNNING_TASK);
    this.startTaskSchedulingIfEnabled(scheduledTask);
  }

  /**
   * Unschedule a task and stop its execution if it is running
   *
   * @param scheduledTask
   */
  public void killTask(ScheduledTask scheduledTask) {
    this.schedulerService.cancelTaskScheduling(scheduledTask, SchedulingCancelType.INTERRUPT_RUNNING_TASK);
    scheduledTask.setEnabled(false);
    this.save(scheduledTask);
    log.info("Scheduling cancelled for task '{}' ({}) with kill running task signal", scheduledTask.getResId(), scheduledTask.getName());
  }

  /****************************************************************/

  private void startTaskSchedulingIfEnabled(ScheduledTask scheduledTask) {
    if (Boolean.TRUE.equals(scheduledTask.isEnabled())) {
      this.schedulerService.startTaskScheduling(this.getRunnableTask(scheduledTask));
      log.info("Scheduling started for task '{}' ({}) [next execution: {}]", scheduledTask.getResId(), scheduledTask.getName(),
              scheduledTask.getNextExecution());
    } else {
      log.debug("Task '{}' ({}) won't be scheduled as it is disabled", scheduledTask.getResId(), scheduledTask.getName());
    }
  }

  private void cancelTaskSchedulingIfScheduled(ScheduledTask scheduledTask, SchedulingCancelType schedulingCancelType) {
    if (this.schedulerService.isTaskScheduled(scheduledTask)) {
      this.schedulerService.cancelTaskScheduling(scheduledTask, schedulingCancelType);
      log.info("Scheduling cancelled for task '{}' ({})", scheduledTask.getResId(), scheduledTask.getName());
    } else {
      log.debug("No need to cancel task '{}' ({}) as it is not scheduled", scheduledTask.getResId(), scheduledTask.getName());
    }
  }

  /**
   * Get a runnable object corresponding to the task type
   *
   * @param scheduledTask
   * @return
   */
  private AbstractTaskRunnable<ScheduledTask> getRunnableTask(ScheduledTask scheduledTask) {
    switch (scheduledTask.getTaskType()) {
      case NOTIFY_DEPOSITS_TO_VALIDATE:
        return new NotifyPublicationsToValidateTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_VALIDATED_DEPOSITS_DEPOSITOR:
        return new NotifyPublicationsValidatedToDepositorTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_VALIDATED_DEPOSITS_CONTRIBUTORS:
        return new NotifyPublicationsValidatedToContributorsTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_DEPOSITS_REJECTED:
        return new NotifyPublicationsRejectedToDepositorTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_VALIDATORS_ON_COMMENTS:
        return new NotifyValidatorsOnCommentsTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_DEPOSITORS_ON_COMMENTS:
        return new NotifyDepositorsOnCommentsTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE:
        return new NotifyForgottenPublicationsToValidatorsTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_FORGOTTEN_DEPOSITS_IN_PROGRESS:
        return new NotifyForgottenPublicationsInProgress(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NOTIFY_CONTRIBUTORS_ON_COMMENTS:
        return new NotifyContributorsOnCommentsTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case SYNCHRONIZE_RESEARCH_GROUPS:
        return new SynchronizeResearchGroupsTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case NEW_RESEARCH_GROUPS_TO_VALIDATE:
        return new NotifyResearchGroupsToValidateTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case REINDEX_PUBLICATIONS_ENDING_EMBARGO:
        return new ReindexPublicationsWithEndingEmbargoTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case SYNC_THESES_TO_SWISS_NATIONAL_LIBRARY:
        return new SyncThesisToSwissNationalLibraryTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case SYNCHRONIZE_ORCID_PROFILE:
        return new SynchronizeOrcidProfilesTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      case CLEAN_DEPOSIT_UPDATED_WITHOUT_CHANGES:
        return new CleanDepositsUpdatedWithoutChangesTask(this, scheduledTask, this.notificationService, this.taskExecutorService);
      default:
        throw new UnsupportedOperationException("Unsupported scheduled task type " + scheduledTask.getTaskType());
    }
  }

  /****************************************************************/

  public void createScheduledTaskFromConfig(AouProperties.ScheduledTaskConfig scheduledTaskConfig) {
    if (!StringTool.isNullOrEmpty(scheduledTaskConfig.getId())) {
      if (!this.existsById(scheduledTaskConfig.getId())) {
        ScheduledTask scheduledTask = new ScheduledTask();
        scheduledTask.setResId(scheduledTaskConfig.getId());
        scheduledTask.setTaskType(TaskType.valueOf(scheduledTaskConfig.getType()));
        scheduledTask.setName(scheduledTaskConfig.getName());
        scheduledTask.setCronExpression(scheduledTaskConfig.getCronExpression());
        scheduledTask.setEnabled(scheduledTaskConfig.isEnabled());
        ScheduledTask savedScheduledTask = this.save(scheduledTask);
        log.info("ScheduledTask '{}' of type '{}' created", savedScheduledTask.getResId(), savedScheduledTask.getTaskType().name());
        if (Boolean.TRUE.equals(savedScheduledTask.isEnabled())) {
          this.startTaskSchedulingIfEnabled(savedScheduledTask);
        }
      }
    } else {
      log.warn("invalid config for scheduled task '{}'", scheduledTaskConfig.getName());
    }
  }
}
