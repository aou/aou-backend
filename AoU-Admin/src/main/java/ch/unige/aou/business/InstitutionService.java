/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - InstitutionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Institution;
import ch.unige.aou.repository.InstitutionRepository;
import ch.unige.aou.specification.InstitutionSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class InstitutionService extends AouResourceService<Institution> {
  static final Logger log = LoggerFactory.getLogger(InstitutionService.class);

  private InstitutionRepository institutionRepository;

  public InstitutionService(InstitutionRepository institutionRepository) {
    this.institutionRepository = institutionRepository;
  }

  @Override
  public InstitutionSpecification getSpecification(Institution institution) {
    return new InstitutionSpecification(institution);
  }

  public void initDefaultData() {
    this.createInstitution("UNIGE", "Université de Genève", "https://www.unige.ch/", "unige.ch");
    this.createInstitution("EPFL", "Ecole Polytechnique Fédérale de Lausanne", "https://www.epfl.ch/", "epfl.ch");
    this.createInstitution("ETH", "ETH Zürich", "https://www.ethz.ch/", "ethz.ch");
    this.createInstitution("UZH", "University of Zurich", "https://www.uzh.ch/", "uzh.ch");
    this.createInstitution("HES-SO", "Haute Ecole Spécialisée de Suisse Occidentale", "https://www.hes-so.ch/", "hesge.ch", "hes-so.ch");
    this.createInstitution("ZHAW", "Zurich University of Applied Sciences", "https://www.zhaw.ch/", "zhaw.ch");
    this.createInstitution("HUG", "Hôpitaux Universitaires de Genève", "https://www.hug-ge.ch", "hcuge.ch");
    this.createInstitution("IHEID", "Graduate Institute of International & Development Studies", "https://www.graduateinstitute.ch/",
            "graduateinstitute.ch");
    this.createInstitution("SWITCHedu-ID", "SWITCH edu-ID", "https://www.switch.ch/edu-id/", "eduid.ch");
  }

  public void createInstitution(String acronym, String name, String webSite, String... emailSuffixes) {
    if (this.institutionRepository.findByName(acronym) == null) {
      Institution i = new Institution();
      i.setName(acronym);
      i.setDescription(name);
      if (!StringTool.isNullOrEmpty(webSite)) {
        try {
          i.setUrl(new URL(webSite));
        } catch (MalformedURLException e) {
          log.warn("Error in creating institution: URL {} is malformed", webSite);
        }
      }
      if (!StringTool.isNullOrEmpty(emailSuffixes[0])) {
        for (String emailSuffix : emailSuffixes) {
          i.getEmailSuffixes().add(emailSuffix);
        }
      }
      this.save(i);
      log.info("Institution '{}' created", acronym);
    }
  }
}
