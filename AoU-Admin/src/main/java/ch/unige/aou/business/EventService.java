/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - EventService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.notification.EventMessage;
import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.repository.EventRepository;
import ch.unige.aou.specification.EventSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class EventService extends AouResourceService<Event> {
  private static final Logger log = LoggerFactory.getLogger(EventService.class);

  private final EventRepository eventRepository;
  private final PublicationService publicationService;

  public EventService(EventRepository eventRepository, PublicationService publicationService) {
    this.eventRepository = eventRepository;
    this.publicationService = publicationService;
  }

  @Override
  public EventSpecification getSpecification(Event event) {
    return new EventSpecification(event);
  }

  public void initDefaultData(Person person) {
    this.createEvent("TEST", EventType.PUBLICATION_CREATED, person);
  }

  private void createEvent(String message, EventType eventType, Person person) {
    log.info("creating event '{}' for", person.getFirstName());
    Event event = this.eventRepository.findByMessage(message);
    if (event == null) {
      event = new Event();
      event.setResId(message.toLowerCase());
      event.setMessage(message);
      event.setEventType(eventType);
      //get permanent publication
      Optional<Publication> publicationOptional = this.publicationService.findById("TEST".toLowerCase());
      if (publicationOptional.isPresent()) {
        log.info("publication exists:" + publicationOptional.get().getTitle());
        event.setPublication(publicationOptional.get());
      } else {
        log.error("Publication '{}' does not exists", "TEST");
      }
      event.setTriggerBy(person);
      this.save(event);
    }
  }

  public void createEvent(Publication publication, EventType type) {
    Event event = new Event();
    event.setPublication(publication);
    event.setTriggerBy(publication.getCreator());
    event.setEventType(type);

    this.saveEvent(event);
  }

  public void createEvent(Publication publication, EventType type, Person person) {
    Event event = new Event();
    event.setPublication(publication);
    event.setTriggerBy(person);
    event.setEventType(type);

    this.saveEvent(event);
  }

  public void createEvent(Comment comment, EventType type) {
    Event event = new Event();
    event.setPublication(comment.getPublication());
    event.setTriggerBy(comment.getPerson());
    event.setEventType(type);

    this.saveEvent(event);
  }

  public void saveEvent(Event event) {
    Event savedEvent = this.save(event);

    EventMessage eventMessage = new EventMessage(savedEvent);

    // publish an application event that will be used to create related notifications
    SolidifyEventPublisher.getPublisher().publishEvent(eventMessage);
  }

  public void changeTriggerByPerson(Person creatorToKeep, Person creatorToUpdate) {
    List<Event> eventList = ((EventRepository) this.itemRepository).findByTriggerByResId(creatorToUpdate.getResId());
    for (Event event : eventList) {
      event.setTriggerBy(creatorToKeep);
      this.itemRepository.save(event);
    }
  }

}
