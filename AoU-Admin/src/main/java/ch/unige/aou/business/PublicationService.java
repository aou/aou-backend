/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import static ch.unige.aou.model.publication.Publication.PublicationStatus.COMPLETED;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.SUBMITTED;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.client.HttpStatusCodeException;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouMetadataValidationException;
import ch.unige.aou.message.PublicationIndexingMessage;
import ch.unige.aou.message.PublicationMessage;
import ch.unige.aou.message.PublicationMetadataUpgradeMessage;
import ch.unige.aou.message.PublicationReindexingMessage;
import ch.unige.aou.message.SavePublicationMessage;
import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFile.AccessLevel;
import ch.unige.aou.model.publication.MetadataDifference;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.Publication.PublicationStatus;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationUserRole;
import ch.unige.aou.model.rest.PersonDTO;
import ch.unige.aou.model.rest.PublicationImportDTO;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.LabeledLanguage;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.repository.PublicationRepository;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.files.PdfDocumentCreator;
import ch.unige.aou.service.metadata.MetadataComparator;
import ch.unige.aou.service.metadata.MetadataExtractor;
import ch.unige.aou.service.metadata.MetadataValidator;
import ch.unige.aou.service.metadata.imports.MetadataImportWorkFlowService;
import ch.unige.aou.service.rest.IndexClientService;
import ch.unige.aou.specification.PublicationContributorOrCreatorSpecification;
import ch.unige.aou.specification.PublicationContributorSpecification;
import ch.unige.aou.specification.PublicationSpecification;
import ch.unige.aou.specification.ValidablePublicationSpecification;

@Service
@ConditionalOnBean(AdminController.class)
@DependsOn("solidifyEventPublisher")
public class PublicationService extends AouResourceService<Publication> {
  private static final Logger log = LoggerFactory.getLogger(PublicationService.class);

  private static final String SUB_SUBTYPE_NOT_PERMITTED = "deposit.error.subSubtype.not_permitted";
  private static final String DUPLICATES_FILES = "deposit.error.document_files.duplicates";
  private static final String FILES_IN_ERROR = "deposit.error.document_files.in_error";
  private static final String FILES_IMPOSSIBLE_TO_DOWNLOADABLE = "deposit.error.document_files.impossible_to_downloadable";

  private final PublicationSubtypeService publicationSubtypeService;
  private final PublicationSubSubtypeService publicationSubSubtypeService;
  private final PersonService personService;
  private final MetadataService metadataService;
  private final EventService eventService;
  private final CommentService commentService;
  private final StructureService structureService;
  private final LabeledLanguageService labeledLanguageService;
  private final ContributorService contributorService;
  private final DocumentFileService documentFileService;
  private final ResearchGroupService researchGroupService;
  private final MetadataImportWorkFlowService metadataImportFlowService;
  private final PdfDocumentCreator pdfDocumentCreator;
  private final HistoryService historyService;
  private final UserService userService;
  private final IndexClientService indexService;
  private final int maxContributorsLimit;

  private final String publicationsFolder;

  private final boolean generateAgreement;

  public PublicationService(AouProperties aouProperties, PublicationSubtypeService publicationSubtypeService,
          PublicationSubSubtypeService publicationSubSubtypeService, PersonService personService, MetadataService metadataService,
          @Lazy EventService eventService, @Lazy CommentService commentService, StructureService structureService,
          LabeledLanguageService labeledLanguageService, ContributorService contributorService, @Lazy DocumentFileService documentFileService,
          ResearchGroupService researchGroupService, @Lazy MetadataImportWorkFlowService metadataImportFlowService,
          PdfDocumentCreator pdfDocumentCreator, HistoryService historyService, UserService userService, IndexClientService indexService) {
    this.publicationSubtypeService = publicationSubtypeService;
    this.publicationSubSubtypeService = publicationSubSubtypeService;
    this.personService = personService;
    this.metadataService = metadataService;
    this.eventService = eventService;
    this.commentService = commentService;
    this.structureService = structureService;
    this.labeledLanguageService = labeledLanguageService;
    this.publicationsFolder = aouProperties.getPublicationsFolder();
    this.contributorService = contributorService;
    this.documentFileService = documentFileService;
    this.researchGroupService = researchGroupService;
    this.metadataImportFlowService = metadataImportFlowService;
    this.pdfDocumentCreator = pdfDocumentCreator;
    this.historyService = historyService;
    this.userService = userService;
    this.indexService = indexService;
    this.generateAgreement = aouProperties.getParameters().isGenerateAgreement();
    this.maxContributorsLimit = aouProperties.getMetadata().getImports().getLimitMaxContributors();
  }

  public Optional<Publication> findByArchiveId(String archiveId) {
    return ((PublicationRepository) this.itemRepository).findByArchiveId(archiveId);
  }

  public Page<Publication> findMyPublications(Publication filterItem, String personId, String cnIndividu, Pageable pageable, String fullName) {
    // Use a specification specifically made to find the publications that has the same creator OR the same cnIndividu
    PublicationContributorOrCreatorSpecification specification = new PublicationContributorOrCreatorSpecification(filterItem, personId,
            cnIndividu);
    specification.setFullName(fullName);
    return this.findAll(specification, pageable);
  }

  /**
   * Returns a list of publications to which the person is a contributor and which have been modified after the given date.
   *
   * @param person
   * @param lastUpdatedAfter
   * @return
   */
  public Set<Publication> findCompletedPublicationsAsContributor(Person person, OffsetDateTime lastUpdatedAfter) {
    Set<Publication> publications = new HashSet<>();
    List<String> unigeCnIndividus = this.personService.getUnigeCnIndividus(person);
    for (String cnIndividu : unigeCnIndividus) {
      Publication filterItem = new Publication();
      filterItem.setStatus(PublicationStatus.COMPLETED);
      PublicationContributorOrCreatorSpecification specification = new PublicationContributorOrCreatorSpecification(filterItem, null,
              cnIndividu);
      specification.setLastUpdatedAfter(lastUpdatedAfter);
      Page<Publication> publicationsPage = this.findAll(specification, Pageable.unpaged());
      publications.addAll(publicationsPage.getContent());
    }
    return publications;
  }

  public Page<Publication> findValidablePublications(String personId, Publication filterItem, Pageable pageable, String fullName) {

    Map<String, List<String>> structuresPublicationsTypesMap = this.personService.getValidablePublicationSubtypesByStructure(personId);

    // Use a specification specifically made to find the publications that can be validated
    ValidablePublicationSpecification specification = new ValidablePublicationSpecification(filterItem, structuresPublicationsTypesMap);
    specification.setFullName(fullName);
    return this.findAll(specification, pageable);
  }

  public Page<Publication> findPublicationByStatusAndLastUpdate(Publication.PublicationStatus status, OffsetDateTime lastUpdatedBefore,
          Pageable pageable) {
    Publication filterItem = new Publication();
    filterItem.setStatus(status);
    PublicationSpecification specification = this.getSpecification(filterItem);
    specification.setLastUpdatedBefore(lastUpdatedBefore);
    return this.findAll(specification, pageable);
  }

  public List<Publication> findThesisLastUpdatedAfter(OffsetDateTime searchFrom) {
    Publication filterItem = new Publication();
    PublicationSubtype thesisSubtype = new PublicationSubtype();
    thesisSubtype.setResId(AouConstants.DEPOSIT_SUBTYPE_THESE_ID);
    filterItem.setSubtype(thesisSubtype);
    PublicationSpecification specification = this.getSpecification(filterItem);
    specification.setLastUpdatedAfter(searchFrom);
    return this.findAll(specification, Pageable.unpaged()).getContent();
  }

  public List<String> getExistingArchiveIds(List<String> archiveIds) {
    return ((PublicationRepository) this.itemRepository).getExistingArchiveIds(archiveIds);
  }

  @Override
  public PublicationSpecification getSpecification(Publication resource) {
    return new PublicationSpecification(resource);
  }

  @Transactional
  public void initDefaultData(Person creator) {
    this.createTestPublication(creator, "TEST");
  }

  private void createTestPublication(Person creator, String resId) {
    if (!this.existsById(resId)) {
      Publication publication = new Publication();
      publication.setResId(resId.toLowerCase());
      publication.setCreator(creator);
      publication.setFormData(AouConstants.TEST_DEPOSIT_JSON_FORM_DATA);
      publication.setTitle(AouConstants.TEST_DEPOSIT_TITLE);
      publication.setStatus(Publication.PublicationStatus.IN_PROGRESS);
      PublicationSubtype subtype = this.publicationSubtypeService.findByCode("A1");
      if (subtype != null) {
        publication.setSubtype(subtype);
      }
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      this.save(publication);
      log.info("Publication '{}' created", resId);
    } else {
      log.info("Publication '{}' already exists", resId);
    }
  }

  /**
   * Check if a publication status can be manually updated to the new given status, according to the publications's management workflow. Some
   * status transitions (e.g. from SUBMITTED to CANONICAL and from CANONICAL to COMPLETED) are considered here as invalid, because even if they
   * exist in the workflow, they are automatically done, and should not be forced manually.
   *
   * @param publication The publication to check
   * @param newStatus   The new status to apply
   * @return True if the status update is valid
   */
  public boolean statusUpdateIsValid(Publication publication, PublicationStatus newStatus) {

    final PublicationStatus currentStatus = publication.getStatus();

    return switch (currentStatus) {
      case IN_PROGRESS -> newStatus == PublicationStatus.IN_VALIDATION;
      case IN_VALIDATION -> newStatus == PublicationStatus.IN_PROGRESS || newStatus == PublicationStatus.SUBMITTED
              || newStatus == PublicationStatus.REJECTED || newStatus == PublicationStatus.FEEDBACK_REQUIRED;
      case IN_ERROR, REJECTED, COMPLETED -> newStatus == PublicationStatus.IN_PROGRESS || newStatus == PublicationStatus.DELETED
              || newStatus == PublicationStatus.CANONICAL || newStatus == PublicationStatus.IN_EDITION;
      case FEEDBACK_REQUIRED -> newStatus == PublicationStatus.IN_PROGRESS || newStatus == PublicationStatus.IN_VALIDATION
              || newStatus == PublicationStatus.IN_EDITION || newStatus == PublicationStatus.UPDATES_VALIDATION;
      case CANONICAL -> newStatus == PublicationStatus.COMPLETED;
      case IN_EDITION -> newStatus == PublicationStatus.COMPLETED || newStatus == PublicationStatus.UPDATES_VALIDATION
              || newStatus == PublicationStatus.CANCEL_EDITION;
      case UPDATES_VALIDATION -> newStatus == PublicationStatus.SUBMITTED || newStatus == PublicationStatus.FEEDBACK_REQUIRED
              || newStatus == PublicationStatus.CANCEL_EDITION;
      default -> false;
    };
  }

  /**
   * When reediting a publication, we need to back up the property metadata to metadata_backup
   *
   * @param publication
   * @param newStatus
   * @return
   */
  public Publication backUpMetadataIfApply(Publication publication, PublicationStatus newStatus) {
    if (publication.getStatus().equals(PublicationStatus.COMPLETED) && newStatus == PublicationStatus.IN_EDITION) {
      publication.setMetadataBackup(publication.getMetadata());
    }
    return publication;
  }

  /******************************************************************************************************/

  @Override
  protected Publication afterFind(Publication publication) {

    AouMetadataVersion currentVersion = publication.getMetadataVersion();
    if (this.upgradeMetadataIfFormatIsPreviousVersion(publication)) {
      // (re)generate Json form data from XML metadata to ensure they are synchronized
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());
      metadataExtractor.setFormDataFromMetadata(publication);
      log.info("Publication '{}' is returned with an upgraded version of its metadata (from version {} to {})", publication.getResId(),
              currentVersion, publication.getMetadataVersion());
    }

    this.setChangeInfoFullName(publication);

    // Set email for publication creator
    Person creator = publication.getCreator();
    List<User> userList = this.userService.findByPersonResId(publication.getCreatorId());
    User user = userList.stream().filter(User::isEnabled).findFirst()
            .orElseThrow(() -> new NoSuchElementException("No user found: we should find at least a user"));
    creator.setEmail(user.getEmail());

    // Set email for publication editor
    if (publication.getLastUpdate() != null && !StringTool.isNullOrEmpty(publication.getLastUpdate().getWho())
            && this.userService.existsByExternalUid(publication.getLastUpdate().getWho())) {
      User editor = this.userService.findByExternalUid(publication.getLastUpdate().getWho());
      publication.setLastEditor(new PersonDTO(editor.getFirstName(), editor.getLastName(), editor.getEmail()));
    }

    return publication;
  }

  @Override
  protected void beforeValidate(Publication publication) {

    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());

    /*
     * (Re)generate:
     *      Json form data from XML metadata if they have been updated
     *    or
     *      XML metadata from Json form data if they have been updated
     */
    boolean xmlMetadataUpdated = publication.isMetadataDirty() && !StringTool.isNullOrEmpty(publication.getMetadata());
    boolean jsonMetadataUpdated = publication.isFormDataDirty() && !StringTool.isNullOrEmpty(publication.getFormData());

    if (xmlMetadataUpdated || jsonMetadataUpdated) {

      if (xmlMetadataUpdated) {
        // metadata are saved as XML
        AouMetadataVersion currentVersion = publication.getMetadataVersion();
        if (this.upgradeMetadataIfFormatIsPreviousVersion(publication)) {
          log.info("Publication '{}' will be stored with an upgraded version of its metadata (from version {} to {})", publication.getResId(),
                  currentVersion, publication.getMetadataVersion());
        }
      } else {
        // metadata are saved as Json --> generate XML metadata
        metadataExtractor.setMetadataFromFormData(publication);
      }

      // clean some metadata values
      metadataExtractor.cleanMetadata(publication);

      metadataExtractor.completeClassificationsMetadataWithDeweyCodes(publication);
      metadataExtractor.completeStructureResId(publication);
      metadataExtractor.completeResearchGroupResId(publication);
      metadataExtractor.completeContributorsCnIndividuFromOrcid(publication);

      if (publication.getFormStep().equals(Publication.DepositFormStep.DESCRIPTION)) {
        metadataExtractor.replaceISSNByISSNL(publication);
        metadataExtractor.replaceShortDOIbyDOI(publication);
      }

      // (re)generate Json form data from XML metadata to ensure they are synchronized
      metadataExtractor.setFormDataFromMetadata(publication);

    } else if (publication.getMetadataValidationType() == null) {
      // if metadata hasn't been updated and the validation type is null, do not validate anything
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
    }

    // If the publication contains files, they should be added in the XML metadata
    if (publication.getDocumentFiles() != null || publication.getThumbnail() != null) {
      metadataExtractor.completeMetadataWithFiles(publication);
    }

    // Replace contributors names by the one that must appear as display name
    if (!publication.getContributorOtherNames().isEmpty()) {
      metadataExtractor.replaceUnigeContributorsDisplayName(publication);

      // (re)generate Json form data from XML metadata to ensure they are synchronized
      metadataExtractor.setFormDataFromMetadata(publication);
    }

    // Set publications properties that are in metadata and must be saved in database
    if (!StringTool.isNullOrEmpty(publication.getMetadata())) {
      metadataExtractor.setPropertiesFromMetadata(publication);
    }

    if (publication.getSubtype() != null && publication.getSubtype().getResId() != null
            && ((publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_ID)
            || publication.getSubtype().getResId().equals(AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_ID))
            && !this.metadataService.isBefore2010orBeforeAuthorJoiningUnige(publication))
            && (!this.historyService.hasBeenUpdatingMetadata(publication.getResId())
            && !this.historyService.hasBeenUpdatingMetadataInBatch(publication.getResId())
            && !publication.getStatus().equals(PublicationStatus.IN_EDITION))) {
      this.generateThesisAgreement(publication);
    }

    if (!publication.getDocumentFiles().isEmpty()) {
      publication.getDocumentFiles().forEach(d -> {
        if (d.getAccessLevel().equals(AccessLevel.RESTRICTED) || d.getAccessLevel().equals(AccessLevel.PRIVATE)) {
          d.setLicense(null); // set license to null if the access level is private or restricted
        }
      });
    }

    if (publication.getImportSource() != null) {
      // When FormStep has the value FILES, the form in the portal has been submitted from the files step.
      // This means that the depositor has been notified by the portal about imported files and do not need to be notified again.
      if (publication.getFormStep() == Publication.DepositFormStep.FILES) {
        publication.setNotifyFilesFromExternalSource(false);
      }
      // When FormStep has the value CONTRIBUTORS, the form in the portal has been submitted from the contributors step.
      // This means that the depositor has been notified by the portal about imported contributors and do not need to be notified again.
      if (publication.getFormStep() == Publication.DepositFormStep.CONTRIBUTORS) {
        publication.setNotifyContributorsFromExternalSource(false);
      }
    }
  }

  @Override
  protected void validateItemSpecificRules(Publication publication, BindingResult errors) {

    boolean hasErrorBefore = errors.hasErrors();

    this.validateSubSubtype(publication, errors);
    this.validateFiles(publication, errors);

    /*
     * Validate metadata content
     */
    final MetadataValidator metadataValidator = this.metadataService.getMetadataValidator(publication.getMetadataVersion());
    metadataValidator.validate(publication, errors);

    if (!hasErrorBefore && errors.hasErrors()) {
      // throw a custom Exception to be caught specifically in order to not log full stacktrace
      throw new AouMetadataValidationException(new ValidationError(errors));
    }
  }

  @Override
  protected void beforeSave(Publication publication) {

    super.beforeSave(publication);

    /*
     * Set publications relations that are in metadata and must be saved in database
     * The method is called after the validation step as it may create new Contributors which are thus validated first.
     */
    if (!StringTool.isNullOrEmpty(publication.getMetadata())) {
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
      metadataExtractor.setRelationsFromMetadata(publication);
    }

    /*
     * Check if status has been updated and update the date of lastStatusUpdate
     */
    if (publication.isStatusDirty()) {
      publication.setLastStatusUpdate(OffsetDateTime.now());
    }

  }

  @Override
  public void patchResource(Publication publication, Map<String, Object> updateMap) {

    // reset statusMessage when status is updated
    if (updateMap.containsKey("status")) {
      updateMap.put("statusMessage", null);
    }

    String unigeDoiToPreserve = this.metadataService.getUnigeDoiToPreserve(publication);
    String unigeUrnToPreserve = this.metadataService.getUnigeUrnToPreserve(publication);

    super.patchResource(publication, updateMap);

    if (!StringTool.isNullOrEmpty(unigeDoiToPreserve)) {
      this.metadataService.restoreUnigeDoiToPreserve(publication, unigeDoiToPreserve);
    }
    if (!StringTool.isNullOrEmpty(unigeUrnToPreserve)) {
      this.metadataService.restoreUnigeUrnToPreserve(publication, unigeUrnToPreserve);
    }
  }

  @Override
  public Publication save(Publication publication) {
    // list contributors before the publication is saved to check after saving if some of them are now orphan and can be deleted
    List<String> currentContributorsIds = publication.getContributors().stream().map(Contributor::getResId).toList();

    final Publication result = super.save(publication);

    // delete eventual new orphan contributors
    this.contributorService.deleteContributorsIfOrphan(currentContributorsIds);

    if (publication.isStatusDirty()) {
      // Do not create events linked to status update, if the status update is related to a batch update
      if (publication.getStatus() == PublicationStatus.UPDATE_IN_BATCH) {
        this.eventService.createEvent(publication, EventType.PUBLICATION_UPDATED_IN_BATCH);
      } else if (!this.statusUpdateIsDueToBatchUpdate(publication.getResId())) {
        switch (publication.getStatus()) {
          case UPDATES_VALIDATION, IN_VALIDATION -> this.eventService.createEvent(publication, EventType.PUBLICATION_TO_VALIDATE);
          case FEEDBACK_REQUIRED -> this.eventService.createEvent(publication.getLastComment(false), EventType.PUBLICATION_FEEDBACK_REQUIRED);
          case COMPLETED -> this.eventService.createEvent(publication, EventType.PUBLICATION_VALIDATED);
          case REJECTED -> this.eventService.createEvent(publication, EventType.PUBLICATION_REJECTED);
        }
      }
    }

    if (publication.isMetadataDirty() || publication.isStatusDirty()) {
      // publish an application event that will be used to index updated publication
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationIndexingMessage(publication.getResId()));
    }
    return result;
  }

  private boolean statusUpdateIsDueToBatchUpdate(String publicationId) {
    return this.historyService.hasBeenUpdatingMetadataInBatch(publicationId);
  }

  /**
   * Save a publication by calling the repository save() method only (methods beforeValidate(), validate() and beforeSave() are not called)
   *
   * @param publication
   * @return
   */
  public Publication simpleSave(Publication publication) {
    return this.itemRepository.save(publication);
  }

  @Override
  public void delete(String publicationId) {

    // list contributors before the publication is deleted to check after deletion if some of them are now orphan and can be deleted
    List<String> currentContributorsIds = new ArrayList<>();
    Publication publication = this.findOne(publicationId);
    if (publication != null) {
      currentContributorsIds = publication.getContributors().stream().map(Contributor::getResId).toList();
    }

    // get the publication files path before deleting the publication to be able to delete its files from disk after the deletion
    final Path publicationFilesFolderPath = this.getPublicationFilesFolderPath(publicationId);

    super.delete(publicationId);

    // delete eventual new orphan contributors
    this.contributorService.deleteContributorsIfOrphan(currentContributorsIds);

    // delete files linked to deleted publication
    if (FileTool.isFolder(publicationFilesFolderPath) && !FileTool.deleteFolder(publicationFilesFolderPath)) {
      throw new SolidifyFileDeleteException(publicationFilesFolderPath.toString());
    }

    // delete publication from indexes
    SolidifyEventPublisher.getPublisher().publishEvent(new PublicationIndexingMessage(publicationId));
  }

  public Publication clonePublication(String publicationId) {
    final Publication originalPublication = this.findOne(publicationId);

    Person creator = this.personService.getLinkedPerson(SecurityContextHolder.getContext().getAuthentication());

    Publication publicationCopy = new Publication(originalPublication);

    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publicationCopy.getMetadataVersion());

    // clean metadata values from identifiers
    metadataExtractor.cleanMetadataToClone(publicationCopy);

    publicationCopy.setCreator(creator);
    publicationCopy.setMetadataValidationType(Publication.MetadataValidationType.NONE);

    Publication clonedPublication = this.save(publicationCopy);
    if (clonedPublication == null) {
      throw new SolidifyRuntimeException("Publication cannot be cloned");
    }
    return clonedPublication;
  }

  public void validateMetadata(Publication publication) {

    final BindingResult errors = new BeanPropertyBindingResult(publication, publication.getClass().getName());

    this.validateSubSubtype(publication, errors);
    this.validateFiles(publication, errors);

    final MetadataValidator metadataValidator = this.metadataService.getMetadataValidator(publication.getMetadataVersion());
    metadataValidator.validate(publication, errors);

    if (errors.hasErrors()) {
      throw new AouMetadataValidationException(new ValidationError(errors));
    }
  }

  /**
   * Check that the eventual subSubtype is valid for the subtype
   *
   * @param publication
   * @return
   */
  private void validateSubSubtype(Publication publication, BindingResult errors) {
    if (publication.getSubSubtype() != null && publication.getSubSubtype().getResId() != null) {
      List<PublicationSubSubtype> subtypesPossibles = this.publicationSubSubtypeService
              .findByPublicationSubtypes(publication.getSubtype().getResId());
      if (subtypesPossibles.stream().noneMatch(psst -> psst.getResId().equals(publication.getSubSubtype().getResId()))) {
        errors.addError(
                new FieldError(publication.getClass().getSimpleName(), "subSubtype", this.messageService.get(SUB_SUBTYPE_NOT_PERMITTED,
                        new Object[] { publication.getSubSubtype().getResId(), publication.getSubtype().getResId() })));
      }
    }
  }

  private void validateFiles(Publication publication, BindingResult errors) {

    if (publication.getMetadataValidationType() != null) {
      if (publication.getMetadataValidationType().equals(Publication.MetadataValidationType.GLOBAL) ||
              publication.getMetadataValidationType().equals(Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION) ||
              (publication.getMetadataValidationType().equals(Publication.MetadataValidationType.CURRENT_STEP)
                      && publication.getFormStep().equals(Publication.DepositFormStep.FILES))
      ) {
        for (int i = 0; i < publication.getDocumentFiles().size(); i++) {
          if (publication.getDocumentFiles().get(i).getStatus().equals(DocumentFile.DocumentFileStatus.IN_ERROR)) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(), "documentFiles", this.messageService.get(FILES_IN_ERROR)));
          }
          if (publication.getDocumentFiles().get(i).getStatus().equals(DocumentFile.DocumentFileStatus.IMPOSSIBLE_TO_DOWNLOAD)) {
            errors.addError(new FieldError(publication.getClass().getSimpleName(), "documentFiles",
                    this.messageService.get(FILES_IMPOSSIBLE_TO_DOWNLOADABLE)));
          }
        }

        this.validateDuplicatedFiles(publication, errors);
      }
    } else {
      throw new SolidifyRuntimeException("MetadataValidationType cannot be null");
    }
  }

  private void validateDuplicatedFiles(Publication publication, BindingResult errors) {
    if (this.hasDuplicateFiles(publication)) {
      errors.addError(new FieldError(publication.getClass().getSimpleName(), "documentFiles", this.messageService.get(DUPLICATES_FILES)));
    }
  }

  /******************************************************************************************************/

  public String convertDefaultProfileDataToJson(Person person) {
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(AouMetadataVersion.getDefaultVersion());

    JSONObject jsonObject = new JSONObject();
    if (!person.getStructures().isEmpty()) {
      JSONArray structuresToJson = metadataExtractor.transformAcademicStructuresToJson(new ArrayList<>(person.getStructures()));
      jsonObject.put("academicStructures", structuresToJson);
    }

    if (!person.getResearchGroups().isEmpty()) {
      JSONArray groupsToJson = metadataExtractor.transformGroupsToJson(new ArrayList<>(person.getResearchGroups()));
      jsonObject.put("groups", groupsToJson);
    }

    return jsonObject.toString();
  }

  public Path getPublicationFilesFolderPath(String publicationId) {
    return this.getPublicationFilesFolderPath(this.findOne(publicationId));
  }

  public Path getPublicationFilesFolderPath(Publication publication) {

    String year = String.valueOf(publication.getCreationTime().getYear());
    NumberFormat formatter = new DecimalFormat("00");
    String month = formatter.format(publication.getCreationTime().getMonth().getValue());

    final Path publicationFilesFolderPath = Paths.get(this.publicationsFolder, year, month, publication.getResId());
    if (!publicationFilesFolderPath.toFile().exists()) {
      try {
        Files.createDirectories(publicationFilesFolderPath);
      } catch (final IOException e) {
        log.error("Problem creating publication directory", e);
        throw new SolidifyProcessingException(
                this.messageService
                        .get("publication.error.path_not_found", new Object[] { publication.getResId(), this.publicationsFolder }));
      }
    }
    return publicationFilesFolderPath;
  }

  /**
   * Create relations of type VALIDATION between the given publication id and all the given structure ids
   *
   * @param publicationId
   * @param structureIds
   */
  public void syncValidationStructureRelations(String publicationId, String[] structureIds) {
    final Publication publication = this.findOne(publicationId);

    List<PublicationStructure> existingValidationStructures = publication.getPublicationStructures().stream()
            .filter(pubStruct -> pubStruct.getLinkType().equals(
                    PublicationStructure.LinkType.VALIDATION)).toList();

    // remove validation structures not it given structure ids
    List<PublicationStructure> publicationStructuresToRemove = new ArrayList<>();
    existingValidationStructures.forEach(publicationStructure -> {
      if (Arrays.stream(structureIds).noneMatch(s -> s.equals(publicationStructure.getStructure().getResId()))) {
        publicationStructuresToRemove.add(publicationStructure);
      }
    });
    publication.getPublicationStructures().removeAll(publicationStructuresToRemove);

    // add relations to validation structure that do not exist yet
    for (String structureId : structureIds) {
      if (existingValidationStructures.stream().noneMatch(pubStruct -> structureId.equals(pubStruct.getStructure().getResId()))) {
        final Structure structure = this.structureService.findOne(structureId);
        PublicationStructure publicationStructure = new PublicationStructure();
        publicationStructure.setStructure(structure);
        publicationStructure.setPublication(publication);
        publicationStructure.setLinkType(PublicationStructure.LinkType.VALIDATION);
        publication.getPublicationStructures().add(publicationStructure);
      }
    }
    this.save(publication);
  }

  /**
   * Return true if the person is the creator of the publication
   *
   * @param personId
   * @param publicationId
   * @return
   */
  public boolean isPersonPublicationCreator(String personId, String publicationId) {
    Publication publication = this.findOne(publicationId);
    return publication.getCreator().getResId().equals(personId);
  }

  public boolean authenticatedUserBelongsToContributors(String publicationId) {
    Optional<String> cnIndividuOpt = this.personService.getAuthenticatedUserUnigeCnIndividu();
    if (cnIndividuOpt.isEmpty()) {
      return false;
    } else {
      return this.cnIndividuBelongsToContributors(publicationId, cnIndividuOpt.get());
    }
  }

  public boolean cnIndividuBelongsToContributors(String publicationId, String cnIndividu) {
    if (this.existsById(publicationId)) {

      Publication criteria = new Publication();
      criteria.setResId(publicationId);

      SolidifySpecification<Publication> specification = new PublicationContributorSpecification(criteria, cnIndividu);
      specification.setFilterOnRootResId(true);
      final Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
      return !this.findAll(specification, pageable).isEmpty();
    } else {
      throw new NoSuchElementException("No publication with id '" + publicationId + "'");
    }
  }

  /**
   * Return true if the person belongs to contributors of the publication
   *
   * @param personId
   * @param publicationId
   * @return
   */
  public boolean isPersonCreatorOrAuthenticatedUserBelongsToContributors(String personId, String publicationId) {
    if (this.existsById(publicationId)) {

      Publication criteria = new Publication();
      criteria.setResId(publicationId);

      Optional<String> cnIndividuOpt = this.personService.getAuthenticatedUserUnigeCnIndividu();

      SolidifySpecification<Publication> specification = new PublicationContributorOrCreatorSpecification(criteria, personId,
              cnIndividuOpt.orElse(null));
      specification.setFilterOnRootResId(true);
      final Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
      return !this.findAll(specification, pageable).isEmpty();
    } else {
      throw new NoSuchElementException("No publication with id '" + publicationId + "'");
    }
  }

  /**
   * @param publicationId
   * @return
   */
  public List<PublicationUserRole> listPublicationUserRoles(String publicationId) {
    Publication publication = this.findOne(publicationId);

    String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
    List<PublicationUserRole> publicationUsersRoles = new ArrayList<>();

    if (this.authenticatedUserHasPriviledgedRole() || this.personService.isAllowedToValidate(authenticatedPersonId, publication)) {
      publicationUsersRoles.add(PublicationUserRole.VALIDATOR);
    }

    if (this.isPersonPublicationCreator(authenticatedPersonId, publicationId)) {
      publicationUsersRoles.add(PublicationUserRole.CREATOR);
    }

    if (this.authenticatedUserBelongsToContributors(publicationId)) {
      publicationUsersRoles.add(PublicationUserRole.CONTRIBUTOR);
    }

    return publicationUsersRoles;
  }

  public boolean isCnIndividuInContributorsMetadata(String publicationId, String cnIndividu) {
    final Publication publication = this.findOne(publicationId);
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    return metadataExtractor.isCnIndividuInContributors(publication, cnIndividu);
  }

  public Publication unlinkContributor(String publicationId, String cnIndividu) {
    final Publication publication = this.findOne(publicationId);
    final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
    metadataExtractor.removeCnIndividuFromContributors(publication, cnIndividu);
    publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
    return this.save(publication);
  }

  public boolean updatePublicationListWithResearchGroup(String researchGroupId, String[] publicationIds) {
    ResearchGroup researchGroup = this.researchGroupService.findOne(researchGroupId);
    if (researchGroup == null) {
      return false;
    }
    for (String s : publicationIds) {
      Publication publication = this.findOne(s);
      if (publication == null) {
        return false;
      }
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
      metadataExtractor.addResearchGroup(researchGroup, publication);
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      this.save(publication);
    }
    return true;
  }

  /**
   * If XML metadata format is in a previous version, upgrade it
   *
   * @param publication
   */
  private boolean upgradeMetadataIfFormatIsPreviousVersion(Publication publication) {
    //First check metadata format version in XML
    AouMetadataVersion version = this.metadataService.detectVersionFromXmlMetadata(publication.getMetadata());
    if (version != AouMetadataVersion.getDefaultVersion()) {
      // metadata are in an obsolete metadata format --> upgrade to the current format
      String upgradedMetadata = this.metadataService.upgradeMetadataFormat(publication.getMetadata());
      publication.setMetadata(upgradedMetadata);
      publication.setMetadataVersion(AouMetadataVersion.getDefaultVersion());
      return true;
    }
    return false;
  }

  /**
   * Get the list of all Publications whose metadata version is not the current one and send a JMS message to upgrade them asynchronously.
   */
  public int startUpgradingAllPublicationsMetadataVersion() {
    List<Publication> publicationsToUpgrade = ((PublicationRepository) this.itemRepository).findPublicationByMetadataVersionIsNot(
            AouMetadataVersion.getDefaultVersion());
    log.info("{} publications will have their metadata version upgraded", publicationsToUpgrade.size());
    for (Publication publication : publicationsToUpgrade) {
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMetadataUpgradeMessage(publication.getResId()));
    }
    return publicationsToUpgrade.size();
  }

  public boolean hasDuplicateFiles(Publication publication) {
    boolean duplicates = false;
    if (publication.getDocumentFiles().size() > 1) {
      for (int i = 0; i < publication.getDocumentFiles().size(); i++) {
        if (!publication.getDocumentFiles().get(i).getStatus().equals(DocumentFile.DocumentFileStatus.IN_ERROR) &&
                !publication.getDocumentFiles().get(i).getStatus().equals(DocumentFile.DocumentFileStatus.IMPOSSIBLE_TO_DOWNLOAD)) {
          String checksumToVerify = publication.getDocumentFiles().get(i).getChecksum();
          for (int j = i + 1; j < publication.getDocumentFiles().size(); j++) {
            if (!StringTool.isNullOrEmpty(checksumToVerify) && checksumToVerify.equals(publication.getDocumentFiles().get(j).getChecksum())) {
              publication.getDocumentFiles().get(i).setStatus(DocumentFile.DocumentFileStatus.DUPLICATE);
              publication.getDocumentFiles().get(j).setStatus(DocumentFile.DocumentFileStatus.DUPLICATE);
              this.documentFileService.save(publication.getDocumentFiles().get(i));
              this.documentFileService.save(publication.getDocumentFiles().get(j));
              duplicates = true;
            }
          }
        }
      }
    }
    return duplicates;
  }

  public boolean updatePublicationListWithStructure(String structureId, String[] publicationIds) {
    Structure structure = this.structureService.findOne(structureId);

    for (String publicationId : publicationIds) {
      Publication publication = this.findOne(publicationId);

      // Add structure
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
      metadataExtractor.addStructure(structure, publication);

      // Then check if some structure must be removed from hierarchy
      final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
      List<Structure> structures = metadataExtractor.getStructures(depositDoc);
      List<String> structureIds = structures.stream().map(Structure::getResId).collect(Collectors.toList());
      List<String> duplicatedStructureIds = this.structureService.getDuplicatedStructureIdsInHierarchy(structureIds);

      for (String duplicatedParentStructureId : duplicatedStructureIds) {
        Structure structureToRemove = this.structureService.findOne(duplicatedParentStructureId);
        metadataExtractor.removeStructure(structureToRemove, publication);
      }

      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      this.save(publication);
    }
    return true;
  }

  public boolean updatePublicationListWithLanguage(String languageId, String[] publicationIds) {
    LabeledLanguage labeledLanguage = this.labeledLanguageService.findOne(languageId);
    if (labeledLanguage == null) {
      return false;
    }
    for (String s : publicationIds) {
      Publication publication = this.findOne(s);
      if (publication == null) {
        return false;
      }
      final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
      metadataExtractor.addLanguage(labeledLanguage, publication);
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      this.save(publication);
    }
    return true;
  }

  public boolean linkPublicationsWithContributorOtherName(String[] publicationIds, String cnIndividu, String lastname, String firstname) {
    Contributor contributor = this.contributorService.findByCnIndividu(cnIndividu);
    if (contributor == null) {
      throw new NoSuchElementException("No contributor with cnIndividu " + cnIndividu);
    }

    ContributorOtherName otherNameToLink = null;
    if (!contributor.getLastName().equals(lastname) || !Objects.equals(contributor.getFirstName(), firstname)) {
      // Name is different from the main contributor name --> a ContributorOtherName must be linked to Publication
      // Check if contributor has already an identical other name
      for (ContributorOtherName otherName : contributor.getOtherNames()) {
        if (otherName.getLastName().equals(lastname) && Objects.equals(otherName.getFirstName(), firstname)) {
          otherNameToLink = otherName;
          break;
        }
      }

      if (otherNameToLink == null) {
        // A new ContributorOtherName must be created
        ContributorOtherName newContributorOtherName = new ContributorOtherName();
        newContributorOtherName.setContributor(contributor);
        newContributorOtherName.setLastName(lastname);
        newContributorOtherName.setFirstName(firstname);
        contributor.getOtherNames().add(newContributorOtherName);
        this.contributorService.save(contributor);
        otherNameToLink = newContributorOtherName;
      }
    }

    for (String resId : publicationIds) {
      Publication publication = this.findOne(resId);

      // First discard eventual existing relation between publication and contributor
      List<ContributorOtherName> filteredContributorsOtherNames = publication.getContributorOtherNames().stream()
              .filter(on -> !on.getContributor().getCnIndividu().equals(cnIndividu)).toList();
      publication.getContributorOtherNames().clear();
      publication.getContributorOtherNames().addAll(filteredContributorsOtherNames);

      // Then add the new other name to link if necessary or clean contributor name in metadata
      if (otherNameToLink != null) {
        publication.getContributorOtherNames().add(otherNameToLink);
      } else {
        // A link to ContributorOtherName is not necessary, but it is necessary to manage the case of resetting the main contributor name
        final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
        metadataExtractor.resetUnigeContributorsDisplayName(publication);
        // Do not validate to not block update if metadata already contains errors
        publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      }
      this.save(publication);
    }
    return true;
  }

  private void generateThesisAgreement(Publication publication) {
    if (this.generateAgreement && publication.getDocumentFiles() != null && !publication.getDocumentFiles().isEmpty()) {
      List<DocumentFile> thesisFilesList = publication.getDocumentFiles().stream()
              .filter(documentFile -> documentFile.getDocumentFileType().getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_THESIS))
              .toList();

      List<DocumentFile> agreementFilesList = publication.getDocumentFiles().stream()
              .filter(documentFile -> documentFile.getDocumentFileType().getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE))
              .toList();

      if (!thesisFilesList.isEmpty() && agreementFilesList.isEmpty()) {

        // Check if there is already an author
        final MetadataExtractor metadataExtractor = this.metadataService.getMetadataExtractor(publication.getMetadataVersion());
        final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());

        if (metadataExtractor.getFirstAuthor(depositDoc).isPresent() && !StringTool.isNullOrEmpty(metadataExtractor.getDoctorEmail(depositDoc))
                && !StringTool.isNullOrEmpty(metadataExtractor.getDoctorAddress(depositDoc))) {
          log.debug("Generating Agreement document for Publication {} : {}", publication.getResId(), thesisFilesList.get(0).getFileName());
          // save the agreement PDF
          this.documentFileService
                  .save(this.pdfDocumentCreator.generateAgreementPdf(publication, thesisFilesList.get(0)));
        }
      }
    }
  }

  /******************************************************************************************************/

  public Publication createFromIdentifier(PublicationImportDTO importDTO, Person creator) {
    if (StringTool.isNullOrEmpty(importDTO.getDoi()) && StringTool.isNullOrEmpty(importDTO.getPmid())) {
      throw new SolidifyRuntimeException("An identifier is required");
    }
    if (!StringTool.isNullOrEmpty(importDTO.getDoi()) && !StringTool.isNullOrEmpty(importDTO.getPmid())) {
      throw new SolidifyRuntimeException("Only one identifier can be provided");
    }

    try {
      Publication publication = null;
      if (!StringTool.isNullOrEmpty(importDTO.getDoi())) {
        publication = this.createFromDOI(importDTO.getDoi(), creator);
      } else if (!StringTool.isNullOrEmpty(importDTO.getPmid())) {
        publication = this.createFromPMID(importDTO.getPmid(), creator);
      }
      if (publication == null) {
        throw new SolidifyRuntimeException("No publication imported");
      }
      this.addCommentForContributorsPartiallyImportedIfNeeded(publication);
      return publication;
    } catch (SolidifyValidationException ex) {

      throw new SolidifyRuntimeException("Publication can not be imported: " + ex.toString());
    }
  }

  public Publication createFromDOI(String doi, Person creator) {
    String xmlMetadata = this.metadataImportFlowService.getMetadataFromDoi(doi, true);
    return this.createFromXmlMetadataAndImportFiles(xmlMetadata, creator, Publication.ImportSource.DOI);
  }

  public Publication createFromPMID(String pmid, Person creator) {
    String xmlMetadata = this.metadataImportFlowService.getMetadataFromPmid(pmid, true);
    return this.createFromXmlMetadataAndImportFiles(xmlMetadata, creator, Publication.ImportSource.PMID);
  }

  private Publication createFromXmlMetadataAndImportFiles(String xmlMetadata, Person creator,
          Publication.ImportSource importSource) {
    Publication savedPublication = this.createPublicationFromXmlMetadata(xmlMetadata, creator, importSource);
    try {
      this.metadataImportFlowService.importFilesFromServices(savedPublication);
    } catch (IOException | NoSuchAlgorithmException | HttpStatusCodeException e) {
      log.warn("Unable to download publication's files", e);
    }
    return savedPublication;
  }

  public Publication createPublicationFromXmlMetadata(String xmlMetadata, Person creator, Publication.ImportSource importSource,
          String archiveId, PublicationStatus status) {
    Publication publication = this.getPublicationFromXmlMetadata(xmlMetadata, creator, importSource, archiveId, status);

    if (publication.getImportSource() != null
            && publication.getImportSource() != Publication.ImportSource.CLONE
            && publication.getImportSource() != Publication.ImportSource.FEDORA3) {
      publication.setNotifyFilesFromExternalSource(true);
      publication.setNotifyContributorsFromExternalSource(true);
    }

    return this.save(publication);
  }

  public Publication createPublicationFromXmlMetadata(String xmlMetadata, Person creator,
          Publication.ImportSource importSource) {
    return this.createPublicationFromXmlMetadata(xmlMetadata, creator, importSource, null, null);
  }

  public Publication getPublicationFromXmlMetadata(String xmlMetadata, Person creator,
          Publication.ImportSource importSource, String archiveId,
          PublicationStatus status) {
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setCreator(creator);
    publication.setImportSource(importSource);
    publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
    publication.setArchiveId(archiveId);
    publication.setStatus(status);
    return publication;
  }

  /**
   * Return true if :
   * - the person has created the publication
   * - the person belongs to publication's contributors
   * - the document file is not private now
   *
   * @param personId
   * @param documentFileId
   * @param publicationId
   * @return
   */
  public boolean isAllowedToDownload(String personId, String documentFileId, String publicationId) {
    if (this.authenticatedUserHasPriviledgedRole()
            || this.isPersonCreatorOrAuthenticatedUserBelongsToContributors(personId, publicationId)
            || this.personService.isAllowedToValidate(personId, this.findOne(publicationId))) {
      // download permission is not related to document file access level
      return true;
    } else {
      // download is allowed if the document is not private now

      DocumentFile documentFile = this.documentFileService.findOne(documentFileId);

      if (documentFile.getAccessLevel() == DocumentFile.AccessLevel.PRIVATE) {
        // final access private --> never allowed
        return false;
      } else {
        if (documentFile.getEmbargoAccessLevel() == null
                || documentFile.getEmbargoEndDate().isBefore(LocalDate.now())) {
          // no embargo or embargo is over --> allowed
          return true;
        } else {
          // embargo running --> allowed if embargo level is not private
          return documentFile.getEmbargoAccessLevel() != DocumentFile.AccessLevel.PRIVATE;
        }
      }
    }
  }

  /**
   * A SavePublicationMessage can be published to make the Publication saved again. This allows to update publication's metadata when entities
   * that are present in metadata are updated
   *
   * @param savePublicationMessage
   * @return
   */
  @EventListener
  @Async
  @Transactional
  public void saveAgain(SavePublicationMessage savePublicationMessage) {
    Publication publication = this.findOne(savePublicationMessage.getResId());
    this.save(publication);
  }

  /**
   * Method used to fix a bug that created Fedora objects without any datastream. By putting the publication in SUBMITTED status it starts
   * the storage process again, which generates all required datastreams and store them in Fedora.
   * <p>
   * Note: the method is @Async to not override the last editor's name in status histories
   *
   * @param publicationId
   */
  @Async
  @Transactional
  public void storeAgain(String publicationId) {
    Publication publication = this.findOne(publicationId);
    if (!StringTool.isNullOrEmpty(publication.getArchiveId()) && publication.getStatus().equals(COMPLETED)) {
      publication.setStatus(SUBMITTED);
      publication.setStatusMessage("Publication is about to be stored again");
      publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION);
      this.save(publication);
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMessage(publicationId));
    } else if (StringTool.isNullOrEmpty(publication.getArchiveId())) {
      throw new SolidifyRuntimeException("Publication cannot be stored again as it doesn't have any archiveId");
    } else {
      throw new SolidifyRuntimeException("Publication cannot be stored again as its status is not COMPLETED");
    }
  }

  public void changePublicationCreator(Person creatorToKeep, Person creatorToUpdate) {
    List<Publication> publicationList = ((PublicationRepository) this.itemRepository)
            .findByCreatorResId(creatorToUpdate.getResId());
    for (Publication publication : publicationList) {
      publication.setMetadataValidationType(Publication.MetadataValidationType.NONE);
      publication.setCreator(creatorToKeep);
      this.save(publication);
    }
  }

  /**
   * Method to check metadata and backupMetadata and see if there any differences between files
   *
   * @param publication
   */
  public boolean areThereChangesInDocumentFiles(Publication publication) {
    final MetadataExtractor metadataExtractor = this.metadataService
            .getMetadataExtractor(publication.getMetadataVersion());

    final Object depositDocBefore = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadataBackup());
    List<MetadataFile> filesBefore = metadataExtractor.getFiles(depositDocBefore);

    final Object depositDocAfter = metadataExtractor.createDepositDocObjectFromXml(publication.getMetadata());
    List<MetadataFile> filesAfter = metadataExtractor.getFiles(depositDocAfter);
    return !filesBefore.equals(filesAfter);
  }

  public List<MetadataFile> getMetadataFiles(Publication publication, String metadata) {
    final MetadataExtractor metadataExtractor = this.metadataService
            .getMetadataExtractor(publication.getMetadataVersion());
    final Object depositDoc = metadataExtractor.createDepositDocObjectFromXml(metadata);
    return metadataExtractor.getFiles(depositDoc);
  }

  public Map<String, Map<String, List<MetadataDifference>>> checkDifferencesInUpdate(Publication publication,
          boolean simplifiedVersion) {
    final MetadataComparator metadataComparator = this.metadataService.getMetadataComparator(publication);
    return metadataComparator.getPendingUpdates(publication, simplifiedVersion);
  }

  public List<StatusHistory> findStatusHistoryOrderByLastDate(String resId) {
    return this.historyService.findStatusHistoryOrderByLastDate(resId);
  }

  public void addCommentForContributorsPartiallyImportedIfNeeded(Publication publication) {
    if (publication.getContributors().size() >= this.maxContributorsLimit) {
      Person person = this.personService.getLinkedPerson(SecurityContextHolder.getContext().getAuthentication());
      Comment comment = new Comment();
      comment.setOnlyForValidators(true);
      comment.setPerson(person); // set current user as creator of the comment
      comment.setPublication(publication);
      comment.setText(this.messageService.get("publication.comment.contributorsPartiallyImported", new Object[] { this.maxContributorsLimit }));
      comment.setSystem(true);
      this.commentService.save(comment);
    }
  }

  /******************************************************************************************************/

  public long reindexAll(String query) {
    return this.reindexAll(query, null);
  }

  public long reindexAll(String query, Integer maxNumber) {
    if (StringTool.isNullOrEmpty(query)) {
      // reindex all publications existing in db
      int page = 0;
      int pageSize = 1000;
      Pageable sortedByCreationDate = PageRequest.of(page, pageSize, Sort.by(ActionName.CREATION));
      Page<String> resIdsPage = ((PublicationRepository) this.itemRepository).findIds(sortedByCreationDate);
      long total = resIdsPage.getTotalElements();
      if (maxNumber != null && total > maxNumber) {
        total = maxNumber;
      }
      int i = 1;
      while (resIdsPage.hasContent()) {
        for (String publicationId : resIdsPage.getContent()) {
          this.sendReindexingMessage(i++, total, publicationId);

          // If a maximum of publications is given, check it
          if (maxNumber != null && i - 1 == maxNumber) {
            return maxNumber;
          }
        }

        page++;
        sortedByCreationDate = PageRequest.of(page, pageSize, Sort.by(ActionName.CREATION));
        resIdsPage = ((PublicationRepository) this.itemRepository).findIds(sortedByCreationDate);
      }
      return total;
    } else {
      // reindex publications already existing in publication in progress index,
      // included in query results
      List<String> publicationIds = this.indexService.findPublicationIds(query);
      long total = publicationIds.size();
      if (maxNumber != null && total > maxNumber) {
        total = maxNumber;
      }
      int i = 1;
      for (String publicationId : publicationIds) {
        this.sendReindexingMessage(i++, total, publicationId);
        if (maxNumber != null && i - 1 == maxNumber) {
          return maxNumber;
        }
      }
      return publicationIds.size();
    }
  }

  private void sendReindexingMessage(int index, long total, String publicationId) {
    String logMessage = "Indexing publication " + index + " / " + total;
    SolidifyEventPublisher.getPublisher().publishEvent(new PublicationReindexingMessage(publicationId, logMessage));
  }
}
