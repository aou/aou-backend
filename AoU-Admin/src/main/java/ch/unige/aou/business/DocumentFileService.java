/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DocumentFileService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.multipart.MultipartFile;

import jakarta.transaction.Transactional;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.ChecksumTool;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.SavePublicationMessage;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFile.DocumentFileStatus;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.rest.DocumentFileStatisticsDTO;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.repository.DocumentFileRepository;
import ch.unige.aou.service.files.FileMetadataExtractor;
import ch.unige.aou.specification.DocumentFileSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class DocumentFileService extends AouResourceService<DocumentFile> {
  private static final Logger log = LoggerFactory.getLogger(DocumentFileService.class);
  private static final String EMBARGO_MISSING_INFO = "documentFile.embargo.error.missing_info";
  private static final String EMBARGO_BAD_ACCESS_LEVEL = "documentFile.embargo.error.bad_access_level";

  private final DocumentFileTypeService documentFileTypeService;
  private final PublicationService publicationService;
  private final LicenseService licenseService;
  private final FileMetadataExtractor fileMetadataExtractor;

  private final String tmpPublicationsFolder;

  public DocumentFileService(AouProperties aouProperties, DocumentFileTypeService documentFileTypeService, PublicationService publicationService,
          LicenseService licenseService, FileMetadataExtractor fileMetadataExtractor) {
    this.documentFileTypeService = documentFileTypeService;
    this.publicationService = publicationService;
    this.licenseService = licenseService;
    this.fileMetadataExtractor = fileMetadataExtractor;

    this.tmpPublicationsFolder = aouProperties.getTempLocation(aouProperties.getPublicationsFolder());
  }

  @Override
  protected void beforeValidate(DocumentFile documentFile) {
    this.setDefaultSortValue(documentFile);

    // forces Access level to private for Agreement files
    if (documentFile.getDocumentFileType() != null) {
      String documentFileTypeValue = documentFile.getDocumentFileType().getValue();
      if (StringTool.isNullOrEmpty(documentFileTypeValue)) {
        documentFileTypeValue = this.documentFileTypeService.findOne(documentFile.getDocumentFileType().getResId()).getValue();
      }

      if (AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE.equals(documentFileTypeValue)
              || AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE.equals(documentFileTypeValue)
              || AouConstants.DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE.equals(documentFileTypeValue)) {

        DocumentFile.AccessLevel accessLevel = DocumentFile.AccessLevel.PRIVATE;
        if (AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE.equals(documentFileTypeValue)) {
          accessLevel = DocumentFile.AccessLevel.PUBLIC;
        }

        documentFile.setEmbargoAccessLevel(null);
        documentFile.setEmbargoEndDate(null);
        documentFile.setLicense(null);
        documentFile.setAccessLevel(accessLevel);
        documentFile.setLabel(null);
      }
    }
    documentFile.setLabel(CleanTool.cleanString(documentFile.getLabel()));
  }

  @Override
  protected void validateItemSpecificRules(DocumentFile documentFile, BindingResult errors) {

    /**
     * Check eventual embargo values coherence
     */
    if (documentFile.getEmbargoAccessLevel() != null || documentFile.getEmbargoEndDate() != null) {

      if (documentFile.getEmbargoAccessLevel() == null) {
        errors.addError(
                new FieldError(documentFile.getClass().getSimpleName(), "embargoAccessLevel", this.messageService.get(EMBARGO_MISSING_INFO)));
      } else if (documentFile.getEmbargoEndDate() == null) {
        errors.addError(
                new FieldError(documentFile.getClass().getSimpleName(), "embargoEndDate", this.messageService.get(EMBARGO_MISSING_INFO)));
      } else {
        // check that the final access level is more open that the embargo access level
        if (documentFile.getAccessLevel() != null && documentFile.getAccessLevel().value() >= documentFile.getEmbargoAccessLevel().value()) {
          errors.addError(new FieldError(documentFile.getClass().getSimpleName(), "embargoAccessLevel", this.messageService
                  .get(EMBARGO_BAD_ACCESS_LEVEL,
                          new Object[] { documentFile.getEmbargoAccessLevel().name(), documentFile.getAccessLevel().name() })));
        }
      }
    }
  }

  @Transactional
  public DocumentFile uploadNewDocumentFile(String publicationId, String documentFileTypeId, DocumentFile.AccessLevel accessLevel,
          DocumentFile.AccessLevel embargoAccessLevel,
          LocalDate embargoEndDate,
          String licenseId,
          boolean notSureForLicense,
          String label,
          MultipartFile multipartFile) {

    Publication publication = this.publicationService.findOne(publicationId);
    DocumentFileType documentFileType = this.documentFileTypeService.findOne(documentFileTypeId);

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setDocumentFileType(documentFileType);
    documentFile.setAccessLevel(accessLevel);

    if (embargoAccessLevel != null) {
      documentFile.setEmbargoAccessLevel(embargoAccessLevel);
    }

    if (embargoEndDate != null) {
      documentFile.setEmbargoEndDate(embargoEndDate);
    }

    if (!StringTool.isNullOrEmpty(licenseId)) {
      License license = this.licenseService.findOne(licenseId);
      documentFile.setLicense(license);
    }

    documentFile.setNotSureForLicense(notSureForLicense);

    if (!StringTool.isNullOrEmpty(label)) {
      documentFile.setLabel(label);
    }

    // copy uploaded file to tmp folder and set sourceData
    try {
      Path tmpFolder = Paths.get(this.tmpPublicationsFolder).resolve(publicationId);
      if (FileTool.ensureFolderExists(tmpFolder)) {
        Path tmpFile = tmpFolder.resolve(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        multipartFile.transferTo(tmpFile);
        documentFile.setSourceData(tmpFile.toUri());
      } else {
        throw new SolidifyRuntimeException("Could not create tmp folder: " + tmpFolder);
      }
    } catch (IOException ioe) {
      throw new SolidifyRuntimeException("Could not save uploaded file: " + multipartFile.getOriginalFilename(), ioe);
    }

    return this.save(documentFile);
  }

  @Override
  public SolidifySpecification<DocumentFile> getSpecification(DocumentFile resource) {
    return new DocumentFileSpecification(resource);
  }

  public void checkFileValidity(DocumentFile documentFile) {
    try {
      if (MediaType.APPLICATION_PDF_VALUE.equals(documentFile.getMimetype())) {
        this.checkPdfValidity(documentFile);
      }
    } catch (IOException io) {
      documentFile.setStatus(DocumentFileStatus.TO_BE_CONFIRMED);
      documentFile.setStatusMessage(this.messageService.get("documentFile.status.to_be_confirmed.parsing_error"));
      log.error("An error occurred while checking validity of file '{}' for deposit {}", documentFile.getFinalData(),
              documentFile.getPublication().getResId(), io);
    }
  }

  private void checkPdfValidity(DocumentFile documentFile) throws IOException {

    /**
     * Check if the document file can be read
     */
    if (!this.fileMetadataExtractor.isValidFile(documentFile)) {
      documentFile.setStatus(DocumentFileStatus.IMPOSSIBLE_TO_DOWNLOAD);
      documentFile.setStatusMessage(this.messageService.get("documentFile.status.impossible_to_download.not_valid_pdf"));
      log.warn("The document file '{}' for the deposit '{}' seems to be invalid", documentFile.getFinalData(),
              documentFile.getPublication().getResId());
    } else if (this.fileMetadataExtractor.isPasswordProtected(documentFile)) {

      /**
       * Check if the document is password protected
       */
      documentFile.setStatus(DocumentFileStatus.TO_BE_CONFIRMED);
      documentFile.setStatusMessage(this.messageService.get("documentFile.status.to_be_confirmed.password_protected"));
      log.error("Unable to read password protected PDF file '{}' for deposit {}", documentFile.getFinalData(),
              documentFile.getPublication().getResId());
    } else {

      /**
       * Check that some text can be extracted from file
       */
      String text = this.getFulltext(documentFile);

      if (!StringUtils.hasLength(text)) {
        String documentTypeValue = documentFile.getDocumentFileType().getValue();
        if (documentTypeValue.equals(AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE)
                || documentTypeValue.equals(AouConstants.DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE)) {
          documentFile.setStatus(DocumentFileStatus.CONFIRMED);
          documentFile.setStatusMessage(this.messageService.get("documentFile.status.auto_confirmed.empty_text"));
          log.info("DocumentFile of type {} for deposit {} doesn't contain any text but is automatically confirmed", documentTypeValue,
                  documentFile.getPublication().getResId());
        } else {
          documentFile.setStatus(DocumentFile.DocumentFileStatus.TO_BE_CONFIRMED);
          documentFile.setStatusMessage(this.messageService.get("documentFile.status.to_be_confirmed.empty_text"));
          log.warn("The document file '{}' for the deposit '{}' does not seem to contain any text", documentFile.getFinalData(),
                  documentFile.getPublication().getResId());
        }
      }
    }
  }

  /**
   * Get fulltext by extracting it from document file or from cache if it already exists.
   * <p>
   * Using a cache prevents files to be downloaded from storage each time a publication is indexed.
   *
   * @param documentFile
   * @return
   */
  public String getFulltext(DocumentFile documentFile) {
    String cachedFulltext = this.readCachedFulltext(documentFile);
    if (cachedFulltext != null) {
      return cachedFulltext;
    }

    try {
      String fulltext = this.fileMetadataExtractor.getText(documentFile);

      if (fulltext != null) {
        fulltext = fulltext.trim();
      } else if (documentFile.getStatus() == DocumentFileStatus.READY || documentFile.getStatus() == DocumentFileStatus.CONFIRMED) {
        // no fulltext can be extracted from the file, but store it in cache to not try again on every indexation
        fulltext = "";
      }

      if (fulltext != null) {
        this.storeFulltextInCache(documentFile, fulltext);
      }
      return fulltext;

    } catch (Exception e) {
      log.error("Unable to read DocumentFile fulltext for {} ({})", documentFile.getResId(), e.getMessage());
    }

    return null;
  }

  public Map<DocumentFileStatus, Long> getMapStatusOccurrences(String publicationId) {
    List<DocumentFileStatisticsDTO> listStatistics = ((DocumentFileRepository) this.itemRepository).getStatusStatistics(publicationId);
    return DocumentFileStatisticsDTO.getMapFromList(listStatistics);
  }

  public boolean statusUpdateIsValid(DocumentFile documentFile, DocumentFileStatus newStatus) {
    if (documentFile.getStatus() == DocumentFileStatus.TO_BE_CONFIRMED) {
      return newStatus == DocumentFileStatus.CONFIRMED;
    }
    return false;
  }

  public boolean updateSortValues(String parentId, List<String> ids) {
    int index = 0;
    for (String documentFileId : ids) {
      DocumentFile documentFile = this.findOne(documentFileId);
      if (documentFile.getPublication().getResId().equals(parentId)) {
        int newSortValue = (index + 1) * AouConstants.ORDER_INCREMENT; // convert sortValues to 10, 20, 30, 40
        documentFile.setSortValue(newSortValue);
        this.save(documentFile);
      } else {
        return false; // the publication id associated with documentFile didn't match with the one provided
      }
      index++;
    }
    return true;
  }

  @Override
  public Optional<Integer> getMaxSortValue(Sortable item) {
    return ((DocumentFileRepository) this.itemRepository).findMaxSortValueByPublication(((DocumentFile) item).getPublication().getResId());
  }

  @Override
  public DocumentFile save(DocumentFile documentFile) {
    final DocumentFile result = super.save(documentFile);

    if (documentFile.getStatus() == DocumentFileStatus.READY) {
      // regenerate publication's metadata
      SolidifyEventPublisher.getPublisher().publishEvent(new SavePublicationMessage(documentFile.getPublication().getResId()));
    }

    return result;
  }

  @Override
  public void delete(String resId) {
    DocumentFile documentFile = this.findOne(resId);
    super.delete(resId);

    // final data may be null if the file could not be downloaded
    if (documentFile.getFinalData() != null && documentFile.getFinalData().getScheme().equalsIgnoreCase("file")) {
      try {
        Files.deleteIfExists(Path.of(documentFile.getFinalData()));
      } catch (IOException e) {
        throw new SolidifyFileDeleteException(e.getMessage());
      }
    }

    // delete cached fulltext
    Path fulltextCacheFile = this.getFulltextCacheFile(documentFile);
    if (FileTool.isFile(fulltextCacheFile)) {
      try {
        FileTool.deleteFile(fulltextCacheFile);
      } catch (IOException e) {
        throw new SolidifyFileDeleteException(e.getMessage());
      }
    }

    // regenerate publication's metadata
    SolidifyEventPublisher.getPublisher().publishEvent(new SavePublicationMessage(documentFile.getPublication().getResId()));
  }

  public void verifyIfThereAreStillDuplicates(String publicationId) {
    //verify if there are still duplicate files
    Publication publication = this.publicationService.findOne(publicationId);
    //change duplicated status to ready if there is no duplicate for that document file
    publication.getDocumentFiles().stream().filter(df -> df.getStatus().equals(DocumentFile.DocumentFileStatus.DUPLICATE))
            .map(df -> {
              if (!this.hasDuplicateFiles(df)) {
                df.setStatus(DocumentFile.DocumentFileStatus.READY);
                this.save(df);
              }
              return true;
            }).collect(Collectors.toList()); // calling collect() is mandatory to make the stream API works on lazy loaded JPA relation
  }

  public boolean hasDuplicateFiles(DocumentFile documentFile) {
    Publication publication = documentFile.getPublication();
    if (publication.getDocumentFiles().size() > 1) {
      String checksumToVerify = documentFile.getChecksum();
      for (int i = 0; i < publication.getDocumentFiles().size(); i++) {
        if (checksumToVerify.equals(publication.getDocumentFiles().get(i).getChecksum())
                && !publication.getDocumentFiles().get(i).equals(documentFile)) {
          return true;
        }
      }
    }
    return false;
  }

  public boolean hasAdminOrRootRoleToDeleteDocument(String id) {
    DocumentFile documentFile = this.findOne(id);
    if (documentFile.getPublication().getStatus() == Publication.PublicationStatus.IN_EDITION &&
            documentFile.getDocumentFileType().getValue().equals(AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE)) {
      // verify if the role has admin role at least to delete the file
      final Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
      Predicate<GrantedAuthority> isAdmin = e -> e.toString().equals(AuthApplicationRole.ADMIN.getResId());
      Predicate<GrantedAuthority> isRoot = e -> e.toString().equals(AuthApplicationRole.ROOT.getResId());
      Predicate<GrantedAuthority> isAdminOrRoot = isAdmin.or(isRoot);
      return authorities.stream().anyMatch(isAdminOrRoot);
    }
    return true;
  }

  private Path getFulltextCacheFolder(DocumentFile documentFile) {
    Path publicationPath = this.publicationService.getPublicationFilesFolderPath(documentFile.getPublication().getResId());
    return Paths.get(publicationPath.toString(), AouConstants.FULLTEXTS_CACHE_FOLDER);
  }

  private Path getFulltextCacheFile(DocumentFile documentFile) {
    Path cacheFolder = this.getFulltextCacheFolder(documentFile);
    return Paths.get(cacheFolder.toString(), documentFile.getResId());
  }

  private String readCachedFulltext(DocumentFile documentFile) {
    Path fulltextCacheFile = this.getFulltextCacheFile(documentFile);
    if (FileTool.isFile(fulltextCacheFile)) {
      try {
        return FileTool.toString(fulltextCacheFile);
      } catch (IOException e) {
        log.error("Unable to read cached content in file {}", fulltextCacheFile, e);
      }
    }
    return null;
  }

  public void storeFulltextInCache(DocumentFile documentFile, String fulltext) {
    FileTool.ensureFolderExists(this.getFulltextCacheFolder(documentFile));
    Path fulltextCacheFile = this.getFulltextCacheFile(documentFile);
    try {
      Files.write(fulltextCacheFile, fulltext.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      log.error("Unable to store fulltext in cache file {}", fulltextCacheFile, e);
    }
  }

  public List<DocumentFile> findDocumentFilesWithEmbargoEndDateEnding(LocalDate startDate, LocalDate endDate) {
    return ((DocumentFileRepository) this.itemRepository).getDocumentFilesWithEmbargoEndDateEnding(startDate, endDate);
  }

  public DocumentFile completeChecksumByDownloadingFile(DocumentFile documentFile) throws IOException, NoSuchAlgorithmException {
    Path tmpFilePath = this.downloadFileInTempFolder(documentFile);
    String checksum = ChecksumTool.computeChecksum(
            new BufferedInputStream(new FileInputStream(tmpFilePath.toString()), SolidifyConstants.BUFFER_SIZE), ChecksumAlgorithm.SHA256);
    documentFile.setChecksum(checksum);
    DocumentFile savedDocumentFile = this.save(documentFile);
    FileTool.deleteFile(tmpFilePath);
    log.info("checksum calculated by downloading file for document file {}", documentFile.getResId());
    return savedDocumentFile;
  }

  public Path downloadFileInTempFolder(DocumentFile documentFile) throws IOException {
    // path of file to store a local copy in order to calculate its checksum
    Path tmpFilePath = this.getLocalTempFilePath(documentFile);

    // check if file already exists on disk
    if (!Files.exists(tmpFilePath) || FileTool.getSize(tmpFilePath) != documentFile.getFileSize()) {
      // download file
      FileTool.ensureFolderExists(tmpFilePath.getParent());

      try (ReadableByteChannel readableByteChannel = Channels.newChannel(documentFile.getDownloadUri().toURL().openStream());
              FileOutputStream fileOutputStream = new FileOutputStream(tmpFilePath.toString())) {
        fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        log.info("document file downloaded to {}", tmpFilePath);
      }
    }
    return tmpFilePath;
  }

  public Path getLocalTempFilePath(DocumentFile documentFile) {
    Path tmpFolder = Paths.get(this.tmpPublicationsFolder).resolve(documentFile.getPublication().getResId());
    return Path.of(tmpFolder.toString(), documentFile.getFileName());
  }
}
