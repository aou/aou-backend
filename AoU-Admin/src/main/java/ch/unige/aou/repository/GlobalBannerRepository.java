/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - GlobalBannerRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.model.GlobalBanner;
import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AdminController;

@Repository
@SuppressWarnings("squid:S1214")
@ConditionalOnBean(AdminController.class)
public interface GlobalBannerRepository extends SolidifyRepository<GlobalBanner> {

  // *************
  // ** Queries **
  // *************

  GlobalBanner findByName(String name);

  @Query("SELECT gb "
          + "FROM GlobalBanner gb "
          + "WHERE gb.enabled = true "
          + "AND gb.startDate <= :dateTime "
          + "AND gb.endDate > :dateTime "
          + "ORDER BY gb.startDate ASC ")
  GlobalBanner findActive(OffsetDateTime dateTime);

  @Query("SELECT gb "
          + "FROM GlobalBanner gb "
          + "WHERE gb.enabled = true "
          + "AND ( "
          + "(:startDate >= gb.startDate AND :startDate < gb.endDate) "
          + "OR (:endDate > gb.startDate AND :endDate <= gb.endDate) "
          + "OR (:startDate <= gb.startDate AND :endDate >= gb.endDate) "
          + ") ")
  List<GlobalBanner> findActiveOverlaping(OffsetDateTime startDate, OffsetDateTime endDate);

}
