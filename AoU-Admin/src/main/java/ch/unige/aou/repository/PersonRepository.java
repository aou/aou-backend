/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Institution;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

@Repository
@ConditionalOnBean(AdminController.class)
public interface PersonRepository extends SolidifyRepository<Person> {

  // *************
  // ** Queries **
  // *************

  @Query("SELECT i"
          + " FROM Person p"
          + " JOIN p.institutions i"
          + " WHERE p.resId = :personId")
  Page<Institution> findInstitutionsByPersonId(@Param("personId") String personId, Pageable pageable);

  @Query("SELECT i"
          + " FROM Person p"
          + " JOIN p.institutions i"
          + " WHERE p.resId = :personId AND i.resId=:institutionId")
  Institution findInstitutionsByPersonId(@Param("personId") String personId, @Param("institutionId") String institutionId);

  @Query("SELECT r FROM Person p JOIN p.researchGroups r WHERE p.resId = :parentId AND r.resId=:id")
  ResearchGroup findResearchGroupByParentId(@Param("parentId") String parentId, @Param("id") String id);

  @Query("SELECT r FROM Person p JOIN p.researchGroups r WHERE p.resId = :parentId")
  Page<ResearchGroup> findResearchGroupByParentId(@Param("parentId") String parentId, Pageable pageable);

  @Query("SELECT s FROM Person p JOIN p.structures s WHERE p.resId = :parentId AND s.resId=:id")
  Structure findStructureByParentId(@Param("parentId") String parentId, @Param("id") String id);

  @Query("SELECT s FROM Person p JOIN p.structures s WHERE p.resId = :parentId")
  Page<Structure> findStructureByParentId(@Param("parentId") String parentId, Pageable pageable);

  Person findByFirstName(String firstName);

  Person findByOrcid(String orcid);

  List<Person> findByFirstNameStartingWith(String firstNameStart);

  @Query("SELECT DISTINCT p FROM Person p"
          + " INNER JOIN User u ON u.person.resId = p.resId"
          + " WHERE p.firstName LIKE %:search% OR p.lastName LIKE %:search% OR p.orcid LIKE %:search%")
  Page<Person> searchAllWithUser(String search, Pageable pageable);

  @Query("SELECT p FROM Person p"
          + " INNER JOIN User u ON u.externalUid = :userExternalUid")
  Person getPersonFromUser(String userExternalUid);

  List<Person> findByOrcidTokenIsNotNull();
}
