/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DocumentFileRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.rest.DocumentFileStatisticsDTO;

@Repository
@ConditionalOnBean(AdminController.class)
public interface DocumentFileRepository extends SolidifyRepository<DocumentFile> {
  @Query("SELECT new ch.unige.aou.model.rest.DocumentFileStatisticsDTO(df.status, COUNT(df.status)) "
          + "FROM DocumentFile df "
          + "WHERE df.publication.resId = :publicationId "
          + "GROUP BY df.status")
  List<DocumentFileStatisticsDTO> getStatusStatistics(@Param("publicationId") String publicationId);

  @Query(value = "SELECT max(sortValue) FROM DocumentFile WHERE publication.resId = :publicationId")
  Optional<Integer> findMaxSortValueByPublication(String publicationId);

  @Query("SELECT df FROM DocumentFile df WHERE df.embargoEndDate BETWEEN :startDate And :endDate")
  List<DocumentFile> getDocumentFilesWithEmbargoEndDateEnding(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);
}
