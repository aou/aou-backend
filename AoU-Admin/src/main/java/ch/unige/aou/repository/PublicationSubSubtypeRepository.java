/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationSubSubtypeRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;

@Repository
@ConditionalOnBean(AdminController.class)
public interface PublicationSubSubtypeRepository extends SolidifyRepository<PublicationSubSubtype> {
  PublicationSubSubtype findByName(String name);

  PublicationSubSubtype findByNameAndPublicationSubtype(String name, PublicationSubtype subtype);

  @Query(value = "SELECT max(psst.sortValue) FROM PublicationSubSubtype psst WHERE psst.publicationSubtype.resId = :publicationSubtypeId")
  Optional<Integer> findMaxSortValueByPublicationSubtype(String publicationSubtypeId);

  @Query(value = "SELECT max(psst.bibliographySortValue) FROM PublicationSubSubtype psst")
  Optional<Integer> findMaxBibliographySortValue();

  @Query(value = "SELECT psst FROM PublicationSubSubtype psst WHERE psst.publicationSubtype.resId = :publicationSubtypeId")
  List<PublicationSubSubtype> findByPublicationSubtypes(String publicationSubtypeId);
}
