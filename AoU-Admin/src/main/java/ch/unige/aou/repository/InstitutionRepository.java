/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - InstitutionRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Institution;
import ch.unige.aou.model.settings.Person;

@Repository
@SuppressWarnings("squid:S1214")
@ConditionalOnBean(AdminController.class)
public interface InstitutionRepository extends SolidifyRepository<Institution> {
  // *************
  // ** Queries **
  // *************

  @Query("SELECT p FROM Institution i JOIN i.people p WHERE i.resId = :parentId")
  public Page<Person> findPeopleByParentId(@Param("parentId") String parentId, Pageable pageable);

  @Query("SELECT p FROM Institution i JOIN i.people p WHERE i.resId = :parentId AND p.resId=:id")
  public Person findPeopleByParentId(@Param("parentId") String parentId, @Param("id") String id);

  @Query("SELECT i FROM Institution i WHERE :emailSuffix IN (i.emailSuffixes)")
  public Institution findByEmailSuffix(@Param("emailSuffix") String emailSuffix);

  Institution findByName(String name);
}
