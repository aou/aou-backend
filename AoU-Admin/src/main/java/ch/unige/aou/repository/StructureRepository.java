/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - StructureRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;

@Repository
@ConditionalOnBean(AdminController.class)
public interface StructureRepository extends SolidifyRepository<Structure> {
  Structure findByCnStructC(String cnStructC);

  Structure findByCodeStruct(String cStruct);

  List<Structure> findByNameAndParentStructureResId(String name, String parentStructureId);

  @Query("SELECT s.parentStructure.resId FROM Structure s WHERE s.resId = :resId")
  String getParentStructureResId(String resId);

  @Query("SELECT u FROM Structure s JOIN s.people u WHERE s.resId = :parentId AND u.resId=:id")
  Person findPersonByParentId(@Param("parentId") String parentId, @Param("id") String id);

  @Query("SELECT u FROM Structure s JOIN s.people u WHERE s.resId = :parentId")
  Page<Person> findPersonByParentId(@Param("parentId") String parentId, Pageable pageable);

  @Query("SELECT s FROM User u JOIN u.person p JOIN p.structures s WHERE u.externalUid like :externalUid")
  Page<Structure> findByUserExternalUid(@Param("externalUid") String externalUid, Pageable pageable);

  @Query(value = "SELECT max(st.sortValue) FROM Structure st WHERE st.parentStructure.resId = :parentStructureId")
  Optional<Integer> findMaxSortValueByParentStructure(String parentStructureId);

  @Query(value = "SELECT max(st.sortValue) FROM Structure st WHERE st.parentStructure.resId IS NULL")
  Optional<Integer> getMaxSortValue();
}
