/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonNotificationController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.time.OffsetDateTime;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.CompositionController;
import ch.unige.solidify.rest.RestCollection;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.security.CurrentUserPermissions;
import ch.unige.aou.specification.NotificationSpecification;

@RestController
@CurrentUserPermissions
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PEOPLE + SolidifyConstants.URL_PARENT_ID + ResourceName.NOTIFICATIONS)
public class PersonNotificationController extends CompositionController<Person, Notification> {

  @Override
  public HttpEntity<RestCollection<Notification>> list(@PathVariable String parentid, @ModelAttribute Notification filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @GetMapping(params = "onlyWithNullReadTime")
  public HttpEntity<RestCollection<Notification>> list(@PathVariable String parentid, @ModelAttribute Notification filterItem,
          Pageable pageable, @RequestParam boolean onlyWithNullReadTime) {

    if (onlyWithNullReadTime) {
      final Person person = this.resourceService.findOne(parentid);
      this.setParentResourceProperty(filterItem, person);
      NotificationSpecification specification = (NotificationSpecification) this.subResourceService.getSpecification(filterItem);
      specification.setOnlyWithNullReadTime(true);
      return this.getSubResourcesCollectionAsResponseEntity(parentid, specification, pageable);
    } else {
      return super.list(parentid, filterItem, pageable);
    }
  }

  @Override
  public HttpEntity<Notification> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @PostMapping(SolidifyConstants.URL_ID + "/" + AouActionName.SET_READ)
  public HttpEntity<Notification> markNotificationAsRead(@PathVariable String parentid, @PathVariable String id) {
    Notification notification = this.checkSubresourceExistsAndIsLinkedToParent(id, parentid);
    notification.setReadTime(OffsetDateTime.now());
    this.subResourceService.save(notification);
    return new ResponseEntity<>(notification, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID + "/" + AouActionName.SET_UNREAD)
  public HttpEntity<Notification> markNotificationAsUnread(@PathVariable String parentid, @PathVariable String id) {
    Notification notification = this.checkSubresourceExistsAndIsLinkedToParent(id, parentid);
    notification.setReadTime(null);
    this.subResourceService.save(notification);
    return new ResponseEntity<>(notification, HttpStatus.OK);
  }

  @Override
  protected Person getParentResourceProperty(Notification notification) {
    return notification.getRecipient();
  }

  @Override
  protected void setParentResourceProperty(Notification notification, Person recipient) {
    notification.setRecipient(recipient);
  }
}
