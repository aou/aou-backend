/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ContributorsController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.contact.PublicationContactMessage;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.search.ContributorSearchType;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.contact.ContactService;

@RootPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_CONTRIBUTORS)
public class ContributorsController extends ResourceController<Contributor> {

  private final ContactService contactService;

  public ContributorsController(ContactService contactService) {
    this.contactService = contactService;
  }

  @Override
  @AdminPermissions // to allow editing its name
  public HttpEntity<Contributor> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  @AdminPermissions
  public HttpEntity<Contributor> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<Contributor>> list(@ModelAttribute Contributor search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @GetMapping("/" + ActionName.SEARCH)
  @ResponseBody
  @EveryonePermissions
  public HttpEntity<RestCollection<Contributor>> advancedSearch(@ModelAttribute Contributor contributor, @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchType, Pageable pageable) {
    return super.advancedSearch(contributor, search, matchType, pageable);
  }

  @GetMapping(value = "/" + ActionName.SEARCH, params = "term")
  @ResponseBody
  @EveryonePermissions
  public HttpEntity<RestCollection<Contributor>> searchContributor(@RequestParam("term") String term,
          @RequestParam(value = "type", required = false, defaultValue = "unige") String type, Pageable pageable) {
    ContributorSearchType searchType = ContributorSearchType.valueOf(type.toUpperCase());
    Page<Contributor> contributorPage = ((ContributorService) this.itemService).searchContributors(term, searchType, pageable);
    this.setResourceLinks(contributorPage);
    final RestCollection<Contributor> collection = this.setCollectionLinks(contributorPage, contributorPage.getPageable());
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @GetMapping("/" + AouActionName.GET_BY_NAME)
  @EveryonePermissions
  public HttpEntity<Contributor> getByName(@RequestParam("lastName") String lastName, @RequestParam("firstName") String firstName) {
    Contributor contributor = ((ContributorService) this.itemService).findByFirstNameAndLastName(firstName, lastName);
    if (contributor == null) {
      throw new NoSuchElementException("No non unige contributor with name " + lastName + ", " + firstName);
    }
    this.addLinks(contributor);
    return new ResponseEntity<>(contributor, HttpStatus.OK);
  }

  @GetMapping("/" + AouActionName.GET_BY_CN_INDIVIDU)
  @EveryonePermissions
  public HttpEntity<Contributor> getByCnIndividu(@RequestParam("cnIndividu") String cnIndividu) {
    Contributor contributor = ((ContributorService) this.itemService).findByCnIndividu(cnIndividu);
    if (contributor == null) {
      throw new NoSuchElementException("No contributor with cnIndividu " + cnIndividu);
    }
    this.addLinks(contributor);
    return new ResponseEntity<>(contributor, HttpStatus.OK);
  }

  @GetMapping("/" + AouActionName.GET_BY_ORCID)
  @EveryonePermissions
  public HttpEntity<Contributor> getByOrcid(@RequestParam("orcid") String orcid) {
    Contributor contributor = ((ContributorService) this.itemService).findByOrcid(orcid);
    if (contributor == null) {
      throw new NoSuchElementException("No contributor with ORCID " + orcid);
    }
    this.addLinks(contributor);
    return new ResponseEntity<>(contributor, HttpStatus.OK);
  }

  @PostMapping("/" + AouActionName.CONTACT_CONTRIBUTOR)
  @EveryonePermissions
  public HttpEntity<Result> contactContributor(@RequestBody PublicationContactMessage contactMessage) {
    this.contactService.sendContactMessage(contactMessage);
    final Result result = new Result();
    result.setStatus(Result.ActionStatus.EXECUTED);
    result.setMesssage("Contact message sent to contributor");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

}
