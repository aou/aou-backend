/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ExternalDataController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;

import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.ExistingPublicationInfo;
import ch.unige.aou.model.rest.JournalTitleDTO;
import ch.unige.aou.model.rest.OpenAireProjectDTO;
import ch.unige.aou.model.rest.SearchDTO;
import ch.unige.aou.model.rest.UnigePersonDTO;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.SolrService;
import ch.unige.aou.service.metadata.imports.MetadataImportWorkFlowService;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.rest.OpenAireService;
import ch.unige.aou.service.rest.RomeoService;
import ch.unige.aou.service.rest.UnigePersonSearch;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_EXTERNAL_DATA)
public class ExternalDataController {

  private final UnigePersonSearch unigePersonSearch;
  private final JournalTitleRemoteService journalTitleService;
  private final RomeoService romeoService;
  private final OpenAireService openAireService;
  private final MetadataImportWorkFlowService metadataImportFlowService;
  private final DuplicateService duplicateService;
  private final MessageService messageService;
  private final SolrService solrService;
  private final PersonService personService;

  public ExternalDataController(UnigePersonSearch unigePersonSearch, JournalTitleRemoteService journalTitleService,
          RomeoService romeoService, OpenAireService openAireService, MetadataImportWorkFlowService metadataImportFlowService,
          DuplicateService duplicateService, MessageService messageService, SolrService solrService, PersonService personService) {
    this.messageService = messageService;
    this.unigePersonSearch = unigePersonSearch;
    this.journalTitleService = journalTitleService;
    this.romeoService = romeoService;
    this.openAireService = openAireService;
    this.metadataImportFlowService = metadataImportFlowService;
    this.duplicateService = duplicateService;
    this.solrService = solrService;
    this.personService = personService;
  }

  @GetMapping(AouActionName.SEARCH_UNIGE_PERSON)
  public HttpEntity<RestCollection<UnigePersonDTO>> searchUnigePerson(
          @RequestParam(value = "firstname", required = false) String firstname,
          @RequestParam(value = "lastname", required = false) String lastname,
          Pageable pageable) {
    RestCollection<UnigePersonDTO> people = this.unigePersonSearch.search(firstname, lastname, pageable);
    return new ResponseEntity<>(people, HttpStatus.OK);
  }

  @GetMapping(AouActionName.GET_METADATA_FROM_DOI)
  public HttpEntity<String> getMetadataFromDoi(@RequestParam String doi,
          @RequestParam(value = "format", required = false, defaultValue = "json") String format) {
    if (!ValidationTool.isValidDOI(doi) && !ValidationTool.isValidShortDOI(doi)) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              this.messageService.get("deposit.error.identifiers.doi.invalid", new Object[] { doi }));
    }
    String result;
    final HttpHeaders headers = new HttpHeaders();
    if ("xml".equals(format)) {
      result = this.metadataImportFlowService.getMetadataFromDoi(doi, true);
      headers.setContentType(MediaType.TEXT_XML);
    } else {
      result = this.metadataImportFlowService.getMetadataFromDoi(doi, false);
      headers.setContentType(MediaType.APPLICATION_JSON);
    }
    return new ResponseEntity<>(result, headers, HttpStatus.OK);
  }

  @GetMapping(AouActionName.GET_METADATA_FROM_PMID)
  public HttpEntity<String> getMetadataFromPmid(@RequestParam String pmid,
          @RequestParam(value = "format", required = false, defaultValue = "json") String format) {
    if (!ValidationTool.isValidPMID(pmid)) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              this.messageService.get("deposit.error.identifiers.pmid.invalid", new Object[] { pmid }));
    }

    String result;
    final HttpHeaders headers = new HttpHeaders();
    if ("xml".equals(format)) {
      result = this.metadataImportFlowService.getMetadataFromPmid(pmid, true);
      headers.setContentType(MediaType.TEXT_XML);
    } else {
      result = this.metadataImportFlowService.getMetadataFromPmid(pmid, false);
      headers.setContentType(MediaType.APPLICATION_JSON);
    }
    return new ResponseEntity<>(result, headers, HttpStatus.OK);
  }

  @GetMapping(AouActionName.GET_METADATA_FROM_ARXIV)
  public HttpEntity<String> getMetadataFromArxiv(@RequestParam String arxivId,
          @RequestParam(value = "format", required = false, defaultValue = "json") String format) {
    String result;
    final HttpHeaders headers = new HttpHeaders();
    if ("xml".equals(format)) {
      result = this.metadataImportFlowService.getMetadataFromArxiv(arxivId, true);
      headers.setContentType(MediaType.TEXT_XML);
    } else {
      result = this.metadataImportFlowService.getMetadataFromArxiv(arxivId, false);
      headers.setContentType(MediaType.APPLICATION_JSON);
    }

    return new ResponseEntity<>(result, headers, HttpStatus.OK);
  }

  @GetMapping(AouActionName.GET_METADATA_FROM_ISBN)
  public HttpEntity<String> getMetadataFromIsbn(@RequestParam String isbn,
          @RequestParam(value = "format", required = false, defaultValue = "json") String format) {
    String result;
    final HttpHeaders headers = new HttpHeaders();
    if ("xml".equals(format)) {
      result = this.metadataImportFlowService.getMetadataFromIsbn(isbn, true);
      headers.setContentType(MediaType.TEXT_XML);
    } else {
      result = this.metadataImportFlowService.getMetadataFromIsbn(isbn, false);
      headers.setContentType(MediaType.APPLICATION_JSON);
    }

    return new ResponseEntity<>(result, headers, HttpStatus.OK);
  }

  @GetMapping(AouActionName.GET_JOURNAL_TITLES)
  public HttpEntity<RestCollection<JournalTitleDTO>> getJournalTitlesByISSN(@RequestParam(required = false) String issn,
          @RequestParam(required = false) String title,
          @RequestParam(required = false, defaultValue = "false") boolean extendedSearch,
          Pageable page) {
    if (!StringTool.isNullOrEmpty(title)) {
      SearchDTO search = new SearchDTO();
      search.setPage(page.getPageNumber());
      search.setSize(page.getPageSize());

      String[] searchArray;

      // cr means electronic(online) format and ta means print version.
      if (extendedSearch) {
        searchArray = new String[] { "alltitle=" + title, "MUST=medium=cr,ta" };
      } else {
        searchArray = new String[] { "alltitle=" + title, "MUST=medium=ta" };
      }
      search.setSearch(searchArray);
      return new ResponseEntity<>(this.journalTitleService.getJournalTitlesByTitle(search), HttpStatus.OK);
    } else if (!StringTool.isNullOrEmpty(issn)) {
      return new ResponseEntity<>(this.journalTitleService.getJournalTitlesByISSN(issn), HttpStatus.OK);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "one of the query parameters 'issn' or 'title' is mandatory");
    }
  }

  @GetMapping(AouActionName.GET_OPEN_AIRE_PROJECTS)
  public HttpEntity<RestCollection<OpenAireProjectDTO>> getOpenAireProjects(@RequestParam String type, @RequestParam String value,
          Pageable pageable) {
    if (type.equals(AouConstants.OPEN_AIRE_NAME_PARAMETER) || type.equals(AouConstants.OPEN_AIRE_GRANT_ID_PARAMETER)
            || type.equals(AouConstants.OPEN_AIRE_ACRONYM_PARAMETER)) {
      return new ResponseEntity<>(this.openAireService.searchProjectsByTypeAndValue(type, value, pageable), HttpStatus.OK);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "'type' parameter must have the value 'acronym', 'grandID' or 'name'");
    }
  }

  @GetMapping(AouActionName.GET_ROMEO_INFOS)
  public HttpEntity<String> getRomeoByIssn(@RequestParam String issn) {
    if (!StringTool.isNullOrEmpty(issn)) {
      String romeoInfos = this.romeoService.getInfosByIssn(issn);
      final HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON);
      return new ResponseEntity<>(romeoInfos, headers, HttpStatus.OK);
    }

    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
  }

  @GetMapping(AouActionName.CHECK_IDENTIFIER_EXISTS)
  public HttpEntity<String> checkIdentifierAlreadyExists(@RequestParam(required = false) String doi, @RequestParam(required = false) String pmid,
          @RequestParam(required = false) String arxivId, @RequestParam(required = false) String isbn) {
    ExistingPublicationInfo existingPublicationInfo = null;
    if (!StringTool.isNullOrEmpty(doi)) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByDoi(doi);
    } else if (!StringTool.isNullOrEmpty(pmid)) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByPmid(pmid);
    } else if (!StringTool.isNullOrEmpty(arxivId)) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByArxivId(arxivId);
    } else if (!StringTool.isNullOrEmpty(isbn)) {
      existingPublicationInfo = this.duplicateService.getExistingPublicationInfoByIsbn(isbn);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "An identifier to check is required");
    }

    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.enable(SerializationFeature.INDENT_OUTPUT);
      ObjectNode resultNode = mapper.createObjectNode();
      resultNode.put("exists", (existingPublicationInfo != null));

      String result = mapper.writeValueAsString(resultNode);
      final HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_JSON);
      return new ResponseEntity<>(result, headers, HttpStatus.OK);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException(e.getMessage());
    }
  }

  @RootPermissions
  @PostMapping(AouActionName.IMPORT_PUBLICATIONS_FROM_FEDORA)
  public HttpStatus importPublicationsFromFedora(@RequestBody(required = false) Map<String, String> postContent) {
    String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());

    String solrConditions = "*:*";
    if (postContent != null && postContent.containsKey("solrConditions")) {
      solrConditions = postContent.get("solrConditions");
    }

    boolean result = this.solrService.startPublicationsImportFromFedora(authenticatedPersonId, solrConditions);
    if (result) {
      return HttpStatus.OK;
    } else {
      return HttpStatus.NOT_FOUND;
    }
  }

}
