/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ContributorRoleController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.business.ContributorRoleService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.ContributorRole;
import ch.unige.aou.model.publication.PublicationSubtypeContributorRoleDTO;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_CONTRIBUTOR_ROLE)
public class ContributorRoleController extends ResourceController<ContributorRole> {

  @Override
  public HttpEntity<RestCollection<ContributorRole>> list(ContributorRole search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @EveryonePermissions
  @GetMapping(AouActionName.LIST_CONTRIBUTOR_ROLES)
  public HttpEntity<Map<String, List<PublicationSubtypeContributorRoleDTO>>> listContributorRoles() {
    return new ResponseEntity<>(((ContributorRoleService)this.itemService).listContributorRoleByPublicationSubType(), HttpStatus.OK);
  }
}
