/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DocumentFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceReadOnlyController;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.security.TrustedUserPermissions;

import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@TrustedUserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_DOCUMENT_FILES)
public class DocumentFileController extends ResourceReadOnlyController<DocumentFile> {

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.FULLTEXT)
  @ResponseBody
  public String fulltext(@PathVariable String id) {
    DocumentFile documentFile = this.itemService.findOne(id);
    if (documentFile.getStatus() == DocumentFile.DocumentFileStatus.READY
            || documentFile.getStatus() == DocumentFile.DocumentFileStatus.CONFIRMED
            || documentFile.getStatus() == DocumentFile.DocumentFileStatus.TO_BE_CONFIRMED) {
      return ((DocumentFileService) this.itemService).getFulltext(documentFile);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              "Fulltext cannot be extracted from a DocumentFile with status '" + documentFile.getStatus() + "'");
    }
  }

  @Override
  public HttpEntity<DocumentFile> get(@PathVariable String id) {
    return super.get(id);
  }

}
