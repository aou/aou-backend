/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationCommentController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.CompositionController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.business.PersonService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PUBLICATIONS + SolidifyConstants.URL_PARENT_ID + ResourceName.COMMENTS)
public class PublicationCommentController extends CompositionController<Publication, Comment> {

  private PersonService personService;

  public PublicationCommentController(PersonService personService) {
    this.personService = personService;
  }

  @GetMapping
  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'LIST_DEPOSIT_COMMENTS')")
  public HttpEntity<RestCollection<Comment>> list(@PathVariable String parentid, @ModelAttribute Comment comment, Pageable pageable) {
    comment.setOnlyForValidators(false);
    return super.list(parentid, comment, pageable);
  }

  @GetMapping(AouActionName.LIST_VALIDATOR_COMMENTS)
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'LIST_VALIDATOR_COMMENTS')")
  public HttpEntity<RestCollection<Comment>> listValidatorComments(@PathVariable String parentid, @ModelAttribute Comment comment,
          Pageable pageable) {
    comment.setOnlyForValidators(true);
    return super.list(parentid, comment, pageable);
  }

  @GetMapping(SolidifyConstants.URL_ID)
  @Override
  @PreAuthorize("@commentPermissionService.isAllowed(#id, 'GET_DEPOSIT_COMMENT')")
  public HttpEntity<Comment> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @PostMapping
  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'ADD_DEPOSIT_COMMENT')")
  public HttpEntity<Comment> create(@PathVariable final String parentid, @RequestBody final Comment comment) {
    String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
    Person person = this.personService.findOne(authenticatedPersonId);
    //set Person
    comment.setPerson(person);
    return super.create(parentid, comment);
  }

  @PatchMapping(SolidifyConstants.URL_ID)
  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'UPDATE_DEPOSIT_COMMENT')")
  public HttpEntity<Comment> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> updateMap) {
    String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
    Comment comment = this.subResourceService.findOne(id);
    Person person = comment.getPerson();
    if (!comment.isSystem() && authenticatedPersonId.equals(person.getResId())) {
      return super.update(parentid, id, updateMap);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  @DeleteMapping(SolidifyConstants.URL_ID)
  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'DELETE_DEPOSIT_COMMENT')")
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
    Comment comment = this.subResourceService.findOne(id);
    Person person = comment.getPerson();
    if (!comment.isSystem() && (authenticatedPersonId.equals(person.getResId()) || this.personService.authenticatedUserHasPriviledgedRole())) {
      return super.delete(parentid, id);
    } else {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
  }

  @Override
  protected Publication getParentResourceProperty(Comment subResource) {
    return subResource.getPublication();
  }

  @Override
  protected void setParentResourceProperty(Comment subResource, Publication parentResource) {
    subResource.setPublication(parentResource);
  }

}
