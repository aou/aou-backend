/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - StructureController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.io.IOException;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_STRUCTURES)
public class StructureController extends ResourceController<Structure> {

  @GetMapping(AouActionName.BY_EXTERNAL_UID)
  public HttpEntity<RestCollection<Structure>> listStructures(@RequestParam(AouConstants.EXTERNAL_UID) String externalUid, Pageable pageable) {
    Page<Structure> listItem = ((StructureService) this.itemService).findByUserExternalUid(externalUid, pageable);
    final RestCollection<Structure> collection = new RestCollection(listItem.toList());
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  @EveryonePermissions
  public HttpEntity<RestCollection<Structure>> list(Structure search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<Structure> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @AdminPermissions
  public HttpEntity<Structure> create(@RequestBody Structure structure) {
    return super.create(structure);
  }

  @Override
  @AdminPermissions
  public HttpEntity<Structure> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  @AdminPermissions
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @AdminPermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @Override
  public HttpEntity<RestCollection<Structure>> advancedSearch(@ModelAttribute Structure structure,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchType, Pageable pageable) {
    return super.advancedSearch(structure, search, matchType, pageable);
  }

  @RootPermissions
  @PostMapping("/" + AouActionName.INITIALIZE)
  public HttpEntity<Result> initDefaultValues() {
    final Result res = new Result();
    try {
      ((StructureService) this.itemService).createDefaultStructuresFromJsonFile();
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("structures.initialization.success"));
    } catch (IOException e) {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      res.setMesssage(this.messageService.get("structures.initialization.error", new Object[] { e.getMessage() }));
    }
    res.add(linkTo(this.getClass()).slash(AouActionName.INITIALIZE).withSelfRel());
    res.add(linkTo(this.getClass()).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }
}
