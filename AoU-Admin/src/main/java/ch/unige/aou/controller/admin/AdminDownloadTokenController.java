/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - AdminDownloadTokenController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.rest.ActionName;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.controller.AouDownloadTokenController;
import ch.unige.aou.model.security.DownloadTokenType;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
public class AdminDownloadTokenController extends AouDownloadTokenController {

  private final String adminModulePath;

  public AdminDownloadTokenController(SolidifyProperties solidifyConfig, DownloadTokenRepository downloadTokenRepository,
          AouProperties aouProperties)
          throws URISyntaxException {
    super(solidifyConfig, downloadTokenRepository);
    this.adminModulePath = new URI(aouProperties.getModule().getAdmin().getPublicUrl()).getPath();
  }

  @GetMapping("/{archiveId}")
  public ResponseEntity<DownloadToken> getToken(@PathVariable String archiveId) {
    final String cookiePath = this.adminModulePath
            + "/" + ResourceName.PUBLICATIONS
            + "/" + archiveId
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(archiveId, DownloadTokenType.ARCHIVE, cookiePath);
  }

  @GetMapping(UrlPath.ADMIN_PUBLICATIONS + "/{publicationId}" + "/" + ResourceName.DOCUMENT_FILES + "/{documentId}" + "/"
          + ActionName.DOWNLOAD_TOKEN)
  @PreAuthorize("@publicationPermissionService.isAllowed(#publicationId, #documentId, 'DOWNLOAD_FILE')")
  public ResponseEntity<DownloadToken> getToken(@PathVariable String publicationId, @PathVariable String documentId) {
    final String cookiePath = this.adminModulePath
            + "/" + ResourceName.PUBLICATIONS
            + "/" + publicationId
            + "/" + ResourceName.DOCUMENT_FILES
            + "/" + documentId
            + "/" + ActionName.DOWNLOAD;
    return this.getToken(documentId, DownloadTokenType.DOCUMENT, cookiePath);

  }

}
