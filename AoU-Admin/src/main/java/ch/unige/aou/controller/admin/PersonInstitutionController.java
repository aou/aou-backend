/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonInstitutionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Institution;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PEOPLE + SolidifyConstants.URL_PARENT_ID + ResourceName.INSTITUTIONS)
public class PersonInstitutionController extends AssociationController<Person, Institution> {

  @Override
  public HttpEntity<List<Institution>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  public HttpEntity<RestCollection<Institution>> list(@PathVariable String parentid, @ModelAttribute Institution filterItem, Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }

  @Override
  public HttpEntity<Institution> update(@PathVariable String parentid, @PathVariable String id, @RequestBody Institution institution) {
    return super.update(parentid, id, institution);
  }

  @Override
  protected String getParentFieldName() {
    return "people";
  }

  @Override
  public Institution getEmptyChildResourceObject() {
    return new Institution();
  }

  @Override
  protected boolean addChildOnParent(Person person, Institution institution) {
    return person.addInstitution(institution);
  }

  @Override
  protected boolean removeChildFromParent(Person person, Institution institution) {
    return person.removeInstitution(institution);
  }
}
