/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - GlobalBannerController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.model.GlobalBanner;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.unige.aou.business.GlobalBannerService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_GLOBAL_BANNERS)
public class GlobalBannerController extends ResourceController<GlobalBanner> {

  @Override
  @PostMapping
  public HttpEntity<GlobalBanner> create(@RequestBody GlobalBanner globalBanner) {
    return super.create(globalBanner);
  }

  @Override
  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<GlobalBanner> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public HttpEntity<RestCollection<GlobalBanner>> list(GlobalBanner search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<GlobalBanner> get(@PathVariable String id) {
    return super.get(id);
  }

  @EveryonePermissions
  @GetMapping(AouActionName.GET_ACTIVE)
  public HttpEntity<GlobalBanner> getActive() {
    GlobalBanner globalBanner = ((GlobalBannerService) this.itemService).findActive();
    if (globalBanner != null) {
      return new ResponseEntity<>(globalBanner, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).getActive()).withRel(AouActionName.GET_ACTIVE));
  }
}
