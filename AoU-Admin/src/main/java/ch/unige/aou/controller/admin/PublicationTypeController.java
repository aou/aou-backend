/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationTypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PUBLICATION_TYPES)
public class PublicationTypeController extends ResourceController<PublicationType> {

  @Override
  @PostMapping
  public HttpEntity<PublicationType> create(@RequestBody PublicationType type) {
    return super.create(type);
  }

  @Override
  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @DeleteMapping
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @Override
  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<PublicationType> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  @UserPermissions
  public HttpEntity<RestCollection<PublicationType>> list(@ModelAttribute PublicationType search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @UserPermissions
  public HttpEntity<PublicationType> get(@PathVariable String id) {
    return super.get(id);
  }
}
