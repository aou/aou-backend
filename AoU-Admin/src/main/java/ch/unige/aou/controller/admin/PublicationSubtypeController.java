/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationSubtypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeContributorRole;
import ch.unige.aou.model.publication.PublicationSubtypeContributorRoleDTO;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileType;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileTypeDTO;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@EveryonePermissions
@RestController
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PUBLICATION_SUBTYPES)
public class PublicationSubtypeController extends ResourceController<PublicationSubtype> {

  @Override
  @PostMapping
  @AdminPermissions
  public HttpEntity<PublicationSubtype> create(@RequestBody PublicationSubtype subtype) {
    return super.create(subtype);
  }

  @Override
  @DeleteMapping(SolidifyConstants.URL_ID)
  @AdminPermissions
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @DeleteMapping
  @AdminPermissions
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @Override
  @PatchMapping(SolidifyConstants.URL_ID)
  @AdminPermissions
  public HttpEntity<PublicationSubtype> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public HttpEntity<RestCollection<PublicationSubtype>> list(@ModelAttribute PublicationSubtype search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<PublicationSubtype> get(@PathVariable String id) {
    return super.get(id);
  }

  @GetMapping(SolidifyConstants.URL_ID + "/" + AouActionName.LIST_DOCUMENT_FILE_TYPES)
  public HttpEntity<RestCollection<PublicationSubtypeDocumentFileTypeDTO>> listDocumentFileTypes(@PathVariable String id,
          @RequestParam(value = "discardUndefinedLabels", required = false, defaultValue = "false") boolean discardUndefinedLabels,
          @ModelAttribute PublicationSubtypeDocumentFileType filterItem) {

    PublicationSubtype publicationSubtype = this.itemService.findOne(id);
    List<PublicationSubtypeDocumentFileType> items = publicationSubtype.getPublicationSubtypeDocumentFileTypes();

    if (discardUndefinedLabels) {
      items = items.stream().filter(item ->
                      !item.getDocumentFileType().getLabels().stream().anyMatch(label -> label.getText().equals(AouConstants.UNDEFINED_VERSION_EN)))
              .collect(Collectors.toList());
    }

    List<PublicationSubtypeDocumentFileTypeDTO> dtoItems = items.stream()
            .map(PublicationSubtypeDocumentFileTypeDTO::new)
            .sorted(Comparator.comparingInt(PublicationSubtypeDocumentFileTypeDTO::getSortValue))
            .collect(Collectors.toList());

    if (filterItem.getLevel() != null) {
      dtoItems = dtoItems.stream()
              .filter(item -> item.getLevel() == filterItem.getLevel())
              .collect(Collectors.toList());
    }

    return new ResponseEntity<>(new RestCollection(dtoItems), HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID + "/" + AouActionName.LIST_CONTRIBUTOR_ROLES)
  public HttpEntity<RestCollection<PublicationSubtypeContributorRoleDTO>> listContributorRoles(@PathVariable String id,
          @ModelAttribute PublicationSubtypeContributorRole filterItem) {

    PublicationSubtype publicationSubtype = this.itemService.findOne(id);
    List<PublicationSubtypeContributorRole> items = publicationSubtype.getPublicationSubtypeContributorRoles()
            .stream()
            .filter(PublicationSubtypeContributorRole::getVisible)
            .collect(Collectors.toList());
    List<PublicationSubtypeContributorRoleDTO> dtoItems = items.stream()
            .map(PublicationSubtypeContributorRoleDTO::new)

            .sorted(Comparator.comparingInt(PublicationSubtypeContributorRoleDTO::getSortValue))
            .collect(Collectors.toList());

    return new ResponseEntity<>(new RestCollection(dtoItems), HttpStatus.OK);
  }
}
