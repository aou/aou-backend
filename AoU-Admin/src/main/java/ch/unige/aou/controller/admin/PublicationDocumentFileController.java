/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationDocumentFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import jakarta.validation.Valid;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringParserTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.controller.AouCompositionController;
import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.storage.StorageService;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PUBLICATIONS + SolidifyConstants.URL_PARENT_ID + ResourceName.DOCUMENT_FILES)
public class PublicationDocumentFileController extends AouCompositionController<Publication, DocumentFile> {

  private static final Logger log = LoggerFactory.getLogger(PublicationDocumentFileController.class);

  private final StorageService storageService;

  PublicationDocumentFileController(StorageService storageService) {
    this.storageService = storageService;
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  public HttpEntity<RestCollection<DocumentFile>> list(@PathVariable String parentid, @ModelAttribute DocumentFile filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'GET_FILE')")
  public HttpEntity<DocumentFile> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'ADD_DOCUMENT_FILE')")
  public HttpEntity<DocumentFile> create(@PathVariable String parentid, @Valid @RequestBody DocumentFile childResource) {
    return super.create(parentid, childResource);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'UPDATE_DOCUMENT_FILE')")
  public HttpEntity<DocumentFile> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> updateMap) {
    return super.update(parentid, id, updateMap);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'DELETE_DOCUMENT_FILE')")
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    if (!((DocumentFileService) this.subResourceService).hasAdminOrRootRoleToDeleteDocument(id)) {
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    ResponseEntity<Void> status = super.delete(parentid, id);
    if (status.getStatusCode() == HttpStatus.OK) {
      ((DocumentFileService) this.subResourceService).verifyIfThereAreStillDuplicates(parentid);
    }
    return status;
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'DELETE_DOCUMENT_FILE')")
  public ResponseEntity<Void> deleteList(@PathVariable final String parentid, @RequestBody String[] ids) {
    ResponseEntity<Void> status = super.deleteList(parentid, ids);
    if (status.getStatusCode() == HttpStatus.OK) {
      ((DocumentFileService) this.subResourceService).verifyIfThereAreStillDuplicates(parentid);
    }
    return status;
  }

  @PostMapping("/" + ActionName.UPLOAD)
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'UPLOAD_DOCUMENT_FILE')")
  public HttpEntity<DocumentFile> uploadFile(
          @PathVariable String parentid,
          @RequestParam("documentFileTypeId") String documentFileTypeId,
          @RequestParam("accessLevel") String accessLevelName,
          @RequestParam(value = "embargoAccessLevel", required = false) String embargoAccessLevelName,
          @RequestParam(value = "embargoEndDate", required = false) String embargoEndDateStr,
          @RequestParam(value = "licenseId", required = false) String licenseId,
          @RequestParam(value = "notSureForLicense", required = false, defaultValue = "false") boolean notSureForLicense,
          @RequestParam(value = "label", required = false) String label,
          @RequestParam("file") MultipartFile multipartFile) {

    DocumentFile.AccessLevel accessLevel = DocumentFile.AccessLevel.valueOf(accessLevelName);

    DocumentFile.AccessLevel embargoAccessLevel = null;
    if (!StringTool.isNullOrEmpty(embargoAccessLevelName)) {
      embargoAccessLevel = DocumentFile.AccessLevel.valueOf(embargoAccessLevelName);
    }

    LocalDate embargoEndDate = null;
    if (!StringTool.isNullOrEmpty(embargoEndDateStr)) {
      embargoEndDate = LocalDate.parse(embargoEndDateStr);
    }

    String logMessage = "Will start upload of new DocumentFile for Publication '" + parentid + "' (documentFileTypeId=" + documentFileTypeId
            + ", accessLevel=" + accessLevel;
    if (!StringTool.isNullOrEmpty(licenseId)) {
      logMessage += ", licenseId=" + licenseId;
    }
    logMessage += ")";
    log.info(logMessage);

    DocumentFile documentFile = ((DocumentFileService) this.subResourceService)
            .uploadNewDocumentFile(parentid, documentFileTypeId, accessLevel, embargoAccessLevel, embargoEndDate, licenseId, notSureForLicense,
                    label, multipartFile);

    this.addLinks(parentid, documentFile);
    return new ResponseEntity<>(documentFile, HttpStatus.OK);
  }

  @GetMapping("/" + ActionName.SEARCH)
  @SuppressWarnings("squid:S4684")
  public HttpEntity<RestCollection<DocumentFile>> search(@PathVariable String parentid, @ModelAttribute DocumentFile documentFile,
          @RequestParam("search") String search, @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
    final SearchCriteria parentCriteria = new SearchCriteria("publication.resId", ":", parentid);
    final Page<DocumentFile> listItem = this.subResourceService
            .findBySubResourceSearchCriteria(documentFile, matchtype, criterias, parentCriteria, pageable);
    final RestCollection<DocumentFile> collection = new RestCollection<>(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'HISTORY')")
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String parentid, @PathVariable String id, Pageable pageable) {
    return super.history(parentid, id, pageable);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.CONFIRM)
  public HttpEntity<Result> confirm(@PathVariable final String parentid, @PathVariable final String id) {
    DocumentFile item = this.checkSubresourceExistsAndIsLinkedToParent(id, parentid);

    final Result res = new Result(id);
    if (item == null) {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      return new ResponseEntity<>(res, HttpStatus.NOT_FOUND);
    } else if (((DocumentFileService) this.subResourceService).statusUpdateIsValid(item, DocumentFile.DocumentFileStatus.CONFIRMED)) {
      item.setStatus(DocumentFile.DocumentFileStatus.CONFIRMED);
      this.subResourceService.save(item);
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage("DocumentFile status changed successfully");
      return new ResponseEntity<>(res, HttpStatus.OK);
    } else {
      res.setStatus(Result.ActionStatus.NON_APPLICABLE);
      res.setMesssage("DocumentFile's status cannot be altered in this way");
      return new ResponseEntity<>(res, HttpStatus.BAD_REQUEST);
    }
  }

  @EveryonePermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.DOWNLOAD)
  @ResponseBody
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, #id, 'DOWNLOAD_FILE')"
          + "|| @downloadTokenPermissionService.isAllowed(#id, T(ch.unige.aou.model.security.DownloadTokenType).DOCUMENT)")
  public HttpEntity<StreamingResponseBody> download(@PathVariable String parentid, @PathVariable String id) throws IOException {
    DocumentFile item = this.checkSubresourceExistsAndIsLinkedToParent(id, parentid);
    if (item == null) {
      return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
    URI fileUri = item.getDownloadUri();
    String contentTypeFile = item.getContentType() == null ? FileTool.getContentType(fileUri).toString() : item.getContentType();
    InputStream is = this.storageService.getInputStream(fileUri);

    return this.buildDownloadResponseEntity(is, fileUri.getPath().substring(fileUri.getPath().lastIndexOf('/') + 1),
            contentTypeFile, item.getFileSize());
  }

  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'LIST_FILES')")
  @GetMapping("/" + AouActionName.LIST_CURRENT_STATUS)
  public ResponseEntity<Object> listCurrentStatus(@PathVariable String parentid, @ModelAttribute DocumentFile filterItem) {
    Map<DocumentFile.DocumentFileStatus, Long> mapStatistics = ((DocumentFileService) this.subResourceService).getMapStatusOccurrences(parentid);
    return new ResponseEntity<>(mapStatistics, HttpStatus.OK);
  }

  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'UPDATE_DOCUMENT_FILE')")
  @PostMapping("/" + AouActionName.UPDATE_SORT_VALUES)
  public ResponseEntity<Object> updateSortValues(@PathVariable String parentid, @RequestBody List<String> ids) {
    if (ids.isEmpty()) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "List of document file resIds is empty");
    }
    boolean updated = ((DocumentFileService) this.subResourceService).updateSortValues(parentid, ids);
    if (updated) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
  }

  @Override
  protected Publication getParentResourceProperty(DocumentFile documentFile) {
    return documentFile.getPublication();
  }

  @Override
  protected void setParentResourceProperty(DocumentFile documentFile, Publication publication) {
    documentFile.setPublication(publication);
  }

}
