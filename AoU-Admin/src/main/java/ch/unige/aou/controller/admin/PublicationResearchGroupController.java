/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationResearchGroupController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

@RestController
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PUBLICATIONS + SolidifyConstants.URL_PARENT_ID + ResourceName.RESEARCH_GROUPS)
public class PublicationResearchGroupController extends AssociationController<Publication, ResearchGroup> {

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<ResearchGroup>> list(@PathVariable String parentid, @ModelAttribute ResearchGroup filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  protected String getParentFieldName() {
    return "publications";
  }

  @Override
  public ResearchGroup getEmptyChildResourceObject() {
    return new ResearchGroup();
  }

  @Override
  protected boolean addChildOnParent(Publication publication, ResearchGroup researchGroup) {
    return publication.addResearchGroup(researchGroup);
  }

  @Override
  protected boolean removeChildFromParent(Publication publication, ResearchGroup researchGroup) {
    return publication.removeResearchGroup(researchGroup);
  }

  @Override
  @PostMapping
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'ADD_DEPOSIT_RESEARCH_GROUP')")
  public HttpEntity<List<ResearchGroup>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  @PatchMapping(SolidifyConstants.URL_ID)
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'UPDATE_DEPOSIT_RESEARCH_GROUP')")
  public HttpEntity<ResearchGroup> update(@PathVariable String parentid, @PathVariable String id, @RequestBody ResearchGroup researchGroup) {
    return super.update(parentid, id, researchGroup);
  }

  @Override
  @DeleteMapping(SolidifyConstants.URL_ID)
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'DELETE_DEPOSIT_RESEARCH_GROUP')")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return super.delete(parentid, id);
  }

  @Override
  @DeleteMapping
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'DELETE_DEPOSIT_RESEARCH_GROUP')")
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.deleteList(parentid, ids);
  }
}
