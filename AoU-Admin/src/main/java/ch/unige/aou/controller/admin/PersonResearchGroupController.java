/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonResearchGroupController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.rest.RestCollection;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.security.CurrentUserPermissions;

@CurrentUserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PEOPLE + SolidifyConstants.URL_PARENT_ID + ResourceName.RESEARCH_GROUPS)
public class PersonResearchGroupController extends AssociationController<Person, ResearchGroup> {

  @Override
  public HttpEntity<RestCollection<ResearchGroup>> list(@PathVariable String parentid, @ModelAttribute ResearchGroup filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<ResearchGroup> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  public HttpEntity<List<ResearchGroup>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    HttpEntity<List<ResearchGroup>> result = super.create(parentid, ids);
    SolidifyEventPublisher.getPublisher().publishEvent(new ResourceCacheMessage(ResearchGroup.class, parentid));
    return result;
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    ResponseEntity<Void> result = super.delete(parentid, id);
    SolidifyEventPublisher.getPublisher().publishEvent(new ResourceCacheMessage(ResearchGroup.class, parentid));
    return result;
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    ResponseEntity<Void> result = super.deleteList(parentid, ids);
    SolidifyEventPublisher.getPublisher().publishEvent(new ResourceCacheMessage(ResearchGroup.class, parentid));
    return result;
  }

  @Override
  protected String getParentFieldName() {
    return "people";
  }

  @Override
  public ResearchGroup getEmptyChildResourceObject() {
    return new ResearchGroup();
  }

  @Override
  protected boolean addChildOnParent(Person person, ResearchGroup researchGroup) {
    return person.addResearchGroup(researchGroup);
  }

  @Override
  protected boolean removeChildFromParent(Person person, ResearchGroup researchGroup) {
    return person.removeResearchGroup(researchGroup);
  }
}
