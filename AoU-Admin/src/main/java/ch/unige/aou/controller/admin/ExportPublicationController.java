/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ExportMetadataController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.access.ExportFormat;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.metadata.exports.ExportPublicationService;
import ch.unige.aou.service.query.QueryBuilderService;

@RestController
@EveryonePermissions
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_EXPORT_PUBLICATIONS)
public class ExportPublicationController extends SolidifyController {

  private final QueryBuilderService queryBuilderService;
  private final ExportPublicationService exportPublicationService;

  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;
  private final String indexName;
  private final String inProgressIndexName;

  public ExportPublicationController(AouProperties aouProperties, QueryBuilderService queryBuilderService,
          IndexResourceService<String, PublicationIndexEntry> indexResourceService,
          ExportPublicationService exportPublicationService) {
    this.queryBuilderService = queryBuilderService;
    this.exportPublicationService = exportPublicationService;
    this.indexResourceService = indexResourceService;
    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.inProgressIndexName = aouProperties.getIndexing().getInProgressPublicationsIndexName();
  }

  @GetMapping(value = "/" + AouActionName.EXPORT)
  public HttpEntity<StreamingResponseBody> exportMetadata(
          @RequestParam(name = "searches", required = false) String commaSeparatedSearchIds,
          @RequestParam(name = "publicationIds", required = false) String commaSeparatedPublicationIds,
          @RequestParam(name = "days", required = false) String searchFrom,
          @RequestParam(name = "format") String format) {

    ExportFormat exportFormat = ExportFormat.fromValue(format);
    if (StringTool.isNullOrEmpty(commaSeparatedSearchIds) && StringTool.isNullOrEmpty(commaSeparatedPublicationIds) && StringTool.isNullOrEmpty(
            searchFrom)) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "Either one search id or one publication id or a date is required");
    }
    List<PublicationIndexEntry> indexResults = this.getIndexEntries(this.indexName, commaSeparatedSearchIds, commaSeparatedPublicationIds,
            searchFrom);

    String resultFromExport = "";
    if (!indexResults.isEmpty()) {
      resultFromExport = this.exportPublicationService.exportMetadata(exportFormat, indexResults);
    }
    return this.buildDownloadResponseFromExport(exportFormat, resultFromExport);
  }

  @PostMapping(value = "/" + AouActionName.EXPORT_DEPOSITS)
  @PreAuthorize("@publicationPermissionService.isAllowed(#commaSeparatedPublicationIds, 'EXPORT_DEPOSITS')")
  public HttpEntity<StreamingResponseBody> exportPublicationsData(
          @RequestBody(required = false) List<String> commaSeparatedPublicationIds) {
    ExportFormat exportFormat = ExportFormat.TSV;

    if (commaSeparatedPublicationIds.isEmpty()) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "Either one search id or one publication id is required");
    }
    List<PublicationIndexEntry> indexResults = this.getIndexEntries(this.inProgressIndexName, null,
            String.join(",", commaSeparatedPublicationIds), null);

    String resultFromExport = "";
    if (!indexResults.isEmpty()) {
      resultFromExport = this.exportPublicationService.exportPublicationInProgressToTsv(indexResults);
    }

    return this.buildDownloadResponseFromExport(exportFormat, resultFromExport);
  }

  private List<PublicationIndexEntry> getIndexEntries(String indexName, String commaSeparatedSearchIds, String commaSeparatedPublicationIds,
          String searchFrom) {

    List<String> searchIds = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(commaSeparatedSearchIds)) {
      searchIds.addAll(Arrays.stream(commaSeparatedSearchIds.split(",")).map(String::trim).toList());
    }

    List<String> publicationIds = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(commaSeparatedPublicationIds)) {
      publicationIds.addAll(Arrays.stream(commaSeparatedPublicationIds.split(",")).map(String::trim).toList());
    }

    AouSearchCondition searchConditions = this.queryBuilderService.getSearchesOrPublicationsSearchCondition(searchFrom, searchIds,
            publicationIds);

    List<FacetRequest> facetRequests = new ArrayList<>(); // not interested in facets here
    List<SearchCondition> searchConditionList = this.queryBuilderService.replaceAliasesByIndexField(List.of(searchConditions));
    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE,
            Sort.by(AouConstants.INDEX_FIELD_FIRST_VALIDATION_DATE).descending());
    FacetPage<PublicationIndexEntry> indexEntriesPage;
    List<PublicationIndexEntry> indexResults = new ArrayList<>();

    do {
      FieldsRequest fieldsRequest = new FieldsRequest();
      fieldsRequest.getExcludes().add(AouConstants.INDEX_FIELD_FULLTEXTS);
      indexEntriesPage = this.indexResourceService.search(indexName, searchConditionList, facetRequests, pageable, fieldsRequest);
      indexResults.addAll(indexEntriesPage.getContent());
      pageable = indexEntriesPage.nextPageable();
    } while (indexEntriesPage.hasNext());

    return indexResults;
  }

  private ResponseEntity<StreamingResponseBody> buildDownloadResponseFromExport(ExportFormat exportFormat, String resultFromExport) {
    // convert the string result to Inputstream
    InputStream is = new BufferedInputStream(new ByteArrayInputStream(resultFromExport.getBytes(StandardCharsets.UTF_8)));

    return this.buildDownloadResponseEntity(is, this.getFileName(exportFormat), this.getContentType(exportFormat), 0L);
  }

  private String getFileName(ExportFormat format) {
    return switch (format) {
      case XML -> "metadata.xml";
      case JSON -> "metadata.json";
      case TSV -> "metadata.csv";
      case BIBTEX -> "metadata.bib";
    };
  }

  private String getContentType(ExportFormat format) {
    return switch (format) {
      case TSV -> "text/tab-separated-values";
      case XML -> "application/xml";
      case JSON -> "application/json";
      case BIBTEX -> "application/x-bibtex";
    };
  }
}
