/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.time.OffsetDateTime;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.business.OrcidSynchronizationService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.security.CurrentUserPermissions;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PEOPLE)
public class PersonController extends ResourceWithFileController<Person> {

  private final OrcidSynchronizationService orcidSynchronizationService;

  public PersonController(OrcidSynchronizationService orcidSynchronizationService) {
    this.orcidSynchronizationService = orcidSynchronizationService;
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<Person>> advancedSearch(@ModelAttribute Person person,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    return super.advancedSearch(person, search, matchtype, pageable);
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<Person>> list(@ModelAttribute Person search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @UserPermissions
  public HttpEntity<Person> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @CurrentUserPermissions
  public HttpEntity<Person> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  protected RestCollection<Person> setCollectionLinksForMethod(Page<Person> listItem, Pageable pageable, WebMvcLinkBuilder linkBuilder) {
    final RestCollection<Person> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkBuilder.withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder()).withRel(ActionName.MODULE));
    this.addSortLinks(linkBuilder, collection);
    this.addPageLinks(linkBuilder, collection, pageable);
    this.addOthersLinks(collection);
    return collection;
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.UPLOAD_AVATAR)
  @CurrentUserPermissions
  public HttpEntity<Person> uploadAvatar(@PathVariable String id, @RequestParam("file") MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.DOWNLOAD_AVATAR)
  @UserPermissions
  @ResponseBody
  public HttpEntity<StreamingResponseBody> downloadAvatar(@PathVariable String id) {
    return super.downloadFile(id);
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.DELETE_AVATAR)
  @CurrentUserPermissions
  @ResponseBody
  public HttpEntity<Person> deleteAvatar(@PathVariable String id) {
    return super.deleteFile(id);
  }

  @PostMapping("/" + AouActionName.IMPORT_ALL_PROFILES_FROM_ORCID)
  @AdminPermissions
  public HttpEntity<Result> importAllProfilesFromOrcid() {
    this.orcidSynchronizationService.synchronizeAllPeopleFromOrcidProfile(OffsetDateTime.now().minusYears(100));
    final Result res = new Result();
    res.setStatus(Result.ActionStatus.EXECUTED);
    res.setMesssage("Import from ORCID for all profiles started");
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.IMPORT_ALL_FROM_ORCID)
  @CurrentUserPermissions
  public HttpEntity<Result> importAllFromOrcid(@PathVariable String id) {
    Person person = this.itemService.findOne(id);
    this.orcidSynchronizationService.synchronizeAllFromOrcidProfileForPerson(person);
    final Result res = new Result(id);
    res.setStatus(Result.ActionStatus.EXECUTED);
    res.setMesssage(this.messageService.get("publication.orcid.sync_from_orcid_started"));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.EXPORT_ALL_TO_ORCID)
  @CurrentUserPermissions
  public HttpEntity<Result> exportAllToOrcid(@PathVariable String id) {
    Person person = this.itemService.findOne(id);
    this.orcidSynchronizationService.synchronizeAllToOrcidProfileForPerson(person, true);
    final Result res = new Result(id);
    res.setStatus(Result.ActionStatus.EXECUTED);
    res.setMesssage(this.messageService.get("publication.orcid.sync_to_orcid_started"));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }
}
