/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ResearchGroupController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.rest.ResearchGroupDTO;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_RESEARCH_GROUPS)
public class ResearchGroupController extends ResourceController<ResearchGroup> {

  @EveryonePermissions
  @Override
  public HttpEntity<RestCollection<ResearchGroup>> advancedSearch(@ModelAttribute ResearchGroup researchGroup,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchType, Pageable pageable) {
    return super.advancedSearch(researchGroup, search, matchType, pageable);
  }

  @UserPermissions
  @GetMapping(AouActionName.BY_EXTERNAL_UID)
  public HttpEntity<RestCollection<ResearchGroup>> listResearchGroups(@RequestParam(AouConstants.EXTERNAL_UID) String externalUid,
          Pageable pageable) {
    Page<ResearchGroup> listItem = ((ResearchGroupService) this.itemService).findByUserExternalUid(externalUid, pageable);
    final RestCollection<ResearchGroup> collection = new RestCollection<>(listItem.toList());
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  @EveryonePermissions
  public HttpEntity<RestCollection<ResearchGroup>> list(ResearchGroup search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @EveryonePermissions
  public HttpEntity<ResearchGroup> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @UserPermissions
  public HttpEntity<ResearchGroup> create(@RequestBody ResearchGroup researchGroup) {
    // Priviledged users create new Research groups that are validated by default
    boolean isPriviledgedUser = ((ResearchGroupService) this.itemService).authenticatedUserHasPriviledgedRole();
    if (isPriviledgedUser && researchGroup.isValidated() == null) {
      researchGroup.setValidated(true);
    } else if (!isPriviledgedUser) {
      researchGroup.setValidated(false);
    }
    return super.create(researchGroup);
  }

  @Override
  public HttpEntity<ResearchGroup> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.VALIDATE)
  public HttpEntity<ResearchGroup> validate(@PathVariable String id) {
    ResearchGroup researchGroup = ((ResearchGroupService) this.itemService).validate(id);
    return new ResponseEntity<>(researchGroup, HttpStatus.OK);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @RootPermissions
  @PostMapping("/" + AouActionName.INITIALIZE)
  public HttpEntity<Result> initDefaultValues() {
    final Result res = new Result();
    try {
      ((ResearchGroupService) this.itemService).createResearchGroupsFromJsonFile("research_groups.json");
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage(this.messageService.get("research_groups.initialization.success"));
    } catch (IOException e) {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      res.setMesssage(this.messageService.get("research_groups.initialization.error", new Object[] { e.getMessage() }));
    }
    res.add(linkTo(this.getClass()).slash(AouActionName.INITIALIZE).withSelfRel());
    res.add(linkTo(this.getClass()).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping("/" + AouActionName.SYNCHRONIZE)
  public HttpEntity<Result> synchronize(@RequestParam(value = "since", required = false) String since) {
    final Result res = new Result();
    LocalDateTime createdOrModifiedSince = LocalDateTime.of(2000, 1, 1, 0, 0);
    if (!StringTool.isNullOrEmpty(since)) {
      createdOrModifiedSince = LocalDateTime.parse(since);
    }
    List<ResearchGroupDTO> synchronizedResearchGroups = ((ResearchGroupService) this.itemService)
            .synchronizeFromUnigeReferential(createdOrModifiedSince);
    res.setStatus(Result.ActionStatus.EXECUTED);
    if (!synchronizedResearchGroups.isEmpty()) {
      res.setMesssage(
              this.messageService.get("research_groups.synchronization.success.number", new Object[] { synchronizedResearchGroups.size() }));
    } else {
      res.setMesssage(this.messageService.get("research_groups.synchronization.success.none"));
    }

    res.add(linkTo(this.getClass()).slash(AouActionName.SYNCHRONIZE).withSelfRel());
    res.add(linkTo(this.getClass()).withRel(ActionName.PARENT));
    return new ResponseEntity<>(res, HttpStatus.OK);
  }
}
