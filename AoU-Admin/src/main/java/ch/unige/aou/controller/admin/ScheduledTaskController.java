/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ScheduledTaskController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.security.RootPermissions;

import ch.unige.aou.business.ScheduledTaskService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.controller.AouResourceController;
import ch.unige.aou.model.schedule.ScheduledTask;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@RootPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_SCHEDULED_TASKS)
public class ScheduledTaskController extends AouResourceController<ScheduledTask> {

  @PostMapping("/" + AouActionName.SCHEDULE_ENABLED_TASKS)
  public HttpStatus scheduleEnabledTasks() {
    ((ScheduledTaskService) this.itemService).scheduleEnabledTasks();
    return HttpStatus.OK;
  }

  @PostMapping("/" + AouActionName.DISABLE_TASKS_SCHEDULING)
  public HttpStatus disableTasksScheduling() {
    ((ScheduledTaskService) this.itemService).disableAllTasksScheduling();
    return HttpStatus.OK;
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.KILL_TASK)
  public HttpStatus killTask(@PathVariable String id) {
    ScheduledTask scheduledTask = this.itemService.findOne(id);
    ((ScheduledTaskService) this.itemService).killTask(scheduledTask);
    return HttpStatus.OK;
  }
}
