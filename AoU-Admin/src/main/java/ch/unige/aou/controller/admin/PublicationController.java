/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import static ch.unige.aou.model.publication.Publication.PublicationStatus.CANCEL_EDITION;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.CANONICAL;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.COMPLETED;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.DELETED;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.FEEDBACK_REQUIRED;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.IN_EDITION;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.IN_PROGRESS;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.IN_VALIDATION;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.REJECTED;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.SUBMITTED;
import static ch.unige.aou.model.publication.Publication.PublicationStatus.UPDATES_VALIDATION;
import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.ResourceWithFileController;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.CommentService;
import ch.unige.aou.business.EventService;
import ch.unige.aou.business.OrcidSynchronizationService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.exception.AouMetadataPotentialDuplicateException;
import ch.unige.aou.exception.ErrorCodes;
import ch.unige.aou.message.CleanIndexesMessage;
import ch.unige.aou.message.PublicationIndexingMessage;
import ch.unige.aou.message.PublicationMessage;
import ch.unige.aou.model.ExistingPublicationInfo;
import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.contact.ContactableContributor;
import ch.unige.aou.model.contact.PublicationContactMessage;
import ch.unige.aou.model.exception.PublicationAlreadyExistsInOrcidProfileException;
import ch.unige.aou.model.exception.PublicationTooOldForOrcidException;
import ch.unige.aou.model.exception.PublicationWithoutExternalIdentifierException;
import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.MetadataDifference;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.Publication.PublicationStatus;
import ch.unige.aou.model.publication.PublicationUserRole;
import ch.unige.aou.model.rest.PublicationImportDTO;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.contact.ContactService;
import ch.unige.aou.service.metadata.imports.MetadataImportWorkFlowService;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.specification.PublicationSpecification;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PUBLICATIONS)
public class PublicationController extends ResourceWithFileController<Publication> {

  private static final Logger log = LoggerFactory.getLogger(PublicationController.class);

  private final PersonService personService;
  private final UserService userService;
  private final MetadataService metadataService;
  private final EventService eventService;
  private final CommentService commentService;
  private final MetadataImportWorkFlowService metadataImportWorkFlowService;
  private final HistoryService historyService;
  private final ContactService contactService;
  private final DuplicateService duplicateService;
  private final OrcidSynchronizationService orcidSynchronizationService;

  public PublicationController(
          AouProperties aouProperties,
          MetadataService metadataService,
          PersonService personService,
          EventService eventService,
          CommentService commentService,
          MetadataImportWorkFlowService metadataImportWorkFlowService,
          UserService userService,
          HistoryService historyService,
          ContactService contactService,
          DuplicateService duplicateService,
          OrcidSynchronizationService orcidSynchronizationService) {
    this.metadataService = metadataService;
    this.personService = personService;
    this.eventService = eventService;
    this.commentService = commentService;
    this.metadataImportWorkFlowService = metadataImportWorkFlowService;
    this.userService = userService;
    this.historyService = historyService;
    this.contactService = contactService;
    this.duplicateService = duplicateService;
    this.orcidSynchronizationService = orcidSynchronizationService;
  }

  @Override
  public HttpEntity<RestCollection<Publication>> list(@ModelAttribute Publication search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<Publication> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowedToCreate(#publication)")
  public HttpEntity<Publication> create(@RequestBody Publication publication) {

    Person creator = this.personService.getLinkedPerson(SecurityContextHolder.getContext().getAuthentication());
    publication.setCreator(creator);
    publication.setMetadataValidationType(Publication.MetadataValidationType.CURRENT_STEP);

    if (publication.getImportSource() != null && publication.getImportSource() != Publication.ImportSource.CLONE
            && publication.getImportSource() != Publication.ImportSource.FEDORA3) {
      publication.setNotifyFilesFromExternalSource(true);
      publication.setNotifyContributorsFromExternalSource(true);
    }

    HttpEntity<Publication> response = super.create(publication);
    this.eventService.createEvent(publication, EventType.PUBLICATION_CREATED);

    // try to import file if possible
    if (publication.getImportSource() != null) {
      try {
        this.metadataImportWorkFlowService.importFilesFromServices(publication);
        ((PublicationService) this.itemService).addCommentForContributorsPartiallyImportedIfNeeded(publication);
      } catch (IOException | NoSuchAlgorithmException | HttpStatusCodeException e) {
        log.warn("Unable to download publication's files", e);
      }
    }

    return response;
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#ids, 'DELETE')")
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowedToUpdate(#id, #updateMap)")
  public HttpEntity<Publication> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {

    Publication publication = this.itemService.findOne(id);

    if (!publication.isModifiable()) {
      throw new SolidifyUnmodifiableException(
              this.messageService.get("validation.resource.unmodifiable",
                      new Object[] { publication.getClass().getSimpleName() + ": " + id }));
    }

    // updating status is not allowed through the update endpoint
    if (updateMap.containsKey("status")) {
      PublicationStatus newStatus = PublicationStatus.valueOf(updateMap.get("status").toString());
      Publication existingPublication = this.itemService.findOne(id);
      if (existingPublication.getStatus() != newStatus) {
        throw new SolidifyCheckingException(this.messageService.get("deposit.error.updating_status_not_allowed_this_way"));
      }
    }

    publication.setMetadataValidationType(Publication.MetadataValidationType.CURRENT_STEP);

    this.itemService.patchResource(publication, updateMap);
    final Publication savedItem = this.itemService.save(publication);
    this.addLinks(savedItem);
    return new ResponseEntity<>(savedItem, HttpStatus.OK);
  }

  @GetMapping(AouActionName.LIST_MY_PUBLICATIONS)
  public HttpEntity<RestCollection<Publication>> listMyPublications(
          @RequestParam(value = "fullName", required = false) String fullName,
          @RequestParam(value = "asContributor", defaultValue = "false") boolean asContributor,
          @ModelAttribute Publication filterItem,
          Pageable pageable) {
    String creatorId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());

    String cnIndividu = null;
    if (asContributor) {
      // If as contributor --> try to find the current user cnIndividu to look for the publications he is a contributor
      Optional<String> cnIndividuOpt = this.personService.getAuthenticatedUserUnigeCnIndividu();
      cnIndividu = cnIndividuOpt.orElse(null);
    }

    Page<Publication> listItem = ((PublicationService) this.itemService).findMyPublications(filterItem, creatorId, cnIndividu, pageable,
            fullName);

    this.setResourceLinks(listItem);
    final RestCollection<Publication> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @GetMapping(AouActionName.LIST_VALIDABLE_PUBLICATIONS)
  public HttpEntity<RestCollection<Publication>> listValidablePublications(
          @RequestParam(value = "fullName", required = false) String fullName,
          @RequestParam(value = "showAllDeposits", required = false, defaultValue = "false") boolean showAllDeposits,
          @ModelAttribute Publication filterItem,
          Pageable pageable) {
    Page<Publication> listItem;
    if (((PublicationService) this.itemService).authenticatedUserHasPriviledgedRole() && showAllDeposits) {
      final Specification<Publication> spec = this.itemService.getSpecification(filterItem);
      ((PublicationSpecification) spec).setFullName(fullName);
      listItem = this.itemService.findAll(spec, pageable);
    } else {
      String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
      listItem = ((PublicationService) this.itemService).findValidablePublications(authenticatedPersonId, filterItem, pageable, fullName);
    }

    this.setResourceLinks(listItem);
    final RestCollection<Publication> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.ALLOWED_TO_SUBMIT)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'SUBMIT')")
  public HttpEntity<Publication> isAllowedToSubmit(@PathVariable String id) {
    return super.get(id);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.SUBMIT_FOR_VALIDATION)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'SUBMIT_FOR_VALIDATION')")
  public HttpEntity<Result> submitForApproval(@PathVariable String id, @RequestBody(required = false) String[] ids) {
    if (ids != null) {
      ((PublicationService) this.itemService).syncValidationStructureRelations(id, ids);
    }
    Publication publication = this.itemService.findOne(id);
    publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL);
    if (this.historyService.hasBeenCompleted(publication.getResId())) {
      return new ResponseEntity<>(this.changePublicationStatus(UPDATES_VALIDATION, publication), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(this.changePublicationStatus(IN_VALIDATION, publication), HttpStatus.OK);
    }
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.SUBMIT)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'SUBMIT')")
  public HttpEntity<Result> submit(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION);
    Result result = this.changePublicationStatus(SUBMITTED, publication);

    // Put publication in queue to be processed
    if (result.getStatus() == Result.ActionStatus.EXECUTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMessage(id));
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.REJECT)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'REJECT')")
  public HttpEntity<Result> reject(@PathVariable String id, @RequestParam(value = AouConstants.REASON, required = true) String reason) {
    if (reason.isBlank()) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Result result;
    Publication publication = this.itemService.findOne(id);
    if (publication.getStatus().equals(UPDATES_VALIDATION)) {
      result = this.changePublicationStatus(CANCEL_EDITION, publication, reason);
    } else {
      result = this.changePublicationStatus(REJECTED, publication, reason);
    }

    // Put publication in queue to be processed
    if (result.getStatus() == Result.ActionStatus.EXECUTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMessage(id));
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.ENABLE_REVISION)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id,'ENABLE_REVISION')")
  public HttpEntity<Result> enableRevision(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    if (this.historyService.hasBeenCompleted(publication.getResId())) {
      return new ResponseEntity<>(this.changePublicationStatus(IN_EDITION, publication), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(this.changePublicationStatus(IN_PROGRESS, publication), HttpStatus.OK);
    }
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.SEND_BACK_FOR_VALIDATION)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'SEND_BACK_FOR_VALIDATION')")
  public HttpEntity<Result> sendBackForValidation(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    if (this.historyService.hasBeenCompleted(publication.getResId())) {
      return new ResponseEntity<>(this.changePublicationStatus(UPDATES_VALIDATION, publication), HttpStatus.OK);
    } else {
      return new ResponseEntity<>(this.changePublicationStatus(IN_VALIDATION, publication), HttpStatus.OK);
    }
  }

  @AdminPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.SEND_TO_CANONICAL)
  public HttpEntity<Result> changeToCanonical(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    return new ResponseEntity<>(this.changePublicationStatus(CANONICAL, publication), HttpStatus.OK);
  }

  @AdminPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.SEND_BACK_TO_COMPLETED)
  public HttpEntity<Result> changeBackToCompleted(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    return new ResponseEntity<>(this.changePublicationStatus(COMPLETED, publication), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.RESUME)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id,'RESUME')")
  public HttpEntity<Result> resume(@PathVariable String id) {
    Publication item = this.itemService.findOne(id);
    Result result = this.saveResumeStatus(item);

    // Put publication in queue to be processed
    if (result.getStatus() == Result.ActionStatus.EXECUTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMessage(id));
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.ASK_FEEDBACK)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'ASK_FEEDBACK')")
  public HttpEntity<Result> askFeedback(@PathVariable String id, @RequestParam(value = AouConstants.REASON) String reason) {
    if (reason.isBlank()) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    Publication publication = this.itemService.findOne(id);

    return new ResponseEntity<>(this.changePublicationStatus(FEEDBACK_REQUIRED, publication, reason), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.DELETE)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'DELETE')")
  public HttpEntity<Result> updateStatusToDelete(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    return new ResponseEntity<>(this.changePublicationStatus(DELETED, publication, null), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.START_METADATA_EDITING)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'START_METADATA_EDITING')")
  public HttpEntity<Result> putInMetadataEditing(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    return new ResponseEntity<>(this.changePublicationStatus(IN_EDITION, publication), HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.CANCEL_METADATA_EDITING)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'CANCEL_METADATA_EDITING')")
  public HttpEntity<Result> cancelMetadataEditing(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    Result result = this.changePublicationStatus(CANCEL_EDITION, publication);

    // Put publication in queue to be processed
    if (result.getStatus() == Result.ActionStatus.EXECUTED) {
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMessage(id));
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.GET_PENDING_METADATA_UPDATES)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'GET_PENDING_METADATA_UPDATES')")
  public HttpEntity<Map<String, Map<String, List<MetadataDifference>>>> checkMetadataDifferencesUpdate(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    // FEEDBACK_REQUIRED can be used before first validation or when editing a publication. Before first validation metadataBackup is null.
    if (publication.getMetadataBackup() == null && publication.getStatus() != FEEDBACK_REQUIRED) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
    Map<String, Map<String, List<MetadataDifference>>> result;

    if (publication.getMetadataBackup() == null) {
      // case of FEEDBACK REQUIRED before first validation
      result = new HashMap<>();
    } else if (((PublicationService) this.itemService).authenticatedUserHasPriviledgedRole()
            || this.personService.isAllowedToValidate(authenticatedPersonId, publication)) {
      result = ((PublicationService) this.itemService).checkDifferencesInUpdate(publication, false);
    } else {
      result = ((PublicationService) this.itemService).checkDifferencesInUpdate(publication, true);
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.UNLINK_CONTRIBUTOR)
  public HttpEntity<Result> unlinkContributor(@PathVariable String id) {

    final Result res = new Result(id);

    Optional<String> cnIndividuOpt = this.personService.getAuthenticatedUserUnigeCnIndividu();

    if (cnIndividuOpt.isPresent() && ((PublicationService) this.itemService).cnIndividuBelongsToContributors(id, cnIndividuOpt.get())) {
      Publication publication = ((PublicationService) this.itemService).unlinkContributor(id, cnIndividuOpt.get());

      // create a comment
      String authenticatedPersonId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
      Person person = this.personService.findOne(authenticatedPersonId);

      Comment comment = new Comment();
      comment.setOnlyForValidators(false);
      comment.setPerson(person); // set current user as creator of the comment
      comment.setPublication(publication);
      comment.setText(this.messageService.get("publication.unlink_contributor", new Object[] { cnIndividuOpt.get() }));
      this.commentService.save(comment);

      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage("Contributor is no more considered as an UNIGE member");
    } else {
      res.setStatus(Result.ActionStatus.NON_APPLICABLE);
      res.setMesssage("No contributor with this cnIndividu found in the publication");
    }

    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'HISTORY')")
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.HISTORY)
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String id, Pageable pageable) {
    final Publication item = this.itemService.findOne(id);
    if (item == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final Page<StatusHistory> listItem = this.historyService.findByResId(id, pageable);
    final RestCollection<StatusHistory> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(methodOn(this.getClass()).history(id, pageable)).withSelfRel());
    collection
            .add(Tool.parentLink((linkTo(methodOn(this.getClass()).history(id, pageable))).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(methodOn(this.getClass()).history(id, pageable)), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @GetMapping("/" + ResourceName.SCHEMA)
  public HttpEntity<StreamingResponseBody> schema(@RequestParam(value = "version", required = false) String version) {
    return this.getSchemaStream(version);
  }

  @GetMapping("/" + AouActionName.LIST_VERSION)
  public HttpEntity<List<String>> listVersions() {
    return new ResponseEntity<>(
            Stream.of(AouMetadataVersion.values())
                    .map(AouMetadataVersion::getVersion)
                    .collect(Collectors.toList()),
            HttpStatus.OK);
  }

  protected HttpEntity<StreamingResponseBody> getSchemaStream(String version) {
    final AouMetadataVersion metadataVersion = this.getVersion(version);
    final ClassPathResource xsd = this.metadataService.getResourceSchema(metadataVersion);
    try {
      final InputStream is = xsd.getInputStream();
      return this.buildDownloadResponseEntity(is, xsd.getFilename(), SolidifyConstants.XML_MIME_TYPE, xsd.contentLength());
    } catch (final IOException e) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  private AouMetadataVersion getVersion(String version) {
    if (!StringTool.isNullOrEmpty(version)) {
      return AouMetadataVersion.fromVersion(version);
    }
    return AouMetadataVersion.getDefaultVersion();
  }

  @GetMapping(AouActionName.GET_MY_DEFAULT_PROFILE_DATA)
  public HttpEntity<String> getDefaultProfileDataToJson() {
    Person creator = this.personService.getLinkedPerson(SecurityContextHolder.getContext().getAuthentication());
    String jsonResult = ((PublicationService) this.itemService).convertDefaultProfileDataToJson(creator);
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return new ResponseEntity<>(jsonResult, headers, HttpStatus.OK);
  }

  @PatchMapping(AouActionName.UPDATE_PUBLICATIONS_RESEARCH_GROUP)
  @PreAuthorize("@publicationPermissionService.isAllowed(#ids, 'UPDATE')")
  public HttpEntity<Result> updatePublicationListWithResearchGroup(
          @RequestParam(value = AouConstants.DB_RESEARCH_GROUP_ID) String researchGroupId,
          @RequestBody String[] ids) {
    boolean noError = ((PublicationService) this.itemService).updatePublicationListWithResearchGroup(researchGroupId, ids);
    final Result res = new Result(researchGroupId);
    if (noError) {
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage("Publications were updated successfully with the Research Group");
    } else {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      res.setMesssage("Error while updating Publications list with a Research Group");
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PatchMapping(AouActionName.UPDATE_PUBLICATIONS_STRUCTURE)
  @PreAuthorize("@publicationPermissionService.isAllowed(#ids, 'UPDATE')")
  public HttpEntity<Result> updatePublicationListWithStructure(@RequestParam(value = AouConstants.DB_STRUCTURE_ID) String structureId,
          @RequestBody String[] ids) {
    boolean noError = ((PublicationService) this.itemService).updatePublicationListWithStructure(structureId, ids);
    final Result res = new Result(structureId);
    if (noError) {
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage("Publications were updated successfully with the Structure");
    } else {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      res.setMesssage("Error while updating Publications list with the Structure");
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PatchMapping(AouActionName.UPDATE_PUBLICATIONS_LANGUAGE)
  @PreAuthorize("@publicationPermissionService.isAllowed(#ids, 'UPDATE')")
  public HttpEntity<Result> updatePublicationListWithLanguage(@RequestParam(value = AouConstants.DB_LANGUAGE_ID) String languageId,
          @RequestBody String[] ids) {
    boolean noError = ((PublicationService) this.itemService).updatePublicationListWithLanguage(languageId, ids);
    final Result res = new Result(languageId);
    if (noError) {
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage("Publications were updated successfully with the Language");
    } else {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      res.setMesssage("Error while updating Publications list with the Language");
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PatchMapping(AouActionName.UPDATE_CONTRIBUTOR_DISPLAY_NAME)
  @AdminPermissions
  public HttpEntity<Result> updatePublicationListWithContributorDisplayName(@RequestParam("cnIndividu") String cnIndividu,
          @RequestParam("lastname") String lastname, @RequestParam("firstname") String firstname, @RequestBody String[] ids) {
    boolean noError = ((PublicationService) this.itemService).linkPublicationsWithContributorOtherName(ids, cnIndividu, lastname, firstname);
    final Result res = new Result();
    if (noError) {
      res.setStatus(Result.ActionStatus.EXECUTED);
      res.setMesssage("Publications were updated successfully with the contributor's display name");
    } else {
      res.setStatus(Result.ActionStatus.NOT_EXECUTED);
      res.setMesssage("Error while updating Publications list with the contributor's display name");
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @PostMapping(AouActionName.CREATE_FROM_IDENTIFIER)
  @PreAuthorize("@publicationPermissionService.isAllowedToCreate()")
  public HttpEntity<Publication> createFromIdentifier(@RequestBody PublicationImportDTO publicationImportDTO) {
    Person creator;
    if (StringTool.isNullOrEmpty(publicationImportDTO.getCreatorId())) {
      // By default, publication creator is the authenticated user
      creator = this.personService.getLinkedPerson(SecurityContextHolder.getContext().getAuthentication());
    } else {
      // Only admin users can specify another creator than themselves
      if (((PublicationService) this.itemService).authenticatedUserHasPriviledgedRole()) {
        String userId = publicationImportDTO.getCreatorId();
        User user = this.userService.findById(userId)
                .orElseThrow(() -> new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST, "No user with id " + userId));
        creator = user.getPerson();
      } else {
        throw new SolidifyHttpErrorException(HttpStatus.FORBIDDEN, "Only admin users can specify another creator than themselves");
      }
    }

    if (!StringTool.isNullOrEmpty(publicationImportDTO.getDoi()) && !ValidationTool.isValidDOI(CleanTool.trim(publicationImportDTO.getDoi()))
            && !ValidationTool.isValidShortDOI(CleanTool.trim(publicationImportDTO.getDoi()))) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              this.messageService.get("deposit.error.identifiers.doi.invalid", new Object[] { publicationImportDTO.getDoi() }));
    }

    if (!StringTool.isNullOrEmpty(publicationImportDTO.getPmid())
            && !ValidationTool.isValidPMID(CleanTool.trim(publicationImportDTO.getPmid()))) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              this.messageService.get("deposit.error.identifiers.pmid.invalid", new Object[] { publicationImportDTO.getPmid() }));
    }

    Publication publication = ((PublicationService) this.itemService).createFromIdentifier(publicationImportDTO, creator);
    return new ResponseEntity<>(publication, HttpStatus.CREATED);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.VALIDATE_METADATA)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id,'VALIDATE_METADATA')")
  public HttpEntity<String> validateMetadata(@PathVariable String id,
          @RequestParam(name = "forSubmit", defaultValue = "false") boolean checkValidForSubmit) {
    Publication publication = this.itemService.findOne(id);

    publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL);
    if (checkValidForSubmit) {
      publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION);
    }

    ((PublicationService) this.itemService).validateMetadata(publication);
    return new ResponseEntity<>("Publication is valid", HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.CHECK_DUPLICATES)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id,'CHECK_DUPLICATES')")
  public HttpEntity<String> checkDuplicates(@PathVariable String id) {
    ExistingPublicationInfo existingPublicationInfo = this.duplicateService.checkPublicationDuplicates(id);
    if (existingPublicationInfo == null) {
      return new ResponseEntity<>("Publication is valid", HttpStatus.OK);
    }
    final Map<Object, Object> params = this.duplicateService.getParamsFromExistingPublicationInfo(existingPublicationInfo);
    throw new AouMetadataPotentialDuplicateException(HttpStatus.BAD_REQUEST, this.messageService.get("deposit.error.duplicate"),
            ErrorCodes.PUBLICATION_POTENTIAL_DUPLICATE.value(), params);
  }

  private Result changePublicationStatus(PublicationStatus newStatus, Publication publication) {
    return this.changePublicationStatus(newStatus, publication, null);
  }

  private Result changePublicationStatus(PublicationStatus newStatus, Publication publication, String statusMessage) {

    final Result result;
    if (publication == null) {
      result = new Result();
      result.setStatus(Result.ActionStatus.NOT_EXECUTED);
    } else if (((PublicationService) this.itemService).statusUpdateIsValid(publication, newStatus)) {
      publication = ((PublicationService) this.itemService).backUpMetadataIfApply(publication, newStatus);
      publication.setStatus(newStatus);
      publication.setStatusMessage(statusMessage);

      if (!StringTool.isNullOrEmpty(statusMessage)) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String authenticatedPersonId = this.personService.getLinkedPersonId(authentication);
        Person currentPerson = this.personService.findOne(authenticatedPersonId);
        Comment comment = new Comment();
        comment.setPerson(currentPerson);
        comment.setPublication(publication);
        comment.setText(statusMessage);
        comment.setOnlyForValidators(false);
        comment.getCreation().setWho(authentication.getName());
        comment.getLastUpdate().setWho(authentication.getName());
        publication.getComments().add(comment);
      }
      this.itemService.save(publication);
      result = new Result(publication.getResId());
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage("Publication status changed successfully");
    } else {
      result = new Result(publication.getResId());
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage("Publications's status cannot be altered in this way");
    }
    return result;
  }

  private Result saveResumeStatus(Publication publication) {
    final Result result = new Result(publication.getResId());
    if (publication.getStatus() == PublicationStatus.COMPLETED) {
      result.setMesssage(this.messageService.get("publication.completed", new Object[] { publication.getResId() }));
    } else if (publication.getStatus() != PublicationStatus.IN_ERROR && publication.getStatus() != PublicationStatus.IN_PREPARATION) {
      result.setMesssage(this.messageService.get("publication.inprogress", new Object[] { publication.getResId() }));
    } else {
      publication.setStatus(SUBMITTED);
      publication.setStatusMessage(null);
      publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION);
      this.itemService.save(publication);
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("publication.resume", new Object[] { publication.getResId() }));
    }
    return result;
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.CLONE)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'CLONE')")
  public HttpEntity<Publication> clonePublication(@PathVariable String id) {
    Publication publicationClone = ((PublicationService) this.itemService).clonePublication(id);

    this.addLinks(publicationClone);
    return new ResponseEntity<>(publicationClone, HttpStatus.OK);
  }

  /******************************************************************************************************/

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.UPLOAD_THUMBNAIL)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'UPDATE')")
  public HttpEntity<Publication> uploadThumbnail(@PathVariable String id, @RequestParam("file") MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    return super.uploadFile(id, file, mimeType);
  }

  @EveryonePermissions
  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.DOWNLOAD_THUMBNAIL)
  @ResponseBody
  public HttpEntity<StreamingResponseBody> downloadThumbnail(@PathVariable String id) throws IOException {
    Publication item = this.itemService.findOne(id);
    if (((ResourceFileInterface) item).getResourceFile() == null) {
      return new ResponseEntity(HttpStatus.NOT_FOUND);
    } else {
      InputStream is = new BufferedInputStream(new ByteArrayInputStream(((ResourceFileInterface) item).getResourceFile().getFileContent()));
      String mimeType = URLConnection.guessContentTypeFromStream(is);
      return this.buildDownloadResponseEntity(is,
              URLEncoder.encode(((ResourceFileInterface) item).getResourceFile().getFileName(), StandardCharsets.UTF_8), mimeType,
              ((ResourceFileInterface) item).getResourceFile().getFileSize());
    }
  }

  @DeleteMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.DELETE_THUMBNAIL)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'UPDATE')")
  public HttpEntity<Publication> deleteThumbnail(@PathVariable String id) {
    return super.deleteFile(id);
  }

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.LIST_PUBLICATION_USER_ROLES)
  @UserPermissions
  public HttpEntity<List<PublicationUserRole>> listPublicationUserRoles(@PathVariable String id) {
    List<PublicationUserRole> publicationUserRoles = ((PublicationService) this.itemService).listPublicationUserRoles(id);
    return new ResponseEntity<>(publicationUserRoles, HttpStatus.OK);
  }

  /******************************************************************************************************/

  @RootPermissions
  @PostMapping("/" + AouActionName.UPGRADE_METADATA)
  public HttpEntity<Result> upgradeAllMetadataVersions() {
    int publicationsToUpgradeTotal = ((PublicationService) this.itemService).startUpgradingAllPublicationsMetadataVersion();
    final Result result = new Result();
    if (publicationsToUpgradeTotal > 0) {
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage(publicationsToUpgradeTotal + " publications metadata are going to be upgraded");
    } else {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage("No publication metadata need to be upgraded");
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.STORE_AGAIN)
  public HttpEntity<Result> storeAgain(@PathVariable String id) {
    Publication publication = this.itemService.findOne(id);
    final Result result = new Result(id);
    HttpStatus resultStatus;
    if (!StringTool.isNullOrEmpty(publication.getArchiveId()) && publication.getStatus().equals(COMPLETED)) {
      ((PublicationService) this.itemService).storeAgain(id);
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage("Publication '" + publication.getArchiveId() + "' is about to be stored again");
      resultStatus = HttpStatus.OK;
      log.info(result.getMessage());
    } else if (StringTool.isNullOrEmpty(publication.getArchiveId())) {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage("Publication '" + id + "' cannot be stored again as it doesn't have any archiveId");
      resultStatus = HttpStatus.BAD_REQUEST;
      log.warn(result.getMessage());
    } else {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage("Publication '" + publication.getArchiveId() + "' cannot be stored again as its status is not COMPLETED (but "
              + publication.getStatus() + ")");
      resultStatus = HttpStatus.BAD_REQUEST;
      log.warn(result.getMessage());
    }
    return new ResponseEntity<>(result, resultStatus);
  }

  /******************************************************************************************************/

  @RootPermissions
  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.REINDEX)
  public HttpEntity<Publication> reindex(@PathVariable String id) {
    Publication item = this.itemService.findOne(id);

    // Put publication in queue to be indexed
    SolidifyEventPublisher.getPublisher().publishEvent(new PublicationIndexingMessage(id));

    return new ResponseEntity<>(item, HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping("/" + AouActionName.REINDEX_ALL)
  public HttpEntity<String> reindexAll(@RequestBody(required = false) Map<String, String> postContent) {
    String query = null;
    Integer maxNumber = null;
    if (postContent != null) {
      if (postContent.containsKey("indexQuery")) {
        query = postContent.get("indexQuery");
      }
      if (postContent.containsKey("maxNumber")) {
        maxNumber = Integer.parseInt(postContent.get("maxNumber"));
      }
    }
    long total = ((PublicationService) this.itemService).reindexAll(query, maxNumber);

    return new ResponseEntity<>(total + " publications will be reindexed", HttpStatus.OK);
  }

  @RootPermissions
  @PostMapping("/" + AouActionName.CLEAN_INDEXES)
  public HttpEntity<Result> cleanIndexes() {
    SolidifyEventPublisher.getPublisher().publishEvent(new CleanIndexesMessage());
    final Result result = new Result();
    result.setStatus(Result.ActionStatus.EXECUTED);
    result.setMesssage("Indexes will be cleaned from deleted publications");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /******************************************************************************************************/

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.GET_CONTACTABLE_CONTRIBUTORS)
  @EveryonePermissions
  public HttpEntity<List<ContactableContributor>> getContributorsContactableByEmail(@PathVariable String id) {
    List<ContactableContributor> contactableContributors = this.contactService.getContributorsContactableByEmail(id);
    return new ResponseEntity<>(contactableContributors, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.CONTACT_CONTRIBUTOR)
  @EveryonePermissions
  public HttpEntity<Result> contactContributor(@PathVariable String id, @RequestBody PublicationContactMessage contactMessage) {
    this.contactService.sendContactMessage(id, contactMessage);
    final Result result = new Result();
    result.setStatus(Result.ActionStatus.EXECUTED);
    result.setMesssage("Contact message sent to contributor");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.ASK_CORRECTION)
  @EveryonePermissions
  public HttpEntity<Result> askForCorrection(@PathVariable String id, @RequestBody PublicationContactMessage contactMessage) {
    this.contactService.sendAskCorrectionMessage(id, contactMessage);
    final Result result = new Result();
    result.setStatus(Result.ActionStatus.EXECUTED);
    result.setMesssage("Correction asking message sent to admin");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /******************************************************************************************************/

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.EXPORT_TO_ORCID)
  @PreAuthorize("@publicationPermissionService.isAllowed(#id, 'EXPORT_TO_ORCID')")
  public HttpEntity<Result> exportToOrcid(@PathVariable String id) {
    Person person = this.personService.getLinkedPerson(SecurityContextHolder.getContext().getAuthentication());
    final Result result = new Result();
    try {
      this.orcidSynchronizationService.sendToOrcidProfile(person, id);
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage(this.messageService.get("publication.orcid.sent_to_your_profile"));
    } catch (PublicationAlreadyExistsInOrcidProfileException e) {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("publication.orcid.already_exists_in_profile"));
    } catch (PublicationWithoutExternalIdentifierException e) {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("publication.orcid.external_id_required"));
    } catch (PublicationTooOldForOrcidException e) {
      result.setStatus(Result.ActionStatus.NON_APPLICABLE);
      result.setMesssage(this.messageService.get("publication.orcid.too_old"));
    }
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  /******************************************************************************************************/

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listVersions()).withRel(ActionName.VALUES));
    w.add(linkTo(methodOn(this.getClass()).schema(AouMetadataVersion.getDefaultVersion().getVersion())).withRel(ResourceName.SCHEMA));
  }

}
