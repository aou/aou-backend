/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonValidationRightStructureController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.Relation3TiersController;
import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.settings.ValidationRight;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.security.CurrentUserPermissions;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PEOPLE + SolidifyConstants.URL_PARENT_ID + ResourceName.VALIDATION_RIGHTS_STRUCTURES)
public class PersonValidationRightStructureController extends Relation3TiersController<Person, Structure, PublicationSubtype, ValidationRight> {

  /**
   * Return the list of validation rights for the given person
   *
   * @param parentid
   * @param pageable
   * @return
   */
  @Override
  @CurrentUserPermissions
  public HttpEntity<RestCollection<Relation3TiersChildDTO>> list(@PathVariable String parentid, @ModelAttribute Structure filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  /**
   * Get the list of validatable subtypes for the given person and structure
   *
   * @param parentid
   * @param id
   * @return
   */
  @Override
  @CurrentUserPermissions
  public HttpEntity<Relation3TiersChildDTO> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  /**
   * Add a list of validatable subtypes for the given person and structure
   *
   * @param parentid
   * @param id
   * @param subtypeIds
   * @return
   */
  @Override
  public HttpEntity<List<ValidationRight>> create(@PathVariable String parentid, @PathVariable String id, @RequestBody String[] subtypeIds) {
    return super.create(parentid, id, subtypeIds);
  }

  /**
   * Add validation rights on all subtypes for the given person on the given list of structures
   *
   * @param parentid
   * @param structureIds
   * @return
   */
  @Override
  public HttpEntity<List<ValidationRight>> create(@PathVariable String parentid, @RequestBody String[] structureIds) {
    return super.create(parentid, structureIds);
  }

  /**
   * Delete all rights referencing the given person and the given list of structures
   *
   * @param parentid
   * @param structureIds
   * @return
   */
  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @RequestBody(required = false) String[] structureIds) {
    return super.delete(parentid, structureIds);
  }

  /**
   * Delete a list of subtypes for the given person and structure
   *
   * @param parentid
   * @param id
   * @param subtypeIds
   * @return
   */
  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id,
          @RequestBody(required = false) String[] subtypeIds) {
    return super.delete(parentid, id, subtypeIds);
  }
}
