/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationStructureController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.Relation2TiersController;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_PUBLICATIONS + SolidifyConstants.URL_PARENT_ID + ResourceName.STRUCTURES)
public class PublicationStructureController extends Relation2TiersController<Publication, Structure, PublicationStructure> {

  @Override
  @PostMapping(SolidifyConstants.URL_ID)
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'ADD_DEPOSIT_VALIDATION_STRUCTURE')")
  public HttpEntity<JoinResourceContainer<PublicationStructure>> create(@PathVariable String parentid, @PathVariable String id,
          @RequestBody PublicationStructure publicationStructure) {
    if (publicationStructure.getLinkType() == PublicationStructure.LinkType.VALIDATION) {
      return super.create(parentid, id, publicationStructure);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              this.messageService.get("deposit.error.structure.endpoint_can_only_be_used_for_validation_structures"));
    }
  }

  @Override
  @PostMapping
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'ADD_DEPOSIT_VALIDATION_STRUCTURE')")
  public HttpEntity<List<JoinResourceContainer<PublicationStructure>>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    return super.create(parentid, ids);
  }

  @Override
  @PatchMapping(SolidifyConstants.URL_ID)
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'UPDATE_DEPOSIT_VALIDATION_STRUCTURE')")
  public HttpEntity<JoinResourceContainer<PublicationStructure>> update(@PathVariable String parentid, @PathVariable String id,
          @RequestBody PublicationStructure pStructure) {
    throw new SolidifyRuntimeException("Method not implemented");
  }

  @Override
  @DeleteMapping(SolidifyConstants.URL_ID)
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'DELETE_DEPOSIT_VALIDATION_STRUCTURE')")
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    PublicationStructure publicationStructure = this.findPublicationStructure(parentid, id);
    if (publicationStructure.getLinkType() == PublicationStructure.LinkType.VALIDATION) {
      return super.delete(parentid, id);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              this.messageService.get("deposit.error.structure.endpoint_can_only_be_used_for_validation_structures"));
    }
  }

  @Override
  @DeleteMapping
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'DELETE_DEPOSIT_VALIDATION_STRUCTURE')")
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    // check that publication structures are all of type VALIDATION
    for (String id : ids) {
      PublicationStructure publicationStructure = this.findPublicationStructure(parentid, id);
      if (publicationStructure.getLinkType() != PublicationStructure.LinkType.VALIDATION) {
        throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
                this.messageService.get("deposit.error.structure.endpoint_can_only_be_used_for_validation_structures"));
      }
    }

    return super.deleteList(parentid, ids);
  }

  @Override
  @PreAuthorize("@publicationPermissionService.isAllowed(#parentid, 'GET')")
  public HttpEntity<JoinResourceContainer<PublicationStructure>> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  @UserPermissions
  public HttpEntity<RestCollection<JoinResourceContainer<PublicationStructure>>> list(@PathVariable String parentid,
          @ModelAttribute Structure filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  private PublicationStructure findPublicationStructure(String parentId, String childId) {
    Structure childItem = this.childItemService.findOne(childId);
    Join2TiersSpecification<PublicationStructure> joinSpecification = this.relationItemService.getJoinSpecification(parentId, childItem);
    joinSpecification.setFilterOnChildResId(true);
    Page<PublicationStructure> page = this.relationItemService.findAllRelations(joinSpecification, Pageable.unpaged());
    if (page.getTotalElements() == 1L) {
      return page.getContent().get(0);
    } else {
      throw new SolidifyResourceNotFoundException("Structure with id '" + childId + "' is not linked to parent '" + parentId + "'");
    }
  }
}
