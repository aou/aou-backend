/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - SystemPropertyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.model.OrcidConfigDTO;
import ch.unige.solidify.model.index.FacetProperties;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.StaticPage;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.settings.StaticPageService;

@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_SYSTEM_PROPERTIES)
public class SystemPropertyController implements ControllerWithHateoasHome {

  private final SystemProperties systemProperties;
  private final IndexFieldAliasService indexFieldAliasService;
  private final StaticPageService staticPageService;
  private final String indexName;

  private static class SystemProperties extends RepresentationModel<SystemProperties> {
    private final String defaultChecksum;
    private final String defaultLicense;
    private final String fileSizeLimit;
    private final int limitMaxContributors;
    private final OrcidConfigDTO orcid;

    private final Map<String, String> defaultContributorRoles;
    private List<FacetProperties> searchFacets;

    private Map<String, String> systemIndexFieldAlias = new HashMap<>();

    private List<StaticPage> staticPages;

    private String[] orcidContributorRolesToSendToProfile;

    public SystemProperties(AouProperties aouProperties, SolidifyProperties solidifyProperties) {
      this.defaultChecksum = aouProperties.getParameters().getDefaultChecksum();
      this.defaultLicense = aouProperties.getParameters().getDefaultLicense();
      this.fileSizeLimit = String.valueOf(aouProperties.getParameters().getFileSizeLimit().toBytes());
      this.limitMaxContributors = aouProperties.getMetadata().getImports().getLimitMaxContributors();
      this.defaultContributorRoles = aouProperties.getMetadata().getDefault().getContributorRolesByDepositSubtype();
      this.orcid = new OrcidConfigDTO(solidifyProperties.getOrcid());
      this.orcidContributorRolesToSendToProfile = aouProperties.getOrcid().getRolesToSendToOrcidProfile();
    }

    public String getDefaultChecksum() {
      return this.defaultChecksum;
    }

    public String getDefaultLicense() {
      return this.defaultLicense;
    }

    public String getFileSizeLimit() {
      return this.fileSizeLimit;
    }

    public int getLimitMaxContributors() {
      return this.limitMaxContributors;
    }

    public OrcidConfigDTO getOrcid() {
      return this.orcid;
    }

    public String[] getOrcidContributorRolesToSendToProfile() {
      return this.orcidContributorRolesToSendToProfile;
    }

    public Map<String, String> getDefaultContributorRoles() {
      return this.defaultContributorRoles;
    }

    public List<FacetProperties> getSearchFacets() {
      return this.searchFacets;
    }

    public void setSearchFacets(List<FacetProperties> searchFacets) {
      this.searchFacets = searchFacets;
    }

    public Map<String, String> getSystemIndexFieldAlias() {
      return this.systemIndexFieldAlias;
    }

    public List<StaticPage> getStaticPages() {
      return this.staticPages;
    }

    public void setStaticPages(List<StaticPage> staticPages) {
      this.staticPages = staticPages;
    }
  }

  public SystemPropertyController(AouProperties aouProperties, SolidifyProperties solidifyProperties, StaticPageService staticPageService,
          IndexFieldAliasService indexFieldAliasService) {
    this.indexFieldAliasService = indexFieldAliasService;
    this.staticPageService = staticPageService;
    this.systemProperties = new SystemProperties(aouProperties, solidifyProperties);
    this.systemProperties.add(linkTo(methodOn(this.getClass()).home()).withSelfRel());
    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
  }

  @GetMapping
  public HttpEntity<SystemProperties> systemProperties() {
    this.systemProperties.setSearchFacets(this.indexFieldAliasService.getFacetProperties(this.indexName));
    this.systemProperties.getSystemIndexFieldAlias().put(AouConstants.INDEX_FIELD_OPEN_ACCESS, AouConstants.INDEX_FIELD_OPEN_ACCESS);
    this.systemProperties.getSystemIndexFieldAlias()
            .put(AouConstants.INDEX_FIELD_PRINCIPAL_FILE_ACCESS_LEVEL, AouConstants.INDEX_FIELD_PRINCIPAL_FILE_ACCESS_LEVEL);
    this.systemProperties.setStaticPages(this.staticPageService.getStaticPages());
    this.systemProperties.removeLinks();
    this.systemProperties.add(linkTo(this.getClass()).withSelfRel());
    return new ResponseEntity<>(this.systemProperties, HttpStatus.OK);
  }
}
