/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonStructureController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.rest.RestCollection;

import ch.unige.aou.business.StructureService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.security.CurrentUserPermissions;

@CurrentUserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PEOPLE + SolidifyConstants.URL_PARENT_ID + ResourceName.STRUCTURES)
public class PersonStructureController extends AssociationController<Person, Structure> {
  @Override
  public HttpEntity<Structure> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  public HttpEntity<RestCollection<Structure>> list(@PathVariable String parentid, @ModelAttribute Structure structure, Pageable pageable) {
    return super.list(parentid, structure, pageable);
  }

  @Override
  public HttpEntity<List<Structure>> create(@PathVariable String parentid, @RequestBody String[] structureIds) {
    HttpEntity<List<Structure>> result = super.create(parentid, structureIds);
    SolidifyEventPublisher.getPublisher().publishEvent(new ResourceCacheMessage(Person.class, parentid));
    return result;
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    ResponseEntity<Void> result = super.delete(parentid, id);
    SolidifyEventPublisher.getPublisher().publishEvent(new ResourceCacheMessage(Person.class, parentid));
    return result;
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    ResponseEntity<Void> result = super.deleteList(parentid, ids);
    SolidifyEventPublisher.getPublisher().publishEvent(new ResourceCacheMessage(Person.class, parentid));
    return result;
  }

  @Override
  protected String getParentFieldName() {
    return "people";
  }

  @Override
  public Structure getEmptyChildResourceObject() {
    return new Structure();
  }

  @Override
  protected boolean addChildOnParent(Person person, Structure structure) {
    ((StructureService) this.subResourceService).duplicatesInStructures(person, structure);
    return person.addStructure(structure);
  }

  @Override
  protected boolean removeChildFromParent(Person person, Structure structure) {
    return person.removeStructure(structure);
  }

}
