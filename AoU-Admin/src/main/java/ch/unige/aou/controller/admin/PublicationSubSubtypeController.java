/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PublicationSubSubtypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.CompositionController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PUBLICATION_SUBTYPES + SolidifyConstants.URL_PARENT_ID + ResourceName.PUBLICATION_SUB_SUBTYPES)
public class PublicationSubSubtypeController extends CompositionController<PublicationSubtype, PublicationSubSubtype> {

  @Override
  @UserPermissions
  public HttpEntity<RestCollection<PublicationSubSubtype>> list(@PathVariable String parentid, @ModelAttribute PublicationSubSubtype filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  @UserPermissions
  public HttpEntity<PublicationSubSubtype> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  protected PublicationSubtype getParentResourceProperty(PublicationSubSubtype subResource) {
    return subResource.getPublicationSubtype();
  }

  @Override
  protected void setParentResourceProperty(PublicationSubSubtype subResource, PublicationSubtype parentResource) {
    subResource.setPublicationSubtype(parentResource);
  }
}
