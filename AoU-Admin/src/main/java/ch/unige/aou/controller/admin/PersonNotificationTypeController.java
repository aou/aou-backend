/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PersonNotificationTypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.AssociationController;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.rest.RestCollection;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.NotificationTypeService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.message.EmailMessage.EmailTemplate;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.security.CurrentUserPermissions;

@CurrentUserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_PEOPLE + SolidifyConstants.URL_PARENT_ID + ResourceName.NOTIFICATION_TYPES)
public class PersonNotificationTypeController extends AssociationController<Person, NotificationType> {

  private final UserService userService;
  private boolean sendEmailOnNotificationTypesSubscriptionChange;

  public PersonNotificationTypeController(AouProperties aouProperties, UserService userService) {
    this.userService = userService;
    this.sendEmailOnNotificationTypesSubscriptionChange = aouProperties.getParameters().isEmailOnNotificationTypesSubscriptionChangeEnabled();
  }

  @Override
  public HttpEntity<RestCollection<NotificationType>> list(@PathVariable String parentid, @ModelAttribute NotificationType filterItem,
          Pageable pageable) {
    return super.list(parentid, filterItem, pageable);
  }

  @Override
  public HttpEntity<NotificationType> get(@PathVariable String parentid, @PathVariable String id) {
    return super.get(parentid, id);
  }

  @Override
  public HttpEntity<List<NotificationType>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    HttpEntity<List<NotificationType>> result = super.create(parentid, ids);
    Map<String, Object> parameters = new HashMap<>();
    parameters.put(AouConstants.NOTIFICATION_IDS, ids);
    this.sendEmailMessage(EmailTemplate.NEW_SUBSCRIPTION, parameters);
    return result;
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    if (((NotificationTypeService) this.subResourceService).getAllMandatoryNotificationTypes().contains(id)) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              this.messageService.get("notificationType.error.delete_mandatory_notification", new Object[] { id }));
    }
    ResponseEntity<Void> result = super.delete(parentid, id);
    Map<String, Object> parameters = new HashMap<>();
    parameters.put(AouConstants.NOTIFICATION_IDS, new String[] { id });
    this.sendEmailMessage(EmailTemplate.REMOVE_SUBSCRIPTION, parameters);
    return result;
  }

  @Override
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    for (String id : ids) {
      if (((NotificationTypeService) this.subResourceService).getAllMandatoryNotificationTypes().contains(id)) {
        throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
                this.messageService.get("notificationType.error.delete_mandatory_notification", new Object[] { id }));
      }
    }
    ResponseEntity<Void> result = super.deleteList(parentid, ids);
    Map<String, Object> parameters = new HashMap<>();
    parameters.put(AouConstants.NOTIFICATION_IDS, ids);
    this.sendEmailMessage(EmailTemplate.REMOVE_SUBSCRIPTION, parameters);
    return result;
  }

  @Override
  protected String getParentFieldName() {
    return "subscribers";
  }

  @Override
  public NotificationType getEmptyChildResourceObject() {
    return new NotificationType();
  }

  @Override
  protected boolean addChildOnParent(Person person, NotificationType notificationType) {
    return person.addNotificationType(notificationType);
  }

  @Override
  protected boolean removeChildFromParent(Person person, NotificationType notificationType) {
    return person.removeNotificationType(notificationType);
  }

  private void sendEmailMessage(EmailTemplate template, Map<String, Object> parameters) {
    if (this.sendEmailOnNotificationTypesSubscriptionChange) {
      SolidifyEventPublisher.getPublisher().publishEvent(new EmailMessage(this.userService.getCurrentUser().getEmail(), template, parameters));
    }
  }

}
