/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ExportMetadataController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.model.security.User;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.controller.index.IndexDataSearchController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.service.IndexFieldAliasInfoProvider;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.metadata.exports.UpdateMetadataService;

@AdminPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_EXPORT_METADATA)
public class UpdateMetadataController extends IndexDataSearchController<PublicationIndexEntry> {

  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;
  private final UpdateMetadataService updateMetadataService;
  private final PersonService personService;
  private final UserService userService;
  private final String indexPublishedPublications;
  private final String exportDataUrl;


  public UpdateMetadataController(IndexResourceService<String, PublicationIndexEntry> indexResourceService,
          IndexingService<PublicationIndexEntry> indexingService, IndexFieldAliasInfoProvider indexFieldAliasInfoProvider,
          UpdateMetadataService updateMetadataService, AouProperties aouProperties, PersonService personService,
                                  UserService userService) {
    super(indexingService, indexFieldAliasInfoProvider);
    this.indexResourceService = indexResourceService;
    this.indexPublishedPublications = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.updateMetadataService = updateMetadataService;
    this.exportDataUrl = aouProperties.getParameters().getExportDataUrlTemplate();
    this.personService = personService;
    this.userService = userService;
  }

  @PostMapping(AouActionName.EXPORT)
  public HttpEntity<String> exportPublications(@RequestBody List<SearchCondition> searchConditions) {
    List<String> archiveIds = new ArrayList<>();
    Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE,
            Sort.by(AouConstants.INDEX_FIELD_FIRST_VALIDATION_DATE).descending());
    FacetPage<PublicationIndexEntry> page;
    searchConditions = this.replaceAliasesByIndexFieldName(searchConditions);
    FieldsRequest fieldsRequest = new FieldsRequest();
    fieldsRequest.getIncludes().add(AouConstants.INDEX_FIELD_ARCHIVE_ID);
    do {
      page = this.indexResourceService.search(this.indexPublishedPublications, searchConditions, null, pageable, fieldsRequest);
      if (page.getTotalPages() == 0) {
        throw new SolidifyRuntimeException("No results found with the query provided");
      }
      archiveIds.addAll(page.getContent().stream().map(PublicationIndexEntry::getArchiveId).toList());
      pageable = page.nextPageable();
    } while (page.hasNext());

    // Generate URl that will be sent to check later on the file
    String idForURL = StringTool.generateResId();
    String downloadUrl = this.exportDataUrl.replace("{id}", idForURL);
    this.updateMetadataService.exportMetadata(archiveIds, idForURL);

    return new ResponseEntity<>(downloadUrl, HttpStatus.OK);
  }

  @GetMapping("/{id}/" + AouActionName.DOWNLOAD)
  public HttpEntity<StreamingResponseBody> downloadExportFile(@PathVariable String id) throws IOException {
    Path filePath = this.updateMetadataService.checkIfFilePathExists(id);
    InputStream fileStream = filePath.toUri().toURL().openStream();
    long fileSize = FileTool.getSize(filePath);
    return this.buildDownloadResponseEntity(fileStream, filePath.toString().substring(filePath.toString().lastIndexOf('/') + 1),
            MediaType.TEXT_XML_VALUE, fileSize);
  }

  @Override
  protected String getIndex() {
    return this.indexPublishedPublications;
  }

  @PostMapping(AouActionName.IMPORT)
  public ResponseEntity<Void> importPublicationsMetadata(@RequestBody String metadata) {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    final User userImporting = this.userService.findByExternalUid(authentication.getName());

    this.updateMetadataService.importPublications(metadata, userImporting);
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
