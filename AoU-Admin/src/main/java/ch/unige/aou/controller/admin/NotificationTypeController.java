/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - NotificationTypeController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.security.NoOnePermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.business.NotificationTypeService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@RestController
@NoOnePermissions
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_NOTIFICATION_TYPES)
public class NotificationTypeController extends ResourceController<NotificationType> {

  @UserPermissions
  @GetMapping("/" + AouActionName.LIST_NOTIFICATIONS_TYPES_MANDATORY)
  public HttpEntity<List<String>> listNotificationTypesMandatory() {
    return new ResponseEntity<>(((NotificationTypeService) this.itemService).getAllMandatoryNotificationTypes(), HttpStatus.OK);
  }

}
