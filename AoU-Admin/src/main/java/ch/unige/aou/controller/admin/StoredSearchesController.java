/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - StoredSearchesController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.StoredSearchService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.search.StoredSearch;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AdminController.class)
@RequestMapping(UrlPath.ADMIN_STORED_SEARCHES)
public class StoredSearchesController extends ResourceController<StoredSearch> {

  private final PersonService personService;

  public StoredSearchesController(PersonService personService) {
    this.personService = personService;
  }

  @Override
  @PreAuthorize("@storedSearchPermissionService.isAllowed(#id, 'GET')")
  public HttpEntity<StoredSearch> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @AdminPermissions
  public HttpEntity<RestCollection<StoredSearch>> list(@ModelAttribute StoredSearch storedSearch, Pageable pageable) {
    return super.list(storedSearch, pageable);
  }

  @GetMapping(AouActionName.LIST_MY_STORED_SEARCHES)
  public HttpEntity<RestCollection<StoredSearch>> listMyStoredSearches(@ModelAttribute StoredSearch storedSearch, Pageable pageable) {
    String personId = this.personService.getLinkedPersonId(SecurityContextHolder.getContext().getAuthentication());
    storedSearch.setCreator(new Person());
    storedSearch.getCreator().setResId(personId);
    return this.list(storedSearch, pageable);
  }

  @Override
  public HttpEntity<StoredSearch> create(@RequestBody StoredSearch storedSearch) {
    return super.create(storedSearch);
  }

  @Override
  @PreAuthorize("@storedSearchPermissionService.isAllowed(#id, 'DELETE')")
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  @PreAuthorize("@storedSearchPermissionService.isAllowed(#ids, 'DELETE')")
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @Override
  @PreAuthorize("@storedSearchPermissionService.isAllowed(#id, 'UPDATE')")
  public HttpEntity<StoredSearch> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    StoredSearch savedSearch = ((StoredSearchService) this.itemService).updateWithCriteria(id, updateMap);
    this.addLinks(savedSearch);
    return new ResponseEntity<>(savedSearch, HttpStatus.OK);
  }
}
