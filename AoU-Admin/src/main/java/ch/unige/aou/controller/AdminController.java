/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - AdminController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.PostConstruct;

import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.RootPermissions;

import ch.unige.aou.business.ApplicationRoleService;
import ch.unige.aou.business.ContributorRoleService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.EventService;
import ch.unige.aou.business.EventTypeService;
import ch.unige.aou.business.InstitutionService;
import ch.unige.aou.business.LabeledLanguageService;
import ch.unige.aou.business.LanguageService;
import ch.unige.aou.business.LicenseGroupService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.NotificationTypeService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.RoleService;
import ch.unige.aou.business.ScheduledTaskService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.CacheService;

@RestController
@ConditionalOnProperty(prefix = "aou.module.admin", name = "enable")
@RequestMapping(UrlPath.ADMIN)
public class AdminController extends ModuleController {

  private final LabeledLanguageService labeledLanguageService;
  private final LanguageService languageService;
  private final LicenseService licenceService;
  private final LicenseGroupService licenceGroupService;
  private final InstitutionService institutionService;
  private final PersonService personService;
  private final UserService userService;
  private final ApplicationRoleService applicationRoleService;
  private final RoleService roleService;
  private final PublicationService publicationService;
  private final PublicationTypeService publicationTypeService;
  private final PublicationSubtypeService publicationSubtypeService;
  private final PublicationSubSubtypeService publicationSubSubtypeService;
  private final DocumentFileTypeService documentFileTypeService;
  private final ContributorRoleService contributorRoleService;
  private final NotificationTypeService notificationTypeService;
  private final EventTypeService eventTypeService;
  private final EventService eventService;
  private final ScheduledTaskService scheduledTaskService;
  private final StructureService structureService;
  private final CacheService cacheService;

  private final List<AouProperties.Test.User> users;
  private final List<AouProperties.ScheduledTaskConfig> scheduledTaskConfigs;
  private final boolean isInit;
  private final boolean isBuilding;
  private final boolean isGenerateData;

  AdminController(AouProperties aouProperties, LabeledLanguageService labeledLanguageService,
          LanguageService languageService, LicenseService licenceService,
          LicenseGroupService licenceGroupService, InstitutionService institutionService,
          PersonService personService, UserService userService, ApplicationRoleService applicationRoleService,
          PublicationTypeService publicationTypeService, PublicationService publicationService,
          PublicationSubtypeService publicationSubtypeService, RoleService roleService, DocumentFileTypeService documentFileTypeService,
          ContributorRoleService contributorRoleService, NotificationTypeService notificationTypeService, EventTypeService eventTypeService,
          EventService eventService, PublicationSubSubtypeService publicationSubSubtypeService, ScheduledTaskService scheduledTaskService,
          StructureService structureService, CacheService cacheService) {
    super(ModuleName.ADMIN);

    this.labeledLanguageService = labeledLanguageService;
    this.languageService = languageService;
    this.licenceService = licenceService;
    this.licenceGroupService = licenceGroupService;
    this.institutionService = institutionService;
    this.personService = personService;
    this.userService = userService;
    this.applicationRoleService = applicationRoleService;
    this.publicationTypeService = publicationTypeService;
    this.publicationSubtypeService = publicationSubtypeService;
    this.publicationService = publicationService;
    this.publicationSubSubtypeService = publicationSubSubtypeService;
    this.roleService = roleService;
    this.documentFileTypeService = documentFileTypeService;
    this.contributorRoleService = contributorRoleService;
    this.notificationTypeService = notificationTypeService;
    this.eventTypeService = eventTypeService;
    this.eventService = eventService;
    this.scheduledTaskService = scheduledTaskService;
    this.structureService = structureService;
    this.cacheService = cacheService;

    this.users = aouProperties.getTest().getUsers();
    this.scheduledTaskConfigs = aouProperties.getScheduledTasks();
    this.isInit = aouProperties.getData().isInit();
    this.isBuilding = aouProperties.getTest().isBuilding();
    this.isGenerateData = aouProperties.getTest().isGenerateData();
  }

  @RootPermissions
  @PostMapping("/" + AouActionName.CLEAN_CACHES)
  public HttpEntity<Result> cleanAllCaches() {
    this.cacheService.cleanAllCaches();
    final Result result = new Result();
    result.setStatus(Result.ActionStatus.EXECUTED);
    result.setMesssage("All caches were cleaned");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  @PostConstruct
  public void initDefaultApplicationData() {
    // Initialize required data
    this.languageService.initDefaultData();
    this.labeledLanguageService.initDefaultData();
    this.applicationRoleService.initDefaultData();
    this.roleService.initDefaultData();
    this.eventTypeService.initDefaultData();
    this.notificationTypeService.initDefaultData();
    this.publicationTypeService.initDefaultData();
    this.publicationSubtypeService.initDefaultData();
    this.publicationSubSubtypeService.initDefaultData();

    // Initialize optional data
    if (this.isInit) {
      this.institutionService.initDefaultData();
      this.contributorRoleService.initDefaultData();
      for (AouProperties.ScheduledTaskConfig scheduledTaskConfig : this.scheduledTaskConfigs) {
        this.scheduledTaskService.createScheduledTaskFromConfig(scheduledTaskConfig);
      }
      this.licenceGroupService.initDefaultData();
    }

    if (!this.isBuilding) {
      this.documentFileTypeService.initDefaultData();
      this.licenceService.initDefaultData();
      this.structureService.initDefaultData();
    }

    // Initialize data for documentation generation
    if (this.isBuilding) {
      for (final AouProperties.Test.User testUser : this.users) {
        final Person testPerson = this.personService.initDefaultData(testUser);
        this.userService.createTestUser(testUser, testPerson);
      }
    }

    // Initialize test data
    if (this.isGenerateData) {
      int index = 0;
      for (final Person testPerson : this.personService.findTestPeople()) {
        if (index == 0) {
          this.publicationService.initDefaultData(testPerson);
          this.eventService.initDefaultData(testPerson);
        }
        index++;
      }
    }
  }
}
