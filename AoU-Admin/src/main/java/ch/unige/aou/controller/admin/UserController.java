/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - UserController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.admin;

import java.text.ParseException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nimbusds.jwt.SignedJWT;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Result.ActionStatus;
import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.business.UserService;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.security.UserDisplayableInfos;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.UserSynchronizationService;
import ch.unige.aou.specification.UserSpecification;

@RestController
@ConditionalOnBean(AdminController.class)
@AdminPermissions
@SuppressWarnings("squid:S4684")
@RequestMapping(UrlPath.ADMIN_USERS)
public class UserController extends ResourceController<User> {
  static final Logger log = LoggerFactory.getLogger(UserController.class);

  private final UserSynchronizationService userSynchronizationService;

  public UserController(UserSynchronizationService userSynchronizationService) {
    this.userSynchronizationService = userSynchronizationService;
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

  @UserPermissions
  @GetMapping("/" + AouActionName.AUTHENTICATED)
  public HttpEntity<User> getAuthenticatedUser() {
    final String userId = this.getAuthenticatedUserExternalUid();
    final User user = ((UserService) this.itemService).findByExternalUid(userId);
    this.addLinks(user);
    return new ResponseEntity<>(user, HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping("/" + AouActionName.GET_USER_DISPLAYABLE_INFOS + "/{externalUid}")
  public HttpEntity<UserDisplayableInfos> getUserDisplayableInfos(@PathVariable String externalUid) {
    final User user = ((UserService) this.itemService).findByExternalUid(externalUid);
    UserDisplayableInfos userInfos = new UserDisplayableInfos(user);
    return new ResponseEntity<>(userInfos, HttpStatus.OK);
  }

  @PostMapping("/" + AouActionName.REVOKE_ALL_TOKENS + "/" + UrlPath.AUTH_USER_ID)
  public ResponseEntity<Object> revokeAllToken(@PathVariable String externalUid) {

    final User user = ((UserService) this.itemService).findByExternalUid(externalUid);
    if (user == null) {
      return ResponseEntity.notFound().build();
    }
    ((UserService) this.itemService).revokeAccessAndRefreshTokens(user);
    return ResponseEntity.ok().build();
  }

  @UserPermissions
  @PostMapping("/" + AouActionName.REVOKE_MY_TOKENS)
  public ResponseEntity<Object> revokeMyToken(@RequestHeader("Authorization") String authorizationHeader) throws ParseException {
    final String token = authorizationHeader.substring(authorizationHeader.indexOf(' ') + 1);
    final String externalUid = SignedJWT.parse(token).getJWTClaimsSet().getClaim("user_name").toString();
    final User user = ((UserService) this.itemService).findByExternalUid(externalUid);
    ((UserService) this.itemService).revokeAccessAndRefreshTokens(user);
    return ResponseEntity.ok().build();
  }

  // Change role permissions are checked by the authorization server
  @AdminPermissions
  @Override
  public HttpEntity<User> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    // get existing user
    final User existingUser = this.itemService.findOne(id);
    if (updateMap == null) {
      return ResponseEntity.badRequest().build();
    }
    // Check if role changed
    String updateApplicationRoleId = ((UserService) this.itemService).getApplicationRoleId(updateMap);
    if (updateApplicationRoleId != null && !existingUser.getApplicationRole().getResId().equals(updateApplicationRoleId)) {
      ((UserService) this.itemService).updateUserRole(existingUser.getExternalUid(), updateApplicationRoleId);
    }
    return super.update(id, updateMap);
  }

  @Override
  public HttpEntity<RestCollection<User>> list(@ModelAttribute User search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @GetMapping(params = "onlyWithValidationRights")
  public HttpEntity<RestCollection<User>> list(@RequestParam boolean onlyWithValidationRights, @ModelAttribute User search, Pageable pageable) {
    final UserSpecification spec = (UserSpecification) this.itemService.getSpecification(search);
    spec.setOnlyWithValidationRights(onlyWithValidationRights);
    final Page<User> listItem = this.itemService.findAll(spec, pageable);
    this.setResourceLinks(listItem);
    final RestCollection<User> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @Override
  public HttpEntity<User> get(@PathVariable String id) {
    return super.get(id);
  }

  /**
   * For all users:
   * - retrieve application role from authorization server
   * - retrieve user login information from authorization server
   * - synchronize (bi-directional) ORCID with authorization server
   *
   * @return the result of the synchronize action
   */
  @RootPermissions
  @PostMapping(AouActionName.SYNCHRONIZE)
  public HttpEntity<Result> synchronize() {
    Result result = new Result("synchronize");
    try {
      this.userSynchronizationService.synchronizeUsersWithAuthServer();
      result.setStatus(Result.ActionStatus.EXECUTED);
      result.setMesssage("User informations synchronized");
    } catch (RuntimeException e) {
      log.error("In synchronising user information", e);
      result.setStatus(Result.ActionStatus.NOT_EXECUTED);
      result.setMesssage(e.getMessage());
    }
    return this.returnHttpResult(result);
  }

  @TrustedUserPermissions
  @PostMapping("/" + AouActionName.NEW_USER + SolidifyConstants.URL_EXTERNAL_UID)
  public HttpStatus createNewUser(@PathVariable String externalUid) {
    ((UserService) this.itemService).createNewUser(externalUid);
    return HttpStatus.OK;
  }

  @PostMapping(SolidifyConstants.URL_ID_PLUS_SEP + AouActionName.MERGE)
  public HttpEntity<Result> mergeUsers(@PathVariable String id, @RequestBody String idToMerge) {
    boolean isMerge = ((UserService) this.itemService).mergeUsersAndOrcid(id, idToMerge);
    final Result res = new Result(id);
    if (isMerge) {
      res.setStatus(ActionStatus.EXECUTED);
      res.setMesssage("Users are merged");
    } else {
      res.setStatus(ActionStatus.NON_APPLICABLE);
      res.setMesssage("Both user are linked already to the same person, they are not merged.");
    }
    return new ResponseEntity<>(res, HttpStatus.OK);
  }

  @UserPermissions
  @GetMapping("/" + AouActionName.IS_USER_MEMBER_OF_AUTHORIZED_INSTITUTION)
  public HttpEntity<Boolean> isUserMemberOfAuthorizedInstitution() {
    final Boolean isMember = ((UserService) this.itemService).isUserMemberOfAuthorizedInstitutions();
    return new ResponseEntity(isMember, HttpStatus.OK);
  }
}
