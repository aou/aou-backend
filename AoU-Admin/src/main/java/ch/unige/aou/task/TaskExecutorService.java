/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - TaskExecutorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.task;

import static java.util.stream.Collectors.groupingBy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.NotificationService;
import ch.unige.aou.business.OrcidSynchronizationService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AdminController;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.message.PublicationIndexingMessage;
import ch.unige.aou.message.PublicationMessage;
import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.orcid.ImportedIdentifiers;
import ch.unige.aou.model.orcid.OrcidSynchronization;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.MetadataDifference;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.rest.ResearchGroupDTO;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.service.AouService;
import ch.unige.aou.service.SwissNationalLibraryService;
import ch.unige.aou.specification.ResearchGroupSpecification;

@Service
@ConditionalOnBean(AdminController.class)
public class TaskExecutorService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(TaskExecutorService.class);

  protected final NotificationService notificationService;
  private final PublicationService publicationService;
  private final PersonService personService;
  private final UserService userService;
  private final ResearchGroupService researchGroupService;
  private final DocumentFileService documentFileService;
  private final SwissNationalLibraryService swissNationalLibraryService;
  private final OrcidSynchronizationService orcidSynchronizationService;

  private final int delaySinceLastCheck;
  private final String[] emailsToSendGeneralNotifications;
  private final int daysSinceLastCheckForResearchGroups;

  private final int daysToKeepPublicationsInEditionWithoutChanges;

  public TaskExecutorService(MessageService messageService, PublicationService publicationService, NotificationService notificationService,
          AouProperties aouProperties, PersonService personService, UserService userService, ResearchGroupService researchGroupService,
          DocumentFileService documentFileService, SwissNationalLibraryService swissNationalLibraryService,
          OrcidSynchronizationService orcidSynchronizationService) {
    super(messageService);
    this.publicationService = publicationService;
    this.delaySinceLastCheck = aouProperties.getDelaySinceLastCheck();
    this.notificationService = notificationService;
    this.personService = personService;
    this.userService = userService;
    this.researchGroupService = researchGroupService;
    this.emailsToSendGeneralNotifications = aouProperties.getParameters().getEmailsToSendGeneralNotifications();
    this.daysSinceLastCheckForResearchGroups = aouProperties.getParameters().getDaysSinceLastCheckForNewResearchGroups();
    this.daysToKeepPublicationsInEditionWithoutChanges = aouProperties.getParameters().getDaysToKeepPublicationsInEditionWithoutChanges();
    this.documentFileService = documentFileService;
    this.swissNationalLibraryService = swissNationalLibraryService;
    this.orcidSynchronizationService = orcidSynchronizationService;
  }

  /**
   * Group notifications by person and keep them only if the related publication has still the given status. (we don't send a notification if
   * the publication status has changed since the notification was created as it wouldn't make sense anymore)
   *
   * @param notificationsToSend
   * @param filterStatuses
   * @return
   */
  protected Map<String, List<Notification>> getFilteredNotificationsByPersonId(List<Notification> notificationsToSend,
          List<Publication.PublicationStatus> filterStatuses) {

    Map<String, List<Notification>> notificationsByPersonId = new HashMap<>();
    for (Notification notificationToSend : notificationsToSend) {

      // keep the notification only if it has the specified filter status
      if (filterStatuses.stream().anyMatch(status -> status == notificationToSend.getEvent().getPublication().getStatus())) {
        String recipientId = notificationToSend.getRecipient().getResId();
        notificationsByPersonId.computeIfAbsent(recipientId, s -> new ArrayList<>());
        notificationsByPersonId.get(recipientId).add(notificationToSend);
      }
    }
    return notificationsByPersonId;
  }

  /**
   * Create and send an email to the person if he has subscribed to the notification type, with publication ids linked to the notifications.
   * The sending process uses a JMS queue.
   *
   * @param personId
   * @param notifications
   * @param notificationType
   * @param emailTemplate
   */
  private void sendEmailWithPublicationIdsIfUserHasSubscribedToNotificationType(String personId, List<Notification> notifications,
          NotificationType notificationType, EmailMessage.EmailTemplate emailTemplate) {

    if (this.personService.hasSubscribedToNotificationType(personId, notificationType)) {

      // validator's email
      List<User> recipientUsers = this.userService.findByPersonResId(personId);

      // compute a list of unique publication ids to notify (to not include the same publication more than once in an email)
      HashMap<String, Object> parameters = new HashMap<>();
      notifications.stream().map(notification -> notification.getEvent().getPublication().getResId()).distinct()
              .forEach(n -> parameters.put(n, null));

      // send email for each user linked to the person of the notification
      for (User user : recipientUsers) {
        SolidifyEventPublisher.getPublisher().publishEvent(new EmailMessage(user.getEmail(), emailTemplate, parameters));
      }

      // mark all notifications as sent
      OffsetDateTime sentTime = OffsetDateTime.now();
      for (Notification notification : notifications) {
        this.notificationService.updateSentTime(notification.getResId(), sentTime);
        log.info("notification {} sentTime updated with value {}", notification.getResId(), sentTime);
      }
    }
  }

  private void sendEmailToValidatorForForgottenPublications(String personId, List<Publication> value,
          EmailMessage.EmailTemplate emailTemplate) {

    // validator's email
    List<User> recipientUsers = this.userService.findByPersonResId(personId);

    // compute a list of unique publication ids to notify (to not include the same publication more than once in an email)
    HashMap<String, Object> parameters = new HashMap<>();
    value.stream().map(ResourceNormalized::getResId).distinct().forEach(n -> parameters.put(n, null));

    // send email for each user linked to the person of the notification
    for (User user : recipientUsers) {
      SolidifyEventPublisher.getPublisher().publishEvent(new EmailMessage(user.getEmail(), emailTemplate, parameters));
    }
  }

  /********************************************************************************************************************************/

  @Transactional
  public void notifyForgottenPublicationsToValidators() {
    OffsetDateTime offsetDelay = OffsetDateTime.now().minusDays(this.delaySinceLastCheck);
    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(ActionName.CREATION));
    // Need to create new List because call to findPublicationByStatusAndLastUpdate return UnmodifiableCollection
    List<Publication> publications = new ArrayList<>(
            this.publicationService.findPublicationByStatusAndLastUpdate(Publication.PublicationStatus.IN_VALIDATION,
                    offsetDelay, pageable).toList());
    // add the search for publications with "UPDATES_VALIDATION" status
    List<Publication> publicationsInUpdate = new ArrayList<>(this.publicationService.findPublicationByStatusAndLastUpdate(
            Publication.PublicationStatus.UPDATES_VALIDATION,
            offsetDelay, pageable).toList());

    if (!publicationsInUpdate.isEmpty()) {
      publications.addAll(publicationsInUpdate);
    }

    Map<Person, List<Publication>> publicationsByValidator = new HashMap<>();
    for (Publication publication : publications) {
      final Publication managedPublication = this.publicationService.findOne(publication.getResId());
      List<Person> validators = this.personService.getValidators(publication);
      validators.forEach(validator -> {
        if (this.personService.hasSubscribedToNotificationType(validator.getResId(), NotificationType.PUBLICATION_FORGOTTEN_TO_VALIDATE)) {
          publicationsByValidator.computeIfAbsent(validator, k -> new ArrayList<>()).add(managedPublication);
        }
      });
    }

    for (Map.Entry<Person, List<Publication>> entry : publicationsByValidator.entrySet()) {
      this.sendEmailToValidatorForForgottenPublications(entry.getKey().getResId(), entry.getValue(),
              EmailMessage.EmailTemplate.FORGOTTEN_PUBLICATIONS_TO_VALIDATE);
    }
  }

  @Transactional
  public void notifyForgottenPublicationsInProgress() {
    OffsetDateTime offsetDelay = OffsetDateTime.now().minusDays(this.delaySinceLastCheck);
    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(ActionName.CREATION));
    List<Publication> publications = new ArrayList<>(
            this.publicationService.findPublicationByStatusAndLastUpdate(Publication.PublicationStatus.IN_PROGRESS,
                    offsetDelay, pageable).toList());
    //find publication with "Feedback_required" status
    List<Publication> publicationsInFeedback = new ArrayList<>(this.publicationService.findPublicationByStatusAndLastUpdate(
            Publication.PublicationStatus.FEEDBACK_REQUIRED,
            offsetDelay, pageable).toList());

    if (!publicationsInFeedback.isEmpty()) {
      publications.addAll(publicationsInFeedback);
    }

    Map<Person, List<Publication>> publicationsByCreatorOrAuthor = new HashMap<>();
    for (Publication publication : publications) {
      final Publication managedPublication = this.publicationService.findOne(publication.getResId());
      //Add contributor to the list of persons to notify in case he is subscribed to the notification
      Set<Contributor> contributorList = managedPublication.getContributors();
      contributorList.forEach(c -> {
        //Search person linked
        try {
          User user = this.userService.findByExternalUid(CleanTool.getFullUnigeExternalUid(c.getCnIndividu()));
          if (this.personService
                  .hasSubscribedToNotificationType(user.getPerson().getResId(), NotificationType.PUBLICATION_FORGOTTEN_IN_PROGRESS)) {
            publicationsByCreatorOrAuthor.computeIfAbsent(user.getPerson(), k -> new ArrayList<>()).add(managedPublication);
          }
        } catch (NoSuchElementException e) {
          log.debug("No user with cnIndividu {} found", c.getCnIndividu());
        }
      });
      //Add creator to the list of persons to notify in case he is subscribed to the notification
      if (this.personService.hasSubscribedToNotificationType(publication.getCreatorId(), NotificationType.PUBLICATION_FORGOTTEN_IN_PROGRESS)) {
        publicationsByCreatorOrAuthor.computeIfAbsent(publication.getCreator(), k -> new ArrayList<>()).add(managedPublication);
      }
    }

    for (Map.Entry<Person, List<Publication>> entry : publicationsByCreatorOrAuthor.entrySet()) {
      this.sendEmailToValidatorForForgottenPublications(entry.getKey().getResId(), entry.getValue(),
              EmailMessage.EmailTemplate.FORGOTTEN_PUBLICATIONS_IN_PROGRESS);
    }
  }

  @Transactional
  public void getNotificationsAndSendEmails(List<Notification> notificationsToSend, List<Publication.PublicationStatus> publicationStatuses,
          NotificationType notificationType, EmailMessage.EmailTemplate template) {
    Map<String, List<Notification>> notificationsByPersonId = this
            .getFilteredNotificationsByPersonId(notificationsToSend, publicationStatuses);

    for (Map.Entry<String, List<Notification>> entry : notificationsByPersonId.entrySet()) {
      this.sendEmailWithPublicationIdsIfUserHasSubscribedToNotificationType(entry.getKey(), entry.getValue(),
              notificationType, template);
    }
  }

  @Transactional
  public void synchronizeFromUnigeReferential(LocalDateTime createdOrModifiedSince) {
    List<ResearchGroupDTO> synchronizedResearchGroups = this.researchGroupService.synchronizeFromUnigeReferential(createdOrModifiedSince);

    if (!synchronizedResearchGroups.isEmpty()) {
      List<User> adminOrRootUsers = this.userService.findAdminOrRootUsers();
      HashMap<String, Object> parameters = new HashMap<>();

      // Send synchronizedResearchGroups according to the operation was made onto them: added, modified, deleted
      Map<String, List<ResearchGroupDTO>> researchGroups = synchronizedResearchGroups.stream()
              .collect(groupingBy(rg -> rg.getAction().getAction()));
      researchGroups.entrySet().forEach(n -> parameters.put(n.getKey(), n.getValue()));

      // send emails
      for (User user : adminOrRootUsers) {
        SolidifyEventPublisher.getPublisher().publishEvent(
                new EmailMessage(user.getEmail(), EmailMessage.EmailTemplate.SYNCHRONIZED_RESEARCH_GROUPS, parameters));
      }
    }
  }

  public void notifyResearchGroupsToValidate() {
    OffsetDateTime offsetDelay = OffsetDateTime.now().minusDays(this.daysSinceLastCheckForResearchGroups);

    //Search all research groups not validated
    ResearchGroup searchResearchGroup = new ResearchGroup();
    searchResearchGroup.setValidated(false);
    ResearchGroupSpecification researchGroupSpecification = this.researchGroupService.getSpecification(searchResearchGroup);
    researchGroupSpecification.setLastUpdateBefore(offsetDelay);
    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(ActionName.CREATION));
    List<ResearchGroup> researchGroupList = this.researchGroupService.findAll(researchGroupSpecification, pageable).toList();

    if (!researchGroupList.isEmpty()) {

      // build research groups strings
      HashMap<String, Object> parameters = new HashMap<>();
      researchGroupList.stream().map(ResearchGroup::getResId).forEach(n -> parameters.put(n, null));

      // send emails
      for (String emailTo : this.emailsToSendGeneralNotifications) {
        SolidifyEventPublisher.getPublisher().publishEvent(
                new EmailMessage(emailTo, EmailMessage.EmailTemplate.NEW_RESEARCH_GROUPS_TO_VALIDATE, parameters));
      }
    }
  }

  public void reindexPublicationsWithEndingEmbargo(LocalDate startDate, LocalDate endDate) {
    List<DocumentFile> documentFiles = this.documentFileService.findDocumentFilesWithEmbargoEndDateEnding(startDate, endDate);
    if (!documentFiles.isEmpty()) {
      documentFiles.forEach(document -> {
        // Put publication in queue to be indexed
        SolidifyEventPublisher.getPublisher().publishEvent(new PublicationIndexingMessage(document.getPublication().getResId()));
      });
    }
  }

  public void sendThesisToSwissNationalLibrary(OffsetDateTime searchFrom) {
    List<Publication> thesesToSend = this.swissNationalLibraryService.getThesesToSend(searchFrom);
    log.info("{} thesis publications must be sent to Swiss National Library sFTP server", thesesToSend.size());
    int sentThesis = 0;
    for (Publication publication : thesesToSend) {
      log.info("Will sync publication {} to Swiss National Library sFTP server", publication.getArchiveId());
      try {
        this.swissNationalLibraryService.sendNonPublicFiles(publication);
        sentThesis++;
      } catch (Exception e) {
        log.error("An error occurred while trying to send thesis {} to Swiss National Library",
                publication.getResId() + " (" + publication.getArchiveId() + ")", e);
      }
    }
    log.info("{} thesis publications have been sent to Swiss National Library sFTP server", sentThesis);
  }

  public void synchronizeOrcidProfiles(OffsetDateTime searchFrom) {
    List<Person> peopleWithOrcidToken = this.personService.findPeopleWithOrcidToken();
    this.synchronizeFromOrcidProfiles(peopleWithOrcidToken, searchFrom);
    this.synchronizeToOrcidProfiles(peopleWithOrcidToken, searchFrom);
  }

  private void synchronizeFromOrcidProfiles(List<Person> peopleWithOrcidToken, OffsetDateTime searchFrom) {
    log.info("--- Starting synchronization of Works from ORCID profiles to AoU (searchFrom: {}) ---", searchFrom);
    ImportedIdentifiers alreadyImportedIdentifiers = new ImportedIdentifiers();
    for (Person person : peopleWithOrcidToken) {
      if (Boolean.TRUE.equals(person.getSyncFromOrcidProfile())) {
        try {
          // try - catch to not block synchronization for everyone if there is a problem with a person
          List<OrcidSynchronization> orcidSynchronizationsToNotify = this.orcidSynchronizationService.synchronizeFromOrcidProfileForPerson(
                  person, searchFrom, alreadyImportedIdentifiers);
          this.sendSynchronizationsFromOrcidProfileEmails(person, orcidSynchronizationsToNotify);
        } catch (Exception e) {
          log.error("An exception occurred while trying to synchronize publications from ORCID for person {}", person, e);
        }
      }
    }
  }

  private void synchronizeToOrcidProfiles(List<Person> peopleWithOrcidToken, OffsetDateTime searchFrom) {
    log.info("--- Starting synchronization of publications from AoU to ORCID profiles (searchFrom: {}) ---", searchFrom);
    for (Person person : peopleWithOrcidToken) {
      if (Boolean.TRUE.equals(person.getSyncToOrcidProfile())) {
        try {
          // try - catch to not block synchronization for everyone if there is a problem with a person
          List<OrcidSynchronization> orcidSynchronizationsToNotify = this.orcidSynchronizationService.synchronizeToOrcidProfileForPerson(person,
                  searchFrom, true);
          this.sendSynchronizationsToOrcidProfileEmails(person, orcidSynchronizationsToNotify);
        } catch (Exception e) {
          log.error("An exception occurred while trying to synchronize publications to ORCID for person {}", person, e);
        }
      }
    }
  }

  public void sendSynchronizationsFromOrcidProfileEmails(Person person, List<OrcidSynchronization> orcidSynchronizationsToNotify) {
    if (!orcidSynchronizationsToNotify.isEmpty()
            && this.personService.hasSubscribedToNotificationType(person.getResId(), NotificationType.PUBLICATION_DETECTED_ON_ORCID)) {
      // compute a list of unique publication ids to notify (to not include the same publication more than once in an email)
      HashMap<String, Object> parameters = new HashMap<>();
      orcidSynchronizationsToNotify.stream().map(orcidSync -> orcidSync.getPublication().getResId()).distinct()
              .forEach(n -> parameters.put(n, null));

      // send emails to people
      List<User> recipientUsers = this.userService.findByPersonResId(person.getResId());
      for (User user : recipientUsers) {
        SolidifyEventPublisher.getPublisher().publishEvent(
                new EmailMessage(user.getEmail(), EmailMessage.EmailTemplate.NEW_PUBLICATIONS_IMPORTED_FROM_ORCID, parameters));
      }
    }
  }

  public void sendSynchronizationsToOrcidProfileEmails(Person person, List<OrcidSynchronization> orcidSynchronizationsToNotify) {
    if (!orcidSynchronizationsToNotify.isEmpty()
            && this.personService.hasSubscribedToNotificationType(person.getResId(), NotificationType.PUBLICATION_EXPORTED_TO_ORCID)) {
      // compute a list of unique publication ids to notify (to not include the same publication more than once in an email)
      HashMap<String, Object> parameters = new HashMap<>();
      orcidSynchronizationsToNotify.stream().map(orcidSync -> orcidSync.getPublication().getResId()).distinct()
              .forEach(n -> parameters.put(n, null));

      // send emails to people
      List<User> recipientUsers = this.userService.findByPersonResId(person.getResId());
      for (User user : recipientUsers) {
        SolidifyEventPublisher.getPublisher().publishEvent(
                new EmailMessage(user.getEmail(), EmailMessage.EmailTemplate.NEW_PUBLICATIONS_EXPORTED_TO_ORCID, parameters));
      }
    }
  }

  @Transactional
  public void cleanPublicationsUpdatedWithoutChanges() {

    OffsetDateTime searchFrom = OffsetDateTime.now().minusDays(this.daysToKeepPublicationsInEditionWithoutChanges);

    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(ActionName.CREATION));
    List<Publication> publicationsToCheck = this.publicationService.findPublicationByStatusAndLastUpdate(
            Publication.PublicationStatus.IN_EDITION, searchFrom, pageable).toList();
    log.info("{} publications have changes to IN_EDITION status and are needed to check if there any difference have been made ",
            publicationsToCheck.size());
    int cleanPublications = 0;
    for (Publication publication : publicationsToCheck) {
      Map<String, Map<String, List<MetadataDifference>>> differenceList = this.publicationService.checkDifferencesInUpdate(publication, false);
      if (differenceList.isEmpty()) {
        cleanPublications++;
        publication.setStatus(Publication.PublicationStatus.CANCEL_EDITION);
        this.publicationService.save(publication);
        log.info("Publication {} has been change to status CANCEL_EDITION since it does no contains any change", publication.getResId());
        SolidifyEventPublisher.getPublisher().publishEvent(new PublicationMessage(publication.getResId()));
      }
    }
    if (cleanPublications > 0) {
      log.info("{} Publications have been change to status CANCEL_EDITION since it does no contains any change", cleanPublications);
    }
  }
}
