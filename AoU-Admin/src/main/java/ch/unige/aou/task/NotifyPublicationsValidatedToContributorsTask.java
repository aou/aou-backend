/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - NotifyPublicationsValidatedToContributorsTask.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.task;

import java.util.List;

import ch.unige.aou.business.NotificationService;
import ch.unige.aou.business.ScheduledTaskService;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.schedule.ScheduledTask;

public class NotifyPublicationsValidatedToContributorsTask extends TaskWithNotificationsRunnable {

  public NotifyPublicationsValidatedToContributorsTask(ScheduledTaskService scheduledTaskService, ScheduledTask scheduledTask,
          NotificationService notificationService, TaskExecutorService taskExecutorService) {
    super(scheduledTaskService, scheduledTask, notificationService, taskExecutorService);
  }

  @Override
  protected void executeTask() {
    List<Notification> notificationsToSend = this.getNotificationsToSend(NotificationType.MY_INDIRECT_PUBLICATION_VALIDATED);
    if (!notificationsToSend.isEmpty()) {
      this.taskExecutorService.getNotificationsAndSendEmails(notificationsToSend, List.of(Publication.PublicationStatus.COMPLETED),
              NotificationType.MY_INDIRECT_PUBLICATION_VALIDATED,
              EmailMessage.EmailTemplate.MY_INDIRECT_PUBLICATION_VALIDATED);
    }
  }
}
