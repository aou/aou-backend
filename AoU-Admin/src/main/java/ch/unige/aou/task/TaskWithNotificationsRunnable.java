/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - TaskWithNotificationsRunnable.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.task;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.scheduler.AbstractTaskRunnable;

import ch.unige.aou.business.NotificationService;
import ch.unige.aou.business.ScheduledTaskService;
import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.schedule.ScheduledTask;
import ch.unige.aou.specification.NotificationSpecification;

public abstract class TaskWithNotificationsRunnable extends AbstractTaskRunnable<ScheduledTask> {

  private static final Logger log = LoggerFactory.getLogger(TaskWithNotificationsRunnable.class);

  protected ScheduledTaskService scheduledTaskService;
  protected final NotificationService notificationService;
  protected final TaskExecutorService taskExecutorService;

  protected TaskWithNotificationsRunnable(ScheduledTaskService scheduledTaskService, ScheduledTask scheduledTask,
          NotificationService notificationService, TaskExecutorService taskExecutorService) {
    super(scheduledTask);
    if (scheduledTask == null) {
      throw new SolidifyRuntimeException("ScheduledTask cannot be null");
    }
    this.scheduledTaskService = scheduledTaskService;
    this.notificationService = notificationService;
    this.taskExecutorService = taskExecutorService;
  }

  /**
   * Get notifications of the given type that have been created since the last execution of the task and have not been sent yet
   *
   * @param notificationType
   * @return
   */
  protected List<Notification> getNotificationsToSend(NotificationType notificationType) {
    if (this.lastExecutionStartTime == null) {
      this.lastExecutionStartTime = OffsetDateTime.of(2000, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC);
    }

    OffsetDateTime searchFrom = this.lastExecutionStartTime.minusSeconds(10);

    Notification search = new Notification();
    search.setNotificationType(notificationType);
    NotificationSpecification notificationSpecification = this.notificationService.getSpecification(search);
    notificationSpecification.setCreatedDateMinValue(searchFrom);
    notificationSpecification.setOnlyWithNullSentTime(true);
    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(ActionName.CREATION));
    return this.notificationService.findAll(notificationSpecification, pageable).toList();
  }

  /**
   * Update the scheduled task, but only if it hasn't been deleted while it was running
   *
   * @param scheduledTask
   */
  @Override
  protected void saveScheduledTask(ScheduledTask scheduledTask) {
    if (this.scheduledTaskService.existsById(scheduledTask.getResId())) {
      this.scheduledTaskService.save(scheduledTask);
    } else {
      log.warn("The scheduled task '{}' does not exist anymore", scheduledTask.getResId());
    }
  }
}
