/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - Fedora3Test.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.storage;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;

import org.hibernate.service.spi.ServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestClientException;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.model.xml.fedora.foxml.v1.DigitalObject;
import ch.unige.aou.model.xml.fedora.repository.FedoraRepository;
import ch.unige.aou.service.storage.Fedora3Client;

@ExtendWith(SpringExtension.class)
@Disabled("Run these tests manually if you have an Fedora3 account")
class Fedora3Test extends StorageServiceTest {
  private static final Logger log = LoggerFactory.getLogger(Fedora3Test.class);

  private static final String FEDORA_SERVER = "url: must be updated";
  private static final String FEDORA_USER = "user: must be updated";
  private static final String FEDORA_PASSWORD = "password: must be updated";

  @Override
  @BeforeEach
  public void setUp() {
    super.setUp();
  }

  @Test
  void fedoraInfoTest() throws RestClientException, JAXBException {
    try {
      Fedora3Client fedoraClient = new Fedora3Client(FEDORA_SERVER, FEDORA_USER, FEDORA_PASSWORD,
              this.metadataService.getAouXmlNamespacePrefixMapper(AouMetadataVersion.getDefaultVersion()));
      FedoraRepository repo = fedoraClient.getServerInfo();
      log.info("Fedora server version: {}", repo.getRepositoryVersion());
    } catch (RuntimeException e) {
      fail("Exception " + e.getClass() + " should not happen");
    }
  }

  @Test
  void foxmlTest() throws ServiceException, JAXBException {
    Fedora3Client fedoraClient = new Fedora3Client(FEDORA_SERVER, FEDORA_USER, FEDORA_PASSWORD,
            this.metadataService.getAouXmlNamespacePrefixMapper(AouMetadataVersion.getDefaultVersion()));
    String publicationPid = fedoraClient.getNextPID("aou-unit-test");
    log.info("Next PID={}", publicationPid);
    if (fedoraClient.check(publicationPid) == HttpStatus.NOT_FOUND) {
      fedoraClient.ingest(publicationPid, "title");
    }
    // Get document
    DigitalObject foxml = fedoraClient.getFoxml(publicationPid);
    // Display Foxml
    StringWriter sw = new StringWriter();
    Marshaller marshaller = this.getJaxbMarshaller();
    marshaller.marshal(foxml, sw);
    String foxmlXml = sw.toString();
    log.info(foxmlXml);
    // Validate Foxml
    try {
      String foxmlXsd = this.getFoxmlSchema();
      XMLTool.validate(foxmlXsd, foxmlXml);
    } catch (Exception e) {
      throw new SolidifyCheckingException(e.getCause().getMessage());
    }
  }

  private String getFoxmlSchema() throws FileNotFoundException, IOException {
    String xsdPath = "foxml-1.1.xsd";
    return FileTool.toString(new FileInputStream(this.getPath(xsdPath).toString()));
  }

  private Path getPath(String f) throws IOException {
    try {
      return new ClassPathResource(f).getFile().toPath();
    } catch (final IOException e) {
      return new FileSystemResource(f).getFile().toPath();
    }
  }

  private Marshaller getJaxbMarshaller() {
    try {
      Marshaller marshaller = JAXBContext.newInstance(DigitalObject.class).createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      marshaller.setProperty(AouConstants.XML_NAMESPACE_PREFIX_MAPPER,
              this.metadataService.getAouXmlNamespacePrefixMapper(AouMetadataVersion.getDefaultVersion()));
      return marshaller;
    } catch (JAXBException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }
}
