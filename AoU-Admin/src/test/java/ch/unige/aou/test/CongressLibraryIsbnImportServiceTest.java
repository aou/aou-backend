/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - GoogleIsbnImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.CongressLibraryIsbnImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
class CongressLibraryIsbnImportServiceTest {

  private static final String INSPIRE_HEP_METADATA_FILE = "test-loc-isbn-response.json";
  private static final String ISBN_ID = "9780192871787";
  private static final String ISBN_ABSTRACT = "The Oxford Handbook of Swiss Politics provides a comprehensive analysis of the many different facets of the Swiss political system and of the major developments in modern Swiss politics. Its breadth offers analyses relevant not only to political science but also to international relations, European studies, history, sociology, law, and economics. The volume brings together a diverse set of more than fifty leading experts in their respective areas, who explore Switzerland's distinctive and sometimes intriguing politics at all levels and across many themes. They firmly place them in an international and comparative context and in conversation with the broader scholarly literature. Therefore, this edited collection provides a necessary corrective to the often rather idealized and sometimes outdated perception of Swiss politics. The edited volume presents an account of Swiss politics that recognizes its inherent diversity by taking a thematic approach in seven sections, an introduction, and an epilogue. However, by presenting new arguments, insights, and data, all chapters also make contributions in their own right. The seven sections are: foundations (Chapters 2 to 7), institutions (Chapters 8 to 12), cantons and municipalities (Chapters 13 to 15), actors (Chapters 16 to 20), elections and votes (Chapters 21 to 23), decision-making processes (Chapters 24 and 25), public policies (Chapters 26 to 40); and three concluding chapters compose the epilogue (Chapters 41 to 43).";

  private CongressLibraryIsbnImportService congressLibraryIsbnImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;
  @Mock
  protected DocumentFileTypeService documentFileTypeService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.congressLibraryIsbnImportService = new CongressLibraryIsbnImportService(aouProperties, this.messageService, this.metadataService,
            this.contributorService, this.journalTitleRemoteService, this.licenseService);
  }

  @Test
  void fillDepositDocTest() {

    JSONObject isbnJson = this.getInspireHepMetadata();
    DepositDoc depositDoc = new DepositDoc();
    assert isbnJson != null;
    this.congressLibraryIsbnImportService.fillDepositDoc(ISBN_ID, depositDoc, isbnJson);

    assertEquals(ISBN_ID, depositDoc.getIdentifiers().getIsbn());
    assertEquals("The Oxford handbook of Swiss politics", depositDoc.getTitle().getContent());
    assertNotNull(depositDoc.getDates());
    assertNotNull(depositDoc.getDates().getDate());
    assertFalse(depositDoc.getDates().getDate().isEmpty());
    assertEquals("2024", depositDoc.getDates().getDate().get(0).getContent());
    assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
    assertNotNull(depositDoc.getContributors());
    assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
    assertEquals(6, depositDoc.getContributors().getContributorOrCollaboration().size());
    assertInstanceOf(Contributor.class, depositDoc.getContributors().getContributorOrCollaboration().get(0));
    assertEquals("Patrick", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
    assertEquals("Emmenegger", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
    assertEquals(ISBN_ABSTRACT, depositDoc.getAbstracts().getAbstract().get(0).getContent());
    assertEquals(AouConstants.DEPOSIT_TYPE_LIVRE_NAME, depositDoc.getType());
    assertEquals(1, depositDoc.getLanguages().getLanguage().size());
    assertEquals("eng", depositDoc.getLanguages().getLanguage().get(0));

  }

  private JSONObject getInspireHepMetadata() {
    try (InputStream jsonStream = new ClassPathResource(INSPIRE_HEP_METADATA_FILE).getInputStream()) {
      JSONTokener tokener = new JSONTokener(jsonStream);
      return new JSONObject(tokener);
    } catch (IOException e) {
      fail("Unable to read " + INSPIRE_HEP_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
