/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DataCiteImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.DataCiteImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
class DataCiteImportServiceTest {

  private static final String DATACITE_JSON_METADATA_FILE = "test-datacite-response.json";
  private static final String TEST_DOI = "10.57974/re:visit_2022_1.19";

  private DataCiteImportService dataCiteImportService;

  @Mock
  private MessageService messageService;

  @Mock
  private MetadataService metadataService;

  @Mock
  private ContributorService contributorService;

  @Mock
  private JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.dataCiteImportService = new DataCiteImportService(aouProperties, this.messageService, this.metadataService, this.contributorService,
            this.journalTitleRemoteService, this.licenseService);
  }

  @Test
  void fillDepositDocTest() throws JsonProcessingException {

    String jsonString = this.getDataCiteJsonMetadata();
    JsonNode parentNode = new ObjectMapper().readTree(jsonString);
    final JsonNode dataNode = parentNode.get("data");
    DepositDoc depositDoc = new DepositDoc();
    this.dataCiteImportService.fillDepositDoc(depositDoc, dataNode);

    assertEquals("Revisiting clinical knowledge through medical artefacts", depositDoc.getTitle().getContent());
    assertEquals(AouConstants.LANG_CODE_GERMAN, depositDoc.getTitle().getLang());
    assertEquals(AouConstants.LANG_CODE_GERMAN, depositDoc.getLanguages().getLanguage().get(0));
    assertNotNull(depositDoc.getDates());
    assertNotNull(depositDoc.getDates().getDate());
    assertFalse(depositDoc.getDates().getDate().isEmpty());
    assertEquals("2022", depositDoc.getDates().getDate().get(0).getContent());
    assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
    assertNotNull(depositDoc.getContributors());
    assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
    assertEquals(3, depositDoc.getContributors().getContributorOrCollaboration().size());
    assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
    assertEquals("Berney", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
    assertEquals("Cédric", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
    assertEquals(TEST_DOI, depositDoc.getIdentifiers().getDoi());

    assertNotNull(depositDoc.getKeywords());
    assertNotNull(depositDoc.getKeywords().getKeyword());
    assertFalse(depositDoc.getKeywords().getKeyword().isEmpty());
    assertEquals(2, depositDoc.getKeywords().getKeyword().size());
    assertTrue(depositDoc.getKeywords().getKeyword().contains("Genetics"));
    assertTrue(depositDoc.getKeywords().getKeyword().contains("FOS: Biological sciences"));

    assertNotNull(depositDoc.getAbstracts());
    assertNotNull(depositDoc.getAbstracts().getAbstract());
    assertFalse(depositDoc.getAbstracts().getAbstract().isEmpty());
    assertEquals(1, depositDoc.getAbstracts().getAbstract().size());
    assertEquals(depositDoc.getAbstracts().getAbstract().get(0).getContent(), "Many ancient and all but forgotten anatomical collections "
            + "are held in European towns such as Paris, Berlin, Vienna, Heidelberg, Montpellier, Stockholm, Zurich, Bologna, Firenze, Geneva, "
            + "Amsterdam, etc. Exhibited in dedicated museums or attached to university hospitals, these collections usually contain wet or "
            + "dry natural specimens, as well as wax casts. The latter were the subject of Wax Bodies. Histories of clinical and artistic "
            + "uses of syphilis ceroplastics, a recent international research workshop held in Paris. The event, organized as part of an "
            + "ongoing research project on the history of syphilis, brought together some 50 historians, dermatologists, artists, "
            + "ceroplasticians and museographers on June 30th and July 1st, 2022. The venue was held at the remarkable Musée des moulages "
            + "de l’hôpital Saint-Louis, which holds some 5,000 pathological casts, about a quarter of which represent body "
            + "parts afflicted by syphilis.");

  }

  private String getDataCiteJsonMetadata() {
    try (InputStream jsonStream = new ClassPathResource(DATACITE_JSON_METADATA_FILE).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + DATACITE_JSON_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }

}
