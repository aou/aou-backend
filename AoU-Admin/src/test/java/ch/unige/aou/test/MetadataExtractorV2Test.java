/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataExtractorV2Test.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.metadata.MetadataExtractorV2;

class MetadataExtractorV2Test extends MetadataTest {

  private MetadataExtractorV2 metadataExtractor;

  @Override
  @BeforeEach
  public void setUp() {
    super.setUp();
    this.metadataExtractor = new MetadataExtractorV2(this.aouProperties, this.publicationTypeService, this.publicationSubtypeService,
            this.publicationSubSubtypeService, this.structureService, this.researchGroupService, this.contributorService,
            this.labeledLanguageService, this.journalTitleRemoteService, this.personService, this.messageService, this.unigePersonSearchService);
  }

  @Test
  void preparationWithMetadataTest() {

    /**
     * Create DepositDoc from Json form
     */
    String jsonFormData = this.getJsonFormData(AouMetadataVersion.getDefaultVersion());

    DepositDoc depositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(jsonFormData);
    assertNotNull(depositDoc);
    this.assertEqualValuesDefaultVersion(depositDoc);

    /**
     * Serialize DepositDoc into Json again, then create another DepositDoc and check that data are preserved
     */
    String xmlData = this.metadataExtractor.serializeDepositDocToXml(depositDoc);
    this.assertIsXmlMetadataValidDefaultVersion(xmlData);
    String serializedJsonFormData = this.metadataExtractor.transformMetadataToFormData(xmlData);

    // test that ids instead of codes are in form data
    assertTrue(serializedJsonFormData.contains(DEPOSIT_TYPE_ARTICLE_ID));
    assertTrue(serializedJsonFormData.contains(DEPOSIT_SUBTYPE_ARTICLE_ID));
    assertTrue(serializedJsonFormData.contains(LANGUAGE_FRENCH_ID));
    assertTrue(serializedJsonFormData.contains(LANGUAGE_ENGLISH_ID));
    assertTrue(serializedJsonFormData.contains(LANGUAGE_GERMAN_ID));

    DepositDoc newDepositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(serializedJsonFormData);
    assertNotNull(newDepositDoc);
    this.assertEqualValuesDefaultVersion(newDepositDoc);
  }

  @Test
  void preparationWithEmptyListsMetadataTest() {
    /**
     * Create DepositDoc from Json form
     */
    String jsonFormData = this.getJsonFormDataWithEmptyLists(AouMetadataVersion.getDefaultVersion());

    DepositDoc depositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(jsonFormData);
    assertNotNull(depositDoc);

    String xmlData = this.metadataExtractor.serializeDepositDocToXml(depositDoc);
    this.assertIsXmlMetadataValidDefaultVersion(xmlData);
  }

  private void assertIsXmlMetadataValidDefaultVersion(String xmlMetadata) {
    assertDoesNotThrow(() -> {
      ClassPathResource xsd = new ClassPathResource(
              SolidifyConstants.SCHEMA_HOME + "/" + AouMetadataVersion.getDefaultVersion().getDepositMetadataSchema());
      String schema = FileTool.toString(xsd.getInputStream());
      XMLTool.validate(schema, xmlMetadata);
    });
  }
}
