/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataValidatorV2Test.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.rest.UnigePersonDTO;
import ch.unige.aou.model.xml.deposit.v2_4.Classification;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.metadata.MetadataExtractorV2;
import ch.unige.aou.service.metadata.MetadataValidatorV2;
import ch.unige.aou.service.rest.RockUnigePersonSearchService;

public class MetadataValidatorV2Test extends MetadataValidatorServiceTest {

  private MetadataValidatorV2 metadataValidator;
  private MetadataExtractorV2 metadataExtractor;
  @Mock
  private RockUnigePersonSearchService unigePersonSearchService;

  protected static final String CN_INDIVIDU = "12345";

  @Override
  @BeforeEach
  public void setUp() {
    super.setUp();
    this.metadataExtractor = new MetadataExtractorV2(this.aouProperties, this.publicationTypeService, this.publicationSubtypeService,
            this.publicationSubSubtypeService, this.structureService, this.researchGroupService, this.contributorService,
            this.labeledLanguageService, this.journalTitleRemoteService, this.personService, this.messageService, this.unigePersonSearchService);

    this.metadataValidator = new MetadataValidatorV2(this.aouProperties, this.metadataExtractor, this.messageService,
            this.publicationTypeService, this.publicationSubtypeService, this.publicationSubSubtypeService, this.researchGroupService,
            this.structureService, this.duplicateService, this.unigePersonSearchService, this.documentFileService, this.historyService);
  }

  @Test
  void validateRulesForValidatorsWithoutErrorsTest() {
    /**
     * Create DepositDoc from Json form
     */
    String jsonFormData = this.getJsonFormData(AouMetadataVersion.getDefaultVersion());

    DepositDoc depositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(jsonFormData);

    Publication publication = new Publication();
    final BindingResult errors = new BeanPropertyBindingResult(publication, publication.getClass().getName());

    UnigePersonDTO unigePersonDTO = new UnigePersonDTO(CN_INDIVIDU, "John", "Doe", 1980, "author", "structure", null);
    when(this.unigePersonSearchService.searchByCnIndividu(CN_INDIVIDU)).thenReturn(Optional.of(unigePersonDTO));

    this.metadataValidator.validateRulesForValidators(publication, depositDoc, errors);
    assertEquals(errors.getAllErrors().size(), 0); // there no errors
  }

  @Test
  void validateRulesForValidatorsWithErrorsTest() {
    /**
     * Create DepositDoc from Json form
     */
    String jsonFormData = this.getJsonFormData(AouMetadataVersion.getDefaultVersion());

    DepositDoc depositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(jsonFormData);

    Publication publication = new Publication();
    final BindingResult errors = new BeanPropertyBindingResult(publication, publication.getClass().getName());
    Pageable pageable = PageRequest.of(0, 10);

    UnigePersonDTO unigePersonDTO = new UnigePersonDTO(CN_INDIVIDU, "Unknown", "Unknown", 1980, "author", "structure", null);
    when(this.unigePersonSearchService.searchByCnIndividu(CN_INDIVIDU)).thenReturn(Optional.of(unigePersonDTO));

    this.metadataValidator.validateRulesForValidators(publication, depositDoc, errors);
    assertEquals(errors.getAllErrors().size(), 2); // there are errors
  }

  @Test
  void validateAllRulesWithoutErrorsTest() {
    /**
     * Create DepositDoc from Json form
     */
    String jsonFormData = this.getJsonFormData(AouMetadataVersion.getDefaultVersion());

    DepositDoc depositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(jsonFormData);
    String metadata = this.metadataExtractor.serializeDepositDocToXml(depositDoc);

    Publication publication = new Publication();
    publication.setMetadata(metadata);
    this.metadataExtractor.setPropertiesFromMetadata(publication);
    publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION);

    final BindingResult errors = new BeanPropertyBindingResult(publication, publication.getClass().getName());

    UnigePersonDTO unigePersonDTO = new UnigePersonDTO(CN_INDIVIDU, "John", "Doe", 1980, "author", "structure", null);
    when(this.unigePersonSearchService.searchByCnIndividu(CN_INDIVIDU)).thenReturn(Optional.of(unigePersonDTO));

    this.metadataValidator.validate(publication, errors);
    assertEquals(0, errors.getAllErrors().size()); // there no errors
  }

  @Test
  void validateAllRulesWithoutErrorsWorkingPaperTest() {
    /**
     * Create DepositDoc from Json form
     */
    try (InputStream jsonStream = new ClassPathResource(JSON_FORM_DATA_V2_4_WORKING_PAPER_FILE).getInputStream()) {
      String jsonFormData = FileTool.toString(jsonStream);
      DepositDoc depositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(jsonFormData);

      // Store a JEL code in lowercase. It will be automatically replaced by a JEL code in uppercase
      Classification classification = depositDoc.getClassifications().getClassification().get(1);
      if (classification.getCode().equals("JEL")) {
        classification.setItem(classification.getItem().toLowerCase());
        assertEquals("h10", depositDoc.getClassifications().getClassification().get(1).getItem());
      } else {
        fail("Unable to modify JEL for testing");
      }

      String metadata = this.metadataExtractor.serializeDepositDocToXml(depositDoc);

      Publication publication = new Publication();
      publication.setMetadata(metadata);
      this.metadataExtractor.cleanMetadata(publication);
      this.metadataExtractor.setPropertiesFromMetadata(publication);
      publication.setMetadataValidationType(Publication.MetadataValidationType.GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION);

      final BindingResult errors = new BeanPropertyBindingResult(publication, publication.getClass().getName());

      UnigePersonDTO unigePersonDTO = new UnigePersonDTO(CN_INDIVIDU, "John", "Doe", 1980, "author", "structure", null);
      when(this.unigePersonSearchService.searchByCnIndividu(CN_INDIVIDU)).thenReturn(Optional.of(unigePersonDTO));

      this.metadataValidator.validate(publication, errors);
      assertEquals(0, errors.getAllErrors().size()); // there no errors
    } catch (IOException e) {
      fail("Unable to read '" + JSON_FORM_DATA_V2_4_WORKING_PAPER_FILE + "' file: " + e.getMessage());
    }

  }
}
