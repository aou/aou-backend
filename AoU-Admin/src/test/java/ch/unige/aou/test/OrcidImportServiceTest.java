/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - OrcidImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.xml.bind.JAXB;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.xml.orcid.v3_0.bulk.Bulk;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Work;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OrcidService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.OrcidImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class OrcidImportServiceTest {

  private static final String ORCID_XML_METADATA_FILE = "test-orcid-work-response.xml";

  private OrcidImportService orcidImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @Mock
  protected OrcidService orcidService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.orcidImportService = new OrcidImportService(aouProperties, this.messageService, this.metadataService,
            this.contributorService, this.journalTitleRemoteService, this.licenseService, this.orcidService);
  }

  @Test
  public void fillDepositDocTest() {

    Bulk bulkDoc = JAXB.unmarshal(new StringReader(this.getOrcidXmlMetadata()), Bulk.class);
    Work work = (Work) bulkDoc.getWorkOrError().get(0);
    DepositDoc depositDoc = new DepositDoc();
    this.orcidImportService.fillDepositDoc(depositDoc, work);

    assertEquals("Biosynthesis of chlorophylls from protoporphyrin IX", depositDoc.getTitle().getContent());
    assertEquals("My first test", depositDoc.getOriginalTitle().getContent());
    assertEquals("eng", depositDoc.getOriginalTitle().getLang());
    assertEquals(AouConstants.DEPOSIT_TYPE_ARTICLE_NAME, depositDoc.getType());
    assertEquals(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, depositDoc.getSubtype());
    assertEquals("2024-04-01", depositDoc.getDates().getDate().get(0).getContent());
    assertEquals("A review of the biosynthesis of chlorophylls and bacteriochlorophylls",
            depositDoc.getAbstracts().getAbstract().get(0).getContent());
    assertEquals("Natural Product Report", depositDoc.getContainer().getTitle());
    assertEquals("http://www.alaxos.ch", depositDoc.getPublisherVersionUrl());
    assertEquals(4, depositDoc.getContributors().getContributorOrCollaboration().size());

    assertEquals("Albert", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
    assertEquals("Levert", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
    assertEquals("0009-0002-5671-5672", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getOrcid());
    assertEquals(AuthorRole.AUTHOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getRole());

    assertEquals("John", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getFirstname());
    assertEquals("Smith", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getLastname());
    assertNull(((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getOrcid());
    assertEquals(AuthorRole.COLLABORATOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getRole());

    assertEquals("John", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(2)).getFirstname());
    assertEquals("Doe", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(2)).getLastname());
    assertNull(((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getOrcid());
    assertEquals(AuthorRole.AUTHOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(2)).getRole());

    assertEquals("Robert", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getFirstname());
    assertEquals("Johnson", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getLastname());
    assertNull(((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getOrcid());
    assertEquals(AuthorRole.AUTHOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getRole());
  }

  private String getOrcidXmlMetadata() {
    try (InputStream jsonStream = new ClassPathResource(ORCID_XML_METADATA_FILE).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + ORCID_XML_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
