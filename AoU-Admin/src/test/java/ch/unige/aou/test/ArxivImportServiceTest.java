/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - ArxivImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.ArxivImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class ArxivImportServiceTest {

  private static final String ARXIV_XML_METADATA_FILE = "test-arxiv-response.xml";
  private static final String ARXIV_ID = "2104.05713v1";

  private static final String summary = "We analyze the CORALIE/HARPS sample of exoplanets (Mayor et al. 2011) found by the Doppler radial "
          + "velocity method for signs of the predicted \"desert\" at 10-$100 M_\\odot$ caused by runaway gas accretion at semimajor "
          + "axes of $&lt; 3\\,$AU. We find that these data are not consistent with this prediction. This result is similar to the finding "
          + "by the MOA gravitational microlensing survey that found no desert in the exoplanet distribution for exoplanets in slightly longer "
          + "period orbits and somewhat lower host masses (Suzuki et al. 2018). Together, these results imply that the runaway accretion "
          + "scenario of the core accretion theory does not have a large influence on the final mass and semimajor axis distribution of "
          + "exoplanets.";
  private ArxivImportService arxivImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;

  @Mock
  protected DocumentFileService documentFileService;

  @Mock
  protected DocumentFileTypeService documentFileTypeService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @Mock
  protected PublicationSubtypeService publicationSubtypeService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.arxivImportService = new ArxivImportService(aouProperties, this.messageService, this.metadataService,
            this.contributorService, this.documentFileService, this.documentFileTypeService, this.journalTitleRemoteService,
            this.licenseService, this.publicationSubtypeService);
  }

  @Test
  public void fillDepositDocTest() {

    try {
      Document arxivDoc = XMLTool.parseXML(this.getArxivXmlMetadata());
      DepositDoc depositDoc = new DepositDoc();
      this.arxivImportService.fillDepositDoc(ARXIV_ID, depositDoc, arxivDoc);

      assertEquals(ARXIV_ID, depositDoc.getIdentifiers().getArxiv());
      assertEquals("No Sub-Saturn Mass Planet Desert in the CORALIE/HARPS Radial Velocity Sample", depositDoc.getTitle().getContent());
      assertNotNull(depositDoc.getDates());
      assertNotNull(depositDoc.getDates().getDate());
      assertTrue(!depositDoc.getDates().getDate().isEmpty());
      assertEquals("2021-04-12", depositDoc.getDates().getDate().get(0).getContent());
      assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
      assertNotNull(depositDoc.getContributors());
      assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
      assertEquals(3, depositDoc.getContributors().getContributorOrCollaboration().size());
      assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
      assertEquals("David P.", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
      assertEquals("Bennett", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
      assertEquals("submitted to the Astronomical Journal, 19 pages with 5 figures", depositDoc.getNote());
      assertEquals(summary, depositDoc.getAbstracts().getAbstract().get(0).getContent());

    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching arxivId '" + ARXIV_ID + "' on Arxiv", e);
    }
  }

  private String getArxivXmlMetadata() {
    try (InputStream jsonStream = new ClassPathResource(ARXIV_XML_METADATA_FILE).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + ARXIV_XML_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
