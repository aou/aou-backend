/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataUpgradeTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.StringReader;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import jakarta.xml.bind.JAXB;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.MetadataExtractorV1;
import ch.unige.aou.service.rest.UnigePersonSearchService;

class MetadataUpgradeTest extends MetadataTest {

  private MetadataService metadataService;

  @Mock
  private UnigePersonSearchService unigePersonSearchService;

  @Override
  @BeforeEach
  public void setUp() {
    super.setUp();
    this.metadataService = new MetadataService(this.aouProperties, this.messageService, this.publicationTypeService,
            this.publicationSubtypeService, this.publicationSubSubtypeService, this.structureService, this.researchGroupService,
            this.contributorService, this.labeledLanguageService, this.journalTitleRemoteService, this.duplicateService, this.personService,
            this.unigePersonSearchService, this.remoteStructureService, this.documentFileService, this.remotePublicationService,
            this.remoteDocumentFileService, this.historyService, this.remotePublicationSubtypeService, this.remoteDocumentFileTypeService,
            this.remoteContributorService, this.remoteLicenseService, this.remoteUserService);
  }

  @Test
  void upgradeMetadataFromV1toDefaultVersion() {

    try {
      /**
       * Create DepositDoc from Json form
       */
      String jsonFormData = this.getJsonFormData(AouMetadataVersion.V1_0);
      MetadataExtractorV1 metadataExtractorV1 = (MetadataExtractorV1) this.metadataService.getMetadataExtractor(AouMetadataVersion.V1_0);
      ch.unige.aou.model.xml.deposit.v1.DepositDoc depositDocV1 = metadataExtractorV1.createDepositDocFromJsonFormData(jsonFormData);
      assertNotNull(depositDocV1);
      String metadata_v1_xml = metadataExtractorV1.serializeDepositDocToXml(depositDocV1);

      String metadata_default_version_xml = this.metadataService.upgradeMetadataFormat(metadata_v1_xml);
      assertNotNull(metadata_default_version_xml);

      DepositDoc depositDocDefaultVersion = JAXB.unmarshal(new StringReader(metadata_default_version_xml), DepositDoc.class);
      this.assertEqualValuesDefaultVersion(depositDocDefaultVersion, true);

    } catch (Exception e) {
      fail(String.format("unable to upgrade metadata from %s to %s", AouMetadataVersion.V1_0, AouMetadataVersion.getDefaultVersion()), e);
    }
  }

  @Test
  void upgradeMetadataFromV2_3toDefaultVersion() {
    AouMetadataVersion startVersion = AouMetadataVersion.V2_3;
    try {
      /**
       * Create DepositDoc from Json form
       */
      String metadata_start_version_xml = this.getXmlMetadata(startVersion);
      String metadata_default_version_xml = this.metadataService.upgradeMetadataFormat(metadata_start_version_xml);
      assertNotNull(metadata_default_version_xml);

      DepositDoc depositDocDefaultVersion = JAXB.unmarshal(new StringReader(metadata_default_version_xml), DepositDoc.class);
      this.assertEqualValuesDefaultVersion(depositDocDefaultVersion, false);

    } catch (Exception e) {
      fail(String.format("unable to upgrade metadata from %s to %s", startVersion, AouMetadataVersion.getDefaultVersion()), e);
    }
  }
}
