/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataExtractorV1Test.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.model.xml.deposit.v1.DepositDoc;
import ch.unige.aou.service.metadata.MetadataExtractorV1;

class MetadataExtractorV1Test extends MetadataTest {

  private MetadataExtractorV1 metadataExtractor;

  @Override
  @BeforeEach
  public void setUp() {
    super.setUp();
    this.metadataExtractor = new MetadataExtractorV1(this.aouProperties, this.publicationTypeService, this.publicationSubtypeService,
            this.publicationSubSubtypeService, this.structureService, this.researchGroupService, this.contributorService,
            this.labeledLanguageService, this.journalTitleRemoteService, this.personService, this.messageService, this.unigePersonSearchService);
  }

  @Test
  void preparationWithMetadataTest() {

    /**
     * Create DepositDoc from Json form
     */
    String jsonFormData = this.getJsonFormData(AouMetadataVersion.V1_0);

    DepositDoc depositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(jsonFormData);
    assertNotNull(depositDoc);
    this.assertEqualValuesV1(depositDoc);

    /**
     * Serialize DepositDoc into Json again, then create another DepositDoc and check that data are preserved
     */
    String xmlData = this.metadataExtractor.serializeDepositDocToXml(depositDoc);
    String serializedJsonFormData = this.metadataExtractor.transformMetadataToFormData(xmlData);

    // test that ids instead of codes are in form data
    assertTrue(serializedJsonFormData.contains(DEPOSIT_TYPE_ARTICLE_ID));
    assertTrue(serializedJsonFormData.contains(DEPOSIT_SUBTYPE_ARTICLE_ID));
    assertTrue(serializedJsonFormData.contains(LANGUAGE_FRENCH_ID));
    assertTrue(serializedJsonFormData.contains(LANGUAGE_ENGLISH_ID));
    assertTrue(serializedJsonFormData.contains(LANGUAGE_GERMAN_ID));

    DepositDoc newDepositDoc = this.metadataExtractor.createDepositDocFromJsonFormData(serializedJsonFormData);
    assertNotNull(newDepositDoc);
    this.assertEqualValuesV1(newDepositDoc);
  }
}
