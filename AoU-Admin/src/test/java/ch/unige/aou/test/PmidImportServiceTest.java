/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - PmidImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.PmidImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class PmidImportServiceTest {
  private static final String PMID_XML_METADATA_FILE = "test-pmid-response.xml";
  private static final String PMID_ID = "32696426";

  private AouProperties aouProperties;

  private PmidImportService pmidImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;

  @Mock
  protected DocumentFileTypeService documentFileTypeService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @BeforeEach
  public void setUp() {
    this.aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.pmidImportService = new PmidImportService(this.aouProperties, this.messageService, this.metadataService, this.contributorService,
            this.journalTitleRemoteService, this.licenseService);
  }

  @Test
  public void fillDepositDocTest() {
    try {
      Document arxivDoc = this.pmidImportService.parseXML(this.getPmidXmlMetadata());
      DepositDoc depositDoc = new DepositDoc();
      this.pmidImportService.fillDepositDoc(PMID_ID, depositDoc, arxivDoc);

      assertEquals(PMID_ID, depositDoc.getIdentifiers().getPmid().toString());
      assertEquals("Treatment of COVID-19 Pneumonia: the Case for Placenta-derived Cell Therapy.", depositDoc.getTitle().getContent());
      assertNotNull(depositDoc.getDates());
      assertNotNull(depositDoc.getDates().getDate());
      assertTrue(!depositDoc.getDates().getDate().isEmpty());
      assertEquals("2021-02", depositDoc.getDates().getDate().get(0).getContent());
      assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
      assertNotNull(depositDoc.getContributors());
      assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
      assertEquals(7, depositDoc.getContributors().getContributorOrCollaboration().size());
      assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
      assertEquals("Ekaterine", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
      assertEquals("Berishvili", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
      assertEquals("10.1007/s12015-020-10004-x", depositDoc.getIdentifiers().getDoi());
      assertEquals("PMC7372209", depositDoc.getIdentifiers().getPmcid());
      assertEquals("17", depositDoc.getContainer().getVolume().toString());
      assertEquals("1", depositDoc.getContainer().getIssue().toString());
      assertEquals(AouConstants.LANG_CODE_ENGLISH, depositDoc.getLanguages().getLanguage().get(0));
      assertEquals("Horizon 2020 Framework Programme", depositDoc.getFundings().getFunding().get(0).getFunder());
      assertEquals("874700", depositDoc.getFundings().getFunding().get(0).getCode());
      assertEquals("Article", depositDoc.getType());
      assertEquals("Article scientifique", depositDoc.getSubtype());
      assertEquals("Article", depositDoc.getSubsubtype());
      assertEquals("63-70", depositDoc.getPages().getPagingOrOther().get(0).getValue());

    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching PMID '" + PMID_ID + "' on Crossref", e);
    }
  }

  private String getPmidXmlMetadata() {
    try (InputStream jsonStream = new ClassPathResource(PMID_XML_METADATA_FILE).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + PMID_XML_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
