/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - SwisscoveryImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.SwisscoveryImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class SwisscoveryImportServiceTest {

  private static final String SWISSCOVERY_XML_METADATA_FILE = "test-swisscovery-response.xml";
  private static final String ISBN_ID = "2104.05713v1";

  private SwisscoveryImportService swisscoveryImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;

  @Mock
  protected DocumentFileTypeService documentFileTypeService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.swisscoveryImportService = new SwisscoveryImportService(aouProperties, this.messageService, this.metadataService,
            this.contributorService, this.journalTitleRemoteService, this.licenseService);
  }

  @Test
  public void fillDepositDocTest() {

    try {
      Document swisscoveryDoc = this.swisscoveryImportService.parseXML(this.getSwisscoveryXmlMetadata());
      DepositDoc depositDoc = new DepositDoc();
      this.swisscoveryImportService.fillDepositDoc(ISBN_ID, depositDoc, swisscoveryDoc);

      assertEquals(ISBN_ID, depositDoc.getIdentifiers().getIsbn());
      assertEquals("Introduction to programming in Java : an interdisciplinary approach", depositDoc.getTitle().getContent());
      assertNotNull(depositDoc.getDates());
      assertNotNull(depositDoc.getDates().getDate());
      assertTrue(!depositDoc.getDates().getDate().isEmpty());
      assertEquals("2007", depositDoc.getDates().getDate().get(0).getContent());
      assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
      assertNotNull(depositDoc.getContributors());
      assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
      assertEquals(2, depositDoc.getContributors().getContributorOrCollaboration().size());
      assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
      assertEquals("Robert", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
      assertEquals("Sedgewick", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
      assertEquals("723 S.", depositDoc.getPages().getPagingOrOther().get(0).getValue());
      assertEquals("Boston", depositDoc.getPublisher().getNameOrPlace().get(0).getValue());
      assertEquals("Pearson/Addison Wesley", depositDoc.getPublisher().getNameOrPlace().get(1).getValue());

    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching ISBN '" + ISBN_ID + "' on Arxiv", e);
    }
  }

  private String getSwisscoveryXmlMetadata() {
    try (InputStream jsonStream = new ClassPathResource(SWISSCOVERY_XML_METADATA_FILE).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + SWISSCOVERY_XML_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
