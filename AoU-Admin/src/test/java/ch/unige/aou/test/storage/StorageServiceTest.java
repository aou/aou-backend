/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - StorageServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.storage;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.AfterEach;
import org.mockito.Mock;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.LabeledLanguageService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.business.UserService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.rest.UnigePersonSearchService;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileTypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedLicenseRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStructureRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;
import ch.unige.aou.service.storage.StorageService;

public abstract class StorageServiceTest {

  protected MetadataService metadataService;

  @Mock
  protected MessageService messageService;
  @Mock
  protected PublicationService publicationService;
  @Mock
  protected UserService userService;
  @Mock
  private PublicationTypeService publicationTypeService;
  @Mock
  private PublicationSubtypeService publicationSubtypeService;
  @Mock
  private PublicationSubSubtypeService publicationSubSubtypeService;
  @Mock
  private StructureService structureService;
  @Mock
  private ResearchGroupService researchGroupService;
  @Mock
  private ContributorService contributorService;
  @Mock
  private PersonService personService;
  @Mock
  private LabeledLanguageService labeledLanguageService;
  @Mock
  private JournalTitleRemoteService journalTitleRemoteService;
  @Mock
  private DuplicateService duplicateService;
  @Mock
  private UnigePersonSearchService unigePersonSearchService;
  @Mock
  private TrustedStructureRemoteResourceService remoteStructureService;
  @Mock
  private DocumentFileService documentFileService;
  @Mock
  private TrustedPublicationRemoteResourceService remotePublicationService;
  @Mock
  private TrustedDocumentFileRemoteResourceService remoteDocumentFileService;
  @Mock
  private HistoryService historyService;
  @Mock
  TrustedPublicationSubtypeRemoteResourceService remotePublicationSubtypeService;
  @Mock
  TrustedDocumentFileTypeRemoteResourceService remoteDocumentFileTypeService;
  @Mock
  private TrustedContributorRemoteResourceService remoteContributorService;
  @Mock
  private TrustedLicenseRemoteResourceService remoteLicenseService;
  @Mock
  private TrustedUserRemoteResourceService remoteUserService;

  protected Path storageDir = Paths.get("src", "test", "resources", "storage");

  protected StorageService storageService;

  protected Path workingDir = Paths.get("src", "test", "resources", "testing");

  public final static String TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE = "{\n"
          + "    \"contributors\": {\n"
          + "        \"contributors\": [\n"
          + "            {\n"
          + "                \"contributor\": {\n"
          + "                    \"cnIndividu\": \"12345\",\n"
          + "                    \"email\": \"john.doe@example.com\",\n"
          + "                    \"firstname\": \"John\",\n"
          + "                    \"institution\": \"unige\",\n"
          + "                    \"lastname\": \"Doe\",\n"
          + "                    \"orcid\": \"1234-1234-1234-1234\",\n"
          + "                    \"role\": \"author\"\n"
          + "                }\n"
          + "            }\n"
          + "        ]\n"
          + "    },\n"
          + "    \"description\": {\n"
          + "        \"container\": {\n"
          + "            \"identifiers\": {\n"
          + "                \"issn\": \"0028-0836\"\n"
          + "            },\n"
          + "            \"issue\": 8,\n"
          + "            \"title\": {\n"
          + "                \"lang\": \"eng\",\n"
          + "                \"text\": \"Nature\"\n"
          + "            }\n"
          + "        }\n"
          + "    },\n"
          + "    \"type\": {\n"
          + "        \"subsubtype\": \"A1-LETTRE\",\n"
          + "        \"subtype\": \"A1\",\n"
          + "        \"title\": \"%s\",\n"
          + "        \"type\": \"ARTICLE\"\n"
          + "    }\n"
          + "}";

  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    aouProperties.setHome(this.workingDir.toAbsolutePath().toString());
    aouProperties.getStorage().setUrl(this.storageDir.toAbsolutePath().toUri().toString());

    this.metadataService = new MetadataService(aouProperties, this.messageService, this.publicationTypeService, this.publicationSubtypeService,
            this.publicationSubSubtypeService, this.structureService, this.researchGroupService, this.contributorService,
            this.labeledLanguageService, this.journalTitleRemoteService, this.duplicateService, this.personService,
            this.unigePersonSearchService, this.remoteStructureService, this.documentFileService, this.remotePublicationService,
            this.remoteDocumentFileService, this.historyService, this.remotePublicationSubtypeService, this.remoteDocumentFileTypeService,
            this.remoteContributorService, this.remoteLicenseService, this.remoteUserService);

    if (!FileTool.ensureFolderExists(this.storageDir)) {
      throw new SolidifyRuntimeException("Cannot create " + this.storageDir);
    }
  }

  @AfterEach
  public void purgeData() {
    FileTool.deleteFolder(this.workingDir);
    FileTool.deleteFolder(this.storageDir);
  }

  public void storePublicationTest(Publication publication) throws Exception {
    this.storageService.checkMetadata(publication);
    this.storageService.generateExportMetadata(publication);
    this.storageService.storePublication(publication);
  }

  protected Publication getPublication(Path source, String aipName, AouMetadataVersion version) {
    final Publication publication = new Publication();
    return publication;
  }

  protected void runTests(String filePrefix) {
    for (final AouMetadataVersion version : AouMetadataVersion.values()) {
      try {
        this.storePublicationTest(this.getPublication(Paths.get("src", "test", "resources", "aip").toAbsolutePath(), filePrefix, version));
      } catch (final Exception e) {

      }
    }
  }

}
