/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - MetadataTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.xml.bind.JAXBElement;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.LabeledLanguageService;
import ch.unige.aou.business.PersonService;
import ch.unige.aou.business.PublicationSubSubtypeService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.business.PublicationTypeService;
import ch.unige.aou.business.ResearchGroupService;
import ch.unige.aou.business.StructureService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.ContributorRole;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeContributorRole;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.model.settings.LabeledLanguage;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.xml.deposit.v1.Citations;
import ch.unige.aou.model.xml.deposit.v1.ContainerIdentifiers;
import ch.unige.aou.model.xml.deposit.v1.Dataset;
import ch.unige.aou.model.xml.deposit.v1.Rights;
import ch.unige.aou.model.xml.deposit.v2_4.Abstracts;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructures;
import ch.unige.aou.model.xml.deposit.v2_4.Classifications;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Collections;
import ch.unige.aou.model.xml.deposit.v2_4.Container;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.Contributors;
import ch.unige.aou.model.xml.deposit.v2_4.Corrections;
import ch.unige.aou.model.xml.deposit.v2_4.Datasets;
import ch.unige.aou.model.xml.deposit.v2_4.Dates;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v2_4.File;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.model.xml.deposit.v2_4.Files;
import ch.unige.aou.model.xml.deposit.v2_4.Fundings;
import ch.unige.aou.model.xml.deposit.v2_4.Groups;
import ch.unige.aou.model.xml.deposit.v2_4.Keywords;
import ch.unige.aou.model.xml.deposit.v2_4.Links;
import ch.unige.aou.model.xml.deposit.v2_4.Pages;
import ch.unige.aou.model.xml.deposit.v2_4.Publisher;
import ch.unige.aou.model.xml.deposit.v2_4.Text;
import ch.unige.aou.model.xml.deposit.v2_4.TextLang;
import ch.unige.aou.service.HistoryService;
import ch.unige.aou.service.rest.DuplicateService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;
import ch.unige.aou.service.rest.UnigePersonSearchService;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileTypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedLicenseRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStructureRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public abstract class MetadataTest {

  protected static final String JSON_FORM_DATA_V1_FILE = "angular-form-data-v1.json";
  protected static final String JSON_FORM_DATA_V2_0_FILE = "angular-form-data-v2.0.json";
  protected static final String JSON_FORM_DATA_V2_1_FILE = "angular-form-data-v2.1.json";
  protected static final String JSON_FORM_DATA_V2_2_FILE = "angular-form-data-v2.2.json";
  protected static final String XML_METADATA_V2_2_FILE = "aou-metadata-v2.2.xml";
  protected static final String XML_METADATA_V2_3_FILE = "aou-metadata-v2.3.xml";
  protected static final String JSON_FORM_DATA_V2_3_FILE = "angular-form-data-v2.3.json";
  protected static final String JSON_FORM_DATA_V2_4_FILE = "angular-form-data-v2.4.json";
  protected static final String JSON_FORM_DATA_V2_4_WORKING_PAPER_FILE = "angular-form-data-v2.4-working-paper.json";

  protected static final String JSON_FORM_DATA_V2_0_EMPTY_LIST_FILE = "angular-form-data-v2.0-empty-lists.json";
  protected static final String JSON_FORM_DATA_V2_1_EMPTY_LIST_FILE = "angular-form-data-v2.1-empty-lists.json";
  protected static final String JSON_FORM_DATA_V2_2_EMPTY_LIST_FILE = "angular-form-data-v2.2-empty-lists.json";
  protected static final String JSON_FORM_DATA_V2_3_EMPTY_LIST_FILE = "angular-form-data-v2.3-empty-lists.json";
  protected static final String JSON_FORM_DATA_V2_4_EMPTY_LIST_FILE = "angular-form-data-v2.4-empty-lists.json";

  protected static final String DEPOSIT_TYPE_ARTICLE_ID = "ARTICLE";
  protected static final String DEPOSIT_TYPE_ARTICLE_NAME = "Article";
  protected static final String DEPOSIT_SUBTYPE_ARTICLE_ID = "A1";
  protected static final String DEPOSIT_SUBTYPE_ARTICLE_NAME = "Article scientifique";
  protected static final String DEPOSIT_SUB_SUBTYPE_ARTICLE_ID = "A1-RAPPORT-DE-CAS";
  protected static final String DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME = "Rapport de cas";

  protected static final String DEPOSIT_TYPE_RAPPORT_ID = "RAPPORT";
  protected static final String DEPOSIT_TYPE_RAPPORT_NAME = "Rapport";
  protected static final String DEPOSIT_SUBTYPE_RAPPORT_ID = "R3";
  protected static final String DEPOSIT_SUBTYPE_RAPPORT_NAME = "Working paper";

  protected static final String DEPOSIT_TITLE = "Article title";
  protected static final String LANGUAGE_FRENCH_ID = "FRE";
  protected static final String LANGUAGE_ENGLISH_ID = "ENG";
  protected static final String LANGUAGE_GERMAN_ID = "GER";
  protected static final String RESEARCH_GROUP_ID_1 = "45689-5679-98";
  protected static final String RESEARCH_GROUP_ID_2 = "142-5369-8574";
  protected static final String RESEARCH_GROUP_NAME_1 = "My 1st group";
  protected static final String RESEARCH_GROUP_NAME_2 = "My 2nd group";
  protected static final String RESEARCH_GROUP_CODE_1 = "123";
  protected static final String RESEARCH_GROUP_CODE_2 = "456";
  protected static final String STRUCTURE_ID = "2";
  protected static final String STRUCTURE_C_STRUCT = "1";
  protected static final String STRUCTURE_NAME = "Faculté des sciences";
  protected static final String STRUCTURE_CN_STRUCTC = "547";

  protected AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));

  @Mock
  protected PublicationTypeService publicationTypeService;

  @Mock
  protected PublicationSubtypeService publicationSubtypeService;

  @Mock
  protected PublicationSubSubtypeService publicationSubSubtypeService;

  @Mock
  protected LabeledLanguageService labeledLanguageService;

  @Mock
  protected StructureService structureService;

  @Mock
  protected ResearchGroupService researchGroupService;

  @Mock
  protected ContributorService contributorService;

  @Mock
  protected PersonService personService;

  @Mock
  protected MessageService messageService;

  @Mock
  protected UnigePersonSearchService unigePersonSearchService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected DuplicateService duplicateService;

  @Mock
  protected TrustedStructureRemoteResourceService remoteStructureService;

  @Mock
  protected TrustedPublicationRemoteResourceService remotePublicationService;

  @Mock
  protected TrustedDocumentFileRemoteResourceService remoteDocumentFileService;

  @Mock
  protected TrustedContributorRemoteResourceService remoteContributorService;

  @Mock
  protected TrustedLicenseRemoteResourceService remoteLicenseService;

  @Mock
  protected TrustedUserRemoteResourceService remoteUserService;

  @Mock
  protected DocumentFileService documentFileService;

  @Mock
  protected HistoryService historyService;

  @Mock
  TrustedPublicationSubtypeRemoteResourceService remotePublicationSubtypeService;

  @Mock
  TrustedDocumentFileTypeRemoteResourceService remoteDocumentFileTypeService;

  public void setUp() {

    // Mock the findById method for PublicationTypeService
    final PublicationType type = new PublicationType();
    type.setResId(DEPOSIT_TYPE_ARTICLE_ID);
    type.setName(DEPOSIT_TYPE_ARTICLE_NAME);
    when(this.publicationTypeService.findById(DEPOSIT_TYPE_ARTICLE_ID)).thenReturn(Optional.of(type));
    when(this.publicationTypeService.findByName(DEPOSIT_TYPE_ARTICLE_NAME)).thenReturn(type);
    final PublicationType type2 = new PublicationType();
    type2.setResId(DEPOSIT_TYPE_RAPPORT_ID);
    type2.setName(DEPOSIT_TYPE_RAPPORT_NAME);
    when(this.publicationTypeService.findById(DEPOSIT_TYPE_RAPPORT_ID)).thenReturn(Optional.of(type2));
    when(this.publicationTypeService.findByName(DEPOSIT_TYPE_RAPPORT_NAME)).thenReturn(type2);

    // Mock the findById method for PublicationSubtypeService
    final PublicationSubtype subtype = new PublicationSubtype();
    subtype.setResId(DEPOSIT_SUBTYPE_ARTICLE_ID);
    subtype.setName(DEPOSIT_SUBTYPE_ARTICLE_NAME);
    subtype.setPublicationType(type);
    this.addContributorRoles(subtype);
    when(this.publicationSubtypeService.findById(DEPOSIT_SUBTYPE_ARTICLE_ID)).thenReturn(Optional.of(subtype));
    when(this.publicationSubtypeService.findOne(DEPOSIT_SUBTYPE_ARTICLE_ID)).thenReturn(subtype);
    when(this.publicationSubtypeService.findByName(DEPOSIT_SUBTYPE_ARTICLE_NAME)).thenReturn(Optional.of(subtype));
    final PublicationSubtype subtype2 = new PublicationSubtype();
    subtype2.setResId(DEPOSIT_SUBTYPE_RAPPORT_ID);
    subtype2.setName(DEPOSIT_SUBTYPE_RAPPORT_NAME);
    subtype2.setPublicationType(type2);
    this.addContributorRoles(subtype2);
    when(this.publicationSubtypeService.findById(DEPOSIT_SUBTYPE_RAPPORT_ID)).thenReturn(Optional.of(subtype2));
    when(this.publicationSubtypeService.findOne(DEPOSIT_SUBTYPE_RAPPORT_ID)).thenReturn(subtype2);
    when(this.publicationSubtypeService.findByName(DEPOSIT_SUBTYPE_RAPPORT_NAME)).thenReturn(Optional.of(subtype2));

    // Mock the findById method for PublicationSubSubtypeService
    final PublicationSubSubtype subSubtype = new PublicationSubSubtype();
    subSubtype.setResId(DEPOSIT_SUB_SUBTYPE_ARTICLE_ID);
    subSubtype.setName(DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME);
    when(this.publicationSubSubtypeService.findById(DEPOSIT_SUB_SUBTYPE_ARTICLE_ID)).thenReturn(Optional.of(subSubtype));
    when(this.publicationSubSubtypeService.findByNameAndPublicationSubType(DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME, subtype)).thenReturn(subSubtype);

    // Mock the findById method for LanguageService
    final LabeledLanguage french = new LabeledLanguage();
    french.setResId(LANGUAGE_FRENCH_ID);
    french.setCode(AouConstants.LANG_CODE_FRENCH);
    final LabeledLanguage english = new LabeledLanguage();
    english.setResId(LANGUAGE_ENGLISH_ID);
    english.setCode(AouConstants.LANG_CODE_ENGLISH);
    final LabeledLanguage german = new LabeledLanguage();
    german.setResId(LANGUAGE_GERMAN_ID);
    german.setCode(AouConstants.LANG_CODE_GERMAN);
    when(this.labeledLanguageService.findById(LANGUAGE_FRENCH_ID)).thenReturn(Optional.of(french));
    when(this.labeledLanguageService.findById(LANGUAGE_ENGLISH_ID)).thenReturn(Optional.of(english));
    when(this.labeledLanguageService.findById(LANGUAGE_GERMAN_ID)).thenReturn(Optional.of(german));
    when(this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_FRENCH)).thenReturn(french);
    when(this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_ENGLISH)).thenReturn(english);
    when(this.labeledLanguageService.findByCode(AouConstants.LANG_CODE_GERMAN)).thenReturn(german);

    // researchGroupService
    final ResearchGroup researchGroup1 = new ResearchGroup();
    researchGroup1.setResId(RESEARCH_GROUP_ID_1);
    researchGroup1.setName(RESEARCH_GROUP_NAME_1);
    researchGroup1.setCode(RESEARCH_GROUP_CODE_1);
    researchGroup1.setValidated(true);
    final ResearchGroup researchGroup2 = new ResearchGroup();
    researchGroup2.setResId(RESEARCH_GROUP_ID_2);
    researchGroup2.setName(RESEARCH_GROUP_NAME_2);
    researchGroup2.setCode(RESEARCH_GROUP_CODE_2);
    when(this.researchGroupService.existsById(RESEARCH_GROUP_ID_1)).thenReturn(true);
    when(this.researchGroupService.findById(RESEARCH_GROUP_ID_1)).thenReturn(Optional.of(researchGroup1));
    when(this.researchGroupService.findOne(RESEARCH_GROUP_ID_1)).thenReturn(researchGroup1);
    when(this.researchGroupService.existsById(RESEARCH_GROUP_ID_2)).thenReturn(true);
    when(this.researchGroupService.findById(RESEARCH_GROUP_ID_2)).thenReturn(Optional.of(researchGroup2));
    when(this.researchGroupService.findOne(RESEARCH_GROUP_ID_2)).thenReturn(researchGroup2);

    // Mock the findByCodeStruct method for StructureService
    final Structure structure = new Structure();
    structure.setResId(STRUCTURE_ID);
    structure.setName(STRUCTURE_NAME);
    structure.setCodeStruct(STRUCTURE_C_STRUCT);
    structure.setCnStructC(STRUCTURE_CN_STRUCTC);
    when(this.structureService.findByCodeStruct(STRUCTURE_C_STRUCT)).thenReturn(structure);
    when(this.structureService.findById(STRUCTURE_ID)).thenReturn(Optional.of(structure));
  }

  private void addContributorRoles(PublicationSubtype subtype) {
    List<PublicationSubtypeContributorRole> publicationSubtypeContributorRoles = new ArrayList<>();
    ContributorRole contributorRole1 = new ContributorRole();
    contributorRole1.setValue(AouConstants.CONTRIBUTOR_ROLE_AUTHOR);
    ContributorRole contributorRole2 = new ContributorRole();
    contributorRole2.setValue(AouConstants.CONTRIBUTOR_ROLE_DIRECTOR);
    PublicationSubtypeContributorRole publicationSubtypeContributorRole1 = new PublicationSubtypeContributorRole();
    publicationSubtypeContributorRole1.setPublicationSubtype(subtype);
    publicationSubtypeContributorRole1.setContributorRole(contributorRole1);
    publicationSubtypeContributorRoles.add(publicationSubtypeContributorRole1);
    PublicationSubtypeContributorRole publicationSubtypeContributorRole2 = new PublicationSubtypeContributorRole();
    publicationSubtypeContributorRole2.setPublicationSubtype(subtype);
    publicationSubtypeContributorRole2.setContributorRole(contributorRole2);
    publicationSubtypeContributorRoles.add(publicationSubtypeContributorRole2);
    subtype.setPublicationSubtypeContributorRoles(publicationSubtypeContributorRoles);
  }

  protected String getJsonFormData(AouMetadataVersion version) {
    String file = null;
    switch (version) {
      case V1_0:
        file = JSON_FORM_DATA_V1_FILE;
        break;
      case V2_0:
        file = JSON_FORM_DATA_V2_0_FILE;
        break;
      case V2_1:
        file = JSON_FORM_DATA_V2_1_FILE;
        break;
      case V2_2:
        file = JSON_FORM_DATA_V2_2_FILE;
        break;
      case V2_3:
        file = JSON_FORM_DATA_V2_3_FILE;
        break;
      case V2_4:
        file = JSON_FORM_DATA_V2_4_FILE;
        break;
    }

    try (InputStream jsonStream = new ClassPathResource(file).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read '" + file + "' file: " + e.getMessage());
      return null;
    }
  }

  protected String getJsonFormDataWithEmptyLists(AouMetadataVersion version) {
    String file = null;
    switch (version) {
      case V2_0:
        file = JSON_FORM_DATA_V2_0_EMPTY_LIST_FILE;
        break;
      case V2_1:
        file = JSON_FORM_DATA_V2_1_EMPTY_LIST_FILE;
        break;
      case V2_2:
        file = JSON_FORM_DATA_V2_2_EMPTY_LIST_FILE;
        break;
      case V2_3:
        file = JSON_FORM_DATA_V2_3_EMPTY_LIST_FILE;
        break;
      case V2_4:
        file = JSON_FORM_DATA_V2_4_EMPTY_LIST_FILE;
        break;
      default:
        throw new SolidifyRuntimeException("unable to get Json form data file with empty lists for metadata version " + version);
    }

    try (InputStream jsonStream = new ClassPathResource(file).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read '" + file + "' file: " + e.getMessage());
      return null;
    }
  }

  protected String getXmlMetadata(AouMetadataVersion version) {
    String file = null;
    switch (version) {
      case V2_2:
        file = XML_METADATA_V2_2_FILE;
        break;
      case V2_3:
        file = XML_METADATA_V2_3_FILE;
        break;
      default:
        throw new SolidifyRuntimeException("unable to get xml metadata file for metadata version " + version);
    }

    try (InputStream jsonStream = new ClassPathResource(file).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read '" + file + "' file: " + e.getMessage());
      return null;
    }
  }

  protected void assertEqualValuesV1(ch.unige.aou.model.xml.deposit.v1.DepositDoc depositDoc) {

    /**
     * Type form section
     */
    String type = depositDoc.getType();
    String subtype = depositDoc.getSubtype();
    String subSubtype = depositDoc.getSubsubtype();
    ch.unige.aou.model.xml.deposit.v1.Text title = depositDoc.getTitle();

    assertEquals(DEPOSIT_TITLE, title.getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, title.getLang());
    assertEquals(DEPOSIT_TYPE_ARTICLE_NAME, type);
    assertEquals(DEPOSIT_SUBTYPE_ARTICLE_NAME, subtype);
    assertEquals(DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME, subSubtype);

    /**
     * Contributors form section
     */
    ch.unige.aou.model.xml.deposit.v1.Contributors contributors = depositDoc.getContributors();
    Serializable serializable1 = contributors.getContributorOrCollaboration().get(0);
    assertTrue(serializable1 instanceof ch.unige.aou.model.xml.deposit.v1.Contributor);
    ch.unige.aou.model.xml.deposit.v1.Contributor contributor1 = (ch.unige.aou.model.xml.deposit.v1.Contributor) serializable1;
    assertEquals("unige", contributor1.getInstitution());
    assertEquals("John", contributor1.getFirstname());
    assertEquals("Doe", contributor1.getLastname());
    assertEquals("john.doe@example.com", contributor1.getEmail());
    assertEquals("author", contributor1.getRole().value());
    assertEquals("1234-5678", contributor1.getOrcid());
    assertEquals("12345", contributor1.getCnIndividu());

    Serializable serializable2 = contributors.getContributorOrCollaboration().get(1);
    assertTrue(serializable2 instanceof ch.unige.aou.model.xml.deposit.v1.Contributor);
    ch.unige.aou.model.xml.deposit.v1.Contributor contributor2 = (ch.unige.aou.model.xml.deposit.v1.Contributor) serializable2;
    assertNull(contributor2.getInstitution());
    assertEquals("Jack", contributor2.getFirstname());
    assertEquals("Dalton", contributor2.getLastname());
    assertNull(contributor2.getEmail());
    assertEquals("director", contributor2.getRole().value());
    assertNull(contributor2.getOrcid());
    assertNull(contributor2.getCnIndividu());

    Serializable serializable3 = contributors.getContributorOrCollaboration().get(2);
    assertTrue(serializable3 instanceof ch.unige.aou.model.xml.deposit.v1.Collaboration);
    ch.unige.aou.model.xml.deposit.v1.Collaboration collaboration1 = (ch.unige.aou.model.xml.deposit.v1.Collaboration) serializable3;
    assertEquals("Collaboration-1", collaboration1.getName());

    Serializable serializable4 = contributors.getContributorOrCollaboration().get(3);
    assertTrue(serializable4 instanceof ch.unige.aou.model.xml.deposit.v1.Contributor);
    ch.unige.aou.model.xml.deposit.v1.Contributor contributor3 = (ch.unige.aou.model.xml.deposit.v1.Contributor) serializable4;
    assertNull(contributor3.getInstitution());
    assertEquals("Nicky", contributor3.getFirstname());
    assertEquals("Larson", contributor3.getLastname());
    assertNull(contributor3.getEmail());
    assertEquals("director", contributor3.getRole().value());
    assertNull(contributor3.getOrcid());
    assertNull(contributor3.getCnIndividu());

    Serializable serializable5 = contributors.getContributorOrCollaboration().get(4);
    assertTrue(serializable5 instanceof ch.unige.aou.model.xml.deposit.v1.Collaboration);
    ch.unige.aou.model.xml.deposit.v1.Collaboration collaboration2 = (ch.unige.aou.model.xml.deposit.v1.Collaboration) serializable5;
    assertEquals("Collaboration-2", collaboration2.getName());

    boolean isUnige = depositDoc.isIsUnige();

    /**
     * Description form section
     */
    ch.unige.aou.model.xml.deposit.v1.Container container = depositDoc.getContainer();
    BigInteger volume = container.getVolume();
    String editor = container.getEditor();
    BigInteger issue = container.getIssue();
    String conferenceDate = container.getConferenceDate();
    ContainerIdentifiers containerIdentifiers = container.getIdentifiers();
    String specialIssue = container.getSpecialIssue();
    String place = container.getPlace();
    ch.unige.aou.model.xml.deposit.v1.Text containerTitleText = container.getTitle();

    assertEquals(5, volume.intValue());
    assertEquals("editor name", editor);
    assertEquals(8, issue.intValue());
    assertEquals("2021-01-08", conferenceDate);
    assertEquals("a special issue", specialIssue);
    assertEquals("container place", place);
    assertEquals("container title", containerTitleText.getContent());
    assertEquals(AouConstants.LANG_CODE_FRENCH, containerTitleText.getLang());
    assertEquals("container isbn", containerIdentifiers.getIsbn());
    assertEquals("container issn", containerIdentifiers.getIssn());
    assertEquals("container-doi-1", containerIdentifiers.getDois().getDoi().get(0));

    String note = depositDoc.getNote();
    String edition = depositDoc.getEdition();
    String language = depositDoc.getLanguage();
    String discipline = depositDoc.getDiscipline();
    String awards = depositDoc.getAwards();
    String aouCollection = depositDoc.getAouCollection();
    String publisherVersionUrl = depositDoc.getPublisherVersionUrl();
    String mandator = depositDoc.getMandator();

    assertEquals("a note", note);
    assertEquals("My edition", edition);
    assertEquals(AouConstants.LANG_CODE_FRENCH, language);
    assertEquals("My discipline", discipline);
    assertEquals("My awards", awards);
    assertEquals("Collection UNIGE", aouCollection);
    assertEquals("https://www.example.com/publisher", publisherVersionUrl);
    assertEquals("My mandator", mandator);
    assertTrue(isUnige);

    ch.unige.aou.model.xml.deposit.v1.Keywords keywords = depositDoc.getKeywords();
    assertEquals("test", keywords.getKeyword().get(0));
    assertEquals("Protoporphyrinogen oxidase", keywords.getKeyword().get(1));

    ch.unige.aou.model.xml.deposit.v1.Fundings fundings = depositDoc.getFundings();
    assertEquals("Funder 1", fundings.getFunding().get(0).getFunder());
    assertEquals(1, fundings.getFunding().get(0).getCode().intValue());
    assertEquals("F1", fundings.getFunding().get(0).getAcronym());
    assertEquals("Funder name 1", fundings.getFunding().get(0).getName());
    assertEquals("Funder 2", fundings.getFunding().get(1).getFunder());
    assertEquals(2, fundings.getFunding().get(1).getCode().intValue());
    assertEquals("F2", fundings.getFunding().get(1).getAcronym());
    assertEquals("Funder name 2", fundings.getFunding().get(1).getName());

    ch.unige.aou.model.xml.deposit.v1.Pages pages = depositDoc.getPages();
    Map<String, String> pagesMap = pages.getOtherOrStartOrEnd().stream()
            .collect(Collectors.toMap(element -> element.getName().getLocalPart(), JAXBElement::getValue));
    assertEquals("123", pagesMap.get("start"));
    assertEquals("132", pagesMap.get("end"));
    assertEquals("9 pages", pagesMap.get("other"));

    Citations citations = depositDoc.getCitations();
    assertEquals("Ma citation", citations.getCitation().get(0).getContent());
    assertEquals(AouConstants.LANG_CODE_FRENCH, citations.getCitation().get(0).getLang());
    assertEquals("My citation", citations.getCitation().get(1).getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, citations.getCitation().get(1).getLang());

    ch.unige.aou.model.xml.deposit.v1.Collections collections = depositDoc.getCollections();
    assertEquals("My collection", collections.getCollection().get(0).getName());
    assertEquals(5, collections.getCollection().get(0).getNumber().intValue());
    assertEquals("My 2nd collection", collections.getCollection().get(1).getName());
    assertEquals(10, collections.getCollection().get(1).getNumber().intValue());

    Rights rights = depositDoc.getRights();
    assertNull(rights.getRight().get(0).getContent());
    assertEquals("http://www.example.com/rights1", rights.getRight().get(0).getUri());
    assertNull(rights.getRight().get(1).getContent());
    assertEquals("http://www.example.com/rights2", rights.getRight().get(1).getUri());

    ch.unige.aou.model.xml.deposit.v1.Links links = depositDoc.getLinks();
    assertEquals("web", links.getLink().get(0).getType().value());
    assertEquals("http://www.example.com/publications/1234-4567", links.getLink().get(0).getTarget());
    assertEquals("My link", links.getLink().get(0).getDescription().getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, links.getLink().get(0).getDescription().getLang());

    ch.unige.aou.model.xml.deposit.v1.Abstracts abstracts = depositDoc.getAbstracts();
    assertEquals("Abstract 1", abstracts.getAbstract().get(0).getContent());
    assertEquals(AouConstants.LANG_CODE_FRENCH, abstracts.getAbstract().get(0).getLang());
    assertEquals("Abstract 2", abstracts.getAbstract().get(1).getContent());
    assertEquals(AouConstants.LANG_CODE_GERMAN, abstracts.getAbstract().get(1).getLang());

    ch.unige.aou.model.xml.deposit.v1.DepositIdentifiers identifiers = depositDoc.getIdentifiers();
    assertEquals("2102.00123", identifiers.getArxiv());
    assertEquals("journals/corr/abs-1906-00001", identifiers.getDblp());
    assertEquals("978-1-56619-909-4", identifiers.getIsbn());
    assertEquals("local number", identifiers.getLocalNumber());
    assertEquals("RePEc:ecm:emetrp:v:85:y:2017:i:2:p:525-548", identifiers.getRepec());
    assertEquals("my rero", identifiers.getRero());
    assertEquals("urn:ISSN:0167-6423", identifiers.getUrn());
    assertEquals("10.1038/nphys1170", identifiers.getDois().getDoi().get(0));
    assertEquals("doi-23456", identifiers.getDois().getDoi().get(1));
    assertEquals(BigInteger.valueOf(26795386), identifiers.getPmid());

    ch.unige.aou.model.xml.deposit.v1.Groups groups = depositDoc.getGroups();
    assertEquals("My 1st group", groups.getGroup().get(0).getName());
    assertEquals(123, groups.getGroup().get(0).getCode().intValue());
    assertEquals("My 2nd group", groups.getGroup().get(1).getName());
    assertEquals(456, groups.getGroup().get(1).getCode().intValue());

    ch.unige.aou.model.xml.deposit.v1.Classifications classifications = depositDoc.getClassifications();
    assertEquals("123-456", classifications.getClassification().get(0).getItem());
    assertEquals("Dewey", classifications.getClassification().get(0).getCode());
    assertEquals("H10", classifications.getClassification().get(1).getItem());
    assertEquals("JEL", classifications.getClassification().get(1).getCode());

    ch.unige.aou.model.xml.deposit.v1.TextLang originalTitle = depositDoc.getOriginalTitle();
    assertEquals("My super cool deposit", originalTitle.getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, originalTitle.getLang());

    ch.unige.aou.model.xml.deposit.v1.Publisher publisher = depositDoc.getPublisher();
    Map<String, String> publisherMap = publisher.getNameOrPlace().stream()
            .collect(Collectors.toMap(element -> element.getName().getLocalPart(), JAXBElement::getValue));
    assertEquals("Mon éditeur", publisherMap.get("name"));
    assertEquals("Genève", publisherMap.get("place"));

    Dataset dataset = depositDoc.getDataset();
    Map<String, String> datasetMap = dataset.getUrlOrDoi().stream()
            .collect(Collectors.toMap(element -> element.getName().getLocalPart(), JAXBElement::getValue));
    assertEquals("example-12345", datasetMap.get("doi"));
    assertEquals("http://www.example.com/dataset", datasetMap.get("url"));

    ch.unige.aou.model.xml.deposit.v1.Dates dates = depositDoc.getDates();
    assertEquals("2020", dates.getDate().get(0).getContent());
    assertEquals("FIRST_ONLINE", dates.getDate().get(0).getType().name());
    assertEquals("2021-01-07", dates.getDate().get(1).getContent());
    assertEquals("PUBLICATION", dates.getDate().get(1).getType().name());

    ch.unige.aou.model.xml.deposit.v1.AcademicStructures academicStructures = depositDoc.getAcademicStructures();
    assertEquals("547", academicStructures.getAcademicStructure().get(0).getCode());
    assertEquals("1", academicStructures.getAcademicStructure().get(0).getOldCode());
    assertEquals("Faculté des sciences", academicStructures.getAcademicStructure().get(0).getName());
  }

  protected void assertEqualValuesDefaultVersion(DepositDoc depositDoc) {
    this.assertEqualValuesDefaultVersion(depositDoc, false);
  }

  protected void assertEqualValuesV2_1(ch.unige.aou.model.xml.deposit.v2_1.DepositDoc depositDoc, boolean isUpgradeFromV1) {

    /**
     * Type form section
     */
    String type = depositDoc.getType();
    String subtype = depositDoc.getSubtype();
    String subSubtype = depositDoc.getSubsubtype();
    ch.unige.aou.model.xml.deposit.v2_1.Text title = depositDoc.getTitle();

    assertEquals(DEPOSIT_TITLE, title.getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, title.getLang());
    assertEquals(DEPOSIT_TYPE_ARTICLE_NAME, type);
    assertEquals(DEPOSIT_SUBTYPE_ARTICLE_NAME, subtype);
    assertEquals(DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME, subSubtype);

    /**
     * Contributors form section
     */
    ch.unige.aou.model.xml.deposit.v2_1.Contributors contributors = depositDoc.getContributors();
    Serializable serializable1 = contributors.getContributorOrCollaboration().get(0);
    assertTrue(serializable1 instanceof ch.unige.aou.model.xml.deposit.v2_1.Contributor);
    ch.unige.aou.model.xml.deposit.v2_1.Contributor contributor1 = (ch.unige.aou.model.xml.deposit.v2_1.Contributor) serializable1;
    assertEquals("unige", contributor1.getInstitution());
    assertEquals("John", contributor1.getFirstname());
    assertEquals("Doe", contributor1.getLastname());
    assertEquals("john.doe@example.com", contributor1.getEmail());
    assertEquals("author", contributor1.getRole().value());
    assertEquals("1234-5678", contributor1.getOrcid());
    assertEquals("12345", contributor1.getCnIndividu());

    Serializable serializable2 = contributors.getContributorOrCollaboration().get(1);
    assertTrue(serializable2 instanceof ch.unige.aou.model.xml.deposit.v2_1.Contributor);
    ch.unige.aou.model.xml.deposit.v2_1.Contributor contributor2 = (ch.unige.aou.model.xml.deposit.v2_1.Contributor) serializable2;
    assertNull(contributor2.getInstitution());
    assertEquals("Jack", contributor2.getFirstname());
    assertEquals("Dalton", contributor2.getLastname());
    assertNull(contributor2.getEmail());
    assertEquals("director", contributor2.getRole().value());
    assertNull(contributor2.getOrcid());
    assertNull(contributor2.getCnIndividu());

    Serializable serializable3 = contributors.getContributorOrCollaboration().get(2);
    assertTrue(serializable3 instanceof ch.unige.aou.model.xml.deposit.v2_1.Collaboration);
    ch.unige.aou.model.xml.deposit.v2_1.Collaboration collaboration1 = (ch.unige.aou.model.xml.deposit.v2_1.Collaboration) serializable3;
    assertEquals("Collaboration-1", collaboration1.getName());

    Serializable serializable4 = contributors.getContributorOrCollaboration().get(3);
    assertTrue(serializable4 instanceof ch.unige.aou.model.xml.deposit.v2_1.Contributor);
    ch.unige.aou.model.xml.deposit.v2_1.Contributor contributor3 = (ch.unige.aou.model.xml.deposit.v2_1.Contributor) serializable4;
    assertNull(contributor3.getInstitution());
    assertEquals("Nicky", contributor3.getFirstname());
    assertEquals("Larson", contributor3.getLastname());
    assertNull(contributor3.getEmail());
    assertEquals("director", contributor3.getRole().value());
    assertNull(contributor3.getOrcid());
    assertNull(contributor3.getCnIndividu());

    Serializable serializable5 = contributors.getContributorOrCollaboration().get(4);
    assertTrue(serializable5 instanceof ch.unige.aou.model.xml.deposit.v2_1.Collaboration);
    ch.unige.aou.model.xml.deposit.v2_1.Collaboration collaboration2 = (ch.unige.aou.model.xml.deposit.v2_1.Collaboration) serializable5;
    assertEquals("Collaboration-2", collaboration2.getName());

    /**
     * Description form section
     */
    ch.unige.aou.model.xml.deposit.v2_1.Container container = depositDoc.getContainer();
    String volume = container.getVolume();
    String editor = container.getEditor();
    String issue = container.getIssue();
    String conferenceDate = container.getConferenceDate();
    String specialIssue = container.getSpecialIssue();
    String conferenceTitle = container.getConferenceTitle();
    String conferencePlace = container.getConferencePlace();

    String containerTitle = container.getTitle();

    assertEquals("5", volume);
    assertEquals("editor name", editor);
    assertEquals("8", issue);
    assertEquals("a special issue", specialIssue);
    assertEquals("container title", containerTitle);

    if (!isUpgradeFromV1) {
      // in case of upgrade from v1 to v2 format, conference title is always null
      assertEquals("conference title", conferenceTitle);
      assertEquals("conference place", conferencePlace);
    } else {
      assertEquals("container place", conferencePlace);
    }

    assertEquals("2021-01-08", conferenceDate);

    String note = depositDoc.getNote();
    String edition = depositDoc.getEdition();
    List<String> languages = depositDoc.getLanguages().getLanguage();
    String discipline = depositDoc.getDiscipline();
    String award = depositDoc.getAward();
    String aouCollection = depositDoc.getAouCollection();
    String publisherVersionUrl = depositDoc.getPublisherVersionUrl();
    String mandator = depositDoc.getMandator();

    assertEquals("a note", note);
    assertEquals("My edition", edition);
    assertFalse(languages.isEmpty());
    assertEquals(AouConstants.LANG_CODE_FRENCH, languages.get(0));

    if (!isUpgradeFromV1) {
      // in case of upgrade from v1 to v2 format, only one language can be present
      assertEquals(AouConstants.LANG_CODE_ENGLISH, languages.get(1));
    }

    assertEquals("My discipline", discipline);

    if (!isUpgradeFromV1) {
      assertEquals("My award", award);
    } else {
      // in case of upgrade from v1 to v2 format, awards is renamed to award
      assertEquals("My awards", award);
    }

    assertEquals("Collection UNIGE", aouCollection);
    assertEquals("https://www.example.com/publisher", publisherVersionUrl);
    assertEquals("My mandator", mandator);

    ch.unige.aou.model.xml.deposit.v2_1.Keywords keywords = depositDoc.getKeywords();
    assertEquals("test", keywords.getKeyword().get(0));
    assertEquals("Protoporphyrinogen oxidase", keywords.getKeyword().get(1));

    ch.unige.aou.model.xml.deposit.v2_1.Fundings fundings = depositDoc.getFundings();
    assertEquals("Funder 1", fundings.getFunding().get(0).getFunder());
    assertEquals(1, fundings.getFunding().get(0).getCode().intValue());
    assertEquals("F1", fundings.getFunding().get(0).getAcronym());
    assertEquals("Funder name 1", fundings.getFunding().get(0).getName());
    if (!isUpgradeFromV1) {
      assertEquals("EU", fundings.getFunding().get(0).getJurisdiction());
      assertEquals("H2020", fundings.getFunding().get(0).getProgram());
    }
    assertEquals("Funder 2", fundings.getFunding().get(1).getFunder());
    assertEquals(2, fundings.getFunding().get(1).getCode().intValue());
    assertEquals("F2", fundings.getFunding().get(1).getAcronym());
    assertEquals("Funder name 2", fundings.getFunding().get(1).getName());
    if (!isUpgradeFromV1) {
      assertEquals("CH", fundings.getFunding().get(1).getJurisdiction());
      assertEquals("Program 2", fundings.getFunding().get(1).getProgram());
    }

    ch.unige.aou.model.xml.deposit.v2_1.Pages pages = depositDoc.getPages();
    Map<String, String> pagesMap = pages.getOtherOrStartOrEnd().stream()
            .collect(Collectors.toMap(element -> element.getName().getLocalPart(), JAXBElement::getValue));
    assertEquals("123", pagesMap.get("start"));
    assertEquals("132", pagesMap.get("end"));
    assertEquals("9 pages", pagesMap.get("other"));

    ch.unige.aou.model.xml.deposit.v2_1.Collections collections = depositDoc.getCollections();
    assertEquals("My collection", collections.getCollection().get(0).getName());
    assertEquals(5, collections.getCollection().get(0).getNumber().intValue());
    assertEquals("My 2nd collection", collections.getCollection().get(1).getName());
    assertEquals(10, collections.getCollection().get(1).getNumber().intValue());

    ch.unige.aou.model.xml.deposit.v2_1.Links links = depositDoc.getLinks();
    assertEquals("web", links.getLink().get(0).getType().value());
    assertEquals("http://www.example.com/publications/1234-4567", links.getLink().get(0).getTarget());
    assertEquals("My link", links.getLink().get(0).getDescription().getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, links.getLink().get(0).getDescription().getLang());

    ch.unige.aou.model.xml.deposit.v2_1.Abstracts abstracts = depositDoc.getAbstracts();
    assertEquals("Abstract 1", abstracts.getAbstract().get(0).getContent());
    assertEquals(AouConstants.LANG_CODE_FRENCH, abstracts.getAbstract().get(0).getLang());
    assertEquals("Abstract 2", abstracts.getAbstract().get(1).getContent());
    assertEquals(AouConstants.LANG_CODE_GERMAN, abstracts.getAbstract().get(1).getLang());

    ch.unige.aou.model.xml.deposit.v2_1.DepositIdentifiers identifiers = depositDoc.getIdentifiers();
    assertEquals("2102.00123", identifiers.getArxiv());
    assertEquals("journals/corr/abs-1906-00001", identifiers.getDblp());
    assertEquals("978-1-56619-909-4", identifiers.getIsbn());
    if (!isUpgradeFromV1) {
      assertEquals("1234-5678", identifiers.getIssn());
    } else {
      // in case of upgrade from v1 to v2 format, ISSN is moved from container to identifiers
      assertEquals("container issn", identifiers.getIssn());
    }
    assertEquals("local number", identifiers.getLocalNumber());
    assertEquals("RePEc:ecm:emetrp:v:85:y:2017:i:2:p:525-548", identifiers.getRepec());

    if (!isUpgradeFromV1) {
      assertEquals("9900123456789", identifiers.getMmsid());
    } else {
      // in case of upgrade from v1 to v2 format, rero is renamed to mmsid
      assertEquals("my rero", identifiers.getMmsid());
    }

    assertEquals("urn:ISSN:0167-6423", identifiers.getUrn());
    assertEquals("10.1038/nphys1170", identifiers.getDoi());
    assertEquals(BigInteger.valueOf(26795386), identifiers.getPmid());
    if (!isUpgradeFromV1) {
      // pmcid doesn't exist in v1
      assertEquals("PMC6760482", identifiers.getPmcid());
    }
    ch.unige.aou.model.xml.deposit.v2_1.Groups groups = depositDoc.getGroups();
    if (!isUpgradeFromV1) {
      // group resId doesn't exist in v1
      assertEquals(RESEARCH_GROUP_ID_1, groups.getGroup().get(0).getResId());
      assertEquals(RESEARCH_GROUP_ID_2, groups.getGroup().get(1).getResId());
    }
    assertEquals(RESEARCH_GROUP_NAME_1, groups.getGroup().get(0).getName());
    assertEquals(RESEARCH_GROUP_CODE_1, groups.getGroup().get(0).getCode().toString());
    assertEquals(RESEARCH_GROUP_NAME_2, groups.getGroup().get(1).getName());
    assertEquals(RESEARCH_GROUP_CODE_2, groups.getGroup().get(1).getCode().toString());

    ch.unige.aou.model.xml.deposit.v2_1.Classifications classifications = depositDoc.getClassifications();
    assertEquals("123-456", classifications.getClassification().get(0).getItem());
    assertEquals("Dewey", classifications.getClassification().get(0).getCode());
    assertEquals("H10", classifications.getClassification().get(1).getItem());
    assertEquals("JEL", classifications.getClassification().get(1).getCode());

    ch.unige.aou.model.xml.deposit.v2_1.TextLang originalTitle = depositDoc.getOriginalTitle();
    assertEquals("My super cool deposit", originalTitle.getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, originalTitle.getLang());

    ch.unige.aou.model.xml.deposit.v2_1.Publisher publisher = depositDoc.getPublisher();
    Map<String, String> publisherMap = publisher.getNameOrPlace().stream()
            .collect(Collectors.toMap(element -> element.getName().getLocalPart(), JAXBElement::getValue));
    assertEquals("Mon éditeur", publisherMap.get("name"));
    assertEquals("Genève", publisherMap.get("place"));

    ch.unige.aou.model.xml.deposit.v2_1.Datasets datasets = depositDoc.getDatasets();
    assertEquals("http://www.example.com/dataset", datasets.getUrl().get(0));
    assertEquals("https://doi.org/example-12345", datasets.getUrl().get(1));
    if (!isUpgradeFromV1) {
      // in case of upgrade from v1 to v2 format, only 2 can be present
      assertEquals("https://alaxos.org/dataset", datasets.getUrl().get(2));
    }
    ch.unige.aou.model.xml.deposit.v2_1.Dates dates = depositDoc.getDates();
    assertEquals("2020", dates.getDate().get(0).getContent());
    assertEquals("FIRST_ONLINE", dates.getDate().get(0).getType().name());
    assertEquals("2021-01-07", dates.getDate().get(1).getContent());
    assertEquals("PUBLICATION", dates.getDate().get(1).getType().name());

    ch.unige.aou.model.xml.deposit.v2_1.AcademicStructures academicStructures = depositDoc.getAcademicStructures();
    if (!isUpgradeFromV1) {
      assertEquals("2", academicStructures.getAcademicStructure().get(0).getResId());
    }
    assertEquals("1", academicStructures.getAcademicStructure().get(0).getCode());
    assertEquals("Faculté des sciences", academicStructures.getAcademicStructure().get(0).getName());

    if (!isUpgradeFromV1) {
      // corrections do not exists in V1
      ch.unige.aou.model.xml.deposit.v2_1.Corrections corrections = depositDoc.getCorrections();
      assertEquals(2, corrections.getCorrection().size());
      assertEquals("a 1st correction", corrections.getCorrection().get(0).getNote());
      assertEquals("a 2nd correction", corrections.getCorrection().get(1).getNote());
      assertEquals("10.1109/5.745673", corrections.getCorrection().get(1).getDoi());
      assertEquals("469713", corrections.getCorrection().get(1).getPmid());

      // doctor infos do not exist in V1
      assertEquals("john.doe@example.com", depositDoc.getDoctorEmail());
      assertEquals("Somewhere", depositDoc.getDoctorAddress());
    }
  }

  protected void assertEqualValuesDefaultVersion(DepositDoc depositDoc, boolean isUpgradeFromV1) {

    /**
     * Type form section
     */
    String type = depositDoc.getType();
    String subtype = depositDoc.getSubtype();
    String subSubtype = depositDoc.getSubsubtype();
    Text title = depositDoc.getTitle();

    assertEquals(DEPOSIT_TITLE, title.getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, title.getLang());
    assertEquals(DEPOSIT_TYPE_ARTICLE_NAME, type);
    assertEquals(DEPOSIT_SUBTYPE_ARTICLE_NAME, subtype);
    assertEquals(DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME, subSubtype);

    /**
     * Contributors form section
     */
    Contributors contributors = depositDoc.getContributors();
    Serializable serializable1 = contributors.getContributorOrCollaboration().get(0);
    assertTrue(serializable1 instanceof Contributor);
    Contributor contributor1 = (Contributor) serializable1;
    assertEquals("unige", contributor1.getInstitution());
    assertEquals("John", contributor1.getFirstname());
    assertEquals("Doe", contributor1.getLastname());
    assertEquals("john.doe@example.com", contributor1.getEmail());
    assertEquals("author", contributor1.getRole().value());
    assertEquals("1234-5678", contributor1.getOrcid());
    assertEquals("12345", contributor1.getCnIndividu());

    Serializable serializable2 = contributors.getContributorOrCollaboration().get(1);
    assertTrue(serializable2 instanceof Contributor);
    Contributor contributor2 = (Contributor) serializable2;
    assertNull(contributor2.getInstitution());
    assertEquals("Jack", contributor2.getFirstname());
    assertEquals("Dalton", contributor2.getLastname());
    assertNull(contributor2.getEmail());
    assertEquals("director", contributor2.getRole().value());
    assertNull(contributor2.getOrcid());
    assertNull(contributor2.getCnIndividu());

    Serializable serializable3 = contributors.getContributorOrCollaboration().get(2);
    assertTrue(serializable3 instanceof Collaboration);
    Collaboration collaboration1 = (Collaboration) serializable3;
    assertEquals("Collaboration-1", collaboration1.getName());

    Serializable serializable4 = contributors.getContributorOrCollaboration().get(3);
    assertTrue(serializable4 instanceof Contributor);
    Contributor contributor3 = (Contributor) serializable4;
    assertNull(contributor3.getInstitution());
    assertEquals("Nicky", contributor3.getFirstname());
    assertEquals("Larson", contributor3.getLastname());
    assertNull(contributor3.getEmail());
    assertEquals("director", contributor3.getRole().value());
    assertNull(contributor3.getOrcid());
    assertNull(contributor3.getCnIndividu());

    Serializable serializable5 = contributors.getContributorOrCollaboration().get(4);
    assertTrue(serializable5 instanceof Collaboration);
    Collaboration collaboration2 = (Collaboration) serializable5;
    assertEquals("Collaboration-2", collaboration2.getName());

    /**
     * Description form section
     */
    Container container = depositDoc.getContainer();
    String volume = container.getVolume();
    String editor = container.getEditor();
    String issue = container.getIssue();
    String conferenceDate = container.getConferenceDate();
    String specialIssue = container.getSpecialIssue();
    String conferenceSubtitle = container.getConferenceSubtitle();
    String conferencePlace = container.getConferencePlace();

    String containerTitle = container.getTitle();

    assertEquals("5", volume);
    assertEquals("editor name", editor);
    assertEquals("8", issue);
    assertEquals("a special issue", specialIssue);
    assertEquals("container title", containerTitle);

    if (!isUpgradeFromV1) {
      // in case of upgrade from v1 to v2 format, conference title is always null
      assertEquals("conference subtitle", conferenceSubtitle);
      assertEquals("conference place", conferencePlace);
    } else {
      assertEquals("container place", conferencePlace);
    }

    assertEquals("2021-01-08", conferenceDate);

    String note = depositDoc.getNote();
    String edition = depositDoc.getEdition();
    List<String> languages = depositDoc.getLanguages().getLanguage();
    String discipline = depositDoc.getDiscipline();
    String award = depositDoc.getAward();
    String aouCollection = depositDoc.getAouCollection();
    String publisherVersionUrl = depositDoc.getPublisherVersionUrl();
    String mandator = depositDoc.getMandator();

    assertEquals("a note", note);
    assertEquals("My edition", edition);
    assertFalse(languages.isEmpty());
    assertEquals(AouConstants.LANG_CODE_FRENCH, languages.get(0));

    if (!isUpgradeFromV1) {
      // in case of upgrade from v1 to v2 format, only one language can be present
      assertEquals(AouConstants.LANG_CODE_ENGLISH, languages.get(1));
    }

    assertEquals("My discipline", discipline);

    if (!isUpgradeFromV1) {
      assertEquals("My award", award);
    } else {
      // in case of upgrade from v1 to v2 format, awards is renamed to award
      assertEquals("My awards", award);
    }

    assertEquals("Collection UNIGE", aouCollection);
    assertEquals("https://www.example.com/publisher", publisherVersionUrl);
    assertEquals("My mandator", mandator);

    Keywords keywords = depositDoc.getKeywords();
    assertEquals("test", keywords.getKeyword().get(0));
    assertEquals("Protoporphyrinogen oxidase", keywords.getKeyword().get(1));

    Fundings fundings = depositDoc.getFundings();
    assertEquals("Funder 1", fundings.getFunding().get(0).getFunder());
    assertEquals("1", fundings.getFunding().get(0).getCode());
    assertEquals("F1", fundings.getFunding().get(0).getAcronym());
    assertEquals("Funder name 1", fundings.getFunding().get(0).getName());
    if (!isUpgradeFromV1) {
      assertEquals("EU", fundings.getFunding().get(0).getJurisdiction());
      assertEquals("H2020", fundings.getFunding().get(0).getProgram());
    }
    assertEquals("Funder 2", fundings.getFunding().get(1).getFunder());
    assertEquals("2", fundings.getFunding().get(1).getCode());
    assertEquals("F2", fundings.getFunding().get(1).getAcronym());
    assertEquals("Funder name 2", fundings.getFunding().get(1).getName());
    if (!isUpgradeFromV1) {
      assertEquals("CH", fundings.getFunding().get(1).getJurisdiction());
      assertEquals("Program 2", fundings.getFunding().get(1).getProgram());
    }

    Pages pages = depositDoc.getPages();
    Map<String, String> pagesMap = pages.getPagingOrOther().stream()
            .collect(Collectors.toMap(element -> element.getName().getLocalPart(), JAXBElement::getValue));
    assertEquals("123-132", pagesMap.get("paging"));
    assertEquals("9 pages", pagesMap.get("other"));

    Collections collections = depositDoc.getCollections();
    assertEquals("My collection", collections.getCollection().get(0).getName());
    assertEquals("5", collections.getCollection().get(0).getNumber());
    assertEquals("My 2nd collection", collections.getCollection().get(1).getName());
    assertEquals("10", collections.getCollection().get(1).getNumber());

    Links links = depositDoc.getLinks();
    assertEquals("web", links.getLink().get(0).getType().value());
    assertEquals("http://www.example.com/publications/1234-4567", links.getLink().get(0).getTarget());
    assertEquals("My link", links.getLink().get(0).getDescription().getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, links.getLink().get(0).getDescription().getLang());

    Abstracts abstracts = depositDoc.getAbstracts();
    assertEquals("Abstract 1", abstracts.getAbstract().get(0).getContent());
    assertEquals(AouConstants.LANG_CODE_FRENCH, abstracts.getAbstract().get(0).getLang());
    assertEquals("Abstract 2", abstracts.getAbstract().get(1).getContent());
    assertEquals(AouConstants.LANG_CODE_GERMAN, abstracts.getAbstract().get(1).getLang());

    DepositIdentifiers identifiers = depositDoc.getIdentifiers();
    assertEquals("2102.00123", identifiers.getArxiv());
    assertEquals("journals/corr/abs-1906-00001", identifiers.getDblp());
    assertEquals("978-1-56619-909-4", identifiers.getIsbn());
    if (!isUpgradeFromV1) {
      assertEquals("1234-5678", identifiers.getIssn());
    } else {
      // in case of upgrade from v1 to v2 format, ISSN is moved from container to identifiers
      assertEquals("container issn", identifiers.getIssn());
    }
    assertEquals("local number", identifiers.getLocalNumber());
    assertEquals("RePEc:ecm:emetrp:v:85:y:2017:i:2:p:525-548", identifiers.getRepec());

    if (!isUpgradeFromV1) {
      assertEquals("9900123456789", identifiers.getMmsid());
    } else {
      // in case of upgrade from v1 to v2 format, rero is renamed to mmsid
      assertEquals("my rero", identifiers.getMmsid());
    }

    assertEquals("urn:ISSN:0167-6423", identifiers.getUrn());
    assertEquals("10.1038/nphys1170", identifiers.getDoi());
    assertEquals(BigInteger.valueOf(26795386), identifiers.getPmid());
    if (!isUpgradeFromV1) {
      // pmcid doesn't exist in v1
      assertEquals("PMC6760482", identifiers.getPmcid());
    }
    Groups groups = depositDoc.getGroups();
    if (!isUpgradeFromV1) {
      // group resId doesn't exist in v1
      assertEquals(RESEARCH_GROUP_ID_1, groups.getGroup().get(0).getResId());
      assertEquals(RESEARCH_GROUP_ID_2, groups.getGroup().get(1).getResId());
    }
    assertEquals(RESEARCH_GROUP_NAME_1, groups.getGroup().get(0).getName());
    assertEquals(RESEARCH_GROUP_CODE_1, groups.getGroup().get(0).getCode().toString());
    assertEquals(RESEARCH_GROUP_NAME_2, groups.getGroup().get(1).getName());
    assertEquals(RESEARCH_GROUP_CODE_2, groups.getGroup().get(1).getCode().toString());

    Classifications classifications = depositDoc.getClassifications();
    assertEquals("123-456", classifications.getClassification().get(0).getItem());
    assertEquals("Dewey", classifications.getClassification().get(0).getCode());
    assertEquals("H10", classifications.getClassification().get(1).getItem());
    assertEquals("JEL", classifications.getClassification().get(1).getCode());

    TextLang originalTitle = depositDoc.getOriginalTitle();
    assertEquals("My super cool deposit", originalTitle.getContent());
    assertEquals(AouConstants.LANG_CODE_ENGLISH, originalTitle.getLang());

    Publisher publisher = depositDoc.getPublisher();
    Map<String, String> publisherMap = publisher.getNameOrPlace().stream()
            .collect(Collectors.toMap(element -> element.getName().getLocalPart(), JAXBElement::getValue));
    assertEquals("Mon éditeur", publisherMap.get("name"));
    assertEquals("Genève", publisherMap.get("place"));

    Datasets datasets = depositDoc.getDatasets();
    assertEquals("http://www.example.com/dataset", datasets.getUrl().get(0));
    assertEquals("https://doi.org/example-12345", datasets.getUrl().get(1));
    if (!isUpgradeFromV1) {
      // in case of upgrade from v1 to v2 format, only 2 can be present
      assertEquals("https://alaxos.org/dataset", datasets.getUrl().get(2));
    }
    Dates dates = depositDoc.getDates();
    assertEquals("2020", dates.getDate().get(0).getContent());
    assertEquals("FIRST_ONLINE", dates.getDate().get(0).getType().name());
    assertEquals("2021-01-07", dates.getDate().get(1).getContent());
    assertEquals("PUBLICATION", dates.getDate().get(1).getType().name());

    AcademicStructures academicStructures = depositDoc.getAcademicStructures();
    if (!isUpgradeFromV1) {
      assertEquals("2", academicStructures.getAcademicStructure().get(0).getResId());
    }
    assertEquals("1", academicStructures.getAcademicStructure().get(0).getCode());
    assertEquals("Faculté des sciences", academicStructures.getAcademicStructure().get(0).getName());

    if (!isUpgradeFromV1) {
      // corrections do not exists in V1
      Corrections corrections = depositDoc.getCorrections();
      assertEquals(2, corrections.getCorrection().size());
      assertEquals("a 1st correction", corrections.getCorrection().get(0).getNote());
      assertEquals("a 2nd correction", corrections.getCorrection().get(1).getNote());
      assertEquals("10.1109/5.745673", corrections.getCorrection().get(1).getDoi());
      assertEquals("469713", corrections.getCorrection().get(1).getPmid());

      // doctor infos do not exist in V1
      assertEquals("john.doe@example.com", depositDoc.getDoctorEmail());
      assertEquals("Somewhere", depositDoc.getDoctorAddress());
    }

    Files files = depositDoc.getFiles();
    if (files != null && files.getFile() != null && !files.getFile().isEmpty()) {
      assertEquals(2, files.getFile().size());
      List<FileType> fileTypes = files.getFile().stream().map(File::getType).collect(Collectors.toList());
      assertTrue(fileTypes.contains(FileType.ARTICLE_ACCEPTED_VERSION));

      // check that "Report (Accepted version)" has been renamed into "Report"
      assertTrue(fileTypes.contains(FileType.REPORT));
    }
  }
}
