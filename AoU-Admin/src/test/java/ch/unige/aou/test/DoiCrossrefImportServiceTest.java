/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - DoiCrossrefImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.DoiCrossrefImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
class DoiCrossrefImportServiceTest {

  private static final String CROSSREF_XML_ARTICLE_METADATA_FILE = "test-crossref-article-response.xml";
  private static final String ARTICLE_DOI = "10.1126/science.1175088";
  private static final String CROSSREF_XML_PROCEEDINGS_CHAPTER_METADATA_FILE = "test-crossref-proceedings-chapter-response.xml";
  private static final String PROCEEDINGS_CHAPTER_DOI = "10.1145/3552327.3552332";
  private static final String CROSSREF_XML_BOOK_METADATA_FILE = "test-crossref-book-response.xml";
  private static final String BOOK_DOI = "10.1007/978-3-030-94212-0";
  private static final String CROSSREF_XML_REPORT_METADATA_FILE = "test-crossref-report-response.xml";
  private static final String REPORT_DOI = "10.57071/948rpn";

  private DoiCrossrefImportService doiCrossrefImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;

  @Mock
  protected DocumentFileService documentFileService;

  @Mock
  protected DocumentFileTypeService documentFileTypeService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.doiCrossrefImportService = new DoiCrossrefImportService(aouProperties, this.messageService, this.metadataService,
            this.contributorService, this.documentFileService, this.documentFileTypeService, this.journalTitleRemoteService,
            this.licenseService);
  }

  @Test
  void fillArticleDepositDocTest() {

    try {
      Document crossrefDoc = XMLTool.parseXML(this.getCrossrefXmlMetadata(CROSSREF_XML_ARTICLE_METADATA_FILE));
      DepositDoc depositDoc = new DepositDoc();
      this.doiCrossrefImportService.fillDepositDoc(ARTICLE_DOI, depositDoc, crossrefDoc);

      assertEquals(AouConstants.DEPOSIT_TYPE_ARTICLE_ID, depositDoc.getType());
      assertEquals(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, depositDoc.getSubtype());
      assertEquals("An ER-Mitochondria Tethering Complex Revealed by a Synthetic Biology Screen", depositDoc.getTitle().getContent());
      assertEquals(AouConstants.LANG_CODE_ENGLISH, depositDoc.getTitle().getLang());
      assertEquals(AouConstants.LANG_CODE_ENGLISH, depositDoc.getLanguages().getLanguage().get(0));
      assertNotNull(depositDoc.getDates());
      assertNotNull(depositDoc.getDates().getDate());
      assertFalse(depositDoc.getDates().getDate().isEmpty());
      assertEquals("2009-07-24", depositDoc.getDates().getDate().get(0).getContent());
      assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
      assertEquals("2009-06-25", depositDoc.getDates().getDate().get(1).getContent());
      assertEquals(DateTypes.FIRST_ONLINE, depositDoc.getDates().getDate().get(1).getType());
      assertNotNull(depositDoc.getContributors());
      assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
      assertEquals(7, depositDoc.getContributors().getContributorOrCollaboration().size());
      assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
      assertEquals("Kornmann", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
      assertEquals("Benoît", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
      assertTrue(depositDoc.getPages().getPagingOrOther().get(0).getName().toString().contains("paging"));
      assertEquals("477-481", depositDoc.getPages().getPagingOrOther().get(0).getValue());
      assertEquals(ARTICLE_DOI, depositDoc.getIdentifiers().getDoi());
      assertEquals("0036-8075", depositDoc.getIdentifiers().getIssn());
      assertEquals("https://www.sciencemag.org/lookup/doi/10.1126/science.1175088", depositDoc.getPublisherVersionUrl());
      assertEquals("Science", depositDoc.getContainer().getTitle());
      assertEquals("325", depositDoc.getContainer().getVolume().toString());
      assertEquals("5939", depositDoc.getContainer().getIssue().toString());
      assertEquals("National Key Research and Development Program of China", depositDoc.getFundings().getFunding().get(0).getFunder());
      assertEquals("Centre of Excellence for Core to Crust Fluid Systems, Australian Research Council",
              depositDoc.getFundings().getFunding().get(1).getFunder());
      assertEquals("1526", depositDoc.getFundings().getFunding().get(1).getCode());

    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching DOI '" + ARTICLE_DOI + "' on Crossref", e);
    }
  }

  @Test
  void fillProceedingsChapterDepositDocTest() {

    try {
      Document crossrefDoc = XMLTool.parseXML(this.getCrossrefXmlMetadata(CROSSREF_XML_PROCEEDINGS_CHAPTER_METADATA_FILE));
      DepositDoc depositDoc = new DepositDoc();
      this.doiCrossrefImportService.fillDepositDoc(PROCEEDINGS_CHAPTER_DOI, depositDoc, crossrefDoc);

      assertEquals(AouConstants.DEPOSIT_TYPE_CONFERENCE_ID, depositDoc.getType());
      assertEquals(AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME, depositDoc.getSubtype());
      assertEquals("Analyzing the challenges of an assistive application’ integration", depositDoc.getTitle().getContent());
      assertEquals(AouConstants.LANG_CODE_ENGLISH, depositDoc.getTitle().getLang());
      assertEquals(AouConstants.LANG_CODE_ENGLISH, depositDoc.getLanguages().getLanguage().get(0));
      assertNotNull(depositDoc.getDates());
      assertNotNull(depositDoc.getDates().getDate());
      assertFalse(depositDoc.getDates().getDate().isEmpty());
      assertEquals("2022-10-04", depositDoc.getDates().getDate().get(0).getContent());
      assertEquals(DateTypes.FIRST_ONLINE, depositDoc.getDates().getDate().get(0).getType());
      assertEquals("2022-10-04", depositDoc.getDates().getDate().get(1).getContent());
      assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(1).getType());

      assertNotNull(depositDoc.getContributors());
      assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
      assertEquals(5, depositDoc.getContributors().getContributorOrCollaboration().size());
      assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
      assertEquals("Larribau", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getLastname());
      assertEquals("Robert", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getFirstname());
      assertEquals(AuthorRole.AUTHOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getRole());
      assertEquals("Hôpitaux Universitaires de Genève, Switzerland",
              ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(3)).getInstitution());

      assertEquals(PROCEEDINGS_CHAPTER_DOI, depositDoc.getIdentifiers().getDoi());
      assertEquals("https://dl.acm.org/doi/10.1145/3552327.3552332", depositDoc.getPublisherVersionUrl());
      assertEquals("ECCE 2022: 33rd European Conference on Cognitive Ergonomics", depositDoc.getContainer().getTitle());
      assertEquals(1, depositDoc.getPublisher().getNameOrPlace().size());
      assertEquals("ACM", depositDoc.getPublisher().getNameOrPlace().get(0).getValue());
      assertEquals("04 10 2022 07 10 2022", depositDoc.getContainer().getConferenceDate());
      assertEquals("Kaiserslautern Germany", depositDoc.getContainer().getConferencePlace());

    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching DOI '" + PROCEEDINGS_CHAPTER_DOI + "' on Crossref", e);
    }
  }

  @Test
  void fillBookDepositDocTest() {

    try {
      Document crossrefDoc = XMLTool.parseXML(this.getCrossrefXmlMetadata(CROSSREF_XML_BOOK_METADATA_FILE));
      DepositDoc depositDoc = new DepositDoc();
      this.doiCrossrefImportService.fillDepositDoc(BOOK_DOI, depositDoc, crossrefDoc);

      assertEquals(AouConstants.DEPOSIT_TYPE_LIVRE_ID, depositDoc.getType());
      assertEquals(AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME, depositDoc.getSubtype());

      assertEquals("Quantifying Quality of Life: Incorporating Daily Life into Medicine", depositDoc.getTitle().getContent());
      assertEquals(AouConstants.LANG_CODE_ENGLISH, depositDoc.getTitle().getLang());
      assertEquals(AouConstants.LANG_CODE_ENGLISH, depositDoc.getLanguages().getLanguage().get(0));

      assertNotNull(depositDoc.getDates());
      assertNotNull(depositDoc.getDates().getDate());
      assertFalse(depositDoc.getDates().getDate().isEmpty());
      assertEquals("2022", depositDoc.getDates().getDate().get(0).getContent());
      assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());

      assertNotNull(depositDoc.getContributors());
      assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
      assertEquals(2, depositDoc.getContributors().getContributorOrCollaboration().size());
      assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
      assertEquals("Wac", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
      assertEquals("Katarzyna", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
      assertEquals(AuthorRole.EDITOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getRole());

      assertEquals(BOOK_DOI, depositDoc.getIdentifiers().getDoi());
      assertEquals("978-3-030-94211-3", depositDoc.getIdentifiers().getIsbn());
      assertEquals("https://link.springer.com/10.1007/978-3-030-94212-0", depositDoc.getPublisherVersionUrl());
      assertEquals(2, depositDoc.getPublisher().getNameOrPlace().size());
      assertEquals("Springer International Publishing", depositDoc.getPublisher().getNameOrPlace().get(0).getValue());
      assertEquals("Cham", depositDoc.getPublisher().getNameOrPlace().get(1).getValue());

    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching DOI '" + PROCEEDINGS_CHAPTER_DOI + "' on Crossref", e);
    }
  }

  @Test
  void fillReportDepositDocTest() {

    try {
      Document crossrefDoc = XMLTool.parseXML(this.getCrossrefXmlMetadata(CROSSREF_XML_REPORT_METADATA_FILE));
      DepositDoc depositDoc = new DepositDoc();
      this.doiCrossrefImportService.fillDepositDoc(REPORT_DOI, depositDoc, crossrefDoc);

      assertEquals(AouConstants.DEPOSIT_TYPE_RAPPORT_ID, depositDoc.getType());
      assertEquals(AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME, depositDoc.getSubtype());

      assertEquals("Proactivité et réactivité: deux orientations pour concevoir des dispositifs visant le développement",
              depositDoc.getTitle().getContent());
      assertNull(depositDoc.getTitle().getLang());
      assertNull(depositDoc.getLanguages());

      assertNotNull(depositDoc.getDates());
      assertNotNull(depositDoc.getDates().getDate());
      assertFalse(depositDoc.getDates().getDate().isEmpty());
      assertEquals("2021-02-08", depositDoc.getDates().getDate().get(0).getContent());
      assertEquals(DateTypes.FIRST_ONLINE, depositDoc.getDates().getDate().get(0).getType());

      assertNotNull(depositDoc.getContributors());
      assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
      assertEquals(3, depositDoc.getContributors().getContributorOrCollaboration().size());
      assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
      assertEquals("Flandin", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
      assertEquals("Simon", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
      assertEquals(AuthorRole.AUTHOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getRole());
      assertEquals("0000-0002-6332-1499", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getOrcid());
      assertEquals("Poizat", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getLastname());
      assertEquals("Germain", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getFirstname());
      assertEquals(AuthorRole.AUTHOR, ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getRole());
      assertEquals("0000-0002-6560-3342", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getOrcid());
      assertEquals("Université de Genève", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(1)).getInstitution());

      assertEquals(REPORT_DOI, depositDoc.getIdentifiers().getDoi());
      assertEquals("https://www.foncsi.org/fr/publications/cahiers-securite-industrielle", depositDoc.getPublisherVersionUrl());
      assertEquals(1, depositDoc.getPublisher().getNameOrPlace().size());
      assertEquals("Fondation pour une culture de sécurité industrielle", depositDoc.getPublisher().getNameOrPlace().get(0).getValue());

    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      throw new SolidifyRuntimeException("An error occurred when searching DOI '" + PROCEEDINGS_CHAPTER_DOI + "' on Crossref", e);
    }
  }

  private String getCrossrefXmlMetadata(String fileName) {
    try (InputStream jsonStream = new ClassPathResource(fileName).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + CROSSREF_XML_ARTICLE_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
