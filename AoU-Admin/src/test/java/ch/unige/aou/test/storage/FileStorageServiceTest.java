/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - FileStorageServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.storage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;

import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.service.storage.FileStorageService;

@ActiveProfiles({ "storage-file", "sec-noauth" })
@ExtendWith(SpringExtension.class)
class FileStorageServiceTest extends StorageServiceTest {

  @Mock
  private LicenseService licenseService;

  @Mock
  private DocumentFileTypeService documentFileTypeService;

  @Mock
  private DocumentFileService documentFileService;

  @BeforeEach
  public void setup() {
    super.setUp();

    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.storageService = new FileStorageService(aouProperties, this.messageService, this.publicationService, this.userService,
            this.metadataService, this.documentFileTypeService, this.licenseService, this.documentFileService);
  }

  @Test
  @Disabled
  void storageTest() {
    this.runTests("faust-1072055880-aip-zip");
  }
}
