/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - RomeoServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.service.rest.RomeoService;

public class RomeoServiceTest {

  protected static final String JSON_ROMEO_API_DATA_FILE = "test-romeo-response.json";

  protected AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
  RomeoService romeoService = new RomeoService(this.aouProperties);

  @Test
  void generateRomeoDataTest() {
    String jsonRomeoApiData = this.getJsonRomeoApiData();
    String aouRomeoJson = this.romeoService.transformRomeoJsonDataIntoAouJson("0094-5765", jsonRomeoApiData);
    assertNotNull(aouRomeoJson);

    try {
      ObjectMapper mapper = new ObjectMapper();
      JsonNode aouRomeoNode = mapper.readTree(aouRomeoJson);

      assertTrue(aouRomeoNode.has("issn"));
      assertEquals("0094-5765", aouRomeoNode.get("issn").asText());

      assertTrue(aouRomeoNode.has("journalType"));
      assertEquals("Hybrid. This journal contains both subscribers-only and Open Access articles.", aouRomeoNode.get("journalType").asText());

      assertTrue(aouRomeoNode.has("listedInDoaj"));
      assertFalse(aouRomeoNode.get("listedInDoaj").booleanValue());

      assertTrue(aouRomeoNode.has("title"));
      assertEquals("Acta Astronautica", aouRomeoNode.get("title").asText());

      assertTrue(aouRomeoNode.has("sherpaRomeoUrl"));
      assertEquals("https://v2.sherpa.ac.uk/id/publication/171", aouRomeoNode.get("sherpaRomeoUrl").asText());

      assertTrue(aouRomeoNode.has("footer"));
      ArrayNode footerNode = (ArrayNode) aouRomeoNode.get("footer");
      assertEquals(2, footerNode.size());
      assertEquals("If your research project was supported by a funder, more advantageous conditions can apply (see link below).",
              footerNode.get(0).asText());
      assertEquals("Whole set of conditions for this journal can be found in Sherpa Romeo: {sherpaRomeoUrl}.",
              footerNode.get(1).asText());

      assertTrue(aouRomeoNode.has("versions"));
      JsonNode versionsNode = aouRomeoNode.get("versions");
      assertTrue(versionsNode.has("accepted"));
      assertTrue(versionsNode.has("submitted"));
      assertTrue(versionsNode.has("published"));

      /**
       * Accepted versions
       */
      JsonNode acceptedNode = versionsNode.get("accepted");
      assertTrue(acceptedNode.has("version"));
      assertEquals("accepted version", acceptedNode.get("version").asText());
      assertTrue(acceptedNode.has("policies"));
      ArrayNode acceptedPoliciesNode = (ArrayNode) acceptedNode.get("policies");
      assertEquals(1, acceptedPoliciesNode.size());
      JsonNode acceptedPolicyNode = acceptedPoliciesNode.get(0);
      assertTrue(acceptedPolicyNode.has("embargo"));
      assertEquals("24 months", acceptedPolicyNode.get("embargo").asText());
      assertTrue(acceptedPolicyNode.has("licences"));
      ArrayNode licencesNode = (ArrayNode) acceptedPolicyNode.get("licences");
      assertEquals("CC BY-NC-ND", licencesNode.get(0).asText());
      assertTrue(acceptedPolicyNode.has("oaDiffusion"));
      JsonNode oaDiffusionNode = acceptedPolicyNode.get("oaDiffusion");
      assertTrue(oaDiffusionNode.has("icon"));
      assertEquals("authorized", oaDiffusionNode.get("icon").asText());
      assertTrue(oaDiffusionNode.has("text"));
      assertEquals("free access authorized", oaDiffusionNode.get("text").asText());

      /**
       * Published versions
       */
      JsonNode publishedNode = versionsNode.get("published");
      assertTrue(publishedNode.has("version"));
      assertEquals("published version", publishedNode.get("version").asText());
      assertTrue(publishedNode.has("policies"));
      ArrayNode publishedPoliciesNode = (ArrayNode) publishedNode.get("policies");
      assertEquals(1, publishedPoliciesNode.size());
      JsonNode publishedPolicyNode = publishedPoliciesNode.get(0);
      assertTrue(publishedPolicyNode.has("licences"));
      ArrayNode publishedLicencesNode = (ArrayNode) publishedPolicyNode.get("licences");
      assertEquals("CC BY-NC-ND \"4.0\"", publishedLicencesNode.get(0).asText());
      assertEquals("CC BY", publishedLicencesNode.get(1).asText());
      assertFalse(publishedPolicyNode.has("embargo"));
      assertTrue(publishedPolicyNode.has("oaDiffusion"));
      JsonNode publishedOaDiffusionNode = publishedPolicyNode.get("oaDiffusion");
      assertTrue(publishedOaDiffusionNode.has("icon"));
      assertEquals("question", publishedOaDiffusionNode.get("icon").asText());
      assertTrue(publishedOaDiffusionNode.has("text"));
      assertEquals(
              "Free access authorized, as long as an APC fee was paid for the article or the document includes an <em>Open Access</em> or <em>Creative Commons</em> mention",
              publishedOaDiffusionNode.get("text").asText());

      /**
       * Submitted version
       */
      JsonNode submittedNode = versionsNode.get("submitted");
      assertTrue(submittedNode.has("version"));
      assertEquals("submitted version", submittedNode.get("version").asText());
      assertTrue(submittedNode.has("policies"));
      ArrayNode submittedPoliciesNode = (ArrayNode) submittedNode.get("policies");
      assertEquals(1, submittedPoliciesNode.size());
      JsonNode submittedPolicyNode = submittedPoliciesNode.get(0);
      assertFalse(submittedPolicyNode.has("licences"));
      assertFalse(submittedPolicyNode.has("embargo"));
      assertTrue(submittedPolicyNode.has("oaDiffusion"));
      JsonNode submittedOaDiffusionNode = submittedPolicyNode.get("oaDiffusion");
      assertTrue(submittedOaDiffusionNode.has("icon"));
      assertEquals("authorized", submittedOaDiffusionNode.get("icon").asText());
      assertTrue(submittedOaDiffusionNode.has("text"));
      assertEquals("free access authorized", submittedOaDiffusionNode.get("text").asText());

    } catch (JsonProcessingException e) {
      fail("unable to read Romeo API file");
    }
  }

  private String getJsonRomeoApiData() {
    try (InputStream jsonStream = new ClassPathResource(JSON_ROMEO_API_DATA_FILE).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + JSON_ROMEO_API_DATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
