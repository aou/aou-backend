/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - GoogleIsbnImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.GoogleIsbnImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class GoogleIsbnImportServiceTest {

  private static final String INSPIRE_HEP_METADATA_FILE = "test-isbn-response.json";
  private static final String ISBN_ID = "9781913453640";
  private static final String ISBN_ABSTRACT = "Captures the unique moment in time created by the Covid 19 pandemic and uses this as a lens to explore contemporary issues for social work education and practice. The 2020 pandemic provided an unprecedented moment of global crisis, which placed health and social care at the forefront of the national agenda. The lockdown, social distancing measures and rapid move to online working created multiple challenges and safeguarding concerns for social work education and practice, whilst the unparalleled death rate exacerbated pre-existing problems with communicating openly about death and bereavement. Many of these issues were already at the surface of social work practice and education and this book examines how the health crisis has exposed these, whilst acting as a potential catalyst for change. This book acts as a testament to the historical moment whilst providing a forum for drawing together discussion from contemporary educators, practitioners and users of social work services.";

  private GoogleIsbnImportService googleIsbnImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;
  @Mock
  protected DocumentFileTypeService documentFileTypeService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected LicenseService licenseService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.googleIsbnImportService = new GoogleIsbnImportService(aouProperties, this.messageService, this.metadataService,
            this.contributorService, this.journalTitleRemoteService, this.licenseService);
  }

  @Test
  public void fillDepositDocTest() {

    JSONObject isbnJson = this.getInspireHepMetadata();
    DepositDoc depositDoc = new DepositDoc();
    this.googleIsbnImportService.fillDepositDoc(ISBN_ID, depositDoc, isbnJson);

    assertEquals(ISBN_ID, depositDoc.getIdentifiers().getIsbn());
    assertEquals("Social Work and Covid 19", depositDoc.getTitle().getContent());
    assertNotNull(depositDoc.getDates());
    assertNotNull(depositDoc.getDates().getDate());
    assertTrue(!depositDoc.getDates().getDate().isEmpty());
    assertEquals("2021-01-11", depositDoc.getDates().getDate().get(0).getContent());
    assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
    assertNotNull(depositDoc.getContributors());
    assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
    assertEquals(1, depositDoc.getContributors().getContributorOrCollaboration().size());
    assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
    assertEquals("Denise", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
    assertEquals("Turner", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
    assertEquals(1, depositDoc.getKeywords().getKeyword().size());
    assertEquals("Social Science", depositDoc.getKeywords().getKeyword().get(0));
    assertEquals("112", depositDoc.getPages().getPagingOrOther().get(0).getValue());
    assertEquals(ISBN_ABSTRACT, depositDoc.getAbstracts().getAbstract().get(0).getContent());
    assertEquals(AouConstants.DEPOSIT_TYPE_LIVRE_NAME, depositDoc.getType());
    assertEquals(1, depositDoc.getLanguages().getLanguage().size());
    assertEquals("eng", depositDoc.getLanguages().getLanguage().get(0));

  }

  private JSONObject getInspireHepMetadata() {
    try (InputStream jsonStream = new ClassPathResource(INSPIRE_HEP_METADATA_FILE).getInputStream()) {
      JSONTokener tokener = new JSONTokener(jsonStream);
      return new JSONObject(tokener);
    } catch (IOException e) {
      fail("Unable to read " + INSPIRE_HEP_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
