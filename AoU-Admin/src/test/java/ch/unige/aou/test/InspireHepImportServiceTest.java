/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Admin - InspireHepImportServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.web.util.HtmlUtils.htmlUnescape;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.business.ContributorService;
import ch.unige.aou.business.DocumentFileService;
import ch.unige.aou.business.DocumentFileTypeService;
import ch.unige.aou.business.LicenseService;
import ch.unige.aou.business.PublicationSubtypeService;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.MetadataService;
import ch.unige.aou.service.metadata.imports.InspireHepImportService;
import ch.unige.aou.service.rest.JournalTitleRemoteService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class InspireHepImportServiceTest {

  private static final String INSPIRE_HEP_METADATA_FILE = "test-inspirehep-response.json";
  private static final String ARXIV_ID = "1106.0522";
  private static final String INSPIRE_HEP_ABSTRACT = "MadGraph 5 is the new version of the MadGraph matrix element generator, written in the "
          + "Python programming language. It implements a number of new, efficient algorithms that provide improved performance and functionality "
          + "in all aspects of the program. It features a new user interface, several new output formats including C++ process libraries for "
          + "Pythia 8, and full compatibility with FeynRules for new physics models implementation, allowing for event generation for any model"
          + " that can be written in the form of a Lagrangian. MadGraph 5 builds on the same philosophy as the previous versions, and its design "
          + "allows it to be used as a collaborative platform where theoretical, phenomenological and simulation projects can be developed and "
          + "then distributed to the high-energy community. We describe the ideas and the most important developments of the code and illustrate "
          + "its capabilities through a few simple phenomenological examples.";

  private InspireHepImportService inspireHepImportService;

  @Mock
  private MessageService messageService;

  @Mock
  protected MetadataService metadataService;

  @Mock
  protected ContributorService contributorService;
  @Mock
  protected DocumentFileTypeService documentFileTypeService;

  @Mock
  protected JournalTitleRemoteService journalTitleRemoteService;

  @Mock
  protected PublicationSubtypeService publicationSubtypeService;

  @Mock
  protected LicenseService licenseService;

  @Mock
  protected DocumentFileService documentFileService;

  @BeforeEach
  public void setUp() {
    AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));
    this.inspireHepImportService = new InspireHepImportService(aouProperties, this.messageService, this.metadataService,
            this.contributorService, this.documentFileTypeService, this.journalTitleRemoteService, this.publicationSubtypeService,
            this.licenseService, this.documentFileService);
  }

  @Test
  public void fillDepositDocTest() {

    JSONObject inspireHepJson = this.getInspireHepMetadata();
    DepositDoc depositDoc = new DepositDoc();
    this.inspireHepImportService.fillDepositDoc(depositDoc, inspireHepJson);

    assertEquals(ARXIV_ID, depositDoc.getIdentifiers().getArxiv());
    assertEquals("MadGraph 5 : Going Beyond", depositDoc.getTitle().getContent());
    assertNotNull(depositDoc.getDates());
    assertNotNull(depositDoc.getDates().getDate());
    assertTrue(!depositDoc.getDates().getDate().isEmpty());
    assertEquals("2011", depositDoc.getDates().getDate().get(0).getContent());
    assertEquals(DateTypes.PUBLICATION, depositDoc.getDates().getDate().get(0).getType());
    assertEquals("JHEP", depositDoc.getContainer().getTitle());
    assertEquals("06", depositDoc.getContainer().getVolume());
    assertNotNull(depositDoc.getContributors());
    assertNotNull(depositDoc.getContributors().getContributorOrCollaboration());
    assertEquals(5, depositDoc.getContributors().getContributorOrCollaboration().size());
    assertTrue(depositDoc.getContributors().getContributorOrCollaboration().get(0) instanceof Contributor);
    assertEquals("Johan", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
    assertEquals("Alwall", ((Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
    assertEquals(12, depositDoc.getKeywords().getKeyword().size());
    assertEquals("QCD Phenomenology", depositDoc.getKeywords().getKeyword().get(0));
    assertEquals("performance", depositDoc.getKeywords().getKeyword().get(1));
    assertEquals("programming", depositDoc.getKeywords().getKeyword().get(2));
    assertEquals("37 pages, 5 figures, 7 tables", depositDoc.getNote());
    assertEquals("10.1007/JHEP06(2011)128", depositDoc.getIdentifiers().getDoi());
    assertEquals(htmlUnescape(INSPIRE_HEP_ABSTRACT.replaceAll("[\\t\\n\\r]+", " ").replaceAll("\\s+", " ")),
            depositDoc.getAbstracts().getAbstract().get(0).getContent());

  }

  private JSONObject getInspireHepMetadata() {
    try (InputStream jsonStream = new ClassPathResource(INSPIRE_HEP_METADATA_FILE).getInputStream()) {
      JSONTokener tokener = new JSONTokener(jsonStream);
      return new JSONObject(tokener);
    } catch (IOException e) {
      fail("Unable to read " + INSPIRE_HEP_METADATA_FILE + ": " + e.getMessage());
      return null;
    }
  }
}
