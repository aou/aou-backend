= Contributing to the AoU code

The AoU code is released under the https://spdx.org/licenses/GPL-2.0-or-later.html[GNU General Public License v2.0 or later] license.

If you would like to contribute to it, or hack the code, this document will help you get started.

== Code of Conduct

This project adheres to the Contributor Covenant link:CODE_OF_CONDUCT.adoc[code of conduct]. By participating, you are expected to uphold this code. +
Please report unacceptable behavior to mailto:eresearch-opensource@unige.ch[,Code of Conduct].


== Using GitLab Issues

We use https://gitlab.unige.ch/aou/aou-backend/-/issues[GitLab] issues to track bugs and enhancements:

* for https://gitlab.unige.ch/aou/aou-backend/-/issues/new?issuable_template=Bug[bugs]
* for https://gitlab.unige.ch/aou/aou-backend/-/issues/new?issuable_template=Feature[new features]
* for https://gitlab.unige.ch/aou/aou-backend/-/issues/new?issuable_template=Improvement[enhancements]
* for https://gitlab.unige.ch/aou/aou-backend/-/issues/new?issuable_template=Question[others requests]

If you are reporting a bug, please help to speed up problem diagnosis by providing as much information as possible.
Ideally, that would include a small sample project that reproduces the problem.

== Reporting Security Vulnerabilities

If you think you have found a security vulnerability in AoU code please *DO NOT* disclose it publicly until we've had a chance to fix it.

You must create https://gitlab.unige.ch/aou/aou-backend/-/issues/new?issuable_template=Security-Vulnerability[security issue] to inform the communauty and detail the vulnerability.

== Code Conventions and Housekeeping

None of these are essential for a merge request, but they will all help. They can also be added after the original merge request, but before a merge.

* Respect the code style of link:https://gitlab.unige.ch/solidify/solidify-backend[Solidify framework]
* A few unit tests would help a lot as well -- someone has to do it.
* If no one else is using your branch, please rebase it against the current main branch (or another target branch in the project).
* When writing a commit message please follow https://ec.europa.eu/component-library/v1.14.2/ec/docs/conventions/git/[these conventions].
** Commit message format:
+
----
<type>(<scope>): <subject>
----


.Commit message quick reference
****

[cols="1a,1,1a"]
|===
|*`<type>`*
|*`<scope>`*
|*`<subject>`*

| _Mandatory_
| _Optional_
| _Mandatory_

|
* `feat:` A new feature
* `fix:` A bug fix
* `docs:` Documentation changes
* `style:` Changes that do not affect the meaning of the code (white space, formatting, missing semicolons, etc)
* `refactor:` A code change that neither fixes a bug nor adds a feature
* `perf:` A code change that improves performance
* `test:` Adding missing tests
* `build:` Changes that affect the build system, CI configuration or external dependencies
* `chore:` Changes to the build process or auxiliary tools and libraries such as documentation generation

|The scope could be anything specifying the place of the commit change.
|
The subject contains succinct a description of the change:

* use the imperative, present tense: “change” not “changed” nor “changes”
* don’t capitalize the first letter
* no dot (.) at the end
|=== 

****

