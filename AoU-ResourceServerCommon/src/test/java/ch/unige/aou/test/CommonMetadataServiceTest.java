/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - CommonMetadataServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeDTO;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileType;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileTypeDTO;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.service.CommonMetadataService;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileTypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedLicenseRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStructureRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class CommonMetadataServiceTest {
  protected AouProperties aouProperties = new AouProperties(new SolidifyProperties(new GitInfoProperties()));

  private CommonMetadataService commonMetadataService;

  @Mock
  MessageService messageService;

  @Mock
  TrustedStructureRemoteResourceService remoteStructureService;

  @Mock
  TrustedPublicationRemoteResourceService remotePublicationService;

  @Mock
  TrustedDocumentFileRemoteResourceService remoteDocumentFileService;

  @Mock

  TrustedPublicationSubtypeRemoteResourceService remotePublicationSubtypeService;

  @Mock
  TrustedDocumentFileTypeRemoteResourceService remoteDocumentFileTypeService;

  @Mock
  TrustedContributorRemoteResourceService remoteContributorService;

  @Mock
  TrustedLicenseRemoteResourceService remoteLicenseService;

  @Mock
  TrustedUserRemoteResourceService remoteUserService;

  @BeforeEach
  public void setUp() {
    this.commonMetadataService = new CommonMetadataService(this.aouProperties, this.messageService, this.remoteStructureService,
            this.remotePublicationService, this.remoteDocumentFileService, this.remotePublicationSubtypeService,
            this.remoteDocumentFileTypeService, this.remoteContributorService, this.remoteLicenseService, this.remoteUserService);

    final String documentFileTypeResId1 = "a7644ab4-df93-4adc-b066-96127d8aad8b";
    final String documentFileTypeValue1 = "Article (Accepted version)";
    final String documentFileTypeResId2 = "c06982ae-27e9-4de8-821a-2899531ce6b3";
    final String documentFileTypeValue2 = "Appendix";

    final String publicationSubtypeResId = "A1";
    final String publicationSubtypeName = "Article scientifique";

    final List<PublicationSubtypeDTO> publicationSubtypes = new ArrayList<>();
    PublicationSubtype publicationSubtype = new PublicationSubtype();
    publicationSubtype.setResId(publicationSubtypeResId);
    publicationSubtype.setName(publicationSubtypeName);
    publicationSubtypes.add(new PublicationSubtypeDTO(publicationSubtype));
    RestCollection<PublicationSubtypeDTO> publicationSubtypesRestCollection = new RestCollection<>(publicationSubtypes);
    when(this.remotePublicationSubtypeService.findAllDTO(any())).thenReturn(publicationSubtypesRestCollection);

    final List<DocumentFileType> documentFileTypes = new ArrayList<>();
    DocumentFileType documentFileType1 = new DocumentFileType();
    documentFileType1.setResId(documentFileTypeResId1);
    documentFileType1.setValue(documentFileTypeValue1);
    documentFileTypes.add(documentFileType1);
    DocumentFileType documentFileType2 = new DocumentFileType();
    documentFileType2.setResId(documentFileTypeResId2);
    documentFileType2.setValue(documentFileTypeValue2);
    documentFileTypes.add(documentFileType2);
    RestCollection<DocumentFileType> documentFileTypesRestCollection = new RestCollection<>(documentFileTypes);
    when(this.remoteDocumentFileTypeService.findAllWithCache(any())).thenReturn(documentFileTypesRestCollection);

    final List<PublicationSubtypeDocumentFileTypeDTO> publicationSubtypeDocumentFileTypes = new ArrayList<>();
    PublicationSubtypeDocumentFileType publicationSubtypeDocumentFileType1 = new PublicationSubtypeDocumentFileType();
    publicationSubtypeDocumentFileType1.setLevel(DocumentFileType.FileTypeLevel.PRINCIPAL);
    publicationSubtypeDocumentFileType1.setDocumentFileType(documentFileType1);
    publicationSubtypeDocumentFileType1.setPublicationSubtype(publicationSubtype);
    publicationSubtypeDocumentFileTypes.add(new PublicationSubtypeDocumentFileTypeDTO(publicationSubtypeDocumentFileType1));

    PublicationSubtypeDocumentFileType publicationSubtypeDocumentFileType2 = new PublicationSubtypeDocumentFileType();
    publicationSubtypeDocumentFileType2.setLevel(DocumentFileType.FileTypeLevel.SECONDARY);
    publicationSubtypeDocumentFileType2.setDocumentFileType(documentFileType2);
    publicationSubtypeDocumentFileType2.setPublicationSubtype(publicationSubtype);
    publicationSubtypeDocumentFileTypes.add(new PublicationSubtypeDocumentFileTypeDTO(publicationSubtypeDocumentFileType2));

    RestCollection<PublicationSubtypeDocumentFileTypeDTO> psdftCollection = new RestCollection<>(publicationSubtypeDocumentFileTypes);
    when(this.remotePublicationSubtypeService.findAllPublicationSubtypeDocumentFileTypesDTO(publicationSubtypeResId,
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by("sortValue")))).thenReturn(psdftCollection);
  }

  @Test
  void testPublicationIndexEntry() {
    String metadataFile = "aou-metadata-v2.4.xml";
    String xmlMetadata = null;
    try (InputStream inputStream = new ClassPathResource(metadataFile).getInputStream()) {
      xmlMetadata = FileTool.toString(inputStream);
    } catch (IOException e) {
      fail("Unable to read '" + metadataFile + "' file: " + e.getMessage());
    }

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    OffsetDateTime now = OffsetDateTime.now();
    publication.setStatus(Publication.PublicationStatus.IN_PROGRESS);
    publication.setLastStatusUpdate(now);
    publication.getCreation().setWhen(now);
    publication.getCreation().setWho("Albert Dupontel");
    publication.getLastUpdate().setWhen(now);
    publication.getLastUpdate().setWho("Albert Dupontel");
    Person creator = new Person();
    creator.setFirstName("Joe");
    creator.setLastName("Dohn");
    publication.setCreator(creator);

    PublicationIndexEntry indexEntry = this.commonMetadataService.getPublicationIndexEntry(publication, publication.getMetadata());
    assertNotNull(indexEntry);

    Map<String, Object> metadata = indexEntry.getMetadata();
    assertEquals("Article title", metadata.get("title"));
    assertEquals("Article scientifique", metadata.get("subtype"));
    assertEquals("Rapport de cas", metadata.get("subsubtype"));

    List<String> languages = (List<String>) metadata.get("languages");
    assertTrue(languages.contains("fre"));

    List<Map<String, String>> dates = (List<Map<String, String>>) metadata.get("dates");
    assertNotNull(dates);
    assertEquals(2, dates.size());
    Optional<Map<String, String>> dateOpt1 = dates.stream().filter(d -> d.get("date").equals("2020")).findFirst();
    Optional<Map<String, String>> dateOpt2 = dates.stream().filter(d -> d.get("date").equals("2021-01-07")).findFirst();
    assertTrue(dateOpt1.isPresent());
    assertEquals("FIRST_ONLINE", dateOpt1.get().get("type"));
    assertTrue(dateOpt2.isPresent());
    assertEquals("PUBLICATION", dateOpt2.get().get("type"));

    HashSet<Integer> years = (HashSet) metadata.get("years");
    assertTrue(years.contains(2020));
    assertTrue(years.contains(2021));

    List<Map<String, String>> contributors = (List<Map<String, String>>) metadata.get("contributors");
    assertNotNull(contributors);
    assertEquals(5, contributors.size());
    Map<String, String> contributor = contributors.get(0);
    assertEquals("John", contributor.get("firstName"));
    assertEquals("Doe", contributor.get("lastName"));
    assertEquals("author", contributor.get("role"));
    assertEquals("12345", contributor.get("cnIndividu"));
    assertEquals("unige", contributor.get("institution"));
    assertEquals("1234-5678", contributor.get("orcid"));
    assertEquals("john.doe@example.com", contributor.get("email"));
    assertEquals("contributor", contributor.get("type"));
    contributor = contributors.get(1);
    assertEquals("Jack", contributor.get("firstName"));
    assertEquals("Dalton", contributor.get("lastName"));
    assertEquals("director", contributor.get("role"));
    assertEquals("contributor", contributor.get("type"));
    contributor = contributors.get(2);
    assertEquals("Collaboration-1", contributor.get("name"));
    assertEquals("collaboration", contributor.get("type"));
    contributor = contributors.get(3);
    assertEquals("Nicky", contributor.get("firstName"));
    assertEquals("Larson", contributor.get("lastName"));
    assertEquals("director", contributor.get("role"));
    assertEquals("contributor", contributor.get("type"));
    contributor = contributors.get(4);
    assertEquals("Collaboration-2", contributor.get("name"));
    assertEquals("collaboration", contributor.get("type"));

    HashSet<String> unigeDirectors = (HashSet<String>) metadata.get(AouConstants.INDEX_FIELD_UNIGE_DIRECTORS);
    assertNotNull(unigeDirectors);
    assertEquals(1, unigeDirectors.size());
    assertTrue(unigeDirectors.contains("451278"));

    List<MetadataFile> metadataFiles = indexEntry.getFiles();
    assertNotNull(metadataFiles);
    assertEquals(2, metadataFiles.size());
    MetadataFile file = metadataFiles.get(0);
    assertEquals("lorem-ipsum.pdf", file.getName());
    assertEquals(new BigInteger("8381"), file.getSize());
    assertEquals("6c284558d9d0778606da228f2e961a0b03a484e56d2462b0e97a75222560c1ba", file.getChecksum());
  }
}
