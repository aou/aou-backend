/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AouAsyncConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableAsync
public class AouAsyncConfig extends AsyncConfigurerSupport {

  private long defaultAsyncExecutionTimeout;

  public AouAsyncConfig(AouProperties aouProperties) {
    this.defaultAsyncExecutionTimeout = aouProperties.getParameters().getDefaultAsyncExecutionTimeout();
  }

  /**
   * Configure async support for Spring MVC.
   */
  @Bean
  public WebMvcConfigurer webMvcConfigurerAdapter(AsyncTaskExecutor taskExecutor) {

    return new WebMvcConfigurer() {
      @Override
      public void configureAsyncSupport(AsyncSupportConfigurer configurer) {

        // set no time limit for async executions to allow downloading very large files with
        // StreamingResponseBody
        configurer.setDefaultTimeout(AouAsyncConfig.this.defaultAsyncExecutionTimeout).setTaskExecutor(taskExecutor);
      }
    };
  }
}
