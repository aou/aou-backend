/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - HistoryRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.StatusHistory;

public interface HistoryRepository extends SolidifyRepository<StatusHistory> {

  // *************
  // ** Queries **
  // *************

  public List<StatusHistory> findByResId(@Param(AouConstants.DB_RES_ID) String resId);

  public Page<StatusHistory> findByResId(@Param(AouConstants.DB_RES_ID) String resId, Pageable pageable);

  @Query("SELECT s FROM StatusHistory s WHERE s.resId = :" + AouConstants.DB_RES_ID + " ORDER BY s.changeTime DESC")
  public List<StatusHistory> findStatusHistoryOrderByLastDate(@Param(AouConstants.DB_RES_ID) String resId);

  @Query("SELECT CASE WHEN (COUNT(s) > 0) THEN TRUE ELSE FALSE END FROM StatusHistory s WHERE s.resId=:resId AND s.status=:status")
  boolean hasBeenInStatus(String resId, String status);

  @Query("SELECT s FROM StatusHistory s WHERE s.resId=:resId AND s.status=:status AND s.changeTime<=:before ORDER BY s.changeTime DESC")
  List<StatusHistory> findAllWithStatusBeforeOrEqualsOrderByChangeTimeDesc(String resId, String status, OffsetDateTime before);

  @Query("SELECT s FROM StatusHistory s WHERE s.resId=:resId AND s.status=:status AND s.changeTime>:after ORDER BY s.changeTime ASC")
  List<StatusHistory> findAllWithStatusAfterOrderByChangeTimeAsc(String resId, String status, OffsetDateTime after);

  // ***************
  // ** Relations **
  // ***************

}
