/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - DepositDocV1Adapter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import static ch.unige.aou.AouConstants.DATE_TYPE_DEFENSE;
import static ch.unige.aou.AouConstants.DATE_TYPE_FIRST_ONLINE;
import static ch.unige.aou.AouConstants.DATE_TYPE_IMPRIMATUR;
import static ch.unige.aou.AouConstants.DATE_TYPE_PUBLICATION;

import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.MetadataClassification;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataCorrection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.MetadataFunding;
import ch.unige.aou.model.publication.MetadataLink;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.xml.deposit.v1.AcademicStructure;
import ch.unige.aou.model.xml.deposit.v1.AcademicStructures;
import ch.unige.aou.model.xml.deposit.v1.AuthorRole;
import ch.unige.aou.model.xml.deposit.v1.Collaboration;
import ch.unige.aou.model.xml.deposit.v1.Collection;
import ch.unige.aou.model.xml.deposit.v1.Container;
import ch.unige.aou.model.xml.deposit.v1.Contributor;
import ch.unige.aou.model.xml.deposit.v1.DateWithType;
import ch.unige.aou.model.xml.deposit.v1.DepositDoc;
import ch.unige.aou.model.xml.deposit.v1.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v1.File;
import ch.unige.aou.model.xml.deposit.v1.Funding;
import ch.unige.aou.model.xml.deposit.v1.Group;
import ch.unige.aou.model.xml.deposit.v1.Groups;
import ch.unige.aou.model.xml.deposit.v1.Link;
import ch.unige.aou.model.xml.deposit.v1.LinkTypes;
import ch.unige.aou.model.xml.deposit.v1.ObjectFactory;
import ch.unige.aou.model.xml.deposit.v1.Text;

public class DepositDocV1Adapter implements DepositDocAdapter {

  ObjectFactory factory = new ObjectFactory();

  @Override
  public String getType(Object depositDocObj) {
    return ((DepositDoc) depositDocObj).getType();
  }

  @Override
  public void setType(Object depositDocObj, String value) {
    ((DepositDoc) depositDocObj).setType(value);
  }

  @Override
  public String getSubtype(Object depositDocObj) {
    return ((DepositDoc) depositDocObj).getSubtype();
  }

  @Override
  public void setSubtype(Object depositDocObj, String value) {
    ((DepositDoc) depositDocObj).setSubtype(value);
  }

  @Override
  public String getSubSubtype(Object depositDocObj) {
    return ((DepositDoc) depositDocObj).getSubsubtype();
  }

  @Override
  public void setSubSubtype(Object depositDocObj, String value) {
    ((DepositDoc) depositDocObj).setSubsubtype(value);
  }

  @Override
  public String getTitleContent(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getTitle() != null) {
      return depositDoc.getTitle().getContent();
    } else {
      return null;
    }
  }

  @Override
  public List<String> getLanguages(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    //return the only one language as a list
    String language = depositDoc.getLanguage();
    return List.of(language);
  }

  @Override
  public void setLanguages(Object depositDocObj, List<String> languagesList) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    depositDoc.setLanguage(languagesList.get(0));
  }

  @Override
  public String getTitleLang(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getTitle() != null) {
      return depositDoc.getTitle().getLang();
    } else {
      return null;
    }
  }

  @Override
  public void setTitleLang(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getTitle() == null) {
      depositDoc.setTitle(new Text());
    }
    depositDoc.getTitle().setLang(value);
  }

  @Override
  public void setISSN(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() == null) {
      depositDoc.setContainer(new Container());
    }
    if (depositDoc.getContainer().getTitle() == null) {
      depositDoc.getContainer().setTitle(this.factory.createText());
    }
    depositDoc.getContainer().getTitle().setContent(value);
  }

  @Override
  public String getISBN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getIsbn();
    } else {
      return null;
    }
  }

  @Override
  public String getArxivId(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getArxiv();
    } else {
      return null;
    }
  }

  @Override
  public String getPmcId(Object depositDocObj) {
    return null;
  }

  @Override
  public String getMmsid(Object depositDocObj) {
    return null;
  }

  @Override
  public String getRepec(Object depositDocObj) {
    return null;
  }

  @Override
  public String getDblp(Object depositDocObj) {
    return null;
  }

  @Override
  public String getLocalNumber(Object depositDocObj) {
    return null;
  }

  @Override
  public String getURN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getUrn();
    } else {
      return null;
    }
  }

  @Override
  public void setURN(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() == null) {
      depositDoc.setIdentifiers(new DepositIdentifiers());
    }
    depositDoc.getIdentifiers().setUrn(value);
  }

  @Override
  public String getISSN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null && depositDoc.getContainer().getIdentifiers() != null) {
      return depositDoc.getContainer().getIdentifiers().getIssn();
    } else {
      return null;
    }
  }

  @Override
  public String getDOI(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null && depositDoc.getIdentifiers().getDois() != null
            && depositDoc.getIdentifiers().getDois().getDoi() != null && !depositDoc.getIdentifiers().getDois().getDoi().isEmpty()) {
      return depositDoc.getIdentifiers().getDois().getDoi().get(0);
    } else {
      return null;
    }
  }

  @Override
  public void setDOI(Object depositDocObj, String value) {
    // Do nothing in V1
  }

  @Override
  public BigInteger getPmid(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getPmid();
    } else {
      return null;
    }
  }

  @Override
  public List<AbstractContributor> getContributors(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<AbstractContributor> contributors = new ArrayList<>();
    if (depositDoc.getContributors() != null) {
      List<Serializable> contributorOrCollaborations = depositDoc.getContributors().getContributorOrCollaboration();
      for (Serializable serializable : contributorOrCollaborations) {
        if (serializable instanceof Contributor) {
          ContributorDTO contributor = new ContributorDTO();
          contributor.setFirstName(((Contributor) serializable).getFirstname());
          contributor.setLastName(((Contributor) serializable).getLastname());
          contributor.setEmail(((Contributor) serializable).getEmail());
          contributor.setCnIndividu(((Contributor) serializable).getCnIndividu());
          contributor.setInstitution(((Contributor) serializable).getInstitution());
          contributor.setOrcid(((Contributor) serializable).getOrcid());
          if (((Contributor) serializable).getRole() != null) {
            contributor.setRole(((Contributor) serializable).getRole().value());
          }
          contributors.add(contributor);
        } else {
          CollaborationDTO collaboration = new CollaborationDTO();
          collaboration.setName(((Collaboration) serializable).getName());
          contributors.add(collaboration);
        }
      }
    }
    return contributors;
  }

  @Override
  public void setContributors(Object depositDocObj, List<AbstractContributor> contributors) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContributors() == null) {
      depositDoc.setContributors(this.factory.createContributors());
    }
    depositDoc.getContributors().getContributorOrCollaboration().clear();
    for (AbstractContributor abstractContributor : contributors) {
      if (abstractContributor instanceof ContributorDTO) {
        ContributorDTO contributorDTO = (ContributorDTO) abstractContributor;
        Contributor contributor = this.factory.createContributor();
        contributor.setRole(AuthorRole.fromValue(contributorDTO.getRole()));
        contributor.setFirstname(contributorDTO.getFirstName());
        contributor.setLastname(contributorDTO.getLastName());
        contributor.setEmail(contributorDTO.getEmail());
        contributor.setCnIndividu(contributorDTO.getCnIndividu());
        contributor.setInstitution(contributorDTO.getInstitution());
        contributor.setOrcid(contributorDTO.getOrcid());
        depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
      } else if (abstractContributor instanceof CollaborationDTO) {
        CollaborationDTO collaborationDTO = (CollaborationDTO) abstractContributor;
        Collaboration collaboration = this.factory.createCollaboration();
        collaboration.setName(collaborationDTO.getName());
        depositDoc.getContributors().getContributorOrCollaboration().add(collaboration);
      }
    }
  }

  @Override
  public List<ResearchGroup> getResearchGroups(Object depositDocObj) {
    List<ResearchGroup> researchGroups = new ArrayList<>();
    Groups groups = ((DepositDoc) depositDocObj).getGroups();
    if (groups != null) {
      List<Group> groupList = groups.getGroup();
      for (Group g : groupList) {
        ResearchGroup researchGroup = new ResearchGroup();
        researchGroup.setName((g != null && g.getName() != null) ? g.getName() : null);  // to avoid null exception
        researchGroup.setCode((g != null && g.getCode() != null) ? g.getCode().toString() : null);
        researchGroups.add(researchGroup);
      }
    }
    return researchGroups;
  }

  @Override
  public List<Structure> getStructures(Object depositDocObj) {
    List<Structure> structures = new ArrayList<>();
    AcademicStructures academicStructures = ((DepositDoc) depositDocObj).getAcademicStructures();
    if (academicStructures != null) {
      List<AcademicStructure> academicStructuresList = academicStructures.getAcademicStructure();
      for (AcademicStructure as : academicStructuresList) {
        Structure structure = new Structure();
        structure.setName((as != null && as.getName() != null) ? as.getName() : ""); // to avoid null exception
        structure.setCodeStruct((as != null && as.getCode() != null) ? as.getCode() : "");
        structure.setCnStructC((as != null && as.getOldCode() != null) ? as.getOldCode() : "");
        structures.add(structure);
      }
    }
    return structures;
  }

  @Override
  public List<MetadataDates> getDates(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataDates> metadataDates = new ArrayList<>();
    if (depositDoc.getDates() != null) {
      List<DateWithType> datesWithType = depositDoc.getDates().getDate();
      for (DateWithType date : datesWithType) {
        MetadataDates metadataDate = new MetadataDates();
        metadataDate.setContent(date.getContent());
        metadataDate.setType(date.getType().value());
        metadataDates.add(metadataDate);
      }
    }
    return metadataDates;
  }

  @Override
  public MetadataDates getDateForGlobalYear(Object depositDocObj) {
    List<MetadataDates> metadataDates = this.getDates(depositDocObj);
    Optional<MetadataDates> publicationDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_PUBLICATION.equals(d.getType())).findFirst();
    if (publicationDateOpt.isPresent()) {
      return publicationDateOpt.get();
    }
    Optional<MetadataDates> firstOnlineDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_FIRST_ONLINE.equals(d.getType())).findFirst();
    if (firstOnlineDateOpt.isPresent()) {
      return firstOnlineDateOpt.get();
    }
    Optional<MetadataDates> imprimaturDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_IMPRIMATUR.equals(d.getType())).findFirst();
    if (imprimaturDateOpt.isPresent()) {
      return imprimaturDateOpt.get();
    }
    Optional<MetadataDates> defenseDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_DEFENSE.equals(d.getType())).findFirst();
    if (defenseDateOpt.isPresent()) {
      return defenseDateOpt.get();
    }
    return null;
  }

  @Override
  public String getOriginalTitleContent(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getOriginalTitle() != null) {
      return depositDoc.getOriginalTitle().getContent();
    }
    return null;
  }

  @Override
  public String getOriginalTitleLang(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getOriginalTitle() != null) {
      return depositDoc.getOriginalTitle().getLang();
    }
    return null;
  }

  @Override
  public void setOriginalTitleLang(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getOriginalTitle() == null) {
      depositDoc.setOriginalTitle(this.factory.createTextLang());
    }
    depositDoc.getOriginalTitle().setLang(value);
  }

  @Override
  public List<MetadataTextLang> getAbstracts(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataTextLang> abstracts = new ArrayList<>();
    if (depositDoc.getAbstracts() != null && depositDoc.getAbstracts().getAbstract() != null) {
      for (Text abstractText : depositDoc.getAbstracts().getAbstract()) {
        abstracts.add(new MetadataTextLang(abstractText.getContent(), abstractText.getLang()));
      }
    }
    return abstracts;
  }

  @Override
  public void setAbstracts(Object depositDocObj, List<MetadataTextLang> abstracts) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getAbstracts() == null) {
      depositDoc.setAbstracts(this.factory.createAbstracts());
    }
    depositDoc.getAbstracts().getAbstract().clear();
    for (MetadataTextLang textLang : abstracts) {
      Text text = this.factory.createText();
      text.setContent(textLang.getContent());
      text.setLang(textLang.getLang());
      depositDoc.getAbstracts().getAbstract().add(text);
    }
  }

  @Override
  public List<MetadataLink> getLinks(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataLink> metadataLinks = new ArrayList<>();
    if (depositDoc.getLinks() != null && depositDoc.getLinks().getLink() != null) {
      for (Link link : depositDoc.getLinks().getLink()) {
        MetadataLink metadataLink = new MetadataLink();
        metadataLink.setTarget(link.getTarget());
        String description = link.getDescription() != null ? link.getDescription().getContent() : null;
        String lang = link.getDescription() != null ? link.getDescription().getLang() : null;
        metadataLink.setDescription(new MetadataTextLang(description, lang));
        if (link.getType() == LinkTypes.WEB) {
          metadataLink.setType(MetadataLink.LinkType.WEB);
        } else if (link.getType() == LinkTypes.INTERNAL) {
          metadataLink.setType(MetadataLink.LinkType.INTERNAL);
        } else if (link.getType() == null) {
          metadataLink.setType(null);
        }
        metadataLinks.add(metadataLink);
      }
    }
    return metadataLinks;
  }

  @Override
  public void setLinks(Object depositDocObj, List<MetadataLink> links) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getLinks() == null) {
      depositDoc.setLinks(this.factory.createLinks());
    }
    depositDoc.getLinks().getLink().clear();
    for (MetadataLink metadataLink : links) {
      Link link = this.factory.createLink();
      link.setTarget(metadataLink.getTarget());

      if (metadataLink.getDescription() != null) {
        Text text = this.factory.createText();
        text.setContent(metadataLink.getDescription().getContent());
        text.setLang(metadataLink.getDescription().getLang());
        link.setDescription(text);
      }

      if (metadataLink.getType() == MetadataLink.LinkType.WEB) {
        link.setType(LinkTypes.WEB);
      } else if (metadataLink.getType() == MetadataLink.LinkType.INTERNAL) {
        link.setType(LinkTypes.INTERNAL);
      }
      depositDoc.getLinks().getLink().add(link);
    }
  }

  @Override
  public List<MetadataCollection> getCollections(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataCollection> collections = new ArrayList<>();
    if (depositDoc.getCollections() != null && depositDoc.getCollections().getCollection() != null) {
      for (Collection collection : depositDoc.getCollections().getCollection()) {
        collections.add(new MetadataCollection(collection.getName(), collection.getNumber().toString()));
      }
    }
    return collections;
  }

  @Override
  public List<MetadataFunding> getFundings(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataFunding> metadataFundings = new ArrayList<>();
    if (depositDoc.getFundings() != null && depositDoc.getFundings().getFunding() != null) {
      for (Funding funding : depositDoc.getFundings().getFunding()) {
        MetadataFunding metadataFunding = new MetadataFunding();
        metadataFunding.setFunder(funding.getFunder());
        metadataFunding.setName(funding.getName());
        metadataFunding.setCode(String.valueOf(funding.getCode()));
        metadataFunding.setAcronym(funding.getAcronym());
        metadataFundings.add(metadataFunding);
      }
    }
    return metadataFundings;
  }

  @Override
  public List<String> getDatasets(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<String> datasets = new ArrayList<>();
    if (depositDoc.getDataset() != null && depositDoc.getDataset().getUrlOrDoi() != null) {
      for (JAXBElement<String> jaxbElement : depositDoc.getDataset().getUrlOrDoi()) {
        datasets.add(jaxbElement.getValue() != null ? jaxbElement.getValue() : null);
      }
    }
    return datasets;
  }

  @Override
  public List<MetadataFile> getFiles(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataFile> files = new ArrayList<>();
    if (depositDoc.getFiles() != null && depositDoc.getFiles().getFile() != null) {
      for (File file : depositDoc.getFiles().getFile()) {
        MetadataFile metadataFile = new MetadataFile();
        metadataFile.setType(file.getType().value());
        metadataFile.setName(file.getName());
        metadataFile.setSize(file.getSize());
        metadataFile.setMimeType(file.getMimetype());
        metadataFile.setAccessLevel(file.getAccessLevel().value());
        metadataFile.setLicense(file.getLicense());
        files.add(metadataFile);
      }
    }
    return files;
  }

  @Override
  public List<MetadataCorrection> getCorrections(Object depositDocObj) {
    // V1 doesn't have any correction
    return new ArrayList<>();
  }

  @Override
  public String getPaging(Object depositDocObj) {
    return null;
  }

  @Override
  public String getPagingOther(Object depositDocObj) {
    return null;
  }

  @Override
  public String getPublisherVersionUrl(Object depositDocObjet) {
    return null;
  }

  @Override
  public String getNote(Object depositDocObjet) {
    return null;
  }

  @Override
  public List<String> getKeywords(Object depositDocObjet) {
    return null;
  }

  @Override
  public List<MetadataClassification> getClassifications(Object depositDocObj) {
    return null;
  }

  @Override
  public String getDiscipline(Object depositDocObj) {
    return null;
  }

  @Override
  public String getMandator(Object depositDocObj) {
    return null;
  }

  @Override
  public String getPublisherName(Object depositDocObj) {
    return null;
  }

  @Override
  public String getPublisherPlace(Object depositDocObj) {
    return null;
  }

  @Override
  public String getEdition(Object depositDocObj) {
    return null;
  }

  @Override
  public String getAward(Object depositDocObj) {
    return null;
  }

  @Override
  public boolean isBeforeUnige(Object depositDocObj) {
    return false;
  }

  @Override
  public String getContainerTitle(Object depositDocObj) {
    return null;
  }

  @Override
  public String getContainerVolume(Object depositDocObj) {
    return null;
  }

  @Override
  public String getContainerIssue(Object depositDocObj) {
    return null;
  }

  @Override
  public String getContainerSpecialIssue(Object depositDocObj) {
    return null;
  }

  @Override
  public String getContainerEditor(Object depositDocObj) {
    return null;
  }

  @Override
  public String getContainerConferencePlace(Object depositDocObj) {
    return null;
  }

  @Override
  public String getContainerConferenceDate(Object depositDocObj) {
    return null;
  }

  @Override
  public String getContainerConferenceSubtitle(Object depositDocObj) {
    return null;
  }

  @Override
  public String getAouCollection(Object depositDocObj) {
    return null;
  }

  @Override
  public String getDoctorAddress(Object depositDocObj) {
    return null;
  }

  @Override
  public String getDoctorEmail(Object depositDocObj) {
    return null;
  }

  @Override
  public void fillAdditionalMetadata(Object depositDocObj, Map<String, Object> additionalMetadata) {
    // Do nothing in V1
  }

  @Override
  public String serializeDepositDocToXml(Object depositDocObject) {
    try {
      DepositDoc depositDoc = (DepositDoc) depositDocObject;
      JAXBContext jaxbContext = JAXBContext.newInstance(DepositDoc.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(depositDoc, sw);
      return sw.toString();
    } catch (Exception e) {
      throw new SolidifyRuntimeException("unable to serialize DepositDoc to XML", e);
    }
  }
}
