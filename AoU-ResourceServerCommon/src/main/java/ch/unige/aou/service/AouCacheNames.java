/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AouCacheNames.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import ch.unige.solidify.rest.CacheNames;

public class AouCacheNames extends CacheNames {
  public static final String USER_BY_EXTERNAL_UID = "userByExternalUid";
  public static final String NEW_USER = "newUser";
  public static final String UNIGE_PERSON_CN_INDIVIDU = "unigePersonCnIndividu";
  public static final String FACET_REQUEST = "facetRequest";
  public static final String INDEX_FIELD_ALIAS = "indexFieldAlias";
  public static final String PARENT_STRUCTURES_IDS = "parentStructuresIds";
  public static final String PARENT_STRUCTURES_NAMES = "parentStructuresNames";
  public static final String STRUCTURE_BY_CN_STRUCT_C = "structureCnStructC";
  public static final String PUBLICATION_SUBTYPES = "publicationSubtypes";
  public static final String PUBLICATION_SUB_SUBTYPES = "publicationSubSubtypes";
  public static final String DOCUMENT_FILE_TYPES = "documentFileTypes";
  public static final String CONTRIBUTOR_CN_INDIVIDU = "contributorCnIndividu";
  public static final String CONTRIBUTOR_ORCID = "contributorOrcid";
  public static final String OPEN_LICENSE_ID = "openLicenseId";
  public static final String PERSON_STRUCTURES = "personStructures";
  public static final String PERSON_AVATAR = "personAvatar";
  public static final String PERSON_RESEARCH_GROUPS = "personResearchGroups";
}
