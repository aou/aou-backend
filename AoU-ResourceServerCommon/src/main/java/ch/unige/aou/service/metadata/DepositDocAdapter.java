/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - DepositDocAdapter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.MetadataClassification;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataCorrection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.MetadataFunding;
import ch.unige.aou.model.publication.MetadataLink;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

public interface DepositDocAdapter {

  String getType(Object depositDocObj);

  void setType(Object depositDocObj, String value);

  String getSubtype(Object depositDocObj);

  void setSubtype(Object depositDocObj, String value);

  String getSubSubtype(Object depositDocObj);

  void setSubSubtype(Object depositDocObj, String value);

  String getTitleContent(Object depositDocObj);

  String getTitleLang(Object depositDocObj);

  void setTitleLang(Object depositDocObj, String value);

  String getOriginalTitleContent(Object depositDocObj);

  String getOriginalTitleLang(Object depositDocObj);

  void setOriginalTitleLang(Object depositDocObj, String value);

  String getISBN(Object depositDocObj);

  String getISSN(Object depositDocObj);

  void setISSN(Object depositDocObj, String value);

  String getDOI(Object depositDocObj);

  void setDOI(Object depositDocObj, String value);

  BigInteger getPmid(Object depositDocObj);

  String getArxivId(Object depositDocObj);

  String getURN(Object depositDocObj);

  void setURN(Object depositDocObj, String value);

  String getPmcId(Object depositDocObj);

  String getMmsid(Object depositDocObj);

  String getRepec(Object depositDocObj);

  String getDblp(Object depositDocObj);

  String getLocalNumber(Object depositDocObj);

  List<AbstractContributor> getContributors(Object depositDocObj);

  void setContributors(Object depositDocObj, List<AbstractContributor> abstractContributors);

  List<ResearchGroup> getResearchGroups(Object depositDocObj);

  List<Structure> getStructures(Object depositDocObj);

  List<MetadataDates> getDates(Object depositDocObj);

  MetadataDates getDateForGlobalYear(Object depositDocObj);

  List<MetadataTextLang> getAbstracts(Object depositDocObj);

  void setAbstracts(Object depositDocObj, List<MetadataTextLang> abstracts);

  List<MetadataLink> getLinks(Object depositDocObj);

  void setLinks(Object depositDocObj, List<MetadataLink> links);

  List<MetadataCollection> getCollections(Object depositDocObj);

  List<String> getLanguages(Object depositDocObj);

  void setLanguages(Object depositDocObj, List<String> languagesList);

  List<MetadataFunding> getFundings(Object depositDocObj);

  List<String> getDatasets(Object depositDocObj);

  List<MetadataFile> getFiles(Object depositDocObj);

  List<MetadataCorrection> getCorrections(Object depositDocObj);

  String getPaging(Object depositDocObj);

  String getPagingOther(Object depositDocObj);

  String getPublisherVersionUrl(Object depositDocObjet);

  String getNote(Object depositDocObjet);

  List<String> getKeywords(Object depositDocObjet);

  List<MetadataClassification> getClassifications(Object depositDocObj);

  String getDiscipline(Object depositDocObj);

  String getMandator(Object depositDocObj);

  String getPublisherName(Object depositDocObj);

  String getPublisherPlace(Object depositDocObj);

  String getEdition(Object depositDocObj);

  String getAward(Object depositDocObj);

  boolean isBeforeUnige(Object depositDocObj);

  String getContainerTitle(Object depositDocObj);

  String getContainerVolume(Object depositDocObj);

  String getContainerIssue(Object depositDocObj);

  String getContainerSpecialIssue(Object depositDocObj);

  String getContainerEditor(Object depositDocObj);

  String getContainerConferencePlace(Object depositDocObj);

  String getContainerConferenceDate(Object depositDocObj);

  String getContainerConferenceSubtitle(Object depositDocObj);

  String getAouCollection(Object depositDocObj);

  String getDoctorAddress(Object depositDocObj);

  String getDoctorEmail(Object depositDocObj);

  void fillAdditionalMetadata(Object depositDocObj, Map<String, Object> additionalMetadata);

  String serializeDepositDocToXml(Object depositDocObj);
}
