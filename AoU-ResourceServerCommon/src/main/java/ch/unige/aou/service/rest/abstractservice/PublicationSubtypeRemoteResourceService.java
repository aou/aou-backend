/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - PublicationSubtypeRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.PublicationSubSubtypeDTO;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeDTO;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileTypeDTO;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public abstract class PublicationSubtypeRemoteResourceService extends AdminRemoteResourceService<PublicationSubtype> {

  protected PublicationSubtypeRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  /**
   * Getting a list of PublicationSubtypeDTO instead of PublicationSubtype class allows to correctly retrieve the language code
   *
   * @param pageable
   * @return
   */
  @Cacheable(AouCacheNames.PUBLICATION_SUBTYPES)
  public RestCollection<PublicationSubtypeDTO> findAllDTO(Pageable pageable) {
    return this.restClientService.getResourceList(this.getResourceUrl(), PublicationSubtypeDTO.class, pageable);
  }

  @Cacheable(AouCacheNames.PUBLICATION_SUBTYPES)
  public RestCollection<PublicationSubtypeDocumentFileTypeDTO> findAllPublicationSubtypeDocumentFileTypesDTO(String publicationSubtypeId,
          Pageable pageable) {
    String url = this.getResourceUrl() + "/" + publicationSubtypeId + "/" + AouActionName.LIST_DOCUMENT_FILE_TYPES;
    return this.restClientService.getResourceList(url, PublicationSubtypeDocumentFileTypeDTO.class, pageable);
  }

  @Cacheable(AouCacheNames.PUBLICATION_SUB_SUBTYPES)
  public RestCollection<PublicationSubSubtypeDTO> findAllPublicationSubSubtypesDTO(String publicationSubtypeId, Pageable pageable) {
    String url = this.getResourceUrl() + "/" + publicationSubtypeId + "/" + ResourceName.PUBLICATION_SUB_SUBTYPES;
    return this.restClientService.getResourceList(url, PublicationSubSubtypeDTO.class, pageable);
  }

  @Override
  protected Class<PublicationSubtype> getResourceClass() {
    return PublicationSubtype.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.PUBLICATION_SUBTYPES;
  }
}
