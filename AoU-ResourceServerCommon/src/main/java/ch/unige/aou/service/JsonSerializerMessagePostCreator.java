/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - JsonSerializerMessagePostCreator.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.springframework.jms.core.MessagePostProcessor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.jms.JMSException;
import jakarta.jms.Message;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class JsonSerializerMessagePostCreator implements MessagePostProcessor {
  private final ObjectMapper objectMapper;

  public JsonSerializerMessagePostCreator() {
    this.objectMapper = new ObjectMapper();
  }

  @Override
  public Message postProcessMessage(Message message) throws JMSException {
    try {
      String json = this.objectMapper.writeValueAsString(((ActiveMQObjectMessage) message).getObject());
      message.setStringProperty("jsonObject", json);
    } catch (JsonProcessingException e) {
      String objectClass = ((ActiveMQObjectMessage) message).getObject().getClass().getSimpleName();
      throw new SolidifyRuntimeException("Unable to serialize JMS message object '" + objectClass + "' in JSON", e);
    }
    return message;
  }
}