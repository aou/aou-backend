/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - MessageProcessor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;

import ch.unige.solidify.SolidifyConstants;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;

public abstract class MessageProcessor<T> {

  final ExecutorService executorService;

  public MessageProcessor(final AouProperties aouProperties) {
    final int nbThreads = aouProperties.getQueue().getNbParallelThreads();
    this.executorService = Executors.newFixedThreadPool(nbThreads);
  }

  public abstract void processMessage(T message);

  public abstract void receiveMessage(T Message);

  void sendForParallelProcessing(T message) {
    this.executorService.submit(() -> this.processMessage(message));
  }

  void setSecurityContext() {
    UserDetails messageProcessorUserDetails = new MessageProcessorUserDetails();
    SecurityContext securityContext = new SecurityContextImpl();
    securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(messageProcessorUserDetails, null,
            messageProcessorUserDetails.getAuthorities()));
    SecurityContextHolder.setContext(securityContext);
  }

  private class MessageProcessorUserDetails implements UserDetails {

    private static final long serialVersionUID = 1289831691500128412L;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
      return Arrays.asList(new SimpleGrantedAuthority(SolidifyConstants.MESSAGE_PROCESSOR_ROLE));
    }

    @Override
    public String getPassword() {
      return null;
    }

    @Override
    public String getUsername() {
      return AouConstants.MESSAGE_PROCESSOR_NAME;
    }

    @Override
    public boolean isAccountNonExpired() {
      return false;
    }

    @Override
    public boolean isAccountNonLocked() {
      return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
      return true;
    }

    @Override
    public boolean isEnabled() {
      return true;
    }
  }
}
