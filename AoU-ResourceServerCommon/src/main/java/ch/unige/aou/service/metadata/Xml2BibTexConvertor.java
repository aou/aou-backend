/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - Xml2BibTexConvertor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_THESE_NAME;
import static ch.unige.aou.AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME;
import static ch.unige.aou.model.xml.deposit.v2_4.AuthorRole.AUTHOR;
import static ch.unige.aou.model.xml.deposit.v2_4.AuthorRole.EDITOR;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import jakarta.xml.bind.JAXBElement;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.access.BibTexEntry;
import ch.unige.aou.model.access.BibTexEntryDefinition;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.list_deposits.Publication;
import ch.unige.aou.model.xml.list_deposits.Publications;

public class Xml2BibTexConvertor {

  public static String convertToBibTex(Publications publications) {
    StringBuilder strBuilder = new StringBuilder();
    // @formatter:off
    for (Publication publication : publications.getPublication()) {
      switch (publication.getDepositDoc().getSubtype()) {
        case DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME,
             DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME,
             DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME -> appendArticleEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME -> appendProceedingsEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME -> appendInProceedingsEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME,
             DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME,
             DEPOSIT_SUBTYPE_POSTER_NAME -> appendMiscEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_LIVRE_NAME,
             DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME -> appendBookEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME,
             DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME -> appendInBookEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_THESE_NAME,
             DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME,
             DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME -> appendPhdThesisEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME,
             DEPOSIT_SUBTYPE_MASTER_NAME -> appendMasterThesisEntry(strBuilder, publication);
        case DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME,
             DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME,
             DEPOSIT_SUBTYPE_WORKING_PAPER_NAME -> appendTechReport(strBuilder, publication);
        case DEPOSIT_SUBTYPE_PREPRINT_NAME -> appendUnpublished(strBuilder, publication);
      }
      // @formatter:on
    }
    return strBuilder.toString();
  }

  private static void appendUnpublished(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.UNPUBLISHED);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "institution", "year", "doi")), publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));

  }

  private static void appendTechReport(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.TECHREPORT);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "institution", "year", "number", "note")), publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void appendMasterThesisEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.MASTERTHESIS);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "school", "year", "note")), publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void appendPhdThesisEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.PHDTHESIS);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "school", "year", "note")), publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void appendInProceedingsEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.INPROCEEDINGS);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "booktitle", "year", "editor", "volume", "number",
            "pages", "publisher", "doi")), publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));

  }

  private static void appendInBookEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.INBOOK);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "booktitle", "publisher", "address", "year", "editor", "volume", "number",
            "edition", "pages", "note", "doi")), publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void appendBookEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.BOOK);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "editor", "title", "publisher", "address", "year", "volume", "number",
            "edition", "isbn", "issn", "doi", "note")), publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void appendProceedingsEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.PROCEEDINGS);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("title", "year", "editor", "volume", "number", "publisher", "address", "doi")), publication);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void appendArticleEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.ARTICLE);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "journal", "year", "volume", "number", "pages", "issn", "note", "doi")),
            publication);
    bibtexEntry.setFields(fields);

    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void appendMiscEntry(StringBuilder strBuilder, Publication publication) {
    BibTexEntry bibtexEntry = new BibTexEntry(BibTexEntryDefinition.MISC);
    Map<String, String> fields = new LinkedHashMap<>();
    addProperties(fields, new LinkedList<>(List.of("author", "title", "year", "note", "howpublished", "doi")), publication);
    bibtexEntry.setFields(fields);
    strBuilder.append(bibtexEntry.toBibTeX(publication.getArchiveId()));
  }

  private static void addProperties(Map<String, String> fields, LinkedList<String> propertiesToAdd, Publication publication) {
    for (String propertyToAdd : propertiesToAdd) {
      switch (propertyToAdd) {
        case "author" -> addAuthor(fields, publication);
        case "booktitle" -> addBookTitle(fields, publication);
        case "doi" -> addDoi(fields, publication);
        case "edition" -> addEdition(fields, publication);
        case "editor" -> addEditor(fields, publication);
        case "title" -> fields.put("title", publication.getDepositDoc().getTitle().getContent());
        case "institution" -> addInstitution(fields, publication);
        case "issn" -> addIssn(fields, publication);
        case "isbn" -> addIsbn(fields, publication);
        case "journal" -> addJournal(fields, publication);
        case "note" -> addNote(fields, publication);
        case "number" -> addNumber(fields, publication);
        case "pages" -> addPages(fields, publication);
        case "publisher" -> addPublisher(fields, publication);
        case "school" -> addSchool(fields, publication);
        case "year" -> addDate(fields, publication);
        case "volume" -> addVolume(fields, publication);
        case "howpublished" -> addHowpublished(fields, publication);
      }
    }
  }

  private static void addHowpublished(Map<String, String> fields, Publication publication) {
    fields.put("howpublished", "\\url{https://archive-ouverte.unige.ch/unige:" + publication.getArchiveId() + "}");
  }

  private static void addDate(Map<String, String> fields, Publication publication) {
    Optional<DateWithType> publicationDateOpt = publication.getDepositDoc().getDates().getDate().stream()
            .filter(d -> d.getType().equals(DateTypes.PUBLICATION)).findFirst();
    Optional<DateWithType> firstOnlineDateOpt = publication.getDepositDoc().getDates().getDate().stream()
            .filter(d -> d.getType().equals(DateTypes.FIRST_ONLINE)).findFirst();
    Optional<DateWithType> imprimaturDateOpt = publication.getDepositDoc().getDates().getDate().stream()
            .filter(d -> d.getType().equals(DateTypes.IMPRIMATUR)).findFirst();
    Optional<DateWithType> defenseDateOpt = publication.getDepositDoc().getDates().getDate().stream()
            .filter(d -> d.getType().equals(DateTypes.DEFENSE)).findFirst();
    if (publicationDateOpt.isPresent() || firstOnlineDateOpt.isPresent()) {
      fields.put("year", publicationDateOpt.isPresent() ? publicationDateOpt.get().getContent() : firstOnlineDateOpt.get().getContent());
    } else if (imprimaturDateOpt.isPresent() || defenseDateOpt.isPresent()) {
      fields.put("year", imprimaturDateOpt.isPresent() ? imprimaturDateOpt.get().getContent() : defenseDateOpt.get().getContent());
    }
  }

  private static void addEdition(Map<String, String> fields, Publication publication) {
    if (!StringTool.isNullOrEmpty(publication.getDepositDoc().getEdition())) {
      fields.put("edition", publication.getDepositDoc().getEdition());
    }
  }

  private static void addSchool(Map<String, String> fields, Publication publication) {
    if (!publication.getDepositDoc().isIsBeforeUnige()) {
      fields.put("school", "Université de Genève");
    }
  }

  private static void addInstitution(Map<String, String> fields, Publication publication) {
    if (!StringTool.isNullOrEmpty(publication.getDepositDoc().getMandator())) {
      fields.put("institution", publication.getDepositDoc().getMandator());
    }
  }

  private static void addAuthor(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getContributors() != null) {
      List<Contributor> contributors = publication.getDepositDoc().getContributors().getContributorOrCollaboration().stream()
              .filter(c -> c instanceof Contributor).filter(c -> ((Contributor) c).getRole().equals(AUTHOR)).map(c -> (Contributor) c).toList();
      String authors = "";
      for (Contributor contributor : contributors) {
        if (!authors.isEmpty()) {
          authors += " and ";
        }
        authors += contributor.getLastname() + "," + contributor.getFirstname();
      }
      fields.put("author", authors);
    }
  }

  private static void addEditor(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getContainer() != null && !StringTool.isNullOrEmpty(
            publication.getDepositDoc().getContainer().getEditor())) {
      fields.put("editor", publication.getDepositDoc().getContainer().getEditor());
    } else if (publication.getDepositDoc().getContributors() != null && publication.getDepositDoc().getContributors()
            .getContributorOrCollaboration().stream().filter(c -> c instanceof Contributor)
            .anyMatch(c -> ((Contributor) c).getRole().equals(EDITOR))) {
      List<Contributor> editorsList = publication.getDepositDoc().getContributors().getContributorOrCollaboration().stream()
              .filter(c -> c instanceof Contributor).map(c -> (Contributor) c).filter(c -> c.getRole().equals(EDITOR)).toList();
      String editors = "";
      for (Contributor contributor : editorsList) {
        if (!editors.isEmpty()) {
          editors += " and ";
        }
        editors += contributor.getLastname() + "," + contributor.getFirstname();
      }
      fields.put("editor", editors);
    }
  }

  private static void addNote(Map<String, String> fields, Publication publication) {
    if (!StringTool.isNullOrEmpty(publication.getDepositDoc().getNote())) {
      fields.put("note", publication.getDepositDoc().getNote());
    }
  }

  private static void addIsbn(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getIdentifiers() != null && !StringTool.isNullOrEmpty(
            publication.getDepositDoc().getIdentifiers().getIsbn())) {
      fields.put("isbn", publication.getDepositDoc().getIdentifiers().getIsbn());
    }
  }

  private static void addIssn(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getIdentifiers() != null && !StringTool.isNullOrEmpty(
            publication.getDepositDoc().getIdentifiers().getIssn())) {
      fields.put("issn", publication.getDepositDoc().getIdentifiers().getIssn());
    }
  }

  private static void addDoi(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getIdentifiers() != null && !StringTool.isNullOrEmpty(
            publication.getDepositDoc().getIdentifiers().getDoi())) {
      fields.put("doi", "https://doi.org/" + publication.getDepositDoc().getIdentifiers().getDoi());
    }
  }

  private static void addPages(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getPages() != null) {
      Optional<JAXBElement<String>> pagingOpt = publication.getDepositDoc().getPages().getPagingOrOther().stream()
              .filter(p -> p.getName().getLocalPart().equals("paging"))
              .findFirst();
      if (pagingOpt.isPresent()) {
        fields.put("pages", pagingOpt.get().getValue());
      } else {
        Optional<JAXBElement<String>> otherOpt = publication.getDepositDoc().getPages().getPagingOrOther().stream()
                .filter(p -> p.getName().getLocalPart().equals("other"))
                .findFirst();
        if (otherOpt.isPresent()) {
          fields.put("pages", otherOpt.get().getValue());
        }
      }
    }
  }

  private static void addVolume(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getContainer() != null && !StringTool.isNullOrEmpty(
            publication.getDepositDoc().getContainer().getVolume())) {
      fields.put("volume", publication.getDepositDoc().getContainer().getVolume());
    }
  }

  private static void addPublisher(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getPublisher() != null) {
      Optional<JAXBElement<String>> nameOpt = publication.getDepositDoc().getPublisher().getNameOrPlace().stream()
              .filter(p -> p.getName().getLocalPart().equals("name"))
              .findFirst();
      if (nameOpt.isPresent()) {
        fields.put("publisher", nameOpt.get().getValue());

        Optional<JAXBElement<String>> placeOpt = publication.getDepositDoc().getPublisher().getNameOrPlace().stream()
                .filter(p -> p.getName().getLocalPart().equals("place"))
                .findFirst();
        if (placeOpt.isPresent()) {
          fields.put("address", placeOpt.get().getValue());
        }
      }
    }
  }

  private static void addNumber(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getContainer() != null && !StringTool.isNullOrEmpty(publication.getDepositDoc().getContainer().getIssue())) {
      fields.put("number", publication.getDepositDoc().getContainer().getIssue());
    } else if (publication.getDepositDoc().getIdentifiers() != null && !StringTool.isNullOrEmpty(
            publication.getDepositDoc().getIdentifiers().getLocalNumber())) {
      fields.put("number", publication.getDepositDoc().getIdentifiers().getLocalNumber());
    }
  }

  private static void addJournal(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getContainer() != null && !StringTool.isNullOrEmpty(publication.getDepositDoc().getContainer().getTitle())) {
      fields.put("journal", publication.getDepositDoc().getContainer().getTitle());
    }
  }

  private static void addBookTitle(Map<String, String> fields, Publication publication) {
    if (publication.getDepositDoc().getContainer() != null && !StringTool.isNullOrEmpty(publication.getDepositDoc().getContainer().getTitle())) {
      fields.put("booktitle", publication.getDepositDoc().getContainer().getTitle());
    }
  }

}
