/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - InMemoryPublicationIndexingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.index.inmemory;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.IndexProperties;
import ch.unige.solidify.index.indexing.inmemory.InMemoryService;
import ch.unige.solidify.index.indexing.inmemory.IndexData;
import ch.unige.solidify.index.settings.inmemory.InMemorySettingsService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.index.PublicationIndexEntry;

@Profile("index-inmemory")
@Service
public class InMemoryPublicationIndexingService extends InMemoryService<PublicationIndexEntry> {
  public InMemoryPublicationIndexingService(AouProperties aouProperties, IndexProperties indexProperties,
          InMemorySettingsService settingsService, IndexData indexData) {
    super(indexProperties, settingsService, aouProperties.getIndexing().getIndexDefinitionList(), indexData);
  }

  @Override
  public String getIndexName(String indexName) {
    return indexName;
  }

  @Override
  protected PublicationIndexEntry getMetadataObject() {
    return new PublicationIndexEntry();
  }
}
