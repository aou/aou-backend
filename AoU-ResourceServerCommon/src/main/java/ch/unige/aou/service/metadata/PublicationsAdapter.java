/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - ListDepositDocAdapter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.List;

import org.glassfish.jaxb.runtime.marshaller.NamespacePrefixMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.model.xml.deposit.v2_4.Abstracts;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructures;
import ch.unige.aou.model.xml.deposit.v2_4.AccessLevel;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Classifications;
import ch.unige.aou.model.xml.deposit.v2_4.Collections;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.Contributors;
import ch.unige.aou.model.xml.deposit.v2_4.Corrections;
import ch.unige.aou.model.xml.deposit.v2_4.Datasets;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.Dates;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v2_4.FileType;
import ch.unige.aou.model.xml.deposit.v2_4.Files;
import ch.unige.aou.model.xml.deposit.v2_4.Fundings;
import ch.unige.aou.model.xml.deposit.v2_4.Groups;
import ch.unige.aou.model.xml.deposit.v2_4.Keywords;
import ch.unige.aou.model.xml.deposit.v2_4.Languages;
import ch.unige.aou.model.xml.deposit.v2_4.Links;
import ch.unige.aou.model.xml.deposit.v2_4.Pages;
import ch.unige.aou.model.xml.deposit.v2_4.Publisher;
import ch.unige.aou.model.xml.deposit.v2_4.Text;
import ch.unige.aou.model.xml.deposit.v2_4.TextLang;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.AbstractsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.AuthorRoleSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.ClassificationsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.CollectionsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.ContributorsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.CorrectionsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DatasetsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DateWithTypeSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DatesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.DepositIdentifiersSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.FilesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.FundingsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.KeywordsSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.LanguagesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.LinksSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.PagesSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.PublisherSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.TextLangSerializer;
import ch.unige.aou.model.xml.deposit.v2_4.serializer.TextSerializer;
import ch.unige.aou.model.xml.list_deposits.Publication;
import ch.unige.aou.model.xml.list_deposits.Publications;
import ch.unige.aou.model.xml.publications.DepositDocExportSerializer;
import ch.unige.aou.model.xml.publications.PublicationsSerializer;
import ch.unige.aou.model.xml.publications.ResearchGroupSerializer;
import ch.unige.aou.model.xml.publications.StructureSerializer;

@Service
public class PublicationsAdapter {
  private static final Logger log = LoggerFactory.getLogger(PublicationsAdapter.class);

  public void addPublication(Publications publications, Object depositDoc, String archiveId, OffsetDateTime lastUpdate) {
    Publication publication = new Publication();
    publication.setArchiveId(archiveId);
    publication.setLastUpdate(lastUpdate);
    publication.setDepositDoc((DepositDoc) depositDoc);
    publications.getPublication().add(publication);
  }

  public String serializePublicationsToXml(Object publicationsObj) {
    NamespacePrefixMapper mapper = new NamespacePrefixMapper() {
      @Override
      public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        if (AouConstants.METADATA_XML_NAMESPACE_V2_4.equals(namespaceUri) && !requirePrefix) {
          return "deposit_doc";
        }
        return "publications";
      }
    };
    try {
      Publications publications = (Publications) publicationsObj;
      JAXBContext jaxbContext = JAXBContext.newInstance(Publications.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      jaxbMarshaller.setProperty(AouConstants.XML_NAMESPACE_PREFIX_MAPPER, mapper);
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(publications, sw);
      return sw.toString();
    } catch (Exception e) {
      throw new SolidifyRuntimeException("unable to serialize Publications to XML", e);
    }
  }

  public String serializePublicationsToJson(Object publicationsObj) {
    try {
      ObjectMapper mapper = this.getJsonFormSerializerMapper();
      return mapper.writeValueAsString(publicationsObj);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to serialize Publications to JSON", e);
    }
  }

  public Publications createPublicationsFromXml(String xmlString) {
    if (xmlString == null) {
      throw new SolidifyValidationException(new ValidationError("XmlData is null. Unable to deserialize XML to Publications."));
    }
    return JAXB.unmarshal(new StringReader(xmlString), Publications.class);
  }

  public void validate(String xmlString, String tempFolder) throws IOException {
    ClassPathResource listDepositXsd = new ClassPathResource(
            SolidifyConstants.SCHEMA_HOME + java.io.File.separator + AouConstants.LIST_DEPOSITS_SCHEMA);
    ClassPathResource depositMetadataXsd = new ClassPathResource(
            SolidifyConstants.SCHEMA_HOME + java.io.File.separator + AouMetadataVersion.getDefaultVersion().getDepositMetadataSchema());
    List<URL> xsdUrls = List.of(listDepositXsd.getURL(), depositMetadataXsd.getURL());
    String tmpXmlFile = "xml-to-validate.xml";
    final Path tempFilePath = Paths.get(tempFolder).resolve(tmpXmlFile);
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(tempFilePath.toString()))) {
      log.info("Write xml {} to file {}", xmlString, tempFilePath);
      writer.write(xmlString);
    } catch (IOException e) {
      FileTool.deleteFile(tempFilePath);
      throw new SolidifyRuntimeException("Error writing Publications xml to file system", e);
    }
    log.info("Validation de xml {} with xsds: {} {} ", xmlString, xsdUrls.get(0), xsdUrls.get(1));
    XMLTool.validate(xsdUrls, tempFilePath);
    log.info("Delete temporary file {} with xml", tempFilePath);
    FileTool.deleteFile(tempFilePath);
  }

  private ObjectMapper getJsonFormSerializerMapper() {
    SimpleModule module = new SimpleModule();
    module.addSerializer(Publications.class, new PublicationsSerializer());
    module.addSerializer(Abstracts.class, new AbstractsSerializer());
    module.addSerializer(AcademicStructures.class, new StructureSerializer());
    module.addSerializer(AuthorRole.class, new AuthorRoleSerializer());
    module.addSerializer(Classifications.class, new ClassificationsSerializer());
    module.addSerializer(Collections.class, new CollectionsSerializer());
    module.addSerializer(Contributors.class, new ContributorsSerializer());
    module.addSerializer(Corrections.class, new CorrectionsSerializer());
    module.addSerializer(Datasets.class, new DatasetsSerializer());
    module.addSerializer(Dates.class, new DatesSerializer());
    module.addSerializer(DateWithType.class, new DateWithTypeSerializer());
    module.addSerializer(DepositDoc.class, new DepositDocExportSerializer());
    module.addSerializer(DepositIdentifiers.class, new DepositIdentifiersSerializer());
    module.addSerializer(Files.class, new FilesSerializer());
    module.addSerializer(Fundings.class, new FundingsSerializer());
    module.addSerializer(Groups.class, new ResearchGroupSerializer());
    module.addSerializer(Keywords.class, new KeywordsSerializer());
    module.addSerializer(Languages.class, new LanguagesSerializer());
    module.addSerializer(Links.class, new LinksSerializer());
    module.addSerializer(Pages.class, new PagesSerializer());
    module.addSerializer(Publisher.class, new PublisherSerializer());
    module.addSerializer(Text.class, new TextSerializer());
    module.addSerializer(TextLang.class, new TextLangSerializer());

    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(module);
    mapper.registerModule(new JavaTimeModule());
    //To convert datetime to ISO-8601
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    return mapper;
  }

  public void removeSensitiveInformationFromDepositDoc(DepositDoc depositDoc) {
    depositDoc.setDoctorAddress(null);
    depositDoc.setDoctorEmail(null);
    if (depositDoc.getFiles() != null) {
      depositDoc.getFiles().getFile()
              .removeIf(file -> file.getType().equals(FileType.AGREEMENT) && file.getAccessLevel().equals(AccessLevel.PRIVATE));
    }
    if (depositDoc.getContributors() != null) {
      depositDoc.getContributors().getContributorOrCollaboration().stream().filter(c -> c instanceof Contributor)
              .forEach(c -> {
                ((Contributor) c).setCnIndividu(null);
                ((Contributor) c).setEmail(null);
                ((Contributor) c).setInstitution(null);
                ((Contributor) c).setStructure(null);
              });
    }
  }

}
