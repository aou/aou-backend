/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - DepositDocV2Adapter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import static ch.unige.aou.AouConstants.DATE_TYPE_DEFENSE;
import static ch.unige.aou.AouConstants.DATE_TYPE_FIRST_ONLINE;
import static ch.unige.aou.AouConstants.DATE_TYPE_IMPRIMATUR;
import static ch.unige.aou.AouConstants.DATE_TYPE_PUBLICATION;

import java.io.Serializable;
import java.io.StringWriter;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.MetadataClassification;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataCorrection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataEmbargo;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.MetadataFunding;
import ch.unige.aou.model.publication.MetadataLink;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.publication.OtherNameDTO;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructure;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructures;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Classification;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Collection;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.Correction;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v2_4.File;
import ch.unige.aou.model.xml.deposit.v2_4.Funding;
import ch.unige.aou.model.xml.deposit.v2_4.Group;
import ch.unige.aou.model.xml.deposit.v2_4.Groups;
import ch.unige.aou.model.xml.deposit.v2_4.Languages;
import ch.unige.aou.model.xml.deposit.v2_4.Link;
import ch.unige.aou.model.xml.deposit.v2_4.LinkTypes;
import ch.unige.aou.model.xml.deposit.v2_4.ObjectFactory;
import ch.unige.aou.model.xml.deposit.v2_4.OtherName;
import ch.unige.aou.model.xml.deposit.v2_4.Pages;
import ch.unige.aou.model.xml.deposit.v2_4.Text;

public class DepositDocV2Adapter implements DepositDocAdapter {

  private static final Logger log = LoggerFactory.getLogger(DepositDocV2Adapter.class);

  ObjectFactory factory = new ObjectFactory();

  @Override
  public String getType(Object depositDocObj) {
    return ((DepositDoc) depositDocObj).getType();
  }

  @Override
  public void setType(Object depositDocObj, String value) {
    ((DepositDoc) depositDocObj).setType(value);
  }

  @Override
  public String getSubtype(Object depositDocObj) {
    return ((DepositDoc) depositDocObj).getSubtype();
  }

  @Override
  public void setSubtype(Object depositDocObj, String value) {
    ((DepositDoc) depositDocObj).setSubtype(value);
  }

  @Override
  public String getSubSubtype(Object depositDocObj) {
    return ((DepositDoc) depositDocObj).getSubsubtype();
  }

  @Override
  public void setSubSubtype(Object depositDocObj, String value) {
    ((DepositDoc) depositDocObj).setSubsubtype(value);
  }

  @Override
  public String getTitleContent(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getTitle() != null) {
      return depositDoc.getTitle().getContent();
    } else {
      return null;
    }
  }

  @Override
  public List<String> getLanguages(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getLanguages() != null) {
      return depositDoc.getLanguages().getLanguage();
    } else {
      return new ArrayList<>();
    }
  }

  @Override
  public void setLanguages(Object depositDocObj, List<String> languagesList) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    Languages languages = new Languages();
    languages.getLanguage().addAll(languagesList);
    depositDoc.setLanguages(languages);
  }

  @Override
  public String getTitleLang(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getTitle() != null) {
      return depositDoc.getTitle().getLang();
    } else {
      return null;
    }
  }

  @Override
  public void setTitleLang(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getTitle() == null) {
      depositDoc.setTitle(new Text());
    }
    depositDoc.getTitle().setLang(value);
  }

  @Override
  public void setISSN(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() == null) {
      depositDoc.setIdentifiers(new DepositIdentifiers());
    }
    depositDoc.getIdentifiers().setIssn(value);
  }

  @Override
  public String getISBN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getIsbn();
    } else {
      return null;
    }
  }

  @Override
  public String getArxivId(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getArxiv();
    } else {
      return null;
    }
  }

  @Override
  public String getPmcId(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getPmcid();
    } else {
      return null;
    }
  }

  @Override
  public String getURN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getUrn();
    } else {
      return null;
    }
  }

  @Override
  public void setURN(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() == null) {
      depositDoc.setIdentifiers(new DepositIdentifiers());
    }
    depositDoc.getIdentifiers().setUrn(value);
  }

  @Override
  public String getISSN(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getIssn();
    } else {
      return null;
    }
  }

  @Override
  public String getDOI(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getDoi();
    } else {
      return null;
    }
  }

  @Override
  public void setDOI(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() == null) {
      depositDoc.setIdentifiers(new DepositIdentifiers());
    }
    depositDoc.getIdentifiers().setDoi(value);
  }

  @Override
  public String getMmsid(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getMmsid();
    } else {
      return null;
    }
  }

  @Override
  public String getRepec(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getRepec();
    } else {
      return null;
    }
  }

  @Override
  public String getDblp(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getDblp();
    } else {
      return null;
    }
  }

  @Override
  public String getLocalNumber(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getLocalNumber();
    } else {
      return null;
    }
  }

  @Override
  public BigInteger getPmid(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getIdentifiers() != null) {
      return depositDoc.getIdentifiers().getPmid();
    } else {
      return null;
    }
  }

  @Override
  public List<AbstractContributor> getContributors(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<AbstractContributor> contributors = new ArrayList<>();
    if (depositDoc.getContributors() != null) {
      List<Serializable> contributorOrCollaborations = depositDoc.getContributors().getContributorOrCollaboration();
      for (Serializable serializable : contributorOrCollaborations) {
        if (serializable instanceof Contributor) {
          ContributorDTO contributor = new ContributorDTO();
          contributor.setFirstName(((Contributor) serializable).getFirstname());
          contributor.setLastName(((Contributor) serializable).getLastname());
          contributor.setEmail(((Contributor) serializable).getEmail());
          contributor.setCnIndividu(((Contributor) serializable).getCnIndividu());
          contributor.setStructure(((Contributor) serializable).getStructure());
          contributor.setInstitution(((Contributor) serializable).getInstitution());
          contributor.setOrcid(((Contributor) serializable).getOrcid());
          if (((Contributor) serializable).getRole() != null) {
            contributor.setRole(((Contributor) serializable).getRole().value());
          }
          if (((Contributor) serializable).getOtherNames() != null) {
            List<OtherNameDTO> otherNameDTOList = new ArrayList<>();
            for (OtherName otherName : ((Contributor) serializable).getOtherNames().getOtherName()) {
              otherNameDTOList.add(new OtherNameDTO(otherName.getFirstname(), otherName.getLastname()));
            }
            contributor.setOtherNames(otherNameDTOList);
          }

          contributors.add(contributor);
        } else {
          CollaborationDTO collaboration = new CollaborationDTO();
          collaboration.setName(((Collaboration) serializable).getName());
          contributors.add(collaboration);
        }
      }
    }
    return contributors;
  }

  @Override
  public void setContributors(Object depositDocObj, List<AbstractContributor> contributors) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContributors() == null) {
      depositDoc.setContributors(this.factory.createContributors());
    }
    depositDoc.getContributors().getContributorOrCollaboration().clear();
    for (AbstractContributor abstractContributor : contributors) {
      if (abstractContributor instanceof ContributorDTO) {
        ContributorDTO contributorDTO = (ContributorDTO) abstractContributor;
        Contributor contributor = this.factory.createContributor();
        contributor.setRole(AuthorRole.fromValue(contributorDTO.getRole()));
        contributor.setFirstname(contributorDTO.getFirstName());
        contributor.setLastname(contributorDTO.getLastName());
        contributor.setEmail(contributorDTO.getEmail());
        contributor.setCnIndividu(contributorDTO.getCnIndividu());
        contributor.setInstitution(contributorDTO.getInstitution());
        contributor.setOrcid(contributorDTO.getOrcid());

        if (!contributorDTO.getOtherNames().isEmpty()) {
          contributor.setOtherNames(this.factory.createOtherNames());
          for (OtherNameDTO otherNameDTO : contributorDTO.getOtherNames()) {
            OtherName otherName = this.factory.createOtherName();
            otherName.setFirstname(otherNameDTO.getFirstName());
            otherName.setLastname(otherNameDTO.getLastName());
            contributor.getOtherNames().getOtherName().add(otherName);
          }
        }
        depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
      } else if (abstractContributor instanceof CollaborationDTO) {
        CollaborationDTO collaborationDTO = (CollaborationDTO) abstractContributor;
        Collaboration collaboration = this.factory.createCollaboration();
        collaboration.setName(collaborationDTO.getName());
        depositDoc.getContributors().getContributorOrCollaboration().add(collaboration);
      }
    }
  }

  @Override
  public List<ResearchGroup> getResearchGroups(Object depositDocObj) {
    List<ResearchGroup> researchGroups = new ArrayList<>();
    Groups groups = ((DepositDoc) depositDocObj).getGroups();
    if (groups != null) {
      List<Group> groupList = groups.getGroup();
      for (Group g : groupList) {
        ResearchGroup researchGroup = new ResearchGroup();
        researchGroup.setResId(g.getResId());
        researchGroup.setName(g.getName());
        researchGroup.setCode(g.getCode() != null ? g.getCode().toString() : null);
        researchGroups.add(researchGroup);
      }
    }
    return researchGroups;
  }

  @Override
  public List<Structure> getStructures(Object depositDocObj) {
    List<Structure> structures = new ArrayList<>();
    AcademicStructures academicStructures = ((DepositDoc) depositDocObj).getAcademicStructures();
    if (academicStructures != null) {
      List<AcademicStructure> academicStructuresList = academicStructures.getAcademicStructure();
      for (AcademicStructure academicStructure : academicStructuresList) {
        Structure structure = new Structure();
        structure.setResId(academicStructure.getResId());
        structure.setName(academicStructure.getName() != null ? academicStructure.getName() : null);
        structure.setCodeStruct(academicStructure.getCode() != null ? academicStructure.getCode() : null);
        structures.add(structure);
      }
    }
    return structures;
  }

  @Override
  public List<MetadataDates> getDates(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataDates> metadataDates = new ArrayList<>();
    if (depositDoc.getDates() != null) {
      List<DateWithType> datesWithType = depositDoc.getDates().getDate();
      for (DateWithType date : datesWithType) {
        MetadataDates metadataDate = new MetadataDates();
        metadataDate.setContent(date.getContent());
        metadataDate.setType(date.getType().value());
        metadataDates.add(metadataDate);
      }
    }
    return metadataDates;
  }

  @Override
  public MetadataDates getDateForGlobalYear(Object depositDocObj) {
    List<MetadataDates> metadataDates = this.getDates(depositDocObj);
    Optional<MetadataDates> publicationDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_PUBLICATION.equals(d.getType())).findFirst();
    if (publicationDateOpt.isPresent()) {
      return publicationDateOpt.get();
    }
    Optional<MetadataDates> firstOnlineDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_FIRST_ONLINE.equals(d.getType())).findFirst();
    if (firstOnlineDateOpt.isPresent()) {
      return firstOnlineDateOpt.get();
    }
    Optional<MetadataDates> imprimaturDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_IMPRIMATUR.equals(d.getType())).findFirst();
    if (imprimaturDateOpt.isPresent()) {
      return imprimaturDateOpt.get();
    }
    Optional<MetadataDates> defenseDateOpt = metadataDates.stream().filter(d -> DATE_TYPE_DEFENSE.equals(d.getType())).findFirst();
    if (defenseDateOpt.isPresent()) {
      return defenseDateOpt.get();
    }
    return null;
  }

  @Override
  public String getOriginalTitleContent(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getOriginalTitle() != null) {
      return depositDoc.getOriginalTitle().getContent();
    }
    return null;
  }

  @Override
  public String getOriginalTitleLang(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getOriginalTitle() != null) {
      return depositDoc.getOriginalTitle().getLang();
    }
    return null;
  }

  @Override
  public void setOriginalTitleLang(Object depositDocObj, String value) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getOriginalTitle() == null) {
      depositDoc.setOriginalTitle(this.factory.createTextLang());
    }
    depositDoc.getOriginalTitle().setLang(value);
  }

  @Override
  public List<MetadataTextLang> getAbstracts(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataTextLang> abstracts = new ArrayList<>();
    if (depositDoc.getAbstracts() != null && depositDoc.getAbstracts().getAbstract() != null) {
      for (Text abstractText : depositDoc.getAbstracts().getAbstract()) {
        abstracts.add(new MetadataTextLang(abstractText.getContent(), abstractText.getLang()));
      }
    }
    return abstracts;
  }

  @Override
  public void setAbstracts(Object depositDocObj, List<MetadataTextLang> abstracts) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getAbstracts() == null) {
      depositDoc.setAbstracts(this.factory.createAbstracts());
    }
    depositDoc.getAbstracts().getAbstract().clear();
    for (MetadataTextLang textLang : abstracts) {
      Text text = this.factory.createText();
      text.setContent(textLang.getContent());
      text.setLang(textLang.getLang());
      depositDoc.getAbstracts().getAbstract().add(text);
    }
  }

  @Override
  public List<MetadataLink> getLinks(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataLink> metadataLinks = new ArrayList<>();
    if (depositDoc.getLinks() != null && depositDoc.getLinks().getLink() != null) {
      for (Link link : depositDoc.getLinks().getLink()) {
        MetadataLink metadataLink = new MetadataLink();
        metadataLink.setTarget(link.getTarget());
        String description = link.getDescription() != null ? link.getDescription().getContent() : null;
        String lang = link.getDescription() != null ? link.getDescription().getLang() : null;
        metadataLink.setDescription(new MetadataTextLang(description, lang));
        if (link.getType() == LinkTypes.WEB) {
          metadataLink.setType(MetadataLink.LinkType.WEB);
        } else if (link.getType() == LinkTypes.INTERNAL) {
          metadataLink.setType(MetadataLink.LinkType.INTERNAL);
        } else if (link.getType() == null) {
          metadataLink.setType(null);
        }
        metadataLinks.add(metadataLink);
      }
    }
    return metadataLinks;
  }

  @Override
  public void setLinks(Object depositDocObj, List<MetadataLink> links) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getLinks() == null) {
      depositDoc.setLinks(this.factory.createLinks());
    }
    depositDoc.getLinks().getLink().clear();
    for (MetadataLink metadataLink : links) {
      Link link = this.factory.createLink();
      link.setTarget(metadataLink.getTarget());

      if (metadataLink.getDescription() != null) {
        Text text = this.factory.createText();
        text.setContent(metadataLink.getDescription().getContent());
        text.setLang(metadataLink.getDescription().getLang());
        link.setDescription(text);
      }

      if (metadataLink.getType() == MetadataLink.LinkType.WEB) {
        link.setType(LinkTypes.WEB);
      } else if (metadataLink.getType() == MetadataLink.LinkType.INTERNAL) {
        link.setType(LinkTypes.INTERNAL);
      }
      depositDoc.getLinks().getLink().add(link);
    }
  }

  @Override
  public List<MetadataCollection> getCollections(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataCollection> collections = new ArrayList<>();
    if (depositDoc.getCollections() != null && depositDoc.getCollections().getCollection() != null) {
      for (Collection collection : depositDoc.getCollections().getCollection()) {
        collections.add(new MetadataCollection(collection.getName(), collection.getNumber()));
      }
    }
    return collections;
  }

  @Override
  public List<MetadataFunding> getFundings(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataFunding> metadataFundings = new ArrayList<>();
    if (depositDoc.getFundings() != null && depositDoc.getFundings().getFunding() != null) {
      for (Funding funding : depositDoc.getFundings().getFunding()) {
        MetadataFunding metadataFunding = new MetadataFunding();
        metadataFunding.setFunder(funding.getFunder() != null ? funding.getFunder() : null);
        metadataFunding.setName(funding.getName() != null ? funding.getName() : null);
        metadataFunding.setCode(funding.getCode() != null ? funding.getCode() : null);
        metadataFunding.setAcronym(funding.getAcronym() != null ? funding.getAcronym() : null);
        metadataFundings.add(metadataFunding);
      }
    }
    return metadataFundings;
  }

  @Override
  public List<String> getDatasets(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<String> datasets = new ArrayList<>();
    if (depositDoc.getDatasets() != null && depositDoc.getDatasets().getUrl() != null) {
      for (String url : depositDoc.getDatasets().getUrl()) {
        datasets.add(!StringTool.isNullOrEmpty(url) ? url : null);
      }
    }
    return datasets;
  }

  @Override
  public List<MetadataFile> getFiles(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataFile> files = new ArrayList<>();
    if (depositDoc.getFiles() != null && depositDoc.getFiles().getFile() != null) {
      for (File file : depositDoc.getFiles().getFile()) {
        MetadataFile metadataFile = new MetadataFile();
        metadataFile.setResId(file.getResId());
        metadataFile.setType(file.getType().value());
        metadataFile.setName(file.getName());
        metadataFile.setSize(file.getSize());
        metadataFile.setChecksum(file.getChecksum());
        metadataFile.setMimeType(file.getMimetype());
        metadataFile.setAccessLevel(file.getAccessLevel().value());
        metadataFile.setLabel(file.getLabel());
        metadataFile.setLicense(file.getLicense());

        if (file.getEmbargo() != null) {
          try {
            MetadataEmbargo metadataEmbargo = new MetadataEmbargo();
            metadataEmbargo.setAccessLevel(file.getEmbargo().getAccessLevel().value());
            XMLGregorianCalendar endDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(file.getEmbargo().getEndDate().toString());
            metadataEmbargo.setEndDate(LocalDate.of(endDate.getYear(), endDate.getMonth(), endDate.getDay()));
            metadataFile.setEmbargo(metadataEmbargo);
          } catch (DatatypeConfigurationException e) {
            log.error("unable to parse embargo end date", e);
          }
        }

        files.add(metadataFile);
      }
    }
    return files;
  }

  @Override
  public List<MetadataCorrection> getCorrections(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataCorrection> metadataCorrections = new ArrayList<>();
    if (depositDoc.getCorrections() != null && depositDoc.getCorrections().getCorrection() != null) {
      List<Correction> corrections = depositDoc.getCorrections().getCorrection();
      for (Correction correction : corrections) {
        MetadataCorrection metadataCorrection = new MetadataCorrection();
        metadataCorrection.setDoi(correction.getDoi());
        metadataCorrection.setPmid(correction.getPmid());
        metadataCorrection.setNote(correction.getNote());
        metadataCorrections.add(metadataCorrection);
      }
    }
    return metadataCorrections;
  }

  @Override
  public String getPaging(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getPages() != null) {
      for (JAXBElement<String> element : depositDoc.getPages().getPagingOrOther()) {
        if (element.getName().getLocalPart().equals("paging")) {
          return element.getValue();
        }
      }
    }
    return null;
  }

  @Override
  public String getPagingOther(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getPages() != null) {
      for (JAXBElement<String> element : depositDoc.getPages().getPagingOrOther()) {
        if (element.getName().getLocalPart().equals("other")) {
          return element.getValue();
        }
      }
    }
    return null;
  }

  public Pages getPages(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getPages();
  }

  @Override
  public String getPublisherVersionUrl(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getPublisherVersionUrl();
  }

  @Override
  public String getNote(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getNote();
  }

  @Override
  public List<String> getKeywords(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getKeywords() != null) {
      return depositDoc.getKeywords().getKeyword();
    }
    return new ArrayList<>();
  }

  @Override
  public List<MetadataClassification> getClassifications(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    List<MetadataClassification> metadataClassifications = new ArrayList<>();
    if (depositDoc.getClassifications() != null) {
      for (Classification classification : depositDoc.getClassifications().getClassification()) {
        metadataClassifications.add(new MetadataClassification(classification.getCode(), classification.getItem()));
      }
    }
    return metadataClassifications;
  }

  @Override
  public String getDiscipline(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getDiscipline();
  }

  @Override
  public String getMandator(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getMandator();
  }

  @Override
  public String getPublisherName(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getPublisher() != null) {
      for (JAXBElement<String> element : depositDoc.getPublisher().getNameOrPlace()) {
        if (element.getName().getLocalPart().equals("name")) {
          return element.getValue();
        }
      }
    }
    return null;
  }

  @Override
  public String getPublisherPlace(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getPublisher() != null) {
      for (JAXBElement<String> element : depositDoc.getPublisher().getNameOrPlace()) {
        if (element.getName().getLocalPart().equals("place")) {
          return element.getValue();
        }
      }
    }
    return null;
  }

  @Override
  public String getEdition(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getEdition();
  }

  @Override
  public String getAward(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getAward();
  }

  @Override
  public boolean isBeforeUnige(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return Boolean.TRUE.equals(depositDoc.isIsBeforeUnige());
  }

  @Override
  public String getContainerTitle(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getTitle();
    }
    return null;
  }

  @Override
  public String getContainerVolume(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getVolume();
    }
    return null;
  }

  @Override
  public String getContainerIssue(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getIssue();
    }
    return null;
  }

  @Override
  public String getContainerSpecialIssue(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getSpecialIssue();
    }
    return null;
  }

  @Override
  public String getContainerEditor(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getEditor();
    }
    return null;
  }

  @Override
  public String getContainerConferencePlace(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getConferencePlace();
    }
    return null;
  }

  @Override
  public String getContainerConferenceDate(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getConferenceDate();
    }
    return null;
  }

  @Override
  public String getContainerConferenceSubtitle(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    if (depositDoc.getContainer() != null) {
      return depositDoc.getContainer().getConferenceSubtitle();
    }
    return null;
  }

  @Override
  public String getAouCollection(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getAouCollection();
  }

  @Override
  public String getDoctorAddress(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getDoctorAddress();
  }

  @Override
  public String getDoctorEmail(Object depositDocObj) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;
    return depositDoc.getDoctorEmail();
  }

  @Override
  public void fillAdditionalMetadata(Object depositDocObj, Map<String, Object> additionalMetadata) {
    DepositDoc depositDoc = (DepositDoc) depositDocObj;

    // Global diffusion level
    if (additionalMetadata.containsKey(AouConstants.INDEX_FIELD_DIFFUSION)) {
      this.ensureAdditionalMetadataExists(depositDoc);
      depositDoc.getAdditionalMetadata().setDiffusion(additionalMetadata.get(AouConstants.INDEX_FIELD_DIFFUSION).toString());
    }

    // Structures with parent name
    if (additionalMetadata.containsKey(AouConstants.INDEX_FIELD_STRUCTURES_WITH_PARENT)) {
      this.ensureStructuresExists(depositDoc);
      for (List<String> structure : (List<List<String>>) additionalMetadata.get(AouConstants.INDEX_FIELD_STRUCTURES_WITH_PARENT)) {
        StringBuilder sb = new StringBuilder();
        for (String structureItem : structure) {
          if (sb.isEmpty()) {
            sb.append(structureItem);
          } else {
            sb.insert(0, " / ");
            sb.insert(0, structureItem);
          }
        }
        depositDoc.getAdditionalMetadata().getStructures().getStructure().add(sb.toString());
      }
    }

    // Contributors' ORCID
    if (additionalMetadata.containsKey(AouConstants.INDEX_FIELD_CONTRIBUTORS) && !((List) additionalMetadata.get(
            AouConstants.INDEX_FIELD_CONTRIBUTORS)).isEmpty()) {
      List<Map<String, String>> propContributors = (List<Map<String, String>>) additionalMetadata.get(AouConstants.INDEX_FIELD_CONTRIBUTORS);
      List<Contributor> xmlContributors = ((DepositDoc) depositDocObj).getContributors().getContributorOrCollaboration().stream()
              .filter(Contributor.class::isInstance).map(Contributor.class::cast).collect(Collectors.toList());
      for (Contributor contributor : xmlContributors) {
        if (StringTool.isNullOrEmpty(contributor.getOrcid()) && !StringTool.isNullOrEmpty(contributor.getCnIndividu())) {
          // For UNIGE contributors, check if there is a corresponding ORCID in properties
          Optional<String> orcidOpt = propContributors.stream()
                  .filter(m -> m.containsKey("cnIndividu") && m.get("cnIndividu").equals(contributor.getCnIndividu()) && m.containsKey("orcid"))
                  .map(m -> m.get("orcid")).findFirst();
          if (orcidOpt.isPresent()) {
            contributor.setOrcid(orcidOpt.get());
          }
        }
      }
    }
  }

  private void ensureAdditionalMetadataExists(DepositDoc depositDoc) {
    if (depositDoc.getAdditionalMetadata() == null) {
      depositDoc.setAdditionalMetadata(this.factory.createAdditionalMetadata());
    }
  }

  private void ensureStructuresExists(DepositDoc depositDoc) {
    this.ensureAdditionalMetadataExists(depositDoc);
    if (depositDoc.getAdditionalMetadata().getStructures() == null) {
      depositDoc.getAdditionalMetadata().setStructures(this.factory.createStructures());
    }
  }

  @Override
  public String serializeDepositDocToXml(Object depositDocObject) {
    try {
      DepositDoc depositDoc = (DepositDoc) depositDocObject;
      JAXBContext jaxbContext = JAXBContext.newInstance(DepositDoc.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(depositDoc, sw);
      return sw.toString();
    } catch (Exception e) {
      throw new SolidifyRuntimeException("unable to serialize DepositDoc to XML", e);
    }
  }
}
