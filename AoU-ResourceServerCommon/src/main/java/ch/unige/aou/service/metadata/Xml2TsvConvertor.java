/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - Xml2CsvConvertor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.metadata;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.Abstracts;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructure;
import ch.unige.aou.model.xml.deposit.v2_4.Classification;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Collection;
import ch.unige.aou.model.xml.deposit.v2_4.Container;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.Contributors;
import ch.unige.aou.model.xml.deposit.v2_4.Correction;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.Dates;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v2_4.File;
import ch.unige.aou.model.xml.deposit.v2_4.Funding;
import ch.unige.aou.model.xml.deposit.v2_4.Group;
import ch.unige.aou.model.xml.deposit.v2_4.Pages;
import ch.unige.aou.model.xml.deposit.v2_4.Text;
import ch.unige.aou.model.xml.list_deposits.Publication;
import ch.unige.aou.model.xml.list_deposits.Publications;

public class Xml2TsvConvertor {

  public static String CELL_DELIMITER = "\t";
  public static String CELL_SEPARATOR = "|";

  public static String convertAllToTSV(Publications publications, Map<String, Map<String, Object>> additionalInfoPublications) {
    StringBuilder strBuilder = Xml2TsvConvertor.generateCommonHeaders(); //generate common header
    // add more headers
    strBuilder.append(CELL_DELIMITER).append("DepositDate").append(CELL_DELIMITER).append("Depositor").append(CELL_DELIMITER)
            .append("Status").append(CELL_DELIMITER).append("ValidationDate").append(CELL_DELIMITER)
            .append("Validator").append(CELL_DELIMITER).append("ValidationStructure").append(CELL_DELIMITER)
            .append("ValidationStructureNotUnige").append(CELL_DELIMITER).append("validatorComment").append(CELL_DELIMITER)
            .append("PublicComment").append(CELL_DELIMITER).append("LastUpdateDate").append(CELL_DELIMITER)
            .append("LastPersonModify").append(CELL_DELIMITER);

    strBuilder.append("\n"); // add new line to separate headers

    Xml2TsvConvertor.fillDataForCommonHeaders(publications, strBuilder, additionalInfoPublications);

    return strBuilder.toString();
  }

  public static String convertToTSV(Publications publications) {
    StringBuilder strBuilder = Xml2TsvConvertor.generateCommonHeaders();
    strBuilder.append("\n"); // add new line to separate headers
    Xml2TsvConvertor.fillDataForCommonHeaders(publications, strBuilder, null);
    return strBuilder.toString();
  }

  private static StringBuilder generateCommonHeaders() {
    StringBuilder strBuilder = new StringBuilder();
    // Write Header
    strBuilder.append("pid").append(CELL_DELIMITER).append("Type").append(CELL_DELIMITER).append("Subtype").append(CELL_DELIMITER)
            .append("Subsubtype").append(CELL_DELIMITER)
            .append("Title").append(CELL_DELIMITER).append("Languages").append(CELL_DELIMITER).append("OriginalTitle").append(CELL_DELIMITER)
            .append("Language").append(CELL_DELIMITER).append("Publication date").append(CELL_DELIMITER).append("First online date")
            .append(CELL_DELIMITER)
            .append("Imprimatur date").append(CELL_DELIMITER).append("Defense date").append(CELL_DELIMITER)
            .append("Contributors(Lastname;FirstName;Role;Orcid;Email;CnIndividu;Institution;Structure)").append(CELL_DELIMITER).append("Pages")
            .append(CELL_DELIMITER).append("Doi").append(CELL_DELIMITER).append("Pmid").append(CELL_DELIMITER).append("Isbn")
            .append(CELL_DELIMITER).append("Issn").append(CELL_DELIMITER).append("arxiv").append(CELL_DELIMITER).append("Identifiers")
            .append(CELL_DELIMITER).append("PublisherVersionURL").append(CELL_DELIMITER).append("Abstracts").append(CELL_DELIMITER)
            .append("Note").append(CELL_DELIMITER).append("Keywords").append(CELL_DELIMITER).append("Classifications").append(CELL_DELIMITER)
            .append("Discipline").append(CELL_DELIMITER).append("Mandator").append(CELL_DELIMITER).append("Collections(Name;Number;)")
            .append(CELL_DELIMITER).append("Edition").append(CELL_DELIMITER).append("Award").append(CELL_DELIMITER)
            .append("Fundings(Funder;Name;Code;Acronym;Jurisdiction;Program)").append(CELL_DELIMITER).append("AcademicStructures")
            .append(CELL_DELIMITER).append("IsBeforeUnige").append(CELL_DELIMITER).append("Groups").append(CELL_DELIMITER).append("Datasets")
            .append(CELL_DELIMITER).append("Container(Title;Editor;Volume;Issue;SpecialIssue;ConferenceDate;ConferencePlace)")
            .append(CELL_DELIMITER).append("AOUCollection").append(CELL_DELIMITER)
            .append("Files(Name;Type;AccessLevel;Size;Mimetype;License;EmbargoDate;EmbargoAccessLevel)").append(CELL_DELIMITER)
            .append("Corrections(Doi;Pmid;Note)").append(CELL_DELIMITER).append("DoctorEmail").append(CELL_DELIMITER).append("DoctorAddress")
            .append(CELL_DELIMITER).append("AdditionalMetadata");

    return strBuilder;
  }

  private static void fillDataForCommonHeaders(Publications publications, StringBuilder strBuilder,
          Map<String, Map<String, Object>> additionalInfoPublications) {
    for (Publication publication : publications.getPublication()) {
      Xml2TsvConvertor.appendValue(strBuilder, publication.getArchiveId());
      appendValue(strBuilder, publication.getDepositDoc().getType());
      appendValue(strBuilder, publication.getDepositDoc().getSubtype());
      appendValue(strBuilder, publication.getDepositDoc().getSubsubtype());
      appendValue(strBuilder, publication.getDepositDoc().getTitle().getContent());

      strBuilder.append(String.join(";", publication.getDepositDoc().getLanguages().getLanguage())).append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(
                      publication.getDepositDoc().getOriginalTitle() != null ? publication.getDepositDoc().getOriginalTitle().getContent() : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getOriginalTitle() != null ? publication.getDepositDoc().getOriginalTitle().getLang() : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(convertDates(publication.getDepositDoc().getDates()));
      strBuilder.append(
                      publication.getDepositDoc().getContributors() != null ? convertContributors(publication.getDepositDoc().getContributors()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getPages() != null ? convertPages(publication.getDepositDoc().getPages()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getIdentifiers() != null && publication.getDepositDoc().getIdentifiers().getDoi() != null ?
              publication.getDepositDoc().getIdentifiers().getDoi() : "").append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getIdentifiers() != null && publication.getDepositDoc().getIdentifiers().getPmid() != null ?
              publication.getDepositDoc().getIdentifiers().getPmid() : "").append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getIdentifiers() != null && publication.getDepositDoc().getIdentifiers().getIsbn() != null ?
              publication.getDepositDoc().getIdentifiers().getIsbn() : "").append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getIdentifiers() != null && publication.getDepositDoc().getIdentifiers().getIssn() != null ?
              publication.getDepositDoc().getIdentifiers().getIssn() : "").append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getIdentifiers() != null && publication.getDepositDoc().getIdentifiers().getArxiv() != null ?
              publication.getDepositDoc().getIdentifiers().getArxiv() : "").append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(
                      publication.getDepositDoc().getIdentifiers() != null ? convertIdentifiers(publication.getDepositDoc().getIdentifiers()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      appendValue(strBuilder, publication.getDepositDoc().getPublisherVersionUrl());
      strBuilder.append(publication.getDepositDoc().getAbstracts() != null ? convertAbstracts(publication.getDepositDoc().getAbstracts()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      appendValue(strBuilder, publication.getDepositDoc().getNote());
      strBuilder.append(
                      publication.getDepositDoc().getKeywords() != null ? convertKeywords(publication.getDepositDoc().getKeywords().getKeyword()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getClassifications() != null ?
                      convertClassifications(publication.getDepositDoc().getClassifications().getClassification()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      appendValue(strBuilder, publication.getDepositDoc().getDiscipline());
      appendValue(strBuilder, publication.getDepositDoc().getMandator());

      strBuilder.append(publication.getDepositDoc().getCollections() != null ?
              convertCollections(publication.getDepositDoc().getCollections().getCollection()) : "").append(Xml2TsvConvertor.CELL_DELIMITER);
      appendValue(strBuilder, publication.getDepositDoc().getEdition());
      appendValue(strBuilder, publication.getDepositDoc().getAward());

      strBuilder.append(
                      publication.getDepositDoc().getFundings() != null ? convertFundings(publication.getDepositDoc().getFundings().getFunding()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getAcademicStructures() != null ?
                      convertAcademicStructures(publication.getDepositDoc().getAcademicStructures().getAcademicStructure()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().isIsBeforeUnige() != null ? publication.getDepositDoc().isIsBeforeUnige().toString() : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getGroups() != null ? convertGroups(publication.getDepositDoc().getGroups().getGroup()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(
                      publication.getDepositDoc().getDatasets() != null ? convertDatasets(publication.getDepositDoc().getDatasets().getUrl()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getContainer() != null ? convertContainer(publication.getDepositDoc().getContainer()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      appendValue(strBuilder, publication.getDepositDoc().getAouCollection());

      strBuilder.append(publication.getDepositDoc().getFiles() != null ? convertFiles(publication.getDepositDoc().getFiles().getFile()) : "")
              .append(Xml2TsvConvertor.CELL_DELIMITER);
      strBuilder.append(publication.getDepositDoc().getCorrections() != null ?
              convertCorrections(publication.getDepositDoc().getCorrections().getCorrection()) : "").append(Xml2TsvConvertor.CELL_DELIMITER);
      appendValue(strBuilder, publication.getDepositDoc().getDoctorEmail());
      appendValue(strBuilder, publication.getDepositDoc().getDoctorAddress());
      strBuilder.append(publication.getDepositDoc().getAdditionalMetadata() != null ?
              publication.getDepositDoc().getAdditionalMetadata().getDiffusion() :
              "").append(Xml2TsvConvertor.CELL_DELIMITER);

      if (additionalInfoPublications != null) {
        Map<String, Object> innerInfo = additionalInfoPublications.get(publication.getArchiveId());
        appendValue(strBuilder, innerInfo.get("depositDate"));
        appendValue(strBuilder, innerInfo.get("depositor"));
        appendValue(strBuilder, innerInfo.get("status"));
        appendValue(strBuilder, innerInfo.get("validationDate"));
        appendValue(strBuilder, innerInfo.get("validatorName"));
        strBuilder.append(innerInfo.get("validationStructure") != null ? convertListValues(innerInfo, "validationStructure") : "")
                .append(Xml2TsvConvertor.CELL_DELIMITER);
        strBuilder.append(innerInfo.get("validationStructuresNamesNotUnige") != null ?
                convertListValues(innerInfo, "validationStructuresNamesNotUnige") :
                "").append(Xml2TsvConvertor.CELL_DELIMITER);
        strBuilder.append(innerInfo.get("validatorComment") != null ? convertListValues(innerInfo, "validatorComment") : "")
                .append(Xml2TsvConvertor.CELL_DELIMITER);
        strBuilder.append(innerInfo.get("publicComment") != null ? convertListValues(innerInfo, "publicComment") : "")
                .append(Xml2TsvConvertor.CELL_DELIMITER);
        appendValue(strBuilder, innerInfo.get("lastUpdateDate"));
        appendValue(strBuilder, innerInfo.get("lastPersonModify"));
      }
      strBuilder.append("\n");
    }
  }

  private static String convertListValues(Map<String, Object> innerInfo, String type) {
    StringBuilder sb = new StringBuilder();
    ((List<String>) innerInfo.get(type)).forEach(text -> sb.append(CleanTool.replaceBlankCharsBySpace(text)).append(CELL_SEPARATOR));
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertDates(Dates dates) {
    StringBuilder sb = new StringBuilder();

    Optional<DateWithType> publicationDateOpt = dates.getDate().stream().filter(d -> d.getType().equals(DateTypes.PUBLICATION)).findFirst();
    Optional<DateWithType> onlineDateOpt = dates.getDate().stream().filter(d -> d.getType().equals(DateTypes.FIRST_ONLINE)).findFirst();
    Optional<DateWithType> imprimaturDateOpt = dates.getDate().stream().filter(d -> d.getType().equals(DateTypes.IMPRIMATUR)).findFirst();
    Optional<DateWithType> defenseDateOpt = dates.getDate().stream().filter(d -> d.getType().equals(DateTypes.DEFENSE)).findFirst();

    String publicationDate = publicationDateOpt.isPresent() ? publicationDateOpt.get().getContent() : "";
    String onlineDate = onlineDateOpt.isPresent() ? onlineDateOpt.get().getContent() : "";
    String imprimaturDate = imprimaturDateOpt.isPresent() ? imprimaturDateOpt.get().getContent() : "";
    String defenseDate = defenseDateOpt.isPresent() ? defenseDateOpt.get().getContent() : "";

    sb.append(publicationDate).append(Xml2TsvConvertor.CELL_DELIMITER);
    sb.append(onlineDate).append(Xml2TsvConvertor.CELL_DELIMITER);
    sb.append(imprimaturDate).append(Xml2TsvConvertor.CELL_DELIMITER);
    sb.append(defenseDate).append(Xml2TsvConvertor.CELL_DELIMITER);

    return sb.toString();
  }

  private static String convertContributors(Contributors contributors) {
    StringBuilder sb = new StringBuilder();
    for (Object contributor : contributors.getContributorOrCollaboration()) {
      if (contributor instanceof Contributor) {
        sb.append(((Contributor) contributor).getLastname()).append(";")
                .append(((Contributor) contributor).getFirstname() != null ? ((Contributor) contributor).getFirstname() : "").append(";")
                .append(((Contributor) contributor).getRole().value()).append(";")
                .append(((Contributor) contributor).getOrcid() != null ? ((Contributor) contributor).getOrcid() : "").append(";")
                .append(((Contributor) contributor).getEmail() != null ? ((Contributor) contributor).getEmail() : "").append(";")
                .append(((Contributor) contributor).getCnIndividu() != null ? ((Contributor) contributor).getCnIndividu() : "").append(";")
                .append(((Contributor) contributor).getInstitution() != null ? ((Contributor) contributor).getInstitution() : "").append(";")
                .append(((Contributor) contributor).getStructure() != null ? ((Contributor) contributor).getStructure() : "")
                .append(CELL_SEPARATOR);

      } else if (contributor instanceof Collaboration) {
        sb.append(((Collaboration) contributor).getName()).append(CELL_SEPARATOR);
      }
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertPages(Pages pages) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < pages.getPagingOrOther().size(); i++) {
      sb.append(pages.getPagingOrOther().get(i).getValue()).append(";");
    }
    return sb.toString();
  }

  private static String convertIdentifiers(DepositIdentifiers identifiers) {
    StringBuilder sb = new StringBuilder();
    if (identifiers.getMmsid() != null)
      sb.append("Mmsid: ").append(identifiers.getMmsid()).append(CELL_SEPARATOR);
    if (identifiers.getRepec() != null)
      sb.append("Repec: ").append(identifiers.getRepec()).append(CELL_SEPARATOR);
    if (identifiers.getDblp() != null)
      sb.append("Dblp: ").append(identifiers.getDblp()).append(CELL_SEPARATOR);
    if (identifiers.getUrn() != null)
      sb.append("Urn: ").append(identifiers.getUrn()).append(CELL_SEPARATOR);
    if (identifiers.getPmcid() != null)
      sb.append("PmcId: ").append(identifiers.getPmcid()).append(CELL_SEPARATOR);
    if (identifiers.getLocalNumber() != null)
      sb.append("Local Number: ").append(identifiers.getLocalNumber()).append(CELL_SEPARATOR);
    return sb.toString();
  }

  private static String convertAbstracts(Abstracts abstracts) {
    StringBuilder sb = new StringBuilder();
    sb.append("\"");

    for (Text abstractText : abstracts.getAbstract()) {
      sb.append(CleanTool.replaceBlankCharsBySpace(abstractText.getContent())).append(";")
              .append(abstractText.getLang()).append(CELL_SEPARATOR);
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.append("\"").toString();
  }

  private static String convertKeywords(List<String> keywords) {
    return String.join(";", keywords);
  }

  private static String convertClassifications(List<Classification> classifications) {
    StringBuilder sb = new StringBuilder();
    for (Classification classification : classifications) {
      sb.append(classification.getCode()).append(";")
              .append(classification.getItem()).append(CELL_SEPARATOR);
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertCollections(List<Collection> collections) {
    StringBuilder sb = new StringBuilder();
    for (Collection collection : collections) {
      sb.append(collection.getName()).append(";")
              .append(collection.getNumber()).append(CELL_SEPARATOR);
    }
    // Delete last CELL_SEPARATOR
    sb.deleteCharAt(sb.length() - 1);
    return sb.toString();
  }

  private static String convertFundings(List<Funding> fundings) {
    StringBuilder sb = new StringBuilder();
    for (Funding funding : fundings) {
      sb.append(funding.getFunder()).append(";")
              .append(funding.getName()).append(";")
              .append(funding.getCode() != null ? funding.getCode() : "").append(";")
              .append(funding.getAcronym() != null ? funding.getAcronym() : "").append(";")
              .append(funding.getJurisdiction()).append(";")
              .append(funding.getProgram()).append(CELL_SEPARATOR);
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertAcademicStructures(List<AcademicStructure> academicStructures) {
    StringBuilder sb = new StringBuilder();
    for (AcademicStructure structure : academicStructures) {
      sb.append(structure.getName()).append(";")
              .append(structure.getCode()).append(CELL_SEPARATOR);
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertGroups(List<Group> groups) {
    StringBuilder sb = new StringBuilder();
    for (Group group : groups) {
      sb.append(group.getName()).append(";")
              .append(group.getCode() != null ? group.getCode() : "").append(CELL_SEPARATOR);
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertDatasets(List<String> urls) {
    StringBuilder sb = new StringBuilder();
    for (String url : urls) {
      sb.append(url).append(";");
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertContainer(Container container) {
    StringBuilder sb = new StringBuilder();
    sb.append(container.getTitle()).append(";");
    sb.append(container.getEditor()).append(";");
    sb.append(container.getVolume()).append(";");
    sb.append(container.getIssue()).append(";");
    sb.append(container.getSpecialIssue()).append(";");
    sb.append(container.getConferenceSubtitle()).append(";");
    sb.append(container.getConferenceDate()).append(";");
    sb.append(container.getConferencePlace()).append(";");

    return sb.toString();
  }

  private static String convertFiles(List<File> files) {
    StringBuilder sb = new StringBuilder();
    for (File file : files) {
      sb.append(file.getName()).append(";")
              .append(file.getType()).append(";")
              .append(file.getAccessLevel().value()).append(";")
              .append(file.getSize()).append(";")
              .append(file.getMimetype() != null ? file.getMimetype() : "").append(";")
              .append(file.getLicense() != null ? file.getLicense() : "").append(";")
              .append(file.getEmbargo() != null ? file.getEmbargo().getEndDate() : "").append(";")
              .append(file.getEmbargo() != null ? file.getEmbargo().getAccessLevel() : "").append(CELL_SEPARATOR);
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }

  private static String convertCorrections(List<Correction> corrections) {
    StringBuilder sb = new StringBuilder();

    for (Correction correction : corrections) {
      sb.append(correction.getNote()).append(";")
              .append(correction.getDoi() != null ? correction.getDoi() : "").append(";")
              .append(correction.getPmid() != null ? correction.getPmid() : "").append(CELL_SEPARATOR);
    }
    if (!sb.isEmpty()) {
      // Delete last CELL_SEPARATOR
      sb.deleteCharAt(sb.length() - 1);
    }

    return sb.toString();
  }

  private static void appendValue(StringBuilder stringBuilder, Object value) {
    if (value != null) {
      stringBuilder.append(value);
    } else {
      stringBuilder.append("");
    }
    stringBuilder.append(Xml2TsvConvertor.CELL_DELIMITER);
  }

  private static void appendValue(StringBuilder stringBuilder, String value) {
    if (value != null) {
      stringBuilder.append(value.replace(System.getProperty("line.separator"), " "));
    } else {
      stringBuilder.append("");
    }
    stringBuilder.append(Xml2TsvConvertor.CELL_DELIMITER);
  }

}
