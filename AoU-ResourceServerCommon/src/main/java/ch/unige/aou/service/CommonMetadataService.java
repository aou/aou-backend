/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - CommonMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import static ch.unige.aou.AouConstants.DATE_TYPE_DEFENSE;
import static ch.unige.aou.AouConstants.DATE_TYPE_FIRST_ONLINE;
import static ch.unige.aou.AouConstants.DATE_TYPE_IMPRIMATUR;
import static ch.unige.aou.AouConstants.DATE_TYPE_PUBLICATION;
import static ch.unige.aou.AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE;
import static ch.unige.aou.AouConstants.DOCUMENT_FILE_TYPE_ARTICLE;
import static ch.unige.aou.AouConstants.DOCUMENT_FILE_TYPE_BOOK;
import static ch.unige.aou.AouConstants.DOCUMENT_FILE_TYPE_BOOK_CHAPTER;
import static ch.unige.aou.AouConstants.DOCUMENT_FILE_TYPE_PREPRINT;
import static ch.unige.aou.AouConstants.DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER;
import static ch.unige.aou.AouConstants.DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE;
import static ch.unige.aou.AouConstants.EMBARGOED_ACCESS_TEXT;
import static ch.unige.aou.AouConstants.INDEX_FIELD_ARCHIVE_ID;
import static ch.unige.aou.AouConstants.INDEX_FIELD_ARCHIVE_ID_INT;
import static ch.unige.aou.AouConstants.INDEX_FIELD_DEFENSE_YEAR;
import static ch.unige.aou.AouConstants.INDEX_FIELD_FIRST_ONLINE_YEAR;
import static ch.unige.aou.AouConstants.INDEX_FIELD_FULLTEXTS;
import static ch.unige.aou.AouConstants.INDEX_FIELD_IMPRIMATUR_YEAR;
import static ch.unige.aou.AouConstants.INDEX_FIELD_PUBLICATION_YEAR;
import static ch.unige.aou.AouConstants.INDEX_FIELD_YEAR;
import static ch.unige.aou.AouConstants.INDEX_FIELD_YEARS;
import static ch.unige.aou.AouConstants.NOT_OPEN_ACCESS_TEXT;
import static ch.unige.aou.AouConstants.OPEN_ACCESS_TEXT;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAIRecord;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OAIMetadataService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.AouXmlNamespacePrefixMapper;
import ch.unige.aou.AouXmlNamespacePrefixMapperV1;
import ch.unige.aou.AouXmlNamespacePrefixMapperV2_0;
import ch.unige.aou.AouXmlNamespacePrefixMapperV2_1;
import ch.unige.aou.AouXmlNamespacePrefixMapperV2_2;
import ch.unige.aou.AouXmlNamespacePrefixMapperV2_3;
import ch.unige.aou.AouXmlNamespacePrefixMapperV2_4;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.exception.AoUMetadataParseException;
import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.publication.DateType;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.MetadataClassification;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataCorrection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.MetadataEmbargo;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.publication.MetadataFunding;
import ch.unige.aou.model.publication.MetadataLink;
import ch.unige.aou.model.publication.MetadataTextLang;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtypeDTO;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileTypeDTO;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.service.metadata.DepositDocAdapter;
import ch.unige.aou.service.metadata.DepositDocV1Adapter;
import ch.unige.aou.service.metadata.DepositDocV2Adapter;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileTypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedLicenseRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStructureRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
public class CommonMetadataService extends AouService implements OAIMetadataService {

  private static final Logger log = LoggerFactory.getLogger(CommonMetadataService.class);

  private static final String INDEX_RES_ID_FIELD = SolidifyConstants.RES_ID_FIELD;
  private static final String INDEX_ACRONYM_FIELD = "acronym";
  private static final String METADATA_ERROR_VERSION = "metadata.error.version";
  private static final String FIRST_NAME = "firstName";
  private static final String LAST_NAME = "lastName";
  private static final String FULL_NAME = "fullName";

  private static final String TYPE_LEVEL = "typeLevel";

  private static final String SORT_VALUE = "sortValue";
  private static final String CITATION_AUTHOR = "citation_author";
  private static final String CITATION_PUBLICATION_DATE = "citation_publication_date";

  private static final int MAX_INDEXED_FULLTEXTS = 3;

  private final TrustedStructureRemoteResourceService remoteStructureService;
  private final TrustedPublicationRemoteResourceService remotePublicationService;
  private final TrustedPublicationSubtypeRemoteResourceService remotePublicationSubtypeService;
  private final TrustedDocumentFileRemoteResourceService remoteDocumentFileService;
  private final TrustedDocumentFileTypeRemoteResourceService remoteDocumentFileTypeService;
  private final TrustedContributorRemoteResourceService remoteContributorService;
  private final TrustedLicenseRemoteResourceService remoteLicenseService;
  private final TrustedUserRemoteResourceService remoteUserService;

  private final String[] metadataDateFormats;
  private final String downloadFileUrlDownloadTemplate;

  public CommonMetadataService(
          AouProperties aouProperties,
          MessageService messageService,
          TrustedStructureRemoteResourceService remoteStructureService,
          TrustedPublicationRemoteResourceService remotePublicationService,
          TrustedDocumentFileRemoteResourceService remoteDocumentFileService,
          TrustedPublicationSubtypeRemoteResourceService remotePublicationSubtypeService,
          TrustedDocumentFileTypeRemoteResourceService remoteDocumentFileTypeService,
          TrustedContributorRemoteResourceService remoteContributorService,
          TrustedLicenseRemoteResourceService remoteLicenseService,
          TrustedUserRemoteResourceService remoteUserService) {

    super(messageService);

    this.remoteStructureService = remoteStructureService;
    this.remotePublicationService = remotePublicationService;
    this.remoteDocumentFileService = remoteDocumentFileService;
    this.remotePublicationSubtypeService = remotePublicationSubtypeService;
    this.remoteDocumentFileTypeService = remoteDocumentFileTypeService;
    this.remoteContributorService = remoteContributorService;
    this.remoteLicenseService = remoteLicenseService;
    this.remoteUserService = remoteUserService;

    this.metadataDateFormats = aouProperties.getParameters().getMetadataDateFormat();
    this.downloadFileUrlDownloadTemplate = aouProperties.getParameters().getDownloadFileUrlTemplate();
  }

  public AouXmlNamespacePrefixMapper getAouXmlNamespacePrefixMapper(AouMetadataVersion version) {
    return switch (version) {
      case V1_0 -> new AouXmlNamespacePrefixMapperV1();
      case V2_0 -> new AouXmlNamespacePrefixMapperV2_0();
      case V2_1 -> new AouXmlNamespacePrefixMapperV2_1();
      case V2_2 -> new AouXmlNamespacePrefixMapperV2_2();
      case V2_3 -> new AouXmlNamespacePrefixMapperV2_3();
      case V2_4 -> new AouXmlNamespacePrefixMapperV2_4();
      default -> throw new SolidifyProcessingException(this.messageService.get(METADATA_ERROR_VERSION, new Object[] { version }));
    };
  }

  public AouMetadataVersion detectVersionFromXmlMetadata(String xmlMetadata) {
    try {
      Document doc = XMLTool.parseXML(xmlMetadata);
      String namespace = doc.getNamespaceURI();

      for (AouMetadataVersion version : AouMetadataVersion.getAllVersions()) {
        AouXmlNamespacePrefixMapper mapper = this.getAouXmlNamespacePrefixMapper(version);
        if (XMLTool.getFirstNode(doc, "/" + AouConstants.XML_NAMESPACE_DEPOSIT_PREFIX + ":deposit_doc", mapper.getNamespacePrefixes()) != null) {
          return version;
        }
      }
      throw new SolidifyProcessingException("namespace '" + namespace + "' does not correspond to any metadata version");
    } catch (SAXException e) {
      throw new AoUMetadataParseException("Unable to parse metadata", e);
    } catch (ParserConfigurationException | IOException e) {
      throw new SolidifyRuntimeException("unable to detect metadata version", e);
    }
  }

  public Object getDepositDoc(String xmlMetadata) {
    AouMetadataVersion version = this.detectVersionFromXmlMetadata(xmlMetadata);
    return switch (version) {
      case V1_0 -> JAXB.unmarshal(new StringReader(xmlMetadata), ch.unige.aou.model.xml.deposit.v1.DepositDoc.class);
      case V2_0, V2_1, V2_2, V2_3, V2_4 -> JAXB.unmarshal(new StringReader(xmlMetadata), DepositDoc.class);
      default -> throw new SolidifyProcessingException(this.messageService.get(METADATA_ERROR_VERSION, new Object[] { version }));
    };
  }

  public DepositDocAdapter getDepositDocAdapter(AouMetadataVersion version) {
    return switch (version) {
      case V1_0 -> new DepositDocV1Adapter();
      case V2_0, V2_1, V2_2, V2_3, V2_4 -> new DepositDocV2Adapter();
      default -> throw new SolidifyProcessingException(this.messageService.get(METADATA_ERROR_VERSION, new Object[] { version }));
    };
  }

  /*****************************************************************************************************/

  public PublicationIndexEntry getPublicationIndexEntry(Publication publication, String xmlMetadata) {
    PublicationIndexEntry publicationIndexEntry = new PublicationIndexEntry();
    publicationIndexEntry.setResId(publication.getResId());

    publicationIndexEntry.setMetadata(this.xmlMetadataToMap(publication.getResId(), xmlMetadata));

    // Additional information
    publicationIndexEntry.getMetadata().put(INDEX_FIELD_ARCHIVE_ID, publication.getArchiveId());
    if (!StringTool.isNullOrEmpty(publication.getArchiveId())) {
      publicationIndexEntry.getMetadata().put(INDEX_FIELD_ARCHIVE_ID_INT, publication.getArchiveId().replaceAll("[^0-9]", ""));
    }

    // add publication status

    // add publication creator
    Map<String, String> creatorMap = new HashMap<>();
    creatorMap.put(INDEX_RES_ID_FIELD, publication.getCreator().getResId());
    creatorMap.put(FIRST_NAME, publication.getCreator().getFirstName());
    creatorMap.put(LAST_NAME, publication.getCreator().getLastName());
    creatorMap.put(FULL_NAME, publication.getCreator().getFullName());
    publicationIndexEntry.getMetadata().put("creator", creatorMap);

    // add publication technical infos
    Map<String, Object> technicalInfosMap = new HashMap<>();
    technicalInfosMap.put("status", publication.getStatus().toString());
    technicalInfosMap.put("lastIndexationDate", OffsetDateTime.now(ZoneOffset.UTC));
    technicalInfosMap.put("creationTime", publication.getCreationTime());
    technicalInfosMap.put("updateTime", publication.getUpdateTime());
    technicalInfosMap.put("lastStatusUpdate", publication.getLastStatusUpdate());

    List<StatusHistory> statusHistories = this.remotePublicationService.history(publication.getResId());
    this.putIfNotNull(technicalInfosMap, "firstValidation", this.getFirstValidationDateTime(statusHistories));

    String firstValidatorName = this.getFirstValidatorName(statusHistories);
    if (!StringTool.isNullOrEmpty(firstValidatorName)) {
      technicalInfosMap.put("validatorName", firstValidatorName);
    }

    publicationIndexEntry.getMetadata().put("technicalInfos", technicalInfosMap);

    return publicationIndexEntry;
  }

  private Map<String, Object> xmlMetadataToMap(String publicationId, String xmlMetadata) {

    Map<String, Object> properties = new HashMap<>();

    AouMetadataVersion version = this.detectVersionFromXmlMetadata(xmlMetadata);
    Object depositDoc = this.getDepositDoc(xmlMetadata);
    DepositDocAdapter depositDocAdapter = this.getDepositDocAdapter(version);

    this.putIfNotNull(properties, "type", depositDocAdapter.getType(depositDoc));
    String subtypeName = depositDocAdapter.getSubtype(depositDoc);
    this.putIfNotNull(properties, "subtype", subtypeName);
    this.putIfNotNull(properties, AouConstants.INDEX_FIELD_SUBTYPE_CODE, this.getPublicationSubtypeIdByName(subtypeName));
    this.putIfNotNull(properties, "subsubtype", depositDocAdapter.getSubSubtype(depositDoc));
    this.putIfNotNull(properties, "languages", depositDocAdapter.getLanguages(depositDoc));

    this.putIfNotNull(properties, "doi", depositDocAdapter.getDOI(depositDoc));
    this.putIfNotNull(properties, "pmid", depositDocAdapter.getPmid(depositDoc));
    this.putIfNotNull(properties, "isbn", depositDocAdapter.getISBN(depositDoc));
    this.putIfNotNull(properties, "issn", depositDocAdapter.getISSN(depositDoc));
    this.putIfNotNull(properties, "arxiv", depositDocAdapter.getArxivId(depositDoc));
    this.putIfNotNull(properties, "mmsid", depositDocAdapter.getMmsid(depositDoc));
    this.putIfNotNull(properties, "repec", depositDocAdapter.getRepec(depositDoc));
    this.putIfNotNull(properties, "dblp", depositDocAdapter.getDblp(depositDoc));
    this.putIfNotNull(properties, "urn", depositDocAdapter.getURN(depositDoc));
    this.putIfNotNull(properties, "pmcid", depositDocAdapter.getPmcId(depositDoc));
    this.putIfNotNull(properties, "localNumber", depositDocAdapter.getLocalNumber(depositDoc));

    this.putIfNotNull(properties, "publisherVersionUrl", depositDocAdapter.getPublisherVersionUrl(depositDoc));
    this.putIfNotNull(properties, "note", depositDocAdapter.getNote(depositDoc));
    this.putIfNotNull(properties, "keywords", depositDocAdapter.getKeywords(depositDoc));
    this.putIfNotNull(properties, "discipline", depositDocAdapter.getDiscipline(depositDoc));
    this.putIfNotNull(properties, "mandator", depositDocAdapter.getMandator(depositDoc));
    this.putIfNotNull(properties, "publisherName", depositDocAdapter.getPublisherName(depositDoc));
    this.putIfNotNull(properties, "publisherPlace", depositDocAdapter.getPublisherPlace(depositDoc));
    this.putIfNotNull(properties, "edition", depositDocAdapter.getEdition(depositDoc));
    this.putIfNotNull(properties, "award", depositDocAdapter.getAward(depositDoc));
    this.putIfNotNull(properties, "isBeforeUnige", depositDocAdapter.isBeforeUnige(depositDoc));
    this.putIfNotNull(properties, "datasets", depositDocAdapter.getDatasets(depositDoc));
    this.putIfNotNull(properties, "aouCollection", depositDocAdapter.getAouCollection(depositDoc));

    this.fillTitle(depositDoc, depositDocAdapter, properties);
    this.fillOriginalTitle(depositDoc, depositDocAdapter, properties);
    this.fillContributors(depositDoc, depositDocAdapter, properties);
    this.fillAbstracts(depositDoc, depositDocAdapter, properties);
    this.fillDates(depositDoc, depositDocAdapter, properties);
    this.fillYears(depositDoc, depositDocAdapter, properties);
    this.fillPages(depositDoc, depositDocAdapter, properties);
    this.fillClassifications(depositDoc, depositDocAdapter, properties);
    this.fillCollections(depositDoc, depositDocAdapter, properties);
    this.fillFundings(depositDoc, depositDocAdapter, properties);
    this.fillStructures(depositDoc, depositDocAdapter, properties);
    this.fillResearchGroups(depositDoc, depositDocAdapter, properties);
    this.fillContainer(depositDoc, depositDocAdapter, properties);
    this.fillLinks(depositDoc, depositDocAdapter, properties);
    this.fillFiles(depositDoc, depositDocAdapter, properties);
    this.fillPrincipalFiles(depositDoc, depositDocAdapter, properties);
    this.fillSecondaryCorrectiveFiles(depositDoc, depositDocAdapter, properties);
    this.fillCorrections(depositDoc, depositDocAdapter, properties);

    this.fillFulltextVersions(depositDoc, depositDocAdapter, properties);
    this.fillEmbargoEndDates(depositDoc, depositDocAdapter, properties);
    this.fillLicenses(depositDoc, depositDocAdapter, properties);
    this.fillCurrentDiffusion(depositDoc, depositDocAdapter, properties);
    this.fillPrincipalFilesCurrentAccessLevel(depositDoc, depositDocAdapter, properties);
    this.fillOpenAccess(depositDoc, depositDocAdapter, properties);
    this.fillFulltexts(publicationId, depositDoc, depositDocAdapter, properties);

    // XML
    depositDocAdapter.fillAdditionalMetadata(depositDoc, properties);
    xmlMetadata = depositDocAdapter.serializeDepositDocToXml(depositDoc);
    this.putIfNotNull(properties, AouConstants.INDEX_FIELD_XML, xmlMetadata);
    this.putIfNotNull(properties, AouConstants.INDEX_FIELD_METADATA_VERSION, version.getVersion());

    // HTML metadata headers
    this.fillHtmlMetadataHeaders(depositDoc, depositDocAdapter, properties);

    return properties;
  }

  private void fillTitle(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    this.putIfNotNull(properties, "title", depositDocAdapter.getTitleContent(depositDoc));
    this.putIfNotNull(properties, "titleLang", depositDocAdapter.getTitleLang(depositDoc));
  }

  private void fillOriginalTitle(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    this.putIfNotNull(properties, "originalTitle", depositDocAdapter.getOriginalTitleContent(depositDoc));
    this.putIfNotNull(properties, "originalTitleLang", depositDocAdapter.getOriginalTitleLang(depositDoc));
  }

  private void fillContributors(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> contributors = new ArrayList<>();
    Set<String> unigeDirectors = new HashSet<>();
    List<Map<String, Object>> contributorsWithOtherNames = new ArrayList<>();
    for (AbstractContributor abstractContributor : depositDocAdapter.getContributors(depositDoc)) {
      Map<String, Object> contributorMap = new HashMap<>();
      if (abstractContributor instanceof ContributorDTO) {

        contributorMap = this.getContributorMap((ContributorDTO) abstractContributor);

        String cnIndividu = ((ContributorDTO) abstractContributor).getCnIndividu();
        if (!StringTool.isNullOrEmpty(cnIndividu)) {
          // Fill UNIGE directors
          if (AouConstants.CONTRIBUTOR_ROLE_DIRECTOR.equals(((ContributorDTO) abstractContributor).getRole())) {
            unigeDirectors.add(cnIndividu);
          }

          // Fill contributors with their eventual other names to be able to find them with their old names during search
          List<Map<String, Object>> sameContributorAllNames = new ArrayList<>();
          sameContributorAllNames.add(contributorMap);
          Contributor contributor = this.remoteContributorService.getByCnIndividu(cnIndividu);
          if (contributor != null) {
            for (ContributorOtherName otherName : contributor.getOtherNames()) {
              this.addOtherNameIfDifferent(sameContributorAllNames, otherName);
            }
          }
          contributorsWithOtherNames.addAll(sameContributorAllNames);
        }

      } else {
        this.putIfNotNull(contributorMap, "type", "collaboration");
        this.putIfNotNull(contributorMap, "name", ((CollaborationDTO) abstractContributor).getName());
      }

      if (!contributorMap.isEmpty()) {
        contributors.add(contributorMap);
      }
    }

    if (!contributors.isEmpty()) {
      properties.put(AouConstants.INDEX_FIELD_CONTRIBUTORS, contributors);
    }

    if (!unigeDirectors.isEmpty()) {
      properties.put(AouConstants.INDEX_FIELD_UNIGE_DIRECTORS, unigeDirectors);
    }

    if (!contributorsWithOtherNames.isEmpty()) {
      properties.put("contributorsWithOtherNames", contributorsWithOtherNames);
    }
  }

  private Map<String, Object> getContributorMap(ContributorDTO contributorDTO) {
    Map<String, Object> contributorMap = new HashMap<>();
    contributorMap.put("type", "contributor");
    this.putIfNotNull(contributorMap, FIRST_NAME, contributorDTO.getFirstName());
    this.putIfNotNull(contributorMap, LAST_NAME, contributorDTO.getLastName());
    this.putIfNotNull(contributorMap, FULL_NAME, contributorDTO.getFullName());
    this.putIfNotNull(contributorMap, "cnIndividu", contributorDTO.getCnIndividu());
    this.putIfNotNull(contributorMap, "role", contributorDTO.getRole());
    this.putIfNotNull(contributorMap, "institution", contributorDTO.getInstitution());
    this.putIfNotNull(contributorMap, "email", contributorDTO.getEmail());
    this.putIfNotNull(contributorMap, "structure", contributorDTO.getStructure());
    if (!StringTool.isNullOrEmpty(contributorDTO.getOrcid())) {
      this.putIfNotNull(contributorMap, "orcid", contributorDTO.getOrcid());
    } else if (!StringTool.isNullOrEmpty(contributorDTO.getCnIndividu())) {
      String cnIndividu = contributorDTO.getCnIndividu();
      if (!cnIndividu.endsWith(AouConstants.UNIGE_EXTERNAL_UID_SUFFIX)) {
        cnIndividu += AouConstants.UNIGE_EXTERNAL_UID_SUFFIX;
      }
      User user = this.remoteUserService.findByExternalUid(cnIndividu);
      if (user != null && user.getPerson() != null && !StringTool.isNullOrEmpty(user.getPerson().getOrcid())) {
        this.putIfNotNull(contributorMap, "orcid", user.getPerson().getOrcid());
      }
    }

    return contributorMap;
  }

  private void addOtherNameIfDifferent(List<Map<String, Object>> sameContributorAllNames, ContributorOtherName otherName) {
    if (sameContributorAllNames.stream().noneMatch(m -> m.get(FULL_NAME).equals(otherName.getFullName()))) {
      Map<String, Object> firstContributor = sameContributorAllNames.get(0);
      Map<String, Object> otherNameContributor = new HashMap<>(firstContributor);
      otherNameContributor.put(FIRST_NAME, otherName.getFirstName());
      otherNameContributor.put(LAST_NAME, otherName.getLastName());
      otherNameContributor.put(FULL_NAME, otherName.getFullName());

      sameContributorAllNames.add(otherNameContributor);
    }
  }

  private void fillAbstracts(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, String>> abstracts = new ArrayList<>();
    for (MetadataTextLang metadataTextLang : depositDocAdapter.getAbstracts(depositDoc)) {
      Map<String, String> metadataMap = new HashMap<>();
      if (!StringTool.isNullOrEmpty(metadataTextLang.getContent())) {
        metadataMap.put("content", metadataTextLang.getContent());
      }
      if (!StringTool.isNullOrEmpty(metadataTextLang.getLang())) {
        metadataMap.put("language", metadataTextLang.getLang());
      }
      abstracts.add(metadataMap);
    }
    if (!abstracts.isEmpty()) {
      properties.put("abstracts", abstracts);
    }
  }

  private void fillDates(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, String>> dates = new ArrayList<>();
    for (MetadataDates metadataDate : depositDocAdapter.getDates(depositDoc)) {
      Map<String, String> metadataMap = new HashMap<>();
      String dateStr = metadataDate.getContent();
      if (dateStr != null) {
        dateStr = dateStr.trim();
      }
      if (!StringTool.isNullOrEmpty(dateStr)) {
        metadataMap.put("date", dateStr);
      }
      if (!StringTool.isNullOrEmpty(metadataDate.getType())) {
        metadataMap.put("type", DateType.fromMetadataValue(metadataDate.getType()).name());
      }
      dates.add(metadataMap);
    }
    if (!dates.isEmpty()) {
      properties.put("dates", dates);
    }
  }

  private void fillYears(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    Set<Integer> years = new HashSet<>();
    Map<String, Integer> yearsByType = new HashMap<>();
    for (MetadataDates metadataDate : depositDocAdapter.getDates(depositDoc)) {
      String dateStr = metadataDate.getContent();
      if (dateStr != null) {
        dateStr = dateStr.trim();
      }
      if (!StringTool.isNullOrEmpty(dateStr)) {
        Integer year = this.getYear(dateStr);
        if (year != null) {
          years.add(year);
          yearsByType.put(metadataDate.getType(), year);
        }
      }
    }
    if (!years.isEmpty()) {
      // Index all years for the publication
      properties.put(INDEX_FIELD_YEARS, years);
    }
    if (!yearsByType.isEmpty()) {
      // Index a global date for the publication
      Integer globalYear = null;
      if (yearsByType.containsKey(DATE_TYPE_PUBLICATION)) {
        globalYear = yearsByType.get(DATE_TYPE_PUBLICATION);
      } else if (yearsByType.containsKey(DATE_TYPE_FIRST_ONLINE)) {
        globalYear = yearsByType.get(DATE_TYPE_FIRST_ONLINE);
      } else if (yearsByType.containsKey(DATE_TYPE_IMPRIMATUR)) {
        globalYear = yearsByType.get(DATE_TYPE_IMPRIMATUR);
      } else if (yearsByType.containsKey(DATE_TYPE_DEFENSE)) {
        globalYear = yearsByType.get(DATE_TYPE_DEFENSE);
      }

      if (globalYear != null) {
        properties.put(INDEX_FIELD_YEAR, globalYear);
      }

      if (yearsByType.containsKey(DATE_TYPE_DEFENSE)) {
        properties.put(INDEX_FIELD_DEFENSE_YEAR, yearsByType.get(DATE_TYPE_DEFENSE));
      }
      if (yearsByType.containsKey(DATE_TYPE_IMPRIMATUR)) {
        properties.put(INDEX_FIELD_IMPRIMATUR_YEAR, yearsByType.get(DATE_TYPE_IMPRIMATUR));
      }
      if (yearsByType.containsKey(DATE_TYPE_PUBLICATION)) {
        properties.put(INDEX_FIELD_PUBLICATION_YEAR, yearsByType.get(DATE_TYPE_PUBLICATION));
      }
      if (yearsByType.containsKey(DATE_TYPE_FIRST_ONLINE)) {
        properties.put(INDEX_FIELD_FIRST_ONLINE_YEAR, yearsByType.get(DATE_TYPE_FIRST_ONLINE));
      }
    }
  }

  public Integer getYear(String dateStr) {
    return CleanTool.extractYearFromDate(dateStr, this.metadataDateFormats);
  }

  public Integer getMonth(String dateStr) {
    if (dateStr != null) {
      dateStr = dateStr.trim();
    }
    DateTimeFormatter formatter;
    for (String dateFormat : this.metadataDateFormats) {
      try {
        formatter = new DateTimeFormatterBuilder().appendPattern(dateFormat).parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
        LocalDate date = LocalDate.parse(dateStr, formatter);
        return date.getMonthValue();
      } catch (DateTimeParseException e) {
        // nothing to do, will try next format
      }
    }
    return null;
  }

  public Integer getDay(String dateStr) {
    if (dateStr != null) {
      dateStr = dateStr.trim();
    }
    DateTimeFormatter formatter;
    for (String dateFormat : this.metadataDateFormats) {
      try {
        formatter = new DateTimeFormatterBuilder().appendPattern(dateFormat).toFormatter();
        LocalDate date = LocalDate.parse(dateStr, formatter);
        return date.getDayOfMonth();
      } catch (DateTimeParseException e) {
        // nothing to do, will try next format
      }
    }
    return null;
  }

  private void fillPages(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    Map<String, Object> pages = new HashMap<>();
    this.putIfNotNull(pages, "paging", depositDocAdapter.getPaging(depositDoc));
    this.putIfNotNull(pages, "other", depositDocAdapter.getPagingOther(depositDoc));
    if (!pages.isEmpty()) {
      properties.put("pages", pages);
    }
  }

  private void fillClassifications(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> classifications = new ArrayList<>();
    for (MetadataClassification metadataClassification : depositDocAdapter.getClassifications(depositDoc)) {
      Map<String, Object> classification = new HashMap<>();
      this.putIfNotNull(classification, "code", metadataClassification.getCode());
      this.putIfNotNull(classification, "item", metadataClassification.getItem());
      if (!classification.isEmpty()) {
        classifications.add(classification);
      }
    }
    if (!classifications.isEmpty()) {
      properties.put("classifications", classifications);
    }
  }

  private void fillCollections(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> collections = new ArrayList<>();
    for (MetadataCollection metadataCollection : depositDocAdapter.getCollections(depositDoc)) {
      Map<String, Object> collection = new HashMap<>();
      this.putIfNotNull(collection, "name", metadataCollection.getName());
      this.putIfNotNull(collection, "exactName", metadataCollection.getName());
      this.putIfNotNull(collection, "number", metadataCollection.getNumber());
      if (!collection.isEmpty()) {
        collections.add(collection);
      }
    }
    if (!collections.isEmpty()) {
      properties.put("collections", collections);
    }
  }

  private void fillFundings(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> fundings = new ArrayList<>();
    for (MetadataFunding metadataFunding : depositDocAdapter.getFundings(depositDoc)) {
      Map<String, Object> funding = new HashMap<>();
      this.putIfNotNull(funding, "name", metadataFunding.getName());
      this.putIfNotNull(funding, "code", metadataFunding.getCode());
      this.putIfNotNull(funding, "funder", metadataFunding.getFunder());
      this.putIfNotNull(funding, INDEX_ACRONYM_FIELD, metadataFunding.getAcronym());
      if (!funding.isEmpty()) {
        fundings.add(funding);
      }
    }
    if (!fundings.isEmpty()) {
      properties.put("fundings", fundings);
    }
  }

  private void fillStructures(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> structures = new ArrayList<>();
    List<List<String>> structuresWithParentsIds = new ArrayList<>();
    List<List<String>> structuresWithParentsNames = new ArrayList<>();
    for (Structure structure : depositDocAdapter.getStructures(depositDoc)) {
      Map<String, Object> structureMap = new HashMap<>();
      this.putIfNotNull(structureMap, "name", structure.getName());
      this.putIfNotNull(structureMap, "code", structure.getCodeStruct());
      this.putIfNotNull(structureMap, "cnStruct", structure.getCnStructC());
      this.putIfNotNull(structureMap, INDEX_ACRONYM_FIELD, structure.getAcronym());
      this.putIfNotNull(structureMap, INDEX_RES_ID_FIELD, structure.getResId());
      if (!structureMap.isEmpty()) {
        structures.add(structureMap);
      }

      // fill parent structures ids
      List<String> parentStructuresIds = new ArrayList<>();
      if (!StringTool.isNullOrEmpty(structure.getResId())) {
        parentStructuresIds = new ArrayList<>(this.remoteStructureService.getParentStructureIds(structure.getResId()));
        parentStructuresIds.add(0, structure.getResId());
      }
      structuresWithParentsIds.add(parentStructuresIds);

      // fill parent structures names
      List<String> parentStructuresNames = new ArrayList<>();
      if (!StringTool.isNullOrEmpty(structure.getResId())) {
        parentStructuresNames = new ArrayList<>(this.remoteStructureService.getParentStructureNames(structure.getResId()));
      }
      parentStructuresNames.add(0, structure.getName());
      structuresWithParentsNames.add(parentStructuresNames);
    }
    if (!structures.isEmpty()) {
      properties.put("structures", structures);
    }
    if (!structuresWithParentsIds.isEmpty()) {
      properties.put("structuresWithParentsIds", structuresWithParentsIds);
      Set<String> topStructuresIds = new HashSet<>();
      structuresWithParentsIds.forEach(ids -> {
        if (!ids.isEmpty()) {
          topStructuresIds.add(ids.get(ids.size() - 1));
        }
      });
      properties.put("topStructuresIds", topStructuresIds);
    }
    if (!structuresWithParentsNames.isEmpty()) {
      properties.put("structuresWithParentsNames", structuresWithParentsNames);
      Set<String> topStructuresNames = new HashSet<>();
      structuresWithParentsNames.forEach(names -> topStructuresNames.add(names.get(names.size() - 1)));
      properties.put("topStructuresNames", topStructuresNames);
    }
  }

  private void fillResearchGroups(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> researchGroups = new ArrayList<>();
    for (ResearchGroup researchGroup : depositDocAdapter.getResearchGroups(depositDoc)) {
      Map<String, Object> researchGroupMap = new HashMap<>();
      this.putIfNotNull(researchGroupMap, "name", researchGroup.getName());
      this.putIfNotNull(researchGroupMap, "code", researchGroup.getCode());
      this.putIfNotNull(researchGroupMap, INDEX_ACRONYM_FIELD, researchGroup.getAcronym());
      this.putIfNotNull(researchGroupMap, INDEX_RES_ID_FIELD, researchGroup.getResId());
      if (!researchGroupMap.isEmpty()) {
        researchGroups.add(researchGroupMap);
      }
    }
    if (!researchGroups.isEmpty()) {
      properties.put("researchGroups", researchGroups);
    }
  }

  private void fillContainer(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    Map<String, Object> containerMap = new HashMap<>();
    this.putIfNotNull(containerMap, "title", depositDocAdapter.getContainerTitle(depositDoc));
    this.putIfNotNull(containerMap, "volume", depositDocAdapter.getContainerVolume(depositDoc));
    this.putIfNotNull(containerMap, "issue", depositDocAdapter.getContainerIssue(depositDoc));
    this.putIfNotNull(containerMap, "specialIssue", depositDocAdapter.getContainerSpecialIssue(depositDoc));
    this.putIfNotNull(containerMap, "editor", depositDocAdapter.getContainerEditor(depositDoc));
    this.putIfNotNull(containerMap, "conferencePlace", depositDocAdapter.getContainerConferencePlace(depositDoc));
    this.putIfNotNull(containerMap, "conferenceDate", depositDocAdapter.getContainerConferenceDate(depositDoc));
    this.putIfNotNull(containerMap, "conferenceSubtitle", depositDocAdapter.getContainerConferenceSubtitle(depositDoc));
    if (!containerMap.isEmpty()) {
      properties.put("container", containerMap);
    }
  }

  private void fillLinks(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> links = new ArrayList<>();
    for (MetadataLink metadataLink : depositDocAdapter.getLinks(depositDoc)) {
      Map<String, Object> link = new HashMap<>();
      this.putIfNotNull(link, "type", metadataLink.getType());
      this.putIfNotNull(link, "target", metadataLink.getTarget());
      if (!link.isEmpty()) {
        links.add(link);
      }
    }
    if (!links.isEmpty()) {
      properties.put("links", links);
    }
  }

  private void fillCorrections(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> corrections = new ArrayList<>();
    for (MetadataCorrection metadataCorrection : depositDocAdapter.getCorrections(depositDoc)) {
      Map<String, Object> correction = new HashMap<>();
      this.putIfNotNull(correction, "note", metadataCorrection.getNote());
      this.putIfNotNull(correction, "doi", metadataCorrection.getDoi());
      this.putIfNotNull(correction, "pmid", metadataCorrection.getPmid());
      if (!correction.isEmpty()) {
        corrections.add(correction);
      }
    }
    if (!corrections.isEmpty()) {
      properties.put("corrections", corrections);
    }
  }

  private void fillPrincipalFiles(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<MetadataFile> principalFiles = this.getPrincipalFiles(depositDoc, depositDocAdapter);
    List<Map<String, Object>> files = new ArrayList<>();
    for (MetadataFile metadataFile : principalFiles) {
      Map<String, Object> fileMap = this.getFileMap(metadataFile);
      this.putIfNotNull(fileMap, TYPE_LEVEL, DocumentFileType.FileTypeLevel.PRINCIPAL.name());
      files.add(fileMap);
    }
    if (!files.isEmpty()) {
      properties.put("principalFiles", files);
    }
  }

  private void fillSecondaryCorrectiveFiles(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<MetadataFile> principalFiles = this.getSecondaryAndCorrectiveFiles(depositDoc, depositDocAdapter);
    List<Map<String, Object>> files = new ArrayList<>();
    for (MetadataFile metadataFile : principalFiles) {
      Map<String, Object> fileMap = this.getFileMap(metadataFile);
      files.add(fileMap);
    }
    if (!files.isEmpty()) {
      properties.put("secondaryFiles", files);
    }
  }

  private List<MetadataFile> getPrincipalFiles(Object depositDoc, DepositDocAdapter depositDocAdapter) {
    List<MetadataFile> files = new ArrayList<>();
    for (MetadataFile metadataFile : depositDocAdapter.getFiles(depositDoc)) {
      this.fillFileTypeLevel(depositDoc, depositDocAdapter, metadataFile);
      if (DocumentFileType.FileTypeLevel.PRINCIPAL.name().equals(metadataFile.getTypeLevel()) || this.isLegacyFileType(metadataFile.getType())) {
        files.add(metadataFile);
      }
    }
    return files;
  }

  private List<MetadataFile> getSecondaryAndCorrectiveFiles(Object depositDoc, DepositDocAdapter depositDocAdapter) {
    List<MetadataFile> files = new ArrayList<>();
    for (MetadataFile metadataFile : depositDocAdapter.getFiles(depositDoc)) {
      this.fillFileTypeLevel(depositDoc, depositDocAdapter, metadataFile);
      if (DocumentFileType.FileTypeLevel.SECONDARY.name().equals(metadataFile.getTypeLevel())
              || DocumentFileType.FileTypeLevel.CORRECTIVE.name().equals(metadataFile.getTypeLevel())) {
        files.add(metadataFile);
      }
    }
    return files;
  }

  private void fillFiles(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<Map<String, Object>> files = new ArrayList<>();
    for (MetadataFile metadataFile : depositDocAdapter.getFiles(depositDoc)) {
      if (metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)) {
        this.fillThumbnail(metadataFile, properties);
      } else if (!this.isFileTypeModeDiffusionOrAgreement(metadataFile.getType())) {
        Map<String, Object> fileMap = this.getFileMap(metadataFile);
        this.fillFileTypeLevel(depositDoc, depositDocAdapter, metadataFile, fileMap);

        if (!fileMap.isEmpty()) {
          files.add(fileMap);
        }
      }
    }
    if (!files.isEmpty()) {
      properties.put("files", files);
    }
  }

  private void fillFileTypeLevel(Object depositDoc, DepositDocAdapter depositDocAdapter, MetadataFile metadataFile) {
    if (StringTool.isNullOrEmpty(metadataFile.getTypeLevel()) && !metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)) {
      String publicationSubtypeId = this.getPublicationSubtypeIdByName(depositDocAdapter.getSubtype(depositDoc));
      String documentFileTypeId = this.getDocumentFileTypeIdByName(metadataFile.getType());
      Optional<PublicationSubtypeDocumentFileTypeDTO> psdtOpt = this.getPublicationSubtypeDocumentFileTypeDTO(publicationSubtypeId,
              documentFileTypeId);
      if (psdtOpt.isPresent()) {
        metadataFile.setTypeLevel(psdtOpt.get().getLevel().name());
      }
    }
  }

  private void fillFileTypeLevel(Object depositDoc, DepositDocAdapter depositDocAdapter, MetadataFile metadataFile,
          Map<String, Object> fileMap) {
    String publicationSubtypeName = depositDocAdapter.getSubtype(depositDoc);
    String publicationSubtypeId = this.getPublicationSubtypeIdByName(publicationSubtypeName);
    String documentFileTypeId = this.getDocumentFileTypeIdByName(metadataFile.getType());
    Optional<PublicationSubtypeDocumentFileTypeDTO> psdtOpt = this.getPublicationSubtypeDocumentFileTypeDTO(publicationSubtypeId,
            documentFileTypeId);
    if (psdtOpt.isPresent()) {
      this.putIfNotNull(fileMap, TYPE_LEVEL, psdtOpt.get().getLevel().name());
    } else if (this.isLegacyFileType(metadataFile.getType())) {
      // Old document file types have no level for the publication subtype --> force the level to PRINCIPAL.
      // This is the case for old document files types "Article", "Book", "Book chapter" and "Proceedings chapter".
      // Forcing a value prevents them to disappear from public details page in UI.
      this.putIfNotNull(fileMap, TYPE_LEVEL, DocumentFileType.FileTypeLevel.PRINCIPAL.name());
    } else {
      throw new IllegalStateException(
              "No FileTypeLevel found for value " + metadataFile.getType() + " for publication subtype " + publicationSubtypeName);
    }
  }

  private boolean isLegacyFileType(String fileType) {
    return fileType.equals(DOCUMENT_FILE_TYPE_ARTICLE)
            || fileType.equals(DOCUMENT_FILE_TYPE_BOOK)
            || fileType.equals(DOCUMENT_FILE_TYPE_BOOK_CHAPTER)
            || fileType.equals(DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER);
  }

  private Optional<PublicationSubtypeDocumentFileTypeDTO> getPublicationSubtypeDocumentFileTypeDTO(String publicationSubtypeId,
          String documentFileTypeId) {
    List<PublicationSubtypeDocumentFileTypeDTO> pubSubDocTypes = this.remotePublicationSubtypeService
            .findAllPublicationSubtypeDocumentFileTypesDTO(
                    publicationSubtypeId, PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(SORT_VALUE)))
            .getData();
    return pubSubDocTypes.stream()
            .filter(psdt -> psdt.getDocumentFileTypeId().equals(documentFileTypeId) && psdt.getPublicationSubtypeId()
                    .equals(publicationSubtypeId))
            .findFirst();
  }

  private String getPublicationSubtypeIdByName(String publicationSubtypeName) {
    List<PublicationSubtypeDTO> publicationSubtypes = this.remotePublicationSubtypeService.findAllDTO(
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(SORT_VALUE))).getData();

    Optional<PublicationSubtypeDTO> publicationSubtypeOpt = publicationSubtypes.stream()
            .filter(ps -> ps.getName().equals(publicationSubtypeName)).findFirst();
    if (publicationSubtypeOpt.isPresent()) {
      return publicationSubtypeOpt.get().getResId();
    } else {
      throw new SolidifyRuntimeException("PublicationSubytpe with name '" + publicationSubtypeName + "' not found");
    }
  }

  private String getDocumentFileTypeIdByName(String fileTypeName) {
    List<DocumentFileType> documentFileTypes = this.remoteDocumentFileTypeService.findAllWithCache(
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by("value"))).getData();

    Optional<DocumentFileType> documentFileTypeOpt = documentFileTypes.stream().filter(dft -> dft.getValue().equals(fileTypeName)).findFirst();
    if (documentFileTypeOpt.isPresent()) {
      return documentFileTypeOpt.get().getResId();
    } else {
      throw new SolidifyRuntimeException("DocumentFileType with name '" + fileTypeName + "' not found");
    }
  }

  private void fillThumbnail(MetadataFile metadataFile, Map<String, Object> properties) {
    Map<String, Object> thumbnailMap = this.getFileMap(metadataFile);
    if (!thumbnailMap.isEmpty()) {
      properties.put("thumbnail", thumbnailMap);
    }
  }

  private Map<String, Object> getFileMap(MetadataFile metadataFile) {
    Map<String, Object> file = new HashMap<>();
    this.putIfNotNull(file, INDEX_RES_ID_FIELD, metadataFile.getResId());
    this.putIfNotNull(file, "type", metadataFile.getType());
    this.putIfNotNull(file, "name", metadataFile.getName());
    this.putIfNotNull(file, "size", metadataFile.getSize());
    this.putIfNotNull(file, "checksum", metadataFile.getChecksum());
    this.putIfNotNull(file, "mimeType", metadataFile.getMimeType());
    this.putIfNotNull(file, "label", metadataFile.getLabel());

    if (!StringTool.isNullOrEmpty(metadataFile.getLicense())) {
      License license = this.remoteLicenseService.findByOpenLicenseId(metadataFile.getLicense());
      this.putIfNotNull(file, "license", metadataFile.getLicense());
      if (license.getUrl() != null) {
        this.putIfNotNull(file, "licenseUrl", license.getUrl().toString());
      }
      this.putIfNotNull(file, "licenseTitle", license.getTitle());
    }

    DocumentFile.AccessLevel currentAccessLevel = DocumentFile.AccessLevel.fromMetadataValue(metadataFile.getAccessLevel());
    boolean isUnderEmbargo = false;

    MetadataEmbargo embargo = metadataFile.getEmbargo();
    if (embargo != null) {
      this.putIfNotNull(file, "finalAccessLevel", DocumentFile.AccessLevel.fromMetadataValue(metadataFile.getAccessLevel()).name());
      this.putIfNotNull(file, "embargoAccessLevel", DocumentFile.AccessLevel.fromMetadataValue(embargo.getAccessLevel()).name());
      this.putIfNotNull(file, "embargoEndDate", embargo.getEndDate());

      // if embargo is not over, check current access level
      if (embargo.getEndDate().isAfter(LocalDate.now())) {
        currentAccessLevel = DocumentFile.AccessLevel.fromMetadataValue(embargo.getAccessLevel());
        isUnderEmbargo = true;
      }
    }

    this.putIfNotNull(file, "accessLevel", currentAccessLevel.name());
    this.putIfNotNull(file, "isUnderEmbargo", isUnderEmbargo);

    return file;
  }

  /*****************************************************************************************************/

  private void fillFulltextVersions(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    String publicationSubtypeId = this.getPublicationSubtypeIdByName(depositDocAdapter.getSubtype(depositDoc));

    Set<String> fulltextVersions = new HashSet<>();

    boolean hasPrincipalFile = false;
    for (MetadataFile metadataFile : depositDocAdapter.getFiles(depositDoc)) {
      // Get fulltext version
      String fulltextVersion = this.getFulltextVersion(metadataFile);
      if (!StringTool.isNullOrEmpty(fulltextVersion)) {
        fulltextVersions.add(fulltextVersion);
      }

      // Check if file is principal
      if (!hasPrincipalFile && this.isPrincipalFile(publicationSubtypeId, metadataFile)) {
        hasPrincipalFile = true;
        break;
      }
    }

    if (!hasPrincipalFile) {
      // If there is no PRINCIPAL file, add a 'noFulltext' value in index
      fulltextVersions.add(AouConstants.NO_FULLTEXT_TEXT);
    }

    this.putIfNotNull(properties, "fulltextVersions", fulltextVersions);
  }

  private String getFulltextVersion(MetadataFile metadataFile) {
    String type = metadataFile.getType();
    if (type.contains(AouConstants.DOCUMENT_PUBLISHED_VERSION_EN)) {
      return AouConstants.PUBLISHED_VERSION_TEXT;
    } else if (type.contains(AouConstants.DOCUMENT_ACCEPTED_VERSION_EN)) {
      return AouConstants.ACCEPTED_VERSION_TEXT;
    } else if (type.contains(AouConstants.DOCUMENT_SUBMITTED_VERSION_EN)) {
      return AouConstants.SUBMITTED_VERSION_TEXT;
    } else {
      return switch (type) {
        case AouConstants.DOCUMENT_FILE_TYPE_ARTICLE, AouConstants.DOCUMENT_FILE_TYPE_BOOK, AouConstants.DOCUMENT_FILE_TYPE_BOOK_CHAPTER,
                AouConstants.DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER -> AouConstants.UNKNOWN_TEXT;
        case AouConstants.DOCUMENT_FILE_TYPE_EXTRACT -> AouConstants.EXTRACT_TEXT;
        case AouConstants.DOCUMENT_FILE_TYPE_RECORDING -> AouConstants.RECORDING_TEXT;
        default -> null;
      };
    }
  }

  /**
   * Return true if the file is of type PRINCIPAL for the publication subtype. It returns also true for old document file types imported from
   * Fedora.
   *
   * @param publicationSubtypeId
   * @param metadataFile
   * @return
   */
  private boolean isPrincipalFile(String publicationSubtypeId, MetadataFile metadataFile) {
    if (metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)) {
      return false;
    }
    String documentFileTypeId = this.getDocumentFileTypeIdByName(metadataFile.getType());
    Optional<PublicationSubtypeDocumentFileTypeDTO> psdtOpt = this.getPublicationSubtypeDocumentFileTypeDTO(publicationSubtypeId,
            documentFileTypeId);
    return ((psdtOpt.isPresent() && psdtOpt.get().getLevel() == DocumentFileType.FileTypeLevel.PRINCIPAL)
            || metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_ARTICLE)
            || metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_BOOK)
            || metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_BOOK_CHAPTER)
            || metadataFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER));
  }

  private void fillEmbargoEndDates(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {

    Set<LocalDate> embargoEndDates = new HashSet<>();

    for (MetadataFile metadataFile : depositDocAdapter.getFiles(depositDoc)) {
      if (metadataFile.getEmbargo() != null) {
        embargoEndDates.add(metadataFile.getEmbargo().getEndDate());
      }
    }

    this.putIfNotNull(properties, AouConstants.INDEX_FIELD_EMBARGO_END_DATES, new ArrayList<>(embargoEndDates));
  }

  private void fillLicenses(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    Set<String> licenses = new HashSet<>();
    for (MetadataFile metadataFile : depositDocAdapter.getFiles(depositDoc)) {
      if (!StringTool.isNullOrEmpty(metadataFile.getLicense())) {
        licenses.add(metadataFile.getLicense());
      }
    }
    this.putIfNotNull(properties, "licenses", licenses);
  }

  private void fillCurrentDiffusion(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    String nullVersionKey = "__null__";
    Map<String, List<String>> accessLevelByTypes = new HashMap<>();

    // Discard files that must not be considered to compute access level
    List<MetadataFile> files = depositDocAdapter.getFiles(depositDoc).stream().filter(f ->
            !f.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE)
                    && !f.getType().equals(DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE)).collect(Collectors.toList());

    // compute a Map with a list of access levels for each type of file
    for (MetadataFile metadataFile : files) {
      String versionText = this.getFulltextVersion(metadataFile);
      if (StringTool.isNullOrEmpty(versionText)) {
        versionText = nullVersionKey;
      }
      String currentAccessLevel = this.getCurrentDiffusion(metadataFile);
      if (!StringTool.isNullOrEmpty(currentAccessLevel)) {
        accessLevelByTypes.computeIfAbsent(versionText, x -> new ArrayList<>()).add(currentAccessLevel);
      }
    }

    // according to this map, determine the global access level of the deposit

    // If we have published, accepted or submitted files, we use them to determine the access level
    Set<String> accessLevelsForPublishedAcceptedSubmittedTypes = new HashSet<>();
    accessLevelByTypes.entrySet().stream()
            .filter(m -> m.getKey().equals(AouConstants.PUBLISHED_VERSION_TEXT)
                    || m.getKey().equals(AouConstants.ACCEPTED_VERSION_TEXT)
                    || m.getKey().equals(AouConstants.SUBMITTED_VERSION_TEXT))
            .forEach(m -> accessLevelsForPublishedAcceptedSubmittedTypes.addAll(m.getValue()));
    String diffusion = this.getMoreOpenAccessLevel(accessLevelsForPublishedAcceptedSubmittedTypes);

    // Then, if diffusion not determined yet, if we have unknown or extract files, we use them to determine the access level
    if (StringTool.isNullOrEmpty(diffusion)) {
      Set<String> accessLevelsForUnknownDraftExtractTypes = new HashSet<>();
      accessLevelByTypes.entrySet().stream()
              .filter(m -> m.getKey().equals(AouConstants.UNKNOWN_TEXT)
                      || m.getKey().equals(AouConstants.EXTRACT_TEXT)
                      || m.getKey().equals(AouConstants.RECORDING_TEXT))
              .forEach(m -> accessLevelsForUnknownDraftExtractTypes.addAll(m.getValue()));
      diffusion = this.getMoreOpenAccessLevel(accessLevelsForUnknownDraftExtractTypes);
    }

    // Finally we take access level from any other files found (example: for Working Paper that do not have any fulltext version)
    if (StringTool.isNullOrEmpty(diffusion)) {
      Set<String> accessLevelsForNullTypes = new HashSet<>();
      accessLevelByTypes.entrySet().stream()
              .filter(m -> m.getKey().equals(nullVersionKey))
              .forEach(m -> accessLevelsForNullTypes.addAll(m.getValue()));
      diffusion = this.getMoreOpenAccessLevel(accessLevelsForNullTypes);
    }

    this.putIfNotNull(properties, AouConstants.INDEX_FIELD_DIFFUSION, diffusion);
    this.putIfNotNull(properties, "accessLevelByTypes", accessLevelByTypes);
  }

  private void fillPrincipalFilesCurrentAccessLevel(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    String principalFileAccessLevel;
    List<MetadataFile> principalFiles = this.getPrincipalFiles(depositDoc, depositDocAdapter);
    if (principalFiles.stream().anyMatch(mf -> mf.getCurrentAccessLevel().equals(DocumentFile.AccessLevel.PUBLIC.metadataValue()))) {
      principalFileAccessLevel = DocumentFile.AccessLevel.PUBLIC.metadataValue();
    } else if (principalFiles.stream().anyMatch(mf -> mf.getCurrentAccessLevel().equals(DocumentFile.AccessLevel.RESTRICTED.metadataValue()))) {
      principalFileAccessLevel = DocumentFile.AccessLevel.RESTRICTED.metadataValue();
    } else if (principalFiles.stream().anyMatch(mf -> mf.getCurrentAccessLevel().equals(DocumentFile.AccessLevel.PRIVATE.metadataValue()))) {
      principalFileAccessLevel = DocumentFile.AccessLevel.PRIVATE.metadataValue();
    } else {
      principalFileAccessLevel = AouConstants.NO_FULLTEXT_TEXT;
    }
    this.putIfNotNull(properties, AouConstants.INDEX_FIELD_PRINCIPAL_FILE_ACCESS_LEVEL, principalFileAccessLevel);
  }

  private void fillOpenAccess(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    String openAccessValue;
    List<MetadataFile> principalFiles = this.getPrincipalFiles(depositDoc, depositDocAdapter);
    // Remove Preprints and Submitted version from principal files used to compute Open Access value
    List<MetadataFile> cleanedPrincipalFiles = principalFiles.stream()
            .filter(mf -> !mf.getType().equals(DOCUMENT_FILE_TYPE_PREPRINT) && Arrays.stream(AouConstants.getSubmittedVersionDocumentFileTypes())
                    .noneMatch(type -> type.equals(mf.getType())))
            .toList();

    // Any file with current access public ?
    if (cleanedPrincipalFiles.stream().anyMatch(mf -> mf.getCurrentAccessLevel().equals(DocumentFile.AccessLevel.PUBLIC.metadataValue()))) {
      openAccessValue = OPEN_ACCESS_TEXT;
    } else if (cleanedPrincipalFiles.stream().anyMatch(mf -> mf.getEmbargo() != null && mf.getEmbargoEndDate().isAfter(LocalDate.now())
            && mf.getAccessLevel().equals(DocumentFile.AccessLevel.PUBLIC.metadataValue()))) {
      openAccessValue = EMBARGOED_ACCESS_TEXT;
    } else {
      openAccessValue = NOT_OPEN_ACCESS_TEXT;
    }
    this.putIfNotNull(properties, AouConstants.INDEX_FIELD_OPEN_ACCESS, openAccessValue);
  }

  private String getMoreOpenAccessLevel(Set<String> accessLevels) {
    if (accessLevels.contains(DocumentFile.AccessLevel.PUBLIC.metadataValue())) {
      return AouConstants.OPEN_ACCESS_TEXT;
    } else if (accessLevels.contains(AouConstants.EMBARGOED_ACCESS_TEXT)) {
      return AouConstants.EMBARGOED_ACCESS_TEXT;
    } else if (accessLevels.contains(DocumentFile.AccessLevel.RESTRICTED.metadataValue())) {
      return AouConstants.RESTRICTED_ACCESS_TEXT;
    } else if (accessLevels.contains(DocumentFile.AccessLevel.PRIVATE.metadataValue())) {
      return AouConstants.CLOSED_ACCESS_TEXT;
    }
    return null;
  }

  private String getCurrentDiffusion(MetadataFile metadataFile) {
    if (metadataFile.getEmbargo() != null && metadataFile.getEmbargo().getEndDate().isAfter(LocalDate.now())) {
      return AouConstants.EMBARGOED_ACCESS_TEXT;
    } else {
      return metadataFile.getAccessLevel();
    }
  }

  private OffsetDateTime getFirstValidationDateTime(List<StatusHistory> statusHistories) {
    for (StatusHistory statusHistory : statusHistories) {
      if (statusHistory.getStatus().equals(Publication.PublicationStatus.COMPLETED.toString())) {
        return statusHistory.getChangeTime();
      }
    }
    return null;
  }

  private String getFirstValidatorName(List<StatusHistory> statusHistories) {
    for (StatusHistory statusHistory : statusHistories) {
      if (statusHistory.getStatus().equals(Publication.PublicationStatus.COMPLETED.toString())) {
        return statusHistory.getCreatorName();
      }
    }
    return null;
  }

  /*****************************************************************************************************/

  private void fillFulltexts(String publicationId, Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    List<String> fulltexts = new ArrayList<>();
    for (MetadataFile metadataFile : depositDocAdapter.getFiles(depositDoc)) {
      String currentAccessLevelText = this.getCurrentDiffusion(metadataFile);
      if (!currentAccessLevelText.equals(AouConstants.EMBARGOED_ACCESS_TEXT)
              && !this.isFileTypeModeDiffusionOrAgreement(metadataFile.getType())
              && DocumentFile.AccessLevel.fromMetadataValue(currentAccessLevelText) == DocumentFile.AccessLevel.PUBLIC) {
        try {
          // Index only fulltext of files that are currently public
          String fulltext = null;
          if (!StringTool.isNullOrEmpty(metadataFile.getResId())) {
            try {
              fulltext = this.remoteDocumentFileService.fulltext(metadataFile.getResId());
            } catch (SolidifyResourceNotFoundException e) {
              // file may not exist in the case of update: if the publication is in edition and a file has been removed,
              // it doesn't exist anymore in database. In such a case, the fulltext of the still published file is not reindexed.
              log.warn("Fulltext cannot be extracted for file {} as file could not be found.", metadataFile.getResId());
            }
          } else {
            // previous metadata version didn't contain resId
            Map<String, String> queryParams = new HashMap<>();
            queryParams.put("size", metadataFile.getSize().toString());
            queryParams.put("fileName", metadataFile.getName());
            List<DocumentFile> documentFiles = this.remoteDocumentFileService.listByPublication(publicationId, queryParams);
            if (documentFiles.size() == 1) {
              fulltext = this.remoteDocumentFileService.fulltext(documentFiles.get(0).getResId());
            }
          }
          if (!StringTool.isNullOrEmpty(fulltext)) {
            fulltexts.add(fulltext);
            if (fulltexts.size() == MAX_INDEXED_FULLTEXTS) {
              // Do not index more fulltexts in order to prevent having a too big index entry
              // Note: in previous Aou version, we used to index only one file
              log.info("Maximum of indexed fulltexts reached for publication {}", publicationId);
              break;
            }
          }
        } catch (SolidifyRestException e) {
          if (e.getCause() != null) {
            log.warn("Unable to retrieve fulltext to index for publication {}: {}", publicationId, e.getCause().getMessage());
          } else {
            log.warn("Unable to retrieve fulltext to index for publication {}", publicationId, e);
          }
        }
      }
    }
    if (!fulltexts.isEmpty()) {
      properties.put(INDEX_FIELD_FULLTEXTS, fulltexts);
    }
  }

  private boolean isFileTypeModeDiffusionOrAgreement(String type) {
    return type.equals(DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE) || type.equals(DOCUMENT_FILE_TYPE_AGREEMENT_VALUE);
  }

  private void putIfNotNull(Map<String, Object> map, String propertyName, Object value) {
    if (value != null && !StringTool.isNullOrEmpty(value.toString())) {
      map.put(propertyName, value);
    }
  }

  /*****************************************************************************************************/

  private void fillHtmlMetadataHeaders(Object depositDoc, DepositDocAdapter depositDocAdapter, Map<String, Object> properties) {
    Map<String, Object> htmlHeadersMap = new HashMap<>();

    this.fillCitationTitle(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationAuthor(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationPublicationDate(htmlHeadersMap, depositDoc, depositDocAdapter);

    // Useful for Google Scholar
    this.fillCitationJournalTitle(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationConferenceTitle(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationISSN(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationISBN(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationVolume(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationIssue(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationFirstPage(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationLastPage(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationDissertationInstitution(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationTechnicalReportInstitution(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationTechnicalReportNumber(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationPdfUrls(htmlHeadersMap, depositDoc, depositDocAdapter);

    // Useful for Zotero
    this.fillCitationInbookTitle(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationSeriesTitle(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationPublisher(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationAbstract(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationDOI(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationPMID(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationPublicUrl(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationLanguage(htmlHeadersMap, depositDoc, depositDocAdapter);

    // Other
    this.fillCitationBookTitle(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationOtherMetaDates(htmlHeadersMap);
    this.fillCitationAuthors(htmlHeadersMap);
    this.fillCitationYear(htmlHeadersMap);
    this.fillCitationPMCID(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationOnlineDate(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationKeywords(htmlHeadersMap, depositDoc, depositDocAdapter);
    this.fillCitationType(htmlHeadersMap, depositDoc, depositDocAdapter);

    properties.put(AouConstants.INDEX_FIELD_HTML_META_HEADERS, htmlHeadersMap);
  }

  private void fillCitationTitle(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    this.putIfNotNull(htmlHeadersMap, "citation_title", depositDocAdapter.getTitleContent(depositDoc));
  }

  private void fillCitationAuthor(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    List<String> authors = new ArrayList<>();
    for (AbstractContributor abstractContributor : depositDocAdapter.getContributors(depositDoc)) {
      if (abstractContributor instanceof ContributorDTO && AouConstants.CONTRIBUTOR_ROLE_AUTHOR.equals(
              ((ContributorDTO) abstractContributor).getRole())) {
        authors.add(((ContributorDTO) abstractContributor).getFullName());
      } else if (abstractContributor instanceof CollaborationDTO) {
        authors.add(((CollaborationDTO) abstractContributor).getName());
      }
    }
    if (!authors.isEmpty()) {
      this.putIfNotNull(htmlHeadersMap, CITATION_AUTHOR, authors);
    }
  }

  private void fillCitationPublicationDate(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {

    // Get date by priority on the type
    DateType dateTypeToIndex = null;
    if (depositDocAdapter.getDates(depositDoc).stream()
            .anyMatch(d -> DateType.PUBLICATION.metadataValue().equals(d.getType()) && !StringTool.isNullOrEmpty(d.getContent()))) {
      dateTypeToIndex = DateType.PUBLICATION;
    } else if (depositDocAdapter.getDates(depositDoc).stream()
            .anyMatch(d -> DateType.IMPRIMATUR.metadataValue().equals(d.getType()) && !StringTool.isNullOrEmpty(d.getContent()))) {
      dateTypeToIndex = DateType.IMPRIMATUR;
    } else if (depositDocAdapter.getDates(depositDoc).stream()
            .anyMatch(d -> DateType.FIRST_ONLINE.metadataValue().equals(d.getType()) && !StringTool.isNullOrEmpty(d.getContent()))) {
      dateTypeToIndex = DateType.FIRST_ONLINE;
    } else if (depositDocAdapter.getDates(depositDoc).stream()
            .anyMatch(d -> DateType.DEFENSE.metadataValue().equals(d.getType()) && !StringTool.isNullOrEmpty(d.getContent()))) {
      dateTypeToIndex = DateType.DEFENSE;
    }

    if (dateTypeToIndex != null) {
      final DateType filterType = dateTypeToIndex;
      Optional<MetadataDates> metadataDateOpt = depositDocAdapter.getDates(depositDoc).stream()
              .filter(d -> filterType.metadataValue().equals(d.getType())).findFirst();
      if (metadataDateOpt.isPresent()) {
        MetadataDates metadataDate = metadataDateOpt.get();
        String dateToIndex = this.getMetaDate(metadataDate);
        if (!StringTool.isNullOrEmpty(dateToIndex)) {
          this.putIfNotNull(htmlHeadersMap, CITATION_PUBLICATION_DATE, dateToIndex);
        }
      }
    }
  }

  private String getMetaDate(MetadataDates metadataDate) {
    String dateStr = metadataDate.getContent().trim();
    String dateToIndex;
    try {
      // Try to use full date
      DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern("yyyy-MM-dd").toFormatter();
      LocalDate date = LocalDate.parse(dateStr, formatter);
      DateTimeFormatter slashFormatter = new DateTimeFormatterBuilder().appendPattern("yyyy/MM/dd").toFormatter();
      dateToIndex = slashFormatter.format(date);
    } catch (DateTimeParseException e) {
      // If metadata date is not full, extract year only
      dateToIndex = this.getYear(dateStr).toString();
    }
    return dateToIndex;
  }

  private void fillCitationJournalTitle(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter)) {
      String containerTitle = depositDocAdapter.getContainerTitle(depositDoc);
      if (!StringTool.isNullOrEmpty(containerTitle)) {
        this.putIfNotNull(htmlHeadersMap, "citation_journal_title", containerTitle);
      }
    }
  }

  private void fillCitationConferenceTitle(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isChapitreActesOrPresentationOrPoster(depositDoc, depositDocAdapter)) {
      String containerTitle = depositDocAdapter.getContainerTitle(depositDoc);
      if (!StringTool.isNullOrEmpty(containerTitle)) {
        this.putIfNotNull(htmlHeadersMap, "citation_conference_title", containerTitle);
      }
    }
  }

  private void fillCitationISSN(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter)) {
      String issn = depositDocAdapter.getISSN(depositDoc);
      if (!StringTool.isNullOrEmpty(issn)) {
        this.putIfNotNull(htmlHeadersMap, "citation_issn", issn);
      }
    }
  }

  private void fillCitationISBN(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isLivreOrOuvrageCollectifOrActesConference(depositDoc, depositDocAdapter)
            || this.isChapitreLivreOrContributionDict(depositDoc, depositDocAdapter)
            || this.isChapitreActesOrPresentationOrPoster(depositDoc, depositDocAdapter)) {
      String isbn = depositDocAdapter.getISBN(depositDoc);
      if (!StringTool.isNullOrEmpty(isbn)) {
        this.putIfNotNull(htmlHeadersMap, "citation_isbn", isbn);
      }
    }
  }

  private void fillCitationVolume(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter) || this.isChapitreLivreOrContributionDict(depositDoc, depositDocAdapter)) {
      String containerVolume = depositDocAdapter.getContainerVolume(depositDoc);
      if (!StringTool.isNullOrEmpty(containerVolume)) {
        this.putIfNotNull(htmlHeadersMap, "citation_volume", containerVolume);
      }
    }
  }

  private void fillCitationIssue(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter)) {
      String issue = depositDocAdapter.getContainerIssue(depositDoc);
      if (!StringTool.isNullOrEmpty(issue)) {
        this.putIfNotNull(htmlHeadersMap, "citation_issue", issue);
      }
    }
  }

  private void fillCitationFirstPage(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter)
            || this.isChapitreLivreOrContributionDict(depositDoc, depositDocAdapter)
            || this.isChapitreActesOrPresentationOrPoster(depositDoc, depositDocAdapter)) {
      String firstPage = null;
      String other = depositDocAdapter.getPagingOther(depositDoc);
      if (!StringTool.isNullOrEmpty(other)) {
        firstPage = other;
      } else {
        String paging = depositDocAdapter.getPaging(depositDoc);
        if (!StringTool.isNullOrEmpty(paging)) {
          firstPage = paging;
        }
      }

      this.putIfNotNull(htmlHeadersMap, "citation_firstpage", firstPage);
    }
  }

  private void fillCitationLastPage(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (AouConstants.DEPOSIT_TYPE_RAPPORT_NAME.equals(depositDocAdapter.getType(depositDoc))
            || AouConstants.DEPOSIT_TYPE_DIPLOME_NAME.equals(depositDocAdapter.getType(depositDoc))
            || this.isLivreOrOuvrageCollectifOrActesConference(depositDoc, depositDocAdapter)) {
      String paging = depositDocAdapter.getPaging(depositDoc);
      if (!StringTool.isNullOrEmpty(paging)) {
        this.putIfNotNull(htmlHeadersMap, "citation_lastpage", paging);
      }
    }
  }

  private void fillCitationDissertationInstitution(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter
          depositDocAdapter) {
    if (AouConstants.DEPOSIT_TYPE_DIPLOME_NAME.equals(depositDocAdapter.getType(depositDoc))) {
      this.putIfNotNull(htmlHeadersMap, "citation_dissertation_institution", AouConstants.UNIVERSITY_OF_GENEVA_FR);
    }
  }

  private void fillCitationTechnicalReportInstitution(Map<String, Object> htmlHeadersMap, Object depositDoc,
          DepositDocAdapter depositDocAdapter) {
    if (AouConstants.DEPOSIT_TYPE_RAPPORT_NAME.equals(depositDocAdapter.getType(depositDoc))) {
      this.putIfNotNull(htmlHeadersMap, "citation_technical_report_institution", AouConstants.UNIVERSITY_OF_GENEVA_FR);
    }
  }

  private void fillCitationTechnicalReportNumber(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (AouConstants.DEPOSIT_TYPE_RAPPORT_NAME.equals(depositDocAdapter.getType(depositDoc))) {
      this.putIfNotNull(htmlHeadersMap, "citation_technical_report_number", depositDocAdapter.getLocalNumber(depositDoc));
    }
  }

  private void fillCitationPdfUrls(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {

    List<MetadataFile> files = depositDocAdapter.getFiles(depositDoc);
    List<MetadataFile> publicFiles = files.stream().filter(f -> !f.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_VIGNETTE)
            && DocumentFile.AccessLevel.fromMetadataValue(f.getAccessLevel()) == DocumentFile.AccessLevel.PUBLIC
            && !this.getCurrentDiffusion(f).equals(AouConstants.EMBARGOED_ACCESS_TEXT)
    ).collect(Collectors.toList());

    List<String> pdfUrls = new ArrayList<>();
    for (MetadataFile file : publicFiles) {
      if (!StringTool.isNullOrEmpty(file.getResId())) {
        String downloadUrl = this.downloadFileUrlDownloadTemplate.replace("{documentFileId}", file.getResId());
        pdfUrls.add(downloadUrl);
      }
    }

    if (!pdfUrls.isEmpty()) {
      this.putIfNotNull(htmlHeadersMap, "citation_pdf_url", pdfUrls);
    }
  }

  private void fillCitationInbookTitle(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isChapitreLivreOrContributionDict(depositDoc, depositDocAdapter)) {
      String containerTitle = depositDocAdapter.getContainerTitle(depositDoc);
      if (!StringTool.isNullOrEmpty(containerTitle)) {
        this.putIfNotNull(htmlHeadersMap, "citation_inbook_title", containerTitle);
      }
    }
  }

  private void fillCitationSeriesTitle(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter)
            || this.isLivreOrOuvrageCollectifOrActesConference(depositDoc, depositDocAdapter)
            || this.isChapitreLivreOrContributionDict(depositDoc, depositDocAdapter)
            || this.isChapitreActesOrPresentationOrPoster(depositDoc, depositDocAdapter)) {
      List<MetadataCollection> metadataCollections = depositDocAdapter.getCollections(depositDoc);
      List<String> collections = new ArrayList<>();
      for (MetadataCollection metadataCollection : metadataCollections) {
        collections.add(metadataCollection.getName());
      }
      if (!collections.isEmpty()) {
        this.putIfNotNull(htmlHeadersMap, "citation_series_title", collections);
      }
    }
  }

  private void fillCitationPublisher(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (AouConstants.DEPOSIT_TYPE_RAPPORT_NAME.equals(depositDocAdapter.getType(depositDoc))
            || this.isLivreOrOuvrageCollectifOrActesConference(depositDoc, depositDocAdapter)
            || this.isChapitreLivreOrContributionDict(depositDoc, depositDocAdapter)
            || this.isChapitreActesOrPresentationOrPoster(depositDoc, depositDocAdapter)) {
      String publisherName = depositDocAdapter.getPublisherName(depositDoc);
      if (!StringTool.isNullOrEmpty(publisherName)) {
        this.putIfNotNull(htmlHeadersMap, "citation_publisher", publisherName);
      }
    } else if (AouConstants.DEPOSIT_TYPE_DIPLOME_NAME.equals(depositDocAdapter.getType(depositDoc))) {
      this.putIfNotNull(htmlHeadersMap, "citation_publisher", AouConstants.UNIVERSITY_OF_GENEVA_FR);
    }
  }

  private void fillCitationAbstract(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    List<MetadataTextLang> abstracts = depositDocAdapter.getAbstracts(depositDoc);
    List<String> abstractValues = new ArrayList<>();
    for (MetadataTextLang text : abstracts) {
      if (!StringTool.isNullOrEmpty(text.getContent())) {
        abstractValues.add(text.getContent());
      }
    }
    if (!abstractValues.isEmpty()) {
      this.putIfNotNull(htmlHeadersMap, "citation_abstract", abstractValues);
    }
  }

  private void fillCitationDOI(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    String doi = depositDocAdapter.getDOI(depositDoc);
    if (!StringTool.isNullOrEmpty(doi)) {
      this.putIfNotNull(htmlHeadersMap, "citation_doi", doi);
    }
  }

  private void fillCitationPMID(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter)) {
      BigInteger pmid = depositDocAdapter.getPmid(depositDoc);
      if (pmid != null) {
        this.putIfNotNull(htmlHeadersMap, "citation_pmid", pmid.toString());
      }
    }
  }

  private void fillCitationPublicUrl(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    String url = depositDocAdapter.getPublisherVersionUrl(depositDoc);
    if (!StringTool.isNullOrEmpty(url)) {
      this.putIfNotNull(htmlHeadersMap, "citation_public_url", url);
    }
  }

  private void fillCitationLanguage(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    List<String> languages = depositDocAdapter.getLanguages(depositDoc);
    if (!languages.isEmpty()) {
      this.putIfNotNull(htmlHeadersMap, "citation_language", languages);
    }
  }

  private void fillCitationBookTitle(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isChapitreLivreOrContributionDict(depositDoc, depositDocAdapter)) {
      String containerTitle = depositDocAdapter.getContainerTitle(depositDoc);
      if (!StringTool.isNullOrEmpty(containerTitle)) {
        this.putIfNotNull(htmlHeadersMap, "citation_book_title", containerTitle);
      }
    }
  }

  private void fillCitationOtherMetaDates(Map<String, Object> htmlHeadersMap) {
    if (htmlHeadersMap.containsKey(CITATION_PUBLICATION_DATE)) {
      htmlHeadersMap.put("citation_cover_date", htmlHeadersMap.get(CITATION_PUBLICATION_DATE));
      htmlHeadersMap.put("citation_date", htmlHeadersMap.get(CITATION_PUBLICATION_DATE));
    }
  }

  private void fillCitationAuthors(Map<String, Object> htmlHeadersMap) {
    if (htmlHeadersMap.containsKey(CITATION_AUTHOR)) {
      List<String> authors = (List<String>) htmlHeadersMap.get(CITATION_AUTHOR);
      htmlHeadersMap.put("citation_authors", String.join("; ", authors));
    }
  }

  private void fillCitationYear(Map<String, Object> htmlHeadersMap) {
    if (htmlHeadersMap.containsKey(CITATION_PUBLICATION_DATE)) {
      String publicationDate = (String) htmlHeadersMap.get(CITATION_PUBLICATION_DATE);
      String year;
      if (publicationDate.contains("/")) {
        year = publicationDate.substring(0, 4);
      } else {
        year = publicationDate;
      }
      this.putIfNotNull(htmlHeadersMap, "citation_year", year);
    }
  }

  private void fillCitationPMCID(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    if (this.isArticleOrJournal(depositDoc, depositDocAdapter)) {
      String pmcid = depositDocAdapter.getPmcId(depositDoc);
      if (!StringTool.isNullOrEmpty(pmcid)) {
        this.putIfNotNull(htmlHeadersMap, "citation_pmcid", pmcid);
      }
    }
  }

  private void fillCitationOnlineDate(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    Optional<MetadataDates> onlineDateOpt = depositDocAdapter.getDates(depositDoc).stream()
            .filter(d -> DateType.FIRST_ONLINE.metadataValue().equals(d.getType()) && !StringTool.isNullOrEmpty(d.getContent())).findFirst();
    if (onlineDateOpt.isPresent()) {
      MetadataDates onlineDate = onlineDateOpt.get();
      String dateToIndex = this.getMetaDate(onlineDate);
      if (!StringTool.isNullOrEmpty(dateToIndex)) {
        this.putIfNotNull(htmlHeadersMap, "citation_online_date", dateToIndex);
      }
    }
  }

  private void fillCitationKeywords(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    List<String> keywords = depositDocAdapter.getKeywords(depositDoc);
    if (!keywords.isEmpty()) {
      this.putIfNotNull(htmlHeadersMap, "citation_keywords", String.join("; ", keywords));
    }
  }

  private void fillCitationType(Map<String, Object> htmlHeadersMap, Object depositDoc, DepositDocAdapter depositDocAdapter) {
    this.putIfNotNull(htmlHeadersMap, "citation_type", depositDocAdapter.getSubtype(depositDoc));
  }

  private boolean isArticleOrJournal(Object depositDoc, DepositDocAdapter depositDocAdapter) {
    return (AouConstants.DEPOSIT_TYPE_ARTICLE_NAME.equals(depositDocAdapter.getType(depositDoc))
            || AouConstants.DEPOSIT_TYPE_JOURNAL_NAME.equals(depositDocAdapter.getType(depositDoc)));
  }

  private boolean isChapitreActesOrPresentationOrPoster(Object depositDoc, DepositDocAdapter depositDocAdapter) {
    return (AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME.equals(depositDocAdapter.getSubtype(depositDoc))
            || AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME.equals(depositDocAdapter.getSubtype(depositDoc))
            || AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME.equals(depositDocAdapter.getSubtype(depositDoc)));
  }

  private boolean isLivreOrOuvrageCollectifOrActesConference(Object depositDoc, DepositDocAdapter depositDocAdapter) {
    return (AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME.equals(depositDocAdapter.getSubtype(depositDoc))
            || AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME.equals(depositDocAdapter.getSubtype(depositDoc))
            || AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME.equals(depositDocAdapter.getSubtype(depositDoc)));
  }

  private boolean isChapitreLivreOrContributionDict(Object depositDoc, DepositDocAdapter depositDocAdapter) {
    return (AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME.equals(depositDocAdapter.getSubtype(depositDoc))
            || AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME.equals(depositDocAdapter.getSubtype(depositDoc)));
  }

  /*****************************************************************************************************/

  @Override
  public List<Class<?>> getOaiXmlClasses() {
    return Arrays.asList(
            ch.unige.aou.model.xml.aou.v1.RepositoryInfo.class,
            ch.unige.aou.model.xml.deposit.v2_4.DepositDoc.class);
  }

  @Override
  public Object getOAIMetadata(String metadataInString) {
    return this.getDepositDoc(metadataInString);
  }

  @Override
  public Marshaller getMarshaller(JAXBContext jaxbContext) throws JAXBException {
    final Marshaller marshaller = jaxbContext.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    marshaller.setProperty(AouConstants.XML_NAMESPACE_PREFIX_MAPPER, new AouXmlNamespacePrefixMapperV2_4());
    return marshaller;
  }

  @Override
  public Map<String, String> getXsltParameters(OAIRecord oaiRecord) {
    final Map<String, String> params = OAIMetadataService.super.getXsltParameters(oaiRecord);

    PublicationIndexEntry indexEntry = (PublicationIndexEntry) oaiRecord;
    if (indexEntry.getDiffusion() != null) {
      params.put(AouConstants.INDEX_FIELD_DIFFUSION, indexEntry.getDiffusion());
    }
    if (indexEntry.getMetadata(AouConstants.INDEX_FIELD_EMBARGO_END_DATES) != null) {
      List<String> embargoEndDates = (List<String>) indexEntry.getMetadata(AouConstants.INDEX_FIELD_EMBARGO_END_DATES);
      if (!embargoEndDates.isEmpty()) {
        params.put("embargoEndDate", embargoEndDates.get(0));
      }
    }

    return params;
  }
}
