/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AouService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.logging.LogLevel;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.AouMetadataVersion;

public abstract class AouService {

  private static final Logger log = LoggerFactory.getLogger(AouService.class);

  protected static final String LOG_OBJECT_WITH_STATUS_TEMPLATE = "{} '{}' ({}) (status: {}): {}";

  protected MessageService messageService;

  public AouService(MessageService messageService) {
    this.messageService = messageService;
  }

  protected void logObjectWithStatusMessage(LogLevel logLevel, String className, String resId, String status, String name, String message,
          Exception... exceptions) {
    switch (logLevel) {
      case TRACE:
        log.trace(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
        break;
      case DEBUG:
        log.debug(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
        break;
      case WARN:
        log.warn(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
        break;
      case ERROR:
        Exception e = (exceptions.length > 0) ? exceptions[0] : null;
        log.error(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message, e);
        break;
      case INFO:
      default:
        log.info(LOG_OBJECT_WITH_STATUS_TEMPLATE, className, resId, name, status, message);
        break;
    }
  }

  public ClassPathResource getResourceSchema(AouMetadataVersion version) {
    ClassPathResource xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + version.getDepositMetadataSchema());
    return xsd;
  }
}
