/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - UserRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.security.UserDisplayableInfos;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public abstract class UserRemoteResourceService extends AdminRemoteResourceService<User> {

  private static final Logger log = LoggerFactory.getLogger(UserRemoteResourceService.class);

  public UserRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  public User getAuthenticatedUser() {
    String url = this.adminModuleUrl + "/" + ResourceName.USERS + "/" + AouActionName.AUTHENTICATED;
    return this.restClientService.getResource(url, User.class);
  }

  public UserDisplayableInfos getUserDisplayableInfos(String externalUid) {
    String url = UriComponentsBuilder.fromUriString(
            this.adminModuleUrl + "/" + ResourceName.USERS + "/" + AouActionName.GET_USER_DISPLAYABLE_INFOS + "/" + externalUid).toUriString();
    return this.restClientService.getResource(url, UserDisplayableInfos.class);
  }

  @Cacheable(AouCacheNames.USER_BY_EXTERNAL_UID)
  public User findByExternalUid(String externalUid) {
    try {
      final List<User> users = this.getUsersByExternalUid(externalUid);

      if (users.size() == 1) {
        return users.get(0);
      } else if (users.size() > 1) {
        throw new SolidifyRuntimeException("more than one user correspond to the externalUid");
      }

    } catch (final Exception e) {
      log.error("Error finding external UID " + externalUid, e);
    }

    return null;
  }

  public List<User> getUsersByExternalUid(String externalUid) {
    final Pageable page = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    // Build URL
    String url = this.adminModuleUrl + "/" + ResourceName.USERS;
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.set("externalUid", externalUid);
    uriBuilder.queryParams(params);
    url = uriBuilder.toUriString();
    // Get User RestCollection
    RestCollection<User> userCollection = this.restClientService.getResourceList(url, User.class, page);
    // Return user list
    return userCollection.getData();
  }

  public boolean isUserConnectedMemberOfAuthorizedInstitution() {
    return this.restClientService.getResource(this.getResourceUrl() + "/" + AouActionName.IS_USER_MEMBER_OF_AUTHORIZED_INSTITUTION, Boolean.class);
  }

  @Override
  protected Class<User> getResourceClass() {
    return User.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.USERS;
  }
}
