/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - PublicationRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import java.nio.file.Path;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.exception.PublicationNotFoundException;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationUserRole;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;

public abstract class PublicationRemoteResourceService extends AdminRemoteResourceService<Publication> {

  protected PublicationRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  @Override
  public Publication findOne(String resId) {
    try {
      return super.findOne(resId);
    } catch (SolidifyResourceNotFoundException resourceNotFoundException) {
      throw new PublicationNotFoundException(resourceNotFoundException.getMessage());
    }
  }

  public List<StatusHistory> history(String resId) {
    final Pageable page = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(AouConstants.CHANGE_TIME));
    // Build URL
    String url = this.getResourceUrl() + "/" + resId + "/" + ResourceName.HISTORY;
    RestCollection<StatusHistory> statusHistoryRestCollection = this.restClientService.getResourceList(url, StatusHistory.class, page);
    return statusHistoryRestCollection.getData();
  }

  public boolean isCreatorOrContributorOrValidator(String publicationId) {
    List<PublicationUserRole> userRoles = this.listPublicationUserRoles(publicationId);
    return (userRoles.contains(PublicationUserRole.CREATOR)
            || userRoles.contains(PublicationUserRole.CONTRIBUTOR)
            || userRoles.contains(PublicationUserRole.VALIDATOR));
  }

  public List<PublicationUserRole> listPublicationUserRoles(String publicationId) {
    String userRolesStr = this.restClientService.getResource(
            this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.LIST_PUBLICATION_USER_ROLES, String.class);

    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.readValue(userRolesStr, new TypeReference<List<PublicationUserRole>>() {
      });
    } catch (Exception e) {
      throw new SolidifyRuntimeException("Unable to read list of PublicationUserRole");
    }
  }

  public void downloadDocumentFile(String publicationId, String documentFileId, Path destinationPath) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + ResourceName.DOCUMENT_FILES + "/" + documentFileId + "/"
            + AouActionName.DOWNLOAD;
    this.restClientService.downloadContent(url, destinationPath, TokenUsage.WITH_TOKEN);
  }

  public void downloadThumbnail(String publicationId, Path destinationPath) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.DOWNLOAD_THUMBNAIL;
    this.restClientService.downloadContent(url, destinationPath, TokenUsage.WITHOUT_TOKEN);
  }

  @Override
  protected Class<Publication> getResourceClass() {
    return Publication.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.PUBLICATIONS;
  }
}
