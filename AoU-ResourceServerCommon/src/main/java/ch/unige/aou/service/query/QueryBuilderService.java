/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - QueryBuilderService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.query;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.rest.SearchOperator;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.model.search.StoredSearch;
import ch.unige.aou.model.search.StoredSearchCriteria;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.service.rest.trusted.TrustedIndexPropertiesRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStoredSearchesRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStructureRemoteResourceService;

@Service
public class QueryBuilderService {

  private final String publishedPublicationsIndexName;
  private final List<String> hiddenNonPublicDocumentSubtypes;
  private final List<String> acceptedIndexFieldNamesWithoutAlias = new ArrayList<>();
  private final List<String> fieldAliasesWithAndOperator = new ArrayList<>();
  private final FieldsRequest defaultFieldsRequest;

  private final TrustedIndexPropertiesRemoteResourceService trustedIndexFieldAliasRemoteResourceService;
  private final TrustedStoredSearchesRemoteResourceService trustedStoredSearchesRemoteResourceService;
  private final TrustedStructureRemoteResourceService trustedStructureRemoteResourceService;

  public QueryBuilderService(TrustedIndexPropertiesRemoteResourceService trustedIndexFieldAliasRemoteResourceService,
          TrustedStoredSearchesRemoteResourceService trustedStoredSearchesRemoteResourceService,
          TrustedStructureRemoteResourceService trustedStructureRemoteResourceService,
          AouProperties aouProperties) {
    this.trustedIndexFieldAliasRemoteResourceService = trustedIndexFieldAliasRemoteResourceService;
    this.trustedStoredSearchesRemoteResourceService = trustedStoredSearchesRemoteResourceService;
    this.trustedStructureRemoteResourceService = trustedStructureRemoteResourceService;

    this.publishedPublicationsIndexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.hiddenNonPublicDocumentSubtypes = List.of(aouProperties.getParameters().getHiddenNonPublicDocumentSubtypes());
    this.acceptedIndexFieldNamesWithoutAlias.addAll(Arrays.asList(aouProperties.getIndexing().getAcceptedIndexFieldNamesWithoutAlias()));
    this.fieldAliasesWithAndOperator.addAll(Arrays.asList(aouProperties.getIndexing().getFieldAliasesWithAndOperator()));

    this.defaultFieldsRequest = aouProperties.getIndexing().getDefaultFieldsRequest();
  }

  public List<FacetRequest> getFacetsList() {
    return this.trustedIndexFieldAliasRemoteResourceService.getFacetRequests(this.publishedPublicationsIndexName);
  }

  public List<FacetRequest> getFacetsList(String facetNamesStr) {
    List<IndexFieldAlias> indexFieldAliases = this.trustedIndexFieldAliasRemoteResourceService.getIndexFieldAliases(
            this.publishedPublicationsIndexName);
    List<FacetRequest> facetRequests = new ArrayList<>();
    String[] facetNames = facetNamesStr.split(",");
    for (String facetName : facetNames) {
      final String cleanFacetName = facetName.trim();
      String facetField = cleanFacetName;

      // eventually replace index alias by real field name
      Optional<IndexFieldAlias> indexFieldAliasOpt = indexFieldAliases.stream().filter(f -> f.getAlias().equals(cleanFacetName)).findFirst();
      if (indexFieldAliasOpt.isPresent()) {
        facetField = indexFieldAliasOpt.get().getField();
      }

      FacetRequest facetRequest = new FacetRequest();
      facetRequest.setName(cleanFacetName);
      facetRequest.setField(facetField);
      facetRequest.setLimit(30);
      facetRequests.add(facetRequest);
    }
    return facetRequests;
  }

  /**
   * Create a copy of default FieldsRequest to prevent excludes and includes lists from being modified after the FieldsRequest has been returned
   *
   * @return
   */
  public FieldsRequest getDefaultFieldsRequest() {
    return this.defaultFieldsRequest.clone();
  }

  public List<AouSearchCondition> getStoredSearchConditions(String storedResearchId) {
    StoredSearch storedSearch = this.trustedStoredSearchesRemoteResourceService.findOneWithCache(storedResearchId);
    List<StoredSearchCriteria> criteriaList = storedSearch.getCriteria();
    List<AouSearchCondition> aouSearchConditions = new ArrayList<>();
    for (StoredSearchCriteria criteria : criteriaList) {
      aouSearchConditions.add(criteria.toAouSearchCondition());
    }
    this.setSearchConditionsOperatorToAND(aouSearchConditions);
    if (!storedSearch.getWithRestrictedAccessMasters()) {
      // discard non public masters
      List<SearchCondition> searchConditions = this.addDiscardNonPublicMastersCondition(
              aouSearchConditions.stream().map(SearchCondition.class::cast).collect(Collectors.toList()));

      List<AouSearchCondition> aouSearchConditionsWithoutMasters = new ArrayList<>();
      for (SearchCondition searchCondition : searchConditions) {
        aouSearchConditionsWithoutMasters.add(AouSearchCondition.fromSearchCondition(searchCondition));
      }
      return aouSearchConditionsWithoutMasters;
    }
    return aouSearchConditions;
  }

  public List<SearchCondition> completeSearchConditions(List<AouSearchCondition> searchConditions) {
    this.setSearchConditionsOperatorToAND(searchConditions);
    return this.replaceAliasesByIndexField(searchConditions);
  }

  public void setSearchConditionsOperatorToAND(List<AouSearchCondition> searchConditions) {
    searchConditions.stream().filter(f -> this.fieldAliasesWithAndOperator.contains(f.getField())).toList()
            .forEach(c -> c.setSearchOperator(SearchOperator.AND));
  }

  public List<SearchCondition> replaceAliasesByIndexField(List<AouSearchCondition> searchConditions) {
    List<IndexFieldAlias> indexFieldAliases = this.trustedIndexFieldAliasRemoteResourceService.getIndexFieldAliases(
            this.publishedPublicationsIndexName);

    for (AouSearchCondition searchCondition : searchConditions) {
      if (searchCondition.getType() == SearchConditionType.NESTED_BOOLEAN) {
        List<AouSearchCondition> nestedSearchConditions = searchCondition.getNestedConditions().stream().map(AouSearchCondition.class::cast)
                .collect(Collectors.toList());
        this.replaceAliasesByIndexField(nestedSearchConditions);
      } else {
        Optional<IndexFieldAlias> indexFieldAliasOpt = indexFieldAliases.stream().filter(f -> f.getAlias().equals(searchCondition.getField()))
                .findFirst();
        if (indexFieldAliasOpt.isPresent()) {
          searchCondition.setField(indexFieldAliasOpt.get().getField());
        } else {
          // has it already been replaced ?
          indexFieldAliasOpt = indexFieldAliases.stream().filter(f -> f.getField().equals(searchCondition.getField())).findFirst();
          if (!indexFieldAliasOpt.isPresent() && !this.acceptedIndexFieldNamesWithoutAlias.contains(searchCondition.getField())) {
            throw new SolidifyRuntimeException("Index field with alias '" + searchCondition.getField() + "' not found");
          }
        }
      }
    }
    return searchConditions.stream().map(SearchCondition.class::cast).collect(Collectors.toList());
  }

  public AouSearchCondition getFindByArchiveIdCondition(String archiveId) {
    AouSearchCondition aouSearchCondition = new AouSearchCondition();
    aouSearchCondition.setType(SearchConditionType.TERM);
    aouSearchCondition.setField(AouConstants.INDEX_FIELD_ARCHIVE_ID);
    aouSearchCondition.setValue(archiveId);
    return aouSearchCondition;
  }

  public AouSearchCondition getConditionsFromLastIndexationDate(Long searchFrom) {
    AouSearchCondition lastIndexationDateCondition = new AouSearchCondition();
    lastIndexationDateCondition.setType(SearchConditionType.RANGE);
    lastIndexationDateCondition.setBooleanClauseType(BooleanClauseType.MUST);
    lastIndexationDateCondition.setField(AouConstants.INDEX_FIELD_ALIAS_LAST_INDEXATION_DATE);
    LocalDateTime minDate = LocalDateTime.now().minusDays(searchFrom);
    lastIndexationDateCondition.setLowerValue(minDate.toString());
    return lastIndexationDateCondition;
  }

  public AouSearchCondition getFindByCnIndividuWhereIsContributorCondition(String cnInvididu) {
    AouSearchCondition aouSearchCondition = new AouSearchCondition();
    aouSearchCondition.setType(SearchConditionType.TERM);
    aouSearchCondition.setField(AouConstants.INDEX_FIELD_CONTRIBUTORS_CN_INDIVIDU);
    aouSearchCondition.setValue(cnInvididu);
    return aouSearchCondition;
  }

  public AouSearchCondition getFindByCnIndividuWhereIsNotDirectorCondition(String cnInvididu) {
    AouSearchCondition aouSearchCondition = new AouSearchCondition();
    aouSearchCondition.setType(SearchConditionType.TERM);
    aouSearchCondition.setField(AouConstants.INDEX_FIELD_UNIGE_DIRECTORS);
    aouSearchCondition.setValue(cnInvididu);
    aouSearchCondition.setBooleanClauseType(BooleanClauseType.MUST_NOT);
    return aouSearchCondition;
  }

  public AouSearchCondition getFindByCnIndividuWhereContributorButNotDirectorCondition(String cnInvididu) {
    List<AouSearchCondition> nestedSearchConditions = new ArrayList<>();
    nestedSearchConditions.add(this.getFindByCnIndividuWhereIsContributorCondition(cnInvididu));
    nestedSearchConditions.add(this.getFindByCnIndividuWhereIsNotDirectorCondition(cnInvididu));
    return this.combineConditionsInSingleCondition(nestedSearchConditions);
  }

  public List<SearchCondition> addDiscardNonPublicMastersCondition(List<SearchCondition> searchConditions) {
    SearchCondition discardCondition = this.getDiscardNonPublicMastersCondition();

    List<SearchCondition> searchConditionsWithoutNonPublicMasters = new ArrayList<>();

    if (!searchConditions.isEmpty()) {
      SearchCondition requestedConditions = new SearchCondition();
      requestedConditions.setBooleanClauseType(BooleanClauseType.MUST);
      requestedConditions.setType(SearchConditionType.NESTED_BOOLEAN);
      requestedConditions.getNestedConditions().addAll(searchConditions);
      searchConditionsWithoutNonPublicMasters.add(requestedConditions);
    }

    searchConditionsWithoutNonPublicMasters.add(discardCondition);
    return searchConditionsWithoutNonPublicMasters;
  }

  private SearchCondition getDiscardNonPublicMastersCondition() {
    // Subtypes that must be hidden when non public
    SearchCondition masterCondition = new SearchCondition();
    masterCondition.setBooleanClauseType(BooleanClauseType.MUST);
    masterCondition.setType(SearchConditionType.TERM);
    masterCondition.setField(AouConstants.INDEX_FIELD_SUBTYPE);
    masterCondition.getTerms().addAll(this.hiddenNonPublicDocumentSubtypes);

    // Non public access levels
    SearchCondition diffusionCondition = new SearchCondition();
    diffusionCondition.setBooleanClauseType(BooleanClauseType.MUST);
    diffusionCondition.setType(SearchConditionType.TERM);
    diffusionCondition.setField(AouConstants.INDEX_FIELD_DIFFUSION);
    diffusionCondition.getTerms().add(AouConstants.EMBARGOED_ACCESS_TEXT);
    diffusionCondition.getTerms().add(AouConstants.RESTRICTED_ACCESS_TEXT);
    diffusionCondition.getTerms().add(AouConstants.CLOSED_ACCESS_TEXT);

    // Include both conditions in one nested to be used as MUST_NOT
    SearchCondition discardCondition = new SearchCondition();
    discardCondition.setBooleanClauseType(BooleanClauseType.MUST_NOT);
    discardCondition.setType(SearchConditionType.NESTED_BOOLEAN);
    discardCondition.getNestedConditions().add(masterCondition);
    discardCondition.getNestedConditions().add(diffusionCondition);
    return discardCondition;
  }

  public AouSearchCondition getSearchesOrPublicationsSearchCondition(String searchFrom, List<String> searchIds, List<String> publicationIds) {
    List<AouSearchCondition> nestedSearchConditions = this.getNestedSearchConditionsFromStoreSearchAndPublicationsIds(searchIds, publicationIds);
    if (!StringTool.isNullOrEmpty(searchFrom)) {
      nestedSearchConditions.add(this.getConditionsFromLastIndexationDate(Long.parseLong(searchFrom)));
    }
    return this.combineConditionsInSingleCondition(nestedSearchConditions);
  }

  public AouSearchCondition getSearchesOrPublicationsSearchCondition(List<String> searchIds, List<String> publicationIds,
          String combinationType, String cnIndividu) {
    List<AouSearchCondition> nestedSearchConditions = this.getNestedSearchConditionsFromStoreSearchAndPublicationsIds(searchIds,
            publicationIds, combinationType, cnIndividu);

    return this.combineConditionsInSingleCondition(nestedSearchConditions);
  }

  private AouSearchCondition combineConditionsInSingleCondition(List<AouSearchCondition> nestedSearchConditions) {
    // Combined conditions are returned in single container condition in order to be able to combine it with other ones (such a years condition
    // when exporting bibliography).
    AouSearchCondition mustCondition = new AouSearchCondition();
    mustCondition.setBooleanClauseType(BooleanClauseType.MUST);
    mustCondition.setType(SearchConditionType.NESTED_BOOLEAN);
    mustCondition.getNestedConditions().addAll(nestedSearchConditions);

    return mustCondition;
  }

  private List<AouSearchCondition> getNestedSearchConditionsFromStoreSearchAndPublicationsIds(List<String> searchIds,
          List<String> publicationIds) {
    return this.getNestedSearchConditionsFromStoreSearchAndPublicationsIds(searchIds, publicationIds, "or", null);
  }

  private List<AouSearchCondition> getNestedSearchConditionsFromStoreSearchAndPublicationsIds(List<String> searchIds,
          List<String> publicationIds,
          String combinationType, String cnIndividu) {
    List<AouSearchCondition> nestedSearchConditions = new ArrayList<>();

    BooleanClauseType booleanClauseType = BooleanClauseType.SHOULD;
    if (combinationType.equalsIgnoreCase("and")) {
      booleanClauseType = BooleanClauseType.FILTER;
    }

    for (String storedSearchId : searchIds) {
      AouSearchCondition aouSearchCondition = new AouSearchCondition();
      aouSearchCondition.setBooleanClauseType(booleanClauseType);
      aouSearchCondition.setType(SearchConditionType.NESTED_BOOLEAN);
      aouSearchCondition.getNestedConditions().addAll(this.getStoredSearchConditions(storedSearchId));
      nestedSearchConditions.add(aouSearchCondition);
    }

    for (String publicationId : publicationIds) {
      AouSearchCondition aouSearchCondition = new AouSearchCondition();
      aouSearchCondition.setType(SearchConditionType.TERM);
      aouSearchCondition.setBooleanClauseType(booleanClauseType);
      aouSearchCondition.setField(AouConstants.INDEX_FIELD_ID);
      aouSearchCondition.setValue(publicationId);
      nestedSearchConditions.add(aouSearchCondition);
    }

    if (!StringTool.isNullOrEmpty(cnIndividu)) {
      AouSearchCondition aouSearchCondition = new AouSearchCondition();
      aouSearchCondition.setType(SearchConditionType.TERM);
      aouSearchCondition.setBooleanClauseType(booleanClauseType);
      aouSearchCondition.setField(AouConstants.INDEX_FIELD_ALIAS_CN_INDIVIDU);
      aouSearchCondition.setValue(cnIndividu);
      nestedSearchConditions.add(aouSearchCondition);
    }
    return nestedSearchConditions;
  }

  public List<AouSearchCondition> buildSearchConditionsFromLegacyParameters(Map<String, Object> parameters) {
    List<AouSearchCondition> searchConditions = new ArrayList<>();

    // Search by author
    if (this.hasParameter(parameters, "cnIndividu")) {
      AouSearchCondition cnIndividuCondition = new AouSearchCondition();
      cnIndividuCondition.setType(SearchConditionType.TERM);
      cnIndividuCondition.setField(AouConstants.INDEX_FIELD_ALIAS_CN_INDIVIDU);
      cnIndividuCondition.setValue(parameters.get("cnIndividu").toString());
      searchConditions.add(cnIndividuCondition);
    }

    // Search by structure
    if (this.hasParameter(parameters, "cnStructC")) {
      String cnStructC = parameters.get("cnStructC").toString();
      boolean withSubStructures = false;
      if (cnStructC.endsWith("_")) {
        cnStructC = cnStructC.substring(0, cnStructC.length() - 1);
        withSubStructures = true;
      }

      Structure structure = this.trustedStructureRemoteResourceService.findByCnStructC(cnStructC);

      AouSearchCondition structureCondition = new AouSearchCondition();
      structureCondition.setType(SearchConditionType.TERM);
      if (withSubStructures) {
        structureCondition.setField("structuresWithParentsId");
      } else {
        structureCondition.setField("structureId");
      }
      structureCondition.setValue(structure.getResId());
      searchConditions.add(structureCondition);
    }

    // Search by research group
    if (this.hasParameter(parameters, "researchGroupId")) {
      AouSearchCondition cnIndividuCondition = new AouSearchCondition();
      cnIndividuCondition.setType(SearchConditionType.TERM);
      cnIndividuCondition.setField("researchGroupId");
      cnIndividuCondition.setValue(parameters.get("researchGroupId").toString());
      searchConditions.add(cnIndividuCondition);
    }

    // Search for specific publications subtypes
    AouSearchCondition subtypesConditions = this.getSubtypesConditions(parameters);
    if (subtypesConditions != null) {
      searchConditions.add(subtypesConditions);
    }

    // Search for specific collections
    if (this.hasParameter(parameters, "collection")) {
      String[] collectionNames = parameters.get("collection").toString().split(",");
      if (collectionNames.length > 0) {
        AouSearchCondition collectionCondition = new AouSearchCondition();
        collectionCondition.setType(SearchConditionType.TERM);
        collectionCondition.setField(AouConstants.INDEX_FIELD_COLLECTION_EXACT_NAME);
        collectionCondition.getTerms().addAll(List.of(collectionNames));
        searchConditions.add(collectionCondition);
      }
    }

    return searchConditions;
  }

  private AouSearchCondition getSubtypesConditions(Map<String, Object> parameters) {

    if (this.hasParameter(parameters, "subtypes")) {
      String[] subtypeCodes = parameters.get("subtypes").toString().split(",");
      List<String> subtypeCodesList = Arrays.asList(subtypeCodes);
      if (!subtypeCodesList.isEmpty()) {

        // some subtype code have evolved during time --> modify old ones to new ones
        subtypeCodesList = this.translateLegacySubtypeCodes(subtypeCodesList);

        // diploma code types to be treated as author or director
        List<String> diplomaSubtypes = List.of("D1a", "D2a", "D3a", "D1d", "D2d", "D3d");

        List<AouSearchCondition> orConditions = new ArrayList<>();

        // Case of simple subtypes
        List<String> simpleSubtypeCodes = subtypeCodesList.stream().filter(c -> !diplomaSubtypes.contains(c)).toList();
        if (!simpleSubtypeCodes.isEmpty()) {
          AouSearchCondition simpleSubtypeCondition = new AouSearchCondition();
          simpleSubtypeCondition.setBooleanClauseType(BooleanClauseType.SHOULD);
          simpleSubtypeCondition.setType(SearchConditionType.TERM);
          simpleSubtypeCondition.setField(AouConstants.INDEX_FIELD_SUBTYPE_CODE);
          simpleSubtypeCondition.getTerms().addAll(simpleSubtypeCodes);
          orConditions.add(simpleSubtypeCondition);
        }

        // Case of subtypes as author or director specifically
        if (this.hasParameter(parameters, "cnIndividu")) {
          List<String> roleSubtypeCodes = subtypeCodesList.stream().filter(c -> diplomaSubtypes.contains(c)).toList();
          if (!roleSubtypeCodes.isEmpty()) {
            for (String roleSubtypeCode : roleSubtypeCodes) {
              String subtypeCode = roleSubtypeCode.substring(0, roleSubtypeCode.length() - 1);

              AouSearchCondition subtypeWithRoleCondition = new AouSearchCondition();
              subtypeWithRoleCondition.setBooleanClauseType(BooleanClauseType.SHOULD);
              subtypeWithRoleCondition.setType(SearchConditionType.NESTED_BOOLEAN);

              AouSearchCondition subtypeCondition = new AouSearchCondition();
              subtypeCondition.setBooleanClauseType(BooleanClauseType.MUST);
              subtypeCondition.setType(SearchConditionType.TERM);
              subtypeCondition.setField(AouConstants.INDEX_FIELD_SUBTYPE_CODE);
              subtypeCondition.setValue(subtypeCode);
              subtypeWithRoleCondition.getNestedConditions().add(subtypeCondition);

              BooleanClauseType booleanClauseType;
              if (roleSubtypeCode.endsWith("d")) {
                // case of directors -> they must be in director
                booleanClauseType = BooleanClauseType.MUST;
              } else {
                // case of authors -> they must NOT be in directors
                booleanClauseType = BooleanClauseType.MUST_NOT;
              }

              AouSearchCondition directorCondition = new AouSearchCondition();
              directorCondition.setBooleanClauseType(booleanClauseType);
              directorCondition.setType(SearchConditionType.TERM);
              directorCondition.setField(AouConstants.INDEX_FIELD_ALIAS_UNIGE_DIRECTORS);
              directorCondition.setValue(parameters.get("cnIndividu").toString());
              subtypeWithRoleCondition.getNestedConditions().add(directorCondition);
              orConditions.add(subtypeWithRoleCondition);
            }
          }
        }

        // List of conditions grouped by a logical OR are returned in MUST condition to be grouped with other MUST conditions
        AouSearchCondition subtypesOrConditions = new AouSearchCondition();
        subtypesOrConditions.setBooleanClauseType(BooleanClauseType.MUST);
        subtypesOrConditions.setType(SearchConditionType.NESTED_BOOLEAN);
        subtypesOrConditions.getNestedConditions().addAll(orConditions);
        return subtypesOrConditions;
      }
    }

    return null;
  }

  private List<String> translateLegacySubtypeCodes(List<String> subtypeCodesList) {
    for (int i = 0; i < subtypeCodesList.size(); i++) {
      String code = subtypeCodesList.get(i);
      if (code.equals("L4")) {
        // Historically Chapitres d'Actes were moved from Livre to Conference and their code were updated from L4 to C4
        subtypeCodesList.set(i, "C4");
      }
      if (code.equals("A4")) {
        // Historically Preprint were moved from Article to Rapport and their code were updated from A4 to R4
        subtypeCodesList.set(i, "R4");
      }
    }
    return subtypeCodesList;
  }

  private boolean hasParameter(Map<String, Object> parameters, String name) {
    return parameters.containsKey(name) && parameters.get(name) != null && !StringTool.isNullOrEmpty(parameters.get(name).toString());
  }
}
