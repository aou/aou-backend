/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - DocumentFileRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import java.util.List;
import java.util.Map;

import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;

public abstract class DocumentFileRemoteResourceService extends RemoteResourceService<DocumentFile> {

  final protected String adminModuleUrl;

  protected DocumentFileRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService);

    this.adminModuleUrl = aouProperties.getModule().getAdmin().getUrl();
  }

  public String fulltext(String resId) {
    String url = this.getResourceUrl() + "/" + resId + "/" + AouActionName.FULLTEXT;
    return this.restClientService.getResource(url, String.class);
  }

  public List<DocumentFile> listByPublication(String publicationId, Map<String, String> queryParameters) {
    String url = this.adminModuleUrl + "/" + ResourceName.PUBLICATIONS + "/" + publicationId + "/" + ResourceName.DOCUMENT_FILES;
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    for (Map.Entry<String, String> queryParam : queryParameters.entrySet()) {
      uriBuilder.queryParam(queryParam.getKey(), queryParam.getValue());
    }
    url = uriBuilder.toUriString();
    return this.restClientService.getAllResources(url, DocumentFile.class);
  }

  @Override
  protected Class<DocumentFile> getResourceClass() {
    return DocumentFile.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.DOCUMENT_FILES;
  }
}
