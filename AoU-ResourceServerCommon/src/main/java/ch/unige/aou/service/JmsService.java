/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - JmsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.transaction.event.TransactionalEventListener;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.message.CleanIndexesMessage;
import ch.unige.aou.message.ContributorUpdateMessage;
import ch.unige.aou.message.EmailMessage;
import ch.unige.aou.message.PublicationDownloadMessage;
import ch.unige.aou.message.PublicationFromFedoraMessage;
import ch.unige.aou.message.PublicationIndexedMessage;
import ch.unige.aou.message.PublicationIndexingMessage;
import ch.unige.aou.message.PublicationMessage;
import ch.unige.aou.message.PublicationMetadataUpgradeMessage;
import ch.unige.aou.message.PublicationReindexingMessage;
import ch.unige.aou.message.PublicationUpdateMessage;

public abstract class JmsService extends AouService {
  private static final Logger log = LoggerFactory.getLogger(JmsService.class);

  @Autowired
  private JmsTemplate queueJmsTemplate;

  @Autowired
  @Qualifier("topicJmsTemplate")
  private JmsTemplate topicJmsTemplate;

  private String publicationsQueue;
  private String publicationsIndexingQueue;
  private String publicationsReindexingQueue;
  private String cleanIndexesQueue;
  private String publicationsMetadataUpgradeQueue;
  private String emailsQueue;
  private String publicationsFromFedoraQueue;
  private String publicationDownloadQueue;
  private final String cacheTopic;
  private final String publicationUpdateTopic;
  private final String publicationsIndexedTopic;
  private final String contributorsUpdatedTopic;

  private final JsonSerializerMessagePostCreator jsonSerializerMessagePostCreator;

  protected JmsService(AouProperties aouProperties, MessageService messageService) {
    super(messageService);
    this.jsonSerializerMessagePostCreator = new JsonSerializerMessagePostCreator();
    this.publicationsQueue = aouProperties.getQueue().getPublications();
    this.publicationsIndexingQueue = aouProperties.getQueue().getPublicationsIndexing();
    this.publicationsReindexingQueue = aouProperties.getQueue().getPublicationsReindexing();
    this.cleanIndexesQueue = aouProperties.getQueue().getCleanIndexes();
    this.publicationsMetadataUpgradeQueue = aouProperties.getQueue().getPublicationsMetadataUpgrade();
    this.emailsQueue = aouProperties.getQueue().getEmails();
    this.publicationsFromFedoraQueue = aouProperties.getQueue().getPublicationsFromFedora();
    this.cacheTopic = aouProperties.getTopic().getCache();
    this.publicationDownloadQueue = aouProperties.getQueue().getPublicationDownload();
    this.publicationUpdateTopic = aouProperties.getTopic().getPublicationsUpdate();
    this.publicationsIndexedTopic = aouProperties.getTopic().getPublicationsIndexed();
    this.contributorsUpdatedTopic = aouProperties.getTopic().getContributorsUpdated();
  }

  protected void convertAndSendToQueue(String queue, Object message) {
    this.convertAndSendToQueue(queue, message, false);
  }

  protected void convertAndSendToQueue(String queue, Object message, boolean withJsonSerialization) {
    if (log.isTraceEnabled()) {
      log.trace("Sending message to queue {} => {}", queue, message);
    }
    if (withJsonSerialization) {
      this.queueJmsTemplate.convertAndSend(queue, message, this.jsonSerializerMessagePostCreator);
    } else {
      this.queueJmsTemplate.convertAndSend(queue, message);
    }
  }

  protected void convertAndSendToTopic(String topic, Object message) {
    this.convertAndSendToTopic(topic, message, false);
  }

  protected void convertAndSendToTopic(String topic, Object message, boolean withJsonSerialization) {
    if (log.isTraceEnabled()) {
      log.trace("Sending message to topic {} => {}", topic, message);
    }
    if (withJsonSerialization) {
      this.topicJmsTemplate.convertAndSend(topic, message, this.jsonSerializerMessagePostCreator);
    } else {
      this.topicJmsTemplate.convertAndSend(topic, message);
    }
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationMessage publicationMessage) {
    this.convertAndSendToQueue(this.publicationsQueue, publicationMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationUpdateMessage publicationUpdateMessage) {
    this.convertAndSendToTopic(this.publicationUpdateTopic, publicationUpdateMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationMetadataUpgradeMessage publicationMetadataUpgradeMessage) {
    this.convertAndSendToQueue(this.publicationsMetadataUpgradeQueue, publicationMetadataUpgradeMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationIndexingMessage publicationIndexingMessage) {
    this.convertAndSendToQueue(this.publicationsIndexingQueue, publicationIndexingMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationReindexingMessage publicationReindexingMessage) {
    this.convertAndSendToQueue(this.publicationsReindexingQueue, publicationReindexingMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationIndexedMessage publicationIndexedMessage) {
    this.convertAndSendToTopic(this.publicationsIndexedTopic, publicationIndexedMessage, true);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(ContributorUpdateMessage contributorUpdatedMessage) {
    this.convertAndSendToTopic(this.contributorsUpdatedTopic, contributorUpdatedMessage, true);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(CleanIndexesMessage cleanIndexesMessage) {
    this.convertAndSendToQueue(this.cleanIndexesQueue, cleanIndexesMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(EmailMessage emailMessage) {
    this.convertAndSendToQueue(this.emailsQueue, emailMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationFromFedoraMessage publicationFromFedoraMessage) {
    this.convertAndSendToQueue(this.publicationsFromFedoraQueue, publicationFromFedoraMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(PublicationDownloadMessage publicationDownloadMessage) {
    this.convertAndSendToQueue(this.publicationDownloadQueue, publicationDownloadMessage);
  }

  @TransactionalEventListener(fallbackExecution = true)
  private void sendMessage(CacheMessage cacheMessage) {
    this.convertAndSendToTopic(this.cacheTopic, cacheMessage);
  }
}
