/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - StructureRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public class StructureRemoteResourceService extends AdminRemoteResourceService<Structure> {

  protected StructureRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  public Structure getParentStructure(String resId) {
    Structure structure = this.findOne(resId);
    if (structure.getParentStructure() != null) {
      return this.findOne(structure.getParentStructure().getResId());
    }
    return null;
  }

  @Cacheable(AouCacheNames.PARENT_STRUCTURES_IDS)
  /**
   * Return a list that cannot be then modified, in order to prevent side effects with cache.
   * If you want to modify its content, clone the list before.
   */
  public List<String> getParentStructureIds(String resId) {
    Structure parentStructure = this.getParentStructure(resId);
    List<String> parentStructureIds = new ArrayList<>();
    while (parentStructure != null) {
      parentStructureIds.add(parentStructure.getResId());
      parentStructure = this.getParentStructure(parentStructure.getResId());
    }
    return Collections.unmodifiableList(parentStructureIds);
  }

  @Cacheable(AouCacheNames.PARENT_STRUCTURES_NAMES)
  /**
   * Return a list that cannot be then modified, in order to prevent side effects with cache.
   * If you want to modify its content, clone the list before.
   */
  public List<String> getParentStructureNames(String resId) {
    Structure parentStructure = this.getParentStructure(resId);
    List<String> parentStructureNames = new ArrayList<>();
    while (parentStructure != null) {
      parentStructureNames.add(parentStructure.getName());
      parentStructure = this.getParentStructure(parentStructure.getResId());
    }
    return Collections.unmodifiableList(parentStructureNames);
  }

  @Cacheable(AouCacheNames.STRUCTURE_BY_CN_STRUCT_C)
  public Structure findByCnStructC(String cnStructC) {
    String url = this.getResourceUrl() + "?cnStructC=" + cnStructC;
    List<Structure> structures = this.restClientService.getAllResources(url, Structure.class);
    if (structures.size() == 1) {
      return structures.get(0);
    } else if (structures.isEmpty()) {
      throw new SolidifyResourceNotFoundException("Structure with cnStructC " + cnStructC + " not found");
    } else {
      throw new SolidifyRuntimeException("Too many structures found with cnStructC " + cnStructC);
    }
  }

  @Override
  protected Class<Structure> getResourceClass() {
    return Structure.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.STRUCTURES;
  }
}
