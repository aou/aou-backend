/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - IdentifierService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.nio.ByteBuffer;
import java.util.UUID;

import org.apache.commons.codec.binary.Base32;
import org.springframework.stereotype.Service;

import ch.unige.aou.config.AouRepositoryDescription;

@Service
public class IdentifierService {

  private final AouRepositoryDescription repositoryInfo;

  public IdentifierService(AouRepositoryDescription repositoryInfo) {
    this.repositoryInfo = repositoryInfo;
  }

  /**
   * Compute DOI with
   *
   * @param resId
   * @return DOI
   */
  public String getNewDOI(String resId) {
    return this.repositoryInfo.getDoiPrefix() + this.repositoryInfo.getPrefix() + this.getShortId(resId);
  }

  /**
   * Extract resId from DOI with
   *
   * @param doi
   * @return resId
   */
  public String getResIdFromDOI(String doi) {
    return this.decodeBase32(doi.substring(doi.lastIndexOf(':')));
  }

  /**
   * Decode resIdInBase32
   *
   * @param resIdInBase32
   * @return resId
   */
  private String decodeBase32(String resIdInBase32) {
    final Base32 decoder = new Base32();
    final byte[] bytes = decoder.decode(resIdInBase32 + "======");
    final ByteBuffer bb = ByteBuffer.wrap(bytes);
    final UUID uuid = new UUID(bb.getLong(), bb.getLong());
    return uuid.toString();
  }

  /**
   * Encode resId in Base32 string
   *
   * @param resId
   * @return resIdInBase32
   */
  private String encodeBase32(String resID) {
    final UUID uuid = UUID.fromString(resID);
    final ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
    bb.putLong(uuid.getMostSignificantBits());
    bb.putLong(uuid.getLeastSignificantBits());
    final Base32 encoder = new Base32();
    final String base32Value = encoder.encodeAsString(bb.array());
    // Remove 6 '=' at the end
    return base32Value.substring(0, base32Value.length() - 6).toLowerCase();
  }

  /**
   * Compute short Id
   *
   * @param resId
   * @return shortId
   */
  private String getShortId(String resId) {
    return this.encodeBase32(resId);
  }
}
