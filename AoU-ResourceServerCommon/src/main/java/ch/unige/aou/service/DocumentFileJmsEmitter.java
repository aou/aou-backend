/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - DocumentFileJmsEmitter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import ch.unige.solidify.service.MessageService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.message.DocumentFileMessage;
import ch.unige.aou.model.publication.DocumentFile;

@Service
public class DocumentFileJmsEmitter extends JmsService {

  private String documentFilesQueue;

  public DocumentFileJmsEmitter(AouProperties aouProperties, MessageService messageService) {
    super(aouProperties, messageService);

    this.documentFilesQueue = aouProperties.getQueue().getDocumentFiles();
  }

  /**
   * Reacts to DocumentFile events that are sent when DocumentFile are persisted (see DocumentFileListener class)
   *
   * @param documentFile
   */
  @EventListener
  private void manageDocumentFileEvent(DocumentFile documentFile) {
    DocumentFileMessage documentFileMessage = new DocumentFileMessage(documentFile);

    /**
     * If DocumentFile status is RECEIVED, the document file processing must start
     * --> this is done async by sending a JMS to the processing queue
     */
    if (documentFile.getStatus() == DocumentFile.DocumentFileStatus.RECEIVED) {
      this.sendDocumentFileMessageToQueue(documentFileMessage);
    }
  }

  public void sendDocumentFileMessageToQueue(DocumentFileMessage documentFileMessage) {
    this.convertAndSendToQueue(this.documentFilesQueue, documentFileMessage);
  }
}
