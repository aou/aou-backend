/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - PersonRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.service.rest.abstractservice;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.ByteResponseDto;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public abstract class PersonRemoteResourceService extends AdminRemoteResourceService<Person> {

  protected PersonRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  @Cacheable(AouCacheNames.PERSON_AVATAR)
  public ByteResponseDto getPersonAvatar(String resId) throws URISyntaxException {
    String url = this.getResourceUrl() + "/" + resId + "/" + AouActionName.DOWNLOAD_AVATAR;

    AbstractRestClientTool restClientTool = this.restClientService.getRestClientTool();
    final ResponseEntity<byte[]> response = restClientTool.getClient().getForEntity(new URI(url), byte[].class);

    final HttpHeaders headers = response.getHeaders();
    final String contentType = Objects.requireNonNull(headers.getContentType()).toString();
    return new ByteResponseDto(Objects.requireNonNull(response.getBody()), contentType);
  }

  @Cacheable(AouCacheNames.PERSON_STRUCTURES)
  public List<Structure> getPersonStructures(String resId) {
    String url = this.getResourceUrl() + "/" + resId + "/" + ResourceName.STRUCTURES;
    return this.restClientService.getAllResources(url, Structure.class);
  }

  @Cacheable(AouCacheNames.PERSON_RESEARCH_GROUPS)
  public List<ResearchGroup> getPersonResearchGroups(String resId) {
    String url = this.getResourceUrl() + "/" + resId + "/" + ResourceName.RESEARCH_GROUPS;
    return this.restClientService.getAllResources(url, ResearchGroup.class);
  }

  @Override
  protected Class<Person> getResourceClass() {
    return Person.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.PEOPLE;
  }
}
