/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - LicenseRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public abstract class LicenseRemoteResourceService extends AdminRemoteResourceService<License> {

  protected LicenseRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  @Cacheable(AouCacheNames.OPEN_LICENSE_ID)
  public License findByOpenLicenseId(String openLicenseId) {
    String url = this.getResourceUrl() + "?openLicenseId=" + openLicenseId;
    List<License> licenses = this.restClientService.getAllResources(url, License.class);
    if (licenses.size() == 1) {
      return licenses.get(0);
    } else if (licenses.isEmpty()) {
      throw new SolidifyResourceNotFoundException("License with openLicenseId " + openLicenseId + " not found");
    } else {
      throw new SolidifyRuntimeException("Too many licenses found with openLicenseId " + openLicenseId);
    }
  }

  @Override
  protected Class<License> getResourceClass() {
    return License.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.LICENSES;
  }
}
