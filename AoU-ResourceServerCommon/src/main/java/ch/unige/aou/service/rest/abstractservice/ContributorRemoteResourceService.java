/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - ContributorRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public abstract class ContributorRemoteResourceService extends AdminRemoteResourceService<Contributor> {

  private static final Logger log = LoggerFactory.getLogger(ContributorRemoteResourceService.class);

  protected ContributorRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  @Cacheable(value = AouCacheNames.CONTRIBUTOR_CN_INDIVIDU, unless = "#result==null")
  public Contributor getByCnIndividu(String cnIndividu) {
    try {
      String url = this.getResourceUrl() + "/" + AouActionName.GET_BY_CN_INDIVIDU + "?cnIndividu=" + cnIndividu;
      return this.restClientService.getResource(url, Contributor.class);
    } catch (Exception e) {
      log.error("Contributor with cnIndividu {} not found", cnIndividu, e);
      return null;
    }
  }

  @Cacheable(value = AouCacheNames.CONTRIBUTOR_ORCID, unless = "#result==null")
  public Optional<Contributor> getByOrcid(String orcid) {
    try {
      String url = this.getResourceUrl() + "/" + AouActionName.GET_BY_ORCID + "?orcid=" + orcid;
      Contributor contributor = this.restClientService.getResource(url, Contributor.class);
      return Optional.of(contributor);
    } catch (Exception e) {
      log.error("No contributor with ORCID '{}' found", orcid, e);
      return Optional.empty();
    }
  }

  public RestCollection<Contributor> listUnigeContributors(Pageable pageable) {
    String url = this.getResourceUrl() + "/" + ActionName.SEARCH + "?term=&type=unige";
    return this.restClientService.getResourceList(url, this.getResourceClass(), pageable);
  }

  @Override
  protected Class<Contributor> getResourceClass() {
    return Contributor.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.CONTRIBUTORS;
  }
}
