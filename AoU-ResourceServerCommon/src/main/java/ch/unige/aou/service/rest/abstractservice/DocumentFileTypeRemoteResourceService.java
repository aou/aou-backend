/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - DocumentFileTypeRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public abstract class DocumentFileTypeRemoteResourceService extends AdminRemoteResourceService<DocumentFileType> {

  protected DocumentFileTypeRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  @Cacheable(AouCacheNames.DOCUMENT_FILE_TYPES)
  public RestCollection<DocumentFileType> findAllWithCache(Pageable pageable) {
    return super.findAll(pageable);
  }

  @Override
  protected Class<DocumentFileType> getResourceClass() {
    return DocumentFileType.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.adminModuleUrl + "/" + ResourceName.DOCUMENT_FILE_TYPES;
  }
}
