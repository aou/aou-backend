/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - HistoryService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.service.MessageService;

import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.security.UserDisplayableInfos;
import ch.unige.aou.repository.HistoryRepository;
import ch.unige.aou.service.rest.propagate.PropagateUserRemoteResourceService;

@Service
public class HistoryService extends AouService {
  private static final Logger log = LoggerFactory.getLogger(HistoryService.class);

  private final HistoryRepository historyRepository;
  private final PropagateUserRemoteResourceService userResourceService;

  public HistoryService(MessageService messageService, HistoryRepository historyRepository,
          PropagateUserRemoteResourceService userResourceService) {
    super(messageService);
    this.historyRepository = historyRepository;
    this.userResourceService = userResourceService;
  }

  public Page<StatusHistory> findByResId(String resId, Pageable pageable) {
    final Page<StatusHistory> listItem = this.historyRepository.findByResId(resId, pageable);

    listItem.forEach(statusHistory -> {
      if (!statusHistory.getCreatedBy().equals(SolidifyConstants.NO_CREDENTIAL)) {
        try {
          final UserDisplayableInfos userDisplayableInfos = this.userResourceService.getUserDisplayableInfos(statusHistory.getCreatedBy());
          statusHistory.setCreatorName(userDisplayableInfos.getFullName());
        } catch (SolidifyResourceNotFoundException e) {
          log.warn("no user with external uid '" + statusHistory.getCreatedBy() + "' found");
        }
      }
    });

    return listItem;
  }

  public List<StatusHistory> getHistory(String resId) {
    return this.historyRepository.findByResId(resId);
  }

  @EventListener
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void saveStatusHistory(StatusHistory statusHistory) {
    if (log.isTraceEnabled()) {
      log.trace("Received StatusHistory {}", statusHistory);
    }
    // Read last statusHistory
    final List<StatusHistory> statusHistoryList = this.findStatusHistoryOrderByLastDate(statusHistory.getResId());
    if (statusHistoryList.isEmpty() || !statusHistoryList.get(0).getStatus().equals(statusHistory.getStatus())) {
      // Add new statusHistory
      this.checkStatusHistoryEvent(statusHistory);
      this.historyRepository.save(statusHistory);
    }
  }

  public StatusHistory updateChangeTime(StatusHistory statusHistory, OffsetDateTime changeTime) {
    statusHistory.setChangeTime(changeTime);
    return this.historyRepository.save(statusHistory);
  }

  public List<StatusHistory> findStatusHistoryOrderByLastDate(String resId) {
    return this.historyRepository.findStatusHistoryOrderByLastDate(resId);
  }

  private void checkStatusHistoryEvent(StatusHistory statusHistory) {
    if (statusHistory.getDescription() == null) {
      String msg = "statusHistory.history";
      if (statusHistory.getType().equals("Publication")) {
        if (statusHistory.getStatus().equals("IN_PROGRESS") && statusHistory.getStep() != null) {
          msg = "publication.status." + statusHistory.getStatus().toLowerCase() + ".formStep." + statusHistory.getStep().toLowerCase();
        } else {
          msg = "publication.status." + statusHistory.getStatus().toLowerCase();
        }
      } else if (statusHistory.getType().endsWith("DocumentFile")) {
        msg = "documentfile.status." + statusHistory.getStatus().toLowerCase();
      }
      statusHistory.setDescription(this.messageService.get(msg));
    }
  }

  public boolean hasBeenUpdatingMetadata(String resId) {
    return this.hasBeenInStatusAndNotCompletedAfter(resId, Publication.PublicationStatus.IN_EDITION);
  }

  public boolean hasBeenUpdatingMetadataInBatch(String resId) {
    return this.hasBeenInStatusAndNotCompletedAfter(resId, Publication.PublicationStatus.UPDATE_IN_BATCH);
  }

  public boolean hasBeenInStatusAndNotCompletedAfter(String resId, Publication.PublicationStatus status) {
    final StatusHistory lastEvent = this.getLastStatusEvent(resId);
    if (lastEvent == null) {
      return false;
    }
    final OffsetDateTime lastEventTime = lastEvent.getChangeTime();
    final OffsetDateTime lastSearchStatus = this.findLastWithStatusTimeBeforeOrEquals(resId, status.toString(), lastEventTime);
    if (lastSearchStatus == null) {
      return false;
    }
    // check if there is a completed status after, if so, it is from a previous editing_metadata
    List<StatusHistory> statusHistoryList = this.historyRepository
            .findAllWithStatusAfterOrderByChangeTimeAsc(resId, Publication.PublicationStatus.COMPLETED.toString(), lastSearchStatus);
    return statusHistoryList.isEmpty();
  }

  public boolean hasBeenCompleted(String resId) {
    return this.historyRepository.hasBeenInStatus(resId, Publication.PublicationStatus.COMPLETED.name());
  }

  private OffsetDateTime findLastWithStatusTimeBeforeOrEquals(String resId, String status, OffsetDateTime before) {
    List<StatusHistory> statusHistoryList = this.historyRepository.findAllWithStatusBeforeOrEqualsOrderByChangeTimeDesc(resId, status, before);
    if (statusHistoryList.isEmpty()) {
      return null;
    } else {
      return statusHistoryList.get(0).getChangeTime();
    }
  }

  private StatusHistory getLastStatusEvent(String resId) {
    // Sort history by date
    final List<StatusHistory> sortedList = this.sortStatusList(resId);
    if (!sortedList.isEmpty()) {
      return sortedList.get(sortedList.size() - 1);
    }
    return null;
  }

  private List<StatusHistory> sortStatusList(String resId) {
    return this.getHistory(resId).stream()
            .sorted(Comparator.comparing(StatusHistory::getSeq))
            .toList();
  }

}
