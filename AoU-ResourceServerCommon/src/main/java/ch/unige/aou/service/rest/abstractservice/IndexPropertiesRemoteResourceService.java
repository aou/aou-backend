/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - IndexPropertiesRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;

import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.AouCacheNames;

public abstract class IndexPropertiesRemoteResourceService extends DataMgmtRemoteResourceService<IndexFieldAlias> {

  protected IndexPropertiesRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  @Cacheable(AouCacheNames.FACET_REQUEST)
  public List<FacetRequest> getFacetRequests(String indexName) {
    String url = this.getResourceUrl() + "/" + AouActionName.LIST_FACET_REQUESTS + "/" + indexName;
    return this.restClientService.getAllResources(url, FacetRequest.class);
  }

  @Cacheable(AouCacheNames.INDEX_FIELD_ALIAS)
  public List<IndexFieldAlias> getIndexFieldAliases(String indexName) {
    String url = this.getResourceUrl() + "/" + AouActionName.LIST_INDEX_FIELD_ALIASES + "/" + indexName;
    return this.restClientService.getAllResources(url, IndexFieldAlias.class);
  }

  @Override
  protected Class<IndexFieldAlias> getResourceClass() {
    return IndexFieldAlias.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.dataMgmtModuleUrl + "/" + ResourceName.INDEX_PROPERTIES;
  }
}
