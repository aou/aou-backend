/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - CacheService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Lazy;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.message.ResourceCacheMessage;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.service.AbstractCacheService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.PersonAvatar;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

@Service
public class CacheService extends AbstractCacheService {
  private static final Logger log = LoggerFactory.getLogger(CacheService.class);

  protected CacheService self;

  public CacheService(@Lazy CacheService cacheService) {
    this.self = cacheService;
  }

  @JmsListener(destination = "${aou.topic.cache}", containerFactory = "topicListenerFactory")
  public void processCacheMessage(CacheMessage message) {
    if (log.isDebugEnabled()) {
      log.debug(message.toString());
    }

    if (message instanceof ResourceCacheMessage) {
      this.self.clearCachedResource(((ResourceCacheMessage) message).getResId());

      if (((ResourceCacheMessage) message).getResourceClass() == IndexFieldAlias.class) {
        this.self.evictCachedFacets();
        this.self.evictCachedIndexFieldAliases();
      } else if (((ResourceCacheMessage) message).getResourceClass() == Structure.class) {
        this.self.evictCachedParentStructuresIds();
        this.self.evictCachedParentStructuresNames();
        this.self.evictCachedStructuresCnStructC();
        this.self.evictCachedPersonStructures();
      } else if (((ResourceCacheMessage) message).getResourceClass() == ResearchGroup.class) {
        this.self.evictCachedPersonResearchGroups();
      } else if (((ResourceCacheMessage) message).getResourceClass() == PublicationSubtype.class) {
        this.self.evictCachedPublicationSubtypes();
      } else if (((ResourceCacheMessage) message).getResourceClass() == PublicationSubSubtype.class) {
        this.self.evictCachedPublicationSubSubtypes();
      } else if (((ResourceCacheMessage) message).getResourceClass() == DocumentFileType.class) {
        this.self.evictCachedDocumentFileTypes();
      } else if (((ResourceCacheMessage) message).getResourceClass() == License.class) {
        this.self.evictCachedOpenLicenseIds();
      } else if (((ResourceCacheMessage) message).getResourceClass() == User.class) {
        this.self.evictCachedUserByExternalUids();
      } else if (((ResourceCacheMessage) message).getResourceClass() == Person.class) {
        this.self.evictCachedPersonStructures();
        this.self.evictCachedPersonResearchGroups();
        this.self.evictCachedPersonAvatar();
      } else if (((ResourceCacheMessage) message).getResourceClass() == PersonAvatar.class) {
        this.self.evictCachedPersonAvatar();
      } else {
        this.self.evictCachedResources();
      }
      if (((ResourceCacheMessage) message).getResourceClass() == Contributor.class) {
        String cnIndividu = (String) message.getOtherProperty("cnIndividu");
        if (!StringTool.isNullOrEmpty(cnIndividu)) {
          this.self.evictCachedContributorByCnIndividu(cnIndividu);
        }
        String orcid = (String) message.getOtherProperty("orcid");
        if (!StringTool.isNullOrEmpty(orcid)) {
          this.self.evictCachedContributorByOrcid(orcid);
        }
      }
    }
  }

  public void cleanAllCaches() {
    this.self.evictCachedContributorsByCnIndividu();
    this.self.evictCachedContributorsByOrcid();
    this.self.evictCachedDocumentFileTypes();
    this.self.evictCachedFacets();
    this.self.evictCachedIndexFieldAliases();
    this.self.evictCachedOpenLicenseIds();
    this.self.evictCachedParentStructuresIds();
    this.self.evictCachedParentStructuresNames();
    this.self.evictCachedPersonAvatar();
    this.self.evictCachedPersonResearchGroups();
    this.self.evictCachedPersonStructures();
    this.self.evictCachedPublicationSubSubtypes();
    this.self.evictCachedPublicationSubtypes();
    this.self.evictCachedResources();
    this.self.evictCachedStructuresCnStructC();
    this.self.evictCachedUserByExternalUids();
  }

  @CacheEvict(value = AouCacheNames.FACET_REQUEST, allEntries = true)
  public void evictCachedFacets() {
    log.info("Facets removed from cache");
  }

  @CacheEvict(value = AouCacheNames.INDEX_FIELD_ALIAS, allEntries = true)
  public void evictCachedIndexFieldAliases() {
    log.info("IndexFieldAliases removed from cache");
  }

  @CacheEvict(value = AouCacheNames.PARENT_STRUCTURES_IDS, allEntries = true)
  public void evictCachedParentStructuresIds() {
    log.info("Parent structures ids removed from cache");
  }

  @CacheEvict(value = AouCacheNames.PARENT_STRUCTURES_NAMES, allEntries = true)
  public void evictCachedParentStructuresNames() {
    log.info("Parent structures names removed from cache");
  }

  @CacheEvict(value = AouCacheNames.STRUCTURE_BY_CN_STRUCT_C, allEntries = true)
  public void evictCachedStructuresCnStructC() {
    log.info("Structures by cnStructC removed from cache");
  }

  @CacheEvict(value = AouCacheNames.CONTRIBUTOR_CN_INDIVIDU)
  public void evictCachedContributorByCnIndividu(String cnIndividu) {
    log.info("Contributor with cnIndividu '{}' removed from cache", cnIndividu);
  }

  @CacheEvict(value = AouCacheNames.CONTRIBUTOR_CN_INDIVIDU, allEntries = true)
  public void evictCachedContributorsByCnIndividu() {
    log.info("All Contributors by cnIndividu removed from cache");
  }

  @CacheEvict(value = AouCacheNames.CONTRIBUTOR_ORCID)
  public void evictCachedContributorByOrcid(String orcid) {
    log.info("Contributor with ORCID '{}' removed from cache", orcid);
  }

  @CacheEvict(value = AouCacheNames.CONTRIBUTOR_ORCID, allEntries = true)
  public void evictCachedContributorsByOrcid() {
    log.info("All Contributors by ORCID removed from cache");
  }

  @CacheEvict(value = AouCacheNames.PUBLICATION_SUBTYPES, allEntries = true)
  public void evictCachedPublicationSubtypes() {
    log.info("PublicationSubtypes removed from cache");
  }

  @CacheEvict(value = AouCacheNames.PUBLICATION_SUB_SUBTYPES, allEntries = true)
  public void evictCachedPublicationSubSubtypes() {
    log.info("PublicationSubSubtypes removed from cache");
  }

  @CacheEvict(value = AouCacheNames.DOCUMENT_FILE_TYPES, allEntries = true)
  public void evictCachedDocumentFileTypes() {
    log.info("DocumentFileTypes removed from cache");
  }

  @CacheEvict(value = AouCacheNames.OPEN_LICENSE_ID, allEntries = true)
  public void evictCachedOpenLicenseIds() {
    log.info("Licenses by openLicenseId removed from cache");
  }

  @CacheEvict(value = AouCacheNames.USER_BY_EXTERNAL_UID, allEntries = true)
  public void evictCachedUserByExternalUids() {
    log.info("Users by external uids removed from cache");
  }

  @CacheEvict(value = AouCacheNames.RESOURCES, allEntries = true)
  public void evictCachedResources() {
    log.debug("Resources removed from cache");
  }

  @Scheduled(cron = "0 0 */3 * * *")
  @CacheEvict(value = AouCacheNames.UNIGE_PERSON_CN_INDIVIDU, allEntries = true)
  public void clearCnIndividuCache() {
    log.info("Cache '{}' allEntries cleared.", AouCacheNames.UNIGE_PERSON_CN_INDIVIDU);
  }

  @CacheEvict(value = AouCacheNames.PERSON_AVATAR, allEntries = true)
  public void evictCachedPersonAvatar() {
    log.info("Person avatar removed from cache");
  }

  @CacheEvict(value = AouCacheNames.PERSON_STRUCTURES, allEntries = true)
  public void evictCachedPersonStructures() {
    log.info("Structures by person removed from cache");
  }

  @CacheEvict(value = AouCacheNames.PERSON_RESEARCH_GROUPS, allEntries = true)
  public void evictCachedPersonResearchGroups() {
    log.info("Research groups by person removed from cache");
  }
}
