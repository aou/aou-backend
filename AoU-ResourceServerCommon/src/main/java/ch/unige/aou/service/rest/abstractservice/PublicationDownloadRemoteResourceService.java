/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - PublicationRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.rest.abstractservice;

import org.elasticsearch.ResourceNotFoundException;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.service.SolidifyRestClientService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;

public abstract class PublicationDownloadRemoteResourceService extends AccessRemoteResourceService<PublicationDownload> {

  protected PublicationDownloadRemoteResourceService(SolidifyRestClientService restClientService, AouProperties aouProperties) {
    super(restClientService, aouProperties);
  }

  @Override
  public PublicationDownload findOne(String resId) {
    try {
      return super.findOne(resId);
    } catch (SolidifyResourceNotFoundException resourceNotFoundException) {
      throw new ResourceNotFoundException(resourceNotFoundException.getMessage());
    }
  }

  public boolean exists(String resId) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + AouActionName.EXISTS;
    return this.restClientService.getResource(url, Boolean.class);
  }
  
  public void prepareDownload(String resId) {
    final String url = this.accessModuleUrl + "/" + ResourceName.METADATA + "/" + resId + "/" + AouActionName.PREPARE_DOWNLOAD;
    this.restClientService.postResourceAction(url);
  }

  @Override
  protected Class<PublicationDownload> getResourceClass() {
    return PublicationDownload.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.accessModuleUrl + "/" + ResourceName.PUBLICATION_DOWNLOAD;
  }
}
