/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AouControllerAction.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

public enum AouControllerAction {
  APPROVE, APPROVE_DISPOSAL, CHECK, CREATE, DELETE, DELETE_FILE, DELETE_FOLDER, DOWNLOAD_FILE,
  DOWNLOAD_SIP_FILE, EXTEND_RETENTION, GET, GET_AIP, GET_CURRENT_FORM, GET_FILE, GET_FORM, GET_MEMBER, HISTORY, LIST, LIST_FILES,
  LIST_FORMS, LIST_MEMBERS, PREPARE_DOWNLOAD_ARCHIVE, REINDEX, REJECT, RESERVE_DOI, RESUME, RESUME_FILE, SEND_FILES, VALIDATE_METADATA,
  SUBMIT_FOR_VALIDATION, SUBMIT, UPDATE, VALIDATE_FILE, ADD_DEPOSIT_RESEARCH_GROUP, DELETE_DEPOSIT_RESEARCH_GROUP, UPDATE_DEPOSIT_RESEARCH_GROUP,
  ADD_DEPOSIT_VALIDATION_STRUCTURE, DELETE_DEPOSIT_VALIDATION_STRUCTURE, UPDATE_DEPOSIT_VALIDATION_STRUCTURE, ADD_DEPOSIT_COMMENT,
  DELETE_DEPOSIT_COMMENT, UPDATE_DEPOSIT_COMMENT, LIST_DEPOSIT_COMMENTS, LIST_VALIDATOR_COMMENTS, GET_DEPOSIT_COMMENT, ADD_DOCUMENT_FILE,
  DELETE_DOCUMENT_FILE, UPDATE_DOCUMENT_FILE, UPLOAD_DOCUMENT_FILE, ASK_FEEDBACK, ENABLE_REVISION, SEND_BACK_FOR_VALIDATION, CLONE,
  START_METADATA_EDITING, CANCEL_METADATA_EDITING, GET_PENDING_METADATA_UPDATES, CHECK_DUPLICATES,
  EXPORT_TO_ORCID, EXPORT_DEPOSITS
}
