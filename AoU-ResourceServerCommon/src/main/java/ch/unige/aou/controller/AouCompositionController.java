/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AouCompositionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.CompositionController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.HistoryService;

@UserPermissions
public abstract class AouCompositionController<T extends Resource, V extends Resource> extends CompositionController<T, V> {

  @Autowired
  HistoryService historyService;

  @GetMapping(SolidifyConstants.URL_ID_PLUS_SEP + ResourceName.HISTORY)
  public HttpEntity<RestCollection<StatusHistory>> history(@PathVariable String parentid, @PathVariable String id, Pageable pageable) {
    final V item = this.checkSubresourceExistsAndIsLinkedToParent(id, parentid);
    if (item == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    final Page<StatusHistory> listItem = this.historyService.findByResId(id, pageable);
    final RestCollection<StatusHistory> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(this.getClass(), parentid, id, pageable).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass(), parentid, id, pageable)).toUriComponentsBuilder()).withRel(ActionName.PARENT));
    this.addPageLinks(linkTo(this.getClass(), parentid, id, pageable), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

}
