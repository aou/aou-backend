/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AouMonitorController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;

import ch.unige.solidify.security.AdminPermissions;
import ch.unige.solidify.security.EveryonePermissions;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.ApplicationRoleService;
import ch.unige.aou.rest.AouActionName;

@AdminPermissions
public abstract class AouMonitorController {

  private ApplicationRoleService applicationRoleService;

  public AouMonitorController(ApplicationRoleService applicationRoleService) {
    this.applicationRoleService = applicationRoleService;
  }

  @GetMapping(AouActionName.MONITOR_DATABASE_CONNECTION)
  @EveryonePermissions
  public HttpStatus testDatabaseConnection() {
    this.applicationRoleService.existsById(AouConstants.DEPOSIT_TYPE_ARTICLE_ID);
    return HttpStatus.OK;
  }
}
