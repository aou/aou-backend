/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AuthenticatedUserSecurity.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.security.User;
import ch.unige.aou.service.rest.propagate.PropagateUserRemoteResourceService;

@Profile("!test")
@Service
public class AuthenticatedUserSecurity implements AuthenticatedUserProvider {

  private final PropagateUserRemoteResourceService userResourceService;

  public AuthenticatedUserSecurity(PropagateUserRemoteResourceService userResourceService) {
    this.userResourceService = userResourceService;
  }

  @Override
  public String getPersonId(Authentication authentication) {
    final String authUserId = authentication.getName();

    /*
     * Get Person linked to user identified by its OAuth2 token
     */
    final User user = this.userResourceService.getAuthenticatedUser();
    if (user.getExternalUid().equals(authUserId)) {
      if (user.getPerson() != null) {
        return user.getPerson().getResId();
      } else {
        throw new SolidifyResourceNotFoundException("Authenticated user is not linked to any person");
      }
    } else {
      throw new SolidifyRuntimeException("User returned does not correspond to authentication");
    }
  }
}
