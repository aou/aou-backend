/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - AouResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.settings.ValueWithTranslations;

public abstract class AouResourceService<T extends Resource> extends ResourceService<T> {

  @Autowired
  protected SpecificationPermissionsFilter<T> specificationPermissionFilter;

  @Autowired
  protected AuthenticatedUserProvider authenticatedUserProvider;

  @Override
  public T findOne(String resId) {
    T item = super.findOne(resId);
    this.afterFind(item);
    return item;
  }

  public Optional<T> findById(String resId) {
    Optional<T> itemOpt = this.itemRepository.findById(resId);
    if (itemOpt.isPresent()) {
      T item = itemOpt.get();
      this.afterFind(item);
    }
    return itemOpt;
  }

  @Override
  public Page<T> findAll(Specification<T> spec, Pageable pageable) {
    Page<T> results = super.findAll(spec, pageable);
    if (results.hasContent()) {
      results.forEach(item -> this.afterFind(item));
    }
    return results;
  }

  @Override
  public Page<T> findAll(Pageable pageable) {
    Page<T> results = super.findAll(pageable);
    if (results.hasContent()) {
      results.forEach(item -> this.afterFind(item));
    }
    return results;
  }

  /**
   * Override if treatment must be done after the entity has been fetched from db
   *
   * @param item
   * @return
   */
  @Override
  protected T afterFind(T item) {
    return item;
  }

  protected ValueWithTranslations newValueWithEngFreTranslations(String value, String englishText, String frenchText) {
    ValueWithTranslations valueWithTranslations = new ValueWithTranslations(value);
    valueWithTranslations.addTranslation(AouConstants.LANG_CODE_ENGLISH, englishText);
    valueWithTranslations.addTranslation(AouConstants.LANG_CODE_FRENCH, frenchText);
    return valueWithTranslations;
  }

  public boolean authenticatedUserHasPriviledgedRole() {
    final String[] applicationRoles = new String[] { AuthApplicationRole.ADMIN_ID, AuthApplicationRole.ROOT_ID,
            AuthApplicationRole.TRUSTED_CLIENT_ID };

    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      final java.util.Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

      for (final String roleName : applicationRoles) {
        for (final GrantedAuthority authority : authorities) {
          if (authority.toString().equals(roleName)) {
            return true;
          }
        }
      }
    }
    return false;
  }

  public Optional<String> externalUidToUnigeCnIndividu(String externalUid) {
    if (!StringTool.isNullOrEmpty(externalUid)) {
      if (externalUid.endsWith(AouConstants.UNIGE_EXTERNAL_UID_SUFFIX)) {
        return Optional.of(StringTool.substringBefore(externalUid, AouConstants.UNIGE_EXTERNAL_UID_SUFFIX));
      } else if (externalUid.endsWith(AouConstants.TEST_EXTERNAL_UID_SUFFIX)) {
        return Optional.of(externalUid);
      }
    }
    return Optional.empty();
  }

  /**
   * Calculates a default sort value if the Sortable doesn't exist yet and has no sortValue property
   * Looks for values existing in database and sets a value placing the entity at the end of a sorted
   * list.
   *
   * @param item
   */
  public void setDefaultSortValue(Sortable item) {
    if (!this.itemRepository.existsById(item.getResId()) && item.getSortValue() == null) {
      Optional<Integer> maxValueOpt = this.getMaxSortValue(item);
      int newSortValue = maxValueOpt.isPresent() ? maxValueOpt.get() + AouConstants.ORDER_INCREMENT : AouConstants.ORDER_INCREMENT;
      item.setSortValue(newSortValue);
    }
  }

  // override in services managing Sortable entities
  public Optional<Integer> getMaxSortValue(Sortable item) {
    throw new SolidifyRuntimeException("method 'getMaxSortValue()' is not implemented");
  }
}
