/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Resource Server Common - ApplicationRoleService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.model.SolidifyApplicationRole;

import ch.unige.aou.repository.ApplicationRoleRepository;
import ch.unige.aou.specification.AouApplicationRoleSpecification;

@Service
public class ApplicationRoleService extends AouResourceService<SolidifyApplicationRole> {
  Logger log = LoggerFactory.getLogger(ApplicationRoleService.class);

  private ApplicationRoleRepository applicationRoleRepository;

  public ApplicationRoleService(ApplicationRoleRepository applicationRoleRepository) {
    this.applicationRoleRepository = applicationRoleRepository;
  }

  @Override
  public AouApplicationRoleSpecification getSpecification(SolidifyApplicationRole resource) {
    return new AouApplicationRoleSpecification(resource);
  }

  public void initDefaultData() {
    for (ApplicationRole role : AuthApplicationRole.getApplicationRoleList()) {
      if (!this.applicationRoleRepository.findById(role.getResId()).isPresent()) {
        SolidifyApplicationRole applicationRole = new SolidifyApplicationRole(role);
        this.save(applicationRole);
        this.log.info("Application Role '{}' created with level {}", role.getResId(), role.getLevel());
      }
    }
  }
}
