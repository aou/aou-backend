###########
# Archive #
###########
archive.error.path_not_found=Directory ({1}) not found for Archive {0}
###########
# Deposit #
###########
deposit.error.type.empty=The type cannot be empty
deposit.error.type.invalid_name=This type doesn't exist
deposit.error.subtype.empty=The subtype cannot be empty
deposit.error.subtype.invalid_name=This subtype doesn't exist
deposit.error.subtype.invalid_type=This subtype cannot be associated with the selected type
deposit.error.subSubtype.invalid_name=This sub-subtype cannot be associated with the selected subtype
deposit.error.subSubtype.not_permitted="Publication sub-subtype ''{0}'' cannot be associated with the selected subtype ''{1}''
#
deposit.error.duplicate=It is possible that this publication has already been created, please check before submitting it
deposit.error.identifiers.doi.invalid=The value ''{0}'' is not a valid DOI
deposit.error.identifiers.shortdoi.invalid=The value ''{0}'' is not a valid shortDOI
deposit.error.identifiers.doi.already_exists=The DOI ''{0}'' already exists in Archive ouverte UNIGE
deposit.error.identifiers.doi.not_found=The DOI ''{0}'' was not found at Crossref nor at DataCite
deposit.error.identifiers.doi.not_found_by_crossref=The DOI ''{0}'' was not found at Crossref
deposit.error.identifiers.doi.not_found_by_datacite=The DOI ''{0}'' was not found at DataCite
deposit.error.identifiers.doi.not_found_by_inspire_hep=The DOI ''{0}'' was not found at INSPIRE-HEP
deposit.error.identifiers.pmid.invalid=The value ''{0}'' is not a valid PMID
deposit.error.identifiers.pmid.already_exists=The PMID ''{0}'' already exists in Archive ouverte UNIGE
deposit.error.identifiers.pmid.not_found=The PMID ''{0}'' was not found by Pubmed
deposit.error.identifiers.arxiv.invalid=The value ''{0}'' is not a valid arXiv identifier
deposit.error.identifiers.arxiv.already_exists=The ArXiv identifier ''{0}'' already exists in Archive ouverte UNIGE
deposit.error.identifiers.arxiv.not_found=The arXiv identifier ''{0}'' was not found at INSPIRE-HEP nor at Arxiv
deposit.error.identifiers.arxiv.not_found_by_arxiv=The arXiv identifier ''{0}'' was not found at Arxiv
deposit.error.identifiers.arxiv.not_found_by_inspire_hep=The arXiv identifier ''{0}'' was not found at INSPIRE-HEP
deposit.error.identifiers.isbn.invalid=The value ''{0}'' is not a valid ISBN
deposit.error.identifiers.isbn.already_exists=The ISBN ''{0}'' already exists in Archive ouverte UNIGE
deposit.error.identifiers.isbn.not_found=Unable to get metadata for the ISBN ''{0}''
deposit.error.identifiers.isbn.not_found_by_swisscovery=The ISBN identifier ''{0}'' was not found at Swisscovery
deposit.error.identifiers.pmcid.not_found=The identifier ''{0}'' was not found by EuropePmc
deposit.error.identifiers.issn.invalid=The value ''{0}'' is not a valid ISSN
deposit.error.identifiers.urn.invalid=The value ''{0}'' is not a valid URN
deposit.error.identifiers.indirect_already_exists=This publication has already been deposited using another identifier. You can complete the information entered or notify us of any necessary changes.
#
deposit.error.structure.validation_structure_not_automatically_set=No validation structure could be automatically retrieved from your profile.
deposit.error.structure.invalid_code=This Structure code doesn't exit
deposit.error.structure.invalid_id=The structure id ''{0}'' doesn''t exit
deposit.error.structure.included_in_another_one=The structure ''{0}'' is already implicitly included because one of its sub structures is selected
deposit.error.structure.endpoint_can_only_be_used_for_validation_structures=This endpoint can only be used to manage the publication's validation structure
#
deposit.error.researchGroup.invalid_code=This research group code doesn't exit
deposit.error.researchGroup.invalid_id=The research group id ''{0}'' doesn't exit
deposit.error.researchGroup.not_valid=Ask an administrator to validate the new research group ''{0}''
#
deposit.error.contributors.publication_has_no_contributor=The publication doesn't contain any contributor
deposit.error.contributors.appears_more_than_once=This contributor appears more than once
deposit.error.contributors.missing_author_in_contributors=Missing author for this subtype of publication: ''{0}''
deposit.error.contributors.missing_director_in_contributors=Missing director for this subtype of publication: ''{0}''
deposit.error.contributors.only_one_author_in_contributors=Only one author is possible for this subtype of publication: ''{0}''
deposit.error.contributors.uppercase_name=Names should not be entered in upper case
deposit.error.contributors.firstname_changed=Firstname: ''{0}'' of the UNIGE contributor has been changed
deposit.error.contributors.lastname_changed=Lastname: ''{0}'' of the UNIGE contributor has been changed
deposit.error.contributors.invalid_role=The author type ''{0}'' is not allowed for the publication type ''{1}''
#
deposit.error.language.appears_more_than_once=This language appears more than once
deposit.error.language.at_least_one_value_mandatory=At least one language is mandatory
#
deposit.error.abstract.maximum_size_exceeded=Maximum size exceeded for abstract {0}
deposit.error.abstract.mandatory_for_diploma=An abstract is mandatory for diploma type deposits
#
deposit.error.files.at_least_one_file_mandatory=At least one file is mandatory
deposit.error.files.at_least_one_file_not_agreement_mandatory=At least one file that is not an agreement is mandatory
deposit.error.files.imprimatur_mandatory_for_thesis=An imprimatur must be uploaded when submitting a thesis from UNIGE
deposit.error.files.publication_mode_mandatory=A ''Mode de publication'' file is required when submitting a thesis in the structure ''{0}''
deposit.error.files.file_type_not_allowed_with_publication_type=The document file type ''{0}'' is not allowed for the publication type ''{1}''
deposit.error.files.not_sure_for_license=The license of the file ''{0}'' must be verified
deposit.error.files.empty=File missing in this publication
deposit.error.files.empty_after_update=Files cannot be removed during update
deposit.error.document_files.duplicates=There are potential duplicates files in this deposit, please delete one of them using the ... option on the right of the file
deposit.error.document_files.in_error=There are files in error in this deposit
deposit.error.document_files.impossible_to_downloadable=Some files could not be recovered automatically. Please delete them (Edit > Click on ... at the very right of the file line > Delete) and replace them with a new file.
#
deposit.error.field_cannot_be_empty=This field cannot be empty
deposit.error.field_invalid_value=This value is not valid
#
deposit.error.date.invalid_date=This date is not a valid date
deposit.error.date.at_least_one_date_is_mandatory=At least one date is mandatory
deposit.error.date.cannot_be_in_the_future=This date cannot be in the future
#
deposit.error.updating_status_not_allowed_this_way=Updating a deposit status is not allowed this way
#
deposit.error.description.invalid_jel=JEL code ''{0}'' is invalid
##############
# Structures #
##############
structures.initialization.success=Structures initialized with success
structures.initialization.error=Structures could not be initialized
#
###################
# Research groups #
###################
research_groups.initialization.success=Research groups initialized with success
research_groups.initialization.error=Research groups could not be initialized
research_groups.synchronization.success.number=Number of search groups successfully synchronized: {0}
research_groups.synchronization.success.none=There was no research group to synchronize
research_groups.error.sameNameActiveStatus=An active research group with the name ''{0}'' already exists
research_groups.error.sameNameInactiveStatus=An inactive research group with the name ''{0}'' already exists
#
###############
# Publication #
###############
publication.status.created=Creation
publication.status.in_progress.formStep.type=Deposit in progress, "type" step
publication.status.in_progress.formStep.files=Deposit in progress, "files" step
publication.status.in_progress.formStep.contributors=Deposit in progress, "contributors" step
publication.status.in_progress.formStep.description=Deposit in progress, "description" step
publication.status.in_progress.formStep.summary=Deposit in progress, "summary" step
publication.status.in_validation=Sent for validation
publication.status.feedback_required=Feedback required
publication.status.submitted=Submitted
publication.status.completed=Completed
publication.status.canonical=Canonical
publication.status.deleted=Deleted
publication.status.in_error=In error
publication.status.rejected=Rejected
publication.status.in_preparation=In preparation for storage
publication.status.in_progress=In progress
publication.status.checked=Metadata valid
publication.status.in_edition=In edition
publication.status.updates_validation=Updates validation
publication.status.cancel_edition=Cancel edition
publication.status.update_in_batch=Update in batch
publication.completed=The publication {0} is already completed
publication.inedition=The publication {0} is being updated
publication.inprogress=The publication {0} is in progress
publication.resume=The publication {0} has been resumed
publication.updates_validation=The publication {0} has been updated
publication.unlink_contributor=I shouldn’t be listed as a contributor for this publication, please dont link me to this publication again.\n\n(My CN Individu number is: ''{0}'')
publication.comment.contributorsPartiallyImported=Only the first {0} contributors (as well as possible additional UNIGE members) have been automatically imported.
publication.error.path_not_found=Directory ({1}) not found for Publication {0}
publication.orcid.already_exists_in_profile=The publication already exists in your ORCID profile
publication.orcid.external_id_required=The publication cannot be sent to your ORCID profile as it doesn't contain any recognized external identifier
publication.orcid.too_old=The publication cannot be sent to your ORCID profile as it date prior to 1900
publication.orcid.sent_to_your_profile=The publication was sent to your ORCID profile
publication.orcid.sync_from_orcid_started=Synchronization from your ORCID profile started
publication.orcid.sync_to_orcid_started=Synchronization to your ORCID profile started
#
################
# DocumentFile #
################
documentfile.status.created=Creation
documentfile.status.received=Received
documentfile.status.to_process=Sent for processing
documentfile.status.processed=Processed
documentfile.status.ready=Ready
documentfile.status.confirmed=Confirmed
documentfile.status.cleaning=Cleaning
documentfile.status.in_error=In error
documentfile.status.impossible_to_download=Impossible to download
documentfile.status.duplicate=Duplicated
documentFile.status.impossible_to_download.not_valid_pdf=The file cannot be downloaded correctly or is invalid
documentFile.status.to_be_confirmed.empty_text=The file doesn't seem to contain text.
documentFile.status.to_be_confirmed.password_protected=The content of the file cannot be read as it is protected by a password.
documentFile.status.to_be_confirmed.parsing_error=File reading or parsing error.
documentFile.status.auto_confirmed.empty_text=The file doesn't seem to contain text but has been automatically confirmed according to its type.
documentFile.error.already_exists=This document file already exists in this publication
documentFile.error.check=An error occurred while checking the document file
documentFile.error.transfer=An error occurred while transferring the document file
documentFile.error.insufficient_space=Not enough space left on the disk to store the document file
documentFile.error.missing_folder=The destination folder ''{0}'' does not exist
documentFile.embargo.error.missing_info=This value is mandatory if an embargo has been set
documentFile.embargo.error.bad_access_level=Access level during embargo ''{0}'' is incoherent with final access level ''{1}''
#
############
# Metadata #
############
metadata.error.version=Metadata version ''{0}'' not supported
#
#################
# Notifications #
#################
notifications.message.new_deposit_to_validate=New deposit to validate: ''{0}''
notifications.message.indirect_deposit_validated=A publication for which you are a contributor has been validated ''{0}''
notifications.message.my_deposit_validated=A publication for which you are a depositor has been validated ''{0}''
notifications.message.deposit_requires_feedback={0} ( {1} )
notifications.message.my_deposit_rejected={0} ( {1} )
notifications.message.comment_deposit_validator={0} ( {1} )
notifications.message.comment_deposit_contributor={0} ( {1} )
notifications.message.comment_deposit_depositor={0} ( {1} )
notifications.message.new_publication_imported_from_orcid=A publication was imported from your ORCID profile
notifications.message.new_publication_exported_to_orcid=A publication was exported to your ORCID profile
#
##################
# StoredSearches #
##################
stored_searches.error.empty_criteria_list=Unable to store a search without criteria
stored_searches.error.name_already_exists_for_person=You already have a search whose name is ''{0}''
stored_searches.error.criteria.field_empty=Field name cannot be null
stored_searches.error.criteria.term_empty_values='value' or 'terms' fields must be filled for a term criteria
stored_searches.error.criteria.term_both_values=Only one of 'value' or 'terms' fields must be filled
stored_searches.error.criteria.empty_value=This field cannot be null
stored_searches.error.criteria.range_empty_values='from' and/or 'to' values must be filled for a range criteria
stored_searches.error.criteria.nested_boolean_unsupported=Criteria of type 'nested' are not supported
#
#####################
# Notification Type #
#####################
notificationType.error.delete_mandatory_notification=It is not possible to unsubscribe from notification {0}
#
###############
# Contributor #
###############
contributor.error.other_name_already_exists=The other name ''{0}'' already exists for this contributor
contributor.error.other_name_identical_to_contributor=The name ''{0}'' is identical to the contributor''s one
#
##########
# Person #
##########
person.error.orcidTokenMustBeSetToEnableSynchronization=You must authorize Archive ouverte to access your ORCID account to activate synchronization. To do this, click on the “world map” icon to the far right of your ORCID number.
person.error.syncToOrcidAvailableToUnigeMembersOnly=Syncronization to ORCID is only available to UNIGE members. If you have an UNIGE account in addition to your HUG account, please contact us at {0}.
#
########
# Misc #
########
validation.resource.alreadyexist=This resource already exists
validation.resource.undeletable=The resource identified by {0} is not deletable
validation.rockUnigePersonSearch.firstname.minimum2chars=The firstname must contain a minimum of 2 characters to perform the search
validation.rockUnigePersonSearch.lastname.minimum2chars=The lastname must contain a minimum of 2 characters to perform the search
scheduledTask.error.invalid_cron_expression=''{0}'' is not a valid cron expression
globalBanner.error.conflict=The gobal banner ''{0}'' is in conflict with the submitted banner
globalBanner.error.nameAlreadyExist=This name already exists
globalBanner.error.dateStartAfterDateEnd=The start date must be before the end date
#
#################################
# Checking Metadata differences #
#################################
title=Title
type=Type
types=Types
subType=SubType
subSubType=SubSubType
content=Content
lang=Language
languages=Languages
languageDeleted=Language {1} at previous position {0} has been deleted
languageAdded=Language {1} at current position {0} has been added
languageChangeOrder=Some languages have changed order
noLanguage=No language
date=Date
publication=Publication date
first_online=First online release date
imprimatur=Imprimatur date
defense=Defense date
publisherVersionUrl=Publisher Version Url
note=Note
originalTitle=Original title
originalTitleLang=Language original title
localNumber=Local Number
abstract=Abstract
abstractDeleted=Abstract at previous position {0} with content {1} has been deleted
abstractAdded=Abstract at current position {0} with content {1} has been added
abstractChangeOrder=Some abstracts have changed order
eng=English
fre=French
ger=German
ara=Arabic
chi=Chinese
spa=Spanish
gre=Greek
ita=Italian
jpn=Japanese
dut=Dutch
por=Portuguese
roh=Romanian
rus=Russian
tur=Turkish
mis=Other
doi=DOI
pmid=PMID
issn=ISSN
isbn=ISBN
arxiv=ARXIV
mmsid=MMSID
repec=REPEC
dbpl=DBPL
urn=URN
pmcid=PMCID
doctor=Doctor
email=Email
address=Address
pages=Pagination
paging=Pagination
pagingOther=Article/Thesis/Report number
corrections=Corrections
correction=Correction
correctionDeleted=Correction at previous position {0} with note {1}, doi {2} and pmid {3} has been deleted
correctionAdded=Correction at current position {0} with note {1}, doi {2} and pmid {3} has been added
correctionsChangeOrder=Some corrections have changed order
fundings=Fundings
funding=Funding
funder=Funder
fundingDeleted=Funding at previous position {0} named ''{1}'' hsa been deleted
fundingAdded=Funding ''{1}'' at current position {0} has been added
fundingsChangeOrder=Some fundings have changed order
name=Name
acronym=Acronym
code=code
identifiers=Identifiers
datasets=Datasets
dataset=Dataset
datasetDeleted=Dataset {1} at previous position {0} has been deleted
datasetAdded=Dataset {1} at current position {0} has been added
datasetsChangeOrder=Some datasets have changed order
keywords=Keywords
keyword=Keyword
keywordDeleted=Keyword {1} at previous position {0} has been deleted
keywordAdded=Keyword {1} at current position {0} has been added
keywordsChangeOrder=Some keywords have changed order
classifications=Classifications
classification=Classification
classificationDeleted=Classification at previous position {0} with item {1} and code {2} has been deleted
classificationAdded=Classification at current position {0} with item {1} and code {2} has been added
classificationsChangeOrder=Some classifications have changed order
item=Item
discipline=Discipline
mandator=Mandator
edition=Edition
award=Award
files=Files
file=File
fileDeleted=File at previous position {0} with name {1} has been deleted
fileAdded=File at current position {0} with name {1} has been added
filesChangeOrder=Some files have changed order
mimeType=MimeType
accessLevel=AccessLevel
label=Label
links=Links
link=Link
linkDeleted=Link at previous position {0} with language {1} and content {2} has been deleted
linkAdded=Link at current position {0} with language {1} and content {2} has been added
linksChangeOrder=Some links have changed order
target=Target
description.lang=Description Lang
description.content=Description Content
collections=Collections
collection=Collection
collectionNumber=Number in the collection
collectionDeleted=Collection at previous position {0} with name {1} and number {2} has been deleted
collectionAdded=Collection at current position {0} with name {1} and number {2} has been added
collectionsChangeOrder=Some collections have changed order
aouCollection=Unige Collection
publisher=Publisher
publisherName=Publisher name
publisherPlace=Publisher place
groups=Groups
group=Group
groupDeleted=Group at previous position {0} with name {1} has been deleted
groupAdded=Group at current position {0} with name {1} has been added
groupsChangeOrder=Some groups have changed order
structures=Structures
structure=Structure
structureDeleted=Structure at previous position {0} with name {1} has been deleted
structureAdded=Structure at current position {0} with name {1}  has been added
structuresChangeOrder=Some structures have changed order
conferencePlace=Conference Place
conferenceDate=Conference Date
conferenceSubtitle=Conference Subtitle
specialIssue=Special Issue
issue=Issue
volume=Volume
editor=Editor
container=Container
embargoAccessLevel=Embargo Access Level
embargoEndDate=Embargo End Date
collaborator=Collaborator
collaboration=Collaboration
contributors=Contributors
contributor=Contributor
contributorDeleted=Contributor ''{0} {1}'' has been deleted
contributorAdded=Contributor ''{0} {1}'' has been added
contributorsChangeOrder=Some contributors have changed order
collaboratorDeleted=Collaborator ''{0} {1}'' has been deleted
collaboratorAdded=Collaborator ''{0} {1}'' has been added
collaboratorsChangeOrder=Some collaborators have changed order
collaborationDeleted=Collaboration with name {1} has been deleted
collaborationAdded=Collaboration with name {1} has been added
collaborationsChangeOrder=Some collaboration have changed order
firstname=Firstname
lastname=Lastname
institution=Institution
role=Role
orcid=Orcid
cnIndividu=cnIndividu
size=Size
license=License
#
################
# Bibliography #
################
bibliography.access_level=[&nbsp;{0} on {1}&nbsp;]
bibliography.empty_access_level=[&nbsp;{0}&nbsp;]
bibliography.public_access=Public access
bibliography.restricted_access=Restricted access
bibliography.closed_access=Closed access
bibliography.open_access=(Open Access)
bibliography.embargoed_open_access=(embargoed Open Access)
bibliography.thesis_supervisions=Thesis supervisions
bibliography.master_of_advanced_thesis_supervisions=Master of advanced studies supervisions
bibliography.master_thesis_supervisions=Master thesis supervisions
bibliography.formatting_error=Unable to format text according to requested format
bibliography.subsubtype.a1-article.plural=Articles
bibliography.subsubtype.a1-article-de-donnees.plural=Data articles
bibliography.subsubtype.a1-article-video.plural=Video articles
bibliography.subsubtype.a1-commentaire.plural=Comments
bibliography.subsubtype.a1-compte-rendu-de-livre.plural=Book reviews
bibliography.subsubtype.a1-editorial.plural=Editorials
bibliography.subsubtype.a1-lettre.plural=Letters
bibliography.subsubtype.a1-meta-analyse.plural=Meta-analysis
bibliography.subsubtype.a1-rapport-de-cas.plural=Case reports
bibliography.subsubtype.a1-revue-de-la-litterature.plural=Reviews
bibliography.subsubtype.a2-article.plural=Articles
bibliography.subsubtype.a2-billet-de-blog.plural=Blog entries
bibliography.subsubtype.other.plural=Miscellaneous
bibliography.subtype.a1.plural=Scientific articles
bibliography.subtype.a2.plural=Professional articles
bibliography.subtype.a3.plural=Newspaper articles
bibliography.subtype.c1.plural=Conference proceedings
bibliography.subtype.c2.plural=Conference presentations
bibliography.subtype.c3.plural=Posters
bibliography.subtype.c4.plural=Proceedings chapters
bibliography.subtype.d1.plural=Doctoral thesis
bibliography.subtype.d2.plural=Masters of advanced studies
bibliography.subtype.d3.plural=Masters
bibliography.subtype.d4.plural=Privat-docent thesis
bibliography.subtype.d5.plural=Doctoral thesis of advanced professional studies (DAPS)
bibliography.subtype.j1.plural=Journal issues
bibliography.subtype.l1.plural=Books
bibliography.subtype.l2.plural=Edited books
bibliography.subtype.l3.plural=Book chapters
bibliography.subtype.l5.plural=Dictionary / Encyclopedia articles
bibliography.subtype.r1.plural=Reports
bibliography.subtype.r2.plural=Technical reports
bibliography.subtype.r3.plural=Working papers
bibliography.subtype.r4.plural=Preprints
bibliography.format.unige_long=University of Geneva
bibliography.format.unige_droit=University of Geneva - Law
bibliography.format.unige_long_with_note=University of Geneva - [with notes]
bibliography.format.unige_american_chemical_society=American Chemical Society
bibliography.format.apa=American Psychological Association
bibliography.format.chicago_fullnote_bibliography=Chicago (Full Note with Bibliography)
bibliography.format.chicago_author_date=Chicago (Author-Date format)
bibliography.format.unige_nlm=National Library of Medicine
bibliography.format.us_geological_survey=U.S. Geological Survey
bibliography.format.unige_vancouver_brackets_no_et_al=Vancouver - All Authors
bibliography.format.unige_vancouver=Vancouver
###################
# Contact message #
###################
contact_message.sender_name_missing=The sender's name cannot be empty
contact_message.sender_email_missing=The sender's email cannot be empty
contact_message.recipient_missing=The recipient cannot be empty
contact_message.empty_message=The message cannot be empty
