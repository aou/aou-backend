/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Common - AouProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.unit.DataSize;

import jakarta.annotation.PostConstruct;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.index.IndexDefinition;
import ch.unige.solidify.model.index.IndexDefinitionList;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.search.AdvancedSearchCriteria;
import ch.unige.aou.model.settings.StaticPage;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;

@Component
@ConfigurationProperties(prefix = "aou")
public class AouProperties {
  private static final Logger log = LoggerFactory.getLogger(AouProperties.class);

  private static final String TMP = "tmp";

  private SolidifyProperties appConfig;

  public AouProperties(SolidifyProperties appConfig) {
    this.appConfig = appConfig;
  }

  // Working folders for the applications
  public enum WorkingDirectory {
    PUBLICATIONS("publications"), LOG("log"), THUMBNAILS("thumbnails"), BIBLIOGRAPHIES("bibliographies"), EXPORT_DATA("export");

    private final String name;

    WorkingDirectory(String name) {
      this.name = name;
    }

    public String getName() {
      return this.name;
    }
  }

  public enum SendMode {
    SFTP("sftp"), HTTP("http");

    private final String name;

    SendMode(String name) {
      this.name = name;
    }

    public String getName() {
      return this.name;
    }
  }

  public SolidifyProperties getAppConfig() {
    return this.appConfig;
  }

  @PostConstruct
  private void checkConfiguration() {
    // Use a temporary directory if aou.home is not defined
    if (this.home.isEmpty()) {
      try {
        FileAttribute<Set<PosixFilePermission>> attr = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwx------"));
        this.home = Files.createTempDirectory("aou", attr).toString();
        log.warn("aou.home not defined, using the temporary directory {}", this.home);
      } catch (IOException e) {
        log.error("Impossible to create temporary directory");
      }
    }

    // Create all needed directories
    for (WorkingDirectory d : WorkingDirectory.values()) {
      Path p = Paths.get(this.home, d.getName());
      try {
        // Create directory
        Files.createDirectories(p);
        // Create temporary directory
        Files.createDirectories(p.resolve(TMP));
      } catch (IOException e) {
        log.error("The directory " + p + " cannot be created", e);
      }
    }

    if (this.getModule().getAdmin().isEnable()) {
      this.needsToBeSet(this.getMetadata().getImports().getArxiv().getUrl(), "metadata.imports.arxiv.url");
      this.needsToBeSet(this.getMetadata().getImports().getDataCite().getUrl(), "metadata.imports.datacite.url");
      this.needsToBeSet(this.getMetadata().getImports().getCrossref().getUrl(), "metadata.imports.crossref.url");
      this.needsToBeSet(this.getMetadata().getImports().getCrossref().getPid(), "metadata.imports.crossref.pid");
      this.needsToBeSet(this.getMetadata().getImports().getCrossref().getUrl(), "metadata.imports.inspirehep.url");
      this.needsToBeSet(this.getMetadata().getImports().getEuropePmc().getUrl(), "aou.metadata.imports.europe-pmc.url");
      this.needsToBeSet(this.getMetadata().getImports().getEuropePmc().getSupplementaryFiles().getUrl(),
              "aou.metadata.imports.europe-pmc.supplementary-files.url");
      this.needsToBeSet(this.getMetadata().getImports().getPmid().getUrl(), "metadata.imports.pmid.url");
      this.needsToBeSet(this.getMetadata().getImports().getUnpaywall().getUrl(), "metadata.imports.unpaywall.url");
      this.needsToBeSet(this.getMetadata().getImports().getUnpaywall().getEmail(), "metadata.imports.unpaywall.email");

      this.needsToBeSet(this.getMetadata().getSearch().getJournal().getUrl(), "metadata.search.journal.url");
      this.needsToBeSet(this.getMetadata().getSearch().getJournal().getUser(), "metadata.search.journal.user");
      this.needsToBeSet(this.getMetadata().getSearch().getJournal().getApiKey(), "metadata.search.journal.api-key");
      this.needsToBeSet(this.getMetadata().getSearch().getOpenAire().getUrl(), "metadata.search.open-aire.url");
      this.needsToBeSet(this.getMetadata().getSearch().getRomeo().getUrl(), "metadata.search.romeo.url");
      this.needsToBeSet(this.getMetadata().getSearch().getRomeo().getApiKey(), "metadata.search.romeo.api-key");

      this.needsToBeSet(this.getMetadata().getSearch().getPersonUnige().getUrl(), "aou.metadata.search.personUnige.url");
      this.needsToBeSet(this.getMetadata().getSearch().getPersonUnige().getTokenUrl(), "aou.metadata.search.personUnige.tokenUrl");
      this.needsToBeSet(this.getMetadata().getSearch().getPersonUnige().getClientId(), "aou.metadata.search.personUnige.clientId");
      this.needsToBeSet(this.getMetadata().getSearch().getPersonUnige().getUsername(), "aou.metadata.search.personUnige.username");
      this.needsToBeSet(this.getMetadata().getSearch().getPersonUnige().getPassword(), "aou.metadata.search.personUnige.password");
      this.needsToBeSet(this.getThesisNfsSharePath(), "aou.thesisNfsSharePath");
    }
  }

  // ****************
  // ** Properties **
  // ****************
  private String home = "";
  private Data data = new Data();
  private Wait wait = new Wait();
  private Queue queue = new Queue();
  private Topic topic = new Topic();
  private Http http = new Http();
  private Module module = new Module();
  private Parameters parameters = new Parameters();
  private Storage storage = new Storage();
  private Indexing indexing = new Indexing();
  private Bibliography bibliography = new Bibliography();
  private Test test = new Test();
  private Metadata metadata = new Metadata();
  private SwissNationalLibrary swissNationalLibrary = new SwissNationalLibrary();
  private List<ScheduledTaskConfig> scheduledTaskConfigs = new ArrayList<>();

  private BatchCorrections batchCorrections = new BatchCorrections();

  private Orcid orcid = new Orcid();
  private int delaySinceLastCheck = 3;
  private String thesisNfsSharePath = "";
  private String adminEmail = "archive-ouverte@unige.ch";
  private long maxPercentageDiskSpaceUsage = 90;

  // *************************
  // ** Embedded Properties **
  // *************************

  // Global Parameters
  public static class Parameters {
    private String[] checksumList = { "MD5", "SHA1", "SHA256" };
    private String defaultChecksum = "SHA256";
    private String defaultLicense = "CC-BY-4.0";
    private String metsIdPrefix = "_";
    private int authorizedUnitsCacheTime = 60;
    private int archiveRatingCacheTime = 60;
    // duration of bibliographies content cache in seconds
    private int bibliographiesCacheDuration = 3600;

    private int daysToKeepPublicationsInEditionWithoutChanges = 2;

    private DataSize fileSizeLimit = DataSize.parse("4GB");
    private int fileNameLengthLimit = 35;
    private long defaultAsyncExecutionTimeout = 0;
    private int maxPreservationPolicyRetentionPeriodInYears = 100;

    private boolean generateAgreement = false;

    // need to specify the same order for an specific type of format for metadata and frontend
    private String[] metadataDateFormat = { "yyyy-MM-dd", "yyyy-MM", "yyyy" };
    private String[] metadataOutputDateFormat = { "yyyy-MM-dd", "yyyy-MM", "yyyy" };
    private String[] frontendEnterDateFormat = { "d.M.yyyy", "M.yyyy",
            "yyyy" }; // format in which a date can be entered on frontend (1 digit allowed for day and month)
    private String[] frontendOutputDateFormat = { "dd.MM.yyyy", "MM.yyyy",
            "yyyy" }; // format to which a date is given back to frontend (2 digits for day and month)

    private String emailsPrefix;
    private String emailTemplatePath;
    private String homepage = "https://archive-ouverte.unige.ch";

    private String publicationUrlTemplate = "https://archive-ouverte.unige.ch/{archiveId}";

    private String downloadFileUrlTemplate = "https://access.archive-ouverte.unige.ch/access/metadata/{documentFileId}/download";
    private String exportDataUrlTemplate = "https://admin.archive-ouverte.unige.ch/admin/export-metadata/{id}/download";

    private String[] emailsToSendGeneralNotifications = {};
    private int daysSinceLastCheckForNewResearchGroups = 3;

    private boolean emailOnNotificationTypesSubscriptionChangeEnabled = false;

    private int schedulerThreadPoolAdditionalSize = 10;

    // Name of the director of the DIS
    private String disDirector = "Marie Fuselier, Directrice de la Division de l'information scientifique";

    private String[] authorizedInstitutions = { "unige.ch", "hcuge.ch" };

    private String[] hiddenNonPublicDocumentSubtypes = { AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME,
            AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME };

    private List<StaticPage> staticPages = new ArrayList<>();

    public String[] getChecksumList() {
      return this.checksumList;
    }

    public String getDefaultChecksum() {
      return this.defaultChecksum;
    }

    public String getDefaultLicense() {
      return this.defaultLicense;
    }

    public String getMetsIdPrefix() {
      return this.metsIdPrefix;
    }

    public int getArchiveRatingCacheTime() {
      return this.archiveRatingCacheTime;
    }

    public int getAuthorizedUnitsCacheTime() {
      return this.authorizedUnitsCacheTime;
    }

    public int getBibliographiesCacheDuration() {
      return this.bibliographiesCacheDuration;
    }

    public void setBibliographiesCacheDuration(int bibliographiesCacheDuration) {
      this.bibliographiesCacheDuration = bibliographiesCacheDuration;
    }

    public int getDaysToKeepPublicationsInEditionWithoutChanges() {
      return this.daysToKeepPublicationsInEditionWithoutChanges;
    }

    public void setDaysToKeepPublicationsInEditionWithoutChanges(int daysToKeepPublicationsInEditionWithoutChanges) {
      this.daysToKeepPublicationsInEditionWithoutChanges = daysToKeepPublicationsInEditionWithoutChanges;
    }

    public String[] getEmailsToSendGeneralNotifications() {
      return this.emailsToSendGeneralNotifications;
    }

    public void setEmailsToSendGeneralNotifications(String[] emailsToSendGeneralNotifications) {
      this.emailsToSendGeneralNotifications = emailsToSendGeneralNotifications;
    }

    public int getDaysSinceLastCheckForNewResearchGroups() {
      return this.daysSinceLastCheckForNewResearchGroups;
    }

    public void setDaysSinceLastCheckForNewResearchGroups(int daysSinceLastCheckForNewResearchGroups) {
      this.daysSinceLastCheckForNewResearchGroups = daysSinceLastCheckForNewResearchGroups;
    }

    public void setChecksumList(String[] checksumList) {
      this.checksumList = checksumList;
    }

    public void setDefaultChecksum(String defaultChecksum) {
      this.defaultChecksum = defaultChecksum;
    }

    public void setDefaultLicense(String defaultLicense) {
      this.defaultLicense = defaultLicense;
    }

    public void setMetsIdPrefix(String metsIdPrefix) {
      this.metsIdPrefix = metsIdPrefix;
    }

    public void setAuthorizedUnitsCacheTime(int authorizedUnitsCacheTime) {
      this.authorizedUnitsCacheTime = authorizedUnitsCacheTime;
    }

    public DataSize getFileSizeLimit() {
      return this.fileSizeLimit;
    }

    public void setFileSizeLimit(DataSize fileSizeLimit) {
      this.fileSizeLimit = fileSizeLimit;
    }

    public int getFileNameLengthLimit() {
      return this.fileNameLengthLimit;
    }

    public void setFileNameLengthLimit(int fileNameLengthLimit) {
      this.fileNameLengthLimit = fileNameLengthLimit;
    }

    public long getDefaultAsyncExecutionTimeout() {
      return this.defaultAsyncExecutionTimeout;
    }

    public void setDefaultAsyncExecutionTimeout(long defaultAsyncExecutionTimeout) {
      this.defaultAsyncExecutionTimeout = defaultAsyncExecutionTimeout;
    }

    public int getMaxPreservationPolicyRetentionPeriodInYears() {
      return this.maxPreservationPolicyRetentionPeriodInYears;
    }

    public void setMaxPreservationPolicyRetentionPeriodInYears(int maxPreservationPolicyRetentionPeriodInYears) {
      this.maxPreservationPolicyRetentionPeriodInYears = maxPreservationPolicyRetentionPeriodInYears;
    }

    public boolean isGenerateAgreement() {
      return this.generateAgreement;
    }

    public void setGenerateAgreement(boolean generateAgreement) {
      this.generateAgreement = generateAgreement;
    }

    public String[] getMetadataDateFormat() {
      return this.metadataDateFormat;
    }

    public void setMetadataDateFormat(String[] metadataDateFormat) {
      this.metadataDateFormat = metadataDateFormat;
    }

    public String[] getMetadataOutputDateFormat() {
      return this.metadataOutputDateFormat;
    }

    public void setMetadataOutputDateFormat(String[] metadataOutputDateFormat) {
      this.metadataOutputDateFormat = metadataOutputDateFormat;
    }

    public String[] getFrontendEnterDateFormat() {
      return this.frontendEnterDateFormat;
    }

    public void setFrontendEnterDateFormat(String[] frontendEnterDateFormat) {
      this.frontendEnterDateFormat = frontendEnterDateFormat;
    }

    public String[] getFrontendOutputDateFormat() {
      return this.frontendOutputDateFormat;
    }

    public void setFrontendOutputDateFormat(String[] frontendOutputDateFormat) {
      this.frontendOutputDateFormat = frontendOutputDateFormat;
    }

    public String getEmailsPrefix() {
      return this.emailsPrefix;
    }

    public void setEmailsPrefix(String emailsPrefix) {
      this.emailsPrefix = emailsPrefix;
    }

    public String getEmailTemplatePath() {
      return this.emailTemplatePath;
    }

    public void setEmailTemplatePath(String emailTemplatePath) {
      this.emailTemplatePath = emailTemplatePath;
    }

    public String getHomepage() {
      return this.homepage;
    }

    public void setHomepage(String homepage) {
      this.homepage = homepage;
    }

    public String getPublicationUrlTemplate() {
      return this.publicationUrlTemplate;
    }

    public void setPublicationUrlTemplate(String publicationUrlTemplate) {
      this.publicationUrlTemplate = publicationUrlTemplate;
    }

    public String getDownloadFileUrlTemplate() {
      return this.downloadFileUrlTemplate;
    }

    public void setDownloadFileUrlTemplate(String downloadFileUrlTemplate) {
      this.downloadFileUrlTemplate = downloadFileUrlTemplate;
    }

    public boolean isEmailOnNotificationTypesSubscriptionChangeEnabled() {
      return this.emailOnNotificationTypesSubscriptionChangeEnabled;
    }

    public void setEmailOnNotificationTypesSubscriptionChangeEnabled(boolean emailOnNotificationTypesSubscriptionChangeEnabled) {
      this.emailOnNotificationTypesSubscriptionChangeEnabled = emailOnNotificationTypesSubscriptionChangeEnabled;
    }

    public int getSchedulerThreadPoolAdditionalSize() {
      return this.schedulerThreadPoolAdditionalSize;
    }

    public void setSchedulerThreadPoolAdditionalSize(int schedulerThreadPoolAdditionalSize) {
      this.schedulerThreadPoolAdditionalSize = schedulerThreadPoolAdditionalSize;
    }

    public String getDisDirector() {
      return this.disDirector;
    }

    public void setDisDirector(String disDirector) {
      this.disDirector = disDirector;
    }

    public String[] getAuthorizedInstitutions() {
      return this.authorizedInstitutions;
    }

    public void setAuthorizedInstitutions(String[] authorizedInstitutions) {
      this.authorizedInstitutions = authorizedInstitutions;
    }

    public String[] getHiddenNonPublicDocumentSubtypes() {
      return this.hiddenNonPublicDocumentSubtypes;
    }

    public void setHiddenNonPublicDocumentSubtypes(String[] hiddenNonPublicDocumentSubtypes) {
      this.hiddenNonPublicDocumentSubtypes = hiddenNonPublicDocumentSubtypes;
    }

    public List<StaticPage> getStaticPages() {
      return this.staticPages;
    }

    public void setStaticPages(List<StaticPage> staticPages) {
      this.staticPages = staticPages;
    }

    public String getExportDataUrlTemplate() {
      return this.exportDataUrlTemplate;
    }

    public void setExportDataUrlTemplate(String exportDataUrlTemplate) {
      this.exportDataUrlTemplate = exportDataUrlTemplate;
    }
  }

  public static class FileList {
    private String[] files = {};
    private String[] puids = {};

    public FileList(String[] puids, String[] files) {
      if (puids != null && puids.length > 0) {
        this.puids = puids;
      }
      if (files != null && files.length > 0) {
        this.files = files;
      }
    }

    public String[] getFiles() {
      return this.files;
    }

    public void setFiles(String[] files) {
      this.files = files;
    }

    public String[] getPuids() {
      return this.puids;
    }

    public void setPuids(String[] puids) {
      this.puids = puids;
    }
  }

  public static class Data {
    private boolean init = true;
    private boolean initIndexes = true;

    public boolean isInit() {
      return this.init;
    }

    public void setInit(boolean init) {
      this.init = init;
    }

    public boolean isInitIndexes() {
      return this.initIndexes;
    }

    public void setInitIndexes(boolean initIndexes) {
      this.initIndexes = initIndexes;
    }
  }

  // Wait Parameter
  public static class Wait {
    private int maxTries = 10;
    private int milliseconds = 500;

    public int getMaxTries() {
      return this.maxTries;
    }

    public int getMilliseconds() {
      return this.milliseconds;
    }

    public void setMaxTries(int maxTries) {
      this.maxTries = maxTries;
    }

    public void setMilliseconds(int milliseconds) {
      this.milliseconds = milliseconds;
    }
  }

  // Queues
  public static class Queue {
    private String history = "history";
    private int nbParallelThreads = 2;
    private String documentFiles = "documentFiles";
    private String publications = "publications";
    private String publicationsIndexing = "publicationsIndexing";
    private String publicationsReindexing = "publicationsReindexing";
    private String cleanIndexes = "cleanIndexes";
    private String publicationsMetadataUpgrade = "publicationsMetadataUpgrade";
    private String emails = "emails";
    private String publicationsFromFedora = "publicationsFromFedora";
    private String publicationDownload = "publicationDownload";

    public String getPublicationDownload() {
      return this.publicationDownload;
    }

    public void setPublicationDownload(String publicationDownload) {
      this.publicationDownload = publicationDownload;
    }

    public String getPublicationsFromFedora() {
      return this.publicationsFromFedora;
    }

    public void setPublicationsFromFedora(String publicationsFromFedora) {
      this.publicationsFromFedora = publicationsFromFedora;
    }

    public String getHistory() {
      return this.history;
    }

    public void setHistory(String history) {
      this.history = history;
    }

    public int getNbParallelThreads() {
      return this.nbParallelThreads;
    }

    public void setNbParallelThreads(int nbParallelThreads) {
      this.nbParallelThreads = nbParallelThreads;
    }

    public String getDocumentFiles() {
      return this.documentFiles;
    }

    public void setDocumentFiles(String documentFiles) {
      this.documentFiles = documentFiles;
    }

    public String getPublications() {
      return this.publications;
    }

    public void setPublications(String publications) {
      this.publications = publications;
    }

    public String getPublicationsIndexing() {
      return this.publicationsIndexing;
    }

    public void setPublicationsIndexing(String publicationsIndexing) {
      this.publicationsIndexing = publicationsIndexing;
    }

    public String getPublicationsReindexing() {
      return this.publicationsReindexing;
    }

    public void setPublicationsReindexing(String publicationsReindexing) {
      this.publicationsReindexing = publicationsReindexing;
    }

    public String getCleanIndexes() {
      return this.cleanIndexes;
    }

    public void setCleanIndexes(String cleanIndexes) {
      this.cleanIndexes = cleanIndexes;
    }

    public String getPublicationsMetadataUpgrade() {
      return this.publicationsMetadataUpgrade;
    }

    public void setPublicationsMetadataUpgrade(String publicationsMetadataUpgrade) {
      this.publicationsMetadataUpgrade = publicationsMetadataUpgrade;
    }

    public String getEmails() {
      return this.emails;
    }

    public void setEmails(String emails) {
      this.emails = emails;
    }
  }

  // Topic queues
  public static class Topic {
    private String cache = "cache";
    private String publicationsUpdate = "publicationsUpdate";
    private String publicationsIndexed = "publicationsIndexed";
    private String contributorsUpdated = "contributorsUpdated";

    public String getCache() {
      return this.cache;
    }

    public void setCache(String cache) {
      this.cache = cache;
    }

    public String getPublicationsUpdate() {
      return this.publicationsUpdate;
    }

    public void setPublicationsUpdate(String publicationsUpdate) {
      this.publicationsUpdate = publicationsUpdate;
    }

    public String getPublicationsIndexed() {
      return this.publicationsIndexed;
    }

    public void setPublicationsIndexed(String publicationsIndexed) {
      this.publicationsIndexed = publicationsIndexed;
    }

    public String getContributorsUpdated() {
      return this.contributorsUpdated;
    }

    public void setContributorsUpdated(String contributorsUpdated) {
      this.contributorsUpdated = contributorsUpdated;
    }
  }

  // HTTP parameters
  public static class Http {
    private String expectedHeadersCharset = "ISO-8859-1";
    private String shibbolethHeadersCharset = "UTF-8";

    public String getExpectedHeadersCharset() {
      return this.expectedHeadersCharset;
    }

    public String getShibbolethHeadersCharset() {
      return this.shibbolethHeadersCharset;
    }

    public void setExpectedHeadersCharset(String expectedHeadersCharset) {
      this.expectedHeadersCharset = expectedHeadersCharset;
    }

    public void setShibbolethHeadersCharset(String shibbolethHeadersCharset) {
      this.shibbolethHeadersCharset = shibbolethHeadersCharset;
    }
  }

  // Modules
  public static class Module {
    private ModuleDetail admin = new ModuleDetail();
    private ModuleDetail deposit = new ModuleDetail();
    private ModuleDetail access = new ModuleDetail();
    private ModuleDetail dataMgmt = new ModuleDetail();

    public ModuleDetail getAdmin() {
      return this.admin;
    }

    public ModuleDetail getDeposit() {
      return this.deposit;
    }

    public ModuleDetail getAccess() {
      return this.access;
    }

    public void setAdmin(ModuleDetail admin) {
      this.admin = admin;
    }

    public void setDeposit(ModuleDetail deposit) {
      this.deposit = deposit;
    }

    public void setAccess(ModuleDetail access) {
      this.access = access;
    }

    public ModuleDetail getDataMgmt() {
      return this.dataMgmt;
    }

    public void setDataMgmt(ModuleDetail dataMgmt) {
      this.dataMgmt = dataMgmt;
    }
  }

  public static class ModuleDetail {
    private String url = "";
    private String publicUrl = "";
    private boolean enable = false;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getPublicUrl() {
      if (this.publicUrl.isEmpty()) {
        return this.getUrl();
      }
      return this.publicUrl;
    }

    public void setPublicUrl(String publicUrl) {
      this.publicUrl = publicUrl;
    }

    public boolean isEnable() {
      return this.enable;
    }

    public void setEnable(boolean enable) {
      this.enable = enable;
    }
  }

  public static class ModuleClusterDetail {
    private String[] urls = {};
    private String[] publicUrls = {};
    private boolean enable = false;

    public String[] getUrls() {
      return this.urls;
    }

    public void setUrls(String[] urls) {
      this.urls = urls;
    }

    public String[] getPublicUrls() {
      if (this.publicUrls.length == 0) {
        return this.getUrls();
      }
      return this.publicUrls;
    }

    public void setPublicUrls(String[] publicUrls) {
      this.publicUrls = publicUrls;
    }

    public boolean isEnable() {
      return this.enable;
    }

    public void setEnable(boolean enable) {
      this.enable = enable;
    }
  }

  // Storage configuration
  public static class Storage {
    private String url;
    private String user;
    private String password;
    private String prefix;
    private String solrUrl;

    public String getSolrUrl() {
      return this.solrUrl;
    }

    public void setSolrUrl(String solrUrl) {
      this.solrUrl = solrUrl;
    }

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getUser() {
      return this.user;
    }

    public void setUser(String user) {
      this.user = user;
    }

    public String getPassword() {
      return this.password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public String getPrefix() {
      return this.prefix;
    }

    public void setPrefix(String prefix) {
      this.prefix = prefix;
    }

  }

  // Indexing service in data mgmt
  public static class Indexing {
    private String indexName = "datamgmt";
    private String publishedPublicationsIndexName = null;
    private String inProgressPublicationsIndexName = null;
    private int maxNumberOfSecondsForPublicationIndexedMessage = 20;

    private List<IndexFieldAlias> fieldAliases = new ArrayList<>();
    private List<AdvancedSearchCriteria> advancedSearchCriteria = new ArrayList<>();
    private String[] acceptedIndexFieldNamesWithoutAlias = { AouConstants.INDEX_FIELD_ID, AouConstants.INDEX_FIELD_YEARS,
            AouConstants.INDEX_FIELD_SUBTYPE_CODE };
    private String[] fieldAliasesWithAndOperator = { AouConstants.INDEX_FIELD_ALIAS_GLOBAL_SEARCH,
            AouConstants.INDEX_FIELD_ALIAS_GLOBAL_SEARCH_WITH_FULLTEXT, AouConstants.INDEX_FIELD_ALIAS_TITLE,
            AouConstants.INDEX_FIELD_ALIAS_PUBLISHED_IN, AouConstants.INDEX_FIELD_ALIAS_ABSTRACT };
    private List<String> defaultExcludedFields = List.of(AouConstants.INDEX_FIELD_FULLTEXTS);

    public String getPublishedPublicationsIndexName() {
      if (!StringTool.isNullOrEmpty(this.publishedPublicationsIndexName)) {
        return this.publishedPublicationsIndexName;
      } else {
        return this.indexName + "-" + AouConstants.INDEX_PUBLISHED;
      }
    }

    public String getInProgressPublicationsIndexName() {
      if (!StringTool.isNullOrEmpty(this.inProgressPublicationsIndexName)) {
        return this.inProgressPublicationsIndexName;
      } else {
        return this.indexName + "-" + AouConstants.INDEX_IN_PROGRESS;
      }
    }

    public void setPublishedPublicationsIndexName(String publishedPublicationsIndexName) {
      this.publishedPublicationsIndexName = publishedPublicationsIndexName;
    }

    public void setInProgressPublicationsIndexName(String inProgressPublicationsIndexName) {
      this.inProgressPublicationsIndexName = inProgressPublicationsIndexName;
    }

    public int getMaxNumberOfSecondsForPublicationIndexedMessage() {
      return this.maxNumberOfSecondsForPublicationIndexedMessage;
    }

    public void setMaxNumberOfSecondsForPublicationIndexedMessage(int maxNumberOfSecondsForPublicationIndexedMessage) {
      this.maxNumberOfSecondsForPublicationIndexedMessage = maxNumberOfSecondsForPublicationIndexedMessage;
    }

    public void setIndexName(String indexName) {
      this.indexName = indexName;
    }

    public IndexDefinitionList getIndexDefinitionList() {
      IndexDefinitionList list = new IndexDefinitionList();
      list.getIndexList().add(new IndexDefinition(this.getPublishedPublicationsIndexName(), "publications.json", "publications-settings.json"));
      list.getIndexList().add(new IndexDefinition(this.getInProgressPublicationsIndexName(), "publications.json", "publications-settings.json"));
      return list;
    }

    public List<IndexFieldAlias> getFieldAliases() {
      return this.fieldAliases;
    }

    public void setFieldAliases(List<IndexFieldAlias> fieldAliases) {
      this.fieldAliases = fieldAliases;
    }

    public List<AdvancedSearchCriteria> getAdvancedSearchCriteria() {
      return this.advancedSearchCriteria;
    }

    public void setAdvancedSearchCriteria(List<AdvancedSearchCriteria> advancedSearchCriteria) {
      this.advancedSearchCriteria = advancedSearchCriteria;
    }

    public String[] getAcceptedIndexFieldNamesWithoutAlias() {
      return this.acceptedIndexFieldNamesWithoutAlias;
    }

    public void setAcceptedIndexFieldNamesWithoutAlias(String[] acceptedIndexFieldNamesWithoutAlias) {
      this.acceptedIndexFieldNamesWithoutAlias = acceptedIndexFieldNamesWithoutAlias;
    }

    public String[] getFieldAliasesWithAndOperator() {
      return this.fieldAliasesWithAndOperator;
    }

    public void setFieldAliasesWithAndOperator(String[] fieldAliasesWithAndOperator) {
      this.fieldAliasesWithAndOperator = fieldAliasesWithAndOperator;
    }

    public List<String> getDefaultExcludedFields() {
      return this.defaultExcludedFields;
    }

    public void setDefaultExcludedFields(List<String> defaultExcludedFields) {
      this.defaultExcludedFields = defaultExcludedFields;
    }

    public FieldsRequest getDefaultFieldsRequest() {
      final FieldsRequest fieldsRequest = new FieldsRequest();
      fieldsRequest.getExcludes().addAll(this.getDefaultExcludedFields());
      return fieldsRequest;
    }

    public FieldsRequest getDefaultFieldsRequestWithXml() {
      FieldsRequest fieldsRequest = this.getDefaultFieldsRequest();
      fieldsRequest.getExcludes().add(AouConstants.INDEX_FIELD_XML);
      return fieldsRequest;
    }
  }

  // Bibliography
  public static class Bibliography {
    private int limit = 1000;
    private String defaultFormat = "unige-long";
    private String[] customCslStylePrefixes = { "unige" };
    private String[] cslFormats = { "unige-long", "unige-droit", "unige-long-with-note", "unige-american-chemical-society", "apa",
            "chicago-fullnote-bibliography", "chicago-author-date", "unige-nlm", "us-geological-survey", "unige-vancouver-brackets-no-et-al",
            "unige-vancouver" };

    public int getLimit() {
      return this.limit;
    }

    public void setLimit(int limit) {
      this.limit = limit;
    }

    public String getDefaultFormat() {
      return this.defaultFormat;
    }

    public void setDefaultFormat(String defaultFormat) {
      this.defaultFormat = defaultFormat;
    }

    public String[] getCustomCslStylePrefixes() {
      return this.customCslStylePrefixes;
    }

    public void setCustomCslStylePrefixes(String[] customCslStylePrefixes) {
      this.customCslStylePrefixes = customCslStylePrefixes;
    }

    public String[] getCslFormats() {
      return this.cslFormats;
    }

    public void setCslFormats(String[] cslFormats) {
      this.cslFormats = cslFormats;
    }
  }

  // Test
  public static class Test {
    public static class User {
      private String name;
      private String role;

      public String getName() {
        return this.name;
      }

      public void setName(final String name) {
        this.name = name;
      }

      public String getRole() {
        return this.role;
      }

      public void setRole(final String role) {
        this.role = role;
      }

    }

    private boolean generateData = false;
    private boolean building = false;

    private List<AouProperties.Test.User> users = new ArrayList<>();

    public List<AouProperties.Test.User> getUsers() {
      return this.users;
    }

    public void setUsers(final List<AouProperties.Test.User> users) {
      this.users = users;
    }

    public boolean isGenerateData() {
      return this.generateData;
    }

    public void setGenerateData(boolean generateData) {
      this.generateData = generateData;
    }

    public void setBuilding(boolean building) {
      this.building = building;
    }

    public boolean isBuilding() {
      return this.building;
    }

  }

  // File Format service
  public static class FileFormat {
    private String tool = "";
    private String[] defaultTools = { "Droid", "Jhove" };

    public String getTool() {
      return this.tool;
    }

    public void setTool(String tool) {
      this.tool = tool;
    }

    public String[] getDefaultTools() {
      return this.defaultTools;
    }

    public void setDefaultTools(String[] defaultTools) {
      this.defaultTools = defaultTools;
    }

  }

  // Virus check service
  public static class VirusCheck {
    private String tool = "";
    private int timeout = 60000;

    public String getTool() {
      return this.tool;
    }

    public int getTimeout() {
      return this.timeout;
    }

    public void setTool(String tool) {
      this.tool = tool;
    }

    public void setTimeout(int timeout) {
      this.timeout = timeout;
    }
  }

  public static class Metadata {
    private Imports imports = new Imports();
    private Search search = new Search();
    private Default defaults = new Default();
    private Validation validation = new Validation();
    private SwissNationalLibrary swissNationalLibrary = new SwissNationalLibrary();
    private String unigeDoiPrefix = "10.13097/archive-ouverte/";

    public Imports getImports() {
      return this.imports;
    }

    public void setImports(final Imports imports) {
      this.imports = imports;
    }

    public Search getSearch() {
      return this.search;
    }

    public void setSearch(Search search) {
      this.search = search;
    }

    public Default getDefault() {
      return this.defaults;
    }

    public void setDefault(Default defaults) {
      this.defaults = defaults;
    }

    public Validation getValidation() {
      return this.validation;
    }

    public void setValidation(Validation validation) {
      this.validation = validation;
    }

    public SwissNationalLibrary getSwissNationalLibrary() {
      return this.swissNationalLibrary;
    }

    public void setSwissNationalLibrary(SwissNationalLibrary swissNationalLibrary) {
      this.swissNationalLibrary = swissNationalLibrary;
    }

    public String getUnigeDoiPrefix() {
      return this.unigeDoiPrefix;
    }

    public void setUnigeDoiPrefix(String unigeDoiPrefix) {
      this.unigeDoiPrefix = unigeDoiPrefix;
    }
  }

  public static class Default {

    // @formatter:off
    private Map<String, String> contributorRolesByDepositSubtype = Map.ofEntries(
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID,                   AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_ID,                  AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_ID,                          AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_ID,                    AuthorRole.EDITOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_ID,              AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_POSTER_ID,                                 AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_ID,                       AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_THESE_ID,                                  AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_ID,               AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_MASTER_ID,                                 AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_ID,                 AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_ID,                        AuthorRole.EDITOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_LIVRE_ID,                                  AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_ID,                      AuthorRole.EDITOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_ID,                      AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_ID, AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_ID,                   AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_ID,                      AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_ID,                          AuthorRole.AUTHOR.value()),
            Map.entry(AouConstants.DEPOSIT_SUBTYPE_PREPRINT_ID,                               AuthorRole.AUTHOR.value())
    );
    // @formatter:on

    public Map<String, String> getContributorRolesByDepositSubtype() {
      return this.contributorRolesByDepositSubtype;
    }
  }

  public static class Validation {
    // default structureId = 2 which is the 'Faculté des sciences' (defined in file 'structures.json' used to init database)
    private String[] structureIdsRequiringPublicationModeForThesis = { "2" };

    private int maxSizeForAbstract = 3500;

    public int getMaxSizeForAbstract() {
      return this.maxSizeForAbstract;
    }

    public void setMaxSizeForAbstract(int maxSizeForAbstract) {
      this.maxSizeForAbstract = maxSizeForAbstract;
    }

    public String[] getStructureIdsRequiringPublicationModeForThesis() {
      return this.structureIdsRequiringPublicationModeForThesis;
    }

    public void setStructureIdsRequiringPublicationModeForThesis(String[] structureIdsRequiringPublicationModeForThesis) {
      this.structureIdsRequiringPublicationModeForThesis = structureIdsRequiringPublicationModeForThesis;
    }
  }

  public static class Imports {
    private Crossref crossref = new Crossref();
    private DataCite dataCite = new DataCite();
    private Arxiv arxiv = new Arxiv();
    private Pmid pmid = new Pmid();
    private Pmc pmc = new Pmc();
    private EuropePmc europePmc = new EuropePmc();
    private Unpaywall unpaywall = new Unpaywall();
    private InspireHep inspireHep = new InspireHep();
    private Isbn isbn = new Isbn();
    private int limitMaxContributors = 100;

    // Regexes of known urls to downloads pdfs that do not work and are thus ignored when found in imported metadata
    private String[] ignoredFilesSourceUrlPatterns = { "^https?://onlinelibrary.wiley.com/doi/pdf(direct)?/.*" };

    public Crossref getCrossref() {
      return this.crossref;
    }

    public void setCrossref(Crossref crossref) {
      this.crossref = crossref;
    }

    public DataCite getDataCite() {
      return this.dataCite;
    }

    public void setDataCite(DataCite dataCite) {
      this.dataCite = dataCite;
    }

    public Arxiv getArxiv() {
      return this.arxiv;
    }

    public void setArxiv(Arxiv arxiv) {
      this.arxiv = arxiv;
    }

    public Pmid getPmid() {
      return this.pmid;
    }

    public void setPmid(Pmid pmid) {
      this.pmid = pmid;
    }

    public Pmc getPmc() {
      return this.pmc;
    }

    public void setPmc(Pmc pmc) {
      this.pmc = pmc;
    }

    public EuropePmc getEuropePmc() {
      return this.europePmc;
    }

    public void setEuropePmc(EuropePmc europePmc) {
      this.europePmc = europePmc;
    }

    public Unpaywall getUnpaywall() {
      return this.unpaywall;
    }

    public void setUnpaywall(Unpaywall unpaywall) {
      this.unpaywall = unpaywall;
    }

    public InspireHep getInspireHep() {
      return this.inspireHep;
    }

    public void setInspireHep(InspireHep inspireHep) {
      this.inspireHep = inspireHep;
    }

    public Isbn getIsbn() {
      return this.isbn;
    }

    public void setIsbn(Isbn isbn) {
      this.isbn = isbn;
    }

    public int getLimitMaxContributors() {
      return this.limitMaxContributors;
    }

    public void setLimitMaxContributors(final int limitMaxContributors) {
      this.limitMaxContributors = limitMaxContributors;
    }

    public String[] getIgnoredFilesSourceUrlPatterns() {
      return this.ignoredFilesSourceUrlPatterns;
    }

    public void setIgnoredFilesSourceUrlPatterns(String[] ignoredFilesSourceUrlPatterns) {
      this.ignoredFilesSourceUrlPatterns = ignoredFilesSourceUrlPatterns;
    }
  }

  public static class Crossref {
    private String url = "http://doi.crossref.org/servlet/query?pid={pid}&format=UNIXREF&id={doi}";
    private String pid;
    private String[] textsToRemoveFromAbstracts = { "<title>ABSTRACT</title>" };

    private Map<String, String> textsToReplaceInAbstracts = Map.ofEntries(Map.entry("<italic>", "<i>"), Map.entry("</italic>", "</i>"));

    private String preprintNoteDepositedInText = "Déposé dans: %s";

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getPid() {
      return this.pid;
    }

    public void setPid(String pid) {
      this.pid = pid;
    }

    public String[] getTextsToRemoveFromAbstracts() {
      return this.textsToRemoveFromAbstracts;
    }

    public void setTextsToRemoveFromAbstracts(String[] textsToRemoveFromAbstracts) {
      this.textsToRemoveFromAbstracts = textsToRemoveFromAbstracts;
    }

    public Map<String, String> getTextsToReplaceInAbstracts() {
      return this.textsToReplaceInAbstracts;
    }

    public void setTextsToReplaceInAbstracts(Map<String, String> textsToReplaceInAbstracts) {
      this.textsToReplaceInAbstracts = textsToReplaceInAbstracts;
    }

    public String getPreprintNoteDepositedInText() {
      return this.preprintNoteDepositedInText;
    }

    public void setPreprintNoteDepositedInText(String preprintNoteDepositedInText) {
      this.preprintNoteDepositedInText = preprintNoteDepositedInText;
    }
  }

  public static class DataCite {
    private String url = "https://api.datacite.org/dois/{doi}";

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }
  }

  public static class Arxiv {
    private String url = "http://export.arxiv.org/api/query?id_list={arxivId}";

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }
  }

  public static class Pmid {
    private String url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.cgi?db=pubmed&retmode=xml&id={pmid}";
    private String doiUrl = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term={doi}";

    private Integer millisecondsBetweenPubmedImports = 400;

    public String getDoiUrl() {
      return this.doiUrl;
    }

    public void setDoiUrl(String doiUrl) {
      this.doiUrl = doiUrl;
    }

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public Integer getMillisecondsBetweenPubmedImports() {
      return this.millisecondsBetweenPubmedImports;
    }

    public void setMillisecondsBetweenPubmedImports(Integer millisecondsBetweenPubmedImports) {
      this.millisecondsBetweenPubmedImports = millisecondsBetweenPubmedImports;
    }
  }

  public static class Pmc {
    private String url;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }
  }

  public static class EuropePmc {
    private String url = "https://www.ebi.ac.uk/europepmc/webservices/rest/search?query={pmcId}&resultType=core&cursorMark=*&pageSize=25&format=xml";
    private SupplementaryFiles supplementaryFiles = new SupplementaryFiles();

    public SupplementaryFiles getSupplementaryFiles() {
      return this.supplementaryFiles;
    }

    public void setSupplementaryFiles(SupplementaryFiles supplementaryFiles) {
      this.supplementaryFiles = supplementaryFiles;
    }

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }
  }

  public static class Unpaywall {
    private String url = "https://api.unpaywall.org/v2/{doi}?email={email}";
    private String email;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getEmail() {
      return this.email;
    }

    public void setEmail(String email) {
      this.email = email;
    }
  }

  public static class InspireHep {
    private String arxivUrl = "https://inspirehep.net/api/arxiv/{arxivId}";

    private String doiUrl = "https://inspirehep.net/api/doi/{doi}";

    public String getDoiUrl() {
      return this.doiUrl;
    }

    public void setDoiUrl(String doiUrl) {
      this.doiUrl = doiUrl;
    }

    public String getArxivUrl() {
      return this.arxivUrl;
    }

    public void setArxivUrl(String arxivUrl) {
      this.arxivUrl = arxivUrl;
    }
  }

  public static class SupplementaryFiles {
    private String url = "https://www.ebi.ac.uk/europepmc/webservices/rest/{pmcId}/supplementaryFiles?includeInlineImage=true";

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }
  }

  public static class Isbn {
    private String googleUrl = "https://www.googleapis.com/books/v1/volumes?q=isbn:{id}";
    private String swisscoveryUrl = "https://slsp-network.alma.exlibrisgroup.com/view/sru/41SLSP_NETWORK?version=1.2&operation=searchRetrieve&recordSchema=marcxml&query=alma.isbn={id}";
    private String congressLibraryUrl = "https://www.loc.gov/search/?all=True&fo=json&q={id}&st=list";

    public String getGoogleUrl() {
      return this.googleUrl;
    }

    public void setGoogleUrl(String googleUrl) {
      this.googleUrl = googleUrl;
    }

    public String getSwisscoveryUrl() {
      return this.swisscoveryUrl;
    }

    public void setSwisscoveryUrl(String swisscoveryUrl) {
      this.swisscoveryUrl = swisscoveryUrl;
    }

    public String getCongressLibraryUrl() {
      return this.congressLibraryUrl;
    }

    public void setCongressLibraryUrl(String congressLibraryUrl) {
      this.congressLibraryUrl = congressLibraryUrl;
    }

  }

  public static class SwissNationalLibrary {
    private FtpServer ftpServer = new FtpServer();
    private String urnPrefix = "urn:nbn:ch:";
    private int minimumYearForUrn = 2002;
    private SendMode sendMode = SendMode.HTTP;

    public String getUrnPrefix() {
      return this.urnPrefix;
    }

    public void setUrnPrefix(String urnPrefix) {
      this.urnPrefix = urnPrefix;
    }

    public int getMinimumYearForUrn() {
      return this.minimumYearForUrn;
    }

    public void setMinimumYearForUrn(int minimumYearForUrn) {
      this.minimumYearForUrn = minimumYearForUrn;
    }

    public FtpServer getFtpServer() {
      return this.ftpServer;
    }

    public void setFtpServer(FtpServer ftpServer) {
      this.ftpServer = ftpServer;
    }

    public SendMode getSendMode() {
      return this.sendMode;
    }

    public void setSendMode(SendMode sendMode) {
      this.sendMode = sendMode;
    }
  }

  public static class FtpServer {
    private String host = "";
    private String path = "";
    private String user = "";
    private String password = "";

    public String getHost() {
      return this.host;
    }

    public void setHost(String host) {
      this.host = host;
    }

    public String getPath() {
      return this.path;
    }

    public void setPath(String path) {
      this.path = path;
    }

    public String getUser() {
      return this.user;
    }

    public void setUser(String user) {
      this.user = user;
    }

    public String getPassword() {
      return this.password;
    }

    public void setPassword(String password) {
      this.password = password;
    }
  }

  public static class Search {
    private Person person = new Person();
    private PersonUnige personUnige = new PersonUnige();
    private ResearchGroup researchGroup = new ResearchGroup();
    private Journal journal = new Journal();
    private Romeo romeo = new Romeo();
    private OpenAire openAire = new OpenAire();
    private Duplicates duplicates = new Duplicates();

    public Person getPerson() {
      return this.person;
    }

    public void setPerson(Person person) {
      this.person = person;
    }

    public PersonUnige getPersonUnige() {
      return this.personUnige;
    }

    public void setPersonUnige(PersonUnige personUnige) {
      this.personUnige = personUnige;
    }

    public ResearchGroup getResearchGroup() {
      return this.researchGroup;
    }

    public void setResearchGroup(ResearchGroup researchGroup) {
      this.researchGroup = researchGroup;
    }

    public Journal getJournal() {
      return this.journal;
    }

    public void setJournal(Journal journal) {
      this.journal = journal;
    }

    public Romeo getRomeo() {
      return this.romeo;
    }

    public void setRomeo(Romeo romeo) {
      this.romeo = romeo;
    }

    public OpenAire getOpenAire() {
      return this.openAire;
    }

    public void setOpenAire(OpenAire openAire) {
      this.openAire = openAire;
    }

    public Duplicates getDuplicates() {
      return this.duplicates;
    }

    public void setDuplicates(Duplicates duplicates) {
      this.duplicates = duplicates;
    }
  }

  public static class Person {
    private String url = "https://archive-ouverte.unige.ch/search-person";
    private String apiKey;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getApiKey() {
      return this.apiKey;
    }

    public void setApiKey(String apiKey) {
      this.apiKey = apiKey;
    }
  }

  public static class PersonUnige {
    private String url;
    private String tokenUrl;
    private String clientId;
    private String password;
    private String username;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getTokenUrl() {
      return this.tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
      this.tokenUrl = tokenUrl;
    }

    public String getClientId() {
      return this.clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public String getPassword() {
      return this.password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public String getUsername() {
      return this.username;
    }

    public void setUsername(String username) {
      this.username = username;
    }
  }

  public static class ResearchGroup {
    private String url = "https://archive-ouverte.unige.ch/search-person";
    private String apiKey;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getApiKey() {
      return this.apiKey;
    }

    public void setApiKey(String apiKey) {
      this.apiKey = apiKey;
    }
  }

  public static class Journal {
    private String url = "https://api.issn.org";
    private String user;
    private String apiKey;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getUser() {
      return this.user;
    }

    public void setUser(String user) {
      this.user = user;
    }

    public String getApiKey() {
      return this.apiKey;
    }

    public void setApiKey(String apiKey) {
      this.apiKey = apiKey;
    }
  }

  public static class Romeo {
    private String url = "https://v2.sherpa.ac.uk/cgi/retrieve";
    private String apiKey;

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getApiKey() {
      return this.apiKey;
    }

    public void setApiKey(String apiKey) {
      this.apiKey = apiKey;
    }
  }

  public static class OpenAire {
    private String url = "http://api.openaire.eu/search/projects";

    public String getUrl() {
      return this.url;
    }

    public void setUrl(String url) {
      this.url = url;
    }
  }

  public static class Duplicates {
    private String identifiersCheckUrl = "https://archive-ouverte.unige.ch/documents/check_exists?{identifierType}={value}";
    private String identifiersCheckUrlWithSubType = "https://archive-ouverte.unige.ch/documents/check_exists?{identifierType}={value}&subtype={subtypeValue}";
    private boolean checkForDuplicates = true;

    public String getIdentifiersCheckUrlWithSubType() {
      return this.identifiersCheckUrlWithSubType;
    }

    public void setIdentifiersCheckUrlWithSubType(String identifiersCheckUrlWithSubType) {
      this.identifiersCheckUrlWithSubType = identifiersCheckUrlWithSubType;
    }

    public String getIdentifiersCheckUrl() {
      return this.identifiersCheckUrl;
    }

    public void setIdentifiersCheckUrl(String identifiersCheckUrl) {
      this.identifiersCheckUrl = identifiersCheckUrl;
    }

    public boolean isCheckForDuplicates() {
      return this.checkForDuplicates;
    }

    public void setCheckForDuplicates(boolean checkForDuplicates) {
      this.checkForDuplicates = checkForDuplicates;
    }
  }

  public static class ScheduledTaskConfig {
    private String id;
    private String type;
    private String name;
    private String cronExpression;
    private boolean enabled = true;

    public String getId() {
      return this.id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getType() {
      return this.type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getCronExpression() {
      return this.cronExpression;
    }

    public void setCronExpression(String cronExpression) {
      this.cronExpression = cronExpression;
    }

    public boolean isEnabled() {
      return this.enabled;
    }

    public void setEnabled(boolean enabled) {
      this.enabled = enabled;
    }
  }

  public static class BatchCorrections {
    private String[] emailsToSendNotifications = {};
    private int numberOfErrorToSendForEmail = 10;

    public int getNumberOfErrorToSendForEmail() {
      return this.numberOfErrorToSendForEmail;
    }

    public void setNumberOfErrorToSendForEmail(int numberOfErrorToSendForEmail) {
      this.numberOfErrorToSendForEmail = numberOfErrorToSendForEmail;
    }

    public String[] getEmailsToSendNotifications() {
      return this.emailsToSendNotifications;
    }

    public void setEmailsToSendNotifications(String[] emailsToSendNotifications) {
      this.emailsToSendNotifications = emailsToSendNotifications;
    }
  }

  public static class Orcid {
    private String[] rolesToSendToOrcidProfile = { AouConstants.CONTRIBUTOR_ROLE_AUTHOR, AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR,
            AouConstants.CONTRIBUTOR_ROLE_EDITOR, AouConstants.CONTRIBUTOR_ROLE_GUEST_EDITOR, AouConstants.CONTRIBUTOR_ROLE_PHOTOGRAPHER };

    public String[] getRolesToSendToOrcidProfile() {
      return this.rolesToSendToOrcidProfile;
    }

    public void setRolesToSendToOrcidProfile(String[] rolesToSendToOrcidProfile) {
      this.rolesToSendToOrcidProfile = rolesToSendToOrcidProfile;
    }
  }

  public Data getData() {
    return this.data;
  }

  public Indexing getIndexing() {
    return this.indexing;
  }

  public void setIndexing(Indexing indexing) {
    this.indexing = indexing;
  }

  public Bibliography getBibliography() {
    return this.bibliography;
  }

  public void setBibliography(Bibliography bibliography) {
    this.bibliography = bibliography;
  }

  public Wait getWait() {
    return this.wait;
  }

  public Queue getQueue() {
    return this.queue;
  }

  public Topic getTopic() {
    return this.topic;
  }

  public Http getHttp() {
    return this.http;
  }

  public Module getModule() {
    return this.module;
  }

  public Parameters getParameters() {
    return this.parameters;
  }

  public BatchCorrections getBatchCorrections() {
    return this.batchCorrections;
  }

  public void setHome(String home) {
    this.home = home;
  }

  public void setData(Data data) {
    this.data = data;
  }

  public void setWait(Wait wait) {
    this.wait = wait;
  }

  public void setQueue(Queue queue) {
    this.queue = queue;
  }

  public void setTopic(Topic topic) {
    this.topic = topic;
  }

  public void setHttp(Http http) {
    this.http = http;
  }

  public void setModule(Module module) {
    this.module = module;
  }

  public void setParameters(Parameters parameters) {
    this.parameters = parameters;
  }

  public void setBatchCorrections(BatchCorrections batchCorrections) {
    this.batchCorrections = batchCorrections;
  }

  public void setThesisNfsSharePath(String path) {
    this.thesisNfsSharePath = path;
  }

  public void setMaxPercentageDiskSpaceUsage(long maxPercentageDiskSpaceUsage) {
    this.maxPercentageDiskSpaceUsage = maxPercentageDiskSpaceUsage;
  }

  public String getHome() {
    return this.home;
  }

  public String getPublicationsFolder() {
    return this.getHome() + "/" + WorkingDirectory.PUBLICATIONS.getName();
  }

  public String getExportPublicationsFolder() {
    return this.getHome() + "/" + WorkingDirectory.EXPORT_DATA.getName();
  }

  public String getLogsFolder() {
    return this.getHome() + "/" + WorkingDirectory.LOG.getName();
  }

  public String getBibliographiesFolder() {
    return this.getHome() + "/" + WorkingDirectory.BIBLIOGRAPHIES.getName();
  }

  public String getTempLocation(String moduleLocation) {
    return moduleLocation + "/" + TMP;
  }

  public Test getTest() {
    return this.test;
  }

  public void setTest(Test test) {
    this.test = test;
  }

  public Metadata getMetadata() {
    return this.metadata;
  }

  public void setMetadata(Metadata metadata) {
    this.metadata = metadata;
  }

  public SwissNationalLibrary getSwissNationalLibrary() {
    return this.swissNationalLibrary;
  }

  public void setSwissNationalLibrary(SwissNationalLibrary swissNationalLibrary) {
    this.swissNationalLibrary = swissNationalLibrary;
  }

  public Storage getStorage() {
    return this.storage;
  }

  public void setStorage(Storage storage) {
    this.storage = storage;
  }

  public List<ScheduledTaskConfig> getScheduledTasks() {
    return this.scheduledTaskConfigs;
  }

  public int getDelaySinceLastCheck() {
    return this.delaySinceLastCheck;
  }

  public void setDelaySinceLastCheck(int delay) {
    this.delaySinceLastCheck = delay;
  }

  public String getThesisNfsSharePath() {
    return this.thesisNfsSharePath;
  }

  public String getAdminEmail() {
    return this.adminEmail;
  }

  public void setAdminEmail(String adminEmail) {
    this.adminEmail = adminEmail;
  }

  public long getMaxPercentageDiskSpaceUsage() {
    return this.maxPercentageDiskSpaceUsage;
  }

  public void setScheduledTasks(List<ScheduledTaskConfig> scheduledTaskConfigs) {
    this.scheduledTaskConfigs = scheduledTaskConfigs;
  }

  public Orcid getOrcid() {
    return this.orcid;
  }

  public void setOrcid(Orcid orcid) {
    this.orcid = orcid;
  }

  private void needsToBeSet(String value, String propertyName) {
    if (StringTool.isNullOrEmpty(value)) {
      throw new IllegalStateException("Property '" + propertyName + "' must be set in config");
    }
  }
}
