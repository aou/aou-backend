/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Common - AouRuntimeException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.exception;

import java.time.LocalDateTime;
import java.util.Map;

import org.springframework.http.HttpStatus;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class AouRuntimeException extends SolidifyRuntimeException {
  private static final long serialVersionUID = -4223693183183404502L;

  private int code;
  private final AouError aouError = new AouError();

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public AouRuntimeException(HttpStatus httpStatus, String message, int code, Map<Object, Object> params) {
    super(message);
    this.code = code;
    this.aouError.setError(httpStatus.toString());
    this.aouError.setMessage(message);
    this.aouError.setStatus(httpStatus);
    this.aouError.setTimeStamp(LocalDateTime.now().toString());
    this.aouError.setAdditionalParameters(params);
    this.aouError.setErrorCode(code);
  }

  public AouError getAouError() {
    return this.aouError;
  }

}
