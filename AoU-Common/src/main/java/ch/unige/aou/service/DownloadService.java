/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Common - DownloadService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.exception.AouDownloadException;

@Service
public class DownloadService {

  private static final int MAX_REDIRECT = 10;

  public void download(URI uri, Path path) {
    try {
      final WebClient client = WebClient.create();
      ResponseEntity<Void> responseEntity;
      URI location = uri;
      Map<String, String> cookies = new HashMap<>();
      int nbRedirect = 0;

      // Follow redirects
      do {
        responseEntity = this.getClientResponse(client, location, cookies);
        if (responseEntity == null) {
          throw new SolidifyRuntimeException("Null responseEntity when downloading " + uri + " to " + path);
        }
        URI newLocation = responseEntity.getHeaders().getLocation();
        if (newLocation != null) {
          location = newLocation;
        }
        List<String> receivedCookies = responseEntity.getHeaders().get("Set-Cookie");
        if (receivedCookies != null) {
          for (String cookieToMerge : receivedCookies) {
            final String cookieNameValue = cookieToMerge.substring(0, cookieToMerge.indexOf(';'));
            final String cookieName = cookieNameValue.substring(0, cookieNameValue.indexOf('='));
            final String cookieValue = cookieNameValue.substring(cookieNameValue.indexOf('=') + 1);
            cookies.put(cookieName, cookieValue);
          }
        }
        nbRedirect++;
      } while (responseEntity.getStatusCode().is3xxRedirection() && nbRedirect < MAX_REDIRECT);

      // Trigger download
      Flux<DataBuffer> dataBufferFlux = client.get()
              .uri(location)
              .headers(this.getCookieConsumer(cookies))
              .retrieve()
              .bodyToFlux(DataBuffer.class);
      DataBufferUtils.write(dataBufferFlux, path, StandardOpenOption.CREATE).block();
    } catch (Exception e) {
      throw new AouDownloadException("Impossible to download file from " + uri.toString());
    }
  }

  private ResponseEntity<Void> getClientResponse(WebClient client, URI uri, Map<String, String> cookies) {
    return client.get()
            .uri(uri)
            .headers(this.getCookieConsumer(cookies))
            .retrieve()
            .onStatus(status -> true, response -> Mono.empty())
            .toBodilessEntity()
            .block();
  }

  private String getCookieString(Map<String, String> cookies) {
    StringBuilder cookieString = new StringBuilder();
    for (Map.Entry<String, String> cookieNameValue : cookies.entrySet()) {
      cookieString.append(cookieNameValue.getKey())
              .append("=")
              .append(cookieNameValue.getValue())
              .append("; ");
    }
    return cookieString.toString();
  }

  private Consumer<HttpHeaders> getCookieConsumer(Map<String, String> cookies) {
    return headers -> {
      final String cookieString = this.getCookieString(cookies);
      if (!StringTool.isNullOrEmpty(cookieString)) {
        headers.set("Cookie", cookieString);
      }
    };
  }

}
