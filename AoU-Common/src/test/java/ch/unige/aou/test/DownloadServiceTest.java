/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Common - DownloadServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import ch.unige.solidify.util.FileTool;

import ch.unige.aou.service.DownloadService;

public class DownloadServiceTest {

  private final static String TEST_URL = "https://link.springer.com/content/pdf/10.1186/s12909-020-1954-7.pdf";

  @Disabled
  @Test
  void testDownload() throws URISyntaxException {
    URI uri = new URI(TEST_URL);
    Path downloadPath = Paths.get(System.getProperty("user.home") + "/test-download.pdf");
    DownloadService downloadService = new DownloadService();
    downloadService.download(uri, downloadPath);
    assertTrue(FileTool.checkFile(downloadPath));
  }

}
