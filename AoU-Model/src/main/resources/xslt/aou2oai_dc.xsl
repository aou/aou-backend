<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:aou="https://archive-ouverte.unige.ch/deposit/v2.4"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                exclude-result-prefixes="aou">

    <xsl:output method="xml" omit-xml-declaration="yes" indent="yes"
                encoding="UTF-8"/>

    <xsl:param name="archiveUrl"/>
    <xsl:param name="resId"/>
    <xsl:param name="oaiId"/>

    <xsl:strip-space elements="*"/>

    <xsl:template match="aou:title">
        <xsl:element name="dc:title">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="aou:type">
        <xsl:element name="dc:type">
            <xsl:value-of select="."/>
        </xsl:element>
        <xsl:choose>
            <xsl:when test=".='Article'">
                <xsl:element name="dc:type">info:eu-repo/semantics/article</xsl:element>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="aou:subtype">
        <xsl:element name="dc:type">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="fileTypes">
        <xsl:for-each select="//aou:files/aou:file">
            <xsl:choose>
                <xsl:when test="contains(./aou:type,'Published version')">
                    <xsl:element name="dc:type">info:eu-repo/semantics/publishedVersion</xsl:element>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:contributors">
        <xsl:for-each select="aou:contributor">
            <xsl:choose>
                <xsl:when test="./aou:role='author'">
                    <xsl:element name="dc:creator"><xsl:value-of select="./aou:lastname"/>, <xsl:value-of select="./aou:firstname"/>
                    </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:element name="dc:contributor"><xsl:value-of select="./aou:lastname"/>, <xsl:value-of select="./aou:firstname"/>
                    </xsl:element>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:classifications">
        <xsl:for-each select="aou:classification[aou:code='Dewey']">
            <xsl:element name="dc:subject">info:eu-repo/classification/ddc/<xsl:value-of select="aou:item"/></xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:keywords">
        <xsl:for-each select="aou:keyword">
            <xsl:element name="dc:subject">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:abstracts">
        <xsl:for-each select="aou:abstract">
            <xsl:element name="dc:description">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:dates">
        <xsl:for-each select="aou:date">
            <xsl:choose>
                <xsl:when test="@type='publication'">
                    <xsl:element name="dc:date">
                        <xsl:value-of select="."/>
                    </xsl:element>
                    <xsl:call-template name="year">
                        <xsl:with-param name="date" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="@type='first_online'">
                    <xsl:element name="dc:date">
                        <xsl:value-of select="."/>
                    </xsl:element>
                    <xsl:call-template name="year">
                        <xsl:with-param name="date" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="@type='imprimatur'">
                    <xsl:element name="dc:date">
                        <xsl:value-of select="."/>
                    </xsl:element>
                    <xsl:call-template name="year">
                        <xsl:with-param name="date" select="."/>
                    </xsl:call-template>
                </xsl:when>
                <xsl:when test="@type='defense'">
                    <xsl:element name="dc:date">
                        <xsl:value-of select="."/>
                    </xsl:element>
                    <xsl:call-template name="year">
                        <xsl:with-param name="date" select="."/>
                    </xsl:call-template>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:languages">
        <xsl:for-each select="aou:language">
            <xsl:element name="dc:language">
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:identifiers">
        <xsl:for-each select="node()">
            <xsl:if test="name(.) != 'doi' and name(.) != 'pmid' and name(.) != 'issn'">
                <xsl:element name="dc:identifier">
                    <xsl:value-of select="name(.)"/><xsl:text>:</xsl:text><xsl:value-of select="."/>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="relations">
        <xsl:for-each select="//aou:identifiers/node()">
            <xsl:choose>
                <xsl:when test="name(.)='doi'">
                    <xsl:element name="dc:relation">info:eu-repo/semantics/altIdentifier/doi/<xsl:value-of select="."/></xsl:element>
                </xsl:when>
                <xsl:when test="name(.)='pmid'">
                    <xsl:element name="dc:relation">info:eu-repo/semantics/altIdentifier/pmid/<xsl:value-of select="."/></xsl:element>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="source">
        <xsl:for-each select="//aou:identifiers/node()">
            <xsl:choose>
                <xsl:when test="name(.)='issn'">
                    <xsl:element name="dc:source">ISSN: <xsl:value-of select="."/></xsl:element>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>
        <xsl:if test="//aou:container/aou:title">
            <xsl:element name="dc:source"><xsl:value-of select="//aou:container/aou:title"/></xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match="aou:files">
        <xsl:for-each select="./aou:file">
            <xsl:choose>
                <xsl:when test="contains(./aou:type,'Published')">
                    <xsl:element name="dc:rights">
                        <xsl:value-of select="./aou:accessLevel"/>
                    </xsl:element>
                    <xsl:if test="./aou:license">
                        <xsl:element name="dc:rights">
                            <xsl:value-of select="./aou:license"/>
                        </xsl:element>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="contains(./aou:type,'Accepted')">
                    <xsl:element name="dc:rights">
                        <xsl:value-of select="./aou:accessLevel"/>
                    </xsl:element>
                    <xsl:if test="./aou:license">
                        <xsl:element name="dc:rights">
                            <xsl:value-of select="./aou:license"/>
                        </xsl:element>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="aou:additionalMetadata/aou:diffusion">
        <xsl:element name="dc:rights">info:eu-repo/semantics/<xsl:value-of select="."/></xsl:element>
    </xsl:template>

    <xsl:template match="aou:deposit_doc">
        <xsl:element name="oai_dc:dc">
            <xsl:attribute name="xsi:schemaLocation">http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd
            </xsl:attribute>
            <xsl:namespace name="dc">http://purl.org/dc/elements/1.1/</xsl:namespace>
            <xsl:apply-templates select="aou:title"/>
            <xsl:apply-templates select="aou:type"/>
            <xsl:call-template name="fileTypes"/>
            <xsl:apply-templates select="aou:subtype"/>
            <xsl:apply-templates select="aou:contributors"/>
            <xsl:apply-templates select="aou:classifications"/>
            <xsl:apply-templates select="aou:keywords"/>
            <xsl:apply-templates select="aou:abstracts"/>
            <xsl:apply-templates select="aou:dates"/>
            <xsl:apply-templates select="aou:languages"/>
            <xsl:element name="dc:identifier">
                <xsl:value-of select="$oaiId"/>
            </xsl:element>
            <xsl:element name="dc:identifier">
                <xsl:value-of select="$archiveUrl"/><xsl:value-of select="$oaiId"/>
            </xsl:element>
            <xsl:apply-templates select="aou:identifiers"/>
            <xsl:call-template name="source"/>
            <xsl:call-template name="relations"/>
            <xsl:apply-templates select="aou:additionalMetadata/aou:diffusion"/>
            <!-- <xsl:apply-templates select="aou:files"/> -->
        </xsl:element>
    </xsl:template>

    <xsl:template name="node_to_string">
        <xsl:param name="delimiter" select="' '"/>
        <xsl:for-each select="descendant::text()">
            <xsl:value-of select="string( . )"/>
            <xsl:if test="position() != last()">
                <xsl:value-of select="$delimiter"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="year">
        <xsl:param name="date"/>
        <xsl:if test="string-length($date) > 4">
            <xsl:element name="dc:date">
                <xsl:value-of select="substring($date, 1, 4)"/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>