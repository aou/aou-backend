<?xml version="1.0"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:aou_deposit="https://archive-ouverte.unige.ch/deposit/v2.4"
        exclude-result-prefixes="aou_deposit">

    <xsl:output encoding="utf-8" method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="position_ids"></xsl:param>

    <xsl:template match="/">
        <admin>
            <applications>
                <application>archive_ouverte</application>
            </applications>
            <deposit>
                <xsl:apply-templates select="aou_deposit:deposit_doc"/>
            </deposit>
        </admin>
    </xsl:template>

    <xsl:template match="aou_deposit:deposit_doc">

        <xsl:choose>
            <xsl:when test="aou_deposit:depositor_email">
                <SubmissionEmail>
                    <xsl:value-of select="aou_deposit:depositor_email"/>
                </SubmissionEmail>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="aou_deposit:depositor_last_name">
                <SubmissionName>
                    <xsl:value-of select="aou_deposit:depositor_last_name"/>
                </SubmissionName>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="aou_deposit:depositor_first_name">
                <SubmissionFirstname>
                    <xsl:value-of select="aou_deposit:depositor_first_name"/>
                </SubmissionFirstname>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="aou_deposit:depositor_unique_id">
                <SubmissionCN>
                    <xsl:value-of select="aou_deposit:depositor_unique_id"/>
                </SubmissionCN>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="aou_deposit:validation_date">
                <SubmissionDate>
                    <xsl:value-of select="aou_deposit:validation_date"/>
                </SubmissionDate>
                <ValidationDate>
                    <xsl:value-of select="aou_deposit:validation_date"/>
                </ValidationDate>
            </xsl:when>
        </xsl:choose>

        <xsl:choose>
            <xsl:when test="aou_deposit:creation_date">
                <CreationDate>
                    <xsl:value-of select="aou_deposit:creation_date"/>
                </CreationDate>
            </xsl:when>
        </xsl:choose>

        <xsl:for-each select="./aou_deposit:files/aou_deposit:file">
            <xsl:variable name="id" select="position()"/>
            <Attachment>
                <xsl:choose>
                    <xsl:when test="tokenize($position_ids,';')[position() = $id] &lt; 10">
                        <Datastream_ID>ATTACHMENT0<xsl:number value="tokenize($position_ids,';')[position() = $id]"/></Datastream_ID>
                    </xsl:when>
                    <xsl:otherwise>
                        <Datastream_ID>ATTACHMENT<xsl:number value="tokenize($position_ids,';')[position() = $id]"/></Datastream_ID>
                    </xsl:otherwise>
                </xsl:choose>
                <FileName>
                    <xsl:value-of select="aou_deposit:name"/>
                </FileName>
                <Size>
                    <xsl:value-of select="aou_deposit:size"/>
                </Size>
                <Checksum>
                    <xsl:value-of select="aou_deposit:checksum"/>
                </Checksum>
                <Description>
                    <xsl:value-of select="aou_deposit:type"/>
                </Description>
                <DescriptionComplement>
                    <xsl:value-of select="aou_deposit:label"/>
                </DescriptionComplement>
                <Licence>
                    <xsl:value-of select="aou_deposit:license"/>
                </Licence>
                <Availability>
                    <xsl:apply-templates select="aou_deposit:accessLevel"/>
                </Availability>

                <xsl:apply-templates select="aou_deposit:embargo"/>

            </Attachment>
        </xsl:for-each>

    </xsl:template>

    <xsl:template match="aou_deposit:embargo">
        <Embargo>
            <xsl:apply-templates select="aou_deposit:accessLevel"/>
        </Embargo>
        <Period>
            <xsl:value-of select="aou_deposit:duration_in_months"/>
        </Period>
        <EmbargoEndDate>
            <xsl:value-of select="aou_deposit:end_date"/>
        </EmbargoEndDate>
    </xsl:template>

    <xsl:template match="aou_deposit:accessLevel">
        <xsl:choose>
            <xsl:when test=". = 'Public'">Internet</xsl:when>
            <xsl:when test=". = 'Restricted'">Intranet</xsl:when>
            <xsl:when test=". = 'Private'">Non diffusé</xsl:when>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
