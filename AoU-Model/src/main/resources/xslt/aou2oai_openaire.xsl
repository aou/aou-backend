<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
 version="2.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:aou="https://archive-ouverte.unige.ch/deposit/v2.4"
 xmlns:aou_deposit="https://archive-ouverte.unige.ch/deposit/v2.4"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns:dcterms="http://purl.org/dc/terms/"
 xmlns:datacite="http://datacite.org/schema/kernel-4"
 xmlns:oaire="http://namespace.openaire.eu/schema/oaire/"
 exclude-result-prefixes="aou">

	<xsl:output method="xml" omit-xml-declaration="yes" indent="yes" encoding="UTF-8" />

	<xsl:param name="archiveUrl" />
	<xsl:param name="resId" />
	<xsl:param name="oaiId" />

	<xsl:strip-space elements="*" />

    <xsl:template match="/">
			<datacite:resource>
				<xsl:attribute name="xsi:schemaLocation">http://namespace.openaire.eu/schema/oaire/ https://www.openaire.eu/schema/repo-lit/4.0/openaire.xsd</xsl:attribute>
				<xsl:namespace name="rdf">http://www.w3.org/1999/02/22-rdf-syntax-ns#</xsl:namespace>
				<xsl:namespace name="xsi">http://www.w3.org/2001/XMLSchema-instance</xsl:namespace>
				<xsl:namespace name="dc">http://purl.org/dc/elements/1.1/</xsl:namespace>
				<xsl:namespace name="dcterms">http://purl.org/dc/terms/</xsl:namespace>
				<xsl:namespace name="datacite">http://datacite.org/schema/kernel-4</xsl:namespace>
				<xsl:namespace name="oaire">http://namespace.openaire.eu/schema/oaire/</xsl:namespace>
					<xsl:apply-templates select="aou_deposit:deposit_doc"/>
			</datacite:resource>
    </xsl:template>

    <xsl:template match="aou_deposit:deposit_doc">
			<xsl:variable name="view">
				<xsl:choose>
					<xsl:when test="aou_deposit:type = 'Article'">article</xsl:when>
					<xsl:when test="aou_deposit:type = 'Conférence'">conference</xsl:when>
					<xsl:when test="aou_deposit:type = 'Diplôme'">diplome</xsl:when>
					<xsl:when test="aou_deposit:type = 'Journal'">journal</xsl:when>
					<xsl:when test="aou_deposit:type = 'Livre'">livre</xsl:when>
					<xsl:when test="aou_deposit:type = 'Rapport'">rapport</xsl:when>
					<xsl:otherwise>DocumentClassNotFoundException</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="subview">
				<xsl:choose>
					<xsl:when test="aou_deposit:subtype = 'Actes de conférence'">actes</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Article professionnel'">art_pro</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Article scientifique'">art_sci</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Autre article'">art_aut</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Chapitre d''actes'">chap_act</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Chapitre de livre'">chap_liv</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Contribution à un dictionnaire / une encyclopédie'">contrib</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Livre'">livre</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Master d''études avancées'">mea</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Master'">master</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Numéro de revue'">numero</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Ouvrage collectif'">liv_coll</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Poster'">poster</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Preprint'">prep</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Présentation / Intervention'">present</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Rapport de recherche'">rap_rec</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Rapport technique'">rap_tec</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Thèse de privat-docent'">priv_doc</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Thèse professionnelle'">th_pro</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Thèse'">these</xsl:when>
					<xsl:when test="aou_deposit:subtype = 'Working paper'">wp</xsl:when>
					<xsl:when test="starts-with(aou_deposit:subtype, 'Chapitre')">chapitres</xsl:when>
					<xsl:otherwise>DocumentClassNotFoundException</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="subarticle">
				<xsl:choose>
					<xsl:when test="aou_deposit:subsubtype = 'Article de données'">datapaper</xsl:when>
					<xsl:when test="aou_deposit:subsubtype = 'Article'">article</xsl:when>
					<xsl:when test="aou_deposit:subsubtype = 'Compte rendu de livre'">bookrev</xsl:when>
					<xsl:when test="aou_deposit:subsubtype = 'Editorial'">edito</xsl:when>
					<xsl:when test="aou_deposit:subsubtype = 'Lettre'">lettre</xsl:when>
					<xsl:otherwise>DocumentClassNotFoundException</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="base_date">
				<xsl:choose>
					<xsl:when test="//aou_deposit:date[@type = 'imprimatur']"><xsl:value-of select="//aou_deposit:date[@type = 'imprimatur']"/></xsl:when>
					<xsl:when test="//aou_deposit:date[@type = 'defense']"><xsl:value-of select="//aou_deposit:date[@type = 'defense']"/></xsl:when>
					<xsl:when test="//aou_deposit:date[@type = 'publication']"><xsl:value-of select="//aou_deposit:date[@type = 'publication']"/></xsl:when>
					<xsl:when test="//aou_deposit:date[@type = 'first_online']"><xsl:value-of select="//aou_deposit:date[@type = 'first_online']"/></xsl:when>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="base_year" select="substring($base_date, 1, 4)"/>

			<!-- 1 Title -->
			<xsl:apply-templates select="aou:title" />
			<!-- 2-3. Creator & Contributor -->
			<xsl:apply-templates select="aou:contributors" />
			<!-- 4. Funding Reference -->
				<!-- Attention - pour les financements FNS ont pourrait rajouter le DOI et l'URL du projet -->
			<xsl:if test="aou_deposit:fundings/aou_deposit:funding">
				<oaire:fundingReferences>
					<xsl:for-each select="aou_deposit:fundings/aou_deposit:funding[aou_deposit:funder!='']">
						<oaire:fundingReference>
							<oaire:funderName><xsl:value-of select="./aou_deposit:funder"/></oaire:funderName>
							<xsl:if test="./aou_deposit:program != ''">
								<oaire:fundingStream><xsl:value-of select="./aou_deposit:program"/></oaire:fundingStream>
							</xsl:if>
							<xsl:if test="./aou_deposit:code != ''">
								<oaire:awardNumber><xsl:value-of select="./aou_deposit:code"/></oaire:awardNumber>
							</xsl:if>
							<xsl:if test="./aou_deposit:name != ''">
								<oaire:awardTitle><xsl:value-of select="./aou_deposit:name"/></oaire:awardTitle>
							</xsl:if>
						</oaire:fundingReference>
					</xsl:for-each>
				</oaire:fundingReferences>
			</xsl:if>
			<!-- 5. Alternate Identifier -->
			<xsl:apply-templates select="aou:identifiers"/>
			<!-- 6. Related Identifier -->
				<!-- Not used for the moment -->
			<!-- 7. Embargo Period Date -->
				<!-- <xsl:if test="$diffusion = 'embargoedAccess'">
					<datacite:date dateType="Accepted">
						<xsl:value-of select="$embargoEndDate"/>  indiquer la date de validation 
					</datacite:date>
					<datacite:date dateType="Available">
						<xsl:value-of select="$embargoEndDate"/>
					</datacite:date>
				</xsl:if> -->

			<!-- 8. Language -->
			<xsl:apply-templates select="aou:languages"/>
			<!-- 9. Publisher -->
			<xsl:choose>
				<xsl:when test="aou:type = 'Diplôme' and aou:isBeforeUnige = 'false'">
					<xsl:element name="dc:publisher">Université de Genève</xsl:element>
				</xsl:when>
				<xsl:when test="aou:publisher/aou:name != ''">
					<xsl:element name="dc:publisher"><xsl:value-of select="aou:publisher/aou:name"/></xsl:element>
				</xsl:when>
				<xsl:when test="aou:mandator != '' and (aou:type = 'Rapport de recherche' or aou:type = 'Rapport technique')">
					<xsl:element name="dc:publisher"><xsl:value-of select="aou:mandator"/></xsl:element>
				</xsl:when>
				<xsl:when test="contains(aou:identifiers/aou:doi,'10.13097/') and (aou:type = 'Rapport de recherche' or aou:type = 'Rapport technique')"><br/>
					<xsl:element name="dc:publisher">Université de Genève</xsl:element>
				</xsl:when>
			</xsl:choose>

			<!-- 10. Publication Date -->
			<datacite:dates>
				<datacite:date dateType="Issue">
					<xsl:value-of select="$base_date"/>
				</datacite:date>
			</datacite:dates>

			<!-- 14. Resource Identifier -->
			<xsl:element name="datacite:identifier">
				<xsl:attribute name="identifierType">Handle</xsl:attribute>
				<xsl:value-of select="$archiveUrl"/><xsl:value-of select="$oaiId"/>
			</xsl:element>

			<xsl:apply-templates select="aou:subtype" />

			<xsl:apply-templates select="aou:abstracts"/>
			<xsl:call-template name="fileTypes"/>
			<!-- <xsl:apply-templates select="aou:classifications"/> -->
			<!-- <xsl:apply-templates select="aou:keywords"/> -->
			<!-- <xsl:apply-templates select="aou:dates"/> -->
			<!-- <xsl:call-template name="source"/> -->
			<!-- <xsl:call-template name="relations"/> -->
			<!-- <xsl:apply-templates select="aou:additionalMetadata/aou:diffusion"/> -->
			<!-- <xsl:apply-templates select="aou:files"/> -->
	</xsl:template>

	<!-- 1 Title -->
	<xsl:template match="aou:title">
		<datacite:title>
			<xsl:if test="./@lang != ''">
				<xsl:attribute name="xml:lang">
					<xsl:value-of select="./@lang" />
				</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="." />
		</datacite:title>
		<xsl:if test="../aou:original_title != ''">
			<datacite:title titleType="AlternativeTitle">
				<xsl:attribute name="xml:lang">
					<xsl:value-of select="../aou:original_title/@lang" />
				</xsl:attribute>
				<xsl:value-of select="../aou:original_title" />
			</datacite:title>
		</xsl:if>
	</xsl:template>

	<!-- 2-3. Creators & Contributors -->
	<xsl:template match="aou:contributors">
		<datacite:creators>
			<xsl:for-each select="aou:contributor">
				<xsl:choose>
					<xsl:when test="./aou:role = 'author'">
						<datacite:creator>
							<datacite:creatorName nameType="Personal">
								<xsl:value-of select="./aou:lastname"/>
								<xsl:if test="aou:firstname != ''">, <xsl:value-of select="./aou:firstname"/></xsl:if>
							</datacite:creatorName>
							<datacite:familyName><xsl:value-of select="./aou:lastname"/></datacite:familyName>
							<!-- Test sur la présence du prénom -->
							<xsl:if test="aou:firstname != ''">
								<datacite:givenName><xsl:value-of select="./aou:firstname"/></datacite:givenName>
							</xsl:if>
							<xsl:if test="aou:orcid != ''">
								<datacite:nameIdentifier nameIdentifierScheme="ORCID" schemeURI="http://orcid.org">
									<xsl:value-of select="./aou:orcid"/>
								</datacite:nameIdentifier>
							</xsl:if>
						</datacite:creator>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
			<xsl:for-each select="aou:collaboration">
				<datacite:creator>
					<datacite:creatorName nameType="Organizational">
						<xsl:value-of select="./aou:name"/>
					</datacite:creatorName>
				</datacite:creator>
			</xsl:for-each>
		</datacite:creators>
		<!-- Attention - Tester s'il y a des contributors au préalable
		                 Les personnes avec le rôle de collaborator sont ignorées pour le moment -->
		<datacite:contributors>
			<xsl:for-each select="aou:contributor">
				<xsl:choose>
					<xsl:when test="aou:role = 'editor' or aou:role = 'guest editor'">
						<datacite:contributor contributorType="Editor">
							<datacite:contributorName nameType="Personal">
								<xsl:value-of select="./aou:lastname"/>
								<xsl:if test="aou:firstname != ''">, <xsl:value-of select="./aou:firstname"/></xsl:if>
							</datacite:contributorName>
							<datacite:familyName><xsl:value-of select="./aou:lastname"/></datacite:familyName>
							<!-- Test sur la présence du prénom -->
							<xsl:if test="aou:firstname != ''">
								<datacite:givenName><xsl:value-of select="./aou:firstname"/></datacite:givenName>
							</xsl:if>
							<xsl:if test="aou:orcid != ''">
								<datacite:nameIdentifier nameIdentifierScheme="ORCID" schemeURI="http://orcid.org">
									<xsl:value-of select="./aou:orcid"/>
								</datacite:nameIdentifier>
							</xsl:if>
						</datacite:contributor>
					</xsl:when>
					<xsl:when test="aou:role = 'director'">
						<datacite:contributor contributorType="Supervisor">
							<datacite:contributorName nameType="Personal">
								<xsl:value-of select="./aou:lastname"/>
								<xsl:if test="aou:firstname != ''">, <xsl:value-of select="./aou:firstname"/></xsl:if>
							</datacite:contributorName>
							<datacite:familyName><xsl:value-of select="./aou:lastname"/></datacite:familyName>
							<!-- Test sur la présence du prénom -->
							<xsl:if test="aou:firstname != ''">
								<datacite:givenName><xsl:value-of select="./aou:firstname"/></datacite:givenName>
							</xsl:if>
							<xsl:if test="aou:orcid != ''">
								<datacite:nameIdentifier nameIdentifierScheme="ORCID" schemeURI="http://orcid.org">
									<xsl:value-of select="./aou:orcid"/>
								</datacite:nameIdentifier>
							</xsl:if>
						</datacite:contributor>
					</xsl:when>
					<xsl:when test="aou:role = 'photographer' or aou:role = 'translator'">
						<datacite:contributor contributorType="RelatedPerson">
							<datacite:contributorName nameType="Personal">
								<xsl:value-of select="./aou:lastname"/>
								<xsl:if test="aou:firstname != ''">, <xsl:value-of select="./aou:firstname"/></xsl:if>
							</datacite:contributorName>
							<datacite:familyName><xsl:value-of select="./aou:lastname"/></datacite:familyName>
							<!-- Test sur la présence du prénom -->
							<xsl:if test="aou:firstname != ''">
								<datacite:givenName><xsl:value-of select="./aou:firstname"/></datacite:givenName>
							</xsl:if>
							<xsl:if test="aou:orcid != ''">
								<datacite:nameIdentifier nameIdentifierScheme="ORCID" schemeURI="http://orcid.org">
									<xsl:value-of select="./aou:orcid"/>
								</datacite:nameIdentifier>
							</xsl:if>
						</datacite:contributor>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
			<xsl:for-each select="../aou:mandator">
				<datacite:contributor contributorType="Sponsor">
					<datacite:contributorName nameType="Organizational">
						<xsl:value-of select="."/>
					</datacite:contributorName>
				</datacite:contributor>
			</xsl:for-each>
		</datacite:contributors>
	</xsl:template>

	<!-- 5. Alternate Identifiers -->
	<xsl:template match="aou:identifiers">
		<datacite:alternateIdentifiers>
			<!-- ISBN -->
			<xsl:if test="./aou:isbn != ''">
				<datacite:alternateIdentifier alternateIdentifierType="ISBN">
					<xsl:value-of select="//aou:isbn"/>
				</datacite:alternateIdentifier>
			</xsl:if>
			<!-- ISSN -->
			<xsl:if test="./aou:issn != ''">
				<datacite:alternateIdentifier alternateIdentifierType="ISSN">
					<xsl:value-of select="//aou:issn"/>
				</datacite:alternateIdentifier>
			</xsl:if>
			<!-- DOI -->
			<xsl:if test="./aou:doi != ''">
				<datacite:alternateIdentifier alternateIdentifierType="DOI">
					<xsl:value-of select="//aou:doi"/>
				</datacite:alternateIdentifier>
			</xsl:if>
			<!-- URN -->
			<xsl:if test="./aou:urn != ''">
				<datacite:alternateIdentifier alternateIdentifierType="URN">
					<xsl:value-of select="//aou:urn"/>
				</datacite:alternateIdentifier>
			</xsl:if>
			<!-- 024 - PMID -->
			<xsl:if test="./aou:pmid != ''">
				<datacite:alternateIdentifier alternateIdentifierType="PMID">
					<xsl:value-of select="//aou:pmid"/>
				</datacite:alternateIdentifier>
			</xsl:if>
			<!-- arxiv -->
			<xsl:if test="./aou:arxiv != ''">
				<datacite:alternateIdentifier alternateIdentifierType="arXiv">
					<xsl:value-of select="//aou:arxiv"/>
				</datacite:alternateIdentifier>
			</xsl:if>
		</datacite:alternateIdentifiers>
	</xsl:template>

	<!-- 8. Language -->
	<xsl:template match="aou:languages">
		<xsl:for-each select="./aou:language">
			<xsl:element name="dc:language">
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>

	<!-- Resource Type -->
	<xsl:template match="aou:subtype">
		<oaire:resourceType resourceTypeGeneral="literature">
			<xsl:choose>
				<xsl:when test="../aou:subsubtype = 'Article de données'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_beb9</xsl:attribute>
					<xsl:text>data paper</xsl:text>
				</xsl:when>
				<xsl:when test="../aou:subsubtype = 'Article'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_2df8fbb1</xsl:attribute>
					<xsl:text>research article</xsl:text>
				</xsl:when>
				<xsl:when test="../aou:subsubtype = 'Compte rendu de livre'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_ba08</xsl:attribute>
					<xsl:text>book review</xsl:text>
				</xsl:when>
				<xsl:when test="../aou:subsubtype = 'Editorial'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_b239</xsl:attribute>
					<xsl:text>editorial</xsl:text>
				</xsl:when>
				<xsl:when test="../aou:subsubtype = 'Lettre'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_545b</xsl:attribute>
					<xsl:text>letter to the editor</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Article scientifique'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_6501</xsl:attribute>
					<xsl:text>journal article</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Livre' or //aou:subtype = 'Ouvrage collectif'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_2f33</xsl:attribute>
					<xsl:text>book</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Chapitre de livre' or //aou:subtype = 'Contribution à un dictionnaire / une encyclopédie'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_3248</xsl:attribute>
					<xsl:text>book part</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Actes de conférence'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_f744</xsl:attribute>
					<xsl:text>conference proceedings</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Présentation / Intervention'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_18cp</xsl:attribute>
					<xsl:text>conference paper not in proceedings</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Poster'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_18co</xsl:attribute>
					<xsl:text>conference poster not in proceedings</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Chapitre d''actes'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_5794</xsl:attribute>
					<xsl:text>conference paper</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Thèse'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_db06</xsl:attribute>
					<xsl:text>doctoral thesis</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Master'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_bdcc</xsl:attribute>
					<xsl:text>master thesis</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Thèse de privat-docent' or //aou:subtype = 'Thèse professionnelle'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_46ec</xsl:attribute>
					<xsl:text>thesis</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Rapport de recherche'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_18ws</xsl:attribute>
					<xsl:text>research report</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Rapport technique'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_18gh</xsl:attribute>
					<xsl:text>technical report</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Preprint'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_816b</xsl:attribute>
					<xsl:text>preprint</xsl:text>
				</xsl:when>
				<xsl:when test="//aou:subtype = 'Working paper'">
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_8042</xsl:attribute>
					<xsl:text>working paper</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="uri">http://purl.org/coar/resource_type/c_18cf</xsl:attribute>
					<xsl:text>text</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</oaire:resourceType>
	</xsl:template>

	<!-- Abstracts / Description -->
	<xsl:template match="aou:abstracts">
		<xsl:for-each select="./aou:abstract">
			<xsl:element name="dc:description">
				<xsl:if test="./@lang != ''">
					<xsl:attribute name="xml:lang">
						<xsl:value-of select="./@lang" />
					</xsl:attribute>
				</xsl:if>
				<xsl:value-of select="."/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>



	<xsl:template name="fileTypes">
		<xsl:for-each select="//aou:files/aou:file">
			<xsl:choose>
				<xsl:when test="contains(./aou:type,'Published version')">
					<xsl:element name="dc:type">
						info:eu-repo/semantics/publishedVersion
					</xsl:element>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="source">
		<xsl:for-each select="//aou:identifiers/node()">
			<xsl:choose>
				<xsl:when test="name(.)='issn'">
					<xsl:element name="dc:source">
						ISSN:
						<xsl:value-of select="." />
					</xsl:element>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
		<xsl:if test="//aou:container/aou:title">
			<xsl:element name="dc:source">
				<xsl:value-of select="//aou:container/aou:title" />
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template name="relations">
		<xsl:for-each select="//aou:identifiers/node()">
			<xsl:choose>
				<xsl:when test="name(.)='doi'">
					<xsl:element name="dc:relation">
						info:eu-repo/semantics/altIdentifier/doi/
						<xsl:value-of select="." />
					</xsl:element>
				</xsl:when>
				<xsl:when test="name(.)='pmid'">
					<xsl:element name="dc:relation">
						info:eu-repo/semantics/altIdentifier/pmid/
						<xsl:value-of select="." />
					</xsl:element>
				</xsl:when>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>