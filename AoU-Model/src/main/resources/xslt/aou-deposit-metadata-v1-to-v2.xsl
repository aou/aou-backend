<?xml version="1.0"?>
<xsl:stylesheet
        version="1.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:aou_deposit_v1="https://archive-ouverte.unige.ch/deposit/v1"
        xmlns:aou_deposit_v2="https://archive-ouverte.unige.ch/deposit/v2"
        exclude-result-prefixes="aou_deposit_v1 aou_deposit_v2">

    <!--
    This XSL allows to transform deposit metadata from version 1 to version 2

    The differences are:

    * language becomes languages to support many values
    * deposit identifiers:
        * DOI is not multiple anymore
        * rero identifier is renamed to mmsid
        * new pmcid identifier
    * container:
        * the container doesn't have identifiers anymore
            * container DOI may be moved to deposit identifiers if deposit identifiers doesn't contain one yet
            * container ISBN may be moved to deposit identifiers if deposit identifiers doesn't contain one yet
            * container ISSN is moved to deposit identifiers
        * new conference_title tag
        * place is renamed to conference_place
    * awards is renamed to award
    * dataset is renamed to datasets
        * it now contains only url tags (no more doi)
    * dateType 'presentation' is not a valid value anymore
    * rights are removed
    * citations are removed
    * months is removed from files embargo
    * old_code is removed from structure
    * isUnige is removed
    -->

    <xsl:output encoding="utf-8" method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/aou_deposit_v1:deposit_doc">
        <deposit_doc xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <type>
                <xsl:value-of select="aou_deposit_v1:type"/>
            </type>
            <subtype>
                <xsl:value-of select="aou_deposit_v1:subtype"/>
            </subtype>
            <subsubtype>
                <xsl:value-of select="aou_deposit_v1:subsubtype"/>
            </subsubtype>

            <xsl:apply-templates select="aou_deposit_v1:title"/>
            <xsl:apply-templates select="aou_deposit_v1:language"/>
            <xsl:apply-templates select="aou_deposit_v1:original_title"/>
            <xsl:apply-templates select="aou_deposit_v1:dates"/>
            <xsl:apply-templates select="aou_deposit_v1:contributors"/>
            <xsl:apply-templates select="aou_deposit_v1:pages"/>
            <xsl:apply-templates select="aou_deposit_v1:identifiers"/>

            <publisher_version_url>
                <xsl:value-of select="aou_deposit_v1:publisher_version_url"/>
            </publisher_version_url>

            <xsl:apply-templates select="aou_deposit_v1:abstracts"/>

            <note>
                <xsl:value-of select="aou_deposit_v1:note"/>
            </note>

            <xsl:apply-templates select="aou_deposit_v1:keywords"/>
            <xsl:apply-templates select="aou_deposit_v1:classifications"/>


            <discipline>
                <xsl:value-of select="aou_deposit_v1:discipline"/>
            </discipline>
            <mandator>
                <xsl:value-of select="aou_deposit_v1:mandator"/>
            </mandator>

            <xsl:apply-templates select="aou_deposit_v1:publisher"/>
            <xsl:apply-templates select="aou_deposit_v1:collections"/>

            <edition>
                <xsl:value-of select="aou_deposit_v1:edition"/>
            </edition>

            <!-- awards is renamed to award -->
            <award>
                <xsl:value-of select="aou_deposit_v1:awards"/>
            </award>

            <xsl:apply-templates select="aou_deposit_v1:fundings"/>
            <xsl:apply-templates select="aou_deposit_v1:academic_structures"/>
            <xsl:apply-templates select="aou_deposit_v1:groups"/>
            <xsl:apply-templates select="aou_deposit_v1:dataset"/>
            <xsl:apply-templates select="aou_deposit_v1:container"/>
            <xsl:apply-templates select="aou_deposit_v1:links"/>

            <aou_collection>
                <xsl:value-of select="aou_deposit_v1:aou_collection"/>
            </aou_collection>

            <xsl:apply-templates select="aou_deposit_v1:files"/>

        </deposit_doc>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:title">
        <title xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:if test="./@lang != ''">
                <xsl:attribute name="lang">
                    <xsl:value-of select="./@lang"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="."/>
        </title>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:language">
        <languages xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <language>
                <xsl:value-of select="."/>
            </language>
        </languages>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:original_title">
        <original_title xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:attribute name="lang">
                <xsl:value-of select="./@lang"/>
            </xsl:attribute>
            <xsl:value-of select="."/>
        </original_title>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:dates">
        <dates xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:date">
                <xsl:if test="string-length(.) > 0 and . != 'presentation'">
                    <xsl:element name="date">
                        <xsl:attribute name="type">
                            <xsl:value-of select="./@type"/>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                    </xsl:element>
                </xsl:if>
            </xsl:for-each>
        </dates>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:contributors">
        <contributors xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </contributors>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:contributor">
        <contributor xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <role>
                <xsl:value-of select="./aou_deposit_v1:role"/>
            </role>
            <firstname>
                <xsl:value-of select="./aou_deposit_v1:firstname"/>
            </firstname>
            <lastname>
                <xsl:value-of select="./aou_deposit_v1:lastname"/>
            </lastname>

            <xsl:if test="string-length(./aou_deposit_v1:cn_individu) > 0">
                <cn_individu>
                    <xsl:value-of select="./aou_deposit_v1:cn_individu"/>
                </cn_individu>
            </xsl:if>

            <xsl:if test="string-length(./aou_deposit_v1:email) > 0">
                <email>
                    <xsl:value-of select="./aou_deposit_v1:email"/>
                </email>
            </xsl:if>

            <xsl:if test="string-length(./aou_deposit_v1:institution) > 0">
                <institution>
                    <xsl:value-of select="./aou_deposit_v1:institution"/>
                </institution>
            </xsl:if>

            <xsl:if test="string-length(./aou_deposit_v1:orcid) > 0">
                <orcid>
                    <xsl:value-of select="./aou_deposit_v1:orcid"/>
                </orcid>
            </xsl:if>

            <xsl:apply-templates select="aou_deposit_v1:other_names"/>

        </contributor>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:collaboration">
        <collaboration xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <name>
                <xsl:value-of select="./aou_deposit_v1:name"/>
            </name>
        </collaboration>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:other_names">
        <other_names xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:other_name">
                <other_name>
                    <firstname>
                        <xsl:value-of select="./aou_deposit_v1:firstname"/>
                    </firstname>
                    <lastname>
                        <xsl:value-of select="./aou_deposit_v1:lastname"/>
                    </lastname>
                </other_name>
            </xsl:for-each>
        </other_names>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:pages">
        <pages xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:if test="string-length(./aou_deposit_v1:start) > 0">
                <start>
                    <xsl:value-of select="./aou_deposit_v1:start"/>
                </start>
            </xsl:if>
            <xsl:if test="string-length(./aou_deposit_v1:end) > 0">
                <end>
                    <xsl:value-of select="./aou_deposit_v1:end"/>
                </end>
            </xsl:if>
            <xsl:if test="string-length(./aou_deposit_v1:other) > 0">
                <other>
                    <xsl:value-of select="./aou_deposit_v1:other"/>
                </other>
            </xsl:if>
        </pages>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:identifiers">
        <identifiers xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <!-- keep first deposit DOI only. Else take eventual container DOI -->
            <xsl:choose>
                <xsl:when test="string-length(./aou_deposit_v1:dois/aou_deposit_v1:doi[1]) > 0">
                    <doi>
                        <xsl:value-of select="./aou_deposit_v1:dois/aou_deposit_v1:doi[1]"/>
                    </doi>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="//aou_deposit_v1:container/aou_deposit_v1:identifiers/aou_deposit_v1:dois/aou_deposit_v1:doi[1] != ''">
                        <doi>
                            <xsl:value-of
                                    select="//aou_deposit_v1:container/aou_deposit_v1:identifiers/aou_deposit_v1:dois/aou_deposit_v1:doi[1]"/>
                        </doi>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>

            <!-- Keep deposit ISBN if it exists. Else take eventual container ISBN -->
            <xsl:choose>
                <xsl:when test="./aou_deposit_v1:isbn != ''">
                    <xsl:apply-templates select="./aou_deposit_v1:isbn"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="//aou_deposit_v1:container/aou_deposit_v1:identifiers/aou_deposit_v1:isbn != ''">
                        <xsl:apply-templates select="//aou_deposit_v1:container/aou_deposit_v1:identifiers/aou_deposit_v1:isbn"/>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>

            <!-- take ISSN from container -->
            <xsl:if test="//aou_deposit_v1:container/aou_deposit_v1:identifiers/aou_deposit_v1:issn != ''">
                <issn xmlns="https://archive-ouverte.unige.ch/deposit/v2">
                    <xsl:apply-templates select="//aou_deposit_v1:container/aou_deposit_v1:identifiers/aou_deposit_v1:issn"/>
                </issn>
            </xsl:if>

            <xsl:apply-templates select="aou_deposit_v1:pmid"/>
            <xsl:apply-templates select="aou_deposit_v1:arxiv"/>
            <xsl:apply-templates select="aou_deposit_v1:rero"/>
            <xsl:apply-templates select="aou_deposit_v1:repec"/>
            <xsl:apply-templates select="aou_deposit_v1:dblp"/>
            <xsl:apply-templates select="aou_deposit_v1:urn"/>
            <xsl:apply-templates select="aou_deposit_v1:local_number"/>
        </identifiers>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:pmid">
        <pmid xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </pmid>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:isbn">
        <isbn xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </isbn>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:arxiv">
        <arxiv xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </arxiv>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:rero">
        <!-- rero is renamed -->
        <mmsid xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </mmsid>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:repec">
        <repec xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </repec>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:dblp">
        <dblp xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </dblp>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:urn">
        <urn xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </urn>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:local_number">
        <local_number xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:value-of select="."/>
        </local_number>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:abstracts">
        <abstracts xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:abstract">
                <xsl:element name="abstract">
                    <xsl:if test="./@lang !=''">
                        <xsl:attribute name="lang">
                            <xsl:value-of select="./@lang"/>
                        </xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:for-each>
        </abstracts>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:keywords">
        <keywords xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:keyword">
                <keyword>
                    <xsl:value-of select="."/>
                </keyword>
            </xsl:for-each>
        </keywords>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:classifications">
        <classifications xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:classification">
                <classification>
                    <code>
                        <xsl:value-of select="./aou_deposit_v1:code"/>
                    </code>
                    <item>
                        <xsl:value-of select="./aou_deposit_v1:item"/>
                    </item>
                </classification>
            </xsl:for-each>
        </classifications>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:publisher">
        <publisher xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:if test="./aou_deposit_v1:name !=''">
                <name>
                    <xsl:value-of select="./aou_deposit_v1:name"/>
                </name>
            </xsl:if>
            <xsl:if test="./aou_deposit_v1:place !=''">
                <place>
                    <xsl:value-of select="./aou_deposit_v1:place"/>
                </place>
            </xsl:if>
        </publisher>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:collections">
        <collections xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:collection">
                <collection>
                    <name>
                        <xsl:value-of select="./aou_deposit_v1:name"/>
                    </name>
                    <number>
                        <xsl:value-of select="./aou_deposit_v1:number"/>
                    </number>
                </collection>
            </xsl:for-each>
        </collections>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:fundings">
        <fundings xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:funding">
                <funding>
                    <funder>
                        <xsl:value-of select="./aou_deposit_v1:funder"/>
                    </funder>
                    <name>
                        <xsl:value-of select="./aou_deposit_v1:name"/>
                    </name>

                    <xsl:if test="./aou_deposit_v1:code !=''">
                        <code>
                            <xsl:value-of select="./aou_deposit_v1:code"/>
                        </code>
                    </xsl:if>

                    <xsl:if test="./aou_deposit_v1:acronym !=''">
                        <acronym>
                            <xsl:value-of select="./aou_deposit_v1:acronym"/>
                        </acronym>
                    </xsl:if>
                </funding>
            </xsl:for-each>
        </fundings>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:academic_structures">
        <academic_structures xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:academic_structure">
                <academic_structure>
                    <name>
                        <xsl:value-of select="./aou_deposit_v1:name"/>
                    </name>
                    <code>
                        <xsl:value-of select="./aou_deposit_v1:old_code"/>
                    </code>
                </academic_structure>
            </xsl:for-each>
        </academic_structures>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:groups">
        <groups xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:group">
                <group>
                    <name>
                        <xsl:value-of select="./aou_deposit_v1:name"/>
                    </name>
                    <code>
                        <xsl:value-of select="./aou_deposit_v1:code"/>
                    </code>
                </group>
            </xsl:for-each>
        </groups>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:dataset">
        <datasets xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:url">
                <url>
                    <xsl:value-of select="."/>
                </url>
            </xsl:for-each>

            <!-- all datasets become url -->
            <xsl:for-each select="./aou_deposit_v1:doi">
                <url>
                    <xsl:choose>
                        <xsl:when test="not(starts-with(., 'https://doi.org/')) and not(starts-with(., 'http://doi.org/'))">https://doi.org/<xsl:value-of
                                select="."/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                </url>
            </xsl:for-each>
        </datasets>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:container">
        <container xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:apply-templates select="aou_deposit_v1:title"/>

            <xsl:if test="./aou_deposit_v1:volume !=''">
                <volume>
                    <xsl:value-of select="./aou_deposit_v1:volume"/>
                </volume>
            </xsl:if>

            <xsl:if test="./aou_deposit_v1:issue !=''">
                <issue>
                    <xsl:value-of select="./aou_deposit_v1:issue"/>
                </issue>
            </xsl:if>

            <xsl:if test="./aou_deposit_v1:special_issue !=''">
                <special_issue>
                    <xsl:value-of select="./aou_deposit_v1:special_issue"/>
                </special_issue>
            </xsl:if>

            <xsl:if test="./aou_deposit_v1:editor !=''">
                <editor>
                    <xsl:value-of select="./aou_deposit_v1:editor"/>
                </editor>
            </xsl:if>

            <!-- container place is renamed to conference_place -->
            <xsl:if test="./aou_deposit_v1:place !=''">
                <conference_place>
                    <xsl:value-of select="./aou_deposit_v1:place"/>
                </conference_place>
            </xsl:if>

            <xsl:if test="./aou_deposit_v1:conference_date !=''">
                <conference_date>
                    <xsl:value-of select="./aou_deposit_v1:conference_date"/>
                </conference_date>
            </xsl:if>

        </container>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:links">
        <links xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:link">
                <link>
                    <xsl:attribute name="type">
                        <xsl:value-of select="./@type"/>
                    </xsl:attribute>
                    <target>
                        <xsl:value-of select="./aou_deposit_v1:target"/>
                    </target>
                    <xsl:apply-templates select="./aou_deposit_v1:description"/>
                </link>
            </xsl:for-each>
        </links>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:description">
        <description xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:if test="./@lang != ''">
                <xsl:attribute name="lang">
                    <xsl:value-of select="./@lang"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:value-of select="."/>
        </description>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:files">
        <files xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <xsl:for-each select="./aou_deposit_v1:file">
                <file>
                    <type>
                        <xsl:value-of select="./aou_deposit_v1:type"/>
                    </type>
                    <accessLevel>
                        <xsl:value-of select="./aou_deposit_v1:accessLevel"/>
                    </accessLevel>
                    <name>
                        <xsl:value-of select="./aou_deposit_v1:name"/>
                    </name>

                    <xsl:if test="./aou_deposit_v1:mimetype !=''">
                        <mimetype>
                            <xsl:value-of select="./aou_deposit_v1:mimetype"/>
                        </mimetype>
                    </xsl:if>

                    <xsl:if test="./aou_deposit_v1:license !=''">
                        <license>
                            <xsl:value-of select="./aou_deposit_v1:license"/>
                        </license>
                    </xsl:if>

                    <xsl:if test="./aou_deposit_v1:size !=''">
                        <size>
                            <xsl:value-of select="./aou_deposit_v1:size"/>
                        </size>
                    </xsl:if>

                    <xsl:apply-templates select="aou_deposit_v1:embargo"/>

                </file>
            </xsl:for-each>
        </files>
    </xsl:template>

    <xsl:template match="aou_deposit_v1:embargo">
        <embargo xmlns="https://archive-ouverte.unige.ch/deposit/v2">
            <accessLevel>
                <xsl:value-of select="./aou_deposit_v1:accessLevel"/>
            </accessLevel>
            <end_date>
                <xsl:value-of select="./aou_deposit_v1:end_date"/>
            </end_date>
        </embargo>
    </xsl:template>
</xsl:stylesheet>
