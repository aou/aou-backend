<?xml version="1.0"?>
<xsl:stylesheet
        version="1.0"
        xmlns:marc="http://www.loc.gov/MARC21/slim"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:aou_deposit="https://archive-ouverte.unige.ch/deposit/v2.4"
        exclude-result-prefixes="aou_deposit">


    <xsl:output encoding="utf-8" method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
        <marc:collection xmlns:marc="http://www.loc.gov/MARC21/slim" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
            <marc:record>
                <xsl:apply-templates select="aou_deposit:deposit_doc"/>
            </marc:record>
        </marc:collection>
    </xsl:template>

    <xsl:template match="aou_deposit:deposit_doc">
        <xsl:variable name="view">
            <xsl:choose>
                <xsl:when test="aou_deposit:type = 'Article'">article</xsl:when>
                <xsl:when test="aou_deposit:type = 'Conférence'">conference</xsl:when>
                <xsl:when test="aou_deposit:type = 'Diplôme'">diplome</xsl:when>
                <xsl:when test="aou_deposit:type = 'Journal'">journal</xsl:when>
                <xsl:when test="aou_deposit:type = 'Livre'">livre</xsl:when>
                <xsl:when test="aou_deposit:type = 'Rapport'">rapport</xsl:when>
                <xsl:otherwise>DocumentClassNotFoundException</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="subview">
            <xsl:choose>
                <xsl:when test="aou_deposit:subtype = 'Actes de conférence'">actes</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Article professionnel'">art_pro</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Article scientifique'">art_sci</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Autre article'">art_aut</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Chapitre d''actes'">chap_act</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Chapitre de livre'">chap_liv</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Contribution à un dictionnaire / une encyclopédie'">contrib</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Livre'">livre</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Master d''études avancées'">mea</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Master'">master</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Numéro de revue'">numero</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Ouvrage collectif'">liv_coll</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Poster'">poster</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Preprint'">prep</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Présentation / Intervention'">present</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Rapport de recherche'">rap_rec</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Rapport technique'">rap_tec</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Thèse de privat-docent'">priv_doc</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Thèse'">these</xsl:when>
                <xsl:when test="aou_deposit:subtype = 'Working paper'">wp</xsl:when>
                <xsl:when test="starts-with(aou_deposit:subtype, 'Chapitre')">chapitres</xsl:when>
                <xsl:otherwise>DocumentClassNotFoundException</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="date_diplome">
            <xsl:choose>
                <xsl:when test="//aou_deposit:date[@type = 'defense']"><xsl:value-of select="//aou_deposit:date[@type = 'defense']"/></xsl:when>
                <xsl:when test="//aou_deposit:date[@type = 'imprimatur']"><xsl:value-of select="//aou_deposit:date[@type = 'imprimatur']"/></xsl:when>
                <xsl:when test="//aou_deposit:date[@type = 'first_online']"><xsl:value-of select="//aou_deposit:date[@type = 'first_online']"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="//aou_deposit:date[@type = 'publication']"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="annee_diplome" select="substring($date_diplome, 0, 5)"/>
        <xsl:variable name="annee_enligne" select="substring(//aou_deposit:date[@type = 'first_online'], 0, 5)"/>
        <xsl:variable name="annee_publication" select="substring(//aou_deposit:date[@type = 'publication'], 0, 5)"/>

        <!-- Les identifiants 020, 022 et 024 sont uniques chez nous -->
        <!-- 020 - ISBN -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:isbn != ''">
            <marc:datafield tag="020" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:isbn"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 022 - ISSN -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:issn != ''">
            <marc:datafield tag="022" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:issn"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 024 - DOI -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:doi != ''">
            <marc:datafield tag="024" ind1="7" ind2=" ">
                <marc:subfield code="2">DOI</marc:subfield>
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:doi"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 024 - PMID -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:pmid != ''">
            <marc:datafield tag="024" ind1="7" ind2=" ">
                <marc:subfield code="2">PMID</marc:subfield>
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:pmid"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 024 - PMCID -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:pmcid != ''">
            <marc:datafield tag="024" ind1="7" ind2=" ">
                <marc:subfield code="2">PMCID</marc:subfield>
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:pmcid"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 024 - arxiv -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:arxiv != ''">
            <marc:datafield tag="024" ind1="7" ind2=" ">
                <marc:subfield code="2">arxiv</marc:subfield>
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:arxiv"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 024 - DBLP -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:dblp != ''">
            <marc:datafield tag="024" ind1="7" ind2=" ">
                <marc:subfield code="2">DBLP</marc:subfield>
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:dblp"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 024 - Swisscovery MMS -->
        <xsl:if test="aou_deposit:identifiers/aou_deposit:mmsid != ''">
            <marc:datafield tag="024" ind1="7" ind2=" ">
                <marc:subfield code="2">Swisscovery</marc:subfield>
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:mmsid"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 041 - Langue du document -->
        <marc:datafield tag="041" ind1=" " ind2=" ">
            <xsl:for-each select="aou_deposit:languages/aou_deposit:language">
                <marc:subfield code="a">
                    <xsl:value-of select="."/>
                </marc:subfield>
            </xsl:for-each>
        </marc:datafield>

        <!-- 082 - Indice(s) Dewey -->
        <xsl:for-each select="aou_deposit:classifications/aou_deposit:classification">
            <xsl:if test="aou_deposit:code = 'Dewey' and aou_deposit:item != ''">
                <marc:datafield tag="082" ind1=" " ind2=" ">
                    <marc:subfield code="a">
                        <xsl:value-of select="./aou_deposit:item"/>
                    </marc:subfield>
                </marc:datafield>
            </xsl:if>
        </xsl:for-each>

        <!-- 084 - Autre classification -->
        <xsl:for-each select="aou_deposit:classifications/aou_deposit:classification">
            <xsl:if test="aou_deposit:code != 'Dewey' and aou_deposit:item != ''">
                <marc:datafield tag="084" ind1=" " ind2=" ">
                    <marc:subfield code="2">
                        <xsl:value-of select="./aou_deposit:code"/>
                    </marc:subfield>
                    <marc:subfield code="a">
                        <xsl:value-of select="./aou_deposit:item"/>
                    </marc:subfield>
                </marc:datafield>
            </xsl:if>
        </xsl:for-each>

		<!-- 084 - Extra for RePEc export - for the old CAKE application -->
		<xsl:if test="aou_deposit:classifications/aou_deposit:classification/aou_deposit:code = 'JEL'">
			<marc:datafield tag="084" ind1=" " ind2=" ">
				<marc:subfield code="2">repec</marc:subfield>
				<marc:subfield code="a">
					<xsl:for-each select="aou_deposit:classifications/aou_deposit:classification">
						<xsl:if test="aou_deposit:code = 'JEL'">
							<xsl:value-of select="aou_deposit:item"/>
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
				</marc:subfield>
			</marc:datafield>
		</xsl:if>

        <!-- 088 - Numero de rapport - champ unique -->
        <xsl:if test="$view = 'rapport' and aou_deposit:identifiers/aou_deposit:local_number != ''">
            <marc:datafield tag="088" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:local_number"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 245 - Titre - Obligatoire -->
        <marc:datafield tag="245" ind1=" " ind2=" ">
            <xsl:if test="aou_deposit:title/@lang != ''">
                <marc:subfield code="9">
                    <xsl:value-of select="//aou_deposit:title/@lang"/>
                </marc:subfield>
            </xsl:if>
            <marc:subfield code="a">
                <xsl:value-of select="//aou_deposit:title"/>
            </marc:subfield>
        </marc:datafield>

        <!-- 246 - Titre original -->
        <xsl:if test="aou_deposit:original_title != ''">
            <marc:datafield tag="246" ind1=" " ind2=" ">
                <marc:subfield code="9">
                    <xsl:value-of select="//aou_deposit:original_title/@lang"/>
                </marc:subfield>
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:original_title"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 250 - Mention edition -->
        <xsl:if test="aou_deposit:edition != ''">
            <marc:datafield tag="250" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:edition"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 260 - Date edition, lieu, editeur -->
        <xsl:if test="$view = 'livre' or $subview = 'actes' or $view='rapport'">
            <marc:datafield tag="260" ind1=" " ind2=" ">
                <xsl:if test="aou_deposit:publisher/aou_deposit:place != ''">
                    <marc:subfield code="a">
                        <xsl:value-of select="//aou_deposit:publisher/aou_deposit:place"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="aou_deposit:publisher/aou_deposit:name != ''">
                        <marc:subfield code="b">
                            <xsl:value-of select="//aou_deposit:publisher/aou_deposit:name"/>
                        </marc:subfield>
                    </xsl:when>
                    <xsl:when test="aou_deposit:mandator != ''">
                        <marc:subfield code="b">
                            <xsl:value-of select="//aou_deposit:mandator"/>
                        </marc:subfield>
                    </xsl:when>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="$annee_enligne">
                        <marc:subfield code="c">
                            <xsl:value-of select="$annee_enligne"/>
                        </marc:subfield>
                    </xsl:when>
                    <xsl:otherwise>
                        <marc:subfield code="c">
                            <xsl:value-of select="$annee_publication"/>
                        </marc:subfield>
                    </xsl:otherwise>
                </xsl:choose>
            </marc:datafield>
        </xsl:if>

        <!-- 269 - stockage des dates - Obligatoire -->
        <marc:datafield tag="269" ind1=" " ind2=" ">
            <xsl:choose>
                <xsl:when test="aou_deposit:dates/aou_deposit:date[@type = 'first_online']">
                    <marc:subfield code="1">
                        <xsl:value-of select="//aou_deposit:date[@type = 'first_online']"/>
                    </marc:subfield>
                </xsl:when>
                <xsl:when test="aou_deposit:dates/aou_deposit:date[@type = 'defense']">
                    <marc:subfield code="1">
                        <xsl:value-of select="//aou_deposit:date[@type = 'defense']"/>
                    </marc:subfield>
                </xsl:when>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="aou_deposit:dates/aou_deposit:date[@type = 'publication']">
                    <marc:subfield code="2">
                        <xsl:value-of select="//aou_deposit:date[@type = 'publication']"/>
                    </marc:subfield>
                </xsl:when>
                <xsl:when test="aou_deposit:dates/aou_deposit:date[@type = 'imprimatur']">
                    <marc:subfield code="2">
                        <xsl:value-of select="//aou_deposit:date[@type = 'imprimatur']"/>
                    </marc:subfield>
                </xsl:when>
            </xsl:choose>
        </marc:datafield>

        <!-- 300 - Nombre de pages -->
        <xsl:if test="aou_deposit:pages/aou_deposit:paging !='' and $view != 'article' and $subview != 'chapitres' and $subview != 'contrib'">
            <marc:datafield tag="300" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:pages/aou_deposit:paging"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 490 - Collection -->
        <xsl:for-each select="aou_deposit:collections/aou_deposit:collection">
            <xsl:if test="./aou_deposit:name !=''">
                <marc:datafield tag="490" ind1=" " ind2=" ">
                    <marc:subfield code="a">
                        <xsl:value-of select="./aou_deposit:name"/>
                    </marc:subfield>
                    <xsl:if test="./aou_deposit:number !=''">
                        <marc:subfield code="v">
                            <xsl:value-of select="./aou_deposit:number"/>
                        </marc:subfield>
                    </xsl:if>
                </marc:datafield>
            </xsl:if>
        </xsl:for-each>

        <!-- 500 - Note -->
        <xsl:if test="aou_deposit:note != ''">
            <marc:datafield tag="500" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:note"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 502 - Diplômes - soutenance, etc. -->
        <xsl:if test="$view = 'diplome'">
            <marc:datafield tag="502" ind1=" " ind2=" ">
                <xsl:if test="aou_deposit:identifiers/aou_deposit:local_number != '' and aou_deposit:academic_structures/aou_deposit:academic_structure != ''">
                    <marc:subfield code="8">
                        <xsl:value-of select="//aou_deposit:local_number"/>
                    </marc:subfield>
                </xsl:if>
                <marc:subfield code="9">
                    <xsl:value-of select="translate($date_diplome,'-','/')"/>
                </marc:subfield>
                <xsl:choose>
                    <xsl:when test="aou_deposit:academic_structures/aou_deposit:academic_structure != ''">
                        <xsl:choose>
                            <xsl:when test="aou_deposit:subtype = 'Thèse'">
                                <marc:subfield code="a">Thèse de doctorat : Univ. Genève, <xsl:value-of select="$annee_diplome"/></marc:subfield>
                            </xsl:when>
                            <xsl:otherwise>
                                <marc:subfield code="a">
                                    <xsl:value-of select="//aou_deposit:subtype"/> : Univ. Genève, <xsl:value-of select="$annee_diplome"/>
                                </marc:subfield>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <!-- Diplômes non-UNIGE -->
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="aou_deposit:subtype = 'Thèse'">
                                <marc:subfield code="a">Thèse de doctorat<xsl:if test="aou_deposit:identifiers/aou_deposit:local_number != ''"> : <xsl:value-of select="//aou_deposit:local_number"/></xsl:if>, <xsl:value-of select="$annee_diplome"/></marc:subfield>
                            </xsl:when>
                            <xsl:otherwise>
                                <marc:subfield code="a">
                                    <xsl:value-of select="//aou_deposit:subtype"/>, <xsl:value-of select="$annee_diplome"/>
                                </marc:subfield>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:if test="aou_deposit:discipline != ''">
                    <marc:subfield code="m">
                        <xsl:value-of select="//aou_deposit:discipline"/>
                    </marc:subfield>
                </xsl:if>
            </marc:datafield>
        </xsl:if>

        <!-- 508 - Directeurs -->
        <xsl:for-each select="aou_deposit:contributors/aou_deposit:contributor">
            <xsl:if test="aou_deposit:role = 'director'">
                <marc:datafield tag="508" ind1=" " ind2=" ">
                    <marc:subfield code="a">
                        <xsl:value-of select="./aou_deposit:lastname"/>
                        <!-- Test sur la présence du prénom -->
                        <xsl:if test="aou_deposit:firstname != ''">, <xsl:value-of select="./aou_deposit:firstname"/></xsl:if>
                    </marc:subfield>
                    <marc:subfield code="e">Dir.</marc:subfield>
                    <xsl:if test="aou_deposit:other_names != ''">
                        <xsl:for-each select="aou_deposit:other_names/aou_deposit:other_name">
                            <marc:subfield code="g">
                                <xsl:value-of select="./aou_deposit:lastname"/>
                                <xsl:if test="aou_deposit:firstname != ''">, <xsl:value-of select="./aou_deposit:firstname"/></xsl:if>
                            </marc:subfield>
                        </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="aou_deposit:cn_individu != ''">
                        <marc:subfield code="9">
                            <xsl:value-of select="./aou_deposit:cn_individu"/>
                        </marc:subfield>
                    </xsl:if>
                    <xsl:if test="aou_deposit:orcid != ''">
                        <marc:subfield code="0">
                            <xsl:value-of select="./aou_deposit:orcid"/>
                        </marc:subfield>
                    </xsl:if>
                </marc:datafield>
            </xsl:if>
        </xsl:for-each>

        <!-- 520 - Résumé -->
        <xsl:for-each select="aou_deposit:abstracts/aou_deposit:abstract">
            <marc:datafield tag="520" ind1=" " ind2=" ">
                <xsl:if test="./@lang != ''">
                    <marc:subfield code="9">
                        <xsl:value-of select="./@lang"/>
                    </marc:subfield>
                </xsl:if>
                <marc:subfield code="a">
                    <xsl:value-of select="."/>
                </marc:subfield>
            </marc:datafield>
        </xsl:for-each>

        <!-- 525 - Corrections -->
        <xsl:for-each select="aou_deposit:corrections/aou_deposit:correction[aou_deposit:note !='']">
            <marc:datafield tag="525" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="./aou_deposit:note"/>
                </marc:subfield>
                <xsl:if test="aou_deposit:doi != ''">
                    <marc:subfield code="d">
                        <xsl:value-of select="./aou_deposit:doi"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="aou_deposit:pmid != ''">
                    <marc:subfield code="p">
                        <xsl:value-of select="./aou_deposit:pmid"/>
                    </marc:subfield>
                </xsl:if>
            </marc:datafield>
        </xsl:for-each>

        <!-- 586 - Prix et recompense -->
        <xsl:if test="aou_deposit:award != ''">
            <marc:datafield tag="586" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:award"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 653 - Mots-cles -->
        <xsl:for-each select="aou_deposit:keywords/aou_deposit:keyword">
            <marc:datafield tag="653" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="."/>
                </marc:subfield>
            </marc:datafield>
        </xsl:for-each>

        <!-- 700 - Auteurs - tous les contributeurs sauf les directeurs -->
        <xsl:for-each select="aou_deposit:contributors/aou_deposit:contributor">
            <xsl:choose>
                <xsl:when test="aou_deposit:role != 'director'">
                    <marc:datafield tag="700" ind1=" " ind2=" ">
                        <marc:subfield code="a">
                            <xsl:value-of select="./aou_deposit:lastname"/>
                            <!-- Test sur la présence du prénom -->
                            <xsl:if test="aou_deposit:firstname != ''">, <xsl:value-of select="./aou_deposit:firstname"/></xsl:if>
                        </marc:subfield>
                        <xsl:choose>
                            <xsl:when test="aou_deposit:role = 'editor'">
                                <marc:subfield code="e">Ed.</marc:subfield>
                            </xsl:when>
                            <xsl:when test="aou_deposit:role = 'guest_editor'">
                                <marc:subfield code="e">Guest Ed.</marc:subfield>
                            </xsl:when>
                            <xsl:when test="aou_deposit:role = 'collaborator'">
                                <marc:subfield code="e">Collab.</marc:subfield>
                            </xsl:when>
                            <xsl:when test="aou_deposit:role = 'translator'">
                                <marc:subfield code="e">Transl.</marc:subfield>
                            </xsl:when>
                            <xsl:when test="aou_deposit:role = 'photographer'">
                                <marc:subfield code="e">Photogr.</marc:subfield>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:if test="aou_deposit:other_names != ''">
                            <xsl:for-each select="aou_deposit:other_names/aou_deposit:other_name">
                                <marc:subfield code="g">
                                    <xsl:value-of select="./aou_deposit:lastname"/>
                                    <xsl:if test="aou_deposit:firstname != ''">, <xsl:value-of select="./aou_deposit:firstname"/></xsl:if>
                                </marc:subfield>
                            </xsl:for-each>
                        </xsl:if>
                        <xsl:if test="aou_deposit:cn_individu != ''">
                            <marc:subfield code="9">
                                <xsl:value-of select="./aou_deposit:cn_individu"/>
                            </marc:subfield>
                        </xsl:if>
                        <xsl:if test="aou_deposit:orcid != ''">
                            <marc:subfield code="0">
                                <xsl:value-of select="./aou_deposit:orcid"/>
                            </marc:subfield>
                        </xsl:if>
                    </marc:datafield>
                </xsl:when>
            </xsl:choose>
        </xsl:for-each>

        <!-- 710 - Mandataire -->
        <xsl:if test="$view='rapport' and aou_deposit:mandator != ''">
            <marc:datafield tag="710" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="//aou_deposit:mandator"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 710 - Collaboration(s) -->
        <xsl:for-each select="aou_deposit:contributors/aou_deposit:collaboration[aou_deposit:name!='']">
            <marc:datafield tag="710" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="./aou_deposit:name"/>
                </marc:subfield>
                <marc:subfield code="e">Collaboration</marc:subfield>
            </marc:datafield>
        </xsl:for-each>

        <!-- 773 - Info sur la publication hôte / container -->
        <xsl:if test="aou_deposit:container">
            <marc:datafield tag="773" ind1=" " ind2=" ">
                <xsl:if test="aou_deposit:container/aou_deposit:editor != ''">
                    <marc:subfield code="a">
                        <xsl:value-of select="//aou_deposit:editor"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="aou_deposit:container/aou_deposit:conference_subtitle != ''">
                    <marc:subfield code="c">
                        <xsl:value-of select="//aou_deposit:conference_subtitle"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="aou_deposit:container/aou_deposit:special_issue != ''">
                    <marc:subfield code="h">
                        <xsl:value-of select="//aou_deposit:special_issue"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="aou_deposit:container/aou_deposit:conference_date != ''">
                    <marc:subfield code="j">
                        <xsl:value-of select="//aou_deposit:conference_date"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="aou_deposit:container/aou_deposit:conference_place != ''">
                    <marc:subfield code="l">
                        <xsl:value-of select="//aou_deposit:container/aou_deposit:conference_place"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="aou_deposit:container/aou_deposit:issue != ''">
                    <marc:subfield code="n">
                        <xsl:value-of select="//aou_deposit:issue"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:choose>
                    <xsl:when test="aou_deposit:pages/aou_deposit:other != ''">
                        <marc:subfield code="p">
                            <xsl:value-of select="//aou_deposit:pages/aou_deposit:other"/>
                        </marc:subfield>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="aou_deposit:pages/aou_deposit:paging != '' and ($view = 'article' or $subview = 'chapitres' or $subview = 'contrib')">
                            <marc:subfield code="p">
                                <xsl:value-of select="//aou_deposit:paging"/>
                            </marc:subfield>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
                <marc:subfield code="t">
                    <xsl:value-of select="//aou_deposit:container/aou_deposit:title"/>
                </marc:subfield>
                <xsl:if test="aou_deposit:container/aou_deposit:volume != ''">
                    <marc:subfield code="v">
                        <xsl:value-of select="//aou_deposit:volume"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="$view = 'journal' or $view = 'article' or $subview = 'poster' or $subview = 'present'">
                    <xsl:choose>
                        <xsl:when test="aou_deposit:container/aou_deposit:volume != '' or aou_deposit:container/aou_deposit:issue != ''">
                            <xsl:choose>
                                <xsl:when test="$annee_publication">
                                    <marc:subfield code="y">
                                        <xsl:value-of select="$annee_publication"/>
                                    </marc:subfield>
                                </xsl:when>
                                <xsl:otherwise>
                                    <marc:subfield code="y">
                                        <xsl:value-of select="$annee_enligne"/>
                                    </marc:subfield>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="$annee_enligne">
                                    <marc:subfield code="y">
                                        <xsl:value-of select="$annee_enligne"/>
                                    </marc:subfield>
                                </xsl:when>
                                <xsl:otherwise>
                                    <marc:subfield code="y">
                                        <xsl:value-of select="$annee_publication"/>
                                    </marc:subfield>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>
            </marc:datafield>
        </xsl:if>

        <!-- 856 - URL commercial -->
        <xsl:if test="string(aou_deposit:publisher_version_url)"> <!-- !='' ne fonctionne pas avec 2 _ -->
            <marc:datafield tag="856" ind1="4" ind2=" ">
                <marc:subfield code="3">Alternate edition</marc:subfield>
                <marc:subfield code="u">
                    <xsl:value-of select="//aou_deposit:publisher_version_url"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 856 - Datasets -->
        <xsl:for-each select="aou_deposit:datasets/aou_deposit:url">
            <marc:datafield tag="856" ind1="4" ind2=" ">
                <marc:subfield code="3">Dataset</marc:subfield>
                <marc:subfield code="u">
                    <xsl:value-of select="."/>
                </marc:subfield>
            </marc:datafield>
        </xsl:for-each>

        <!-- 856 - URN -->
        <xsl:choose>
            <xsl:when test="aou_deposit:identifiers/aou_deposit:urn != '' and starts-with(aou_deposit:identifiers/aou_deposit:urn, 'https://nbn-resolving.org/')">
                <marc:datafield tag="856" ind1="4" ind2=" ">
                    <marc:subfield code="3">URN</marc:subfield>
                        <marc:subfield code="u"><xsl:value-of select="//aou_deposit:urn"/></marc:subfield>
                </marc:datafield>
            </xsl:when>
            <xsl:when test="aou_deposit:identifiers/aou_deposit:urn != ''">
                <marc:datafield tag="856" ind1="4" ind2=" ">
                    <marc:subfield code="3">URN</marc:subfield>
                        <marc:subfield code="u">https://nbn-resolving.org/<xsl:value-of select="//aou_deposit:urn"/></marc:subfield>
                </marc:datafield>
            </xsl:when>
        </xsl:choose>

        <!-- 920 - UNIGE -->
        <xsl:if test="aou_deposit:academic_structures/aou_deposit:academic_structure != ''">
            <marc:datafield tag="920" ind1=" " ind2=" ">
                <marc:subfield code="a">Université de Genève</marc:subfield>
            </marc:datafield>
        </xsl:if>

        <!-- 927 - Groupe de recherche -->
        <xsl:for-each select="aou_deposit:groups/aou_deposit:group[aou_deposit:name!='']">
            <marc:datafield tag="927" ind1=" " ind2=" ">
                <xsl:choose>
                    <xsl:when test="./aou_deposit:code">
                        <marc:subfield code="a"><xsl:value-of select="./aou_deposit:name"/> (<xsl:value-of select="./aou_deposit:code"/>)</marc:subfield>
                    </xsl:when>
                    <xsl:otherwise>
                        <marc:subfield code="a"><xsl:value-of select="./aou_deposit:name"/></marc:subfield>
                    </xsl:otherwise>
                </xsl:choose>
                <marc:subfield code="c">
                    <xsl:value-of select="./aou_deposit:resId"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:for-each>

        <!-- 928 - Structures academiques -->
        <xsl:for-each select="aou_deposit:academic_structures/aou_deposit:academic_structure[aou_deposit:name!='']">
            <marc:datafield tag="928" ind1=" " ind2=" ">
                <xsl:variable name="structure_position">
                    <xsl:value-of select="position()"/>
                </xsl:variable>
                <marc:subfield code="a">
                    <xsl:value-of select="./aou_deposit:name"/>
                </marc:subfield>
                <marc:subfield code="b">
                    <xsl:value-of select="(../../aou_deposit:additionalMetadata/aou_deposit:structures/aou_deposit:structure)[position()=$structure_position]"/>
                </marc:subfield>
                <marc:subfield code="c">
                    <xsl:value-of select="./aou_deposit:code"/>
                </marc:subfield>
            </marc:datafield>
        </xsl:for-each>
        
        <!-- 929 - Structures -->
        <xsl:for-each select="aou_deposit:additionalMetadata/aou_deposit:structures/aou_deposit:structure">
            <marc:datafield tag="929" ind1=" " ind2=" ">
                <marc:subfield code="a">
                    <xsl:value-of select="."/>
                </marc:subfield>
            </marc:datafield>
        </xsl:for-each>

        <!-- 980 - Type du document -->
        <marc:datafield tag="980" ind1=" " ind2=" ">
            <!-- Temporaire jusqu'à la correction C4 -> L4 dans interface de consultation -->
            <xsl:choose>
                <xsl:when test="aou_deposit:subtype = 'Chapitre d''actes'">
                    <marc:subfield code="a">Livre</marc:subfield>
                </xsl:when>
                <xsl:otherwise>
                    <marc:subfield code="a">
                        <xsl:value-of select="//aou_deposit:type"/>
                    </marc:subfield>
                </xsl:otherwise>
            </xsl:choose>
            <marc:subfield code="b">
                <xsl:value-of select="//aou_deposit:subtype"/>
            </marc:subfield>
            <xsl:if test="aou_deposit:subsubtype != ''">
                <marc:subfield code="c">
                    <xsl:value-of select="//aou_deposit:subsubtype"/>
                </marc:subfield>
            </xsl:if>
        </marc:datafield>

        <!-- 988 - Bourse ou projet -->
        <xsl:for-each select="aou_deposit:fundings/aou_deposit:funding[aou_deposit:funder!='']">
            <marc:datafield tag="988" ind1=" " ind2=" ">
                <xsl:if test="./aou_deposit:program != ''">
                    <marc:subfield code="2">
                        <xsl:value-of select="./aou_deposit:program"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="./aou_deposit:acronym != ''">
                    <marc:subfield code="a">
                        <xsl:value-of select="./aou_deposit:acronym"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="./aou_deposit:code != ''">
                    <marc:subfield code="c">
                        <xsl:value-of select="./aou_deposit:code"/>
                    </marc:subfield>
                </xsl:if>
                <marc:subfield code="f">
                    <xsl:value-of select="./aou_deposit:funder"/>
                </marc:subfield>
                <xsl:if test="./aou_deposit:jurisdiction != ''">
                    <marc:subfield code="j">
                        <xsl:value-of select="./aou_deposit:jurisdiction"/>
                    </marc:subfield>
                </xsl:if>
                <xsl:if test="./aou_deposit:name != ''">
                    <marc:subfield code="n">
                        <xsl:value-of select="./aou_deposit:name"/>
                    </marc:subfield>
                </xsl:if>
            </marc:datafield>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>


