<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:deposit_doc="https://archive-ouverte.unige.ch/deposit/v2.4"
                xmlns:publications="https://archive-ouverte.unige.ch/deposit/list-deposits">
    <xsl:output method="text" encoding="UTF-8"/>

    <!-- Header of the TSV -->
    <xsl:template match="/">
        <xsl:text>archiveId,type,subtype,title,language,first_online,publication_date,author_lastname,author_firstname,author_institution,doi,pmid,issn,abstract,keywords&#10;</xsl:text>
        <xsl:apply-templates select="//publications:publication"/>
    </xsl:template>

    <xsl:template match="publications:publication">
        <xsl:variable name="archiveId" select="publications:archiveId"/>
        <xsl:variable name="type" select="deposit_doc:deposit_doc/deposit_doc:type"/>
        <xsl:variable name="subtype" select="deposit_doc:deposit_doc/deposit_doc:subtype"/>
        <xsl:variable name="title" select="deposit_doc:deposit_doc/deposit_doc:title"/>
        <xsl:variable name="language" select="deposit_doc:deposit_doc/deposit_doc:languages/deposit_doc:language"/>
        <xsl:variable name="first_online" select="deposit_doc:deposit_doc/deposit_doc:dates/deposit_doc:date[@type='first_online']"/>
        <xsl:variable name="publication_date" select="deposit_doc:deposit_doc/deposit_doc:dates/deposit_doc:date[@type='publication']"/>
        <xsl:variable name="doi" select="deposit_doc:deposit_doc/deposit_doc:identifiers/deposit_doc:doi"/>
        <xsl:variable name="pmid" select="deposit_doc:deposit_doc/deposit_doc:identifiers/deposit_doc:pmid"/>
        <xsl:variable name="issn" select="deposit_doc:deposit_doc/deposit_doc:identifiers/deposit_doc:issn"/>
        <xsl:variable name="abstract" select="deposit_doc:deposit_doc/deposit_doc:abstracts/deposit_doc:abstract"/>
        <xsl:variable name="keywords">
            <xsl:for-each select="deposit_doc:deposit_doc/deposit_doc:keywords/deposit_doc:keyword">
                <xsl:value-of select="."/>
                <xsl:if test="position() != last()">
                    <xsl:text>; </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <!-- Authors -->
        <xsl:for-each select="deposit_doc:deposit_doc/deposit_doc:contributors/deposit_doc:contributor">
            <xsl:value-of select="$archiveId"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$type"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$subtype"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$title"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$language"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$first_online"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$publication_date"/><xsl:text>,</xsl:text>
            <xsl:value-of select="deposit_doc:lastname"/><xsl:text>,</xsl:text>
            <xsl:value-of select="deposit_doc:firstname"/><xsl:text>,</xsl:text>
            <xsl:value-of select="deposit_doc:institution"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$doi"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$pmid"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$issn"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$abstract"/><xsl:text>,</xsl:text>
            <xsl:value-of select="$keywords"/><xsl:text>&#10;</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
