<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:aou="https://archive-ouverte.unige.ch/deposit/v2.4"
                xmlns:epicur="urn:nbn:de:1111-2004033116"
                exclude-result-prefixes="aou">

    <xsl:output method="xml" omit-xml-declaration="yes" indent="yes"
                encoding="UTF-8"/>

    <xsl:param name="archiveUrl"/>
    <xsl:param name="resId"/>
    <xsl:param name="oaiId"/>

    <xsl:strip-space elements="*"/>

    <xsl:template match="aou:deposit_doc">
        <xsl:element name="epicur:epicur">
            <xsl:attribute name="xsi:schemaLocation">urn:nbn:de:1111-2004033116
                http://www.persistent-identifier.de/xepicur/version1.0/xepicur.xsd"
            </xsl:attribute>
            <epicur:administrative_data>
                <epicur:delivery>
                    <epicur:update_status type="urn_new"></epicur:update_status>
                </epicur:delivery>
            </epicur:administrative_data>
           <epicur:record>
                <epicur:identifier scheme="urn:nbn:ch"><xsl:value-of select="aou:identifiers/aou:urn"/></epicur:identifier>
                <epicur:resource>
                  <epicur:identifier origin="original" role="primary" scheme="url" type="frontpage"><xsl:value-of select="$archiveUrl"/><xsl:value-of select="$oaiId"/></epicur:identifier>
                  <epicur:format scheme="imt">text/html</epicur:format>
              </epicur:resource>
            </epicur:record>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>