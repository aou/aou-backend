<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet 
  version="1.0"
  xmlns:marc="http://www.loc.gov/MARC21/slim" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!--
  Convert MARCXML records into AoU pivot format with schema v2.4
  
  Pay attention to:
  - tag <resId/> for <academic_structure/> is always empty because info doesn't exist in MARCXML
  - tag <resId/> for <group/> may be empty because info doesn't exist in MARCXML for non UNIGE groups
  - empty attribute xmlns="" is sometimes created as in <classification xmlns="">
  - empty tag <datasets/> is created when no dataset exists in MARCXML
  - no information regarding files are retrieved (access level, licence, number of files, links...)
  
  For information:
  - MARC 269$1+$2 rule over 260$c, 502$9 or 773$y
  - MARC leader discarded
  - MARC controlfield 001 discarded
  - MARC 035, 506, 930 discarded
  - MARC 700$8 discarded
  - MARC 710 discarded if $a = Collaboration
  - MARC 856 discarded if $3 != Alternate edition or Dataset
  - MARC 300 is cleaned with removal of " p."
  - SNSF codes are reduced to their second part only
  - Subsubtype of article is added if missing + new treatment of diplomas
  -->

  <xsl:output method="xml" indent="yes"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="marc:record">
    <deposit_doc xmlns="https://archive-ouverte.unige.ch/deposit/v2.4">

    <xsl:variable name="view">
      <xsl:choose>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='a'] = 'Article'">article</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='a'] = 'Conférence'">conference</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='a'] = 'Diplôme'">diplome</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='a'] = 'Journal'">journal</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='a'] = 'Livre'">livre</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='a'] = 'Rapport'">rapport</xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="subview">
      <xsl:choose>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Livre'">livre</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Chapitre de livre'">chap_liv</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Actes de conférence'">actes</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Chapitre d''actes'">chap_act</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Ouvrage collectif'">livre_collectif</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Contribution à un dictionnaire / une encyclopédie'">contrib</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Article scientifique'">art_sci</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Article professionnel'">art_pro</xsl:when>
        <xsl:when test="marc:datafield[@tag='980']/marc:subfield[@code='b'] = 'Thèse'">thesis</xsl:when>
      </xsl:choose>
    </xsl:variable>

    <!-- **** **** **** -->
    <!-- **** TYPE  **** -->
    <!-- **** **** **** -->
    <type>
      <xsl:choose>
        <!-- Pour tenir compte du changement de parent pour les chapitres d'actes -->
        <xsl:when test="$subview = 'chap_act'">
          <xsl:text>Conférence</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="marc:datafield[@tag='980']/marc:subfield[@code='a']"/>
        </xsl:otherwise>
      </xsl:choose>
    </type>
    <subtype>
      <xsl:value-of select="marc:datafield[@tag='980']/marc:subfield[@code='b']"/>
    </subtype>
    <xsl:if test="marc:datafield[@tag='980']/marc:subfield[@code='c'] != '' and ($subview = 'art_sci' or $subview = 'art_pro')">
      <subsubtype>
        <xsl:value-of select="marc:datafield[@tag='980']/marc:subfield[@code='c']"/>
      </subsubtype>
    </xsl:if>
    <xsl:if test="($subview = 'art_sci' or $subview = 'art_pro') and not(marc:datafield[@tag='980']/marc:subfield[@code='c'])">
      <subsubtype>
        <xsl:text>Article</xsl:text>
      </subsubtype>
    </xsl:if>

    <!-- **** ***** **** -->
    <!-- **** TITLE **** -->
    <!-- **** ***** **** -->
    <title>
      <xsl:if test="marc:datafield[@tag='245']/marc:subfield[@code='9'] != ''">
        <xsl:attribute name='lang'><xsl:value-of select="marc:datafield[@tag='245']/marc:subfield[@code='9']"/></xsl:attribute>
      </xsl:if>
      <xsl:value-of select="marc:datafield[@tag='245']/marc:subfield[@code='a']"/>
    </title>

    <!-- **** ******** **** -->
    <!-- **** LANGUAGE **** -->
    <!-- **** ******** **** -->
    <languages>
      <xsl:for-each select="marc:datafield[@tag='041']">
        <xsl:for-each select="marc:subfield[@code='a']">
          <language>
            <xsl:value-of select="."/>
          </language>
        </xsl:for-each>
      </xsl:for-each>
    </languages>

    <!-- **** ************** **** -->
    <!-- **** ORIGINAL TITLE **** -->
    <!-- **** ************** **** -->
    <xsl:for-each select="marc:datafield[@tag='246']">
      <original_title>
        <xsl:attribute name='lang'><xsl:value-of select="marc:subfield[@code='9']"/></xsl:attribute>
        <xsl:value-of select="marc:subfield[@code='a']"/>
      </original_title>
    </xsl:for-each>

    <!-- **** **** **** -->
    <!-- **** DATE **** -->
    <!-- **** **** **** -->
    <dates>
      <xsl:choose>
        <xsl:when test="marc:datafield[@tag='269'] != ''"> <!-- for new records -->
          <xsl:choose>
            <xsl:when test="$view = 'diplome'">
              <xsl:if test="marc:datafield[@tag='269']/marc:subfield[@code='1'] != ''">
                <date>
                  <xsl:attribute name='type'>defense</xsl:attribute>
                  <xsl:value-of select="marc:datafield[@tag='269']/marc:subfield[@code='1']"/>
                </date>
              </xsl:if>
              <xsl:if test="marc:datafield[@tag='269']/marc:subfield[@code='2'] != ''">
                <date>
                  <xsl:attribute name='type'>imprimatur</xsl:attribute>
                  <xsl:value-of select="marc:datafield[@tag='269']/marc:subfield[@code='2']"/>
                </date>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <xsl:if test="marc:datafield[@tag='269']/marc:subfield[@code='1'] != ''">
                <date>
                  <xsl:attribute name='type'>first_online</xsl:attribute>
                  <xsl:value-of select="marc:datafield[@tag='269']/marc:subfield[@code='1']"/>
                </date>
              </xsl:if>
              <xsl:if test="marc:datafield[@tag='269']/marc:subfield[@code='2'] != ''">
                <date>
                  <xsl:attribute name='type'>publication</xsl:attribute>
                  <xsl:value-of select="marc:datafield[@tag='269']/marc:subfield[@code='2']"/>
                </date>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise> <!-- for old records -->
          <xsl:choose>
            <xsl:when test="marc:datafield[@tag='260']/marc:subfield[@code='c'] != ''">
              <date>
                <xsl:attribute name='type'>publication</xsl:attribute>
                <xsl:value-of select="marc:datafield[@tag='260']/marc:subfield[@code='c']"/>
              </date>
            </xsl:when>
            <xsl:when test="marc:datafield[@tag='502']/marc:subfield[@code='9'] != ''">
              <date>
                <xsl:attribute name='type'>defense</xsl:attribute>
                <xsl:value-of select="translate(marc:datafield[@tag='502']/marc:subfield[@code='9'],'/','-')"/>
              </date>
            </xsl:when>
            <xsl:when test="marc:datafield[@tag='773']/marc:subfield[@code='y'] != ''">
              <date>
                <xsl:attribute name='type'>publication</xsl:attribute>
                <xsl:value-of select="marc:datafield[@tag='773']/marc:subfield[@code='y']"/>
              </date>
            </xsl:when>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </dates>

    <!-- **** ************ **** -->
    <!-- **** CONTRIBUTORS **** -->
    <!-- **** ************ **** -->
    <contributors>
      <xsl:for-each select="marc:datafield[@tag='700']">
        <contributor>
          <role>
            <xsl:choose>
              <xsl:when test="marc:subfield[@code='e'] = 'Ed.'">
                <xsl:text>editor</xsl:text>
              </xsl:when>
              <xsl:when test="marc:subfield[@code='e'] = 'Guest Ed.'">
                <xsl:text>guest_editor</xsl:text>
              </xsl:when>
              <xsl:when test="marc:subfield[@code='e'] = 'Collab.'">
                <xsl:text>collaborator</xsl:text>
              </xsl:when>
              <xsl:when test="marc:subfield[@code='e'] = 'Transl.'">
                <xsl:text>translator</xsl:text>
              </xsl:when>
              <xsl:when test="marc:subfield[@code='e'] = 'Photogr.'">
                <xsl:text>photographer</xsl:text>
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>author</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </role>
          <xsl:choose>
            <xsl:when test="contains(marc:subfield[@code='a'],',')">
              <lastname><xsl:value-of select="normalize-space(substring-before(marc:subfield[@code='a'],','))"/></lastname>
              <firstname><xsl:value-of select="normalize-space(substring-after(marc:subfield[@code='a'],','))"/></firstname>
            </xsl:when>
            <xsl:otherwise>
              <lastname><xsl:value-of select="marc:subfield[@code='a']"/></lastname>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="marc:subfield[@code='0'] != ''">
            <orcid><xsl:value-of select="marc:subfield[@code='0']"/></orcid>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='9'] != ''">
            <cn_individu><xsl:value-of select="marc:subfield[@code='9']"/></cn_individu>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='g'] != ''">
            <other_names>
              <xsl:for-each select="marc:subfield[@code='g']">
                <other_name>
                  <xsl:choose>
                    <xsl:when test="contains(.,',')">
                      <lastname><xsl:value-of select="normalize-space(substring-before(.,','))"/></lastname>
                      <firstname><xsl:value-of select="normalize-space(substring-after(.,','))"/></firstname>
                    </xsl:when>
                    <xsl:otherwise>
                      <lastname><xsl:value-of select="."/></lastname>
                    </xsl:otherwise>
                  </xsl:choose>
                </other_name>
              </xsl:for-each>
            </other_names>
          </xsl:if>
        </contributor>
      </xsl:for-each>
      <!-- **** Directeur these **** -->
      <xsl:for-each select="marc:datafield[@tag='508']">
        <contributor>
          <role><xsl:text>director</xsl:text></role>
          <xsl:choose>
            <xsl:when test="contains(marc:subfield[@code='a'],',')">
              <lastname><xsl:value-of select="normalize-space(substring-before(marc:subfield[@code='a'],','))"/></lastname>
              <firstname><xsl:value-of select="normalize-space(substring-after(marc:subfield[@code='a'],','))"/></firstname>
            </xsl:when>
            <xsl:otherwise>
              <lastname><xsl:value-of select="marc:subfield[@code='a']"/></lastname>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="marc:subfield[@code='0'] != ''">
            <orcid><xsl:value-of select="marc:subfield[@code='0']"/></orcid>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='9'] != ''">
            <cn_individu><xsl:value-of select="marc:subfield[@code='9']"/></cn_individu>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='g'] != ''">
            <other_names>
              <xsl:for-each select="marc:subfield[@code='g']">
                <other_name>
                  <xsl:choose>
                    <xsl:when test="contains(.,',')">
                      <lastname><xsl:value-of select="normalize-space(substring-before(.,','))"/></lastname>
                      <firstname><xsl:value-of select="normalize-space(substring-after(.,','))"/></firstname>
                    </xsl:when>
                    <xsl:otherwise>
                      <lastname><xsl:value-of select="."/></lastname>
                    </xsl:otherwise>
                  </xsl:choose>
                </other_name>
              </xsl:for-each>
            </other_names>
          </xsl:if>
        </contributor>
      </xsl:for-each>
      <!-- **** Collaboration **** -->
      <xsl:for-each select="marc:datafield[@tag='710']">
        <xsl:if test="marc:subfield[@code='e'] = 'Collaboration' and marc:subfield[@code='a'] != 'Collaboration'">
          <collaboration>
            <name>
              <xsl:value-of select="marc:subfield[@code='a']"/>
            </name>
          </collaboration>
        </xsl:if>
      </xsl:for-each>
    </contributors>

    <!-- **** ***** **** -->
    <!-- **** PAGES **** -->
    <!-- **** ***** **** -->
    <xsl:choose>
      <xsl:when test="marc:datafield[@tag='300'] and $view != 'article' and $subview != 'chap_act' and $subview != 'chap_liv' and $subview != 'contrib'">
        <pages>
          <paging><xsl:value-of select="translate(marc:datafield[@tag='300']/marc:subfield[@code='a'],' p.','')"/></paging>
        </pages>
      </xsl:when>
      <xsl:when test="marc:datafield[@tag='773']/marc:subfield[@code='p'] != ''">
        <pages>
          <xsl:choose>
            <xsl:when test="contains(marc:datafield[@tag='773']/marc:subfield[@code='p'],'-') or contains(marc:datafield[@tag='773']/marc:subfield[@code='p'],'–')">
              <paging><xsl:value-of select="translate(marc:datafield[@tag='773']/marc:subfield[@code='p'],'– ','-')"/></paging>
            </xsl:when>
            <xsl:when test="$view = 'article'">
              <other><xsl:value-of select="marc:datafield[@tag='773']/marc:subfield[@code='p']"/></other>
            </xsl:when>
            <xsl:otherwise>
              <paging><xsl:value-of select="translate(marc:datafield[@tag='773']/marc:subfield[@code='p'],' p.','')"/></paging>
            </xsl:otherwise>
          </xsl:choose>
        </pages>
      </xsl:when>
    </xsl:choose>

    <!-- **** *********** **** -->
    <!-- **** IDENTIFIERS **** -->
    <!-- **** *********** **** -->
    <xsl:if test="marc:datafield[@tag='020'] or marc:datafield[@tag='022'] or marc:datafield[@tag='024'] or marc:datafield[@tag='088'] or marc:datafield[@tag='502'] or marc:datafield[@tag='856']/marc:subfield[@code='3'] = 'URN'">
      <identifiers>
        <xsl:if test="marc:datafield[@tag='020']">
          <isbn><xsl:value-of select="marc:datafield[@tag='020']/marc:subfield[@code='a']"/></isbn>
        </xsl:if>
        <xsl:if test="marc:datafield[@tag='022']">
          <issn><xsl:value-of select="marc:datafield[@tag='022']/marc:subfield[@code='a']"/></issn>
        </xsl:if>
        <xsl:for-each select="marc:datafield[@tag='024']">
          <xsl:if test="marc:subfield[@code='2'] = 'DOI'">
            <doi><xsl:value-of select="marc:subfield[@code='a']"/></doi>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='2'] = 'PMID'">
            <pmid><xsl:value-of select="marc:subfield[@code='a']"/></pmid>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='2'] = 'PMCID'">
            <pmcid><xsl:value-of select="marc:subfield[@code='a']"/></pmcid>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='2'] = 'arxiv'">
            <arxiv><xsl:value-of select="marc:subfield[@code='a']"/></arxiv>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='2'] = 'DBLP'">
            <dblp><xsl:value-of select="marc:subfield[@code='a']"/></dblp>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='2'] = 'Swisscovery'">
            <mmsid><xsl:value-of select="marc:subfield[@code='a']"/></mmsid>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='2'] = 'URN'">
            <urn><xsl:value-of select="marc:subfield[@code='a']"/></urn>
          </xsl:if>
        </xsl:for-each>
        <xsl:choose>
          <xsl:when test="marc:datafield[@tag='502']/marc:subfield[@code='8'] != ''">
            <local_number><xsl:value-of select="marc:datafield[@tag='502']/marc:subfield[@code='8']"/></local_number>
          </xsl:when>
          <xsl:when test="marc:datafield[@tag='088']">
            <xsl:if test="marc:datafield[@tag='088']/marc:subfield[@code='a'] != marc:datafield[@tag='490']/marc:subfield[@code='v']">
              <local_number><xsl:value-of select="marc:datafield[@tag='088']/marc:subfield[@code='a']"/></local_number>
            </xsl:if>
          </xsl:when>
        </xsl:choose>
      </identifiers>
    </xsl:if>

    <!-- **** ************* **** -->
    <!-- **** PUBLISHER URL **** -->
    <!-- **** ************* **** -->
    <xsl:for-each select="marc:datafield[@tag='856']">
      <xsl:if test="marc:subfield[@code='3'] = 'Alternate edition'">
        <publisher_version_url><xsl:value-of select="marc:subfield[@code='u']"/></publisher_version_url>
      </xsl:if>
    </xsl:for-each>

    <!-- **** ********* **** -->
    <!-- **** ABSTRACTS **** -->
    <!-- **** ********* **** -->
    <xsl:if test="marc:datafield[@tag='520']">
      <abstracts>
        <xsl:for-each select="marc:datafield[@tag='520']">
          <abstract>
            <xsl:if test="marc:subfield[@code='9'] != ''">
              <xsl:attribute name='lang'><xsl:value-of select="marc:subfield[@code='9']"/></xsl:attribute>
            </xsl:if>
            <xsl:value-of select="marc:subfield[@code='a']"/>
          </abstract>
        </xsl:for-each>
      </abstracts>
    </xsl:if>

    <!-- **** **** **** -->
    <!-- **** NOTE **** -->
    <!-- **** **** **** -->
    <xsl:choose>
      <xsl:when test="contains(marc:datafield[@tag='502']/marc:subfield[@code='a'],'Genève') and contains(marc:datafield[@tag='502']/marc:subfield[@code='a'],'Lausanne')">
        <note><xsl:text>Diplôme commun des univ. de Genève et Lausanne</xsl:text>
          <xsl:if test="marc:datafield[@tag='500']">
            <xsl:text>. </xsl:text>
            <xsl:value-of select="marc:datafield[@tag='500']/marc:subfield[@code='a']"/>
          </xsl:if>
        </note>
      </xsl:when>
      <xsl:when test="contains(marc:datafield[@tag='502']/marc:subfield[@code='a'],'Genève')">
        <xsl:if test="marc:datafield[@tag='500']">
          <note><xsl:value-of select="marc:datafield[@tag='500']/marc:subfield[@code='a']"/></note>
        </xsl:if>
      </xsl:when>
      <xsl:when test="marc:datafield[@tag='502']/marc:subfield[@code='a'] != ''">
        <note><xsl:text>Diplôme décerné par </xsl:text><xsl:value-of select="substring-before(substring-after(marc:datafield[@tag='502']/marc:subfield[@code='a'],' : '),', ')"/>
          <xsl:if test="marc:datafield[@tag='500']">
            <xsl:text>. </xsl:text>
            <xsl:value-of select="marc:datafield[@tag='500']/marc:subfield[@code='a']"/>
          </xsl:if>
        </note>
      </xsl:when>
      <xsl:when test="marc:datafield[@tag='500']">
        <note><xsl:value-of select="marc:datafield[@tag='500']/marc:subfield[@code='a']"/></note>
      </xsl:when>
    </xsl:choose>

    <!-- **** ******** **** -->
    <!-- **** KEYWORDS **** -->
    <!-- **** ******** **** -->
    <xsl:if test="marc:datafield[@tag='653']">
      <keywords>
        <xsl:for-each select="marc:datafield[@tag='653']">
          <keyword><xsl:value-of select="marc:subfield[@code='a']"/></keyword>
        </xsl:for-each>
      </keywords>
    </xsl:if>

    <!-- **** ************** **** -->
    <!-- **** CLASSIFICATION **** -->
    <!-- **** ************** **** -->
    <xsl:if test="marc:datafield[@tag='082'] or marc:datafield[@tag='084']">
      <classifications>
        <xsl:for-each select="marc:datafield[@tag='082']">
          <classification>
            <code><xsl:text>Dewey</xsl:text></code>
            <item><xsl:value-of select="marc:subfield[@code='a']"/></item>
          </classification>
        </xsl:for-each>
        <xsl:for-each select="marc:datafield[@tag='084']">
          <xsl:choose>
            <xsl:when test="marc:subfield[@code='2'] = 'repec'">
              <xsl:choose>
                <xsl:when test="contains(marc:subfield[@code='a'],',')">
                  <classification>
                    <code>JEL</code>
                    <item><xsl:value-of select="normalize-space(substring-before(marc:subfield[@code='a'],','))"/></item>
                  </classification>
                  <xsl:call-template name="extraitJEL">
                    <xsl:with-param name="chaineJELS" select="normalize-space(substring-after(marc:subfield[@code='a'],','))"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                  <classification>
                    <code>JEL</code>
                    <item><xsl:value-of select="marc:subfield[@code='a']"/></item>
                  </classification>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <classification>
                <code><xsl:value-of select="marc:subfield[@code='2']"/></code>
                <item><xsl:value-of select="marc:subfield[@code='a']"/></item>
              </classification>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </classifications>
    </xsl:if>

    <!-- **** ********** **** -->
    <!-- **** DISCIPLINE **** -->
    <!-- **** ********** **** -->
    <xsl:if test="marc:datafield[@tag='502']/marc:subfield[@code='m'] != ''">
      <discipline>
        <xsl:value-of select="marc:datafield[@tag='502']/marc:subfield[@code='m']"/>
      </discipline>
    </xsl:if>

    <!-- **** ******** **** -->
    <!-- **** MANDATOR **** -->
    <!-- **** ******** **** -->
    <xsl:for-each select="marc:datafield[@tag='710']">
      <xsl:choose>
        <xsl:when test="marc:subfield[@code='e'] = 'Collaboration'">
        </xsl:when>
        <xsl:otherwise>
          <mandator><xsl:value-of select="marc:subfield[@code='a']"/></mandator>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>

    <!-- **** ********* **** -->
    <!-- **** PUBLISHER **** -->
    <!-- **** ********* **** -->
    <xsl:for-each select="marc:datafield[@tag='260']">
      <xsl:if test="marc:subfield[@code='a'] or marc:subfield[@code='b']">
        <publisher>
          <xsl:if test="marc:subfield[@code='a'] and marc:subfield[@code='a'] != '[S.l.]'">
            <place>
              <xsl:value-of select="marc:subfield[@code='a']"/>
            </place>
          </xsl:if>
          <xsl:if test="marc:subfield[@code='b'] and marc:subfield[@code='b'] != '[s.n.]'">
            <name>
              <xsl:value-of select="marc:subfield[@code='b']"/>
            </name>
          </xsl:if>
        </publisher>
      </xsl:if>
    </xsl:for-each>

    <!-- **** ********** **** -->
    <!-- **** COLLECTION **** -->
    <!-- **** ********** **** -->
    <xsl:if test="marc:datafield[@tag='490']/marc:subfield[@code='a'] != '' or marc:datafield[@tag='981']/marc:subfield[@code='a'] != ''">
      <collections>
        <xsl:for-each select="marc:datafield[@tag='490']">
          <collection>
            <name>
              <xsl:value-of select="marc:subfield[@code='a']"/>
            </name>
            <xsl:if test="marc:subfield[@code='v']">
              <number>
                <xsl:value-of select="marc:subfield[@code='v']"/>
              </number>
            </xsl:if>
          </collection>
        </xsl:for-each>
        <xsl:for-each select="marc:datafield[@tag='981']">
          <collection>
            <name>
              <xsl:value-of select="marc:subfield[@code='a']"/>
            </name>
            <xsl:if test="marc:subfield[@code='v']">
              <number>
                <xsl:value-of select="marc:subfield[@code='v']"/>
              </number>
            </xsl:if>
          </collection>
        </xsl:for-each>
      </collections>
    </xsl:if>

    <!-- **** ******* **** -->
    <!-- **** EDITION **** -->
    <!-- **** ******* **** -->
    <xsl:if test="marc:datafield[@tag='250']/marc:subfield[@code='a'] != ''">
      <edition>
        <xsl:value-of select="marc:datafield[@tag='250']/marc:subfield[@code='a']"/>
      </edition>
    </xsl:if>

    <!-- **** ***** **** -->
    <!-- **** AWARD **** -->
    <!-- **** ***** **** -->
    <xsl:if test="marc:datafield[@tag='586']/marc:subfield[@code='a'] != ''">
      <award>
        <xsl:value-of select="marc:datafield[@tag='586']/marc:subfield[@code='a']"/>
      </award>
    </xsl:if>

    <!-- **** ******** **** -->
    <!-- **** FUNDINGS **** -->
    <!-- **** ******** **** -->
    <xsl:if test="marc:datafield[@tag='988']/marc:subfield[@code='f'] != ''">
      <fundings>
        <xsl:for-each select="marc:datafield[@tag='988']">
          <funding>
            <funder>
              <xsl:value-of select="marc:subfield[@code='f']"/>
            </funder>
            <xsl:if test="marc:subfield[@code='n']">
              <name>
                <xsl:value-of select="marc:subfield[@code='n']"/>
              </name>
            </xsl:if>
            <xsl:choose>
              <xsl:when test="marc:subfield[@code='c'] and marc:subfield[@code='f'] = 'Swiss National Science Foundation'">
                <xsl:choose>
                  <xsl:when test="contains(marc:subfield[@code='c'],'-')">
                    <xsl:call-template name="extraitFNS">
                      <xsl:with-param name="codeFNS" select="substring-after(translate(marc:subfield[@code='c'],'_','-'),'-')"/>
                    </xsl:call-template>
                  </xsl:when>
                  <xsl:otherwise>
                    <code>
                      <xsl:value-of select="marc:subfield[@code='c']"/>
                    </code>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:if test="marc:subfield[@code='c']">
                  <code>
                    <xsl:value-of select="marc:subfield[@code='c']"/>
                  </code>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="marc:subfield[@code='a']">
              <acronym>
                <xsl:value-of select="marc:subfield[@code='a']"/>
              </acronym>
            </xsl:if>
            <xsl:if test="marc:subfield[@code='j']">
              <jurisdiction>
                <xsl:value-of select="marc:subfield[@code='j']"/>
              </jurisdiction>
            </xsl:if>
            <xsl:if test="marc:subfield[@code='2']">
              <program>
                <xsl:value-of select="marc:subfield[@code='2']"/>
              </program>
            </xsl:if>
          </funding>
        </xsl:for-each>
      </fundings>
    </xsl:if>

    <!-- **** ********** **** -->
    <!-- **** STRUCTURES **** -->
    <!-- **** ********** **** -->
    <xsl:if test="marc:datafield[@tag='928']/marc:subfield[@code='a'] != ''">
      <academic_structures>
        <xsl:for-each select="marc:datafield[@tag='928']">
          <academic_structure>
            <resId></resId>
            <xsl:choose>
              <xsl:when test="contains(marc:subfield[@code='a'],';')">
                <name>
                  <xsl:call-template name="extraitStruct">
                    <xsl:with-param name="chaineStructures" select="marc:subfield[@code='a']"/>
                  </xsl:call-template>
                </name>
              </xsl:when>
              <xsl:otherwise>
                <name>
                  <xsl:value-of select="marc:subfield[@code='a']"/>
                </name>
              </xsl:otherwise>
            </xsl:choose>
            <code>
              <xsl:value-of select="marc:subfield[@code='c']"/>
            </code>
          </academic_structure>
        </xsl:for-each>
      </academic_structures>
    </xsl:if>

    <!-- **** ***** **** -->
    <!-- **** UNIGE **** -->
    <!-- **** ***** **** -->
    <xsl:choose>
      <xsl:when test="marc:datafield[@tag='920']/marc:subfield[@code='a'] != ''">
        <isBeforeUnige>false</isBeforeUnige>
      </xsl:when>
      <xsl:otherwise>
        <isBeforeUnige>true</isBeforeUnige>
      </xsl:otherwise>
    </xsl:choose>

    <!-- **** ****** **** -->
    <!-- **** GROUPS **** -->
    <!-- **** ****** **** -->
    <xsl:if test="marc:datafield[@tag='927']/marc:subfield[@code='a'] != ''">
      <groups>
        <xsl:for-each select="marc:datafield[@tag='927']">
          <group>
            <resId>
              <xsl:value-of select="marc:subfield[@code='c']"/>
            </resId>
            <xsl:choose>
              <xsl:when test="contains(marc:subfield[@code='a'],'(')">
                <xsl:variable name="Debut">
                  <xsl:value-of select="substring-before(marc:subfield[@code='a'],' (')"/>
                </xsl:variable>
                <xsl:variable name="Apres">
                  <xsl:value-of select="substring-after(marc:subfield[@code='a'],'(')"/>
                </xsl:variable>
                <xsl:choose>
                  <xsl:when test="contains($Apres,'(')">
                    <xsl:variable name="Milieu">
                      <xsl:value-of select="substring-before($Apres,' (')"/>
                    </xsl:variable>
                    <xsl:variable name="Fin">
                      <xsl:value-of select="substring-before(substring-after($Apres,'('),')')"/>
                    </xsl:variable>
                    <xsl:choose>
                      <xsl:when test="floor($Fin) = $Fin">
                        <name>
                          <xsl:value-of select="concat($Debut,' (',$Milieu)"/>
                        </name>
                        <code>
                          <xsl:value-of select="$Fin"/>
                        </code>
                      </xsl:when>
                      <xsl:otherwise>
                        <name>
                          <xsl:value-of select="marc:subfield[@code='a']"/>
                        </name>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:variable name="Fin">
                      <xsl:value-of select="substring-before($Apres,')')"/>
                    </xsl:variable>
                    <xsl:choose>
                      <xsl:when test="floor($Fin) = $Fin">
                        <name>
                          <xsl:value-of select="$Debut"/>
                        </name>
                        <code>
                          <xsl:value-of select="$Fin"/>
                        </code>
                      </xsl:when>
                      <xsl:otherwise>
                        <name>
                          <xsl:value-of select="marc:subfield[@code='a']"/>
                        </name>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <name>
                  <xsl:value-of select="marc:subfield[@code='a']"/>
                </name>
              </xsl:otherwise>
            </xsl:choose>
          </group>
        </xsl:for-each>
      </groups>
    </xsl:if>

    <!-- **** ******** **** -->
    <!-- **** DATASETS **** -->
    <!-- **** ******** **** -->
    <datasets>
      <xsl:for-each select="marc:datafield[@tag='856']">
        <xsl:if test="marc:subfield[@code='3'] = 'Dataset'">
            <xsl:choose>
              <xsl:when test="marc:subfield[@code='3'] = 'Dataset'">
                <url>
                  <xsl:value-of select="marc:subfield[@code='u']"/>
                </url>
              </xsl:when>
            </xsl:choose>
        </xsl:if>
      </xsl:for-each>
    </datasets>

    <!-- **** ********* **** -->
    <!-- **** CONTAINER **** -->
    <!-- **** ********* **** -->
    <xsl:for-each select="marc:datafield[@tag='773']">
      <container>
        <title>
          <xsl:value-of select="marc:subfield[@code='t']"/>
        </title>
        <xsl:if test="marc:subfield[@code='a'] != ''">
          <editor>
            <xsl:value-of select="marc:subfield[@code='a']"/>
          </editor>
        </xsl:if>
        <xsl:if test="marc:subfield[@code='v'] != ''">
          <volume>
            <xsl:value-of select="marc:subfield[@code='v']"/>
          </volume>
        </xsl:if>
        <xsl:if test="marc:subfield[@code='n'] != ''">
          <issue>
            <xsl:value-of select="marc:subfield[@code='n']"/>
          </issue>
        </xsl:if>
        <xsl:if test="marc:subfield[@code='h'] != '' and marc:subfield[@code='h'] != 'C'">
          <special_issue>
            <xsl:value-of select="marc:subfield[@code='h']"/>
          </special_issue>
        </xsl:if>
        <xsl:if test="marc:subfield[@code='c'] != ''">
          <conference_subtitle>
            <xsl:value-of select="marc:subfield[@code='c']"/>
          </conference_subtitle>
        </xsl:if>
        <xsl:if test="marc:subfield[@code='j'] != ''">
          <conference_date>
            <xsl:value-of select="marc:subfield[@code='j']"/>
          </conference_date>
        </xsl:if>
        <xsl:if test="marc:subfield[@code='l'] != ''">
          <conference_place>
            <xsl:value-of select="marc:subfield[@code='l']"/>
          </conference_place>
        </xsl:if>
      </container>
    </xsl:for-each>

    <!-- **** *********** **** -->
    <!-- **** CORRECTIONS **** -->
    <!-- **** *********** **** -->
    <xsl:if test="marc:datafield[@tag='525']/marc:subfield[@code='a'] != ''">
      <corrections>
        <xsl:for-each select="marc:datafield[@tag='525']">
          <correction>
            <note>
              <xsl:value-of select="marc:subfield[@code='a']"/>
            </note>
            <xsl:if test="marc:subfield[@code='d'] != ''">
              <doi>
                <xsl:value-of select="marc:subfield[@code='d']"/>
              </doi>
            </xsl:if>
            <xsl:if test="marc:subfield[@code='p'] != ''">
              <pmid>
                <xsl:value-of select="marc:subfield[@code='p']"/>
              </pmid>
            </xsl:if>
          </correction>
        </xsl:for-each>
      </corrections>
    </xsl:if>
    </deposit_doc>
  </xsl:template>


  <xsl:template name="extraitStruct">
    <xsl:param name="chaineStructures"/>
    <xsl:variable name="maStructure">
      <xsl:value-of select="substring-after($chaineStructures,'; ')"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="contains($maStructure,';')">
        <xsl:call-template name="extraitStruct">
          <xsl:with-param name="chaineStructures" select="substring-after($chaineStructures,'; ')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$maStructure"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="extraitJEL">
    <xsl:param name="chaineJELS"/>
    <xsl:choose>
      <xsl:when test="contains($chaineJELS,',')">
        <classification>
          <code>JEL</code>
          <item><xsl:value-of select="normalize-space(substring-before($chaineJELS,','))"/></item>
        </classification>
        <xsl:call-template name="extraitJEL">
          <xsl:with-param name="chaineJELS" select="substring-after($chaineJELS,',')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <classification>
          <code>JEL</code>
          <item><xsl:value-of select="normalize-space($chaineJELS)"/></item>
        </classification> 
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Pour ne conserver que les 5 ou 6 derniers chiffres du code -->
  <xsl:template name="extraitFNS">
    <xsl:param name="codeFNS"/>
    <xsl:choose>
      <xsl:when test="contains($codeFNS,'-')">
        <xsl:call-template name="extraitFNS">
          <xsl:with-param name="codeFNS" select="substring-after($codeFNS,'-')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <code>
          <xsl:value-of select="$codeFNS"/>
        </code>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
