-- ====================================================================
-- THIS FILE CONTAINS UPGRADES THAT MAY BE NECESSARY DURING DEVELOPMENT
-- ====================================================================


-- ====================================================================
-- 2024-05-28
-- ====================================================================

-- New ScheduledTask type SYNCHRONIZE_ORCID_PROFILE
ALTER TABLE `scheduled_task`
    MODIFY COLUMN task_type enum ('NOTIFY_DEPOSITS_TO_VALIDATE','NOTIFY_VALIDATED_DEPOSITS_DEPOSITOR','NOTIFY_VALIDATED_DEPOSITS_CONTRIBUTORS',
        'NOTIFY_DEPOSITS_REJECTED','NOTIFY_VALIDATORS_ON_COMMENTS','NOTIFY_CONTRIBUTORS_ON_COMMENTS','NOTIFY_DEPOSITORS_ON_COMMENTS',
        'NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE','NOTIFY_FORGOTTEN_DEPOSITS_IN_PROGRESS','SYNCHRONIZE_RESEARCH_GROUPS',
        'NEW_RESEARCH_GROUPS_TO_VALIDATE','REINDEX_PUBLICATIONS_ENDING_EMBARGO','SYNC_THESES_TO_SWISS_NATIONAL_LIBRARY',
        'SYNCHRONIZE_ORCID_PROFILE') NOT NULL;

-- New Publication import sources
ALTER TABLE `publication`
    MODIFY COLUMN import_source enum ('DOI','ARXIV','PMID','ISBN','DOI_ORCID','ARXIV_ORCID','PMID_ORCID','ISBN_ORCID','ORCID',
        'CLONE','FEDORA3') NULL;

-- New 'sync_from_orcid_profile' field on Person with default value to 'false'
ALTER TABLE `person`
    ADD COLUMN `sync_from_orcid_profile` varchar(255) NOT NULL CHECK (`sync_from_orcid_profile` in ('false', 'true'));

UPDATE `person`
SET `sync_from_orcid_profile`='false';

-- New status "UPDATE_IN_BATCHfor publication
ALTER TABLE publication MODIFY COLUMN status enum('IN_PROGRESS','IN_VALIDATION','FEEDBACK_REQUIRED','REJECTED','SUBMITTED','CHECKED',
    'IN_PREPARATION','COMPLETED','CANONICAL','DELETED','IN_ERROR','IN_EDITION','UPDATES_VALIDATION','CANCEL_EDITION', 'UPDATE_IN_BATCH') NOT NULL;

-- New status "UPDATE_IN_BATCHfor publication
ALTER TABLE publication MODIFY COLUMN status enum('IN_PROGRESS','IN_VALIDATION','FEEDBACK_REQUIRED','REJECTED','SUBMITTED','CHECKED',
    'IN_PREPARATION','COMPLETED','CANONICAL','DELETED','IN_ERROR','IN_EDITION','UPDATES_VALIDATION','CANCEL_EDITION', 'UPDATE_IN_BATCH') NOT NULL;

ALTER TABLE scheduled_task MODIFY COLUMN task_type enum('NOTIFY_DEPOSITS_TO_VALIDATE','NOTIFY_VALIDATED_DEPOSITS_DEPOSITOR','NOTIFY_VALIDATED_DEPOSITS_CONTRIBUTORS',
    'NOTIFY_DEPOSITS_REJECTED','NOTIFY_VALIDATORS_ON_COMMENTS','NOTIFY_CONTRIBUTORS_ON_COMMENTS','NOTIFY_DEPOSITORS_ON_COMMENTS','NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE',
    'NOTIFY_FORGOTTEN_DEPOSITS_IN_PROGRESS','SYNCHRONIZE_RESEARCH_GROUPS','NEW_RESEARCH_GROUPS_TO_VALIDATE','REINDEX_PUBLICATIONS_ENDING_EMBARGO',
    'SYNC_THESES_TO_SWISS_NATIONAL_LIBRARY','SYNCHRONIZE_ORCID_PROFILE', 'CLEAN_DEPOSIT_UPDATED_WITHOUT_CHANGES') NOT NULL

-- ====================================================================
-- 2024-10-14
-- ====================================================================

-- New event type PUBLICATION_UPDATED_IN_BATCH
INSERT INTO `event_type` (`res_id`, `creation_when`, `creation_who`, `last_update_when`, `last_update_who`, `name`) VALUES ('PUBLICATION_UPDATED_IN_BATCH', NOW(), NULL, NOW(), NULL, 'Publication updated in batch');

-- PUBLICATION_EXPORTED_TO_ORCID_ID resId renamed into PUBLICATION_EXPORTED_TO_ORCID
UPDATE `event_type` SET name = 'Publication exported to ORCID (old)' WHERE res_id = 'PUBLICATION_EXPORTED_TO_ORCID_ID';
INSERT INTO `event_type` (`res_id`, `creation_when`, `creation_who`, `last_update_when`, `last_update_who`, `name`) VALUES ('PUBLICATION_EXPORTED_TO_ORCID', NOW(), NULL, NOW(), NULL, 'Publication exported to ORCID');
UPDATE `notification_type` SET event_type_res_id = 'PUBLICATION_EXPORTED_TO_ORCID' WHERE event_type_res_id = 'PUBLICATION_EXPORTED_TO_ORCID_ID';
UPDATE `event` SET event_type_id = 'PUBLICATION_EXPORTED_TO_ORCID' WHERE event_type_id = 'PUBLICATION_EXPORTED_TO_ORCID_ID';
DELETE FROM `event_type` WHERE res_id = 'PUBLICATION_EXPORTED_TO_ORCID_ID';

-- Update sort and labels of publication subtype
UPDATE publication_subtype SET sort_value = 70 WHERE res_id = 'C1';
UPDATE publication_subtype_labels SET `text` = 'Proceedings (set of individual contributions)' WHERE publication_subtype_id = 'C1' AND language_id = 'ENG';
UPDATE publication_subtype_labels SET `text` = 'Actes (ensemble de contributions individuelles)' WHERE publication_subtype_id = 'C1' AND language_id = 'FRE';

UPDATE publication_subtype SET sort_value = 40 WHERE res_id = 'C4';
UPDATE publication_subtype_labels SET `text` = 'Chapter / Article (individual contribution to proceedings)' WHERE publication_subtype_id = 'C4' AND language_id = 'ENG';
UPDATE publication_subtype_labels SET `text` = 'Chapitre / Article (contribution parue dans des actes)' WHERE publication_subtype_id = 'C4' AND language_id = 'FRE';

-- ====================================================================
-- 2025-01-20
-- ====================================================================
-- ORCID tokens can have empty names
ALTER TABLE `orcid_token` CHANGE `name` `name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;
