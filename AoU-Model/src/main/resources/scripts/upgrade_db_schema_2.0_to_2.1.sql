DROP PROCEDURE IF EXISTS `create_status_sequence`;

DELIMITER //

CREATE PROCEDURE create_status_sequence(IN seq_start_value INT)
BEGIN
    SET @create_sequence_sql = CONCAT('CREATE SEQUENCE status_history_seq START WITH ', seq_start_value, ' INCREMENT BY 50');
    PREPARE stmt FROM @create_sequence_sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END //

DELIMITER ;

DROP SEQUENCE IF EXISTS status_history_seq;
SET @next_hibernate_sequence_val = NEXT VALUE FOR hibernate_sequence;
SET @next_status_history_seq_val = @next_hibernate_sequence_val + 1000; -- Increment by 1000 which is the cache size
CALL create_status_sequence(@next_status_history_seq_val);

DROP TABLE IF EXISTS `hibernate_sequence`;

-- citation
ALTER TABLE `citation`
    MODIFY COLUMN `mode` enum ('BIBLIOGRAPHY','CITATION') NOT NULL;
ALTER TABLE `citation`
    MODIFY COLUMN `output_format` enum ('TEXT','HTML','ASCII_DOC','FO','RTF') NOT NULL;

-- comment
ALTER TABLE `comment`
    MODIFY COLUMN `only_for_validators` varchar(255) NOT NULL CHECK (`only_for_validators` in ('false', 'true'));

-- document_file
ALTER TABLE `document_file`
    MODIFY COLUMN `access_level` enum ('PUBLIC','RESTRICTED','PRIVATE') NOT NULL;
ALTER TABLE `document_file`
    MODIFY COLUMN `embargo_access_level` enum ('PUBLIC','RESTRICTED','PRIVATE') NULL;
ALTER TABLE `document_file`
    MODIFY COLUMN `is_imported` varchar(255) DEFAULT NULL CHECK (`is_imported` in ('false', 'true'));
ALTER TABLE `document_file`
    MODIFY COLUMN `not_sure_for_license` varchar(255) NOT NULL CHECK (`not_sure_for_license` in ('false', 'true'));
ALTER TABLE `document_file`
    MODIFY COLUMN `status` enum ('RECEIVED','TO_PROCESS','PROCESSED','READY','CLEANING','DUPLICATE','IN_ERROR','TO_BE_CONFIRMED','CONFIRMED','IMPOSSIBLE_TO_DOWNLOAD') DEFAULT NULL;

-- download_token
ALTER TABLE `download_token`
    DROP IF EXISTS resource_id;
ALTER TABLE `download_token` RENAME COLUMN archive_id TO resource_id;
-- resource_type rajouté automatiquement


-- global_banner
ALTER TABLE `global_banner`
    MODIFY COLUMN `enabled` varchar(255) NOT NULL CHECK (`enabled` in ('false', 'true'));
ALTER TABLE `global_banner`
    MODIFY COLUMN `type` enum ('CRITICAL','WARNING','INFO') NOT NULL;


-- index_field_alias
ALTER TABLE `index_field_alias`
    MODIFY COLUMN `facet` varchar(255) NOT NULL CHECK (`facet` in ('false', 'true'));
ALTER TABLE `index_field_alias`
    MODIFY COLUMN `system` varchar(255) NOT NULL CHECK (`system` in ('false', 'true'));

-- institution_logo
ALTER TABLE `institution_logo`
    DROP IF EXISTS file_content;
ALTER TABLE `institution_logo` RENAME COLUMN image_content TO file_content;
ALTER TABLE `institution`
    ADD UNIQUE `UK_g851qvtrykitppcs2fw8pr6qx` (logo_id);
ALTER TABLE `institution`
    DROP KEY IF EXISTS FKeq684it95m563so9ff0pmvh85;


-- labeled_language_labels
ALTER TABLE `labeled_language_labels`
    ADD PRIMARY KEY (`language_entity_id`, `language_id`, `text`);

-- license
ALTER TABLE `license`
    MODIFY COLUMN `visible` varchar(255) NOT NULL CHECK (`visible` in ('false', 'true'));

-- license_group
ALTER TABLE `license_group`
    MODIFY COLUMN `visible` varchar(255) NOT NULL CHECK (`visible` in ('false', 'true'));

-- oai_metadata_prefix
ALTER TABLE `oai_metadata_prefix`
    MODIFY COLUMN `enabled` varchar(255) NOT NULL CHECK (`enabled` in ('false', 'true'));
ALTER TABLE `oai_metadata_prefix`
    MODIFY COLUMN `metadata_schema` mediumtext DEFAULT NULL;
ALTER TABLE `oai_metadata_prefix`
    MODIFY COLUMN `metadata_xml_transformation` mediumtext DEFAULT NULL;
ALTER TABLE `oai_metadata_prefix`
    MODIFY COLUMN `reference` varchar(255) NOT NULL CHECK (`reference` in ('false', 'true'));

-- oai_set
ALTER TABLE `oai_set`
    MODIFY COLUMN `enabled` varchar(255) NOT NULL CHECK (`enabled` in ('false', 'true'));

-- person
ALTER TABLE `person`
    MODIFY COLUMN `verified_orcid` varchar(255) DEFAULT NULL CHECK (`verified_orcid` in ('false', 'true'));
ALTER TABLE `person`
    ADD COLUMN `sync_from_orcid_profile` varchar(255) NOT NULL CHECK (`sync_from_orcid_profile` in ('false', 'true'));
UPDATE `person`
SET `sync_from_orcid_profile`='false';
ALTER TABLE `person`
    ADD COLUMN `sync_to_orcid_profile` varchar(255) NOT NULL CHECK (`sync_to_orcid_profile` in ('false', 'true'));
UPDATE `person`
SET `sync_to_orcid_profile`='false';
ALTER TABLE `person`
    ADD UNIQUE `UK_dhp3nnpty2rfxbfx0k2crbn3o` (`avatar_id`);
ALTER TABLE `person`
    DROP KEY IF EXISTS `FKsp25avsyok9j9uycmi0jl8wko`;

-- person_avatar
ALTER TABLE `person_avatar` RENAME COLUMN image_content TO file_content;

-- publication
ALTER TABLE `publication`
    MODIFY COLUMN `form_step` enum ('TYPE','FILES','CONTRIBUTORS','DESCRIPTION','SUMMARY') NOT NULL;
ALTER TABLE `publication`
    MODIFY COLUMN `import_source` enum ('DOI','ARXIV','PMID','ISBN','DOI_ORCID','ARXIV_ORCID','PMID_ORCID','ISBN_ORCID', 'ORCID', 'CLONE','FEDORA3') NULL;
ALTER TABLE `publication`
    MODIFY COLUMN `metadata_version` enum ('V1_0','V2_0','V2_1','V2_2','V2_3','V2_4') NOT NULL;
ALTER TABLE `publication`
    MODIFY COLUMN `status` enum ('IN_PROGRESS','IN_VALIDATION','FEEDBACK_REQUIRED','REJECTED','SUBMITTED','CHECKED',
        'IN_PREPARATION','COMPLETED','CANONICAL','DELETED','IN_ERROR','IN_EDITION','UPDATES_VALIDATION','CANCEL_EDITION', 'UPDATE_IN_BATCH') NOT NULL;
ALTER TABLE `publication`
    DROP KEY IF EXISTS `StatusIndex`;
ALTER TABLE `publication`
    DROP KEY IF EXISTS `archiveIdIndex`;
ALTER TABLE `publication`
    DROP KEY IF EXISTS `publicationCreationWhenIndex`;
ALTER TABLE `publication`
    ADD UNIQUE `UK_f3q988ku647phnan8c5if77ah` (`thumbnail_id`);
ALTER TABLE `publication`
    DROP KEY IF EXISTS `FKf43ndrh6ymqibgok8qaw4p82m`;

-- publication_download
ALTER TABLE `publication_download`
    MODIFY COLUMN `status` enum ('IN_PROGRESS','SUBMITTED','IN_PREPARATION','DOWNLOADING','READY','GENERATING_COVER_PAGE','IN_ERROR') NOT NULL;

-- publication_structures
ALTER TABLE `publication_structures`
    MODIFY COLUMN `link_type` enum ('METADATA','VALIDATION') NOT NULL;

-- publication_subtype_contributor_role
ALTER TABLE `publication_subtype_contributor_role`
    MODIFY COLUMN `visible` varchar(255) NOT NULL CHECK (`visible` in ('false', 'true'));

-- publication_subtype_document_file_type
ALTER TABLE `publication_subtype_document_file_type`
    MODIFY COLUMN `level` enum ('PRINCIPAL','SECONDARY','CORRECTIVE','ADMINISTRATION') NOT NULL;

-- publication_thumbnail
ALTER TABLE `publication_thumbnail`
    DROP IF EXISTS file_content;
ALTER TABLE `publication_thumbnail` RENAME COLUMN image_content TO file_content;

-- research_group
ALTER TABLE `research_group`
    MODIFY COLUMN `active` varchar(255) NOT NULL CHECK (`active` in ('false', 'true'));
ALTER TABLE `research_group`
    MODIFY COLUMN `validated` varchar(255) NOT NULL CHECK (`validated` in ('false', 'true'));

-- scheduled_task
ALTER TABLE `scheduled_task`
    MODIFY COLUMN `enabled` varchar(255) DEFAULT NULL CHECK (`enabled` in ('false', 'true'));
ALTER TABLE `scheduled_task`
    MODIFY COLUMN `task_type` enum ('NOTIFY_DEPOSITS_TO_VALIDATE','NOTIFY_VALIDATED_DEPOSITS_DEPOSITOR','NOTIFY_VALIDATED_DEPOSITS_CONTRIBUTORS','NOTIFY_DEPOSITS_REJECTED','NOTIFY_VALIDATORS_ON_COMMENTS','NOTIFY_CONTRIBUTORS_ON_COMMENTS','NOTIFY_DEPOSITORS_ON_COMMENTS','NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE','NOTIFY_FORGOTTEN_DEPOSITS_IN_PROGRESS','SYNCHRONIZE_RESEARCH_GROUPS','NEW_RESEARCH_GROUPS_TO_VALIDATE','REINDEX_PUBLICATIONS_ENDING_EMBARGO','SYNC_THESES_TO_SWISS_NATIONAL_LIBRARY','SYNCHRONIZE_ORCID_PROFILE', 'CLEAN_DEPOSIT_UPDATED_WITHOUT_CHANGES') NOT NULL;

-- stored_search
ALTER TABLE `stored_search`
    MODIFY COLUMN `used_for_bibliography` varchar(255) NOT NULL CHECK (`used_for_bibliography` in ('false', 'true'));
ALTER TABLE `stored_search`
    MODIFY COLUMN `with_restricted_access_masters` varchar(255) NOT NULL CHECK (`with_restricted_access_masters` in ('false', 'true'));

-- stored_search_criteria
ALTER TABLE `stored_search_criteria`
    MODIFY COLUMN `boolean_clause_type` enum ('MUST','FILTER','SHOULD','MUST_NOT') NOT NULL;
ALTER TABLE `stored_search_criteria`
    MODIFY COLUMN `source` enum ('FACET','SEARCH','ADVANCED_SEARCH') NOT NULL;
ALTER TABLE `stored_search_criteria`
    MODIFY COLUMN `type` enum ('MATCH','TERM','WILDCARD','RANGE','QUERY','SIMPLE_QUERY','NESTED_BOOLEAN') NOT NULL;

-- structure
ALTER TABLE `structure`
    MODIFY COLUMN `status` enum ('ACTIVE','OBSOLETE','HISTORICAL') NOT NULL;

-- user
ALTER TABLE `user`
    MODIFY COLUMN `disabled` varchar(255) NOT NULL CHECK (`disabled` in ('false', 'true'));
