/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - EmailMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.message;

import java.io.Serializable;
import java.util.Map;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;

import ch.unige.aou.AouConstants;

public class EmailMessage implements Serializable {
  private static final long serialVersionUID = 4919737146903678970L;

  //@formatter:off
  public enum EmailTemplate {
    NEW_SUBSCRIPTION(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Inscription à des notifications / Subscription to notifications"),
    REMOVE_SUBSCRIPTION(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Désinscription à des notifications / Unsubscription to notifications"),
    NEW_DEPOSITS_TO_VALIDATE(AouConstants.NOTIFICATION_EMAIL_VALIDATION_PREFIX + " Nouveaux dépôts à valider"),
    NEW_COMMENT_FOR_VALIDATORS_DEPOSIT(AouConstants.NOTIFICATION_EMAIL_VALIDATION_PREFIX + " Nouveaux commentaires pour les validateurs/trices"),
    NEW_COMMENT_FOR_CONTRIBUTORS_DEPOSIT(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Nouveaux commentaires / New comments"),
    NEW_COMMENT_FOR_DEPOSITOR_DEPOSIT(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Nouveaux commentaires / New comments"),
    MY_INDIRECT_PUBLICATION_VALIDATED(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Nouvelles publications / New publications"),
    MY_PUBLICATIONS_VALIDATED(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Dépôts validés / Approved deposits"),
    MY_DEPOSIT_REQUIRES_FEEDBACK(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Commentaire attendu / Feedback requested"),
    MY_PUBLICATIONS_REJECTED(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Dépôts rejetés / Rejected deposits"),
    FORGOTTEN_PUBLICATIONS_TO_VALIDATE(AouConstants.NOTIFICATION_EMAIL_VALIDATION_PREFIX + " Dépôts en attente de validation (oubliés ?)"),
    FORGOTTEN_PUBLICATIONS_IN_PROGRESS(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Dépôts en cours - en attente d'action de votre part / Deposits in progress - awaiting action on your part"),
    SYNCHRONIZED_RESEARCH_GROUPS(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Groupes de recherche synchronisés"),
    NEW_RESEARCH_GROUPS_TO_VALIDATE(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Nouveaux groupes de recherche à valider"),
    NEW_PUBLICATION_VALIDATED_DOCTOR_THESIS(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Votre thèse a été validée / Your thesis was approved"),
    CONTACT_MESSAGE_TO_CONTRIBUTOR ("Message de contact depuis l'Archive ouverte UNIGE / Contact message from Archive ouverte UNIGE"),
    ASK_CORRECTION_FOR_PUBLICATION ("Demande de modification de document envoyée depuis l'Archive ouverte UNIGE / Document update request from Archive ouverte UNIGE"),
    NEW_PUBLICATIONS_IMPORTED_FROM_ORCID (AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Nouvelle(s) publication(s) importée(s) depuis votre profil ORCID / New publication(s) imported from your ORCID profile"),
    NEW_PUBLICATIONS_EXPORTED_TO_ORCID(AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Nouvelle(s) publication(s) exportée(s) vers votre profil ORCID / New publication(s) exported to your ORCID profile"),
    BATCH_UPDATES_REPORT (AouConstants.NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX + " Résultat de la mise à jour en masse");

    private final String subject;

    EmailTemplate(String subject) {
      this.subject = subject;
    }

    public String getSubject() {
      return this.subject;
    }
  }
  // @formatter:on

  @Enumerated(EnumType.STRING)
  @NotNull
  private EmailTemplate template;

  private String to;

  private Map<String, Object> parameters;

  public EmailMessage(String toRecipient, EmailTemplate template, Map<String, Object> parameters) {
    this.template = template;
    this.to = toRecipient;
    this.parameters = parameters;
  }

  public EmailTemplate getTemplate() {
    return this.template;
  }

  public void setTemplate(EmailTemplate template) {
    this.template = template;
  }

  public String getTo() {
    return this.to;
  }

  public void setTo(String to) {
    this.to = to;
  }

  public Map<String, Object> getParameters() {
    return this.parameters;
  }

  public void setParameters(Map<String, Object> parameters) {
    this.parameters = parameters;
  }

  @Override
  public String toString() {
    return super.toString() + " [template=" + this.template + "]" + " [to=" + this.to + "]";
  }
}
