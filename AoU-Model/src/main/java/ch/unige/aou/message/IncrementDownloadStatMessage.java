/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - IncrementDownloadStatMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.message;

import java.io.Serializable;

import ch.unige.aou.model.access.PublicationDownload;

public class IncrementDownloadStatMessage implements Serializable {
  private String documentFileId;

  private PublicationDownload publicationDownload;

  public IncrementDownloadStatMessage(String documentFileId, PublicationDownload publicationDownload) {
    this.documentFileId = documentFileId;
    this.publicationDownload = publicationDownload;
  }

  public String getDocumentFileId() {
    return this.documentFileId;
  }

  public PublicationDownload getPublicationDownload() {
    return this.publicationDownload;
  }
}
