/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationFromFedoraMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.message;

import java.io.Serializable;

public class PublicationFromFedoraMessage implements Serializable {
  private static final long serialVersionUID = 8494071655812171554L;

  private String pid;
  private String creatorResId;
  private String logMessage;

  public PublicationFromFedoraMessage(String pid, String creator, String logMessage) {
    this.pid = pid;
    this.creatorResId = creator;
    this.logMessage = logMessage;
  }

  public String getCreatorResId() {
    return this.creatorResId;
  }

  public void setCreatorResId(String creator) {
    this.creatorResId = creator;
  }

  public String getPid() {
    return this.pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  public String getLogMessage() {
    return this.logMessage;
  }

  public void setLogMessage(String logMessage) {
    this.logMessage = logMessage;
  }
}
