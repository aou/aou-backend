/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationIndexingMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.message;

import ch.unige.solidify.util.StringTool;

public class PublicationReindexingMessage extends AouEntityMessage {
  private static final long serialVersionUID = 4918737882872678970L;

  private String logMessage;

  public PublicationReindexingMessage(String resId) {
    super(resId);
  }

  public PublicationReindexingMessage(String resId, String logMessage) {
    super(resId);
    this.logMessage = logMessage;
  }

  public String getLogMessage() {
    return this.logMessage;
  }

  public void setLogMessage(String logMessage) {
    this.logMessage = logMessage;
  }

  @Override
  public String toString() {
    String str = this.getClass().getName() + " [resId=" + this.resId + "]";
    if (!StringTool.isNullOrEmpty(this.logMessage)) {
      str += " " + this.logMessage;
    }
    return str;
  }
}
