/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.publication.PublicationSubtype;

public class PublicationSubtypeSpecification extends SolidifySpecification<PublicationSubtype> {
  private static final long serialVersionUID = -6165416319841641651L;

  public PublicationSubtypeSpecification(PublicationSubtype criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<PublicationSubtype> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getCode() != null) {
      predicatesList.add(builder.like(root.get("code"), "%" + this.criteria.getCode() + "%"));
    }
    if (this.criteria.getSortValue() != null) {
      predicatesList.add(builder.equal(root.get("sortValue"), this.criteria.getSortValue()));
    }
    if (this.criteria.getBibliographySortValue() != null) {
      predicatesList.add(builder.equal(root.get("bibliographiesSortValue"), this.criteria.getBibliographySortValue()));
    }
    if (this.criteria.getPublicationType() != null && this.criteria.getPublicationType().getResId() != null) {
      predicatesList.add(builder.equal(root.get("publicationType").get("resId"), this.criteria.getPublicationType().getResId()));
    }
  }
}
