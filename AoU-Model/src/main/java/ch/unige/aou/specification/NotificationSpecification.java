/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - NotificationSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.time.OffsetDateTime;
import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.notification.NotificationType;

public class NotificationSpecification extends SolidifySpecification<Notification> {

  private static final long serialVersionUID = -7569094645132768485L;

  private OffsetDateTime createdDateMinValue = null;
  private OffsetDateTime createdDateMaxValue = null;
  private boolean onlyWithNullSentTime = false;
  private boolean onlyWithNullReadTime = false;

  public NotificationSpecification(Notification criteria) {
    super(criteria);
  }

  @Override
  public void completePredicatesList(Root<Notification> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
          List<Predicate> predicatesList) {
    if (this.criteria.getNotificationType() != null) {
      this.setNotificationTypeCriteria(root, criteriaBuilder, predicatesList, this.criteria.getNotificationType());
    }
    if (this.criteria.getMessage() != null) {
      predicatesList.add(criteriaBuilder.like(root.get("message"), "%" + this.criteria.getMessage() + "%"));
    }
    if (this.criteria.getRecipient() != null) {
      predicatesList.add(criteriaBuilder.equal(root.get("recipient").get("resId"), this.criteria.getRecipient().getResId()));
    }

    if (this.createdDateMinValue != null) {
      predicatesList.add(criteriaBuilder.greaterThanOrEqualTo(root.get("creation").<OffsetDateTime>get("when"), this.createdDateMinValue));
    }

    if (this.createdDateMaxValue != null) {
      predicatesList.add(criteriaBuilder.lessThanOrEqualTo(root.get("creation").<OffsetDateTime>get("when"), this.createdDateMaxValue));
    }

    if (this.onlyWithNullSentTime) {
      predicatesList.add(criteriaBuilder.isNull(root.get("sentTime")));
    }

    if (this.onlyWithNullReadTime) {
      predicatesList.add(criteriaBuilder.isNull(root.get("readTime")));
    }

  }

  private void setNotificationTypeCriteria(Root<Notification> root, CriteriaBuilder criteriaBuilder, List<Predicate> listPredicate,
          NotificationType notificationType) {
    final Path<NotificationType> proxy = root.get("notificationType");
    if (NotificationType.getAllNotificationTypes().stream().anyMatch(nt -> notificationType.getResId().equals(nt.getResId()))) {
      listPredicate.add(criteriaBuilder.equal(proxy.get("resId"), notificationType.getResId()));
    }
  }

  public OffsetDateTime getCreatedDateMinValue() {
    return this.createdDateMinValue;
  }

  public void setCreatedDateMinValue(OffsetDateTime createdDateMinValue) {
    this.createdDateMinValue = createdDateMinValue;
  }

  public OffsetDateTime getCreatedDateMaxValue() {
    return this.createdDateMaxValue;
  }

  public void setCreatedDateMaxValue(OffsetDateTime createdDateMaxValue) {
    this.createdDateMaxValue = createdDateMaxValue;
  }

  public boolean isOnlyWithNullSentTime() {
    return this.onlyWithNullSentTime;
  }

  public void setOnlyWithNullSentTime(boolean onlyWithNullSentTime) {
    this.onlyWithNullSentTime = onlyWithNullSentTime;
  }

  public boolean isOnlyWithNullReadTime() {
    return this.onlyWithNullReadTime;
  }

  public void setOnlyWithNullReadTime(boolean onlyWithNullReadTime) {
    this.onlyWithNullReadTime = onlyWithNullReadTime;
  }

}
