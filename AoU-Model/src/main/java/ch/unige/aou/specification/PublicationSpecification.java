/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.Publication;

public class PublicationSpecification extends SolidifySpecification<Publication> {
  private static final long serialVersionUID = -2649320901418924922L;

  private String fullName;

  private OffsetDateTime lastUpdatedBefore;
  private OffsetDateTime lastUpdatedAfter;

  public PublicationSpecification(Publication criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<Publication> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getCreator() != null) {
      predicatesList.add(builder.equal(root.get("creator").get("resId"), this.criteria.getCreator().getResId()));
    }
    if (this.criteria.getStatus() != null) {
      predicatesList.add(builder.equal(root.get("status"), this.criteria.getStatus()));
    }
    if (this.criteria.getTitle() != null) {
      predicatesList.add(builder.like(root.get("title"), "%" + this.criteria.getTitle() + "%"));
    }
    if (this.criteria.getFormData() != null) {
      predicatesList.add(builder.like(root.get("formData"), "%" + this.criteria.getFormData() + "%"));
    }
    if (this.criteria.getMetadata() != null) {
      predicatesList.add(builder.like(root.get("metadata"), "%" + this.criteria.getMetadata() + "%"));
    }
    if (this.criteria.getSubtype() != null && this.criteria.getSubtype().getResId() != null) {
      predicatesList.add(builder.equal(root.get("subtype").get("resId"), this.criteria.getSubtype().getResId()));
    }
    if (!StringTool.isNullOrEmpty(this.criteria.getDoi())) {
      predicatesList.add(builder.like(root.get("doi"), "%" + this.criteria.getDoi() + "%"));
    }
    if (this.criteria.getPmid() != null) {
      predicatesList.add(builder.like(root.get("pmid"), "%" + this.criteria.getPmid() + "%"));
    }
    if (!StringTool.isNullOrEmpty(this.criteria.getArchiveId())) {
      predicatesList.add(builder.equal(root.get(AouConstants.INDEX_FIELD_ARCHIVE_ID), this.criteria.getArchiveId()));
    }
    if (!StringTool.isNullOrEmpty(this.fullName)) {
      List<String> listTerm = Arrays.stream(this.fullName.split(" ")).filter(term -> !StringTool.isNullOrEmpty(term))
              .collect(Collectors.toList());
      if (listTerm.size() > 0) {
        for (String term : listTerm) {
          Predicate likeFirstName = builder.like(root.get("creator").get("firstName"), "%" + term + "%");
          Predicate likeLastName = builder.like(root.get("creator").get("lastName"), "%" + term + "%");
          predicatesList.add(builder.or(likeFirstName, likeLastName));
        }
      }
    }

    if (this.lastUpdatedBefore != null) {
      predicatesList.add(builder.lessThanOrEqualTo(root.get("lastUpdate").get("when"), this.lastUpdatedBefore));
    }

    if (this.lastUpdatedAfter != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("lastUpdate").get("when"), this.lastUpdatedAfter));
    }
  }

  public void setFullName(String fullName) {
    if (!StringTool.isNullOrEmpty(fullName)) {
      fullName = fullName.replaceAll(",", "");
    }
    this.fullName = fullName;
  }

  public void setLastUpdatedBefore(OffsetDateTime lastUpdatedBefore) {
    this.lastUpdatedBefore = lastUpdatedBefore;
  }

  public void setLastUpdatedAfter(OffsetDateTime lastUpdatedAfter) {
    this.lastUpdatedAfter = lastUpdatedAfter;
  }
}
