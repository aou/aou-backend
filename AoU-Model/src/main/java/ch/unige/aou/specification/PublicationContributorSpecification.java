/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationContributorSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;

public class PublicationContributorSpecification extends PublicationSpecification {

  private final String cnIndividu;

  public PublicationContributorSpecification(Publication criteria, String cnIndividu) {
    super(criteria);
    this.cnIndividu = cnIndividu;
  }

  @Override
  protected void completePredicatesList(Root<Publication> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    super.completePredicatesList(root, query, builder, predicatesList);

    Path<Person> contributorPath = root.join("contributors", JoinType.LEFT).get(AouConstants.DB_CN_INDIVIDU);
    Predicate equalContributor = builder.equal(contributorPath, this.cnIndividu);

    //Get Publications that contain the cnIndividu in its contributors
    predicatesList.add(equalContributor);

    query.distinct(true);
  }
}
