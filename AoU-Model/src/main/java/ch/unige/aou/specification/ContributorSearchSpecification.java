/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContributorSearchSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.search.ContributorSearchType;

public class ContributorSearchSpecification implements Specification<Contributor> {
  private static final long serialVersionUID = -6039373889994880237L;

  private static final String FIRST_NAME = "firstName";
  private static final String LAST_NAME = "lastName";

  private final String searchTerm;
  private final ContributorSearchType searchType;

  public ContributorSearchSpecification(String searchTerm, ContributorSearchType searchType) {
    this.searchTerm = searchTerm;
    this.searchType = searchType;
  }

  @Override
  public Predicate toPredicate(Root<Contributor> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

    final List<Predicate> predicatesList = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(this.searchTerm)) {
      if (!this.searchTerm.contains(",")) {
        this.fillLooseNamesConditions(root, builder, predicatesList);
      } else {
        this.fillFirstNameAndLastNameConditions(root, builder, predicatesList);
      }
    }

    Predicate cnIndividuPredicate = this.getCnIndividuPredicate(root, builder);
    if (cnIndividuPredicate != null) {
      predicatesList.add(cnIndividuPredicate);
    }

    query.distinct(true);

    return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
  }

  private void fillLooseNamesConditions(Root<Contributor> root, CriteriaBuilder builder, List<Predicate> predicatesList) {
    List<String> termsList = Arrays.stream(this.searchTerm.split(" ")).map(String::trim).filter(term -> !StringTool.isNullOrEmpty(term))
            .collect(Collectors.toList());

    Predicate orNamesPredicate = null;
    Path<ContributorOtherName> otherNamePath = root.join("otherNames", JoinType.LEFT);

    for (String term : termsList) {
      // Contributor name
      Predicate likeFirstName = builder.like(root.get(FIRST_NAME), "%" + term + "%");
      if (orNamesPredicate == null) {
        orNamesPredicate = builder.or(likeFirstName);
      } else {
        orNamesPredicate = builder.or(orNamesPredicate, likeFirstName);
      }
      Predicate likeLastName = builder.like(root.get(LAST_NAME), "%" + term + "%");
      orNamesPredicate = builder.or(orNamesPredicate, likeLastName);

      // Other names
      Predicate likeOtherFirstName = builder.like(otherNamePath.get(FIRST_NAME), "%" + term + "%");
      orNamesPredicate = builder.or(orNamesPredicate, likeOtherFirstName);
      Predicate likeOtherLastName = builder.like(otherNamePath.get(LAST_NAME), "%" + term + "%");
      orNamesPredicate = builder.or(orNamesPredicate, likeOtherLastName);
    }

    if (!orNamesPredicate.getExpressions().isEmpty()) {
      predicatesList.add(orNamesPredicate);
    }
  }

  private void fillFirstNameAndLastNameConditions(Root<Contributor> root, CriteriaBuilder builder, List<Predicate> predicatesList) {
    String[] names = this.searchTerm.split(",");
    if (names.length > 0 && names.length <= 2) {

      String lastName = names[0].trim();
      String firstName = names.length == 2 ? names[1].trim() : null;

      // Contributor name
      Predicate contributorPredicate = this.buildFirstnameAndLastNamePredicate(builder, root, lastName, firstName);

      // Other names
      Path<ContributorOtherName> otherNamePath = root.join("otherNames", JoinType.LEFT);
      Predicate otherNamesPredicate = this.buildFirstnameAndLastNamePredicate(builder, otherNamePath, lastName, firstName);

      Predicate orContributorOtherNamesPredicate = builder.or(contributorPredicate, otherNamesPredicate);
      predicatesList.add(orContributorOtherNamesPredicate);
    }
  }

  private Predicate buildFirstnameAndLastNamePredicate(CriteriaBuilder builder, Path rootPath, String lastName, String firstName) {
    Predicate likeLastName = builder.like(rootPath.get(LAST_NAME), "%" + lastName + "%");

    if (!StringTool.isNullOrEmpty(firstName)) {
      Predicate likeFirstName = builder.like(rootPath.get(FIRST_NAME), "%" + firstName + "%");
      return builder.and(likeLastName, likeFirstName);
    } else {
      return likeLastName;
    }
  }

  private Predicate getCnIndividuPredicate(Root<Contributor> root, CriteriaBuilder builder) {
    switch (this.searchType) {
      case UNIGE:
        return builder.isNotNull(root.get("cnIndividu"));
      case NON_UNIGE:
        return builder.isNull(root.get("cnIndividu"));
      case ALL:
      default:
        return null;
    }
  }
}
