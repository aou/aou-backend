/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - UserSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.security.User;

public class UserSpecification extends SolidifySpecification<User> {
  private static final long serialVersionUID = 6657807556908159308L;

  private boolean onlyWithValidationRights = false;

  public UserSpecification(User criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getExternalUid() != null) {
      predicatesList.add(builder.equal(root.get("externalUid"), this.criteria.getExternalUid()));
    }

    if (this.criteria.getFirstName() != null) {
      predicatesList.add(builder.like(root.get("firstName"), "%" + this.criteria.getFirstName() + "%"));
    }

    if (this.criteria.getLastName() != null) {
      predicatesList.add(builder.like(root.get("lastName"), "%" + this.criteria.getLastName() + "%"));
    }

    if (this.criteria.getEmail() != null) {
      predicatesList.add(builder.like(root.get("email"), "%" + this.criteria.getEmail() + "%"));
    }

    if (this.criteria.getPerson() != null) {
      predicatesList.add(builder.equal(root.get("person").get("resId"), this.criteria.getPerson().getResId()));
    }
    if (this.onlyWithValidationRights) {
      predicatesList.add(builder.ge(builder.size(root.get("person").get("validationRights")), 1));
    }

  }

  public void setOnlyWithValidationRights(boolean onlyWithValidationRights) {
    this.onlyWithValidationRights = onlyWithValidationRights;
  }
}
