/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubSubtypeSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.publication.PublicationSubSubtype;

public class PublicationSubSubtypeSpecification extends SolidifySpecification<PublicationSubSubtype> {
  private static final long serialVersionUID = 8504672467150246639L;

  public PublicationSubSubtypeSpecification(PublicationSubSubtype criteria) {
   super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<PublicationSubSubtype> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getSortValue() != null) {
      predicatesList.add(builder.equal(root.get("sortValue"), this.criteria.getSortValue()));
    }
    if (this.criteria.getPublicationSubtype() != null && this.criteria.getPublicationSubtype().getResId() != null) {
      predicatesList.add(builder.equal(root.get("publicationSubtype").get("resId"), this.criteria.getPublicationSubtype().getResId()));
    }
  }
}
