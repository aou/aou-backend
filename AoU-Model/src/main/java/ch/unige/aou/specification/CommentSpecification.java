/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - CommentSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.publication.Comment;

public class CommentSpecification extends SolidifySpecification<Comment> {

    public CommentSpecification(Comment criteria) {
        super(criteria);
    }

    @Override
    protected void completePredicatesList(Root<Comment> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
        if (this.criteria.getPublication() != null) {
            predicatesList.add(builder.like(root.get("publication").get("resId"), this.criteria.getPublication().getResId()));
        }
        if (this.criteria.getText() != null) {
            predicatesList.add(builder.like(root.get("text"), "%" + this.criteria.getText() + "%"));
        }
        if (this.criteria.hasOnlyForValidatorValue()) {
            if (this.criteria.isOnlyForValidators()) {
                predicatesList.add(builder.isTrue(root.get("onlyForValidators")));
            } else {
                predicatesList.add(builder.isFalse(root.get("onlyForValidators")));
            }
        }
    }

}
