/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ResearchGroupSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.time.OffsetDateTime;
import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.settings.ResearchGroup;

public class ResearchGroupSpecification extends SolidifySpecification<ResearchGroup> {
  private static final long serialVersionUID = 8669419110874167742L;

  private OffsetDateTime lastUpdateBefore;

  public ResearchGroupSpecification(ResearchGroup criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<ResearchGroup> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getCode() != null) {
      predicatesList.add(builder.like(root.get("code"), "%" + this.criteria.getCode() + "%"));
    }
    if (this.criteria.getAcronym() != null) {
      predicatesList.add(builder.like(root.get("acronym"), "%" + this.criteria.getAcronym() + "%"));
    }
    if (this.criteria.isActive() != null) {
      predicatesList.add(builder.equal(root.get("active"), this.criteria.isActive()));
    }
    if (this.criteria.isValidated() != null) {
      predicatesList.add(builder.equal(root.get("validated"), this.criteria.isValidated()));
    }
    if (this.lastUpdateBefore != null) {
      predicatesList.add(builder.greaterThanOrEqualTo(root.get("lastUpdate").get("when"), this.lastUpdateBefore));
    }
  }

  public OffsetDateTime getLastUpdateBefore() {
    return this.lastUpdateBefore;
  }

  public void setLastUpdateBefore(OffsetDateTime lastUpdateBefore) {
    this.lastUpdateBefore = lastUpdateBefore;
  }

}
