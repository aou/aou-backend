/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - LicenseSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.settings.License;

public class LicenseSpecification extends SolidifySpecification<License> {
  private static final long serialVersionUID = -1233296744491155033L;

  public LicenseSpecification(License license) {
    super(license);
  }

  @Override
  protected void completePredicatesList(Root<License> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getTitle() != null) {
      predicatesList.add(builder.like(root.get("title"), "%" + this.criteria.getTitle() + "%"));
    }
    if (this.criteria.getOpenLicenseId() != null) {
      predicatesList.add(builder.equal(root.get("openLicenseId"), this.criteria.getOpenLicenseId()));
    }
    if (this.criteria.getVersion() != null) {
      predicatesList.add(builder.like(root.get("version"), "%" + this.criteria.getVersion() + "%"));
    }
    if (this.criteria.getUrl() != null) {
      predicatesList.add(builder.like(root.get("url"), "%" + this.criteria.getUrl() + "%"));
    }
    if (this.criteria.getSortValue() != null) {
      predicatesList.add(builder.equal(root.get("sortValue"), this.criteria.getSortValue()));
    }
    if (this.criteria.getVisible() != null) {
      predicatesList.add(builder.equal(root.get("visible"), this.criteria.getVisible()));
    }

    if (this.criteria.getLicenseGroup() != null && !StringTool.isNullOrEmpty(this.criteria.getLicenseGroup().getResId())) {
      predicatesList.add(builder.equal(root.get("licenseGroup").get("resId"), this.criteria.getLicenseGroup().getResId()));
    }
  }
}
