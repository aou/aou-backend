/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - NotificationTypeSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.notification.NotificationType;

public class NotificationTypeSpecification extends SolidifySpecification<NotificationType> {

  private static final long serialVersionUID = -8473671588587082484L;

  public NotificationTypeSpecification(NotificationType criteria) {
    super(criteria);
  }

  @Override
  public void completePredicatesList(
          Root<NotificationType> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, List<Predicate> predicatesList) {
    if (this.criteria.getEventType() != null) {
      predicatesList.add(criteriaBuilder.equal(root.get("eventType").get("resId"), this.criteria.getEventType().getResId()));
    }
  }
}
