/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ValidablePublicationSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.SolidifyConstants;

import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Structure;

public class ValidablePublicationSpecification extends PublicationSpecification {
  private static final long serialVersionUID = -2648761982528924922L;

  private final Map<String, List<String>> structuresPublicationsTypesMap;

  public ValidablePublicationSpecification(Publication criteria, Map<String, List<String>> structuresPublicationsTypesMap) {
    super(criteria);
    this.structuresPublicationsTypesMap = structuresPublicationsTypesMap;
  }

  @Override
  protected void completePredicatesList(Root<Publication> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    super.completePredicatesList(root, query, builder, predicatesList);

    /**
     * Build the where clause according to validation rights and combine it
     * with criteria on publication itself (built in parent class)
     */
    predicatesList.add(this.buildStructuresAndSubtypesConditions(root, builder));

    // Set distinct to avoid duplicates depending on existing validation rights
    query.distinct(true);
  }

  /**
   * Build a Predicate that is a where clause like
   * <p>
   * WHERE
   * (structure.res_id IN ("1", "2", "3") AND publication.subtype_res_id IN ("A1", "A2", "A3"))
   * OR
   * (structure.res_id IN ("12", "13", "45") AND publication.subtype_res_id IN ("R1", "R2", "R3"))
   * OR
   * ...
   *
   * @param root
   * @return
   */
  private Predicate buildStructuresAndSubtypesConditions(Root<Publication> root, CriteriaBuilder builder) {

    /**
     * In order to limit the number of WHERE conditions, group the structures that have the same publication subtypes
     */
    Map<List<String>, List<String>> whereClause = this.getGroupedWhereConditions(this.structuresPublicationsTypesMap);

    Path<Structure> structurePath = root.join("publicationStructures").get("compositeKey").get("structure").get(SolidifyConstants.DB_RES_ID);
    Path<Structure> publicationSubtypePath = root.get("subtype").get(SolidifyConstants.DB_RES_ID);

    List<Predicate> structuresAndSubtypesPredicates = new ArrayList<>();
    whereClause.forEach((publicationSubtypeIds, structureIds) -> {

      /**
       * structure.res_id IN ("1", "2", "3")
       */
      Predicate structuresPredicate = structurePath.in(structureIds);

      /**
       * publication.subtype_res_id IN ("A1", "A2", "A3")
       */
      Predicate publicationSubtypesPredicate = publicationSubtypePath.in(publicationSubtypeIds);

      /**
       * ... AND ...
       */
      structuresAndSubtypesPredicates.add(builder.and(structuresPredicate, publicationSubtypesPredicate));
    });

    /**
     * ...
     * OR
     * ...
     */
    Predicate[] predicates = new Predicate[structuresAndSubtypesPredicates.size()];
    structuresAndSubtypesPredicates.toArray(predicates);
    return builder.or(predicates);
  }

  /**
   * Group the map's key values if their list values are identical
   * <p>
   * <p>
   * 1  --->  [1, 2, 3, 4]
   * 2  --->  [1, 2, 7, 9]
   * 3  --->  [1, 2, 7, 9]
   * 4  --->  [1, 2, 7, 8]
   * 5  --->  [1, 2, 3, 4]
   * <p>
   * is returned as
   * <p>
   * [1, 2, 3, 4] ---> [1, 5]
   * [1, 2, 7, 9] ---> [2, 3]
   * [1, 2, 7, 8] ---> [4]
   *
   * @param structuresPublicationsTypesMap Map of structureId --> List of publication subtype ids
   * @return Map of List of publication subtype ids --> List of structure ids
   */
  private Map<List<String>, List<String>> getGroupedWhereConditions(Map<String, List<String>> structuresPublicationsTypesMap) {

    Map<List<String>, List<String>> whereClause = new HashMap<>();

    Set<String> structureIds = structuresPublicationsTypesMap.keySet();
    for (String structureId : structureIds) {
      List<String> subtypeIds = structuresPublicationsTypesMap.get(structureId);
      Collections.sort(subtypeIds);
      if (!whereClause.containsKey(subtypeIds)) {
        whereClause.put(subtypeIds, new ArrayList<>());
      }
      List<String> addedStructureIds = whereClause.get(subtypeIds);
      if (!addedStructureIds.contains(structureId)) {
        addedStructureIds.add(structureId);
      }
    }
    return whereClause;
  }
}
