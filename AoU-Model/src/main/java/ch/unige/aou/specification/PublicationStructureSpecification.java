/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationStructureSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.specification.Join2TiersSpecification;

import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.settings.Structure;

/**
 *
 */
public class PublicationStructureSpecification extends Join2TiersSpecification<PublicationStructure> {

  public PublicationStructureSpecification(PublicationStructure joinCriteria) {
    super(joinCriteria, PublicationStructure.PATH_TO_PUBLICATION, PublicationStructure.PATH_TO_STRUCTURE);
  }

  @Override
  protected String getParentId() {
    return this.joinCriteria.getPublication().getResId();
  }

  @Override
  protected String getChildId() {
    return this.joinCriteria.getStructure().getResId();
  }

  @Override
  protected void completeJoinPredicatesList(Root<PublicationStructure> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.joinCriteria.getLinkType() != null) {
      predicatesList.add(builder.equal(root.get("linkType"), this.joinCriteria.getLinkType()));
    }
  }

  @Override
  protected void completeChildPredicatesList(Root<PublicationStructure> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    Structure structure = this.joinCriteria.getStructure();
    Path structurePath = this.getChildPath(root);

    if (structure.getParentStructure() != null && structure.getParentStructure().getResId() != null) {
      predicatesList.add(builder
              .equal(structurePath.get("parentStructure").get(SolidifyConstants.DB_RES_ID), structure.getParentStructure().getResId()));
    }
    if (structure.getName() != null) {
      predicatesList.add(builder.like(structurePath.get("name"), "%" + structure.getName() + "%"));
    }
    if (structure.getCnStructC() != null) {
      predicatesList.add(builder.equal(structurePath.get("cnStructC"), structure.getCnStructC()));
    }
    if (structure.getCodeStruct() != null) {
      predicatesList.add(builder.equal(structurePath.get("codeStruct"), structure.getCodeStruct()));
    }
    if (structure.getDewey() != null) {
      predicatesList.add(builder.equal(structurePath.get("dewey"), structure.getDewey()));
    }
    if (structure.getStatus() != null) {
      predicatesList.add(builder.equal(structurePath.get("status"), structure.getStatus()));
    }
    if (structure.getSortValue() != null) {
      predicatesList.add(builder.equal(structurePath.get("sortValue"), structure.getSortValue()));
    }
  }
}
