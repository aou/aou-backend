/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - StructureSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.specification;

import java.util.List;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.specification.SolidifySpecification;

import ch.unige.aou.model.settings.Structure;

public class StructureSpecification extends SolidifySpecification<Structure> {
  private static final long serialVersionUID = -465981654968886514l;

  public StructureSpecification(Structure criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<Structure> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.criteria.getParentStructure() != null && this.criteria.getParentStructure().getResId() != null) {
      predicatesList
              .add(builder.equal(root.get("parentStructure").get(SolidifyConstants.DB_RES_ID), this.criteria.getParentStructure().getResId()));
    }

    if (this.criteria.getName() != null) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }
    if (this.criteria.getCnStructC() != null) {
      predicatesList.add(builder.equal(root.get("cnStructC"), this.criteria.getCnStructC()));
    }
    if (this.criteria.getCodeStruct() != null) {
      predicatesList.add(builder.equal(root.get("codeStruct"), this.criteria.getCodeStruct()));
    }
    if (this.criteria.getDewey() != null) {
      predicatesList.add(builder.equal(root.get("dewey"), this.criteria.getDewey()));
    }
    if (this.criteria.getStatus() != null) {
      predicatesList.add(builder.equal(root.get("status"), this.criteria.getStatus()));
    }
    if (this.criteria.getAcronym() != null) {
      predicatesList.add(builder.like(root.get("acronym"), "%" + this.criteria.getAcronym() + "%"));
    }
  }
}
