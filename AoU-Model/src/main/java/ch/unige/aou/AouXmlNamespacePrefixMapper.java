/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AouXmlNamespacePrefixMapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou;

import ch.unige.solidify.OAIXmlNamespacePrefixMapper;

public abstract class AouXmlNamespacePrefixMapper extends OAIXmlNamespacePrefixMapper {

  public AouXmlNamespacePrefixMapper() {
    super();
    this.addSchema("https://archive-ouverte.unige.ch/aou/v1", "aou");
    this.addSchema("http://purl.org/dc/elements/1.1/", "dc");
    this.addSchema("http://purl.org/dc/terms/", "dcterms");
    this.addSchema("http://www.loc.gov/MARC21/slim", "marc");
    this.addSchema("info:fedora/fedora-system:def/foxml#", "foxml");
    this.addSchema("http://www.fedora.info/definitions/1/0/management/", "fedora-mgmt");
    this.addSchema("http://www.fedora.info/definitions/1/0/access/", "fedora-access");
    this.addSchema("urn:nbn:de:1111-2004033116", "epicur");
    this.addSchema("http://datacite.org/schema/kernel-4", "datacite");
    this.addSchema("http://namespace.openaire.eu/schema/oaire/", "oaire");
  }
}
