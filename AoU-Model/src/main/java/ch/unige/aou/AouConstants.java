/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AouConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou;

import java.util.HashMap;
import java.util.Map;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;

import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

public class AouConstants {

  private AouConstants() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  // Modules
  public static final String AOU = "AoU";
  public static final String AOU_SOLUTION = "Archive ouverte UNIGE";

  // @formatter:off
  private static final String[] PUBLIC_URLS = {
          "/docs/**",
          "/AoU-logo.png",
          "/*/css/*.css",
          "/swagger-ui.html",
          "/swagger-config",
          "/swagger-ui/**",
          "/api-docs",
          "/api-docs.yaml",
          "/api-docs/**",
          "/" + ResourceName.XSL,
          "/" + ModuleName.ADMIN + "/" + ResourceName.MODULES,
          "/" + ModuleName.ADMIN + "/" + ResourceName.SYSTEM_PROPERTIES,
          "/" + ModuleName.ADMIN + "/" + ResourceName.GLOBAL_BANNERS + "/" + AouActionName.GET_ACTIVE,
          "/" + ModuleName.ADMIN + "/" + ResourceName.CONTRIBUTOR_ROLE + "/" + AouActionName.LIST_CONTRIBUTOR_ROLES,
          "/" + ModuleName.ADMIN + "/" + ResourceName.MONITOR + "/" + AouActionName.MONITOR_DATABASE_CONNECTION,
          "/" + ModuleName.ADMIN + "/" + ResourceName.CONTRIBUTORS + "/" + ActionName.SEARCH,
          "/" + ModuleName.ADMIN + "/" + ResourceName.CONTRIBUTORS + "/" + AouActionName.GET_BY_NAME,
          "/" + ModuleName.ADMIN + "/" + ResourceName.CONTRIBUTORS + "/" + AouActionName.GET_BY_CN_INDIVIDU,
          "/" + ModuleName.ADMIN + "/" + ResourceName.CONTRIBUTORS + "/" + AouActionName.GET_BY_ORCID,
          "/" + ModuleName.ADMIN + "/" + ResourceName.CONTRIBUTORS + "/" + AouActionName.CONTACT_CONTRIBUTOR,
          "/" + ModuleName.ADMIN + "/" + ResourceName.STRUCTURES,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATION_SUBTYPES,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATION_SUBTYPES + "/**",
          "/" + ModuleName.ADMIN + "/" + ResourceName.RESEARCH_GROUPS,
          "/" + ModuleName.ADMIN + "/" + ResourceName.RESEARCH_GROUPS + "/**",
          "/" + ModuleName.ADMIN + "/" + ResourceName.RESEARCH_GROUPS + "/" + ActionName.SEARCH,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + AouActionName.DOWNLOAD_THUMBNAIL,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + AouActionName.GET_CONTACTABLE_CONTRIBUTORS,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + AouActionName.CONTACT_CONTRIBUTOR,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + AouActionName.ASK_CORRECTION,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + ResourceName.DOCUMENT_FILES + "/*/" + AouActionName.DOWNLOAD,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/" + AouActionName.EXPORT,
          "/" + ModuleName.ADMIN + "/" + SolidifyConstants.ORCID + "/" + ActionName.LANDING,
          "/" + ModuleName.ACCESS + "/" + ResourceName.SETTINGS + "/" + AouActionName.LIST_ADVANCED_SEARCH_CRITERIA,
          "/" + ModuleName.ACCESS + "/" + ResourceName.SETTINGS + "/" + AouActionName.LIST_BIBLIOGRAPHY_FORMATS,
          "/" + ModuleName.ACCESS + "/" + ResourceName.METADATA,
          "/" + ModuleName.ACCESS + "/" + ResourceName.METADATA + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.METADATA + "/" + ActionName.SEARCH,
          "/" + ActionName.DOWNLOAD,
          "/" + ModuleName.ACCESS + "/" + ResourceName.BIBLIOGRAPHY + "/" + AouActionName.JAVASCRIPT,
          "/" + ModuleName.ACCESS + "/" + ResourceName.BIBLIOGRAPHY + "/" + AouActionName.JAVASCRIPT_BY_AUTHOR,
          "/" + ModuleName.ACCESS + "/" + ResourceName.BIBLIOGRAPHY + "/" + AouActionName.JAVASCRIPT_BY_STRUCTURE,
          "/" + ModuleName.ACCESS + "/" + ResourceName.BIBLIOGRAPHY + "/" + AouActionName.JAVASCRIPT_BY_RESEARCH_GROUP,
          "/" + ModuleName.ACCESS + "/" + ResourceName.BIBLIOGRAPHY + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.CONTRIBUTORS + "/**",
          "/" + ModuleName.ACCESS + "/" + ResourceName.MONITOR + "/" + AouActionName.MONITOR_DATABASE_CONNECTION,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE,
          "/" + OAIConstants.OAI_MODULE + "/" + OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE + "/" + ResourceName.XSL,
          "/" + OAIConstants.OAI_RESOURCE,
          "/" + OAIConstants.OAI_RESOURCE + "/" + ResourceName.XSL,
          "/" + ActionName.SITEMAP_XML,
          "/" + ActionName.SITEMAP + "-*" + ActionName.XML_EXTENSION,
  };
  // @formatter:on

  // @formatter:off
  private static final String[] DISABLED_CSRF_URLS = {
          "/" + OAIConstants.OAI_RESOURCE,
          "/" + ModuleName.ADMIN + "/" + ResourceName.CONTRIBUTORS + "/" + AouActionName.CONTACT_CONTRIBUTOR,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + AouActionName.GET_CONTACTABLE_CONTRIBUTORS,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + AouActionName.CONTACT_CONTRIBUTOR,
          "/" + ModuleName.ADMIN + "/" + ResourceName.PUBLICATIONS + "/*/" + AouActionName.ASK_CORRECTION,
          "/" + ModuleName.ACCESS + "/" + ResourceName.METADATA + "/" + ActionName.SEARCH,
          "/"  + ModuleName.ACCESS + "/" + ResourceName.METADATA + "/*/" + AouActionName.PREPARE_DOWNLOAD,
  };
  // @formatter:on

  public static String[] getPublicUrls() {
    return PUBLIC_URLS.clone();
  }

  public static String[] getDisabledCsrfUrls() {
    return DISABLED_CSRF_URLS.clone();
  }

  public static Map<String, String> getSwissNationalLibraryUrnConversionTable() {
    Map<String, String> map = new HashMap<>();
    map.put("0", "1");
    map.put("1", "2");
    map.put("2", "3");
    map.put("3", "4");
    map.put("4", "5");
    map.put("5", "6");
    map.put("6", "7");
    map.put("7", "8");
    map.put("8", "9");
    map.put("9", "41");
    map.put("a", "18");
    map.put("b", "14");
    map.put("c", "19");
    map.put("d", "15");
    map.put("e", "16");
    map.put("f", "21");
    map.put("g", "22");
    map.put("h", "23");
    map.put("i", "24");
    map.put("j", "25");
    map.put("k", "42");
    map.put("l", "26");
    map.put("m", "27");
    map.put("n", "13");
    map.put("o", "28");
    map.put("p", "29");
    map.put("q", "31");
    map.put("r", "12");
    map.put("s", "32");
    map.put("t", "33");
    map.put("u", "11");
    map.put("v", "34");
    map.put("w", "35");
    map.put("x", "36");
    map.put("y", "37");
    map.put("z", "38");
    map.put("-", "39");
    map.put("+", "49");
    map.put(":", "17");
    map.put("_", "43");
    map.put(".", "47");
    map.put("/", "45");
    return map;
  }

  // Default Values
  public static final String LANG_CODE_FRENCH = "fre";
  public static final String LANG_CODE_ENGLISH = "eng";
  public static final String LANG_CODE_GERMAN = "ger";
  public static final String LANG_CODE_SPANISH = "spa";

  public static final int ORDER_INCREMENT = 10;

  // Deposit types
  public static final String DEPOSIT_TYPE_ARTICLE_ID = "ARTICLE";
  public static final String DEPOSIT_TYPE_ARTICLE_NAME = "Article";
  public static final String DEPOSIT_TYPE_CONFERENCE_ID = "CONFERENCE";
  public static final String DEPOSIT_TYPE_CONFERENCE_NAME = "Conférence";
  public static final String DEPOSIT_TYPE_DIPLOME_ID = "DIPLOME";
  public static final String DEPOSIT_TYPE_DIPLOME_NAME = "Diplôme";
  public static final String DEPOSIT_TYPE_JOURNAL_ID = "JOURNAL";
  public static final String DEPOSIT_TYPE_JOURNAL_NAME = "Journal";
  public static final String DEPOSIT_TYPE_LIVRE_ID = "LIVRE";
  public static final String DEPOSIT_TYPE_LIVRE_NAME = "Livre";
  public static final String DEPOSIT_TYPE_RAPPORT_ID = "RAPPORT";
  public static final String DEPOSIT_TYPE_RAPPORT_NAME = "Rapport";

  // @formatter:off
  public static final String DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME                   = "Article scientifique";
  public static final String DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID                     = "A1";
  public static final String DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME                  = "Article professionnel";
  public static final String DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_ID                    = "A2";
  public static final String DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME                          = "Autre article";
  public static final String DEPOSIT_SUBTYPE_AUTRE_ARTICLE_ID                            = "A3";

  public static final String DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME                    = "Actes de conférence";
  public static final String DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_ID                      = "C1";
  public static final String DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME              = "Présentation / Intervention";
  public static final String DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_ID                = "C2";
  public static final String DEPOSIT_SUBTYPE_POSTER_NAME                                 = "Poster";
  public static final String DEPOSIT_SUBTYPE_POSTER_ID                                   = "C3";
  public static final String DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME                       = "Chapitre d'actes";
  public static final String DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_ID                         = "C4";

  public static final String DEPOSIT_SUBTYPE_THESE_NAME                                  = "Thèse";
  public static final String DEPOSIT_SUBTYPE_THESE_ID                                    = "D1";
  public static final String DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME               = "Master d'études avancées";
  public static final String DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_ID                 = "D2";
  public static final String DEPOSIT_SUBTYPE_MASTER_NAME                                 = "Master";
  public static final String DEPOSIT_SUBTYPE_MASTER_ID                                   = "D3";
  public static final String DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME                 = "Thèse de privat-docent";
  public static final String DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_ID                   = "D4";
  public static final String DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME            = "Thèse professionnelle";
  public static final String DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_ID              = "D5";

  public static final String DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME                        = "Numéro de revue";
  public static final String DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_ID                          = "J1";

  public static final String DEPOSIT_SUBTYPE_LIVRE_NAME                                  = "Livre";
  public static final String DEPOSIT_SUBTYPE_LIVRE_ID                                    = "L1";
  public static final String DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME                      = "Ouvrage collectif";
  public static final String DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_ID                        = "L2";
  public static final String DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME                      = "Chapitre de livre";
  public static final String DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_ID                        = "L3";
  public static final String DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME = "Contribution à un dictionnaire / une encyclopédie";
  public static final String DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_ID   = "L5";

  public static final String DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME                   = "Rapport de recherche";
  public static final String DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_ID                     = "R1";
  public static final String DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME                      = "Rapport technique";
  public static final String DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_ID                        = "R2";
  public static final String DEPOSIT_SUBTYPE_WORKING_PAPER_NAME                          = "Working paper";
  public static final String DEPOSIT_SUBTYPE_WORKING_PAPER_ID                            = "R3";
  public static final String DEPOSIT_SUBTYPE_PREPRINT_NAME                               = "Preprint";
  public static final String DEPOSIT_SUBTYPE_PREPRINT_ID                                 = "R4";
  // @formatter:on

  public static final String DEPOSIT_SUB_SUBTYPE_ARTICLE_NAME = "Article";
  public static final String DEPOSIT_SUB_SUBTYPE_COMPTE_RENDU_LIVRE_NAME = "Compte rendu de livre";
  public static final String DEPOSIT_SUB_SUBTYPE_RAPPORT_DU_CAS_NAME = "Rapport de cas";
  public static final String DEPOSIT_SUB_SUBTYPE_COMMENTAIRE_NAME = "Commentaire";
  public static final String DEPOSIT_SUB_SUBTYPE_ARTICLE_DONNEES_NAME = "Article de données";
  public static final String DEPOSIT_SUB_SUBTYPE_EDITORIAL_NAME = "Editorial";
  public static final String DEPOSIT_SUB_SUBTYPE_LETTRE_NAME = "Lettre";
  public static final String DEPOSIT_SUB_SUBTYPE_META_ANALYSIS_NAME = "Méta-analyse";
  public static final String DEPOSIT_SUB_SUBTYPE_REVIEW_LITERATURE_NAME = "Revue de la littérature";
  public static final String DEPOSIT_SUB_SUBTYPE_ARTICLE_VIDEO = "Article vidéo";
  public static final String DEPOSIT_SUB_SUBTYPE_BILLET_DE_BLOG_NAME = "Billet de blog";

  // Contributor roles
  public static final String CONTRIBUTOR_ROLE_AUTHOR = "author";
  public static final String CONTRIBUTOR_ROLE_DIRECTOR = "director";
  public static final String CONTRIBUTOR_ROLE_EDITOR = "editor";
  public static final String CONTRIBUTOR_ROLE_GUEST_EDITOR = "guest_editor";
  public static final String CONTRIBUTOR_ROLE_PHOTOGRAPHER = "photographer";
  public static final String CONTRIBUTOR_ROLE_TRANSLATOR = "translator";
  public static final String CONTRIBUTOR_ROLE_COLLABORATOR = "collaborator";

  public static final String CONTRIBUTOR_OTHER_NAMES_SEPARATOR = " | ";

  // Document file types
  public static final String DOCUMENT_FILE_TYPE_RECORDING = "Recording";
  public static final String DOCUMENT_FILE_TYPE_SUMMARY = "Summary";
  public static final String DOCUMENT_FILE_TYPE_SUPPLEMENTAL_DATA = "Supplemental data";
  public static final String DOCUMENT_FILE_TYPE_APPENDIX = "Appendix";
  public static final String DOCUMENT_FILE_TYPE_EXTRACT = "Extract";
  public static final String DOCUMENT_FILE_TYPE_TRANSLATION = "Translation";
  public static final String DOCUMENT_FILE_TYPE_POSTER = "Poster";
  public static final String DOCUMENT_FILE_TYPE_PREPRINT = "Preprint";
  public static final String DOCUMENT_FILE_TYPE_ADDENDUM = "Addendum";
  public static final String DOCUMENT_FILE_TYPE_CORRIGENDUM = "Corrigendum";
  public static final String DOCUMENT_FILE_TYPE_ERRATUM = "Erratum";
  public static final String DOCUMENT_FILE_TYPE_RETRACTION = "Retraction";
  public static final String DOCUMENT_FILE_TYPE_PRESENTATION = "Presentation";
  public static final String DOCUMENT_FILE_TYPE_VIGNETTE = "Vignette";
  public static final String DOCUMENT_FILE_TYPE_THESIS = "Thesis";
  public static final String DOCUMENT_FILE_TYPE_MASTER_THESIS = "Master thesis";
  public static final String DOCUMENT_FILE_TYPE_AGREEMENT_VALUE = "Agreement";
  public static final String DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE = "Imprimatur";
  public static final String DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE = "Publication mode";
  public static final String DOCUMENT_FILE_TYPE_WORKING_PAPER = "Working paper";
  public static final String DOCUMENT_FILE_TYPE_REPORT = "Report";
  public static final String DOCUMENT_FILE_TYPE_ARTICLE = "Article";

  public static final String DOCUMENT_FILE_TYPE_BOOK = "Book";
  public static final String DOCUMENT_FILE_TYPE_BOOK_CHAPTER = "Book chapter";
  public static final String DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER = "Proceedings chapter";

  public static final String DOCUMENT_FILE_TYPE_ARTICLE_SUBMITTED_VERSION = "Article (Submitted version)";
  public static final String DOCUMENT_FILE_TYPE_PROCEEDINGS_SUBMITTED_VERSION = "Proceedings (Submitted version)";
  public static final String DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER_SUBMITTED_VERSION = "Proceedings chapter (Submitted version)";
  public static final String DOCUMENT_FILE_TYPE_BOOK_SUBMITTED_VERSION = "Book (Submitted version)";
  public static final String DOCUMENT_FILE_TYPE_BOOK_CHAPTER_SUBMITTED_VERSION = "Book chapter (Submitted version)";
  public static final String DOCUMENT_FILE_TYPE_ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION = "Encyclopedia entry (Submitted version)";

  private static final String[] SUBMITTED_VERSION_DOCUMENT_FILE_TYPES = {
          DOCUMENT_FILE_TYPE_ARTICLE_SUBMITTED_VERSION,
          DOCUMENT_FILE_TYPE_PROCEEDINGS_SUBMITTED_VERSION,
          DOCUMENT_FILE_TYPE_PROCEEDINGS_CHAPTER_SUBMITTED_VERSION,
          DOCUMENT_FILE_TYPE_BOOK_SUBMITTED_VERSION,
          DOCUMENT_FILE_TYPE_BOOK_CHAPTER_SUBMITTED_VERSION,
          DOCUMENT_FILE_TYPE_ENCYCLOPEDIA_ENTRY_SUBMITTED_VERSION
  };

  public static String[] getSubmittedVersionDocumentFileTypes() {
    return SUBMITTED_VERSION_DOCUMENT_FILE_TYPES.clone();
  }

  // Document file versions
  public static final String DOCUMENT_PUBLISHED_VERSION_EN = "Published version";
  public static final String DOCUMENT_ACCEPTED_VERSION_EN = "Accepted version";
  public static final String DOCUMENT_SUBMITTED_VERSION_EN = "Submitted version";
  public static final String DOCUMENT_PUBLISHED_VERSION_FR = "Version publiée";
  public static final String DOCUMENT_ACCEPTED_VERSION_FR = "Version acceptée";
  public static final String DOCUMENT_SUBMITTED_VERSION_FR = "Version soumise";
  public static final String UNDEFINED_VERSION_EN = "Undefined version";
  public static final String UNDEFINED_VERSION_FR = "Version non définie";

  // Bibliographies
  public static final String BIBLIOGRAPHY_SORT_VALUE = "bibliographySortValue";

  // Database column size
  public static final int DB_SHORT_STRING_LENGTH = 30;
  public static final int DB_MEDIUM_STRING_LENGTH = 100;
  public static final int DB_LONG_STRING_LENGTH = 1024;
  public static final int DB_BIG_STRING_LENGTH = 2048;
  public static final int DB_LARGE_STRING_LENGTH = 4096;
  public static final int DB_DATE_LENGTH = 3;

  // Database column name
  public static final String DB_RES_ID = "resId";
  public static final String DB_INSTITUTION_ID = "institutionId";
  public static final String DB_PERSON_ID = "personId";
  public static final String DB_CREATOR_ID = "creatorId";
  public static final String DB_CONTRIBUTOR_ID = "contributorId";
  public static final String DB_FIRST_NAME = "firstName";
  public static final String DB_LAST_NAME = "lastName";
  public static final String DB_JOB_ID = "jobId";
  public static final String DB_LANGUAGE_ID = "language_id";
  public static final String DB_LANGUAGE_ENTITY_ID = "language_entity_id";
  public static final String DB_LOGO_ID = "logoId";
  public static final String DB_AVATAR_ID = "avatarId";
  public static final String DB_NAME = "name";
  public static final String DB_PARENT_STRUCTURE_ID = "parent_structure_id";
  public static final String DB_USER_ID = "userId";
  public static final String DB_RESEARCH_GROUP_ID = "researchGroupId";
  public static final String DB_STRUCTURE_ID = "structureId";
  public static final String DB_PUBLICATION_SUBTYPE_ID = "publication_subtype_id";
  public static final String DB_PUBLICATION_SUB_SUBTYPE_ID = "publication_sub_subtype_id";
  public static final String DB_THUMBNAIL_ID = "thumbnail_id";
  public static final String DB_DOCUMENT_FILE_TYPE_ID = "document_file_type_id";
  public static final String DB_DOCUMENT_FILE_ID = "document_file_id";
  public static final String DB_CONTRIBUTOR_ROLE_ID = "contributor_role_id";
  public static final String DB_GLOBAL_BANNER_ID = "global_banner_id";
  public static final String DB_LICENSE_ID = "license_id";
  public static final String DB_LICENSE_GROUP_ID = "license_group_id";
  public static final String DB_PUBLICATION_ID = "publicationId";
  public static final String DB_EVENT_TYPE_ID = "event_type_id";
  public static final String DB_NOTIFICATION_ID = "notification_id";
  public static final String DB_NOTIFICATION_TYPE_ID = "notification_type_id";
  public static final String DB_CN_INDIVIDU = "cnIndividu";
  public static final String DB_ROLE_ID = "role_id";

  // Index
  public static final String INDEX_PUBLISHED = "publications";
  public static final String INDEX_IN_PROGRESS = "publications-in-progress";

  public static final String CURRENT_DIFFUSION_FIELD = "currentDiffusion";
  public static final String FULLTEXT_VERSION_FIELD = "fulltextVersion";
  public static final String EMBARGO_END_DATE_FIELD = "embargoEndDate";

  public static final String SUBMITTED_VERSION_TEXT = "submittedVersion";
  public static final String ACCEPTED_VERSION_TEXT = "acceptedVersion";
  public static final String PUBLISHED_VERSION_TEXT = "publishedVersion";
  public static final String EXTRACT_TEXT = "extract";
  public static final String RECORDING_TEXT = "recording";
  public static final String UNKNOWN_TEXT = "unknown";

  public static final String NO_FULLTEXT_TEXT = "noFulltext";
  public static final String METADATA_ONLY_TEXT = "metadataOnly";
  public static final String EXTRACT_ONLY_TEXT = "extractOnly";

  public static final String OPEN_ACCESS_TEXT = "openAccess";
  public static final String NOT_OPEN_ACCESS_TEXT = "notOpenAccess";
  public static final String EMBARGOED_ACCESS_TEXT = "embargoedAccess";
  public static final String RESTRICTED_ACCESS_TEXT = "restrictedAccess";
  public static final String CLOSED_ACCESS_TEXT = "closedAccess";

  public static final String INDEX_FIELD_ID = "_id";
  public static final String INDEX_FIELD_YEAR = "year";
  public static final String INDEX_FIELD_DEFENSE_YEAR = "defenseYear";
  public static final String INDEX_FIELD_IMPRIMATUR_YEAR = "imprimaturYear";
  public static final String INDEX_FIELD_PUBLICATION_YEAR = "publicationYear";
  public static final String INDEX_FIELD_FIRST_ONLINE_YEAR = "firstOnlineYear";
  public static final String INDEX_FIELD_YEARS = "years";
  public static final String INDEX_FIELD_UNIGE_DIRECTORS = "unigeDirectors";
  public static final String INDEX_FIELD_ARCHIVE_ID = "archiveId";
  public static final String INDEX_FIELD_ARCHIVE_ID_INT = "archiveIdInt";
  public static final String INDEX_FIELD_FIRST_VALIDATION_DATE = "technicalInfos.firstValidation";
  public static final String INDEX_FIELD_STATUS = "technicalInfos.status";
  public static final String INDEX_FIELD_FULLTEXTS = "fulltexts";
  public static final String INDEX_FIELD_XML = "aou.xml";
  public static final String INDEX_FIELD_METADATA_VERSION = "metadata-version";
  public static final String INDEX_FIELD_SUBTYPE = "subtype";
  public static final String INDEX_FIELD_DIFFUSION = "diffusion";
  public static final String INDEX_FIELD_PRINCIPAL_FILE_ACCESS_LEVEL = "principalFileAccessLevel";
  public static final String INDEX_FIELD_OPEN_ACCESS = "openAccess";
  public static final String INDEX_FIELD_DOI_CASE_INSENSITIVE = "doi_ci";
  public static final String INDEX_FIELD_CORRECTIONS_DOI_CASE_INSENSITIVE = "corrections.doi_ci";
  public static final String INDEX_FIELD_PMID = "pmid";
  public static final String INDEX_FIELD_CORRECTIONS_PMID = "corrections.pmid";
  public static final String INDEX_FIELD_ARXIV = "arxiv";
  public static final String INDEX_FIELD_ISBN = "isbn";
  public static final String INDEX_FIELD_ISSN = "issn";
  public static final String INDEX_FIELD_SUBTYPE_CODE = "subtypeCode";
  public static final String INDEX_FIELD_COLLECTION_EXACT_NAME = "exactCollection";
  public static final String INDEX_FIELD_CONTRIBUTORS = "contributors";
  public static final String INDEX_FIELD_CONTRIBUTORS_CN_INDIVIDU = "contributors.cnIndividu";
  public static final String INDEX_FIELD_EMBARGO_END_DATES = "embargoEndDates";
  public static final String INDEX_FIELD_HTML_META_HEADERS = "htmlMetaHeaders";
  public static final String INDEX_FIELD_STRUCTURES_WITH_PARENT = "structuresWithParentsNames";
  public static final String INDEX_FIELD_TITLE = "title";

  public static final String INDEX_FIELD_ALIAS_UNIGE_DIRECTORS = "unigeDirector";
  public static final String INDEX_FIELD_ALIAS_GLOBAL_SEARCH = "globalSearch";
  public static final String INDEX_FIELD_ALIAS_GLOBAL_SEARCH_WITH_FULLTEXT = "globalSearchWithFulltext";
  public static final String INDEX_FIELD_ALIAS_PUBLISHED_IN = "publishedIn";
  public static final String INDEX_FIELD_ALIAS_TITLE = "title";
  public static final String INDEX_FIELD_ALIAS_ABSTRACT = "abstract";
  public static final String INDEX_FIELD_ALIAS_CN_INDIVIDU = "cnIndividu";
  public static final String INDEX_FIELD_ALIAS_LAST_INDEXATION_DATE = "lastIndexationDate";

  // Schema
  public static final String XSL_HOME = "xslt";
  public static final String METADATA_V1_TO_V2_XSL = "aou-deposit-metadata-v1-to-v2.xsl";
  public static final String METADATA_XML_NAMESPACE_V1 = "https://archive-ouverte.unige.ch/deposit/v1";
  public static final String METADATA_XML_NAMESPACE_V2 = "https://archive-ouverte.unige.ch/deposit/v2";
  public static final String METADATA_XML_NAMESPACE_V2_1 = "https://archive-ouverte.unige.ch/deposit/v2.1";
  public static final String METADATA_XML_NAMESPACE_V2_2 = "https://archive-ouverte.unige.ch/deposit/v2.2";
  public static final String METADATA_XML_NAMESPACE_V2_3 = "https://archive-ouverte.unige.ch/deposit/v2.3";
  public static final String METADATA_XML_NAMESPACE_V2_4 = "https://archive-ouverte.unige.ch/deposit/v2.4";
  public static final String XML_LIST_DEPOSITS = "https://archive-ouverte.unige.ch/deposit/list-deposits";

  // Folders
  public static final String METADATA_FOLDER = "metadata";
  public static final String SCHEMA_FOLDER = SolidifyConstants.SCHEMA_HOME;
  public static final String DOC_FOLDER = "documentation";
  public static final String DATA_FOLDER = "data";
  public static final String RESEARCH_DATA_FOLDER = "researchdata";
  public static final String SOFTWARE_FOLDER = "software";
  public static final String PACKAGE = "package";
  public static final String TOMBSTONE = "tombstone";
  public static final String NEW_PACKAGE = "new-package";
  public static final String FULLTEXTS_CACHE_FOLDER = "fulltexts_cache";

  // Cover page
  public static final String FILE_WITH_COVER_PAGE_PREFIX = "Cover_";
  public static final String COVER_PAGE_TEMPLATE = "cover_page";
  public static final String COVER_PAGE_SIMPLE_TEMPLATE = "cover_page_simple";
  public static final String COVER_PAGE_FILENAME = "CoverPage.pdf";

  // OAI-PMH
  public static final String OAI_AOU = "aou";
  public static final String OAI_AOU_NAME = "AoU Metadata";
  public static final String OAI_AOU_SCHEMA_2_4 = "https://archive-ouverte.unige.ch/schema/v2.4/aou-deposit-metadata.xsd";
  public static final String OAI_AOU_NAMESPACE_2_4 = "https://archive-ouverte.unige.ch/deposit/v2.4";

  public static final String OAI_MARC21 = "marc21";
  public static final String OAI_MARC21_NAME = "MARC 21 XML format";
  public static final String OAI_MARC21_SCHEMA = "marc-21.xsd";
  public static final String OAI_MARC21_SCHEMA_URL = "https://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd";
  public static final String OAI_MARC21_NAMESPACE = "http://www.loc.gov/MARC21/slim";

  public static final String OAI_EPICUR = "epicur";
  public static final String OAI_EPICUR_NAME = "Epicur format";
  public static final String OAI_EPICUR_SCHEMA = "xepicur.xsd";
  public static final String OAI_EPICUR_SCHEMA_URL = "http://www.persistent-identifier.de/xepicur/version1.0/xepicur.xsd";
  public static final String OAI_EPICUR_NAMESPACE = "urn:nbn:de:1111-2004033116";

  public static final String OAI_OPENAIRE = "oai_openaire";
  public static final String OAI_OPENAIRE_NAME = "Metadata for OpenAIRE / EOSC";
  public static final String OAI_OPENAIRE_SCHEMA = "openaire.xsd";
  public static final String OAI_OPENAIRE_SCHEMA_URL = "https://www.openaire.eu/schema/repo-lit/4.0/openaire.xsd";
  public static final String OAI_OPENAIRE_NAMESPACE = "http://namespace.openaire.eu/schema/oaire/";

  // Metadata field name
  public static final String DATE_TYPE_FIRST_ONLINE = "first_online";
  public static final String DATE_TYPE_PUBLICATION = "publication";
  public static final String DATE_TYPE_DEFENSE = "defense";
  public static final String DATE_TYPE_IMPRIMATUR = "imprimatur";
  public static final String THUMBNAIL = "thumbnail";

  // Metadata values
  public static final String INSTITUTION_UNIGE = "unige";

  public static final String FORMAT_XML = "text/xml";

  // Search
  public static final String SEARCH_QUERY = "query";
  public static final String SEARCH_FIELD = "field";
  public static final String SEARCH_FROM = "from";
  public static final String SEARCH_TO = "to";
  public static final String SEARCH_SET = "set";
  public static final String SEARCH_ORG_UNIT = "orgUnit";
  public static final String SEARCH_CONDITIONS = "conditions";

  // Preservation Job
  public static final String REPORT_ID = "{reportId}";

  // Misc.
  public static final String UNKNOWN = "unknown";

  public static final String REASON = "reason";
  public static final String GRADE = "grade";
  public static final String RATING_TYPE = "ratingType";
  public static final String CHANGE_TIME = "changeTime";

  // Tests
  public static final String PERMANENT_TEST_DATA_LABEL = "[Permanent Test Data] ";
  public static final String TEMPORARY_TEST_DATA_LABEL = "[Temporary Test Data] ";
  public static final String NO_REPLY_PREFIX = "noreply-";
  public static final String TEST_RES_ID = "test";
  public static final String TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE = """
          {
            "contributors": {
              "contributors": [
                {
                  "contributor": {
                    "cnIndividu": "12345",
                    "email": "john.doe@example.com",
                    "firstname": "John",
                    "institution": "unige",
                    "lastname": "Doe",
                    "orcid": "1234-1234-1234-1234",
                    "role": "author"
                  }
                }
              ]
            },
            "description": {
              "languages": [{
                "language" : "ENG"
                }],
              "dates": [{
                "date": "24.07.2009",
                "type": "PUBLICATION"
              }],
              "container": {
                "identifiers": {
                  "issn": "0028-0836"
                },
                "issue": "8",
                "title": "Nature"
              },
              "collections": [
                {
                  "name": "Collection1"
                },
                {
                  "name": "Collection2",
                  "number": "888"
                 }
               ]
             },
             "type": {
               "subsubtype": "A1-LETTRE",
               "subtype": "A1",
               "title": { "lang": "FRA", "text" : "%s"},
               "type": "ARTICLE"
              }
            }"
          """;

  public static final String STRUCTURE_JSON_V2_0 = """
          "academicStructures": [
            {
              "code": "%s",
              "oldCode": "%s",
              "name": "%s"
            }
          ],
          """;

  public static final String STRUCTURE_JSON_V2_1 = "\"academicStructures\": [\"%s\"],";

  public static final String RESEARCH_GROUP_JSON = """
          "groups": [
            {
              "code": "%s",
              "name": "%s"
            }
          ],
          """;
  public static final String TEST_DEPOSIT_TITLE = "Test deposit";
  public static final String TEST_DEPOSIT_JSON_FORM_DATA = String.format(TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE, TEST_DEPOSIT_TITLE);

  public static final String TEST_DEPOSIT_XML_METADATA_TEMPLATE_2_3 = """
          <deposit_doc xmlns="https://archive-ouverte.unige.ch/deposit/v2.3">
            <type>Diplôme</type>
            <subtype>Thèse</subtype>
            <title>%s</title>
            <languages>
              <language>eng</language>
            </languages>
            <pages>
              <paging>p. 12345    -67</paging>
            </pages>
          </deposit_doc>
          """;

  public static final String TEST_DEPOSIT_XML_METADATA_TEMPLATE_2_4 = """
          <deposit_doc xmlns="https://archive-ouverte.unige.ch/deposit/v2.4">
            <type>Diplôme</type>
            <subtype>Thèse</subtype>
            <title>%s</title>
            <languages>
              <language>eng</language>
            </languages>
            <pages>
              <paging>p. 12345    -67</paging>
            </pages>
          </deposit_doc>
          """;

  // Exception error message
  public static final String BAD_SEARCH_REQUEST = "Bad search request";

  // Message processor
  public static final String MESSAGE_PROCESSOR_NAME = "AoU Message Processor";

  // XML
  public static final String XML_NAMESPACE_PREFIX_MAPPER = "org.glassfish.jaxb.namespacePrefixMapper";
  public static final String XML_NAMESPACE_DEPOSIT_PREFIX = "aou_deposit";

  // XML Namespace
  public static final String AOU_SCHEMA_1 = "aou-1.0.xsd";
  public static final String AOU_NAMESPACE_1 = "https://archive-ouverte.unige.ch/aou/v1";

  public static final String LIST_DEPOSITS_SCHEMA = "aou-list-deposits-2.4.xsd";
  // Facets
  public static final String INDEXING_KEYWORD = ".keyword";
  public static final String METADATA_VERSION_FACET = "metadata-versions";
  public static final String ORG_UNIT_FACET = "organizational-units";
  public static final String ACCESS_LEVEL_FACET = "access-levels";
  public static final String DATA_TAG_FACET = "data-tags";
  public static final String CREATOR_FACET = "contributors";
  public static final String FORMAT_FACET = "formats";
  public static final String PATH_FACET = "paths";
  public static final String TYPE_FACET = "types";
  public static final String RETENTION_FACET = "retentions";
  public static final String ARCHIVE_FACET = "archive-units";
  public static final String EXTERNAL_UID = "external-uid";

  // Emails
  public static final String NOTIFICATION_EMAIL_ARCHIVE_OUVERTE_PREFIX = "[Archive ouverte]";
  public static final String NOTIFICATION_EMAIL_VALIDATION_PREFIX = "[AoU validation]";
  public static final String NOTIFICATION_IDS = "notificationIds";

  public static final String OPEN_AIRE_NAME_PARAMETER = "name";
  public static final String OPEN_AIRE_GRANT_ID_PARAMETER = "grantID";
  public static final String OPEN_AIRE_ACRONYM_PARAMETER = "acronym";

  public static final String UNIGE_EXTERNAL_UID_SUFFIX = "@unige.ch";

  public static final String UNIGE_HOME_ORGANIZATION_VALUE = "unige.ch";
  public static final String TEST_EXTERNAL_UID_SUFFIX = "_externalUid";

  public static final String TXT_EXT = ".txt";

  public static final String FILE_NAME_REPLACEMENT = "file";

  public static final String UNIVERSITY_OF_GENEVA_FR = "Université de Genève";
  public static final String HUG_FR = "Hôpitaux universitaires de Genève";
  public static final String UNIVERSITY_OF_GENEVA_ROR_ID = "https://ror.org/01swzsf04";
  public static final String HUG_ROR_ID = "https://ror.org/01m1pv723";
}
