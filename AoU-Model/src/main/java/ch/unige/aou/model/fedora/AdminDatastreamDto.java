/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AdminDatastreamDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.fedora;

import java.util.ArrayList;
import java.util.List;

public class AdminDatastreamDto {
  private String submissionDate;
  private String validationDate;
  private String creationDate;

  private String submissionCn;
  private String submissionFirstname;
  private String submissionLastName;
  private String submissionEmail;

  private List<AttachmentDto> attachments = new ArrayList<>();

  public String getSubmissionDate() {
    return this.submissionDate;
  }

  public void setSubmissionDate(String submissionDate) {
    this.submissionDate = submissionDate;
  }

  public String getValidationDate() {
    return this.validationDate;
  }

  public void setValidationDate(String validationDate) {
    this.validationDate = validationDate;
  }

  public String getCreationDate() {
    return this.creationDate;
  }

  public void setCreationDate(String creationDate) {
    this.creationDate = creationDate;
  }

  public String getSubmissionCn() {
    return this.submissionCn;
  }

  public void setSubmissionCn(String submissionCn) {
    this.submissionCn = submissionCn;
  }

  public String getSubmissionFirstname() {
    return this.submissionFirstname;
  }

  public void setSubmissionFirstname(String submissionFirstname) {
    this.submissionFirstname = submissionFirstname;
  }

  public String getSubmissionLastName() {
    return this.submissionLastName;
  }

  public void setSubmissionLastName(String submissionLastName) {
    this.submissionLastName = submissionLastName;
  }

  public String getSubmissionEmail() {
    return this.submissionEmail;
  }

  public void setSubmissionEmail(String submissionEmail) {
    this.submissionEmail = submissionEmail;
  }

  public List<AttachmentDto> getAttachments() {
    return this.attachments;
  }
}
