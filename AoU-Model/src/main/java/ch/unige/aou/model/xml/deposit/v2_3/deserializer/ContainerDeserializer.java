/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContainerDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_3.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v2_3.Container;

public class ContainerDeserializer extends AouStdDeserializer<Container> {

  private static final long serialVersionUID = 659865L;

  @Override
  public Container deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    Container container = new Container();

    boolean hasValue = false;

    JsonNode titleNode = jsonNode.get("title");
    if (this.nodeHasTextValue(titleNode)) {
      container.setTitle(titleNode.textValue());
      hasValue = true;
    }

    JsonNode editorNode = jsonNode.get("editor");
    if (this.nodeHasTextValue(editorNode)) {
      container.setEditor(editorNode.textValue());
      hasValue = true;
    }

    JsonNode volumeNode = jsonNode.get("volume");
    if (this.nodeHasTextValue(volumeNode)) {
      container.setVolume(volumeNode.textValue());
      hasValue = true;
    }

    JsonNode issueNode = jsonNode.get("issue");
    if (this.nodeHasTextValue(issueNode)) {
      container.setIssue(issueNode.textValue());
      hasValue = true;
    }

    JsonNode specialIssueNode = jsonNode.get("specialIssue");
    if (this.nodeHasTextValue(specialIssueNode)) {
      container.setSpecialIssue(specialIssueNode.textValue());
      hasValue = true;
    }

    JsonNode conferenceSubtitleNode = jsonNode.get("conferenceSubtitle");
    if (this.nodeHasTextValue(conferenceSubtitleNode)) {
      container.setConferenceSubtitle(conferenceSubtitleNode.textValue());
      hasValue = true;
    }

    JsonNode conferenceDateNode = jsonNode.get("conferenceDate");
    if (this.nodeHasTextValue(conferenceDateNode)) {
      container.setConferenceDate(conferenceDateNode.textValue());
      hasValue = true;
    }

    JsonNode conferencePlaceNode = jsonNode.get("conferencePlace");
    if (this.nodeHasTextValue(conferencePlaceNode)) {
      container.setConferencePlace(conferencePlaceNode.textValue());
      hasValue = true;
    }

    if (hasValue) {
      return container;
    } else {
      return null;
    }
  }
}
