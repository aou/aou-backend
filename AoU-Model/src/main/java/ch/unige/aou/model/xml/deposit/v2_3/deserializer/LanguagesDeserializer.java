/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - LanguagesDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_3.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v2_3.Languages;

/**
 * "links": [
 * {
 * "description": {
 * "lang": "FR",
 * "text": "remplace"
 * },
 * "target": "unige:34567",
 * "type": "INTERNAL"
 * }
 * ],
 */

public class LanguagesDeserializer extends AouStdDeserializer<Languages> {

  private static final long serialVersionUID = 987795L;

  @Override
  public Languages deserialize(JsonParser parser, DeserializationContext context) throws IOException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    if (jsonNode instanceof ArrayNode) {
      if (jsonNode.isEmpty()) {
        return null;
      }

      Languages languages = new Languages();
      for (JsonNode languageNode : jsonNode) {
        JsonNode langNode = languageNode.get("language");
        if (this.nodeHasTextValue(langNode)) {
          languages.getLanguage().add(langNode.textValue());
        } else {
          languages.getLanguage()
                  .add(""); // insert empty string b/c the front can send empty field. empty string will help when validating the data.
        }
      }
      return languages;
    } else {
      throw new SolidifyRuntimeException("languages is not an array");
    }
  }
}
