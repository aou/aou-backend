/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - SwissNationalLibraryUpload.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.schedule;

import java.time.OffsetDateTime;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.publication.Publication;

@Entity
public class SwissNationalLibraryUpload extends AouResourceNormalized {

  @ManyToOne(targetEntity = Publication.class)
  private Publication publication;

  @NotNull
  private String documentFileChecksum;

  @NotNull
  private String documentFileName;

  @NotNull
  private String documentFileType;

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime uploadDate;

  public Publication getPublication() {
    return this.publication;
  }

  public void setPublication(Publication publication) {
    this.publication = publication;
  }

  public String getDocumentFileChecksum() {
    return this.documentFileChecksum;
  }

  public void setDocumentFileChecksum(String documentFileChecksum) {
    this.documentFileChecksum = documentFileChecksum;
  }

  public String getDocumentFileName() {
    return this.documentFileName;
  }

  public void setDocumentFileName(String documentFileName) {
    this.documentFileName = documentFileName;
  }

  public String getDocumentFileType() {
    return this.documentFileType;
  }

  public void setDocumentFileType(String documentFileType) {
    this.documentFileType = documentFileType;
  }

  public OffsetDateTime getUploadDate() {
    return this.uploadDate;
  }

  public void setUploadDate(OffsetDateTime uploadDate) {
    this.uploadDate = uploadDate;
  }
}
