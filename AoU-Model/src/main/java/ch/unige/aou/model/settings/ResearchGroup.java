/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ResearchGroup.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.util.HashSet;
import java.util.Set;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.util.SearchCriteria;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouSearchableResourceNormalized;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.specification.ResearchGroupSearchSpecification;

@Entity
public class ResearchGroup extends AouSearchableResourceNormalized<ResearchGroup> {

  @Size(min = 1)
  @NotNull
  @Column(name = AouConstants.DB_NAME)
  private String name;

  @Column(unique = true)
  private String code;

  @Column()
  private String acronym;

  @NotNull
  private Boolean active;

  /**
   * Simple users can create new research groups, but by default they are not validated
   */
  @NotNull
  private Boolean validated;

  @ManyToMany(mappedBy = "researchGroups")
  @JsonIgnore
  private Set<Person> people;

  @ManyToMany(mappedBy = "researchGroups")
  @JsonIgnore
  private Set<Publication> publications = new HashSet<>();

  /****************************************************************/

  @Override
  public void init() {

    if (this.active == null) {
      this.active = true;
    }

    if (this.validated == null) {
      this.validated = false;
    }
  }

  /****************************************************************/

  public Set<Person> getPeople() {
    return this.people;
  }

  public String getName() {
    return this.name;
  }

  public String getCode() {
    return this.code;
  }

  public String getAcronym() {
    return this.acronym;
  }

  public Boolean isActive() {
    return this.active;
  }

  public Boolean isValidated() {
    return this.validated;
  }

  public Set<Publication> getPublications() {
    return this.publications;
  }

  /****************************************************************/

  public void setName(String name) {
    this.name = name;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public void setValidated(Boolean validated) {
    this.validated = validated;
  }

  public void setPublications(Set<Publication> publications) {
    this.publications = publications;
  }

  /****************************************************************/

  public boolean addPerson(Person p) {
    final boolean result = this.people.add(p);
    if (!p.getResearchGroups().contains(this)) {
      p.addResearchGroup(this);
    }
    return result;
  }

  public boolean removePerson(Person p) {
    final boolean result = this.people.remove(p);
    if (p.getResearchGroups().contains(this)) {
      p.removeResearchGroup(this);
    }
    return result;
  }

  public boolean addPublication(Publication p) {
    final boolean result = this.publications.add(p);
    if (!p.getResearchGroups().contains(this)) {
      p.addResearchGroup(this);
    }
    return result;
  }

  public boolean removePublication(Publication p) {
    final boolean result = this.publications.remove(p);
    if (p.getResearchGroups().contains(this)) {
      p.removeResearchGroup(this);
    }
    return result;
  }

  @Override
  public Specification<ResearchGroup> getSearchSpecification(SearchCriteria criteria) {
    return new ResearchGroupSearchSpecification(criteria);
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PUBLICATIONS).withRel(ResourceName.PUBLICATIONS));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PEOPLE).withRel(ResourceName.PEOPLE));
    }
  }
}
