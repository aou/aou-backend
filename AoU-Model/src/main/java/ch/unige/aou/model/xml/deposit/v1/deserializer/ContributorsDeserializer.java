/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContributorsDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v1.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v1.AuthorRole;
import ch.unige.aou.model.xml.deposit.v1.Collaboration;
import ch.unige.aou.model.xml.deposit.v1.Contributor;
import ch.unige.aou.model.xml.deposit.v1.Contributors;
import ch.unige.aou.model.xml.deposit.v1.OtherName;
import ch.unige.aou.model.xml.deposit.v1.OtherNames;

public class ContributorsDeserializer extends AouStdDeserializer<Contributors> {

  private static final long serialVersionUID = 59359324234L;

  @Override
  public Contributors deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    if (jsonNode instanceof ArrayNode) {
      if (jsonNode.isEmpty()) {
        return null;
      }

      Contributors contributors = new Contributors();
      for (JsonNode subnode : jsonNode) {
        if (subnode.get("contributor") != null) {
          this.addContributor(subnode.get("contributor"), contributors);
        } else if (subnode.get("collaboration") != null) {
          this.addCollaboration(subnode.get("collaboration"), contributors);
        } else {
          throw new SolidifyRuntimeException("contributors list must contain 'contributor' or 'collaboration'");
        }
      }
      return contributors;

    } else {
      throw new SolidifyRuntimeException("contributors is not an array");
    }
  }

  private void addContributor(JsonNode contributorNode, Contributors contributors) {
    Contributor contributor = new Contributor();

    JsonNode firstnameNode = contributorNode.get("firstname");
    if (this.nodeHasTextValue(firstnameNode)) {
      contributor.setFirstname(firstnameNode.textValue());
    }

    JsonNode lastnameNode = contributorNode.get("lastname");
    if (this.nodeHasTextValue(lastnameNode)) {
      contributor.setLastname(lastnameNode.textValue());
    }

    JsonNode institutionNode = contributorNode.get("institution");
    if (this.nodeHasTextValue(institutionNode)) {
      contributor.setInstitution(institutionNode.textValue());
    }

    JsonNode roleNode = contributorNode.get("role");
    if (this.nodeHasTextValue(roleNode)) {
      contributor.setRole(AuthorRole.fromValue(roleNode.textValue()));
    }

    JsonNode cnIndividuNode = contributorNode.get("cnIndividu");
    if (this.nodeHasTextValue(cnIndividuNode)) {
      contributor.setCnIndividu(cnIndividuNode.textValue());
    }

    JsonNode emailNode = contributorNode.get("email");
    if (this.nodeHasTextValue(emailNode)) {
      contributor.setEmail(emailNode.textValue());
    }

    JsonNode orcidNode = contributorNode.get("orcid");
    if (this.nodeHasTextValue(orcidNode)) {
      contributor.setOrcid(orcidNode.textValue());
    }

    JsonNode otherNamesNode = contributorNode.get("otherNames");
    if (otherNamesNode instanceof ArrayNode) {

      OtherNames otherNames = new OtherNames();

      otherNamesNode.forEach(otherNameNode -> {
        OtherName otherName = new OtherName();

        JsonNode otherNameFirstnameNode = otherNameNode.get("firstname");
        if (this.nodeHasTextValue(otherNameFirstnameNode)) {
          otherName.setFirstname(otherNameFirstnameNode.textValue());
        }

        JsonNode otherNameLastnameNode = otherNameNode.get("lastname");
        if (this.nodeHasTextValue(otherNameLastnameNode)) {
          otherName.setLastname(otherNameLastnameNode.textValue());
        }

        otherNames.getOtherName().add(otherName);
      });

      contributor.setOtherNames(otherNames);
    }

    contributors.getContributorOrCollaboration().add(contributor);
  }

  private void addCollaboration(JsonNode collaborationNode, Contributors contributors) {
    Collaboration collaboration = new Collaboration();

    JsonNode nameNode = collaborationNode.get("name");
    if (this.nodeHasTextValue(nameNode)) {
      collaboration.setName(nameNode.textValue());
    }

    contributors.getContributorOrCollaboration().add(collaboration);
  }
}
