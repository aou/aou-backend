/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AttachmentDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.fedora;

import java.time.OffsetDateTime;

public class AttachmentDto {
  private String datastreamId;
  private String fileName;
  private Long size;
  private String availability;
  private String description;
  private String descriptionComplement;
  private String license;
  private String embargoAvailability;
  private String embargoEndDate;
  private Integer embargoPeriod;
  private String lastSyncWithSwissNationalLibrarySha1;
  private OffsetDateTime lastSyncWithSwissNationalLibraryDate;

  public String getDatastreamId() {
    return this.datastreamId;
  }

  public void setDatastreamId(String datastreamId) {
    this.datastreamId = datastreamId;
  }

  public String getFileName() {
    return this.fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Long getSize() {
    return this.size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  public String getAvailability() {
    return this.availability;
  }

  public void setAvailability(String availability) {
    this.availability = availability;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getDescriptionComplement() {
    return this.descriptionComplement;
  }

  public void setDescriptionComplement(String descriptionComplement) {
    this.descriptionComplement = descriptionComplement;
  }

  public String getLicense() {
    return this.license;
  }

  public void setLicense(String license) {
    this.license = license;
  }

  public String getEmbargoAvailability() {
    return this.embargoAvailability;
  }

  public void setEmbargoAvailability(String embargoAvailability) {
    this.embargoAvailability = embargoAvailability;
  }

  public String getEmbargoEndDate() {
    return this.embargoEndDate;
  }

  public void setEmbargoEndDate(String embargoEndDate) {
    this.embargoEndDate = embargoEndDate;
  }

  public Integer getEmbargoPeriod() {
    return this.embargoPeriod;
  }

  public void setEmbargoPeriod(Integer embargoPeriod) {
    this.embargoPeriod = embargoPeriod;
  }

  public String getLastSyncWithSwissNationalLibrarySha1() {
    return this.lastSyncWithSwissNationalLibrarySha1;
  }

  public void setLastSyncWithSwissNationalLibrarySha1(String lastSyncWithSwissNationalLibrarySha1) {
    this.lastSyncWithSwissNationalLibrarySha1 = lastSyncWithSwissNationalLibrarySha1;
  }

  public OffsetDateTime getLastSyncWithSwissNationalLibraryDate() {
    return this.lastSyncWithSwissNationalLibraryDate;
  }

  public void setLastSyncWithSwissNationalLibraryDate(OffsetDateTime lastSyncWithSwissNationalLibraryDate) {
    this.lastSyncWithSwissNationalLibraryDate = lastSyncWithSwissNationalLibraryDate;
  }
}
