/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DateType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

public enum DateType {
  PUBLICATION("publication"),
  FIRST_ONLINE("first_online"),
  IMPRIMATUR("imprimatur"),
  DEFENSE("defense");

  private final String metadataValue;

  DateType(String metadataValue) {
    this.metadataValue = metadataValue;
  }

  public String metadataValue() {
    return this.metadataValue;
  }

  public static DateType fromMetadataValue(String metadataValue) {
    for (DateType dateType : DateType.values()) {
      if (dateType.metadataValue.equals(metadataValue)) {
        return dateType;
      }
    }
    throw new IllegalArgumentException(metadataValue);
  }
}
