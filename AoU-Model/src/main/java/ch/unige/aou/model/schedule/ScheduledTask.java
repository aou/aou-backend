/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ScheduledTask.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.schedule;

import java.time.OffsetDateTime;
import java.util.Objects;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.scheduling.support.CronExpression;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.scheduler.ScheduledTaskInterface;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.listener.HistoryListener;
import ch.unige.aou.model.AouResourceNormalized;

@Entity
@EntityListeners({ HistoryListener.class })
public class ScheduledTask extends AouResourceNormalized implements ScheduledTaskInterface {

  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String name;

  @NotNull
  @Enumerated(EnumType.STRING)
  private TaskType taskType;

  /**
   * min length is with "* * * * * *" which has 11 chars
   */
  @NotNull
  @Size(min = 11)
  private String cronExpression;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime lastExecutionStart;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime lastExecutionEnd;

  private Boolean enabled;

  @Transient
  private boolean isNameDirty = false;

  @Transient
  private boolean isTaskTypeDirty = false;

  @Transient
  private boolean isCronExpressionDirty = false;

  @Transient
  private boolean isEnabledDirty = false;

  /****************************************************************************************************/

  @Override
  public void init() {
    if (this.enabled == null) {
      this.enabled = true;
    }
  }

  /****************************************************************************************************/

  @Override
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    if (!Objects.equals(this.name, name)) {
      this.name = name;
      this.isNameDirty = true;
    }
  }

  @JsonIgnore
  public boolean isNameDirty() {
    return this.isNameDirty;
  }

  public TaskType getTaskType() {
    return this.taskType;
  }

  public void setTaskType(TaskType taskType) {
    if (!Objects.equals(this.taskType, taskType)) {
      this.taskType = taskType;
      this.isTaskTypeDirty = true;
    }
  }

  @JsonIgnore
  public boolean isTaskTypeDirty() {
    return this.isTaskTypeDirty;
  }

  @Override
  public String getCronExpression() {
    return this.cronExpression;
  }

  public void setCronExpression(String cronExpression) {
    if (!Objects.equals(this.cronExpression, cronExpression)) {
      this.cronExpression = cronExpression;
      this.isCronExpressionDirty = true;
    }
  }

  @JsonIgnore
  public boolean isCronExpressionDirty() {
    return this.isCronExpressionDirty;
  }

  @Override
  public OffsetDateTime getLastExecutionStart() {
    return this.lastExecutionStart;
  }

  @Override
  public void setLastExecutionStart(OffsetDateTime lastExecutionStart) {
    this.lastExecutionStart = lastExecutionStart;
  }

  @Override
  public OffsetDateTime getLastExecutionEnd() {
    return this.lastExecutionEnd;
  }

  @Override
  public void setLastExecutionEnd(OffsetDateTime lastExecutionEnd) {
    this.lastExecutionEnd = lastExecutionEnd;
  }

  @Override
  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    if (!Objects.equals(this.enabled, enabled)) {
      this.enabled = enabled;
      this.isEnabledDirty = true;
    }
  }

  @JsonIgnore
  public boolean isEnabledDirty() {
    return this.isEnabledDirty;
  }

  /****************************************************************************************************/

  public OffsetDateTime getNextExecution() {
    if (StringTool.isNullOrEmpty(this.cronExpression) || !this.enabled) {
      return null;
    }

    CronExpression cronTrigger = CronExpression.parse(this.cronExpression);
    return cronTrigger.next(OffsetDateTime.now());
  }

  public void resetDirtyFields() {
    this.isNameDirty = false;
    this.isTaskTypeDirty = false;
    this.isCronExpressionDirty = false;
    this.isEnabledDirty = false;
  }
}
