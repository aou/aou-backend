/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationMetadataDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.index;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.AouConstants;

public class PublicationMetadataDeserializer extends JsonDeserializer<PublicationMetadata> {

  @Override
  public PublicationMetadata deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    final JsonNode node = jp.getCodec().readTree(jp);
    if (node.has("type") && node.has("index") && node.has(AouConstants.DB_RES_ID)) {
      final PublicationMetadata am = new PublicationMetadata(IndexType.valueOf(node.get("type").asText().toUpperCase()));
      am.setResId(node.get(AouConstants.DB_RES_ID).asText());
      am.setIndex(node.get("index").asText());
      if (node.has("metadata")) {
        am.setMetadata(node.findValue("metadata").toString());
      }
      return am;
    } else {
      throw new SolidifyValidationException(new ValidationError("JSON fields 'type', 'index' and '" +
              AouConstants.DB_RES_ID + "' must be present"));
    }
  }
}
