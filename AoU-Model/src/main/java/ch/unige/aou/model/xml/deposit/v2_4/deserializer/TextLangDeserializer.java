/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - TextLangDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_4.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.TextLang;

/**
 * Parse a TextLang which is a text with a mandatory lang
 */
public class TextLangDeserializer extends AouStdDeserializer<TextLang> {

  private static final long serialVersionUID = 2843139899413104666L;

  @Override
  public TextLang deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    JsonNode textNode = jsonNode.get("text");
    JsonNode langNode = jsonNode.get("lang");

    TextLang textLang = new TextLang();

    boolean hasValue = false;

    if (this.nodeHasTextValue(textNode)) {
      textLang.setContent(textNode.textValue());
      hasValue = true;
    }

    if (this.nodeHasTextValue(langNode)) {
      textLang.setLang(langNode.textValue());
      hasValue = true;
    }

    if (hasValue) {
      return textLang;
    } else {
      return null;
    }
  }
}
