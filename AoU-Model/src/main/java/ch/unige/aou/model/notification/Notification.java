/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Notification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.notification;

import java.time.OffsetDateTime;
import java.util.Objects;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.settings.Person;

@Entity
public class Notification extends AouResourceNormalized {

  @NotNull
  @ManyToOne
  private Person recipient;

  @NotNull
  @Size(min = 1, max = AouConstants.DB_LARGE_STRING_LENGTH)
  private String message;

  @NotNull
  @ManyToOne
  private NotificationType notificationType;

  @NotNull
  @ManyToOne
  private Event event;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = 3)
  private OffsetDateTime readTime;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = 3)
  private OffsetDateTime sentTime;

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Notification)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    final Notification that = (Notification) o;
    return this.notificationType == that.notificationType &&
            Objects.equals(this.recipient, that.recipient) &&
            Objects.equals(this.message, that.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.notificationType, this.recipient, this.message);
  }

  public Person getRecipient() {
    return this.recipient;
  }

  public void setRecipient(Person recipient) {
    this.recipient = recipient;
  }

  public String getMessage() {
    return this.message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public NotificationType getNotificationType() {
    return this.notificationType;
  }

  public void setNotificationType(NotificationType notificationType) {
    this.notificationType = notificationType;
  }

  public OffsetDateTime getReadTime() {
    return this.readTime;
  }

  public void setReadTime(OffsetDateTime readTime) {
    this.readTime = readTime;
  }

  public OffsetDateTime getSentTime() {
    return this.sentTime;
  }

  public void setSentTime(OffsetDateTime sentTime) {
    this.sentTime = sentTime;
  }

  public Event getEvent() {
    return this.event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }
}
