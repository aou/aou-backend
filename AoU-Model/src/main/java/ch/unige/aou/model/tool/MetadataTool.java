/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - MetadataTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.tool;

import java.util.Map;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;

public class MetadataTool {

  public static String generateSwissNationalLibraryUrn(String urnPrefix, String archiveId) {
    if (StringTool.isNullOrEmpty(archiveId)) {
      throw new SolidifyRuntimeException("archiveId should not be null");
    }

    String urnWithoutControlValue = urnPrefix + archiveId.replace(":", "-");
    String controlValue = MetadataTool.getUrnControlValue(urnWithoutControlValue);
    return urnWithoutControlValue + controlValue;
  }

  private static String getUrnControlValue(String urnWithoutControlValue) {
    StringBuilder sb = new StringBuilder();
    Map<String, String> conversionTable = AouConstants.getSwissNationalLibraryUrnConversionTable();
    for (char c : urnWithoutControlValue.toCharArray()) {
      sb.append(conversionTable.get(String.valueOf(c)));
    }

    String encodedUrn = sb.toString();
    int total = 0;
    for (int i = 1; i <= encodedUrn.length(); i++) {
      char c = encodedUrn.charAt(i - 1);
      int digit = Integer.parseInt(String.valueOf(c));
      total += i * digit;
    }

    int divider = Integer.parseInt(encodedUrn.substring(encodedUrn.length() - 1));
    float division = (float) total / (float) divider;
    String divisionStr = String.valueOf(division);
    int dotIndex = divisionStr.indexOf(".");
    return divisionStr.substring(dotIndex - 1, dotIndex);
  }
}
