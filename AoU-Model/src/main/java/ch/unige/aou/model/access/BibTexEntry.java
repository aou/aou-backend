/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ExportFormat.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.access;

import java.util.LinkedHashMap;
import java.util.Map;

import ch.unige.aou.model.tool.CleanTool;

public class BibTexEntry {
  private BibTexEntryDefinition type;
  private Map<String, String> fields = new LinkedHashMap<>();

  public BibTexEntry(BibTexEntryDefinition type) {
    this.type = type;
  }

  public BibTexEntryDefinition getType() {
    return type;
  }

  public void setType(BibTexEntryDefinition type) {
    this.type = type;
  }

  public Map<String, String> getFields() {
    return fields;
  }

  public void setFields(Map<String, String> fields) {
    this.fields = fields;
  }

  public boolean isValid() {
    return this.type.isValid(this);
  }

  public String toBibTeX(String archiveId) {
    StringBuilder sb = new StringBuilder();
    sb.append("@").append(type.getType()).append("{").append(archiveId).append(",");
    sb.append("\n");

    for (Map.Entry<String, String> entry : fields.entrySet()) {
      sb.append("\t").append(entry.getKey()).append(" = {").append(this.escapeValue(entry.getValue(), entry.getKey().equals("title"))).append("},").append("\n");
    }

    // Delete last comma and close bracket
    sb.deleteCharAt(sb.length() - 2);
    sb.append("}");
    sb.append("\n");

    return sb.toString();
  }

  private String escapeValue(String value, boolean isTitleProperty) {
    value = CleanTool.removeAllHtmlTags(value);
    return BibTexEscaper.escapeForBibTeX(value, isTitleProperty);
  }

}
