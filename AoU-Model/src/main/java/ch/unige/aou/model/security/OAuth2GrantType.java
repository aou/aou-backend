/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - OAuth2GrantType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.security;

import com.fasterxml.jackson.annotation.JsonValue;

public enum OAuth2GrantType {

  AUTHORIZATION_CODE("authorization_code"), CLIENT_CREDENTIALS("client_credentials"), IMPLICIT("implicit"), REFRESH_TOKEN("refresh_token");

  private final String displayName;

  OAuth2GrantType(String displayName) {
    this.displayName = displayName;
  }

  @JsonValue
  @Override
  public String toString() {
    return this.displayName;
  }
}
