/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContributorDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ch.unige.solidify.util.StringTool;

public class ContributorDTO extends AbstractContributor {
  private String firstName;
  private String lastName;
  private String email;
  private String institution;
  private String role;
  private String orcid;
  private String cnIndividu;
  private String structure;
  private String type;
  private List<OtherNameDTO> otherNames = new ArrayList<>();

  public ContributorDTO() {
  }

  public ContributorDTO(String firstName, String lastName, String orcid, String cnIndividu) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.orcid = orcid;
    this.cnIndividu = cnIndividu;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getInstitution() {
    return this.institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public String getRole() {
    return this.role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getOrcid() {
    return this.orcid;
  }

  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public String getCnIndividu() {
    return this.cnIndividu;
  }

  public void setCnIndividu(String cnIndividu) {
    this.cnIndividu = cnIndividu;
  }

  public String getStructure() {
    return this.structure;
  }

  public void setStructure(String structure) {
    this.structure = structure;
  }

  public List<OtherNameDTO> getOtherNames() {
    return this.otherNames;
  }

  public void setOtherNames(List<OtherNameDTO> otherNames) {
    this.otherNames = otherNames;
  }
  
  public String getFullName() {
    String fullName = this.lastName;
    if (!StringTool.isNullOrEmpty(this.firstName)) {
      fullName = fullName + ", " + this.firstName;
    }
    return fullName;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }

    ContributorDTO that = (ContributorDTO) o;

    if (!StringTool.isNullOrEmpty(this.cnIndividu) && Objects.equals(this.cnIndividu, that.cnIndividu)) {
      return true;
    }

    if (!StringTool.isNullOrEmpty(this.orcid) && Objects.equals(this.orcid, that.orcid)) {
      return true;
    }

    if (StringTool.isNullOrEmpty(this.cnIndividu) && StringTool.isNullOrEmpty(this.orcid)
            && StringTool.isNullOrEmpty(that.cnIndividu) && StringTool.isNullOrEmpty(that.orcid)) {
      // homonyms without any identifier are considered as the same person
      if (Objects.equals(this.firstName, that.firstName) && Objects.equals(this.lastName, that.lastName)) {
        return true;
      }
    }

    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.firstName, this.lastName, this.orcid, this.cnIndividu);
  }

  @Override
  public String toString() {
    String str = this.getFullName();
    if (!StringTool.isNullOrEmpty(this.cnIndividu)) {
      str += " (" + this.cnIndividu + ")";
    }
    return str;
  }
}
