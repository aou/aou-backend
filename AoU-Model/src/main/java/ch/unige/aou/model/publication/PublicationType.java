/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.rest.ResourceName;

@Entity
public class PublicationType extends AouResourceNormalized implements Sortable {

  /**
   * Type name (e.g: Article, Livre)
   */
  @Size(min = 1)
  @NotNull
  @Column(unique = true)
  private String name;

  /**
   * Property allowing to sort types in lists
   */
  @Column(nullable = false)
  private Integer sortValue;

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public Integer getSortValue() {
    return this.sortValue;
  }

  @Override
  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PUBLICATION_SUBTYPES).withRel(ResourceName.PUBLICATION_SUBTYPES));
    }
  }
}
