/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - JournalTitleDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.rest;

import java.io.Serializable;

public class JournalTitleDTO implements Serializable {
  private String mainTitle;
  private String startingYear;
  private String keytitle;
  private String publisher;
  private String issn;
  private String issnl;
  private String url;
  private String countryCode;

  public JournalTitleDTO() {
  }

  public String getMainTitle() {
    return mainTitle;
  }

  public void setMainTitle(String mainTitle) {
    this.mainTitle = mainTitle;
  }

  public String getStartingYear() {
    return startingYear;
  }

  public void setStartingYear(String startingYear) {
    this.startingYear = startingYear;
  }

  public String getKeytitle() {
    return keytitle;
  }

  public void setKeytitle(String keytitle) {
    this.keytitle = keytitle;
  }

  public String getIssn() {
    return issn;
  }

  public void setIssn(String issn) {
    this.issn = issn;
  }

  public String getIssnl() {
    return issnl;
  }

  public void setIssnl(String issnl) {
    this.issnl = issnl;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPublisher() {
    return publisher;
  }

  public void setPublisher(String publisher) {
    this.publisher = publisher;
  }

  @Override
  public String toString() {
    return "JournalTitleDTO{" +
            "mainTitle='" + mainTitle + '\'' +
            ", startingYear='" + startingYear + '\'' +
            ", keytitle='" + keytitle + '\'' +
            ", publisher='" + publisher + '\'' +
            ", issn='" + issn + '\'' +
            ", issnl='" + issnl + '\'' +
            ", url='" + url + '\'' +
            ", countryCode='" + countryCode + '\'' +
            '}';
  }
}
