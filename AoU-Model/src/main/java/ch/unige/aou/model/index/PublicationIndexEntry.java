/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationIndexEntry.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.index;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.model.oai.OAIRecord;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.MetadataFile;

public class PublicationIndexEntry extends IndexMetadata implements OAIRecord {

  @JsonIgnore
  public String getArchiveId() {
    if (this.getMetadata().get(AouConstants.INDEX_FIELD_ARCHIVE_ID) != null) {
      return this.getMetadata().get(AouConstants.INDEX_FIELD_ARCHIVE_ID).toString();
    }
    return null;
  }

  @JsonIgnore
  public Integer getYear() {
    if (this.getMetadata().get("year") != null) {
      return Integer.parseInt(this.getMetadata().get("year").toString());
    }
    return null;
  }

  @JsonIgnore
  public String getTitle() {
    return this.getMetadata().get(AouConstants.INDEX_FIELD_TITLE).toString();
  }

  @JsonIgnore
  public String getType() {
    return this.getMetadata().get("type").toString();
  }

  @JsonIgnore
  public String getSubtype() {
    return this.getMetadata().get("subtype").toString();
  }

  @JsonIgnore
  public String getSubSubtype() {
    if (this.getMetadata().get("subsubtype") != null) {
      return this.getMetadata().get("subsubtype").toString();
    }
    return null;
  }

  @JsonIgnore
  public Map<String, Object> getContributors() {
    return (Map<String, Object>) this.getMetadata().get("contributors");
  }

  @JsonIgnore
  public Map<String, Object> getFundings() {
    return (Map<String, Object>) this.getMetadata().get("fundings");
  }

  @JsonIgnore
  public String getFirstContributorFullname() {
    if (this.getMetadata().containsKey("contributors")) {
      Map<String, Object> firstContributorMap = (Map<String, Object>) ((List) this.getMetadata().get("contributors")).get(0);
      String fullName = "";
      if (firstContributorMap.containsKey("lastName")) {
        fullName += firstContributorMap.get("lastName").toString();
      }
      if (firstContributorMap.containsKey("firstName")) {
        fullName += ", " + firstContributorMap.get("firstName").toString();
      }
      return fullName;
    }
    return null;
  }

  @JsonIgnore
  public List<String> getDirectorsCnIndividus() {
    if (this.getMetadata().containsKey(AouConstants.INDEX_FIELD_UNIGE_DIRECTORS)) {
      return (List<String>) this.getMetadata().get(AouConstants.INDEX_FIELD_UNIGE_DIRECTORS);
    }
    return new ArrayList<>();
  }

  @JsonIgnore
  public List<String> getContributorsCnIndividus() {
    if (this.getMetadata().containsKey(AouConstants.INDEX_FIELD_CONTRIBUTORS)) {
      List<Map<String, String>> propContributors = (List<Map<String, String>>) this.getMetadata().get(AouConstants.INDEX_FIELD_CONTRIBUTORS);
      return propContributors.stream()
              .filter(m -> m.containsKey(AouConstants.INDEX_FIELD_ALIAS_CN_INDIVIDU))
              .map(m -> m.get(AouConstants.INDEX_FIELD_ALIAS_CN_INDIVIDU)).toList();
    }
    return new ArrayList<>();
  }

  @JsonIgnore
  public String getFirstAbstract() {
    if (this.getMetadata("abstracts") != null) {
      Map<String, Object> firstAbstractMap = (Map<String, Object>) ((List) this.getMetadata().get("abstracts")).get(0);
      return firstAbstractMap.get("content").toString();
    }
    return null;
  }

  @JsonIgnore
  public boolean hasThumbnail() {
    return this.getMetadata("thumbnail") != null;
  }

  @JsonIgnore
  public boolean hasFulltext() {
    return this.getMetadata(AouConstants.INDEX_FIELD_FULLTEXTS) != null;
  }

  @JsonIgnore
  public String getDiffusion() {
    if (this.getMetadata().get("diffusion") != null) {
      return this.getMetadata().get("diffusion").toString();
    } else {
      return null;
    }
  }

  @JsonIgnore
  public String getOpenAccess() {
    if (this.getMetadata().get(AouConstants.INDEX_FIELD_OPEN_ACCESS) != null) {
      return this.getMetadata().get(AouConstants.INDEX_FIELD_OPEN_ACCESS).toString();
    } else {
      return null;
    }
  }

  @JsonIgnore
  public String getPrincipalFileAccessLevel() {
    if (this.getMetadata().get(AouConstants.INDEX_FIELD_PRINCIPAL_FILE_ACCESS_LEVEL) != null) {
      return this.getMetadata().get(AouConstants.INDEX_FIELD_PRINCIPAL_FILE_ACCESS_LEVEL).toString();
    } else {
      return null;
    }
  }

  @JsonIgnore
  public List<MetadataFile> getFiles() {
    List<MetadataFile> metadataFiles = new ArrayList<>();
    if (this.getMetadata("files") != null) {
      final ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      for (Map<String, Object> file : ((List<Map<String, Object>>) this.getMetadata().get("files"))) {
        metadataFiles.add(objectMapper.convertValue(file, MetadataFile.class));
      }
    }
    return metadataFiles;
  }

  @Override
  @JsonIgnore
  public OffsetDateTime getCreationDate() {
    final String updateTime = "technicalInfos.updateTime";
    if (this.getMetadata(updateTime.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) instanceof OffsetDateTime creation) {
      return creation;
    } else {
      return OffsetDateTime.parse(
                      (String) this.getMetadata(updateTime.split(SolidifyConstants.REGEX_FIELD_PATH_SEP))).toInstant()
              .atOffset(ZoneOffset.UTC)
              .withNano(0);
    }
  }

  @JsonIgnore
  public OffsetDateTime getValidationDate() {
    final String firstValidationDate = "technicalInfos.firstValidation";
    if (this.getMetadata(firstValidationDate.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) == null) {
      return null;
    } else if (this.getMetadata(firstValidationDate.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) instanceof OffsetDateTime validationDate) {
      return validationDate;
    } else {
      return OffsetDateTime.parse(
                      (String) this.getMetadata(firstValidationDate.split(SolidifyConstants.REGEX_FIELD_PATH_SEP))).toInstant()
              .atOffset(ZoneOffset.UTC)
              .withNano(0);
    }
  }

  @JsonIgnore
  public OffsetDateTime getLastUpdateDate() {
    final String updateFile = "technicalInfos.updateTime";
    if (this.getMetadata(updateFile.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) == null) {
      return null;
    } else if (this.getMetadata(updateFile.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) instanceof OffsetDateTime updateDate) {
      return updateDate;
    } else {
      return OffsetDateTime.parse(
                      (String) this.getMetadata(updateFile.split(SolidifyConstants.REGEX_FIELD_PATH_SEP))
              ).toInstant()
              .atOffset(ZoneOffset.UTC)
              .withNano(0);
    }
  }

  @JsonIgnore
  public String getStatus() {
    final String status = "technicalInfos.status";
    if (this.getMetadata(status.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) instanceof String statusValue) {
      return statusValue;
    } else {
      return null;
    }
  }

  @JsonIgnore
  public String getCreator() {
    final String creator = "creator.fullName";
    if (this.getMetadata(creator.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) instanceof String fullName) {
      return fullName;
    } else {
      return null;
    }
  }

  @JsonIgnore
  public String getValidatorName() {
    final String validatorNameStr = "technicalInfos.validatorName";
    if (this.getMetadata(validatorNameStr.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) instanceof String validatorName) {
      return validatorName;
    } else {
      return null;
    }
  }

  @JsonIgnore
  public String getPublisherVersionUrl() {
    if (this.getMetadata().get("publisherVersionUrl") != null) {
      return this.getMetadata().get("publisherVersionUrl").toString();
    } else {
      return null;
    }
  }

  @JsonIgnore
  public OffsetDateTime getLastIndexationDate() {
    final String lastIndexationDateStr = "technicalInfos.lastIndexationDate";
    if (this.getMetadata(lastIndexationDateStr.split(SolidifyConstants.REGEX_FIELD_PATH_SEP)) instanceof OffsetDateTime lastIndexation) {
      return lastIndexation;
    } else {
      return OffsetDateTime.parse(
                      (String) this.getMetadata(lastIndexationDateStr.split(SolidifyConstants.REGEX_FIELD_PATH_SEP))).toInstant()
              .atOffset(ZoneOffset.UTC)
              .withNano(0);
    }
  }

  @JsonIgnore
  public List<String> getLanguages() {
    List<String> languages = new ArrayList<>();
    if (this.getMetadata("languages") != null) {
      final ObjectMapper objectMapper = new ObjectMapper();
      objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      for (Map<String, Object> lang : ((List<Map<String, Object>>) this.getMetadata().get("languages"))) {
        languages.add(objectMapper.convertValue(lang, String.class));
      }
    }
    return languages;
  }

  @JsonIgnore
  public String getContainerTitle() {
    if (this.getMetadata("container", AouConstants.INDEX_FIELD_TITLE) != null) {
      return this.getMetadata("container", AouConstants.INDEX_FIELD_TITLE).toString();
    }
    return null;
  }

  @JsonIgnore
  public String getXmlMetadata() {
    return (String) this.getMetadata(AouConstants.INDEX_FIELD_XML);
  }

  @Override
  @JsonIgnore
  public String getOAIMetadata() {
    return (String) this.getMetadata(AouConstants.INDEX_FIELD_XML);
  }

  @Override
  @JsonIgnore
  public String getOaiId() {
    return this.getArchiveId();
  }

}
