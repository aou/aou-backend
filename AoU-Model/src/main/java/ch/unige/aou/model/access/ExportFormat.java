/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ExportFormat.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.access;

public enum ExportFormat {
  JSON("json"),
  XML("xml"),
  TSV("tsv"),
  BIBTEX("bibtex");

  private final String name;

  ExportFormat(String format) {
    this.name = format;
  }

  public String value() {
    return this.name;
  }

  public static ExportFormat fromValue(String format) {
    for(ExportFormat exportFormat: ExportFormat.values()) {
      if (exportFormat.name.equals(format)) {
        return exportFormat;
      }
    }
    throw new IllegalArgumentException(format);
  }
}
