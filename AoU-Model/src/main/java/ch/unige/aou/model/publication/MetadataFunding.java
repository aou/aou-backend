/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - MetadataFunding.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.Objects;

public class MetadataFunding {
  protected String funder;
  protected String name;
  protected String code;
  protected String acronym;

  public MetadataFunding() {
  }

  public MetadataFunding(String funder, String name, String code, String acronym) {
    this.funder = funder;
    this.name = name;
    this.code = code;
    this.acronym = acronym;
  }

  public String getFunder() {
    return this.funder;
  }

  public void setFunder(String funder) {
    this.funder = funder;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getAcronym() {
    return this.acronym;
  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof MetadataFunding)) {
      return false;
    }
    final MetadataFunding castOther = (MetadataFunding) other;
    return Objects.equals(this.funder, castOther.funder) && Objects.equals(this.name, castOther.name)
            && Objects.equals(this.code, castOther.code) && Objects.equals(this.acronym, castOther.acronym);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.funder, this.name, this.code, this.acronym);
  }
}
