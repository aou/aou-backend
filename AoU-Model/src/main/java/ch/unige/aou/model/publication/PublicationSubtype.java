/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtype.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.ValidationRight;
import ch.unige.aou.rest.ResourceName;

@Entity
public class PublicationSubtype extends AouResourceNormalized {

  @ManyToOne(targetEntity = PublicationType.class)
  @NotNull
  private PublicationType publicationType;

  /**
   * Subtype name (e.g: Article scientifique, Chapitre de livre)
   */
  @Size(min = 1)
  @NotNull
  @Column(unique = true)
  private String name;

  /**
   * Subtype code. Used like a unique id.
   */
  @Size(min = 1)
  @NotNull
  @Column(unique = true, length = AouConstants.DB_SHORT_STRING_LENGTH)
  private String code;

  /**
   * Property allowing to sort subtypes in lists
   */
  @Column(nullable = false)
  private Integer sortValue;

  /**
   * Property allowing to sort subtypes in lists
   */
  @Column(nullable = false)
  private Integer bibliographySortValue;

  @JsonIgnore
  @OneToMany(mappedBy = PublicationSubtypeDocumentFileType.PATH_TO_PUBLICATION_SUBTYPE, cascade = CascadeType.ALL)
  private List<PublicationSubtypeDocumentFileType> publicationSubtypeDocumentFileTypes;

  @JsonIgnore
  @OneToMany(mappedBy = PublicationSubtypeContributorRole.PATH_TO_PUBLICATION_SUBTYPE, cascade = CascadeType.ALL)
  private List<PublicationSubtypeContributorRole> publicationSubtypeContributorRoles;

  @JsonIgnore
  @OneToMany(mappedBy = ValidationRight.PATH_TO_PUBLICATION_SUBTYPE, cascade = CascadeType.ALL, orphanRemoval = true)
  protected final List<ValidationRight> validationRights = new ArrayList<>();

  @ElementCollection
  @Column(name = AouConstants.DB_PUBLICATION_SUBTYPE_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_PUBLICATION_SUBTYPE_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_PUBLICATION_SUBTYPE_ID, AouConstants.DB_LANGUAGE_ID }))
  private List<Label> descriptions = new ArrayList<>();

  @ElementCollection
  @Column(name = AouConstants.DB_PUBLICATION_SUBTYPE_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_PUBLICATION_SUBTYPE_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_PUBLICATION_SUBTYPE_ID, AouConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  /****************************************************************/

  public PublicationType getPublicationType() {
    return this.publicationType;
  }

  public void setPublicationType(PublicationType publicationType) {
    this.publicationType = publicationType;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getSortValue() {
    return this.sortValue;
  }

  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public Integer getBibliographySortValue() {
    return this.bibliographySortValue;
  }

  public void setBibliographySortValue(Integer bibliographySortValue) {
    this.bibliographySortValue = bibliographySortValue;
  }

  public List<PublicationSubtypeDocumentFileType> getPublicationSubtypeDocumentFileTypes() {
    return this.publicationSubtypeDocumentFileTypes;
  }

  public void setPublicationSubtypeDocumentFileTypes(
          List<PublicationSubtypeDocumentFileType> publicationSubtypeDocumentFileTypes) {
    this.publicationSubtypeDocumentFileTypes = publicationSubtypeDocumentFileTypes;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }

  public boolean addPublicationSubtypeDocumentFileType(PublicationSubtypeDocumentFileType publicationSubtypeDocumentFileType) {
    publicationSubtypeDocumentFileType.setPublicationSubtype(this);
    return this.publicationSubtypeDocumentFileTypes.add(publicationSubtypeDocumentFileType);
  }

  public List<PublicationSubtypeContributorRole> getPublicationSubtypeContributorRoles() {
    return this.publicationSubtypeContributorRoles;
  }

  public void setPublicationSubtypeContributorRoles(List<PublicationSubtypeContributorRole> publicationSubtypeContributorRoles) {
    this.publicationSubtypeContributorRoles = publicationSubtypeContributorRoles;
  }

  public boolean addPublicationSubtypeContributorRole(PublicationSubtypeContributorRole publicationSubtypeContributorRole) {
    publicationSubtypeContributorRole.setPublicationSubtype(this);
    return this.publicationSubtypeContributorRoles.add(publicationSubtypeContributorRole);
  }

  public List<Label> getDescriptions() {
    return this.descriptions;
  }

  public void setDescriptions(List<Label> descriptions) {
    this.descriptions = descriptions;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PUBLICATION_SUB_SUBTYPES).withRel(ResourceName.PUBLICATION_SUB_SUBTYPES));
    }
  }
}
