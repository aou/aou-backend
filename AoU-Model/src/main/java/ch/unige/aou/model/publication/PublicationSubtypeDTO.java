/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.LabelDTO;

public class PublicationSubtypeDTO implements Serializable {

  private String resId;
  private String name;
  private String code;
  private Integer sortValue;
  private Integer bibliographySortValue;
  private PublicationTypeDTO type;
  private List<LabelDTO> labels = new ArrayList<>();

  public PublicationSubtypeDTO() {
  }

  public PublicationSubtypeDTO(PublicationSubtype subtype) {
    if (subtype != null) {
      this.setResId(subtype.getResId());
      this.setCode(subtype.getCode());
      this.setName(subtype.getName());
      this.setSortValue(subtype.getSortValue());
      this.setBibliographySortValue(subtype.getBibliographySortValue());
      this.setType(new PublicationTypeDTO(subtype.getPublicationType()));
      for (Label label : subtype.getLabels()) {
        LabelDTO labelDTO = new LabelDTO();
        labelDTO.setText(label.getText());
        labelDTO.setLanguageCode(label.getLanguageCode());
        this.labels.add(labelDTO);
      }
    }
  }

  public String getResId() {
    return this.resId;
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getSortValue() {
    return this.sortValue;
  }

  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public Integer getBibliographySortValue() {
    return this.bibliographySortValue;
  }

  public void setBibliographySortValue(Integer bibliographySortValue) {
    this.bibliographySortValue = bibliographySortValue;
  }

  public PublicationTypeDTO getType() {
    return this.type;
  }

  public void setType(PublicationTypeDTO publicationType) {
    this.type = publicationType;
  }

  public List<LabelDTO> getLabels() {
    return this.labels;
  }

  public void setLabels(List<LabelDTO> labels) {
    this.labels = labels;
  }
}
