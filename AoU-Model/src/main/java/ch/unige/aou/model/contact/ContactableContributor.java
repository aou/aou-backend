/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContactableContributor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.contact;

import java.io.Serializable;

import ch.unige.solidify.util.StringTool;

public class ContactableContributor implements Serializable {
  private String cnIndividu;
  private String firstName;
  private String lastName;

  public ContactableContributor() {
  }

  public ContactableContributor(String cnIndividu, String firstName, String lastName) {
    this.cnIndividu = cnIndividu;
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getCnIndividu() {
    return this.cnIndividu;
  }

  public void setCnIndividu(String cnIndividu) {
    this.cnIndividu = cnIndividu;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFullName() {
    String fullName = this.lastName;
    if (!StringTool.isNullOrEmpty(this.firstName)) {
      fullName = fullName + ", " + this.firstName;
    }
    return fullName;
  }
}
