/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Contributor.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouSearchableResourceNormalized;
import ch.unige.aou.specification.ContributorAdvancedSearchSpecification;

@Entity
public class Contributor extends AouSearchableResourceNormalized<Contributor> {

  private String firstName;

  @NotNull
  @Size(min = 1)
  private String lastName;

  @Size(min = 1)
  @Column(unique = true)
  private String cnIndividu;

  @Size(min = 1)
  @Pattern(regexp = "|[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}|[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}X", message = "The ORCID should be of the form 1234-1234-1234-1234 or 1234-1234-1234-123X")
  @Column(unique = true)
  private String orcid;

  @JsonIgnore
  @ManyToMany(mappedBy = "contributors")
  private Set<Publication> publications = new HashSet<>();

  @OneToMany(mappedBy = "contributor", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<ContributorOtherName> otherNames = new ArrayList<>();

  @JsonIgnore
  @NotNull
  @Column(unique = true)
  private String uniqueKey;

  /****************************************************************/

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public String getCnIndividu() {
    return this.cnIndividu;
  }

  public String getOrcid() {
    return this.orcid;
  }

  public Set<Publication> getPublications() {
    return this.publications;
  }

  public String getFullName() {
    String fullName = this.lastName;
    if (!StringTool.isNullOrEmpty(this.firstName)) {
      fullName = fullName + ", " + this.firstName;
    }
    return fullName;
  }

  public List<ContributorOtherName> getOtherNames() {
    return this.otherNames;
  }

  public String getDisplayName() {
    StringBuilder sb = new StringBuilder(this.getFullName());
    if (!this.getOtherNames().isEmpty()) {
      sb.append(" (");
      sb.append(this.otherNames.stream().map(ContributorOtherName::getFullName)
              .collect(Collectors.joining(AouConstants.CONTRIBUTOR_OTHER_NAMES_SEPARATOR)));
      sb.append(")");
    }
    return sb.toString();
  }

  /****************************************************************/

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setCnIndividu(String cnIndividu) {
    this.cnIndividu = cnIndividu;
  }

  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public void setPublications(Set<Publication> publications) {
    this.publications = publications;
  }

  public void setUniqueKey(String uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

  /****************************************************************/

  @Override
  public Specification<Contributor> getSearchSpecification(SearchCriteria criteria) {
    return new ContributorAdvancedSearchSpecification(criteria);
  }

  @Override
  @Transient
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    final CacheMessage cacheMessage = super.getCacheMessage();
    cacheMessage.addOtherProperty("cnIndividu", this.cnIndividu);
    cacheMessage.addOtherProperty("orcid", this.orcid);
    return cacheMessage;
  }
}
