/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AouStdDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import ch.unige.solidify.util.StringTool;

public abstract class AouStdDeserializer<T> extends StdDeserializer<T> {

  public AouStdDeserializer() {
    // empty constructor required to run in annotation
    this(null);
  }

  public AouStdDeserializer(Class<?> vc) {
    super(vc);
  }

  protected boolean nodeHasTextValue(JsonNode node) {
    return node != null && !node.isNull() && !StringTool.isNullOrEmpty(node.textValue());
  }

  protected boolean nodeHasValue(JsonNode node) {
    return node != null && !node.isNull() && !StringTool.isNullOrEmpty(node.asText());
  }
}
