/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - User.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.security;

import java.time.OffsetDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthUserDto;
import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.model.SolidifyApplicationRole;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.ModuleName;

@Entity
public class User extends ResourceNormalized implements UserInfo {
  private static final int LAST_LOGIN_IP_ADDRESS_SIZE = 255;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = 3)
  private OffsetDateTime lastLoginTime;

  @Size(max = LAST_LOGIN_IP_ADDRESS_SIZE)
  private String lastLoginIpAddress;

  @NotNull
  @ManyToOne
  private SolidifyApplicationRole applicationRole;

  @NotNull
  private Boolean disabled = Boolean.FALSE;

  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String email;

  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String externalUid;

  @NotNull
  @Size(min = 1)
  private String firstName;

  @NotNull
  @Size(min = 1, max = 512)
  private String homeOrganization;

  @NotNull
  @Size(min = 1)
  private String lastName;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_PERSON_ID, referencedColumnName = AouConstants.DB_RES_ID)
  private Person person;

  public User() {
  }

  public User(AuthUserDto authUserDto) {
    super();
    this.setResId(authUserDto.getResId());
    this.setExternalUid(authUserDto.getExternalUid());
    this.setEmail(authUserDto.getEmail());
    this.setApplicationRole(new SolidifyApplicationRole(authUserDto.getApplicationRole()));
    this.setFirstName(authUserDto.getFirstName());
    this.setLastName(authUserDto.getLastName());
    this.setHomeOrganization(authUserDto.getHomeOrganization());
    this.getCreation().setWhen(authUserDto.getCreationWhen());
    this.getCreation().setWho(authUserDto.getCreationWho());
    this.getLastUpdate().setWhen(authUserDto.getLastUpdateWhen());
    this.getLastUpdate().setWho(authUserDto.getLastUpdateWho());
    if (authUserDto.getLastLogin() != null) {
      this.setLastLoginIpAddress(authUserDto.getLastLogin().getLoginIpAddress());
      this.setLastLoginTime(authUserDto.getLastLogin().getLoginTime());
    }
  }

  public void setAuthUserDto(AuthUserDto authUserDto) {
    // Application Role
    if (authUserDto.getApplicationRole() != null) {
      this.setApplicationRole(new SolidifyApplicationRole(authUserDto.getApplicationRole()));
    }

    // User name
    if (authUserDto.getLastUpdateWhen().isAfter(this.getLastUpdate().getWhen())) {
      // Update name only if properties coming from auth server are more recent than the ones is project
      this.setFirstName(authUserDto.getFirstName());
      this.setLastName(authUserDto.getLastName());
    }

    // Person name
    if (this.person != null && authUserDto.getLastUpdateWhen().isAfter(this.person.getLastUpdate().getWhen())) {
      // Update name only if properties coming from auth server are more recent than the ones is project
      this.person.setFirstName(this.getFirstName());
      this.person.setLastName(this.getLastName());
    }

    // Last login
    if (authUserDto.getLastLogin() != null) {
      if (authUserDto.getLastLogin().getLoginIpAddress() != null) {
        this.setLastLoginIpAddress(authUserDto.getLastLogin().getLoginIpAddress());
      }
      if (authUserDto.getLastLogin().getLoginTime() != null) {
        this.setLastLoginTime(authUserDto.getLastLogin().getLoginTime());
      }
    }
  }

  @Override
  public ApplicationRole getApplicationRole() {
    return this.applicationRole;
  }

  @Override
  @Transient
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    final CacheMessage cacheMessage = super.getCacheMessage();
    cacheMessage.addOtherProperty("externalUid", this.getExternalUid());
    return cacheMessage;
  }

  @Override
  public String getEmail() {
    return this.email;
  }

  @Override
  public String getExternalUid() {
    return this.externalUid;
  }

  @Override
  public String getFirstName() {
    return this.firstName;
  }

  @Override
  public String getFullName() {
    if (this.getPerson() == null) {
      return this.getLastName() + ", " + this.getFirstName();
    }
    return this.getPerson().getFullName();
  }

  public String getHomeOrganization() {
    return this.homeOrganization;
  }

  @Override
  public String getLastName() {
    return this.lastName;
  }

  @Override
  public Person getPerson() {
    return this.person;
  }

  public String getLastLoginIpAddress() {
    return this.lastLoginIpAddress;
  }

  public OffsetDateTime getLastLoginTime() {
    return this.lastLoginTime;
  }

  @Transient
  @JsonIgnore
  public String getUniqueId() {
    return this.externalUid;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public boolean isEnabled() {
    return !this.disabled;
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public void setApplicationRole(SolidifyApplicationRole applicationRole) {
    this.applicationRole = applicationRole;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setExternalUid(String externalUid) {
    this.externalUid = externalUid;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setHomeOrganization(String homeOrganization) {
    this.homeOrganization = homeOrganization;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public void setLastLoginTime(OffsetDateTime lastLoginTime) {
    this.lastLoginTime = lastLoginTime;
  }

  public void setLastLoginIpAddress(String lastLoginIpAddress) {
    this.lastLoginIpAddress = StringTool.truncate(lastLoginIpAddress, LAST_LOGIN_IP_ADDRESS_SIZE);
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("User [aai=").append(this.externalUid).append(", email=").append(this.email).append(", application role")
            .append(this.applicationRole).append("]");
    return builder.toString();
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof User)) {
      return false;
    }
    if (!super.equals(other)) {
      return false;
    }
    final User castOther = (User) other;
    return Objects.equals(this.applicationRole, castOther.applicationRole) && Objects.equals(this.email, castOther.email)
            && Objects.equals(this.externalUid, castOther.externalUid) && Objects.equals(this.firstName, castOther.firstName)
            && Objects.equals(this.homeOrganization, castOther.homeOrganization) && Objects.equals(this.lastName, castOther.lastName)
            && Objects.equals(this.person, castOther.person);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.applicationRole, this.email, this.externalUid, this.firstName,
            this.homeOrganization, this.lastName, this.person);
  }

}
