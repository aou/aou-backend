/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeDocumentFileTypeDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.unige.aou.model.settings.Label;

public class PublicationSubtypeDocumentFileTypeDTO implements Serializable {

  private DocumentFileType.FileTypeLevel level;
  private int sortValue;
  private String value;
  private String documentFileTypeId;
  private String publicationSubtypeId;
  private List<Label> labels = new ArrayList<>();

  public PublicationSubtypeDocumentFileTypeDTO() {
  }

  public PublicationSubtypeDocumentFileTypeDTO(PublicationSubtypeDocumentFileType subtypeDocFile) {
    this.level = subtypeDocFile.getLevel();
    this.sortValue = subtypeDocFile.getSortValue();

    if (subtypeDocFile.getDocumentFileType() != null) {
      this.value = subtypeDocFile.getDocumentFileType().getValue();
      this.labels.addAll(subtypeDocFile.getDocumentFileType().getLabels());
    }

    if (subtypeDocFile.getDocumentFileType() != null) {
      this.documentFileTypeId = subtypeDocFile.getDocumentFileType().getResId();
    }

    if (subtypeDocFile.getPublicationSubtype() != null) {
      this.publicationSubtypeId = subtypeDocFile.getPublicationSubtype().getResId();
    }
  }

  public DocumentFileType.FileTypeLevel getLevel() {
    return this.level;
  }

  public String getValue() {
    return this.value;
  }

  public int getSortValue() {
    return this.sortValue;
  }

  public String getPublicationSubtypeId() {
    return this.publicationSubtypeId;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  public String getDocumentFileTypeId() {
    return this.documentFileTypeId;
  }
}
