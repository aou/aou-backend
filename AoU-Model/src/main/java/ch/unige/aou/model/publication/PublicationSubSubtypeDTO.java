/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubSubtypeDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.io.Serializable;

public class PublicationSubSubtypeDTO implements Serializable {
  private String resId;
  private String name;
  private PublicationSubtypeDTO subtype;
  private Integer bibliographySortValue;

  public PublicationSubSubtypeDTO() {
  }

  public PublicationSubSubtypeDTO(PublicationSubSubtype subSubtype) {
    if (subSubtype != null) {
      this.setResId(subSubtype.getResId());
      this.setName(subSubtype.getName());
      this.setSubtype(new PublicationSubtypeDTO(subSubtype.getPublicationSubtype()));
    }
  }

  public String getResId() {
    return this.resId;
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public PublicationSubtypeDTO getSubtype() {
    return this.subtype;
  }

  public void setSubtype(PublicationSubtypeDTO subtype) {
    this.subtype = subtype;
  }

  public Integer getBibliographySortValue() {
    return this.bibliographySortValue;
  }

  public void setBibliographySortValue(Integer bibliographySortValue) {
    this.bibliographySortValue = bibliographySortValue;
  }
}
