/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - FilesDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_2.deserializer;

import java.io.IOException;
import java.math.BigInteger;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v2_2.AccessLevel;
import ch.unige.aou.model.xml.deposit.v2_2.File;
import ch.unige.aou.model.xml.deposit.v2_2.FileType;
import ch.unige.aou.model.xml.deposit.v2_2.Files;

public class FilesDeserializer extends AouStdDeserializer<Files> {

  private static final long serialVersionUID = 659865L;

  @Override
  public Files deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    if (jsonNode instanceof ArrayNode) {
      if (jsonNode.isEmpty()) {
        return null;
      }

      Files files = new Files();
      for (JsonNode fileNode : jsonNode) {
        this.addFile(fileNode, files);
      }
      return files;

    } else {
      throw new SolidifyRuntimeException("files is not an array");
    }
  }

  private void addFile(JsonNode fileNode, Files files) {
    File file = new File();

    JsonNode typeNode = fileNode.get("type");
    if (this.nodeHasTextValue(typeNode)) {
      file.setType(FileType.fromValue(typeNode.textValue()));
    }

    JsonNode accessLevelNode = fileNode.get("accessLevel");
    if (this.nodeHasTextValue(accessLevelNode)) {
      file.setAccessLevel(AccessLevel.fromValue(accessLevelNode.textValue()));
    }

    JsonNode nameNode = fileNode.get("name");
    if (this.nodeHasTextValue(nameNode)) {
      file.setName(nameNode.textValue());
    }

    JsonNode sizeNode = fileNode.get("size");
    if (this.nodeHasTextValue(sizeNode)) {
      file.setSize(new BigInteger(sizeNode.textValue()));
    }

    JsonNode mimeTypeNode = fileNode.get("mimetype");
    if (this.nodeHasTextValue(mimeTypeNode)) {
      file.setMimetype(mimeTypeNode.textValue());
    }

    JsonNode licenseNode = fileNode.get("license");
    if (this.nodeHasTextValue(licenseNode)) {
      file.setLicense(licenseNode.textValue());
    }

    files.getFile().add(file);
  }
}
