/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ModuleList.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model;

import org.springframework.hateoas.RepresentationModel;

public class ModuleList extends RepresentationModel<ModuleList> {
  private final String authorization;
  private final String admin;
  private final String access;
  private final String index;
  private final String oaiInfo;

  public ModuleList(String authUrl, String adminUrl, String accessUrl, String index, String oaiInfo) {
    // Modules with unique instance
    this.authorization = authUrl;
    this.admin = adminUrl;
    this.access = accessUrl;
    this.index = index;
    this.oaiInfo = oaiInfo;
  }

  public String getAuthorization() {
    return this.authorization;
  }

  public String getAdmin() {
    return this.admin;
  }

  public String getAccess() {
    return this.access;
  }

  public String getIndex() {
    return index;
  }

  public String getOaiInfo() {
    return this.oaiInfo;
  }
}
