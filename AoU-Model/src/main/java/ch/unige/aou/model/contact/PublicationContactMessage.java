/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationContactMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.contact;

import java.io.Serializable;

public class PublicationContactMessage implements Serializable {
  private String senderName;
  private String senderEmail;
  private String messageContent;
  private String recipientCnIndividu;

  public PublicationContactMessage() {
  }

  public PublicationContactMessage(String senderName, String senderEmail, String messageContent) {
    this.senderName = senderName;
    this.senderEmail = senderEmail;
    this.messageContent = messageContent;
  }

  public String getSenderName() {
    return this.senderName;
  }

  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }

  public String getSenderEmail() {
    return this.senderEmail;
  }

  public void setSenderEmail(String senderEmail) {
    this.senderEmail = senderEmail;
  }

  public String getMessageContent() {
    return this.messageContent;
  }

  public void setMessageContent(String messageContent) {
    this.messageContent = messageContent;
  }

  public String getRecipientCnIndividu() {
    return this.recipientCnIndividu;
  }

  public void setRecipientCnIndividu(String recipientCnIndividu) {
    this.recipientCnIndividu = recipientCnIndividu;
  }
}
