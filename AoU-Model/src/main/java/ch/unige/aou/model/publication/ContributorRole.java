/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContributorRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.settings.Label;

@Entity
public class ContributorRole extends AouResourceNormalized {

  @JsonIgnore
  @OneToMany(mappedBy = PublicationSubtypeContributorRole.PATH_TO_CONTRIBUTOR_ROLE)
  private List<PublicationSubtypeContributorRole> publicationSubtypeContributorRoles;

  @Size(min = 1)
  @NotNull
  @Column(unique = true)
  private String value;

  @ElementCollection
  @Column(name = AouConstants.DB_CONTRIBUTOR_ROLE_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_CONTRIBUTOR_ROLE_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_CONTRIBUTOR_ROLE_ID, AouConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  /****************************************************************/

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }

  public List<PublicationSubtypeContributorRole> getPublicationSubtypeContributorRoles() {
    return publicationSubtypeContributorRoles;
  }

  public void setPublicationSubtypeContributorRoles(List<PublicationSubtypeContributorRole> contributorRoles) {
    this.publicationSubtypeContributorRoles = contributorRoles;
  }
}
