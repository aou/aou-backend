/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - BibTexEscaper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.access;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BibTexEscaper {

  private static final Map<Character, String> ESCAPE_MAP = new HashMap<>();

  static {
    ESCAPE_MAP.put('*', "\\*");
    ESCAPE_MAP.put('#', "\\#");
    ESCAPE_MAP.put('@', "\\@");
    ESCAPE_MAP.put('!', "\\!");
    ESCAPE_MAP.put('&', "\\&"); // Escaping the & character
  }

  /**
   * Needed to escape certains characters and uppercase words only for title property
   * @param input
   * @param isTitleProperty
   * @return
   */
  public static String escapeForBibTeX(String input, Boolean isTitleProperty) {
    StringBuilder strBuilder = new StringBuilder();
    if (isTitleProperty) {
      if (containsUpperCaseCharacter(input)) {
        strBuilder.append(escapeUppercaseWord(input));
      } else {
        strBuilder.append(escapeSpecialCharacters(input)).append(" ");
      }
    } else {
      String[] words = input.split("\\s+");
      for (String word : words) {
        strBuilder.append(escapeSpecialCharacters(word)).append(" ");
      }
    }
    return strBuilder.toString().trim();
  }

  private static boolean containsUpperCaseCharacter(String word) {
    // Check if the word is all uppercase and has more than one character
    Pattern pattern = Pattern.compile("(?=.*[A-Z].*)^.{1,}[^A-Z]*[A-Z].*");

    Matcher matcher = pattern.matcher(word);
    return word.length() > 1 && matcher.matches();
  }

  private static boolean hasAnyUpperCaseCharacter(String word) {
    return word.chars().anyMatch(Character::isUpperCase);
  }

  private static String escapeUppercaseWord(String input) {
    // Method to escape between {} any uppercase character that is not the start of the title.
    StringBuilder stringBuilder = new StringBuilder();
    //Separate each input
    String[] parts = input.split("[\\s,;.:?!\"/']+");
    int lengthFirstWord = parts[0].length();
    for (int i = 0; i < input.length(); i++) {
      // First word should not be escape
      if (Character.isUpperCase(input.charAt(i)) && i > lengthFirstWord) {
        stringBuilder.append("{" + input.charAt(i) + "}");
      } else {
        stringBuilder.append(escapeSpecialCharacters(String.valueOf(input.charAt(i))));
      }
    }
    return stringBuilder.toString();
  }


  private static String escapeSpecialCharacters(String input) {
    StringBuilder escapedText = new StringBuilder();

    for (char c : input.toCharArray()) {
      String escapedChar = ESCAPE_MAP.get(c);
      if (escapedChar != null) {
        escapedText.append(escapedChar);
      } else {
        escapedText.append(c);
      }
    }
    return escapedText.toString();
  }

}

