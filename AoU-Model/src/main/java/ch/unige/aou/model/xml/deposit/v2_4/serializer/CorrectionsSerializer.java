/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - CorrectionsSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_4.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.xml.deposit.v2_4.Correction;
import ch.unige.aou.model.xml.deposit.v2_4.Corrections;

public class CorrectionsSerializer extends StdSerializer<Corrections> {
  public CorrectionsSerializer() {
    this(null);
  }

  public CorrectionsSerializer(Class<Corrections> t) {
    super(t);
  }

  @Override
  public void serialize(Corrections value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
    jgen.writeStartArray();
    for (Correction correction : value.getCorrection()) {
      jgen.writeStartObject();
      jgen.writeStringField("note", correction.getNote());
      if (!StringTool.isNullOrEmpty(correction.getDoi())) {
        jgen.writeObjectField("doi", correction.getDoi());
      }
      if (!StringTool.isNullOrEmpty(correction.getPmid())) {
        jgen.writeObjectField("pmid", correction.getPmid());
      }
      jgen.writeEndObject();
    }
    jgen.writeEndArray();
  }
}
