/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DocumentFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.net.URI;
import java.time.LocalDate;
import java.util.Objects;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.listener.DocumentFileEntityListener;
import ch.unige.aou.listener.HistoryListener;
import ch.unige.aou.model.AouSearchableResourceNormalized;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.specification.DocumentFileSearchSpecification;

@Entity
@EntityListeners({ HistoryListener.class, DocumentFileEntityListener.class })
public class DocumentFile extends AouSearchableResourceNormalized<DocumentFile> implements Sortable {

  //@formatter:off
  public enum DocumentFileStatus {
    RECEIVED,   // entity has been created (or reset to RECEIVED to restart processing)
    TO_PROCESS, // to download or to copy to working folder
    PROCESSED,  // file is ready. It can be checked (virus check, checksums computed, ...)
    READY,      // the file processing is over for the file.
    CLEANING,
    DUPLICATE, // if there are two files that are duplicate
    IN_ERROR,   // an error has occurred during file processing
    TO_BE_CONFIRMED,   // cannot read the full text of pdf document for multiple reason
    CONFIRMED, // when user confirm the quality control of the document file
    IMPOSSIBLE_TO_DOWNLOAD // when the url provided for the document file is not reachable or invalid
  }

  public enum AccessLevel {
    PUBLIC(10, "Public"),     // open to the world
    RESTRICTED(20, "Restricted"), // open to authenticated users
    PRIVATE(30, "Private");    // open to publication's contributors

    private final int level;
    private final String metadataValue;

    AccessLevel(int level, String metadataValue) {
      this.level = level;
      this.metadataValue = metadataValue;
    }

    public int value() {
      return this.level;
    }

    public String metadataValue() {
      return this.metadataValue;
    }

    public static AccessLevel fromMetadataValue(String metadataValue) {
      for (AccessLevel accessLevel : AccessLevel.values()) {
        if (accessLevel.metadataValue.equals(metadataValue)) {
          return accessLevel;
        }
      }
      throw new IllegalArgumentException(metadataValue);
    }
  }
  //@formatter:on

  @ManyToOne
  @NotNull
  private Publication publication;

  @ManyToOne
  @NotNull
  private DocumentFileType documentFileType;

  @ManyToOne
  private License license;

  @Column(nullable = false)
  private Boolean notSureForLicense;

  @NotNull
  @Column(length = AouConstants.DB_BIG_STRING_LENGTH)
  private URI sourceData;

  @Column(length = AouConstants.DB_BIG_STRING_LENGTH)
  private URI finalData;

  private String fileName;

  private Long fileSize;

  private String mimetype;

  private String label;

  private Boolean isImported;

  @JsonIgnore
  private String checksum;

  @Enumerated(EnumType.STRING)
  private DocumentFileStatus status;

  @Size(max = AouConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  /**
   * Final access level
   */
  @Enumerated(EnumType.STRING)
  @NotNull
  private AccessLevel accessLevel;

  /**
   * Access level during an eventual embargo
   */
  @Enumerated(EnumType.STRING)
  private AccessLevel embargoAccessLevel;

  /**
   * End date of an eventual embargo
   */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate embargoEndDate;

  @Column(nullable = false)
  private Integer sortValue;

  /****************************************************************/

  @Override
  public void init() {
    if (this.status == null) {
      this.setStatus(DocumentFileStatus.RECEIVED);
    }

    if (this.isImported == null) {
      this.setIsImported(false);
    }

    if (this.notSureForLicense == null) {
      this.setNotSureForLicense(false);
    }
  }

  @Override
  public Specification<DocumentFile> getSearchSpecification(SearchCriteria criteria) {
    return new DocumentFileSearchSpecification(criteria);
  }

  /****************************************************************/

  public Publication getPublication() {
    return this.publication;
  }

  public DocumentFileType getDocumentFileType() {
    return this.documentFileType;
  }

  public License getLicense() {
    return this.license;
  }

  public Long getFileSize() {
    return this.fileSize;
  }

  public URI getFinalData() {
    return this.finalData;
  }

  public URI getSourceData() {
    return this.sourceData;
  }

  public DocumentFileStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public String getFileName() {
    return this.fileName;
  }

  public String getMimetype() {
    return this.mimetype;
  }

  public String getLabel() {
    return this.label;
  }

  public AccessLevel getAccessLevel() {
    return this.accessLevel;
  }

  public AccessLevel getEmbargoAccessLevel() {
    return this.embargoAccessLevel;
  }

  public LocalDate getEmbargoEndDate() {
    return this.embargoEndDate;
  }

  public String getChecksum() {
    return this.checksum;
  }

  public Boolean getIsImported() {
    return this.isImported;
  }

  public Boolean getNotSureForLicense() {
    return this.notSureForLicense;
  }

  @Override
  public Integer getSortValue() {
    return this.sortValue;
  }

  /****************************************************************/

  public void setPublication(Publication publication) {
    this.publication = publication;
  }

  public void setDocumentFileType(DocumentFileType documentFileType) {
    this.documentFileType = documentFileType;
  }

  public void setLicense(License license) {
    this.license = license;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public void setFinalData(URI finalData) {
    this.finalData = finalData;
  }

  public void setSourceData(URI sourceData) {
    this.sourceData = sourceData;
  }

  public void setStatus(DocumentFileStatus status) {
    this.status = status;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public void setMimetype(String mimetype) {
    this.mimetype = mimetype;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public void setAccessLevel(AccessLevel accessLevel) {
    this.accessLevel = accessLevel;
  }

  public void setEmbargoAccessLevel(AccessLevel embargoAccessLevel) {
    this.embargoAccessLevel = embargoAccessLevel;
  }

  public void setEmbargoEndDate(LocalDate embargoEndDate) {
    this.embargoEndDate = embargoEndDate;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public void setIsImported(Boolean isImported) {
    this.isImported = isImported;
  }

  public void setNotSureForLicense(Boolean notSureForLicense) {
    this.notSureForLicense = notSureForLicense;
  }

  @Override
  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  /****************************************************************/

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public AccessLevel getCurrentAccessLevel() {

    if (this.embargoAccessLevel != null) {
      if (this.embargoEndDate != null) {
        if (this.embargoEndDate.isAfter(LocalDate.now())) {
          // file is still under embargo
          return this.embargoAccessLevel;
        }
      } else {
        throw new SolidifyRuntimeException(
                "DocumentFile " + this.getResId() + " has an embargo access level but no embargo end date is defined");
      }
    }

    return this.accessLevel;
  }

  public void setStatusWithMessage(DocumentFileStatus status, String statusMessage) {
    this.setStatus(status);
    this.setStatusMessage(statusMessage);
  }

  @Override
  public void addLinks(final WebMvcLinkBuilder linkBuilder, final boolean mainRes, final boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
    }
  }

  @Override
  public URI getDownloadUri() {
    if (!this.canBeDownloaded(this.getStatus())) {
      throw new SolidifyRuntimeException("Document file not available");
    }
    if (this.getFinalData() == null) {
      throw new SolidifyRuntimeException("Document file not accessible");
    }
    return this.getFinalData();
  }

  @Override
  public String getContentType() {
    if (!this.canBeDownloaded(this.getStatus())) {
      throw new SolidifyRuntimeException("Document file not available");
    }
    if (this.getMimetype() == null) {
      throw new SolidifyRuntimeException("Document file format not accessible");
    }
    return this.getMimetype();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    DocumentFile that = (DocumentFile) o;
    return Objects.equals(this.publication, that.publication)
            && Objects.equals(this.documentFileType, that.documentFileType)
            && Objects.equals(this.license, that.license)
            && Objects.equals(this.notSureForLicense, that.notSureForLicense)
            && Objects.equals(this.sourceData, that.sourceData)
            && Objects.equals(this.finalData, that.finalData)
            && Objects.equals(this.fileName, that.fileName)
            && Objects.equals(this.fileSize, that.fileSize)
            && Objects.equals(this.mimetype, that.mimetype)
            && Objects.equals(this.label, that.label)
            && Objects.equals(this.isImported, that.isImported)
            && Objects.equals(this.checksum, that.checksum)
            && this.status == that.status
            && Objects.equals(this.statusMessage, that.statusMessage)
            && this.accessLevel == that.accessLevel
            && this.embargoAccessLevel == that.embargoAccessLevel
            && Objects.equals(this.embargoEndDate, that.embargoEndDate)
            && Objects.equals(this.sortValue, that.sortValue);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.publication, this.documentFileType, this.license, this.notSureForLicense, this.sourceData,
            this.finalData, this.fileName, this.fileSize, this.mimetype, this.label, this.isImported, this.checksum, this.status,
            this.statusMessage, this.accessLevel, this.embargoAccessLevel, this.embargoEndDate, this.sortValue);
  }

  private boolean canBeDownloaded(DocumentFileStatus status) {
    return (status == DocumentFileStatus.PROCESSED || status == DocumentFileStatus.READY || status == DocumentFileStatus.TO_BE_CONFIRMED
            || status == DocumentFileStatus.CONFIRMED || status == DocumentFileStatus.DUPLICATE);
  }

  public boolean isStoredOutside() {
    return this.getPublication().getImportSource() != null
            && this.getPublication().getImportSource().equals(Publication.ImportSource.FEDORA3);
  }
}
