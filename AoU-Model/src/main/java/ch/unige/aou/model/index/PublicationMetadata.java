/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationMetadata.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.index;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.util.JSONTool;

import ch.unige.aou.AouConstants;

@JsonDeserialize(using = PublicationMetadataDeserializer.class)
public class PublicationMetadata extends NoSqlResource {

  private String index;
  private Map<String, Object> metadata = new HashMap<>();
  private String type;

  public PublicationMetadata() {
  }

  public PublicationMetadata(IndexType type) {
    this.type = type.toString().toLowerCase();
  }

  public void addLinksForAccess(WebMvcLinkBuilder linkBuilder) {
    super.addLinks(linkBuilder);
  }

  public boolean check() {
    return (this.getResId() != null && this.index != null && this.type != null && !this.metadata.isEmpty());
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof PublicationMetadata)) {
      return false;
    }
    final PublicationMetadata castOther = (PublicationMetadata) other;
    return Objects.equals(this.index, castOther.index) && Objects.equals(this.type, castOther.type)
            && Objects.equals(this.metadata, castOther.metadata);
  }

  public String getIndex() {
    return this.index;
  }

  public Map<String, Object> getMetadata() {
    return this.metadata;
  }

  public Object getMetadata(String... xpath) {
    return this.getMetadata(this.metadata, xpath);
  }

  @JsonIgnore
  public String getMetadataVersion() {
    return (String) this.getMetadata(AouConstants.INDEX_FIELD_METADATA_VERSION);
  }

  @JsonIgnore
  public String getXmlMetadata() {
    return (String) this.getMetadata(AouConstants.INDEX_FIELD_XML);
  }

  public String getType() {
    return this.type;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.index, this.type, this.metadata);
  }

  public void setIndex(String index) {
    this.index = index;
  }

  public void setMetadata(Map<String, Object> metadata) {
    this.metadata = metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = JSONTool.convertJson2Map(metadata);
  }

  @Override
  public String toString() {
    return "ArchiveMetadata [resId=" + this.getResId() + ", index=" + this.index + ", type=" + this.type + ", metadata=" + this.metadata + "]";
  }

  @Override
  public boolean update(NoSqlResource item) {
    // TODO Auto-generated method stub
    return true;
  }

  @SuppressWarnings("unchecked")
  private Object getMetadata(Map<String, Object> map, String... xpath) {
    if (map != null) {
      if (xpath.length == 1) {
        return map.get(xpath[0]);
      }
      return this.getMetadata((Map<String, Object>) map.get(xpath[0]), Arrays.copyOfRange(xpath, 1, xpath.length));
    } else {
      return null;
    }
  }

  private List<Object> getMetadataPropertiesList(String propertyPath) {
    final Object propertiesList = this.getMetadata(propertyPath.split("\\."));
    if (propertiesList != null && propertiesList instanceof ArrayList) {
      return (List<Object>) propertiesList;
    } else if (propertiesList != null) {
      // Only contains one element, that is the case of access_level restricted without embargo and
      // licence
      return Arrays.asList(propertiesList);
    } else {
      return new ArrayList<>();
    }
  }

}
