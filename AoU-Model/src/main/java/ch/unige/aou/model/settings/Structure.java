/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Structure.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouSearchableResourceNormalized;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.specification.StructureSearchSpecification;

@Entity
public class Structure extends AouSearchableResourceNormalized<Structure> implements Sortable {

  public enum StructureStatus {
    ACTIVE,
    OBSOLETE,
    HISTORICAL
  }

  /****************************************************************/

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_PARENT_STRUCTURE_ID)
  private Structure parentStructure;

  @OneToMany(mappedBy = "parentStructure")
  @JsonIgnore
  private List<Structure> childStructures;

  /**
   * Subtype name (e.g: Article scientifique, Chapitre de livre)
   */
  @Size(min = 1)
  @NotNull
  private String name;

  @Enumerated(EnumType.STRING)
  @NotNull
  private StructureStatus status;

  /**
   * Property allowing to sort structures in lists
   */
  @Column(nullable = false)
  private Integer sortValue;

  /**
   * Code used to make a link with UNIGE structures in Oracle database
   */
  @Size(min = 1)
  @NotNull
  @Column(unique = true)
  private String cnStructC;

  /**
   * Code used to make a link with UNIGE structures in Oracle database
   */
  @Size(min = 1)
  @NotNull
  @Column(unique = true)
  @JsonProperty(ResourceName.CSTRUCT)
  private String codeStruct;

  /**
   * Dewey code (https://en.wikipedia.org/wiki/List_of_Dewey_Decimal_classes)
   */
  @Size(min = 3)
  @NotNull
  private String dewey;

  @Column()
  private String acronym;

  /**
   * Eventual date since which the structure is no longer valid
   */
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_FORMAT)
  private LocalDate validUntil;

  private LocalDate openingDate;

  @ManyToMany(mappedBy = "structures")
  @JsonIgnore
  private Set<Person> people;

  @JsonIgnore
  @OneToMany(mappedBy = PublicationStructure.PATH_TO_STRUCTURE, cascade = CascadeType.ALL, orphanRemoval = true)
  private List<PublicationStructure> publicationStructures = new ArrayList<>();

  @JsonIgnore
  @OneToMany(mappedBy = ValidationRight.PATH_TO_STRUCTURE, cascade = CascadeType.ALL, orphanRemoval = true)
  protected final List<ValidationRight> validationRights = new ArrayList<>();

  /****************************************************************/

  public Structure getParentStructure() {
    return this.parentStructure;
  }

  public List<Structure> getChildStructures() {
    return this.childStructures;
  }

  public String getName() {
    return this.name;
  }

  public StructureStatus getStatus() {
    return this.status;
  }

  @Override
  public Integer getSortValue() {
    return this.sortValue;
  }

  @Override
  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public String getCnStructC() {
    return this.cnStructC;
  }

  public String getCodeStruct() {
    return this.codeStruct;
  }

  public String getDewey() {
    return this.dewey;
  }

  public LocalDate getValidUntil() {
    return this.validUntil;
  }

  public List<PublicationStructure> getPublicationStructures() {
    return this.publicationStructures;
  }

  public Set<Person> getPeople() {
    return this.people;
  }

  public String getAcronym() {
    return this.acronym;
  }

  /****************************************************************/

  public void setParentStructure(Structure parentStructure) {
    this.parentStructure = parentStructure;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setStatus(StructureStatus status) {
    this.status = status;
  }

  public void setCnStructC(String cnStructC) {
    this.cnStructC = cnStructC;
  }

  public void setCodeStruct(String codeStruct) {
    this.codeStruct = codeStruct;
  }

  public void setDewey(String dewey) {
    this.dewey = dewey;
  }

  public void setValidUntil(LocalDate validUntil) {
    this.validUntil = validUntil;
  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  private boolean setPublicationStructure(Publication publication, PublicationStructure.LinkType status) {
    final PublicationStructure publicationStructure = new PublicationStructure();
    publicationStructure.setStructure(this);
    publicationStructure.setPublication(publication);
    publicationStructure.setLinkType(status);
    return this.publicationStructures.add(publicationStructure);
  }

  /****************************************************************/

  @Override
  public Specification<Structure> getSearchSpecification(SearchCriteria criteria) {
    return new StructureSearchSpecification(criteria);
  }

  @Override
  public void init() {
    if (this.status == null) {
      this.setStatus(StructureStatus.ACTIVE);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (!super.equals(o)) {
      return false;
    }
    Structure structure = (Structure) o;
    return Objects.equals(this.cnStructC, structure.cnStructC) && Objects.equals(this.codeStruct, structure.codeStruct)
            && Objects.equals(this.people, structure.people);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.cnStructC, this.codeStruct, this.people);
  }

  public boolean addPerson(Person p) {
    final boolean result = this.people.add(p);
    if (!p.getStructures().contains(this)) {
      p.addStructure(this);
    }
    return result;
  }

  public boolean removePerson(Person p) {
    final boolean result = this.people.remove(p);
    if (p.getStructures().contains(this)) {
      p.removeStructure(this);
    }
    return result;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PEOPLE).withRel(ResourceName.PEOPLE));
    }
  }
}
