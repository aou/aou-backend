/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - LabeledLanguage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.UniqueConstraint;

import ch.unige.solidify.model.Language;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;

/**
 * Language representation names based in ISO 639-2
 */
@Entity
public class LabeledLanguage extends AouResourceNormalized {

  @Column(unique = true, nullable = false)
  private String code;

  /**
   * Property allowing to sort languages in lists
   */
  @Column(nullable = false)
  private Integer sortValue;

  @ElementCollection
  @Column(name = AouConstants.DB_LANGUAGE_ENTITY_ID)
  @CollectionTable(name = "labeled_language_labels",
                   joinColumns = { @JoinColumn(name = AouConstants.DB_LANGUAGE_ENTITY_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_LANGUAGE_ENTITY_ID, AouConstants.DB_LANGUAGE_ID }))
  private Set<Label> labels = new HashSet<>();

  /****************************************************************/

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Integer getSortValue() {
    return this.sortValue;
  }

  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public Set<Label> getLabels() {
    return this.labels;
  }

  public void setLabels(Set<Label> labels) {
    this.labels = labels;
  }

  /****************************************************************/

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof Language)) {
      return false;
    }
    final Language castOther = (Language) other;
    return Objects.equals(this.getCode(), castOther.getCode());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getCode());
  }
}
