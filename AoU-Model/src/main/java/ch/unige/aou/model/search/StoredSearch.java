/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - StoredSearch.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.search;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.settings.Person;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { AouConstants.DB_CREATOR_ID, AouConstants.DB_NAME }) })
public class StoredSearch extends AouResourceNormalized {

  @ManyToOne(targetEntity = Person.class)
  @JoinColumn(name = AouConstants.DB_CREATOR_ID, referencedColumnName = AouConstants.DB_RES_ID, nullable = false)
  private Person creator;

  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  @Column(name = AouConstants.DB_NAME)
  private String name;

  @OneToMany(mappedBy = "search", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<StoredSearchCriteria> criteria = new ArrayList<>();

  @NotNull
  private Boolean withRestrictedAccessMasters;

  @NotNull
  private Boolean usedForBibliography;

  /****************************************************************/

  public Person getCreator() {
    return this.creator;
  }

  public void setCreator(Person creator) {
    this.creator = creator;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<StoredSearchCriteria> getCriteria() {
    return this.criteria;
  }

  public Boolean getWithRestrictedAccessMasters() {
    return this.withRestrictedAccessMasters;
  }

  public void setWithRestrictedAccessMasters(Boolean withRestrictedAccessMasters) {
    this.withRestrictedAccessMasters = withRestrictedAccessMasters;
  }

  public Boolean getUsedForBibliography() {
    return usedForBibliography;
  }

  public void setUsedForBibliography(Boolean usedForBibliography) {
    this.usedForBibliography = usedForBibliography;
  }

  /****************************************************************/

  @Override
  public void init() {
    if (this.withRestrictedAccessMasters == null) {
      this.withRestrictedAccessMasters = false;
    }
    if (this.usedForBibliography == null) {
      this.usedForBibliography = false;
    }
  }
}
