/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeDocumentFileTypePkId.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.io.Serializable;
import java.util.Objects;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import ch.unige.aou.AouConstants;

@Embeddable
public class PublicationSubtypeDocumentFileTypePkId implements Serializable {
  private static final long serialVersionUID = 20860598719857L;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_PUBLICATION_SUBTYPE_ID)
  private PublicationSubtype publicationSubtype;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_DOCUMENT_FILE_TYPE_ID)
  private DocumentFileType documentFileType;

  public PublicationSubtype getPublicationSubtype() {
    return this.publicationSubtype;
  }

  public void setPublicationSubtype(PublicationSubtype publicationSubtype) {
    this.publicationSubtype = publicationSubtype;
  }

  public DocumentFileType getDocumentFileType() {
    return this.documentFileType;
  }

  public void setDocumentFileType(DocumentFileType documentFileType) {
    this.documentFileType = documentFileType;
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }

    if (!(other instanceof PublicationSubtypeDocumentFileTypePkId)) {
      return false;
    }

    final PublicationSubtypeDocumentFileTypePkId castOther = (PublicationSubtypeDocumentFileTypePkId) other;
    return Objects.equals(this.getPublicationSubtype(), castOther.getPublicationSubtype()) && Objects
            .equals(this.getDocumentFileType(), castOther.getDocumentFileType());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getPublicationSubtype(), this.getDocumentFileType());
  }
}
