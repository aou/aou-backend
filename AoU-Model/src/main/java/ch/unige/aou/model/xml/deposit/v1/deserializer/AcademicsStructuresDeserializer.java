/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AcademicsStructuresDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v1.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v1.AcademicStructure;
import ch.unige.aou.model.xml.deposit.v1.AcademicStructures;

public class AcademicsStructuresDeserializer extends AouStdDeserializer<AcademicStructures> {

  private static final long serialVersionUID = 659865L;

  @Override
  public AcademicStructures deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    if (jsonNode instanceof ArrayNode) {
      if (jsonNode.isEmpty()) {
        return null;
      }

      AcademicStructures academicStructures = new AcademicStructures();
      for (JsonNode collectionNode : jsonNode) {
        AcademicStructure structure = new AcademicStructure();

        JsonNode nameNode = collectionNode.get("name");
        JsonNode codeNode = collectionNode.get("code");
        JsonNode oldCode = collectionNode.get("oldCode");

        //if list contain one element and all properties of this only element are empty we ignore the list
        if (jsonNode.size() == 1 && !this.nodeHasValue(nameNode) && !this.nodeHasValue(codeNode) && !this.nodeHasValue(oldCode)) {
          return null;
        }

        if (this.nodeHasTextValue(nameNode)) {
          structure.setName(nameNode.textValue());
        }

        if (this.nodeHasTextValue(codeNode)) {
          structure.setCode(codeNode.textValue());
        }

        if (this.nodeHasTextValue(oldCode)) {
          structure.setOldCode(oldCode.textValue());
        }

        academicStructures.getAcademicStructure().add(structure);

      }
      return academicStructures;

    } else {
      throw new SolidifyRuntimeException("academicStructures is not an array");
    }
  }
}
