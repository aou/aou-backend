/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationStructureId.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.io.Serializable;
import java.util.Objects;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.settings.Structure;

@Embeddable
public class PublicationStructureId implements Serializable {
    private static final long serialVersionUID = 2086021467257889857L;

    @ManyToOne
    @JoinColumn(name = AouConstants.DB_PUBLICATION_ID, referencedColumnName = AouConstants.DB_RES_ID)
    private Publication publication;

    @ManyToOne
    @JoinColumn(name = AouConstants.DB_STRUCTURE_ID, referencedColumnName = AouConstants.DB_RES_ID)
    private Structure structure;

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof PublicationStructureId)) {
            return false;
        }
        final PublicationStructureId castOther = (PublicationStructureId) other;
        return Objects.equals(this.getPublication(), castOther.getPublication()) && Objects
                .equals(this.getStructure(), castOther.getStructure());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getPublication(), this.getStructure());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " + AouConstants.DB_PUBLICATION_ID + "=" + this.getPublication().getResId() + " "
                + AouConstants.DB_STRUCTURE_ID + "=" + this.getStructure().getResId();
    }
}
