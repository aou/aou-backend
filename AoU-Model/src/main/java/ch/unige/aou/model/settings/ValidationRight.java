/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ValidationRight.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.util.Objects;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.rest.JoinResource;

import ch.unige.aou.model.publication.PublicationSubtype;

@Entity
@Table(name = "personValidationRightStructure")
public class ValidationRight extends JoinResource<ValidationRightKey> {

  public static final String PATH_TO_PERSON = "compositeKey.person";
  public static final String PERSON_RELATION_PROPERTY_NAME = "validationRights";
  public static final String PATH_TO_STRUCTURE = "compositeKey.structure";
  public static final String STRUCTURE_RELATION_PROPERTY_NAME = "validationRights";
  public static final String PATH_TO_PUBLICATION_SUBTYPE = "compositeKey.publicationSubtype";

  @EmbeddedId
  @JsonIgnore
  private ValidationRightKey compositeKey;

  public ValidationRight() {
    this.compositeKey = new ValidationRightKey();
  }

  @Override
  public ValidationRightKey getCompositeKey() {
    return this.compositeKey;
  }

  @Override
  public void setCompositeKey(ValidationRightKey compositeKey) {
    this.compositeKey = compositeKey;
  }

  /*************************************************************************************/

  @JsonIgnore
  public Person getPerson() {
    return this.getCompositeKey().getPerson();
  }

  public void setPerson(Person person) {
    this.getCompositeKey().setPerson(person);
  }

  @JsonIgnore
  public Structure getStructure() {
    return this.getCompositeKey().getStructure();
  }

  public void setStructure(Structure structure) {
    this.getCompositeKey().setStructure(structure);
  }

  @JsonIgnore
  public PublicationSubtype getPublicationSubtype() {
    return this.getCompositeKey().getPublicationSubtype();
  }

  public void setPublicationSubtype(PublicationSubtype publicationSubtype) {
    this.getCompositeKey().setPublicationSubtype(publicationSubtype);
  }

  /*************************************************************************************/

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || this.getClass() != other.getClass()) {
      return false;
    }

    ValidationRight castOther = (ValidationRight) other;
    return Objects.equals(this.getStructure(), castOther.getStructure())
            && Objects.equals(this.getPerson(), castOther.getPerson())
            && Objects.equals(this.getPublicationSubtype(), castOther.getPublicationSubtype());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getStructure(), this.getPerson(), this.getPublicationSubtype());
  }

  /*************************************************************************************/
  /*************************************************************************************/

}
