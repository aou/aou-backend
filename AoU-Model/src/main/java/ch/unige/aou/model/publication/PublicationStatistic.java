/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationStatistic.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.aou.model.AouResourceNormalized;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "archiveId", "publicationYear" }) })
public class PublicationStatistic extends AouResourceNormalized {
  @NotNull
  @Size(min = 1)
  private String archiveId;

  @NotNull
  @Size(min = 1)
  private String title;

  private String containerTitle;

  private Integer viewNumber;
  private Integer downloadNumber;
  @NotNull
  private int publicationYear;
  @NotNull
  private String accessLevel;
  @NotNull
  private String openAccess;

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContainerTitle() {
    return this.containerTitle;
  }

  public void setContainerTitle(String containerTitle) {
    this.containerTitle = containerTitle;
  }

  public String getAccessLevel() {
    return this.accessLevel;
  }

  public void setAccessLevel(String accessLevel) {
    this.accessLevel = accessLevel;
  }

  public String getOpenAccess() {
    return this.openAccess;
  }

  public void setOpenAccess(String openAccess) {
    this.openAccess = openAccess;
  }

  public int getPublicationYear() {
    return this.publicationYear;
  }

  public void setPublicationYear(int year) {
    this.publicationYear = year;
  }

  public Integer getDownloadNumber() {
    return this.downloadNumber;
  }

  public void setDownloadNumber(Integer downloadNumber) {
    this.downloadNumber = downloadNumber;
  }

  public Integer getViewNumber() {
    return this.viewNumber;
  }

  public void setViewNumber(Integer viewNumber) {
    this.viewNumber = viewNumber;
  }

  public String getArchiveId() {
    return this.archiveId;
  }

  public void setArchiveId(String archiveId) {
    this.archiveId = archiveId;
  }
}
