/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Publication.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import static ch.unige.solidify.SolidifyConstants.RES_ID_FIELD;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.listener.HistoryListener;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.PublicationAwareResource;
import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.orcid.OrcidSynchronization;
import ch.unige.aou.model.rest.PersonDTO;
import ch.unige.aou.model.schedule.SwissNationalLibraryUpload;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;

@Entity
@EntityListeners({ HistoryListener.class })
@Table(indexes = { @Index(columnList = "status"), @Index(columnList = "archiveId"), @Index(columnList = "creationWhen") })
public class Publication extends AouResourceNormalized implements PublicationAwareResource, ResourceFileInterface {

  public Publication() {
  }

  public Publication(Publication originalPublication) {
    this.title = originalPublication.title;
    this.status = PublicationStatus.IN_PROGRESS;
    this.subtype = originalPublication.subtype;
    this.subSubtype = originalPublication.subSubtype;
    this.importSource = ImportSource.CLONE;
    this.statusMessage = originalPublication.statusMessage;
    this.formStep = DepositFormStep.TYPE;
    this.metadataVersion = originalPublication.metadataVersion;
    if (originalPublication.thumbnail != null) {
      this.thumbnail = new PublicationThumbnail(originalPublication.thumbnail);
    }
    this.setMetadata(originalPublication.metadata); // use setter to make the field dirty
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, AouConstants.THUMBNAIL);
  }

  // @formatter:off
  public enum PublicationStatus {
    IN_PROGRESS,        // publication is being deposited
    IN_VALIDATION,      // publication must be reviewed by a validator
    FEEDBACK_REQUIRED,  // feedback from depositor is required
    REJECTED,           // publication has not been accepted
    SUBMITTED,          // publication has been accepted and is being processed
    CHECKED,            // publication has been checked (valid metadata)
    IN_PREPARATION,     // publication is in preparation (generating additional/export metadata)
    COMPLETED,          // publication has been accepted and processed. It is online.
    CANONICAL,          // publication is super-validated and can not be edited anymore by validators
    DELETED,            // publication is soft deleted
    IN_ERROR,           // publication has some technical error
    IN_EDITION,         // publication is being updated
    UPDATES_VALIDATION, // publication update must be reviewed by a validator
    CANCEL_EDITION,      // publication cancel the update in progress
    UPDATE_IN_BATCH     // publication has been updated in batch
  }
  // @formatter:on

  public enum DepositFormStep {
    TYPE, FILES, CONTRIBUTORS, DESCRIPTION, SUMMARY
  }

  public enum ImportSource {
    DOI, ARXIV, PMID, ISBN,
    DOI_ORCID, ARXIV_ORCID, PMID_ORCID, ISBN_ORCID, ORCID,
    CLONE, FEDORA3
  }

  // @formatter:off
  public enum MetadataValidationType {
    NONE,                                  // publication's metadata are not validated at all
    CURRENT_STEP,                          // only metadata corresponding to the current publication's 'formStep' value are validated
    GLOBAL,                                // all publications's metadata are validated, without additional rules that must pass for the publication to be validated
    GLOBAL_WITH_RULES_FOR_FINAL_VALIDATION // all publications's metadata are validated, including additional rules that must pass for the publication to be validated
  }
  // @formatter:on

  @NotNull
  @Size(min = 1, max = AouConstants.DB_LARGE_STRING_LENGTH)
  private String title;

  @Enumerated(EnumType.STRING)
  @NotNull
  private PublicationStatus status;

  @ManyToOne
  @NotNull
  private Person creator;

  @ManyToOne
  @NotNull
  private PublicationSubtype subtype;

  @ManyToOne
  private PublicationSubSubtype subSubtype;

  @NotNull
  @Size(min = 1)
  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String formData;
  @NotNull
  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String metadata;

  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String metadataBackup;

  @Enumerated(EnumType.STRING)
  private ImportSource importSource;

  @NotNull
  private Boolean notifyFilesFromExternalSource;

  @NotNull
  private Boolean notifyContributorsFromExternalSource;

  @Size(max = AouConstants.DB_LONG_STRING_LENGTH)
  private String statusMessage;

  @Enumerated(EnumType.STRING)
  @NotNull
  private Publication.DepositFormStep formStep;

  @Transient
  @JsonIgnore
  private MetadataValidationType metadataValidationType;

  @OneToMany(mappedBy = PublicationStructure.PATH_TO_PUBLICATION, cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<PublicationStructure> publicationStructures = new ArrayList<>();

  @ManyToMany()
  @JsonIgnore
  private Set<ResearchGroup> researchGroups = new HashSet<>();

  @ManyToMany(mappedBy = "favoritePublications")
  @JsonIgnore
  private Set<Person> people = new HashSet<>();

  @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, orphanRemoval = true)
  @OrderBy("sortValue ASC")
  @JsonIgnore
  private List<DocumentFile> documentFiles = new ArrayList<>();

  @JsonIgnore
  @ManyToMany()
  private Set<Contributor> contributors = new HashSet<>();

  @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<Comment> comments = new ArrayList<>();

  @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<Event> events = new ArrayList<>();

  @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<SwissNationalLibraryUpload> swissNationalLibraryUploads = new ArrayList<>();

  @OneToMany(mappedBy = "publication", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<OrcidSynchronization> orcidSynchronizations = new ArrayList<>();

  @NotNull
  @Enumerated(EnumType.STRING)
  private AouMetadataVersion metadataVersion;

  @Column(unique = true)
  private String doi;

  @Column(unique = true)
  private String pmid;

  private String archiveId;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = AouConstants.DB_THUMBNAIL_ID, referencedColumnName = AouConstants.DB_RES_ID, nullable = true)
  private PublicationThumbnail thumbnail;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @CreatedDate // default to the same value as date of creation
  private OffsetDateTime lastStatusUpdate;

  @ManyToMany()
  @JsonIgnore
  private Set<ContributorOtherName> contributorOtherNames = new HashSet<>();

  @Transient
  @JsonInclude()
  private PersonDTO lastEditor;

  @Transient
  private boolean isStatusDirty = false;

  @Transient
  private boolean isMetadataDirty = false;

  @Transient
  private boolean isFormDataDirty = false;

  /****************************************************************/

  public Person getCreator() {
    return this.creator;
  }

  public String getFormData() {
    return this.formData;
  }

  public String getMetadata() {
    return this.metadata;
  }

  public String getMetadataBackup() {
    return this.metadataBackup;
  }

  public ImportSource getImportSource() {
    return this.importSource;
  }

  public Boolean getNotifyFilesFromExternalSource() {
    return this.notifyFilesFromExternalSource;
  }

  public Boolean getNotifyContributorsFromExternalSource() {
    return this.notifyContributorsFromExternalSource;
  }

  public PublicationStatus getStatus() {
    return this.status;
  }

  public String getStatusMessage() {
    return this.statusMessage;
  }

  public String getTitle() {
    return this.title;
  }

  public PublicationSubtypeDTO getSubtype() {
    return new PublicationSubtypeDTO(this.subtype);
  }

  public PublicationSubSubtypeDTO getSubSubtype() {
    if (this.subSubtype != null) {
      return new PublicationSubSubtypeDTO(this.subSubtype);
    }
    return null;
  }

  public DepositFormStep getFormStep() {
    return this.formStep;
  }

  public MetadataValidationType getMetadataValidationType() {
    return this.metadataValidationType;
  }

  public Set<ResearchGroup> getResearchGroups() {
    return this.researchGroups;
  }

  public Set<Person> getPeople() {
    return this.people;
  }

  public List<PublicationStructure> getPublicationStructures() {
    return this.publicationStructures;
  }

  @JsonIgnore
  public List<DocumentFile> getDocumentFiles() {
    return this.documentFiles;
  }

  public Set<Contributor> getContributors() {
    return this.contributors;
  }

  public Set<ContributorOtherName> getContributorOtherNames() {
    return this.contributorOtherNames;
  }

  @JsonIgnore
  public List<Comment> getComments() {
    return this.comments;
  }

  @JsonIgnore
  public List<Event> getEvents() {
    return this.events;
  }

  @JsonIgnore
  public List<SwissNationalLibraryUpload> getSwissNationalLibraryUploads() {
    return this.swissNationalLibraryUploads;
  }

  @JsonIgnore
  public List<OrcidSynchronization> getOrcidSynchronizations() {
    return this.orcidSynchronizations;
  }

  public AouMetadataVersion getMetadataVersion() {
    if (this.metadataVersion == null) {
      return AouMetadataVersion.getDefaultVersion();
    }
    return this.metadataVersion;
  }

  public String getDoi() {
    return this.doi;
  }

  public String getPmid() {
    return this.pmid;
  }

  public String getArchiveId() {
    return this.archiveId;
  }

  public PublicationThumbnail getThumbnail() {
    return this.thumbnail;
  }

  public OffsetDateTime getLastStatusUpdate() {
    return this.lastStatusUpdate;
  }

  public PersonDTO getLastEditor() {
    return this.lastEditor;
  }

  /****************************************************************/

  public void setCreator(Person creator) {
    this.creator = creator;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setSubtype(PublicationSubtype subtype) {
    this.subtype = subtype;
  }

  public void setSubSubtype(PublicationSubSubtype subSubtype) {
    this.subSubtype = subSubtype;
  }

  public void setFormData(String formData) {
    if (!Objects.equals(this.formData, formData)) {
      this.formData = formData;
      this.isFormDataDirty = true;
    }
  }

  public void setMetadata(String metadata) {
    if (!Objects.equals(this.metadata, metadata)) {
      this.metadata = metadata;
      this.isMetadataDirty = true;
    }
  }

  public void setMetadataBackup(String metadataBackUp) {
    this.metadataBackup = metadataBackUp;
  }

  public void setImportSource(ImportSource importSource) {
    this.importSource = importSource;
  }

  public void setNotifyFilesFromExternalSource(Boolean notifyFilesFromExternalSource) {
    this.notifyFilesFromExternalSource = notifyFilesFromExternalSource;
  }

  public void setNotifyContributorsFromExternalSource(Boolean notifyContributorsFromExternalSource) {
    this.notifyContributorsFromExternalSource = notifyContributorsFromExternalSource;
  }

  public void setStatus(PublicationStatus status) {
    if (this.status != status) {
      this.status = status;
      this.isStatusDirty = true;
    }
  }

  public void setFormStep(DepositFormStep formStep) {
    this.formStep = formStep;
  }

  public void setMetadataValidationType(MetadataValidationType metadataValidationType) {
    this.metadataValidationType = metadataValidationType;
  }

  public void setStatusMessage(String message) {
    this.statusMessage = StringTool.truncateWithEllipsis(message, AouConstants.DB_LONG_STRING_LENGTH);
  }

  public void setPublicationStructures(List<PublicationStructure> publicationStructures) {
    this.publicationStructures = publicationStructures;
  }

  public void setResearchGroups(Set<ResearchGroup> researchGroups) {
    this.researchGroups = researchGroups;
  }

  public void setPeople(Set<Person> people) {
    this.people = people;
  }

  public void setDocumentFiles(List<DocumentFile> documentFiles) {
    this.documentFiles = documentFiles;
  }

  public void setContributors(Set<Contributor> contributors) {
    this.contributors = contributors;
  }

  public void setContributorOtherNames(Set<ContributorOtherName> contributorOtherNames) {
    this.contributorOtherNames = contributorOtherNames;
  }

  public void setMetadataVersion(AouMetadataVersion metadataVersion) {
    this.metadataVersion = metadataVersion;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public void setPmid(String pmid) {
    this.pmid = pmid;
  }

  public void setArchiveId(String archiveId) {
    this.archiveId = archiveId;
  }

  public void setThumbnail(PublicationThumbnail thumbnail) {
    this.thumbnail = thumbnail;
  }

  public void setLastStatusUpdate(OffsetDateTime dateTime) {
    this.lastStatusUpdate = dateTime;
  }

  public void setLastEditor(PersonDTO editor) {
    this.lastEditor = editor;
  }

  /****************************************************************/

  public long getNumberOfComments() {
    return this.getComments().stream().filter(c -> !c.isOnlyForValidators()).count();
  }

  public long getNumberOfValidatorComments() {
    return this.getComments().stream().filter(Comment::isOnlyForValidators).count();
  }

  /**
   * Get last recent comment of the publication according to the value of includeCommentsBetweenValidators parameter. In case of true, it will
   * take into account all comments made to filter the last recent comment, if not, it will take only the comments made by the depositors/users
   * excluding the validator's comment using the isOnlyForValidators property to filter.
   */
  public Comment getLastComment(boolean includeCommentsBetweenValidators) {
    if (this.comments.isEmpty()) {
      return null;
    }
    Comparator<Comment> creationTimeComparator = Comparator.comparing(Comment::getCreationTime);
    Optional<Comment> commentOpt = this.getComments().stream()
            .filter(includeCommentsBetweenValidators ? c -> true : c -> !c.isOnlyForValidators()).max(creationTimeComparator);
    return commentOpt.orElse(null);
  }

  public int getNumberOfFiles() {
    return this.documentFiles.size();
  }

  public List<StructureDTO> getMetadataStructures() {
    return this.getStructureDTOs(PublicationStructure.LinkType.METADATA);
  }

  public List<StructureDTO> getValidationStructures() {
    return this.getStructureDTOs(PublicationStructure.LinkType.VALIDATION);
  }

  private List<StructureDTO> getStructureDTOs(PublicationStructure.LinkType linkType) {
    List<StructureDTO> structureDTOS = new ArrayList<>();
    if (this.publicationStructures != null) {
      List<PublicationStructure> pubStructures = this.publicationStructures.stream().filter(s -> s.getLinkType() == linkType)
              .toList();
      for (PublicationStructure publicationStructure : pubStructures) {
        Structure structure = publicationStructure.getStructure();
        StructureDTO structureDTO = new StructureDTO(structure.getResId(), structure.getName(), structure.getStatus(),
                structure.getCodeStruct());
        structureDTOS.add(structureDTO);
      }
    }
    return structureDTOS;
  }

  @JsonIgnore
  public boolean isStatusDirty() {
    return this.isStatusDirty;
  }

  @JsonIgnore
  public boolean isMetadataDirty() {
    return this.isMetadataDirty;
  }

  @JsonIgnore
  public boolean isFormDataDirty() {
    return this.isFormDataDirty;
  }

  /****************************************************************/

  @JsonIgnore
  public PublicationStatus getDefaultStatus() {
    return PublicationStatus.IN_PROGRESS;
  }

  @JsonIgnore
  public DepositFormStep getDefaultStep() {
    return DepositFormStep.TYPE;
  }

  public void setErrorStatusWithMessage(String statusMessage) {
    this.setStatus(PublicationStatus.IN_ERROR);
    this.setStatusMessage(statusMessage);
  }

  public boolean addStructure(Structure structure) {
    return this.addPublicationStructure(structure, PublicationStructure.LinkType.VALIDATION);
  }

  public boolean addPublicationStructure(Structure t, PublicationStructure.LinkType v) {
    final PublicationStructure publicationStructure = new PublicationStructure();
    publicationStructure.setPublication(this);
    publicationStructure.setStructure(t);
    publicationStructure.setLinkType(v);
    return this.publicationStructures.add(publicationStructure);
  }

  /****************************************************************/

  public boolean addResearchGroup(ResearchGroup researchGroup) {
    return this.researchGroups.add(researchGroup);
  }

  public boolean removeResearchGroup(ResearchGroup researchGroup) {
    return this.researchGroups.remove(researchGroup);
  }

  public boolean addPerson(Person person) {
    return this.people.add(person);
  }

  public boolean removePerson(Person person) {
    return this.people.remove(person);
  }

  public boolean addContributor(Contributor contributor) {
    return this.contributors.add(contributor);
  }

  public boolean removeContributor(Contributor contributor) {
    return this.contributors.remove(contributor);
  }

  public boolean addContributorOtherName(ContributorOtherName contributorOtherName) {
    return this.contributorOtherNames.add(contributorOtherName);
  }

  public boolean removeContributorOtherName(ContributorOtherName contributorOtherName) {
    return this.contributorOtherNames.remove(contributorOtherName);
  }

  /***************************************************************/

  @Override
  public void init() {
    if (this.status == null) {
      this.status = this.getDefaultStatus();
    }
    if (this.formStep == null) {
      this.formStep = this.getDefaultStep();
    }
    if (this.metadataVersion == null) {
      this.metadataVersion = AouMetadataVersion.getDefaultVersion();
    }
    if (this.notifyFilesFromExternalSource == null) {
      this.notifyFilesFromExternalSource = false;
    }
    if (this.notifyContributorsFromExternalSource == null) {
      this.notifyContributorsFromExternalSource = false;
    }
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.COMMENTS).withRel(ResourceName.COMMENTS));
      this.add(linkBuilder.slash(this.getResId()).slash(ActionName.HISTORY).withRel(ActionName.HISTORY));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.DOCUMENT_FILES).withRel(ResourceName.DOCUMENT_FILES));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.RESEARCH_GROUPS).withRel(ResourceName.RESEARCH_GROUPS));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.STRUCTURES).withRel(ResourceName.STRUCTURES));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.CONTRIBUTORS).withRel(ResourceName.CONTRIBUTORS));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PUBLICATION_SUBTYPES).withRel(ResourceName.PUBLICATION_SUBTYPES));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PUBLICATION_TYPES).withRel(ResourceName.PUBLICATION_TYPES));
      this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.DOWNLOAD_THUMBNAIL).withRel(AouActionName.DOWNLOAD_THUMBNAIL));
      this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.UPLOAD_THUMBNAIL).withRel(AouActionName.UPLOAD_THUMBNAIL));
      this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.DELETE_THUMBNAIL).withRel(AouActionName.DELETE_THUMBNAIL));
    }
    switch (this.getStatus()) {
      case IN_PROGRESS:
        this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.SUBMIT_FOR_VALIDATION).withRel(AouActionName.SUBMIT_FOR_VALIDATION));
        break;
      case IN_VALIDATION:
        this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.SUBMIT).withRel(AouActionName.SUBMIT));
        this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.ASK_FEEDBACK).withRel(AouActionName.ASK_FEEDBACK));
        this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.REJECT).withRel(AouActionName.REJECT));
        this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.ENABLE_REVISION).withRel(AouActionName.ENABLE_REVISION));
        break;
      case IN_ERROR, REJECTED, COMPLETED, FEEDBACK_REQUIRED:
        this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.ENABLE_REVISION).withRel(AouActionName.ENABLE_REVISION));
        break;
      default:
        break;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (!(o instanceof Publication publication))
      return false;
    if (!super.equals(o))
      return false;
    return this.status == publication.status &&
            Objects.equals(this.title, publication.title) &&
            Objects.equals(this.metadata, publication.metadata) &&
            Objects.equals(this.formData, publication.formData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.status, this.title, this.metadata, this.formData);
  }

  @JsonIgnore
  @Override
  public String getPublicationId() {
    return this.getResId();
  }

  @JsonIgnore
  @Override
  public String getCreatorId() {
    return this.creator.getResId();
  }

  @Override
  public boolean isDeletable() {
    return (this.getStatus() == PublicationStatus.REJECTED || this.getStatus() == PublicationStatus.IN_ERROR
            || this.getStatus() == PublicationStatus.IN_PROGRESS
            || this.getStatus() == PublicationStatus.FEEDBACK_REQUIRED
            || this.getStatus() == PublicationStatus.DELETED);
  }

  @JsonIgnore
  @Override
  public boolean isModifiable() {
    return (this.getStatus() != PublicationStatus.REJECTED && this.getStatus() != PublicationStatus.SUBMITTED
            && this.getStatus() != PublicationStatus.COMPLETED && this.getStatus() != PublicationStatus.DELETED
            && this.getStatus() != PublicationStatus.CHECKED && this.getStatus() != PublicationStatus.IN_PREPARATION
            && this.getStatus() != PublicationStatus.CANONICAL);
  }

  @Override
  @JsonIgnore
  public ResourceFile setNewResourceFile() {
    if (this.getThumbnail() == null) {
      this.setThumbnail(new PublicationThumbnail());
    }
    return this.getThumbnail();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getThumbnail();
  }

  @Override
  @JsonIgnore
  public void setResourceFile(ResourceFile resourceFile) {
    this.setThumbnail((PublicationThumbnail) resourceFile);
  }
}
