/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeContributorRolePkId.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.io.Serializable;
import java.util.Objects;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import ch.unige.aou.AouConstants;

@Embeddable
public class PublicationSubtypeContributorRolePkId implements Serializable {
  private static final long serialVersionUID = 64684894123187L;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_PUBLICATION_SUBTYPE_ID)
  private PublicationSubtype publicationSubtype;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_CONTRIBUTOR_ROLE_ID)
  private ContributorRole contributorRole;

  public PublicationSubtype getPublicationSubtype() {
    return this.publicationSubtype;
  }

  public void setPublicationSubtype(PublicationSubtype publicationSubtype) {
    this.publicationSubtype = publicationSubtype;
  }

  public ContributorRole getContributorRole() {
    return this.contributorRole;
  }

  public void setContributorRole(ContributorRole contributorRole) {
    this.contributorRole = contributorRole;
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }

    if (!(other instanceof PublicationSubtypeContributorRolePkId)) {
      return false;
    }

    final PublicationSubtypeContributorRolePkId castOther = (PublicationSubtypeContributorRolePkId) other;
    return Objects.equals(this.getPublicationSubtype(), castOther.getPublicationSubtype()) && Objects
            .equals(this.getContributorRole(), castOther.getContributorRole());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getPublicationSubtype(), this.getContributorRole());
  }
}
