/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - StatusHistory.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model;

import java.io.Serial;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;

@Entity
@Table(indexes = { @Index(columnList = "changeTime") })
public class StatusHistory implements Serializable {
  public static final String CREATED_STATUS = "CREATED";

  @Serial
  private static final long serialVersionUID = 481362136969468794L;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = 3)
  private OffsetDateTime changeTime;

  private String createdBy;

  @Transient
  private String creatorName;

  @Transient
  private String step;

  @Size(max = AouConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @NotNull
  @JsonIgnore
  private String resId;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @JsonIgnore
  private long seq;

  @NotNull
  private String status;

  @NotNull
  @JsonIgnore
  private String type;

  public StatusHistory() {
  }

  public StatusHistory(String type, String id, OffsetDateTime datetime, String createdBy) {
    this.type = type;
    this.resId = id;
    this.status = CREATED_STATUS;
    this.changeTime = datetime;
    this.createdBy = createdBy;
  }

  public StatusHistory(String type, String id, OffsetDateTime datetime, String description, String createdBy) {
    this.type = type;
    this.resId = id;
    this.status = CREATED_STATUS;
    this.changeTime = datetime;
    this.createdBy = createdBy;
    this.description = description;
  }

  public StatusHistory(String type, String id, OffsetDateTime datetime, String sts, String desc, String createdBy) {
    this.type = type;
    this.resId = id;
    this.status = sts;
    this.changeTime = datetime;
    this.description = desc;
    this.createdBy = createdBy;
  }

  public StatusHistory(String type, String id, OffsetDateTime datetime, String sts, String desc, String step, String createdBy) {
    this.type = type;
    this.resId = id;
    this.status = sts;
    this.changeTime = datetime;
    this.description = desc;
    this.createdBy = createdBy;
    this.step = step;
  }

  public StatusHistory(String type, String id, String sts) {
    this.type = type;
    this.resId = id;
    this.status = sts;
    this.changeTime = OffsetDateTime.now(ZoneOffset.UTC);
    this.createdBy = AouConstants.AOU;
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof StatusHistory)) {
      return false;
    }
    final StatusHistory castOther = (StatusHistory) other;
    return Objects.equals(this.seq, castOther.seq) && Objects.equals(this.resId, castOther.resId) && Objects.equals(this.type, castOther.type)
            && Objects
            .equals(this.changeTime, castOther.changeTime)
            && Objects.equals(this.status, castOther.status) && Objects
            .equals(this.description, castOther.description);
  }

  public OffsetDateTime getChangeTime() {
    return this.changeTime;
  }

  public void setChangeTime(OffsetDateTime changeTime) {
    this.changeTime = changeTime;
  }

  public String getCreatedBy() {
    return this.createdBy;
  }

  public String getCreatorName() {
    if (StringTool.isNullOrEmpty(this.creatorName)) {
      return this.getCreatedBy();
    } else {
      return this.creatorName;
    }
  }

  public String getDescription() {
    return this.description;
  }

  public String getResId() {
    return this.resId;
  }

  public long getSeq() {
    return this.seq;
  }

  public String getStatus() {
    return this.status;
  }

  public String getType() {
    return this.type;
  }

  public String getStep() {
    return this.step;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.seq, this.resId, this.type, this.changeTime, this.status, this.description);
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public void setCreatorName(String creatorName) {
    this.creatorName = creatorName;
  }

  public void setDescription(String description) {
    this.description = StringTool.truncateWithEllipsis(description, AouConstants.DB_LONG_STRING_LENGTH);
  }

  @Override
  public String toString() {
    return "StatusHistory [resId=" + this.resId + ", type=" + this.type + ", changeTime=" + this.changeTime + ", status=" + this.status
            + ", description="
            + this.description + "]";
  }

}
