/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DocumentFileStatisticsDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.unige.aou.model.publication.DocumentFile.DocumentFileStatus;

public class DocumentFileStatisticsDTO {
  private DocumentFileStatus documentFileStatus;
  private long numberOfOccurence;

  public DocumentFileStatisticsDTO(DocumentFileStatus documentFileStatus, long numberOfOccurrence) {
    this.documentFileStatus = documentFileStatus;
    this.numberOfOccurence = numberOfOccurrence;
  }
  public DocumentFileStatus getDocumentFileStatus() {
    return this.documentFileStatus;
  }
  public long getNumberOfOccurence() {
    return this.numberOfOccurence;
  }
  public static Map<DocumentFileStatus, Long> getMapFromList(List<DocumentFileStatisticsDTO> listStatistics) {
    Map<DocumentFileStatus, Long> mapStatistics = new HashMap<>();
    for(DocumentFileStatisticsDTO stat : listStatistics) {
      mapStatistics.put(stat.getDocumentFileStatus(), stat.getNumberOfOccurence());
    }
    return mapStatistics;
  }
}
