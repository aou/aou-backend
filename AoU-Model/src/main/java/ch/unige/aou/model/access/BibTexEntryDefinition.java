/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - BibtexEntryDefinition.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.access;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class BibTexEntryDefinition {
  public static final BibTexEntryDefinition ARTICLE = new BibTexEntryDefinition("article", new LinkedHashSet<>(Arrays.asList("author", "title", "journal", "year")));
  public static final BibTexEntryDefinition BOOK = new BibTexEntryDefinition("book", new LinkedHashSet<>(Arrays.asList("author", "editor", "title", "publisher", "year")));
  public static final BibTexEntryDefinition INBOOK = new BibTexEntryDefinition("inbook", new LinkedHashSet<>(Arrays.asList("author", "title", "booktitle", "publisher", "year")));
  public static final BibTexEntryDefinition PROCEEDINGS = new BibTexEntryDefinition("proceedings", new LinkedHashSet<>(Arrays.asList("title", "year")));
  public static final BibTexEntryDefinition INPROCEEDINGS = new BibTexEntryDefinition("inproceedings", new LinkedHashSet<>(Arrays.asList("author", "title", "booktitle", "year")));
  public static final BibTexEntryDefinition PHDTHESIS = new BibTexEntryDefinition("phdthesis", new LinkedHashSet<>(Arrays.asList("author", "title", "school", "year")));
  public static final BibTexEntryDefinition MASTERTHESIS = new BibTexEntryDefinition("masterthesis", new LinkedHashSet<>(Arrays.asList("author", "title", "school", "year")));
  public static final BibTexEntryDefinition TECHREPORT = new BibTexEntryDefinition("techreport", new LinkedHashSet<>(Arrays.asList("author", "title", "year")));
  public static final BibTexEntryDefinition UNPUBLISHED = new BibTexEntryDefinition("unpublished", new LinkedHashSet<>(Arrays.asList("author", "title", "year")));
  public static final BibTexEntryDefinition MISC = new BibTexEntryDefinition("misc", new LinkedHashSet<>());

  private String type;
  private LinkedHashSet<String> requiredFields = new LinkedHashSet<>();

  public BibTexEntryDefinition(String type, LinkedHashSet<String> requiredFields) {
    this.requiredFields = requiredFields;
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public boolean isValid(BibTexEntry bibtexEntry) {
    // Check if property fields contains the requiredFields in their order
    Iterator<String> requiredIterator = requiredFields.iterator();
    Iterator<String> fieldsIterator = bibtexEntry.getFields().keySet().iterator();

    while (requiredIterator.hasNext()) {
      String requiredField = requiredIterator.next();

      while (fieldsIterator.hasNext()) {
        String currentField = fieldsIterator.next();
        if (currentField.equals(requiredField)) {
          break;
        }
      }

      // If we reach the end of the fieldsIterator means that the required Fields are not in order.
      if (!fieldsIterator.hasNext() && requiredIterator.hasNext()) {
        return false;
      }
    }

    return true;
  }

}
