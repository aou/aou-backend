/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Comment.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.PublicationAwareResource;
import ch.unige.aou.model.settings.Person;

@Entity
public class Comment extends AouResourceNormalized implements PublicationAwareResource {

  @NotNull
  @Size(min = 1, max = AouConstants.DB_LARGE_STRING_LENGTH)
  private String text;

  @NotNull
  @ManyToOne(targetEntity = Publication.class)
  private Publication publication;

  @ManyToOne(targetEntity = Person.class)
  private Person person;

  @NotNull
  private Boolean onlyForValidators;

  // Indicates if the comment was automatically created by the backend
  @Column(nullable = false)
  private Boolean system;

  @Transient
  @JsonIgnore
  public Boolean hasOnlyForValidatorValue() {
    return (this.onlyForValidators != null);
  }

  public boolean isOnlyForValidators() {
    return Objects.requireNonNullElse(this.onlyForValidators, Boolean.TRUE);
  }

  public void setSystem(Boolean system) {
    this.system = system;
  }

  public String getText() {
    return this.text;
  }

  public Publication getPublication() {
    return this.publication;
  }

  public Person getPerson() {
    return this.person;
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setPublication(Publication publication) {
    this.publication = publication;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public void setOnlyForValidators(boolean onlyForValidators) {
    this.onlyForValidators = onlyForValidators;
  }

  public Boolean isSystem() {
    return this.system;
  }

  @JsonIgnore
  @Override
  public String getPublicationId() {
    return this.publication.getResId();
  }

  @JsonIgnore
  @Override
  public String getCreatorId() {
    return this.person.getResId();
  }

  @Override
  @PrePersist
  @PreUpdate
  public void init() {
    if (this.onlyForValidators == null) {
      this.onlyForValidators = true;
    }
    if (this.system == null) {
      this.system = false;
    }
  }
}
