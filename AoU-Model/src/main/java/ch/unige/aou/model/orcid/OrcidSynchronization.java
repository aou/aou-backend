/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - OrcidSynchronization.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.model.orcid;

import java.math.BigInteger;
import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "publication_res_id", "person_res_id", "put_code" }) })
public class OrcidSynchronization extends AouResourceNormalized {

  @NotNull
  @ManyToOne(targetEntity = Publication.class)
  private Publication publication;

  @NotNull
  @ManyToOne(targetEntity = Person.class)
  private Person person;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime uploadDate;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime downloadDate;

  @NotNull
  private BigInteger putCode;

  public Publication getPublication() {
    return this.publication;
  }

  public void setPublication(Publication publication) {
    this.publication = publication;
  }

  public Person getPerson() {
    return this.person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public OffsetDateTime getUploadDate() {
    return this.uploadDate;
  }

  public void setUploadDate(OffsetDateTime uploadDate) {
    this.uploadDate = uploadDate;
  }

  public OffsetDateTime getDownloadDate() {
    return this.downloadDate;
  }

  public void setDownloadDate(OffsetDateTime downloadDate) {
    this.downloadDate = downloadDate;
  }

  public BigInteger getPutCode() {
    return this.putCode;
  }

  public void setPutCode(BigInteger putCode) {
    this.putCode = putCode;
  }
}
