/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - UserDisplayableInfos.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.aou.model.settings.PersonAvatar;

public class UserDisplayableInfos {
  private String firstName;
  private String lastName;
  private String orcid;
  private PersonAvatar avatar;

  public UserDisplayableInfos() {
  }

  public UserDisplayableInfos(User user) {
    if (user.getPerson() != null) {
      this.firstName = user.getPerson().getFirstName();
      this.lastName = user.getPerson().getLastName();
      this.orcid = user.getPerson().getOrcid();
      this.avatar = user.getPerson().getAvatar();
    } else {
      this.firstName = user.getFirstName();
      this.lastName = user.getLastName();
    }
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getOrcid() {
    return this.orcid;
  }

  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public PersonAvatar getAvatar() {
    return this.avatar;
  }

  public void setAvatar(PersonAvatar avatar) {
    this.avatar = avatar;
  }

  @JsonIgnore
  public String getFullName() {
    return this.getLastName() + ", " + this.getFirstName();
  }
}
