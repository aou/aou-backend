/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Role.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.model.settings.Label;
import ch.unige.aou.model.settings.Person;

@Entity
public class Role extends AouResourceNormalized implements Sortable {

  @NotNull
  @Column(unique = true)
  private String name;

  /**
   * Property allowing to sort roles in lists
   */
  @Column(nullable = false)
  private Integer sortValue;

  @ElementCollection
  @Column(name = AouConstants.DB_ROLE_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_ROLE_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_ROLE_ID, AouConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  @ManyToMany(mappedBy = "roles")
  @JsonIgnore
  private Set<Person> people;

  /****************************************************************/

  public String getName() {
    return this.name;
  }

  @Override
  public Integer getSortValue() {
    return this.sortValue;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  /****************************************************************/

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }
}
