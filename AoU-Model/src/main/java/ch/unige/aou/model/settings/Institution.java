/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Institution.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.converter.WeakManyToManyConverter;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.unige.aou.AouConstants;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Entity
public class Institution extends ResourceNormalized implements ResourceFileInterface {
  @Size(max = AouConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Convert(converter = WeakManyToManyConverter.class)
  private List<String> emailSuffixes = new ArrayList<>();

  @NotNull
  @Column(unique = true)
  @Size(min = 1)
  private String name;

  @ManyToMany(mappedBy = "institutions")
  @JsonIgnore
  private Set<Person> people;

  private URL url;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = AouConstants.DB_LOGO_ID, referencedColumnName = AouConstants.DB_RES_ID, nullable = true)
  private InstitutionLogo logo;

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.PEOPLE).withRel(ResourceName.PEOPLE));
      this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.UPLOAD_LOGO).withRel(AouActionName.UPLOAD_LOGO));
      this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.DOWNLOAD_LOGO).withRel(AouActionName.DOWNLOAD_LOGO));
    }
  }

  public boolean addPerson(Person p) {
    final boolean result = this.people.add(p);
    if (!p.getInstitutions().contains(this)) {
      p.addInstitution(this);
    }
    return result;
  }

  public String getDescription() {
    return this.description;
  }

  public List<String> getEmailSuffixes() {
    return this.emailSuffixes;
  }

  public String getName() {
    return this.name;
  }

  public Set<Person> getPeople() {
    return this.people;
  }

  public URL getUrl() {
    return this.url;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public boolean removePerson(Person p) {
    final boolean result = this.people.remove(p);
    if (p.getInstitutions().contains(this)) {
      p.removeInstitution(this);
    }
    return result;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setEmailSuffixes(List<String> emailSuffixes) {
    this.emailSuffixes = emailSuffixes;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public InstitutionLogo getLogo() {
    return this.logo;
  }

  public void setLogo(InstitutionLogo logo) {
    this.logo = logo;
  }

  @Override
  public ResourceFile setNewResourceFile() {
    if (this.getLogo() == null) {
      this.setLogo(new InstitutionLogo());
    }
    return this.getLogo();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getLogo();
  }

  @Override
  @JsonIgnore
  public void setResourceFile(ResourceFile logo) {
    this.setLogo((InstitutionLogo) logo);
  }
}
