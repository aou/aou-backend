/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - StructureDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.io.Serializable;

import ch.unige.aou.model.settings.Structure;

public class StructureDTO implements Serializable {
  private String resId;
  private String name;
  private Structure.StructureStatus status;
  private String code;

  public StructureDTO() {
  }

  public StructureDTO(String resId, String name, Structure.StructureStatus status, String code) {
    this.resId = resId;
    this.name = name;
    this.status = status;
    this.code = code;
  }

  public String getResId() {
    return this.resId;
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Structure.StructureStatus getStatus() {
    return this.status;
  }

  public void setStatus(Structure.StructureStatus status) {
    this.status = status;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
