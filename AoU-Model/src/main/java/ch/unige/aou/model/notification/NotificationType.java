/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - NotificationType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.notification;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;

import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.settings.Person;

@Entity
public class NotificationType extends AouResourceNormalized {

  // For User
  public static final NotificationType MY_PUBLICATION_COMMENTED = new NotificationType("MY_PUBLICATION_COMMENTED",
          EventType.COMMENT_IN_PUBLICATION);
  public static final NotificationType MY_INDIRECT_PUBLICATION_COMMENTED = new NotificationType("MY_INDIRECT_PUBLICATION_COMMENTED",
          EventType.COMMENT_IN_PUBLICATION);
  public static final NotificationType MY_PUBLICATION_REJECTED = new NotificationType("MY_PUBLICATION_REJECTED", EventType.PUBLICATION_REJECTED);
  public static final NotificationType MY_PUBLICATION_VALIDATED = new NotificationType("MY_PUBLICATION_VALIDATED",
          EventType.PUBLICATION_SUBMITTED);
  public static final NotificationType MY_PUBLICATION_FEEDBACK_REQUIRED = new NotificationType("MY_PUBLICATION_FEEDBACK_REQUIRED",
          EventType.PUBLICATION_FEEDBACK_REQUIRED);
  public static final NotificationType MY_INDIRECT_PUBLICATION_VALIDATED = new NotificationType("MY_INDIRECT_PUBLICATION_VALIDATED",
          EventType.PUBLICATION_SUBMITTED);
  public static final NotificationType PUBLICATION_FORGOTTEN_IN_PROGRESS = new NotificationType("PUBLICATION_FORGOTTEN_IN_PROGRESS",
          EventType.PUBLICATION_CREATED);

  public static final NotificationType PUBLICATION_DETECTED_ON_ORCID = new NotificationType("PUBLICATION_DETECTED_ON_ORCID",
          EventType.PUBLICATION_DETECTED_ON_ORCID);

  public static final NotificationType PUBLICATION_EXPORTED_TO_ORCID = new NotificationType("PUBLICATION_EXPORTED_TO_ORCID",
          EventType.PUBLICATION_EXPORTED_TO_ORCID);

  // For Validator
  public static final NotificationType PUBLICATION_TO_VALIDATE = new NotificationType("PUBLICATION_TO_VALIDATE",
          EventType.PUBLICATION_TO_VALIDATE);
  public static final NotificationType PUBLICATION_TO_VALIDATE_COMMENTED = new NotificationType("PUBLICATION_TO_VALIDATE_COMMENTED",
          EventType.COMMENT_IN_PUBLICATION);
  public static final NotificationType PUBLICATION_FORGOTTEN_TO_VALIDATE = new NotificationType("PUBLICATION_FORGOTTEN_TO_VALIDATE",
          EventType.PUBLICATION_TO_VALIDATE);

  @NotNull
  @ManyToOne
  private EventType eventType;

  @ManyToMany(mappedBy = "subscribedNotificationTypes")
  @JsonIgnore
  private Set<Person> subscribers;

  public NotificationType(String resId, EventType eventType) {
    this.setResId(resId);
    this.eventType = eventType;
  }

  public NotificationType() {
  }

  public static List<NotificationType> getAllNotificationTypes() {
    return Arrays.asList(PUBLICATION_TO_VALIDATE, PUBLICATION_TO_VALIDATE_COMMENTED, MY_PUBLICATION_REJECTED,
            MY_PUBLICATION_VALIDATED, MY_INDIRECT_PUBLICATION_VALIDATED, MY_PUBLICATION_FEEDBACK_REQUIRED, PUBLICATION_FORGOTTEN_TO_VALIDATE,
            PUBLICATION_FORGOTTEN_IN_PROGRESS, MY_INDIRECT_PUBLICATION_COMMENTED, MY_PUBLICATION_COMMENTED, PUBLICATION_DETECTED_ON_ORCID,
            PUBLICATION_EXPORTED_TO_ORCID);
  }

  public Set<Person> getSubscribers() {
    return this.subscribers;
  }

  public boolean addSubscriber(Person p) {
    final boolean result = this.subscribers.add(p);
    if (!p.getSubscribedNotifications().contains(this)) {
      p.addNotificationType(this);
    }
    return result;
  }

  public boolean removeSubscriber(Person p) {
    final boolean result = this.subscribers.remove(p);
    if (p.getSubscribedNotifications().contains(this)) {
      p.removeNotificationType(this);
    }
    return result;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof NotificationType))
      return false;
    final NotificationType that = (NotificationType) o;
    return this.getResId().equals(that.getResId()) &&
            Objects.equals(this.eventType, that.eventType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.eventType);
  }

  public EventType getEventType() {
    return this.eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }
}
