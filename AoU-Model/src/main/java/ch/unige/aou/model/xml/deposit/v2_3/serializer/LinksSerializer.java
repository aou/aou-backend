/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - LinksSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_3.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.unige.aou.model.xml.deposit.v2_3.Link;
import ch.unige.aou.model.xml.deposit.v2_3.Links;

public class LinksSerializer extends StdSerializer<Links> {
  public LinksSerializer() {
    this(null);
  }

  public LinksSerializer(Class<Links> t) {
    super(t);
  }

  @Override
  public void serialize(Links value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
    jgen.writeStartArray();
    for (Link link : value.getLink()) {
      jgen.writeStartObject();
      jgen.writeStringField("target", link.getTarget());
      jgen.writeStringField("type", link.getType() != null ? link.getType().value() : null); // avoid NPE exception when type is null
      jgen.writeObjectField("description", link.getDescription());
      jgen.writeEndObject();
    }
    jgen.writeEndArray();
  }
}
