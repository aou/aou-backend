/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DepositIdentifiersDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_4.deserializer;

import java.io.IOException;
import java.math.BigInteger;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

import ch.unige.aou.model.tool.ValidationTool;
import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;

public class DepositIdentifiersDeserializer extends AouStdDeserializer<DepositIdentifiers> {

  private static final long serialVersionUID = 659865L;

  @Override
  public DepositIdentifiers deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    DepositIdentifiers depositIdentifiers = new DepositIdentifiers();

    boolean hasValue = false;

    JsonNode doiNode = jsonNode.get("doi");
    if (this.nodeHasTextValue(doiNode)) {
      depositIdentifiers.setDoi(doiNode.textValue());
      hasValue = true;
    }

    JsonNode pmidNode = jsonNode.get("pmid");
    if (pmidNode != null && !pmidNode.isNull() && !StringTool.isNullOrEmpty(pmidNode.asText())) {
      if (!ValidationTool.isValidPMID(pmidNode.asText())) {
        ValidationError error = new ValidationError("Invalid PMID");
        error.addError("description.identifiers.pmid", "Invalid PMID");
        throw new SolidifyValidationException(error);
      }
      depositIdentifiers.setPmid(new BigInteger(pmidNode.asText()));
      hasValue = true;
    }

    JsonNode isbnNode = jsonNode.get("isbn");
    if (this.nodeHasTextValue(isbnNode)) {
      depositIdentifiers.setIsbn(isbnNode.textValue());
      hasValue = true;
    }

    JsonNode issnNode = jsonNode.get("issn");
    if (this.nodeHasTextValue(issnNode)) {
      depositIdentifiers.setIssn(issnNode.textValue());
      hasValue = true;
    }

    JsonNode arxivNode = jsonNode.get("arxiv");
    if (this.nodeHasTextValue(arxivNode)) {
      depositIdentifiers.setArxiv(arxivNode.textValue());
      hasValue = true;
    }

    JsonNode mmsidNode = jsonNode.get("mmsid");
    if (this.nodeHasTextValue(mmsidNode)) {
      depositIdentifiers.setMmsid(mmsidNode.textValue());
      hasValue = true;
    }

    JsonNode repecNode = jsonNode.get("repec");
    if (this.nodeHasTextValue(repecNode)) {
      depositIdentifiers.setRepec(repecNode.textValue());
      hasValue = true;
    }

    JsonNode dblpNode = jsonNode.get("dblp");
    if (this.nodeHasTextValue(dblpNode)) {
      depositIdentifiers.setDblp(dblpNode.textValue());
      hasValue = true;
    }

    JsonNode urnNode = jsonNode.get("urn");
    if (this.nodeHasTextValue(urnNode)) {
      depositIdentifiers.setUrn(urnNode.textValue());
      hasValue = true;
    }

    JsonNode localNumberNode = jsonNode.get("localNumber");
    if (this.nodeHasTextValue(localNumberNode)) {
      depositIdentifiers.setLocalNumber(localNumberNode.textValue());
      hasValue = true;
    }

    JsonNode pmcidNode = jsonNode.get("pmcid");
    if (this.nodeHasTextValue(pmcidNode)) {
      depositIdentifiers.setPmcid(pmcidNode.textValue());
      hasValue = true;
    }

    if (hasValue) {
      return depositIdentifiers;
    } else {
      return null;
    }
  }

}
