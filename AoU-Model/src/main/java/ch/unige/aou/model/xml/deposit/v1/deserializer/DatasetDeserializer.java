/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DatasetDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v1.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v1.Dataset;
import ch.unige.aou.model.xml.deposit.v1.ObjectFactory;

public class DatasetDeserializer extends AouStdDeserializer<Dataset> {

  private static final long serialVersionUID = 659865L;

  @Override
  public Dataset deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    Dataset dataset = new Dataset();

    ObjectFactory factory = new ObjectFactory();

    boolean hasValue = false;

    JsonNode urlNode = jsonNode.get("url");
    if (this.nodeHasTextValue(urlNode)) {
      dataset.getUrlOrDoi().add(factory.createDatasetUrl(urlNode.textValue()));
      hasValue = true;
    }

    JsonNode doiNode = jsonNode.get("doi");
    if (this.nodeHasTextValue(doiNode)) {
      dataset.getUrlOrDoi().add(factory.createDatasetDoi(doiNode.textValue()));
      hasValue = true;
    }

    if (hasValue) {
      return dataset;
    } else {
      return null;
    }
  }
}
