/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - CleanTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.tool;

import java.text.Normalizer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Safelist;
import org.springframework.web.util.HtmlUtils;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;

public class CleanTool {

  private static final String NUMBER_SYMBOL_REGEX = "^[N|n]°\\s?\\d+";

  public static final String[] TAGS_ALLOWED_IN_ABSTRACT = { "b", "strong", "p", "a", "li", "ul", "sub", "sup" };
  public static final String[] TAGS_ALLOWED_IN_TITLE = { "em", "i", "sub", "sup" };

  private static final String HTML_QUOTE = "&quot;";

  /**
   * List of different existing hyphens
   * <p>
   * –   en dash                   0x2013  &ndash;
   * —   em dash                   0x2014  &mdash;
   * ‒   figure dash               0x2012  &#8210;
   * ﹘  small em dash             0xFE58  &#65112;
   * -   hyphen-minus              0x2D    &#45;
   * ֊   armenian hyphen           0x58A   &#1418;
   * ‐   hyphen                    0x2010  &#8208;
   * ‑   non-breaking hyphen       0x2011  &#8209;
   * ⁃   hyphen bullet             0x2043  &#8259;
   * ﹣  small hyphen-minus        0xFE63  &#65123;
   * －  fullwidth hyphen-minus    0xFF0D  &#65293;
   */
  public static final List<String> hyphensList = List.of("–", "—", "‒", "﹘", "-", "֊", "‐", "‑", "⁃", "﹣", "－");

  private static final Safelist titleJsoupSafeList = new Safelist().addTags(TAGS_ALLOWED_IN_TITLE);

  private static final Safelist abstractJsoupSafeList = new Safelist(titleJsoupSafeList).addTags(TAGS_ALLOWED_IN_ABSTRACT)
          .addAttributes("a", "href", "rel", "target")
          .addProtocols("a", "href", "http", "https");

  private CleanTool() {
    throw new IllegalStateException("Utility class");
  }

  public static String cleanTitle(String value) {
    if (!StringTool.isNullOrEmpty(value)) {
      value = CleanTool.removeNbsp(value);
      value = Jsoup.clean(value, CleanTool.titleJsoupSafeList);
      value = CleanTool.replaceBlankCharsBySpace(value);
      value = CleanTool.removeFinalDot(value);
    }
    return value;
  }

  public static String cleanAbstract(String value) {
    if (!StringTool.isNullOrEmpty(value)) {
      value = CleanTool.removeNbsp(value);
      value = Jsoup.clean(value, CleanTool.abstractJsoupSafeList);
      value = CleanTool.removeDoubleSpaces(value);
    }
    return value;
  }

  public static String cleanString(String value) {
    if (!StringTool.isNullOrEmpty(value)) {
      value = CleanTool.trim(value);
      value = CleanTool.replaceBlankCharsBySpace(value);
    }
    return value;
  }

  public static String cleanStringAndHyphens(String value, boolean clearSpacesSurroundingHyphens) {
    value = CleanTool.cleanString(value);
    value = CleanTool.cleanHyphens(value, clearSpacesSurroundingHyphens);
    return value;
  }

  public static String cleanPaging(String value) {
    value = CleanTool.trim(value);
    value = CleanTool.replaceBlankCharsBySpace(value);
    value = CleanTool.cleanHyphens(value, true);
    value = CleanTool.completeEndPageValue(value);
    return value;
  }

  public static String cleanFileName(String value) {
    return CleanTool.cleanFileName(value, false);
  }

  public static String cleanFileName(String value, boolean removeAccents) {
    if (!StringTool.isNullOrEmpty(value)) {
      if (removeAccents) {
        value = CleanTool.removeAccents(value);
      }
      value = value.replaceAll("\\s+", "_");
      value = value.replaceAll("\\+", "_");
    }
    return value;
  }

  public static String removeAllHtmlTags(String value) {
    return CleanTool.removeAllHtmlTags(value, true);
  }

  public static String removeAllHtmlTags(String value, boolean keepNewLines) {
    if (!StringTool.isNullOrEmpty(value)) {
      Document doc = Jsoup.parse(value);
      if (keepNewLines) {
        Document.OutputSettings outputSettings = new Document.OutputSettings();
        outputSettings.prettyPrint(false);
        doc.outputSettings(outputSettings);
        doc.select("br").before("\\n").after("\\n");
        doc.select("p").before("\\n").after("\\n");
        String str = doc.html().replaceAll("\\\\n", "\n");
        value = Jsoup.clean(str, "", Safelist.none(), outputSettings);
        value = value.replace("&nbsp;", " ");
        value = value.replace("&amp;", "&");
        value = value.replace("&lt;", "<");
        value = value.replace("&gt;", ">");
        value = value.replaceAll("\n +", "\n"); // remove spaces at beginning of new lines
        value = value.replaceAll(" {1,10}\n", "\n"); // remove spaces at end of lines
        value = value.replaceAll(" {1,10}", " "); // remove double spaces
        value = value.replaceAll("\n{1,10}", "\n"); // remove double \n
        value = value.trim();
      } else {
        value = doc.text();
      }
    }
    return value;
  }

  public static String restoreAllowedHtmlTags(String value, List<String> allowedTags) {
    if (!StringTool.isNullOrEmpty(value)) {
      for (String tag : allowedTags) {
        String openingTag = "<" + tag + ">";
        String closingTag = "</" + tag + ">";
        String encodedOpeningTag = HtmlUtils.htmlEscape(openingTag);
        String encodedClosingTag = HtmlUtils.htmlEscape(closingTag);
        value = StringTool.replaceIgnoreCase(value, encodedOpeningTag, openingTag);
        value = StringTool.replaceIgnoreCase(value, encodedClosingTag, closingTag);
      }
      value = value.replace("&amp;amp;lt;", "&amp;lt;");
      value = value.replace("&amp;amp;gt;", "&amp;gt;");

      // case of html entities
      value = value.replace("&amp;gt;", "&gt;");
      value = value.replace("&amp;lt;", "&lt;");
      value = value.replace("&amp;amp;", "&amp;");
    }
    return value;
  }

  public static String cleanQuotes(String value) {
    value = value.replace("&laquo;&nbsp;", "&laquo;");
    value = value.replace("&laquo;", HTML_QUOTE);
    value = value.replace("&nbsp;&raquo;", "&raquo;");
    value = value.replace("&raquo;", HTML_QUOTE);
    value = value.replace("&lsquo;", HTML_QUOTE);
    value = value.replace("&rsquo;", HTML_QUOTE);
    value = value.replace("&ldquo;", HTML_QUOTE);
    value = value.replace("&rdquo;", HTML_QUOTE);
    value = value.replace("&bdquo;", HTML_QUOTE);
    return value;
  }

  /*******************************************************************************************/

  public static String trim(String value) {
    if (value != null) {
      value = value.trim();
    }
    return value;
  }

  public static String removeDoubleSpaces(String value) {
    if (value != null) {
      value = value.replaceAll(" +", " ");
    }
    return value;
  }

  /**
   * Replaces \t \n \x0B \f \r chars by single spaces
   * <p>
   * Note: this method transforms a multilines text to single line text
   *
   * @param value
   * @return
   */
  public static String replaceBlankCharsBySpace(String value) {
    if (value != null) {

      value = value.replaceAll("\\s+", " ");
    }
    return value;
  }

  public static String removeFinalDot(String value) {
    if (value != null && value.endsWith(".")) {
      value = value.substring(0, value.length() - 1);
    }
    return value;
  }

  /**
   * Remove all hard coded &nbsp; and replace them with simple space
   *
   * @param value
   * @return
   */
  public static String removeNbsp(String value) {
    if (value != null) {
      value = value.replace("&nbsp;", " ");
    }
    return value;
  }

  /**
   * Replace hyphens of different kinds by a simple dash and remove surrounding spaces
   *
   * @param value
   * @return
   */
  public static String cleanHyphens(String value, boolean clearSurroundingSpaces) {
    if (value != null) {
      for (String hyphen : CleanTool.hyphensList) {
        if (hyphen.equals("-")) {
          // escape simple hyphen to be used in regex
          hyphen = "\\\\" + hyphen;
        }

        String regex = hyphen;
        if (clearSurroundingSpaces) {
          regex = "\\s*[" + hyphen + "]\\s*";
        }

        value = value.replaceAll(regex, "-");
      }
    }
    return value;
  }

  /**
   * Complete end page after hyphen such as :
   * <p>
   * "p. 123-45" --> "p. 123-145"
   * but
   * "p. 123-17" isn't updated as the result would make no sense.
   *
   * @param value
   * @return
   */
  public static String completeEndPageValue(String value) {
    if (value != null) {
      Pattern p = Pattern.compile("\\d{1,10}\\-\\d{1,10}");
      Matcher m = p.matcher(value);
      if (m.find()) {
        String pages = m.group(0);
        String startPage = pages.substring(0, pages.indexOf("-"));
        StringBuilder endPageBuilder = new StringBuilder(pages.substring(pages.indexOf("-") + 1));
        while (startPage.length() > endPageBuilder.length()) {
          int unitToComplete = startPage.length() - endPageBuilder.length() - 1;
          endPageBuilder.insert(0, startPage.charAt(unitToComplete));
        }
        if (Integer.parseInt(startPage) <= Integer.parseInt(endPageBuilder.toString())) {
          String newPages = startPage + "-" + endPageBuilder;
          value = value.replace(pages, newPages);
        }
      }
    }
    return value;
  }

  public static String capitalizeString(String name) {
    if (name.equals(name.toUpperCase())) {
      StringBuilder str = new StringBuilder(name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase());
      Matcher matcher = Pattern.compile("[- \\']").matcher(name);
      // Check all occurrences
      while (matcher.find()) {
        str.setCharAt(matcher.start() + 1, Character.toUpperCase(name.charAt(matcher.start() + 1)));
      }
      return str.toString();
    }
    return name;
  }

  public static String removeTrailingDots(String value) {
    if (value != null) {
      value = value.replaceAll("\\.{1,30}$", "");
    }
    return value;
  }

  public static String removeTrailingParenthesisAndContent(String value) {
    if (value != null) {
      value = value.replaceAll("\\(.*\\)$", "");
    }
    return value;
  }

  public static String removeInvisibleCharacters(String value) {
    if (value != null) {
      value = value.replaceAll("\\p{C}", "");
    }
    return value;
  }

  public static String removeTrailingColon(String value) {
    if (value != null) {
      value = value.replaceAll(":{1,30}$", "");
    }
    return value;
  }

  public static String extractNumbersFromString(String value) {
    if (value != null && value.matches(NUMBER_SYMBOL_REGEX)) {
      return value.replaceAll("\\D+", "");
    }
    return value;
  }

  public static Integer extractYearFromDate(String dateStr, String[] metadataDateFormat) {
    if (dateStr != null) {
      dateStr = dateStr.trim();
      DateTimeFormatter formatter;
      for (String dateFormat : metadataDateFormat) {
        try {
          formatter = new DateTimeFormatterBuilder()
                  .appendPattern(dateFormat)
                  .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                  .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                  .toFormatter();
          LocalDate date = LocalDate.parse(dateStr, formatter);
          return date.getYear();
        } catch (DateTimeParseException e) {
          // nothing to do, will try next format
        }
      }
    }
    return null;
  }

  /**
   * Replace chars with accents by the char only.
   * Note: long hyphens for instance are also replaced by standard hyphens.
   *
   * @param value
   * @return
   */
  public static String removeAccents(String value) {
    if (value != null) {
      // Split accented chars into chars + accents
      value = Normalizer.normalize(value, Normalizer.Form.NFKD);
      // Remove accents, letting chars only
      value = value.replaceAll("\\p{M}", "");
    }
    return value;
  }

  public static String getLanguageCode(String code) {
    if (code == null) {
      return null;
    }

    code = code.toLowerCase();

    return switch (code) {
      case "fr", "fra", "fre", "fr-fr", "fr-ch" -> "fre";
      case "en", "eng", "en-gb", "en-us" -> "eng";
      case "de", "de-de", "de-ch", "deu", "ger" -> "ger";
      case "ar", "ara" -> "ara";
      case "zh", "zho", "chi" -> "chi";
      case "es", "spa", "es-es" -> "spa";
      case "el", "ell", "gre" -> "gre";
      case "it", "ita" -> "ita";
      case "ja", "jpn" -> "jpn";
      case "nl", "nld", "dut" -> "dut";
      case "pt", "por" -> "por";
      case "rm", "roh" -> "roh";
      case "ru", "rus" -> "rus";
      case "tr", "tur" -> "tur";
      default -> "mis";
    };
  }

  public static String getFullUnigeExternalUid(String cnIndividu) {
    if (!StringTool.isNullOrEmpty(cnIndividu)) {
      if (cnIndividu.matches("\\d+")) {
        return cnIndividu + AouConstants.UNIGE_EXTERNAL_UID_SUFFIX;
      } else if (cnIndividu.endsWith(AouConstants.UNIGE_EXTERNAL_UID_SUFFIX)) {
        return cnIndividu;
      }
    }
    throw new SolidifyRuntimeException("Invalid cnIndividu '" + cnIndividu + "'");
  }

  public static String externalUidToCnIndividu(String externalUid) {
    if (!StringTool.isNullOrEmpty(externalUid)) {
      if (externalUid.endsWith(AouConstants.UNIGE_EXTERNAL_UID_SUFFIX)) {
        return externalUid.replace(AouConstants.UNIGE_EXTERNAL_UID_SUFFIX, "");
      } else if (externalUid.matches("\\d+")) {
        return externalUid;
      }
    }
    throw new SolidifyRuntimeException("externalUid '" + externalUid + "' does not end with '" + AouConstants.UNIGE_EXTERNAL_UID_SUFFIX + "'");
  }
}
