/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationsSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.publications;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.unige.aou.model.xml.list_deposits.Publication;
import ch.unige.aou.model.xml.list_deposits.Publications;

public class PublicationsSerializer extends StdSerializer<Publications> {

  public PublicationsSerializer() {
    this(null);
  }

  public PublicationsSerializer(Class<Publications> t) {
    super(t);
  }

  @Override
  public void serialize(Publications value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
    jgen.writeStartArray();
    for (Publication publication: value.getPublication()) {
      jgen.writeStartObject();
      jgen.writeObjectField("publication", publication);
      jgen.writeEndObject();

    }
    jgen.writeEndArray();
  }
}
