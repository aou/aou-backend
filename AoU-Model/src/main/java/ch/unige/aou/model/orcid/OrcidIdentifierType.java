/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - OrcidIdentifierType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.model.orcid;

/**
 * Types of identifiers accepted by ORCID API. Based on the list found here https://pub.orcid.org/v3.0/identifiers
 */
public enum OrcidIdentifierType {

  AGR("agr", "agr: Agricola"),
  ARK("ark", "ark: Archival Resource Key Identifier"),
  ARXIV("arxiv", "arxiv: ArXiv"),
  ASIN("asin", "asin: Amazon Standard Identification Number"),
  ASIN_TLD("asin-tld", "asin-tld: ASIN top-level domain"),
  AUTHENTICUSID("authenticusid", "authenticusid: AuthenticusID"),
  BIBCODE("bibcode", "Bibcode"),
  CBA("cba", "cba: Chinese Biological Abstracts"),
  CGN("cgn", "cgn: Culturegraph Number"),
  CIENCIAIUL("cienciaiul", "cienciaiul: Ciência-IUL Identifier"),
  CIT("cit", "cit: CiteSeer"),
  CSTR("cstr", "cstr: Science and technology resource identification"),
  CTX("ctx", "ctx: CiteExplore submission"),
  DNB("dnb", "dnb: German National Library identifier"),
  DOI("doi", "doi: Digital object identifier"),
  EID("eid", "Scopus Identifier"),
  EMDB("emdb", "emdb: Electron Microscopy Data Bank"),
  EMPIAR("empiar", "empiar: Electron Microscopy Public Image Archive"),
  ETHOS("ethos", "ethos: EThOS Persistent ID"),
  GRANT_NUMBER("grant_number", "grant number"),
  HAL("hal", "hal: Hyper Articles en Ligne"),
  HANDLE("handle", "handle: Handle"),
  HIR("hir", "hir: NHS Evidence"),
  ISBN("isbn", "isbn: International Standard Book Number"),
  ISMN("ismn", "ismn: International Standard Music Number"),
  ISSN("issn", "issn: International Standard Serial Number. Includes print and electronic ISSN."),
  JFM("jfm", "jfm: Jahrbuch über die Fortschritte der Mathematik"),
  JSTOR("jstor", "jstor: JSTOR abstract"),
  K10PLUS("k10plus", "k10plus: K10plus"),
  KUID("kuid", "kuid: KoreaMed Unique Identifier"),
  LCCN("lccn", "lccn: Library of Congress Control Number"),
  LENSID("lensid", "lensid: Lens ID"),
  MR("mr", "mr: Mathematical Reviews"),
  OCLC("oclc", "oclc: Online Computer Library Center"),
  OL("ol", "ol: Open Library"),
  OSTI("osti", "osti: Office of Scientific and Technical Information"),
  OTHER_ID("other-id", "Other identifier type"),
  PAT("pat", "Patent number"),
  PDB("pdb", "pdb: Protein Data Bank identifier"),
  PMC("pmc", "pmc: PubMed Central article number"),
  PMID("pmid", "pmid: PubMed Unique Identifier"),
  PPR("ppr", "ppr: Europe PMC Preprint Identifier"),
  PROPOSAL_ID("proposal-id", "Proposal ID"),
  RFC("rfc", "rfc: Request for Comments"),
  RRID("rrid", "rrid: Research Resource IDentifier"),
  SOURCE_WORK_ID("source-work-id", "Non-standard ID from work data source"),
  SSRN("ssrn", "ssrn: Social Science Research Network"),
  URI("uri", "uri: URI"),
  URN("urn", "urn: URN"),
  WOSUID("wosuid", "wosuid: Web of Science™ identifier"),
  ZBL("zbl", "zbl: Zentralblatt MATH");

  private final String name;
  private final String description;

  OrcidIdentifierType(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public String getName() {
    return this.name;
  }

  public String getDescription() {
    return this.description;
  }
}
