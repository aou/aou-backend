/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - StoredSearchCriteria.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.search;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.converter.KeywordsConverter;
import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.SearchConditionType;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.rest.AouSearchCondition;

@Entity
public class StoredSearchCriteria extends AouResourceNormalized {

  @NotNull
  @ManyToOne(targetEntity = StoredSearch.class)
  @JsonIgnore
  private StoredSearch search;

  @NotNull
  @Enumerated(EnumType.STRING)
  private CriteriaSource source;

  @NotNull
  @Enumerated(EnumType.STRING)
  private SearchConditionType type = SearchConditionType.MATCH;

  @NotNull
  @Enumerated(EnumType.STRING)
  private BooleanClauseType booleanClauseType = BooleanClauseType.MUST;

  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String field;

  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String value;

  @Convert(converter = KeywordsConverter.class)
  @Column(length = AouConstants.DB_LONG_STRING_LENGTH)
  private final List<String> terms = new ArrayList<>();

  @Size(min = 1, max = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private String lowerValue;

  @Size(min = 1, max = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private String upperValue;

  /****************************************************************/

  @Override
  @PrePersist
  @PreUpdate
  public void init() {
    this.clean();
    if (this.source == null) {
      this.source = CriteriaSource.ADVANCED_SEARCH;
    }
  }

  public void clean() {
    switch (this.type) {
      case MATCH:
      case QUERY:
      case SIMPLE_QUERY:
        this.terms.clear();
        this.lowerValue = null;
        this.upperValue = null;
        break;
      case TERM:
        this.lowerValue = null;
        this.upperValue = null;
        break;
      case RANGE:
        this.value = null;
        this.terms.clear();
        break;
      case NESTED_BOOLEAN:
        this.value = null;
        this.terms.clear();
        this.lowerValue = null;
        this.upperValue = null;
        break;
    }
  }

  /****************************************************************/

  public StoredSearch getSearch() {
    return this.search;
  }

  public void setSearch(StoredSearch search) {
    this.search = search;
  }

  public CriteriaSource getSource() {
    return this.source;
  }

  public void setSource(CriteriaSource source) {
    this.source = source;
  }

  public SearchConditionType getType() {
    return this.type;
  }

  public void setType(SearchConditionType type) {
    this.type = type;
  }

  public BooleanClauseType getBooleanClauseType() {
    return this.booleanClauseType;
  }

  public void setBooleanClauseType(BooleanClauseType booleanClauseType) {
    this.booleanClauseType = booleanClauseType;
  }

  public String getField() {
    return this.field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public List<String> getTerms() {
    return this.terms;
  }

  public String getLowerValue() {
    return this.lowerValue;
  }

  public void setLowerValue(String lowerValue) {
    this.lowerValue = lowerValue;
  }

  public String getUpperValue() {
    return this.upperValue;
  }

  public void setUpperValue(String upperValue) {
    this.upperValue = upperValue;
  }

  /****************************************************************/

  public AouSearchCondition toAouSearchCondition() {
    AouSearchCondition aouSearchCondition = new AouSearchCondition();
    aouSearchCondition.setSource(this.getSource());
    aouSearchCondition.setField(this.getField());
    aouSearchCondition.setBooleanClauseType(this.getBooleanClauseType());
    aouSearchCondition.setLowerValue(this.getLowerValue());
    aouSearchCondition.setUpperValue(this.getUpperValue());
    aouSearchCondition.setType(this.getType());
    aouSearchCondition.setValue(this.getValue());
    aouSearchCondition.getTerms().addAll(this.getTerms());
    return aouSearchCondition;
  }
}
