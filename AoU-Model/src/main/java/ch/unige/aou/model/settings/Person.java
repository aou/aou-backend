/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Person.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.auth.model.PersonInfo;
import ch.unige.solidify.model.OrcidToken;
import ch.unige.solidify.model.PersonWithOrcid;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.util.SearchCriteria;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouSearchableResourceNormalized;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.security.Role;
import ch.unige.aou.model.security.User;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.specification.PersonSearchSpecification;

@Entity
public class Person extends AouSearchableResourceNormalized<Person> implements PersonInfo, PersonWithOrcid, ResourceFileInterface {

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personStructures", joinColumns = { @JoinColumn(name = AouConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = AouConstants.DB_STRUCTURE_ID) }, uniqueConstraints = {
          @UniqueConstraint(columnNames = { AouConstants.DB_PERSON_ID, AouConstants.DB_STRUCTURE_ID }) })
  protected Set<Structure> structures = new HashSet<>();

  @JsonIgnore
  @OneToMany(mappedBy = ValidationRight.PATH_TO_PERSON, cascade = CascadeType.ALL, orphanRemoval = true)
  protected final List<ValidationRight> validationRights = new ArrayList<>();

  @NotNull
  @Size(min = 1)
  private String firstName;

  @JsonIgnore
  @OneToMany(mappedBy = "person")
  private List<User> users = new ArrayList<>();

  private Boolean verifiedOrcid = false;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "orcid_token_id", referencedColumnName = AouConstants.DB_RES_ID, nullable = true)
  private OrcidToken orcidToken;

  @Schema(description = "Indicates whether publications in the user's ORCID profile must be automatically synchronized to AoU.")
  @NotNull
  private Boolean syncFromOrcidProfile;

  @Schema(description = "Indicates whether publications to which the user is a contributor should be automatically synchronised with their ORCID profile.")
  @NotNull
  private Boolean syncToOrcidProfile;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personInstitutions", joinColumns = { @JoinColumn(name = AouConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = AouConstants.DB_INSTITUTION_ID) }, uniqueConstraints = {
          @UniqueConstraint(columnNames = { AouConstants.DB_PERSON_ID, AouConstants.DB_INSTITUTION_ID }) })
  private Set<Institution> institutions = new HashSet<>();

  @NotNull
  @Size(min = 1)
  private String lastName;

  @Pattern(regexp = "|[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}|[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}X", message = "The ORCID should be of the form 1234-1234-1234-1234 or 1234-1234-1234-123X")
  @Column(unique = true)
  private String orcid;

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = AouConstants.DB_AVATAR_ID, referencedColumnName = AouConstants.DB_RES_ID, nullable = true)
  private PersonAvatar avatar;

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personResearchGroups", joinColumns = { @JoinColumn(name = AouConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = AouConstants.DB_RESEARCH_GROUP_ID) }, uniqueConstraints = {
          @UniqueConstraint(columnNames = { AouConstants.DB_PERSON_ID, AouConstants.DB_RESEARCH_GROUP_ID }) })
  private Set<ResearchGroup> researchGroups = new HashSet<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personNotificationTypes", joinColumns = { @JoinColumn(name = AouConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = AouConstants.DB_NOTIFICATION_TYPE_ID) }, uniqueConstraints = {
          @UniqueConstraint(columnNames = { AouConstants.DB_PERSON_ID, AouConstants.DB_NOTIFICATION_TYPE_ID }) })
  private Set<NotificationType> subscribedNotificationTypes = new HashSet<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personPublications", joinColumns = { @JoinColumn(name = AouConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = AouConstants.DB_PUBLICATION_ID) }, uniqueConstraints = {
          @UniqueConstraint(columnNames = { AouConstants.DB_PERSON_ID, AouConstants.DB_PUBLICATION_ID }) })
  private Set<Publication> favoritePublications = new HashSet<>();

  @JsonIgnore
  @ManyToMany
  @JoinTable(name = "personRoles", joinColumns = { @JoinColumn(name = AouConstants.DB_PERSON_ID) }, inverseJoinColumns = {
          @JoinColumn(name = AouConstants.DB_ROLE_ID) }, uniqueConstraints = {
          @UniqueConstraint(columnNames = { AouConstants.DB_PERSON_ID, AouConstants.DB_ROLE_ID }) })
  private Set<Role> roles = new HashSet<>();

  @Transient
  private String email;

  /****************************************************************/

  public boolean addInstitution(Institution institution) {
    final boolean result = this.institutions.add(institution);
    if (!institution.getPeople().contains(this)) {
      institution.addPerson(this);
    }
    return result;
  }

  public boolean addResearchGroup(ResearchGroup researchGroup) {
    final boolean result = this.researchGroups.add(researchGroup);
    if (!researchGroup.getPeople().contains(this)) {
      researchGroup.addPerson(this);
    }
    return result;
  }

  public boolean addNotificationType(NotificationType notificationType) {
    final boolean result = this.subscribedNotificationTypes.add(notificationType);
    if (!notificationType.getSubscribers().contains(this)) {
      notificationType.addSubscriber(this);
    }
    return result;
  }

  public boolean removeNotificationType(NotificationType notificationType) {
    final boolean result = this.subscribedNotificationTypes.remove(notificationType);
    if (notificationType.getSubscribers().contains(this)) {
      notificationType.removeSubscriber(this);
    }
    return result;
  }

  public boolean addFavoritePublication(Publication publication) {
    final boolean result = this.favoritePublications.add(publication);
    if (!publication.getPeople().contains(this)) {
      publication.addPerson(this);
    }
    return result;
  }

  public boolean removeFavoritePublication(Publication publication) {
    final boolean result = this.favoritePublications.remove(publication);
    if (publication.getPeople().contains(this)) {
      publication.removePerson(this);
    }
    return result;
  }

  public boolean addRole(Role role) {
    return this.roles.add(role);
  }

  public boolean removeRole(Role role) {
    return this.roles.remove(role);
  }

  public boolean addStructure(Structure structure) {
    final boolean result = this.structures.add(structure);
    if (!structure.getPeople().contains(this)) {
      structure.addPerson(this);
    }
    return result;
  }

  @Override
  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    super.addLinks(linkBuilder, mainRes, subResOnly);
    if (mainRes) {
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.INSTITUTIONS).withRel(ResourceName.INSTITUTIONS));
      this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.DOWNLOAD_AVATAR).withRel(AouActionName.DOWNLOAD_AVATAR));
      this.add(linkBuilder.slash(this.getResId()).slash(AouActionName.UPLOAD_AVATAR).withRel(AouActionName.UPLOAD_AVATAR));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.RESEARCH_GROUPS).withRel(ResourceName.RESEARCH_GROUPS));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.STRUCTURES).withRel(ResourceName.STRUCTURES));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.NOTIFICATION_TYPES).withRel(ResourceName.NOTIFICATION_TYPES));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.VALIDATION_RIGHTS_STRUCTURES)
              .withRel(ResourceName.VALIDATION_RIGHTS_STRUCTURES));
      this.add(linkBuilder.slash(this.getResId()).slash(ResourceName.ROLES).withRel(ResourceName.ROLES));
    }
  }

  /************************************************************/

  public String getFirstName() {
    return this.firstName;
  }

  @Override
  public String getFullName() {
    return this.getLastName() + ", " + this.getFirstName();
  }

  @Override
  public Boolean isVerifiedOrcid() {
    return this.verifiedOrcid;
  }

  public Boolean getSyncFromOrcidProfile() {
    return this.syncFromOrcidProfile;
  }

  public Boolean getSyncToOrcidProfile() {
    return this.syncToOrcidProfile;
  }

  public Set<Institution> getInstitutions() {
    return this.institutions;
  }

  public String getLastName() {
    return this.lastName;
  }

  @Override
  public String getOrcid() {
    return this.orcid;
  }

  public OrcidToken getOrcidToken() {
    return this.orcidToken;
  }

  public PersonAvatar getAvatar() {
    return this.avatar;
  }

  public Set<Structure> getStructures() {
    return this.structures;
  }

  @Override
  public Specification<Person> getSearchSpecification(SearchCriteria criteria) {
    return new PersonSearchSpecification(criteria);
  }

  public Set<ResearchGroup> getResearchGroups() {
    return this.researchGroups;
  }

  @JsonIgnore
  public Set<NotificationType> getSubscribedNotifications() {
    return this.subscribedNotificationTypes;
  }

  @JsonIgnore
  public Set<Publication> getFavoritePublications() {
    return this.favoritePublications;
  }

  public Set<Role> getRoles() {
    return this.roles;
  }

  public List<String> getRoleIds() {
    return this.roles.stream().map(Role::getResId).collect(Collectors.toList());
  }

  public List<User> getUsers() {
    return this.users;
  }

  @Override
  public void init() {
    // Do nothing
    if (this.syncFromOrcidProfile == null) {
      this.syncFromOrcidProfile = false;
    }

    if (this.syncToOrcidProfile == null) {
      this.syncToOrcidProfile = false;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ADMIN;
  }

  public boolean removeInstitution(Institution institution) {
    final boolean result = this.institutions.remove(institution);
    if (institution.getPeople().contains(this)) {
      institution.removePerson(this);
    }
    return result;
  }

  public boolean removeResearchGroup(ResearchGroup r) {
    final boolean result = this.researchGroups.remove(r);
    if (r.getPeople().contains(this)) {
      r.removePerson(this);
    }
    return result;
  }

  public boolean removeStructure(Structure structure) {
    final boolean result = this.structures.remove(structure);
    if (structure.getPeople().contains(this)) {
      structure.removePerson(this);
    }
    return result;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @Override
  public void setVerifiedOrcid(Boolean verifiedOrcid) {
    this.verifiedOrcid = verifiedOrcid;
  }

  public void setSyncFromOrcidProfile(Boolean syncFromOrcidProfile) {
    this.syncFromOrcidProfile = syncFromOrcidProfile;
  }

  public void setSyncToOrcidProfile(Boolean syncToOrcidProfile) {
    this.syncToOrcidProfile = syncToOrcidProfile;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  @Override
  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  @Override
  public void setOrcidToken(OrcidToken orcidToken) {
    this.orcidToken = orcidToken;
  }

  public void setAvatar(PersonAvatar avatar) {
    this.avatar = avatar;
  }

  public void setEmail(String creatorEmail) {
    this.email = creatorEmail;
  }

  public String getEmail() {
    return this.email;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("Person [resId=").append(this.getResId())
            .append(", firstName=").append(this.firstName)
            .append(", lastName=").append(this.lastName)
            .append(", orcid=").append(this.orcid).append("]");
    return builder.toString();
  }

  @Override
  public ResourceFile setNewResourceFile() {
    if (this.getAvatar() == null) {
      this.setAvatar(new PersonAvatar());
    }
    return this.getAvatar();
  }

  @Override
  @JsonIgnore
  public ResourceFile getResourceFile() {
    return this.getAvatar();
  }

  @Override
  @JsonIgnore
  public void setResourceFile(ResourceFile resourceFile) {
    this.setAvatar((PersonAvatar) resourceFile);
  }
}
