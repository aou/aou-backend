/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - LicenseGroup.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.Sortable;

/**
 * Structure based on open licenses. http://licenses.opendefinition.org
 */
@Entity
public class LicenseGroup extends AouResourceNormalized implements Sortable {

  @NotNull
  @Column(unique = true)
  private String name;

  /**
   * Property allowing to sort licenses inside groups
   */
  @Column(nullable = false)
  private Integer sortValue;

  @Column(nullable = false)
  private Boolean visible;

  @ElementCollection
  @Column(name = AouConstants.DB_LICENSE_GROUP_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_LICENSE_GROUP_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_LICENSE_GROUP_ID, AouConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  /***********************************************************/

  public String getName() {
    return this.name;
  }

  @Override
  public Integer getSortValue() {
    return this.sortValue;
  }

  public Boolean getVisible() {
    return this.visible;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  /***********************************************************/

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public void setVisible(Boolean visible) {
    this.visible = visible;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }

  /***********************************************************/

  @Override
  public void init() {
    if (this.visible == null) {
      this.visible = true;
    }
  }
}
