/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AouSearchCondition.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.rest;

import ch.unige.solidify.rest.SearchCondition;

import ch.unige.aou.model.search.CriteriaSource;

public class AouSearchCondition extends SearchCondition {

  private CriteriaSource source;

  public CriteriaSource getSource() {
    return this.source;
  }

  public void setSource(CriteriaSource source) {
    this.source = source;
  }

  public static AouSearchCondition fromSearchCondition(SearchCondition searchCondition) {
    if (searchCondition == null) {
      return null;
    }

    if (searchCondition instanceof AouSearchCondition) {
      return (AouSearchCondition) searchCondition;
    }

    AouSearchCondition aouSearchCondition = new AouSearchCondition();
    aouSearchCondition.setType(searchCondition.getType());
    aouSearchCondition.setSearchOperator(searchCondition.getSearchOperator());
    aouSearchCondition.setBooleanClauseType(searchCondition.getBooleanClauseType());
    aouSearchCondition.setField(searchCondition.getField());
    aouSearchCondition.setValue(searchCondition.getValue());
    aouSearchCondition.setUpperValue(searchCondition.getUpperValue());
    aouSearchCondition.setLowerValue(searchCondition.getLowerValue());

    aouSearchCondition.getMultiMatchFields().addAll(searchCondition.getMultiMatchFields());
    aouSearchCondition.getTerms().addAll(searchCondition.getTerms());
    for (SearchCondition subSearchCondition : searchCondition.getNestedConditions()) {
      aouSearchCondition.getNestedConditions().add(AouSearchCondition.fromSearchCondition(subSearchCondition));
    }
    return aouSearchCondition;
  }
}
