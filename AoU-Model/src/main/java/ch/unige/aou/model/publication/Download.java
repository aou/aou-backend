/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Download.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import jakarta.persistence.Entity;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.aou.model.AouResourceNormalized;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "documentFileId", "year", "month" }) },
       indexes = { @Index(columnList = "publicationId") })
public class Download extends AouResourceNormalized {

  @NotNull
  @Size(min = 1)
  private String documentFileId;

  @NotNull
  @Size(min = 1)
  private String publicationId;

  @NotNull
  @Size(min = 1)
  private String filename;

  private Integer downloadNumber;

  @NotNull
  private int year;

  @NotNull
  private int month;

  public String getDocumentFileId() {
    return this.documentFileId;
  }

  public void setDocumentFileId(String documentId) {
    this.documentFileId = documentId;
  }

  public String getPublicationId() {
    return this.publicationId;
  }

  public void setPublicationId(String publicationId) {
    this.publicationId = publicationId;
  }

  public String getFilename() {
    return this.filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public Integer getDownloadNumber() {
    return this.downloadNumber;
  }

  public void setDownloadNumber(Integer downloadNumber) {
    this.downloadNumber = downloadNumber;
  }

  public int getYear() {
    return this.year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public int getMonth() {
    return this.month;
  }

  public void setMonth(int month) {
    this.month = month;
  }
}
