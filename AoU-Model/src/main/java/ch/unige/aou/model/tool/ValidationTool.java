/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ValidationTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.tool;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.unige.solidify.util.StringTool;

public class ValidationTool {

  private static final String DEWEY_CODE_REGEX = "^\\d{1,5}((\\.|-|\\ -\\ |/|\\ /\\ |\\ /)\\d{1,5}){0,8}$";
  private static final String ORCID_ID_REGEX = "^\\d{4}-\\d{4}-\\d{4}-\\d{3}[\\dX]$";
  private static final String ARXIVID_REGEX = "^[\\w./-]+$";
  private static final String URN_REGEX = "^urn:[a-z0-9][a-z0-9-]{0,31}:[a-z0-9()+,\\-.:=@;$_!*'%/?#]+$";
  private static final String ISBN_REGEX = "^[0-9\\-]+[xX]*$";
  private static final String ISSN_REGEX = "^\\d{4}\\-\\d{3}[\\dxX]$";
  private static final String DOI_REGEX = "^10.\\d{4,9}/[-._;()<>/:\\+A-Za-z0-9]+$";
  private static final String SHORT_DOI_REGEX = "^10/[a-zA-Z0-9]+$";
  private static final String JEL_REGEX = "^[A-Z][0-9]+$";
  private static final int MAX_ARXIVID_LENGTH = 25;
  private static final int MIN_YEAR = 1800;

  /**
   * Valid ISSN is made of: 4 number, 1 dash, 3 numbers, 1 number or X (1234-5678 or 0004-567X)
   *
   * @param issn The ISSN code to validate
   * @return
   */
  public static boolean isValidISSN(String issn) {
    return issn.matches(ISSN_REGEX);
  }

  /**
   * Valid DOI is made of: prefix/suffix where prefix is build as 10.NNNN where NNNN are at least four digits greater or
   * equal to 1000.
   *
   * @param doi The DOI code to validate
   * @return
   */
  public static boolean isValidDOI(String doi) {
    if (!StringTool.isNullOrEmpty(doi)) {
      return doi.matches(DOI_REGEX);
    }
    return false;
  }

  /**
   * Valid shortDOI is made of: prefix/suffix where prefix is build as 10/NNNN where NNNN is a short string of alphanumeric
   * characters.
   *
   * @param doi The ShortDOI code to validate
   * @return
   */
  public static boolean isValidShortDOI(String doi) {
    if (!StringTool.isNullOrEmpty(doi)) {
      return doi.matches(SHORT_DOI_REGEX);
    }
    return false;
  }


  /**
   * ISBN contains only digit, dashes and a eventual trailing X (for ISBN-10)
   *
   * @param isbn The ISBN code to validate
   * @return
   */
  public static boolean isValidISBN(String isbn) {
    return isbn.matches(ISBN_REGEX);
  }

  /**
   * Different formats for dates are valid: yyyy or given date format (at the moment: yyyy-MM-dd and yyyy-MM)
   *
   * @param dateStr
   * @param validDateFormats
   * @return True if the date string is valid according to the format
   */
  public static boolean isValidDate(String dateStr, String[] validDateFormats, boolean isValidInFuture) {
    if (StringTool.isNullOrEmpty(dateStr)) {
      return false;
    }
    int currentYear = LocalDate.now().getYear();
    int maxYear = currentYear + 10;

    if (dateStr.length() == 4) {
      // case of year only
      if (dateStr.matches("\\d+")) {
        try {
          int year = Integer.parseInt(dateStr);
          if (isValidInFuture) {
            return (year <= maxYear) && (year > MIN_YEAR);
          } else {
            return (year <= currentYear) && (year > MIN_YEAR);
          }
        } catch (NumberFormatException e) {
          return false;
        }
      } else {
        return false;
      }
    } else {
      // case of full dates
      if (validDateFormats == null) {
        return false;
      }

      DateTimeFormatter formatter;
      for (String pattern : validDateFormats) {
        try {
          formatter = new DateTimeFormatterBuilder()
                  .appendPattern(pattern)
                  .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                  .toFormatter();
          LocalDate date = LocalDate.parse(dateStr, formatter);
          if ((isValidInFuture && date.getYear() <= maxYear)
                  || !isValidInFuture && (date.isBefore(LocalDate.now()) || date.isEqual(LocalDate.now()))) {
            return date.getYear() > MIN_YEAR;
          }
        } catch (DateTimeParseException e) {
          continue;
        }
      }
      return false;
    }
  }

  public static boolean isValidPMID(String pmidString) {
    BigInteger result;
    if (pmidString.matches("\\d+")) {
      try {
        result = new BigInteger(pmidString);
        return !result.equals(BigInteger.ZERO);
      } catch (NumberFormatException e) {
        return false;
      }
    } else {
      return false;
    }
  }

  public static boolean isValidDeweyCode(String dewey) {
    if (!StringTool.isNullOrEmpty(dewey)) {
      return dewey.matches(DEWEY_CODE_REGEX);
    }
    return false;
  }

  public static boolean isValidOrcid(String orcId) {
    if (!StringTool.isNullOrEmpty(orcId)) {
      return orcId.matches(ORCID_ID_REGEX);
    }
    return false;
  }

  public static boolean isValidArxivId(String arxivId) {
    // Very relax validation, to implement a strict one see:
    // https://arxiv.org/help/arxiv_identifier
    if (arxivId == null || arxivId.length() > MAX_ARXIVID_LENGTH) {
      return false;
    }
    Pattern p = Pattern.compile(ARXIVID_REGEX);
    Matcher m = p.matcher(arxivId);
    return m.matches();
  }

  public static boolean isValidURN(String urn) {
    if (urn == null) {
      return false;
    }
    Pattern p = Pattern.compile(URN_REGEX, Pattern.CASE_INSENSITIVE);
    Matcher m = p.matcher(urn);
    return m.matches();
  }

  /**
   * Checking names validity is very difficult, as there are many valid forms of names (e.g: "Jean", "Jean-Blaise", "Martínez-de-la-Casa",
   * "Slof-Op 't Landt")
   * <p>
   * What is sometimes a problem in AoU is the fact that names are entered entirely in upper case, therefore we only validate that there's
   * no occurrence of 3 consecutive letters in upper case.
   *
   * @param name
   * @return
   */
  public static boolean isValidName(String name) {
    if (StringTool.isNullOrEmpty(name)) {
      return false;
    }

    int consecutive = 0;
    for (char c : name.toCharArray()) {
      if (Character.isUpperCase(c)) {
        consecutive++;
        if (consecutive == 3) {
          return false;
        }
      } else {
        consecutive = 0;
      }
    }

    return true;
  }

  /**
   * Vliad JEL code : start with ALPHA followed by numbers ex: G1 , N24
   *
   * @param jelCode the jel code to ba validated
   * @return true if valid JEL code
   */
  public static boolean isValidJEL(String jelCode) {
    return jelCode.matches(JEL_REGEX);
  }
}
