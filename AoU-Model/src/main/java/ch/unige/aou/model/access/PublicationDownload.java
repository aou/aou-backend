/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationDownload.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.access;

import java.net.URI;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ResourceNormalized;

import ch.unige.aou.AouConstants;
import ch.unige.aou.rest.ModuleName;

@Entity
public class PublicationDownload extends ResourceNormalized {

  public enum DownloadStatus {
    IN_PROGRESS, SUBMITTED, IN_PREPARATION, DOWNLOADING, READY, GENERATING_COVER_PAGE, IN_ERROR
  }

  private String name;

  @Enumerated(EnumType.STRING)
  private DownloadStatus status;

  private String publicationId;

  private Long fileSize;

  private String contentType;

  @Column(length = AouConstants.DB_BIG_STRING_LENGTH)
  private URI finalData;

  @Override
  public void init() {
    if (this.status == null) {
      this.status = DownloadStatus.IN_PROGRESS;
    }
  }

  @Override
  public String managedBy() {
    return ModuleName.ACCESS;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setStatus(DownloadStatus status) {
    this.status = status;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public void setContentType(String mimetype) {
    this.contentType = mimetype;
  }

  public void setFinalData(URI finalData) {
    this.finalData = finalData;
  }

  public void setPublicationId(String publicationId) {
    this.publicationId = publicationId;
  }

  public Long getFileSize() {
    return this.fileSize;
  }

  public DownloadStatus getStatus() {
    return this.status;
  }

  @Override
  public String getContentType() {
    return this.contentType;
  }

  public URI getFinalData() {
    return this.finalData;
  }

  public String getPublicationId() {
    return this.publicationId;
  }

  public String getName() {
    return this.name;
  }

  @Override
  public URI getDownloadUri() {
    if (!this.getStatus().equals(DownloadStatus.READY)) {
      throw new SolidifyRuntimeException("Document file not available");
    }
    if (this.getFinalData() == null) {
      throw new SolidifyRuntimeException("Document file not accessible");
    }
    return this.getFinalData();
  }

}
