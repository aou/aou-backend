/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - UnigePersonDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.rest;

import java.io.Serializable;
import java.util.Locale;

import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.solidify.util.StringTool;

public class UnigePersonDTO extends PersonDTO implements Serializable {

  private String cnIndividu;
  private Integer yearOfBirth;
  private String function;
  private String structure;
  private Boolean isEmployee = false;

  public UnigePersonDTO(String cnIndividu, String firstname, String lastname, Integer yearOfBirth, String function, String structure,
          String email) {
    this.setFirstname(firstname);
    this.setLastname(lastname);
    this.setEmail(email);
    this.cnIndividu = cnIndividu;
    this.yearOfBirth = yearOfBirth;
    this.function = function;
    this.structure = structure;
  }

  public UnigePersonDTO(JsonNode node) {
    this.setFirstname(this.getValueFromEditedField(node, "firstname", "firstnameEdited", "usualFirstname"));
    this.setLastname(this.getValueFromEditedField(node, "lastname", "lastnameEdited", "usualLastname"));
    this.cnIndividu = this.getValue(node, ("entityId"));
    this.function = this.getValue(node, "function");
    this.structure = this.getValue(node, "structure");
    this.isEmployee = this.getCollabValue(node);
    final String birthDate = this.getValue(node, "birth");
    if (birthDate != null) {
      this.yearOfBirth = Integer.parseInt(birthDate.substring(0, 4));
    }
    String email = this.getValue(node, "employeeInstEmail");
    if (StringTool.isNullOrEmpty(email)) {
      email = this.getValue(node, "studentInstEmail");
    }
    this.setEmail(email);
  }

  public String getCnIndividu() {
    return this.cnIndividu;
  }

  public Integer getYearOfBirth() {
    return this.yearOfBirth;
  }

  public String getFunction() {
    return this.function;
  }

  public String getStructure() {
    return this.structure;
  }

  public Boolean isEmployee() {
    return this.isEmployee;
  }

  public void setCnIndividu(String cnIndividu) {
    this.cnIndividu = cnIndividu;
  }

  public void setYearOfBirth(Integer yearOfBirth) {
    this.yearOfBirth = yearOfBirth;
  }

  public void setFunction(String function) {
    this.function = function;
  }

  public void setStructure(String structure) {
    this.structure = structure;
  }

  public void setIsEmployee(Boolean isEmployee) {
    this.isEmployee = isEmployee;
  }

  private String getValue(JsonNode mainNode, String fieldName) {
    final JsonNode fieldNode = mainNode.get(fieldName);
    if (fieldNode == null) {
      return null;
    }
    return fieldNode.textValue();
  }

  @SuppressWarnings("java:S2447")
  private Boolean getCollabValue(JsonNode node) {
    var indicators = node.get("indicators");
    if (indicators != null) {
      var isCollab = indicators.get("isCollab");
      if (isCollab != null) {
        return isCollab.asBoolean();
      }
    }
    return null;
  }

  private String getValueFromEditedField(JsonNode node, String fieldName, String editedFieldName, String usualFieldName) {
    final String usualValue = this.getValue(node, usualFieldName);
    if (!StringTool.isNullOrEmpty(usualValue)) {
      return usualValue;
    }
    final String editedValue = this.getValue(node, editedFieldName);
    if (!StringTool.isNullOrEmpty(editedValue)) {
      return editedValue;
    }
    String value = this.getValue(node, fieldName);
    if (value == null) {
      return null;
    }
    value = value.trim().replace("\\s+", " ").toLowerCase(Locale.ROOT);
    return this.capitalize(value);
  }

  private String capitalize(String value) {
    StringBuilder result = new StringBuilder();
    boolean separatorFound = false;
    for (int i = 0; i < value.length(); i++) {
      if ((value.charAt(i) == ' ' || value.charAt(i) == '-') && i < value.length() - 1) {
        separatorFound = true;
        result.append(value.charAt(i));
      } else if (i == 0 || separatorFound) {
        separatorFound = false;
        result.append(Character.toUpperCase(value.charAt(i)));
      } else {
        result.append(value.charAt(i));
      }
    }
    return result.toString();
  }

}
