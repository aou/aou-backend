/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AdvancedSearchCriteria.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.search;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AdvancedSearchCriteria implements Serializable {

  private static final long serialVersionUID = 831316416135184911L;

  public enum BuildType {
    NONE,           // no treatment to do when building the advanced search criteria list
    DOCUMENT_TYPE   // get list of documents types as selectable values
  }

  public enum CriteriaType {
    FREE_TEXT_TERM,           // free text value, search as SearchConditionType.TERM
    FREE_TEXT_MATCH,          // free text value, search as SearchConditionType.MATCH
    FREE_TEXT_WILDCARD,       // free text value, search as SearchConditionType.WILDCARD
    ENUM_SINGLE,              // closed list of values, only one can be selected
    ENUM_MULTIPLE,            // closed list of values, many can be selected
    INTERVAL_YEAR,            // range of years
    STRUCTURE,                // one academic structure
    STRUCTURE_SUB_STRUCTURES, // one structure and its sub structures
    RESEARCH_GROUP,           // research groups
    CONTRIBUTOR_UNIGE,        // UNIGE contributors
    CONTRIBUTOR_ALL,          // All contributors (UNIGE or not)
  }

  public static class EnumValue implements Serializable {

    private static final long serialVersionUID = 83177896135184911L;

    private String value;
    private List<Label> labels = new ArrayList<>();

    public String getValue() {
      return this.value;
    }

    public void setValue(String value) {
      this.value = value;
    }

    public List<Label> getLabels() {
      return this.labels;
    }

    public void setLabels(List<Label> labels) {
      this.labels = labels;
    }
  }

  public static class Label implements Serializable {

    private static final long serialVersionUID = 6589746416135184911L;

    private String text;
    private String languageCode;

    public String getText() {
      return this.text;
    }

    public void setText(String text) {
      this.text = text;
    }

    public String getLanguageCode() {
      return this.languageCode;
    }

    public void setLanguageCode(String languageCode) {
      this.languageCode = languageCode;
    }
  }

  private String alias;
  private List<Label> labels = new ArrayList<>();
  private CriteriaType type;
  @JsonIgnore
  private BuildType buildType = BuildType.NONE;
  private List<EnumValue> selectableValues = new ArrayList<>();

  public String getAlias() {
    return this.alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }

  public CriteriaType getType() {
    return this.type;
  }

  public void setType(CriteriaType type) {
    this.type = type;
  }

  public BuildType getBuildType() {
    return this.buildType;
  }

  public void setBuildType(BuildType buildType) {
    this.buildType = buildType;
  }

  public List<EnumValue> getSelectableValues() {
    return this.selectableValues;
  }

  public void setSelectableValues(List<EnumValue> selectableValues) {
    this.selectableValues = selectableValues;
  }
}
