/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - License.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.jpa.domain.Specification;

import ch.unige.solidify.util.SearchCriteria;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouSearchableResourceNormalized;
import ch.unige.aou.model.Sortable;
import ch.unige.aou.specification.LicenseSearchSpecification;

/**
 * Structure based on open licenses. http://licenses.opendefinition.org
 */
@Entity
public class License extends AouSearchableResourceNormalized<License> implements Sortable {

  @ManyToOne
  @NotNull
  private LicenseGroup licenseGroup;

  @NotNull
  @Column(unique = true, nullable = false)
  private String openLicenseId;

  @NotNull
  @Column(unique = true)
  private String title;

  private String version;

  /**
   * Property allowing to sort licenses
   */
  @Column(nullable = false)
  private Integer sortValue;

  private URL url;

  @Column(nullable = false)
  private Boolean visible;

  @ElementCollection
  @Column(name = AouConstants.DB_LICENSE_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_LICENSE_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_LICENSE_ID, AouConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  /***********************************************************/

  public LicenseGroup getLicenseGroup() {
    return this.licenseGroup;
  }

  public String getOpenLicenseId() {
    return this.openLicenseId;
  }

  public String getTitle() {
    return this.title;
  }

  public String getVersion() {
    return this.version;
  }

  @Override
  public Integer getSortValue() {
    return this.sortValue;
  }

  public URL getUrl() {
    return this.url;
  }

  public Boolean getVisible() {
    return this.visible;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  /***********************************************************/

  public void setLicenseGroup(LicenseGroup licenseGroup) {
    this.licenseGroup = licenseGroup;
  }

  public void setOpenLicenseId(String openLicenseId) {
    this.openLicenseId = openLicenseId;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  @Override
  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public void setUrl(URL url) {
    this.url = url;
  }

  public void setVisible(Boolean visible) {
    this.visible = visible;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }

  /***********************************************************/

  @Override
  public void init() {
    if (this.visible == null) {
      this.visible = true;
    }
  }

  @Override
  public Specification<License> getSearchSpecification(SearchCriteria criteria) {
    return new LicenseSearchSpecification(criteria);
  }
}
