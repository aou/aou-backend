/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - BibliographyParameters.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.bibliography;

import java.util.Locale;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;

public class BibliographyParameters implements Cloneable {

  public enum SortType {
    AUTHORS,
    TITLE,
    YEAR,
    VALIDATION_DATE
  }

  public enum Lang {
    ENGLISH(AouConstants.LANG_CODE_ENGLISH, "en-GB", Locale.ENGLISH),
    FRENCH(AouConstants.LANG_CODE_FRENCH, "fr-FR", Locale.FRENCH),
    SPANISH(AouConstants.LANG_CODE_SPANISH, "es-ES", new Locale("ES")),
    GERMAN(AouConstants.LANG_CODE_GERMAN, "de-DE", Locale.GERMAN);

    private final String code;
    private final String cslCode;
    private final Locale locale;

    Lang(String code, String cslCode, Locale locale) {
      this.code = code;
      this.cslCode = cslCode;
      this.locale = locale;
    }

    public String getCslCode() {
      return this.cslCode;
    }

    public String getCode() {
      return this.code;
    }

    public Locale getLocale() {
      return this.locale;
    }
  }

  private String uid = StringTool.generateResId().replace("-", "_");

  private Lang lang = Lang.FRENCH;

  private Integer fromYear;
  private Integer toYear;
  private Integer years;

  private int limit = 1000;

  private String format;

  private SortType sort = SortType.YEAR;

  private boolean groupByType = false;
  private boolean groupBySubtype = false;
  private boolean groupByYear = false;

  private boolean linkOnAllText = false;
  private boolean linkTargetBlank = false;

  private boolean withAbstract = false;
  private int abstractLength = 1000;
  private boolean withThumbnail = false;
  private boolean withAccessLevel = false;

  public String getUid() {
    return this.uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public Lang getLang() {
    return this.lang;
  }

  public void setLang(Lang lang) {
    this.lang = lang;
  }

  public void setLang(String langCode) {
    if (!StringTool.isNullOrEmpty(langCode)) {
      switch (langCode.toLowerCase()) {
        case "fr":
        case "fre":
        case "fr-fr":
        case "fr-ch":
          this.lang = Lang.FRENCH;
          break;
        case "es":
        case "es-es":
        case "es-cl":
        case "es-mx":
        case "spa":
          this.lang = Lang.SPANISH;
          break;
        case "de":
        case "de-at":
        case "de-de":
        case "de-li":
        case "de-lu":
        case "de-ch":
          this.lang = Lang.GERMAN;
          break;
        default:
          this.lang = Lang.ENGLISH;
          break;
      }
    }
  }

  public Integer getFromYear() {
    return this.fromYear;
  }

  public void setFromYear(Integer fromYear) {
    this.fromYear = fromYear;
  }

  public Integer getToYear() {
    return this.toYear;
  }

  public void setToYear(Integer toYear) {
    this.toYear = toYear;
  }

  public Integer getYears() {
    return this.years;
  }

  public void setYears(Integer years) {
    this.years = years;
  }

  public int getLimit() {
    return this.limit;
  }

  public void setLimit(int limit) {
    this.limit = limit;
  }

  public String getFormat() {
    return this.format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public SortType getSort() {
    return this.sort;
  }

  public void setSort(SortType sort) {
    this.sort = sort;
  }

  public boolean isGroupByType() {
    return this.groupByType;
  }

  public void setGroupByType(boolean groupByType) {
    this.groupByType = groupByType;
  }

  public boolean isGroupBySubtype() {
    return this.groupBySubtype;
  }

  public void setGroupBySubtype(boolean groupBySubtype) {
    this.groupBySubtype = groupBySubtype;
  }

  public boolean isGroupByYear() {
    return this.groupByYear;
  }

  public void setGroupByYear(boolean groupByYear) {
    this.groupByYear = groupByYear;
  }

  public boolean isLinkOnAllText() {
    return this.linkOnAllText;
  }

  public void setLinkOnAllText(boolean linkOnAllText) {
    this.linkOnAllText = linkOnAllText;
  }

  public boolean isLinkTargetBlank() {
    return this.linkTargetBlank;
  }

  public void setLinkTargetBlank(boolean linkTargetBlank) {
    this.linkTargetBlank = linkTargetBlank;
  }

  public boolean isWithAbstract() {
    return this.withAbstract;
  }

  public void setWithAbstract(boolean withAbstract) {
    this.withAbstract = withAbstract;
  }

  public int getAbstractLength() {
    return this.abstractLength;
  }

  public void setAbstractLength(int abstractLength) {
    this.abstractLength = abstractLength;
  }

  public boolean isWithThumbnail() {
    return this.withThumbnail;
  }

  public void setWithThumbnail(boolean withThumbnail) {
    this.withThumbnail = withThumbnail;
  }

  public boolean isWithAccessLevel() {
    return this.withAccessLevel;
  }

  public void setWithAccessLevel(boolean withAccessLevel) {
    this.withAccessLevel = withAccessLevel;
  }

  @Override
  public BibliographyParameters clone() {
    BibliographyParameters clone = new BibliographyParameters();
    clone.setLang(this.getLang());
    clone.setFromYear(this.getFromYear());
    clone.setToYear(this.getFromYear());
    clone.setYears(this.getYears());
    clone.setLimit(this.getLimit());
    clone.setFormat(this.getFormat());
    clone.setSort(this.getSort());
    clone.setGroupByType(this.isGroupByType());
    clone.setGroupByYear(this.isGroupByYear());
    clone.setLinkOnAllText(this.isLinkOnAllText());
    clone.setLinkTargetBlank(this.isLinkTargetBlank());
    clone.setWithAbstract(this.isWithAbstract());
    clone.setWithThumbnail(this.isWithThumbnail());
    clone.setWithAccessLevel(this.isWithAccessLevel());
    return clone;
  }
}
