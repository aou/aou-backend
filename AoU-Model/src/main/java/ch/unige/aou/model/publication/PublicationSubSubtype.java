/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubSubtype.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.settings.Label;

@Entity
public class PublicationSubSubtype extends AouResourceNormalized {

  @ManyToOne(targetEntity = PublicationSubtype.class)
  @NotNull
  private PublicationSubtype publicationSubtype;

  /**
   * SubSubtype name (e.g: Article, Article de données, Article Vidéo...)
   */
  @Size(min = 1)
  @NotNull
  @Column(nullable = false)
  private String name;

  /**
   * Property allowing to sort types in lists
   */
  @Column(nullable = false)
  private Integer sortValue;

  /**
   * Property allowing to sort subtypes in lists
   */
  @Column(nullable = false)
  private Integer bibliographySortValue;

  @ElementCollection
  @Column(name = AouConstants.DB_PUBLICATION_SUB_SUBTYPE_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_PUBLICATION_SUB_SUBTYPE_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_PUBLICATION_SUB_SUBTYPE_ID,
                           AouConstants.DB_LANGUAGE_ID }))
  private List<Label> descriptions = new ArrayList<>();

  @ElementCollection
  @Column(name = AouConstants.DB_PUBLICATION_SUB_SUBTYPE_ID)
  @CollectionTable(joinColumns = { @JoinColumn(name = AouConstants.DB_PUBLICATION_SUB_SUBTYPE_ID) },
                   uniqueConstraints = @UniqueConstraint(columnNames = { AouConstants.DB_PUBLICATION_SUB_SUBTYPE_ID,
                           AouConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  /****************************************************************/

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSortValue() {
    return this.sortValue;
  }

  public void setSortValue(Integer sortValue) {
    this.sortValue = sortValue;
  }

  public Integer getBibliographySortValue() {
    return this.bibliographySortValue;
  }

  public void setBibliographySortValue(Integer bibliographySortValue) {
    this.bibliographySortValue = bibliographySortValue;
  }

  public PublicationSubtype getPublicationSubtype() {
    return this.publicationSubtype;
  }

  public void setPublicationSubtype(PublicationSubtype publicationSubtype) {
    this.publicationSubtype = publicationSubtype;
  }

  public List<Label> getDescriptions() {
    return this.descriptions;
  }

  public void setDescriptions(List<Label> descriptions) {
    this.descriptions = descriptions;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }
}
