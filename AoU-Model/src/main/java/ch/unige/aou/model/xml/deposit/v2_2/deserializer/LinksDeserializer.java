/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - LinksDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_2.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v2_2.Link;
import ch.unige.aou.model.xml.deposit.v2_2.LinkTypes;
import ch.unige.aou.model.xml.deposit.v2_2.Links;
import ch.unige.aou.model.xml.deposit.v2_2.Text;

/**
 * "links": [
 * {
 * "description": {
 * "lang": "FR",
 * "text": "remplace"
 * },
 * "target": "unige:34567",
 * "type": "INTERNAL"
 * }
 * ],
 */

public class LinksDeserializer extends AouStdDeserializer<Links> {

  private static final long serialVersionUID = 659865L;

  @Override
  public Links deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    if (jsonNode instanceof ArrayNode) {
      if (jsonNode.isEmpty()) {
        return null;
      }

      Links links = new Links();
      for (JsonNode linkNode : jsonNode) {
        JsonNode descriptionNode = linkNode.get("description");
        JsonNode targetNode = linkNode.get("target");
        JsonNode typeNode = linkNode.get("type");

        //if list contain one element and all properties of this only element are empty we ignore the list
        if (jsonNode.size() == 1 && !this.nodeHasValue(descriptionNode) && !this.nodeHasValue(targetNode) && !this.nodeHasValue(typeNode)) {
          return null;
        }

        Link link = new Link();

        if (descriptionNode != null) {
          Text description = codec.treeToValue(descriptionNode, Text.class);
          link.setDescription(description);
        }

        if (this.nodeHasTextValue(targetNode)) {
          link.setTarget(targetNode.textValue());
        }

        if (this.nodeHasTextValue(typeNode)) {
          link.setType(LinkTypes.fromValue(typeNode.textValue()));
        }

        links.getLink().add(link);
      }
      return links;

    } else {
      throw new SolidifyRuntimeException("links is not an array");
    }
  }
}
