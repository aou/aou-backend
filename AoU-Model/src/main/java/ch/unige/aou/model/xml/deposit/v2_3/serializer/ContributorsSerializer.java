/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContributorsSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_3.serializer;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.unige.aou.model.xml.deposit.v2_3.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_3.Contributor;
import ch.unige.aou.model.xml.deposit.v2_3.Contributors;

public class ContributorsSerializer extends StdSerializer<Contributors> {
  public ContributorsSerializer() {
    this(null);
  }

  public ContributorsSerializer(Class<Contributors> t) {
    super(t);
  }

  @Override
  public void serialize(Contributors value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
    jgen.writeStartArray();
    for (Serializable serializable : value.getContributorOrCollaboration()) {
      jgen.writeStartObject();
      if (serializable instanceof Contributor) {
        jgen.writeObjectField("contributor", serializable);
      } else if (serializable instanceof Collaboration) {
        jgen.writeObjectField("collaboration", serializable);
      }
      jgen.writeEndObject();
    }
    jgen.writeEndArray();
  }
}
