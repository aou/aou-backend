/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - Event.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.notification;

import java.util.ArrayList;
import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Person;

@Entity
public class Event extends AouResourceNormalized {

  @NotNull
  @ManyToOne
  @JoinColumn(name = AouConstants.DB_EVENT_TYPE_ID)
  private EventType eventType;

  @NotNull
  @ManyToOne
  @JoinColumn(name = AouConstants.DB_PUBLICATION_ID)
  private Publication publication;

  @ManyToOne
  private Person triggerBy;

  @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
  @JsonIgnore
  private List<Notification> notifications = new ArrayList<>();

  private String message;

  public EventType getEventType() {
    return this.eventType;
  }

  public Publication getPublication() {
    return this.publication;
  }

  public String getMessage() {
    return this.message;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public void setPublication(Publication publication) {
    this.publication = publication;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Person getTriggerBy() {
    return this.triggerBy;
  }

  public void setTriggerBy(Person emitter) {
    this.triggerBy = emitter;
  }

  public List<Notification> getNotifications() {
    return this.notifications;
  }

  public void setNotifications(List<Notification> notifications) {
    this.notifications = notifications;
  }
}
