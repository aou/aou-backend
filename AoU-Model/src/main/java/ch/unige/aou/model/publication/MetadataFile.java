/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - MetadataFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class MetadataFile {
  private String resId;
  private String type;
  private String name;
  private BigInteger size;
  private String checksum;
  private String mimeType;
  private String license;
  private String label;
  private String accessLevel;
  private MetadataEmbargo embargo;
  private String licenseTitle;
  private String licenseUrl;
  private String typeLevel;

  public String getResId() {
    return this.resId;
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigInteger getSize() {
    return this.size;
  }

  public void setSize(BigInteger size) {
    this.size = size;
  }

  public String getChecksum() {
    return this.checksum;
  }

  public void setChecksum(String checksum) {
    this.checksum = checksum;
  }

  public String getMimeType() {
    return this.mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  public String getLicense() {
    return this.license;
  }

  public void setLicense(String license) {
    this.license = license;
  }

  public String getLabel() {
    return this.label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getAccessLevel() {
    return this.accessLevel;
  }

  public void setAccessLevel(String accessLevel) {
    this.accessLevel = accessLevel;
  }

  public MetadataEmbargo getEmbargo() {
    return this.embargo;
  }

  public void setEmbargo(MetadataEmbargo embargo) {
    this.embargo = embargo;
  }

  public String getLicenseTitle() {
    return this.licenseTitle;
  }

  public void setLicenseTitle(String licenseTitle) {
    this.licenseTitle = licenseTitle;
  }

  public String getLicenseUrl() {
    return this.licenseUrl;
  }

  public void setLicenseUrl(String licenseUrl) {
    this.licenseUrl = licenseUrl;
  }

  public String getTypeLevel() {
    return this.typeLevel;
  }

  public void setTypeLevel(String typeLevel) {
    this.typeLevel = typeLevel;
  }

  public String getEmbargoAccessLevel() {
    if (this.embargo != null && this.embargo.getAccessLevel() != null) {
      return this.embargo.getAccessLevel();
    }
    return null;
  }

  public LocalDate getEmbargoEndDate() {
    if (this.embargo != null && this.embargo.getEndDate() != null) {
      return this.embargo.getEndDate();
    }
    return null;
  }

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public String getCurrentAccessLevel() {

    if (this.embargo != null) {
      if (this.embargo.getEndDate() != null) {
        if (this.embargo.getEndDate().isAfter(LocalDate.now())) {
          // file is still under embargo
          return this.embargo.getAccessLevel();
        }
      } else {
        throw new SolidifyRuntimeException(
                "DocumentFile " + this.getResId() + " has an embargo access level but no embargo end date is defined");
      }
    }

    return this.accessLevel;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof MetadataFile))
      return false;
    final MetadataFile castOther = (MetadataFile) o;
    return Objects.equals(this.type, castOther.type) && Objects.equals(this.name, castOther.name)
            && Objects.equals(this.size, castOther.size) && Objects.equals(this.mimeType, castOther.mimeType)
            && Objects.equals(this.license, castOther.license) && Objects.equals(this.label, castOther.label)
            && Objects.equals(this.accessLevel, castOther.accessLevel) && Objects.equals(this.embargo, castOther.embargo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.resId, this.type, this.name, this.size, this.checksum, this.mimeType, this.license, this.label, this.accessLevel,
            this.embargo, this.licenseTitle, this.licenseUrl, this.typeLevel);
  }
}
