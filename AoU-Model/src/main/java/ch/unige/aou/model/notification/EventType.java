/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - EventType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.notification;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;

import ch.unige.aou.model.AouResourceNormalized;

@Entity
public class EventType extends AouResourceNormalized {

  public static final String PUBLICATION_TO_VALIDATE_ID = "PUBLICATION_TO_VALIDATE";
  public static final EventType PUBLICATION_TO_VALIDATE = new EventType(PUBLICATION_TO_VALIDATE_ID, "Publication to validate");

  public static final String PUBLICATION_CREATED_ID = "PUBLICATION_CREATED";
  public static final EventType PUBLICATION_CREATED = new EventType(PUBLICATION_CREATED_ID, "Publication created");

  public static final String PUBLICATION_SUBMITTED_ID = "PUBLICATION_SUBMITTED";
  public static final EventType PUBLICATION_SUBMITTED = new EventType(PUBLICATION_SUBMITTED_ID, "Publication submitted");

  public static final String PUBLICATION_VALIDATED_ID = "PUBLICATION_VALIDATED";
  public static final EventType PUBLICATION_VALIDATED = new EventType(PUBLICATION_VALIDATED_ID, "Publication validated");

  public static final String PUBLICATION_REJECTED_ID = "PUBLICATION_REJECTED";
  public static final EventType PUBLICATION_REJECTED = new EventType(PUBLICATION_REJECTED_ID, "Publication rejected");

  public static final String COMMENT_IN_PUBLICATION_ID = "COMMENT_IN_PUBLICATION";
  public static final EventType COMMENT_IN_PUBLICATION = new EventType(COMMENT_IN_PUBLICATION_ID, "Comment in publication");

  public static final String COMMENT_FOR_VALIDATORS_IN_PUBLICATION_ID = "COMMENT_FOR_VALIDATORS_IN_PUBLICATION";
  public static final EventType COMMENT_FOR_VALIDATORS_IN_PUBLICATION = new EventType(COMMENT_FOR_VALIDATORS_IN_PUBLICATION_ID,
          "Comment for validators in publication");

  public static final String PUBLICATION_FEEDBACK_REQUIRED_ID = "PUBLICATION_FEEDBACK_REQUIRED";
  public static final EventType PUBLICATION_FEEDBACK_REQUIRED = new EventType(PUBLICATION_FEEDBACK_REQUIRED_ID, "Publication requires feedback");

  public static final String PUBLICATION_DETECTED_ON_ORCID_ID = "PUBLICATION_DETECTED_ON_ORCID";
  public static final EventType PUBLICATION_DETECTED_ON_ORCID = new EventType(PUBLICATION_DETECTED_ON_ORCID_ID, "Publication detected on ORCID");

  public static final String PUBLICATION_EXPORTED_TO_ORCID_ID = "PUBLICATION_EXPORTED_TO_ORCID";
  public static final EventType PUBLICATION_EXPORTED_TO_ORCID = new EventType(PUBLICATION_EXPORTED_TO_ORCID_ID, "Publication exported to ORCID");

  public static final String PUBLICATION_UPDATED_IN_BATCH_ID = "PUBLICATION_UPDATED_IN_BATCH";
  public static final EventType PUBLICATION_UPDATED_IN_BATCH = new EventType(PUBLICATION_UPDATED_IN_BATCH_ID, "Publication updated in batch");

  @NotNull
  @Column(unique = true)
  private String name;

  public EventType() {
    //no-arg constructor
  }

  public EventType(String resId, String name) {
    this.setResId(resId);
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof EventType))
      return false;
    final EventType e = (EventType) o;
    return this.getResId().equals(e.getResId()) &&
            Objects.equals(this.name, e.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name);
  }

  public static List<EventType> getAllEventTypes() {
    return Arrays.asList(PUBLICATION_CREATED, PUBLICATION_TO_VALIDATE, PUBLICATION_SUBMITTED, PUBLICATION_VALIDATED, PUBLICATION_REJECTED,
            COMMENT_IN_PUBLICATION,
            COMMENT_FOR_VALIDATORS_IN_PUBLICATION, PUBLICATION_FEEDBACK_REQUIRED, PUBLICATION_DETECTED_ON_ORCID, PUBLICATION_EXPORTED_TO_ORCID,
            PUBLICATION_UPDATED_IN_BATCH);
  }

}
