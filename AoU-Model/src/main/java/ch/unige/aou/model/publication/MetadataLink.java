/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - MetadataLink.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.Objects;

public class MetadataLink {

  public enum LinkType {INTERNAL, WEB}

  String target;
  MetadataTextLang description;
  LinkType type;

  public String getTarget() {
    return this.target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public MetadataTextLang getDescription() {
    return this.description;
  }

  public void setDescription(MetadataTextLang description) {
    this.description = description;
  }

  public LinkType getType() {
    return this.type;
  }

  public void setType(LinkType type) {
    this.type = type;
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof MetadataLink)) {
      return false;
    }
    final MetadataLink castOther = (MetadataLink) other;
    return Objects.equals(this.target, castOther.target) && Objects.equals(this.type, castOther.type)
            && Objects.equals(this.description, castOther.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.target, this.description, this.type);
  }
}
