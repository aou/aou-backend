/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContributorOtherName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.Objects;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.AouResourceNormalized;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = { AouConstants.DB_CONTRIBUTOR_ID, AouConstants.DB_FIRST_NAME, AouConstants.DB_LAST_NAME }) })
public class ContributorOtherName extends AouResourceNormalized {

  @ManyToOne(targetEntity = Contributor.class)
  @JoinColumn(name = AouConstants.DB_CONTRIBUTOR_ID, referencedColumnName = AouConstants.DB_RES_ID, nullable = false)
  @JsonIgnore
  private Contributor contributor;

  private String firstName;

  @NotNull
  @Size(min = 1)
  private String lastName;

  public Contributor getContributor() {
    return this.contributor;
  }

  public void setContributor(Contributor contributor) {
    this.contributor = contributor;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getFullName() {
    String fullName = this.lastName;
    if (!StringTool.isNullOrEmpty(this.firstName)) {
      fullName = fullName + ", " + this.firstName;
    }
    return fullName;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof ContributorOtherName)) {
      return false;
    }
    final ContributorOtherName that = (ContributorOtherName) o;
    return this.getResId().equals(that.getResId()) &&
            Objects.equals(this.contributor.getResId(), that.contributor.getResId()) &&
            Objects.equals(this.getFirstName(), that.getFirstName()) &&
            Objects.equals(this.getLastName(), that.getLastName());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.contributor, this.firstName, this.lastName);
  }

  @Override
  @Transient
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    final CacheMessage cacheMessage = super.getCacheMessage();
    if (this.contributor != null) {
      return this.contributor.getCacheMessage();
    }
    return cacheMessage;
  }
}
