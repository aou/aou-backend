/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - TaskType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.schedule;

public enum TaskType {
  NOTIFY_DEPOSITS_TO_VALIDATE, NOTIFY_VALIDATED_DEPOSITS_DEPOSITOR, NOTIFY_VALIDATED_DEPOSITS_CONTRIBUTORS, NOTIFY_DEPOSITS_REJECTED,
  NOTIFY_VALIDATORS_ON_COMMENTS, NOTIFY_CONTRIBUTORS_ON_COMMENTS, NOTIFY_DEPOSITORS_ON_COMMENTS, NOTIFY_FORGOTTEN_DEPOSITS_TO_VALIDATE,
  NOTIFY_FORGOTTEN_DEPOSITS_IN_PROGRESS, SYNCHRONIZE_RESEARCH_GROUPS, NEW_RESEARCH_GROUPS_TO_VALIDATE, REINDEX_PUBLICATIONS_ENDING_EMBARGO,
  SYNC_THESES_TO_SWISS_NATIONAL_LIBRARY, SYNCHRONIZE_ORCID_PROFILE, CLEAN_DEPOSIT_UPDATED_WITHOUT_CHANGES
}
