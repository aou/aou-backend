/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DepositIdentifiersSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_2.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.unige.aou.model.xml.deposit.v2_2.DepositIdentifiers;

public class DepositIdentifiersSerializer extends StdSerializer<DepositIdentifiers> {
  public DepositIdentifiersSerializer() {
    this(null);
  }

  public DepositIdentifiersSerializer(Class<DepositIdentifiers> t) {
    super(t);
  }

  @Override
  public void serialize(DepositIdentifiers value, JsonGenerator jgen, SerializerProvider provider)
          throws IOException, JsonProcessingException {
    jgen.writeStartObject();
    jgen.writeStringField("arxiv", value.getArxiv());
    jgen.writeStringField("dblp", value.getDblp());
    jgen.writeObjectField("doi", value.getDoi());
    jgen.writeStringField("isbn", value.getIsbn());
    jgen.writeStringField("issn", value.getIssn());
    jgen.writeStringField("localNumber", value.getLocalNumber());
    String pmid = null;
    if (value.getPmid() != null) {
      pmid = value.getPmid().toString();
    }
    jgen.writeStringField("pmid", pmid);
    jgen.writeStringField("repec", value.getRepec());
    jgen.writeStringField("mmsid", value.getMmsid());
    jgen.writeStringField("urn", value.getUrn());
    jgen.writeStringField("pmcid", value.getPmcid());
    jgen.writeEndObject();
  }
}
