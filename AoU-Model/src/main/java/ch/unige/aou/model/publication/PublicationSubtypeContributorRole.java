/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeContributorRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.Objects;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.rest.JoinResource;

@Entity
public class PublicationSubtypeContributorRole extends JoinResource<PublicationSubtypeContributorRolePkId> {

  public final static String PATH_TO_CONTRIBUTOR_ROLE = "compositeKey.contributorRole";
  public final static String PATH_TO_PUBLICATION_SUBTYPE = "compositeKey.publicationSubtype";

  @EmbeddedId
  @JsonIgnore
  private PublicationSubtypeContributorRolePkId compositeKey = new PublicationSubtypeContributorRolePkId();

  /**
   * Property allowing to sort file types in lists
   */
  @Column(nullable = false)
  private int sortValue;

  /**
   * Property allowing to sort file types in lists
   */
  @Column(nullable = false)
  private Boolean visible;

  /****************************************************************/

  public PublicationSubtype getPublicationSubtype() {
    return this.compositeKey.getPublicationSubtype();
  }

  public void setPublicationSubtype(PublicationSubtype publicationSubtype) {
    this.compositeKey.setPublicationSubtype(publicationSubtype);
  }

  public ContributorRole getContributorRole() {
    return this.compositeKey.getContributorRole();
  }

  public void setContributorRole(ContributorRole contributorRole) {
    this.compositeKey.setContributorRole(contributorRole);
  }

  public int getSortValue() {
    return this.sortValue;
  }

  public void setSortValue(int sortValue) {
    this.sortValue = sortValue;
  }

  public Boolean getVisible() {
    return visible;
  }

  public void setVisible(Boolean visible) {
    this.visible = visible;
  }

  /****************************************************************/

  @Override
  public PublicationSubtypeContributorRolePkId getCompositeKey() {
    return this.compositeKey;
  }

  @Override
  public void setCompositeKey(PublicationSubtypeContributorRolePkId compositeKey) {
    this.compositeKey = compositeKey;
  }

  /*************************************************************************************/

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;

    PublicationSubtypeContributorRole that = (PublicationSubtypeContributorRole) o;

    return Objects.equals(this.getPublicationSubtype(), that.getPublicationSubtype()) &&
            Objects.equals(this.getContributorRole(), that.getContributorRole());
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + this.getPublicationSubtype().hashCode();
    result = 31 * result + this.getContributorRole().hashCode();
    return result;
  }
}
