/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - FundingsDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_2.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v2_2.Funding;
import ch.unige.aou.model.xml.deposit.v2_2.Fundings;

public class FundingsDeserializer extends AouStdDeserializer<Fundings> {

  private static final long serialVersionUID = 659865L;

  @Override
  public Fundings deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    if (jsonNode instanceof ArrayNode) {
      if (jsonNode.isEmpty()) {
        return null;
      }

      Fundings fundings = new Fundings();
      for (JsonNode fundingNode : jsonNode) {
        JsonNode funderNode = fundingNode.get("funder");
        JsonNode nameNode = fundingNode.get("name");
        JsonNode codeNode = fundingNode.get("code");
        JsonNode acronymNode = fundingNode.get("acronym");
        JsonNode jurisdictionNode = fundingNode.get("jurisdiction");
        JsonNode programNode = fundingNode.get("program");

        //if list contain one element and all properties of this only element are empty we ignore the list
        if (jsonNode.size() == 1 && !this.nodeHasValue(funderNode) && !this.nodeHasValue(nameNode) && !this.nodeHasValue(codeNode) && !this
                .nodeHasValue(acronymNode)) {
          return null;
        }

        Funding funding = new Funding();

        if (this.nodeHasTextValue(funderNode)) {
          funding.setFunder(funderNode.textValue());
        }

        if (this.nodeHasTextValue(nameNode)) {
          funding.setName(nameNode.textValue());
        }

        if (this.nodeHasTextValue(codeNode)) {
          funding.setCode(codeNode.textValue());
        }

        if (this.nodeHasTextValue(acronymNode)) {
          funding.setAcronym(acronymNode.textValue());
        }

        if (this.nodeHasTextValue(jurisdictionNode)) {
          funding.setJurisdiction(jurisdictionNode.textValue());
        }

        if (this.nodeHasTextValue(programNode)) {
          funding.setProgram(programNode.textValue());
        }

        fundings.getFunding().add(funding);
      }
      return fundings;

    } else {
      throw new SolidifyRuntimeException("fundings is not an array");
    }
  }
}
