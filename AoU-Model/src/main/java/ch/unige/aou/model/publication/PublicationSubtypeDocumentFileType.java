/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeDocumentFileType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.Objects;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.rest.JoinResource;

@Entity
public class PublicationSubtypeDocumentFileType extends JoinResource<PublicationSubtypeDocumentFileTypePkId> {

  public final static String PATH_TO_PUBLICATION_SUBTYPE = "compositeKey.publicationSubtype";
  public final static String PATH_TO_DOCUMENT_FILE_TYPE = "compositeKey.documentFileType";

  @EmbeddedId
  @JsonIgnore
  private PublicationSubtypeDocumentFileTypePkId compositeKey = new PublicationSubtypeDocumentFileTypePkId();

  @NotNull
  @Enumerated(EnumType.STRING)
  private DocumentFileType.FileTypeLevel level;

  /**
   * Property allowing to sort file types in lists
   */
  @Column(nullable = false)
  private int sortValue;

  /****************************************************************/

  public PublicationSubtype getPublicationSubtype() {
    return this.compositeKey.getPublicationSubtype();
  }

  public void setPublicationSubtype(PublicationSubtype publicationSubtype) {
    this.compositeKey.setPublicationSubtype(publicationSubtype);
  }

  public DocumentFileType getDocumentFileType() {
    return this.compositeKey.getDocumentFileType();
  }

  public void setDocumentFileType(DocumentFileType documentFileType) {
    this.compositeKey.setDocumentFileType(documentFileType);
  }

  public DocumentFileType.FileTypeLevel getLevel() {
    return this.level;
  }

  public void setLevel(DocumentFileType.FileTypeLevel level) {
    this.level = level;
  }

  public int getSortValue() {
    return this.sortValue;
  }

  public void setSortValue(int sortValue) {
    this.sortValue = sortValue;
  }

  /****************************************************************/

  @Override
  @JsonIgnore
  public PublicationSubtypeDocumentFileTypePkId getCompositeKey() {
    return this.compositeKey;
  }

  @Override
  public void setCompositeKey(PublicationSubtypeDocumentFileTypePkId compositeKey) {
    this.compositeKey = compositeKey;
  }

  /*************************************************************************************/

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;

    PublicationSubtypeDocumentFileType that = (PublicationSubtypeDocumentFileType) o;

    return Objects.equals(this.getPublicationSubtype(), that.getPublicationSubtype()) &&
            Objects.equals(this.getDocumentFileType(), that.getDocumentFileType());
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + this.getPublicationSubtype().hashCode();
    result = 31 * result + this.getDocumentFileType().hashCode();
    return result;
  }
}
