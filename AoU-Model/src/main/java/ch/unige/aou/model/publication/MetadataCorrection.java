/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - MetadataCorrection.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import java.util.Objects;

public class MetadataCorrection {
  private String doi;
  private String pmid;
  private String note;

  public String getDoi() {
    return this.doi;
  }

  public void setDoi(String doi) {
    this.doi = doi;
  }

  public String getPmid() {
    return this.pmid;
  }

  public void setPmid(String pmid) {
    this.pmid = pmid;
  }

  public String getNote() {
    return this.note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof MetadataCorrection)) {
      return false;
    }
    final MetadataCorrection castOther = (MetadataCorrection) other;
    return Objects.equals(this.doi, castOther.doi) && Objects.equals(this.pmid, castOther.pmid)
            && Objects.equals(this.note, castOther.note);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.doi, this.pmid, this.note);
  }
}
