/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ContainerIdentifiersDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v1.deserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.aou.model.xml.deposit.AouStdDeserializer;
import ch.unige.aou.model.xml.deposit.v1.ContainerIdentifiers;
import ch.unige.aou.model.xml.deposit.v1.Dois;

public class ContainerIdentifiersDeserializer extends AouStdDeserializer<ContainerIdentifiers> {

  private static final long serialVersionUID = 659865L;

  @Override
  public ContainerIdentifiers deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

    ObjectCodec codec = parser.getCodec();
    JsonNode jsonNode = codec.readTree(parser);

    boolean hasValue = false;
    ContainerIdentifiers containerIdentifiers = new ContainerIdentifiers();

    JsonNode doisNode = jsonNode.get("dois");
    if (doisNode != null) {
      Dois dois = codec.treeToValue(doisNode, Dois.class);
      if (dois != null && !dois.getDoi().isEmpty()) {
        containerIdentifiers.setDois(dois);
        hasValue = true;
      }
    }

    JsonNode isbnNode = jsonNode.get("isbn");
    if (this.nodeHasTextValue(isbnNode)) {
      containerIdentifiers.setIsbn(isbnNode.textValue());
      hasValue = true;
    }

    JsonNode issnNode = jsonNode.get("issn");
    if (this.nodeHasTextValue(issnNode)) {
      containerIdentifiers.setIssn(issnNode.textValue());
      hasValue = true;
    }

    if (hasValue) {
      return containerIdentifiers;
    } else {
      return null;
    }
  }
}
