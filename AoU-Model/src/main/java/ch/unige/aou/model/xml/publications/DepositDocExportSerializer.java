/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DepositDocExportSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.publications;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;

import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;

public class DepositDocExportSerializer extends StdSerializer<DepositDoc> {
  public DepositDocExportSerializer() {
    this(null);
  }

  public DepositDocExportSerializer(Class<DepositDoc> t) {
    super(t);
  }

  @Override
  public void serialize(DepositDoc value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException {
    jsonGenerator.writeStartObject();

    jsonGenerator.writeObjectField("title", value.getTitle());
    jsonGenerator.writeObjectField("type", value.getType());
    jsonGenerator.writeObjectField("subtype", value.getSubtype());
    jsonGenerator.writeObjectField("subsubtype", value.getSubsubtype());

    jsonGenerator.writeObjectField("files", value.getFiles());

    ////contributorsFormStep.put("isCollaboration", isCollaboration);

    jsonGenerator.writeObjectField("contributors", value.getContributors());

    jsonGenerator.writeObjectField("academicStructures", value.getAcademicStructures());
    boolean isBeforeUnige = value.isIsBeforeUnige() != null && value.isIsBeforeUnige();
    jsonGenerator.writeObjectField("isBeforeUnige", isBeforeUnige);
    jsonGenerator.writeObjectField("groups", value.getGroups());

    jsonGenerator.writeObjectField("container", value.getContainer());
    jsonGenerator.writeObjectField("note", value.getNote());
    jsonGenerator.writeObjectField("keywords", value.getKeywords());
    jsonGenerator.writeObjectField("edition", value.getEdition());
    jsonGenerator.writeObjectField("languages", value.getLanguages());
    jsonGenerator.writeObjectField("discipline", value.getDiscipline());
    jsonGenerator.writeObjectField("pages", value.getPages());
    jsonGenerator.writeObjectField("collections", value.getCollections());
    jsonGenerator.writeObjectField("links", value.getLinks());
    jsonGenerator.writeObjectField("mandator", value.getMandator());
    jsonGenerator.writeObjectField("abstracts", value.getAbstracts());
    jsonGenerator.writeObjectField("identifiers", value.getIdentifiers());
    jsonGenerator.writeObjectField("dates", value.getDates());
    jsonGenerator.writeObjectField("classifications", value.getClassifications());
    jsonGenerator.writeObjectField("originalTitle", value.getOriginalTitle());
    jsonGenerator.writeObjectField("award", value.getAward());
    jsonGenerator.writeObjectField("publisherVersionUrl", value.getPublisherVersionUrl());
    jsonGenerator.writeObjectField("publisher", value.getPublisher());
    jsonGenerator.writeObjectField("aouCollection", value.getAouCollection());
    jsonGenerator.writeObjectField("datasets", value.getDatasets());
    jsonGenerator.writeObjectField("corrections", value.getCorrections());
    jsonGenerator.writeObjectField("doctorEmail", value.getDoctorEmail());
    jsonGenerator.writeObjectField("doctorAddress", value.getDoctorAddress());
    jsonGenerator.writeObjectField("fundings", value.getFundings());

    jsonGenerator.writeEndObject();
  }
}
