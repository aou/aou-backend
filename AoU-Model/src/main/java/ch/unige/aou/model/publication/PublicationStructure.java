/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationStructure.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.publication;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.rest.JoinResource;

import ch.unige.aou.model.settings.Structure;

@Entity
@Table(name = "publicationStructures")
public class PublicationStructure extends JoinResource<PublicationStructureId> {

  public final static String PATH_TO_PUBLICATION = "compositeKey.publication";
  public final static String PATH_TO_STRUCTURE = "compositeKey.structure";

  public enum LinkType {
    METADATA,   // the publication contains the structure in its metadata
    VALIDATION, // the link with the structure is only used to make the publication visible by validators
  }

  @EmbeddedId
  @JsonIgnore
  private PublicationStructureId compositeKey = new PublicationStructureId();

  @Enumerated(EnumType.STRING)
  @NotNull
  private PublicationStructure.LinkType linkType;

  /****************************************************************/

  public LinkType getLinkType() {
    return this.linkType;
  }

  @Transient
  @JsonIgnore
  public void setPublication(Publication publication) {
    this.compositeKey.setPublication(publication);
  }

  @Transient
  @JsonIgnore
  public void setStructure(Structure structure) {
    this.compositeKey.setStructure(structure);
  }

  @JsonIgnore
  public Publication getPublication() {
    return this.compositeKey.getPublication();
  }

  @JsonIgnore
  public Structure getStructure() {
    return this.compositeKey.getStructure();
  }

  public void setLinkType(LinkType linkType) {
    this.linkType = linkType;
  }

  /****************************************************************/

  @Override
  @JsonIgnore
  public PublicationStructureId getCompositeKey() {
    return this.compositeKey;
  }

  @Override
  public void setCompositeKey(PublicationStructureId compositeKey) {
    this.compositeKey = compositeKey;
  }

  @Override
  public String toString() {
    return super.toString() + " (LinkType=" + this.getLinkType() + ")";
  }
}
