/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DepositDocSerializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.xml.deposit.v2_2.serializer;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import ch.unige.aou.model.xml.deposit.v2_2.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_2.DepositDoc;

public class DepositDocSerializer extends StdSerializer<DepositDoc> {
  public DepositDocSerializer() {
    this(null);
  }

  public DepositDocSerializer(Class<DepositDoc> t) {
    super(t);
  }

  @Override
  public void serialize(DepositDoc value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException, JsonProcessingException {

    jsonGenerator.writeStartObject();

    /**
     * Type page
     */
    Map<String, Object> typeFormStep = new HashMap<>();
    typeFormStep.put("title", value.getTitle());
    typeFormStep.put("type", value.getType());
    typeFormStep.put("subtype", value.getSubtype());
    typeFormStep.put("subsubtype", value.getSubsubtype());
    jsonGenerator.writeObjectField("type", typeFormStep);

    /**
     * Files page
     */
    Map<String, Object> filesFormStep = new HashMap<>();
    typeFormStep.put("files", value.getFiles());
    jsonGenerator.writeObjectField("files", filesFormStep);

    /**
     * Contributors page
     */
    Map<String, Object> contributorsFormStep = new HashMap<>();
    contributorsFormStep.put("contributors", value.getContributors());

    boolean isCollaboration = false;
    List<Serializable> contributorsOrCollaborations = null;
    if (value.getContributors() != null) {
      contributorsOrCollaborations = value.getContributors().getContributorOrCollaboration();
      isCollaboration = contributorsOrCollaborations.stream().anyMatch(serializable -> serializable instanceof Collaboration);
    }
    contributorsFormStep.put("isCollaboration", isCollaboration);
    contributorsFormStep.put("academicStructures", value.getAcademicStructures());
    contributorsFormStep.put("groups", value.getGroups());
    jsonGenerator.writeObjectField("contributors", contributorsFormStep);

    /**
     * Description page
     */
    Map<String, Object> descriptionFormStep = new HashMap<>();
    descriptionFormStep.put("container", value.getContainer());
    descriptionFormStep.put("note", value.getNote());
    descriptionFormStep.put("keywords", value.getKeywords());
    descriptionFormStep.put("edition", value.getEdition());
    descriptionFormStep.put("languages", value.getLanguages());
    descriptionFormStep.put("discipline", value.getDiscipline());
    descriptionFormStep.put("pages", value.getPages());
    descriptionFormStep.put("collections", value.getCollections());
    descriptionFormStep.put("links", value.getLinks());
    descriptionFormStep.put("mandator", value.getMandator());
    descriptionFormStep.put("abstracts", value.getAbstracts());
    descriptionFormStep.put("identifiers", value.getIdentifiers());
    descriptionFormStep.put("dates", value.getDates());
    descriptionFormStep.put("classifications", value.getClassifications());
    descriptionFormStep.put("originalTitle", value.getOriginalTitle());
    descriptionFormStep.put("award", value.getAward());
    descriptionFormStep.put("publisherVersionUrl", value.getPublisherVersionUrl());
    descriptionFormStep.put("publisher", value.getPublisher());
    descriptionFormStep.put("aouCollection", value.getAouCollection());
    descriptionFormStep.put("datasets", value.getDatasets());
    descriptionFormStep.put("corrections", value.getCorrections());
    descriptionFormStep.put("doctorEmail", value.getDoctorEmail());
    descriptionFormStep.put("doctorAddress", value.getDoctorAddress());
    descriptionFormStep.put("fundings", value.getFundings());
    jsonGenerator.writeObjectField("description", descriptionFormStep);

    jsonGenerator.writeEndObject();
  }
}
