/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - OpenAireProjectDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.rest;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

public class OpenAireProjectDTO implements Serializable {
  private String program;
  private String code;
  private String acronym;
  private String title;
  private OpenAireFundingDTO funding = new OpenAireFundingDTO();

  public OpenAireProjectDTO() {
  }

  public OpenAireProjectDTO(String program, String code, String acronym, String title, OpenAireFundingDTO funding) {
    this.program = program;
    this.code = code;
    this.acronym = acronym;
    this.title = title;
    this.funding = funding;
  }

  public String getProgram() {
    return this.program;
  }

  public void setProgram(String program) {
    this.program = program;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getAcronym() {
    return this.acronym;
  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public OpenAireFundingDTO getFunding() {
    return this.funding;
  }

  public void setFunding(OpenAireFundingDTO funding) {
    this.funding = funding;
  }
}
