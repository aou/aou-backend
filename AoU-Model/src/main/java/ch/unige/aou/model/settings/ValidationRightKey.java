/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ValidationRightKey.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.settings;

import java.io.Serializable;
import java.util.Objects;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.PublicationSubtype;

@Embeddable
public class ValidationRightKey implements Serializable {

  private static final long serialVersionUID = 15869983822477L;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_STRUCTURE_ID, referencedColumnName = AouConstants.DB_RES_ID)
  private Structure structure;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_PERSON_ID, referencedColumnName = AouConstants.DB_RES_ID)
  private Person person;

  @ManyToOne
  @JoinColumn(name = AouConstants.DB_PUBLICATION_SUBTYPE_ID, referencedColumnName = AouConstants.DB_RES_ID)
  private PublicationSubtype publicationSubtype;

  /*************************************************************************************/

  public Structure getStructure() {
    return this.structure;
  }

  public void setStructure(Structure structure) {
    this.structure = structure;
  }

  public Person getPerson() {
    return this.person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public PublicationSubtype getPublicationSubtype() {
    return this.publicationSubtype;
  }

  public void setPublicationSubtype(PublicationSubtype publicationSubtype) {
    this.publicationSubtype = publicationSubtype;
  }

  /*************************************************************************************/

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (other == null || this.getClass() != other.getClass()) {
      return false;
    }

    ValidationRightKey castOther = (ValidationRightKey) other;
    return Objects.equals(this.getStructure(), castOther.getStructure())
            && Objects.equals(this.getPerson(), castOther.getPerson())
            && Objects.equals(this.getPublicationSubtype(), castOther.getPublicationSubtype());
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getStructure(), this.getPerson(), this.getPublicationSubtype());
  }
}
