/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - PublicationSubtypeValidationRightDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.display;

import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.ResourceBase;
import ch.unige.solidify.util.ReflectionTool;

import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.settings.ValidationRight;

public class PublicationSubtypeValidationRightDTO extends PublicationSubtype implements JoinResourceContainer<ValidationRight> {

  ValidationRight joinResource;

  public PublicationSubtypeValidationRightDTO(ValidationRight validationRight) {
    ReflectionTool.copyFields(this, validationRight.getPublicationSubtype(), ResourceBase.class);
    this.add(validationRight.getPublicationSubtype().getLinks());
    this.joinResource = validationRight;
  }

  @Override
  public ValidationRight getJoinResource() {
    return this.joinResource;
  }
}
