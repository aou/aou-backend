/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ResearchGroupDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.model.rest;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

import ch.unige.aou.model.tool.CleanTool;

public class ResearchGroupDTO implements Serializable {

  public enum Action {
    ADDED("ADDED"),
    MODIFIED("MODIFIED"),
    DELETED("DELETED"),
    NONE("NONE");

    private final String action;

    Action(String action) { this.action = action; }

    public String getAction() { return this.action; }
  }

  private String id;
  private String name;
  private String code;
  private String acronym;
  private String oldName;
  private boolean deleted;
  private LocalDateTime created;
  private LocalDateTime modified;

  @Enumerated(EnumType.STRING)
  private Action action;

  public Action getAction() {
    return action;
  }

  public void setAction(Action action) {
    this.action = action;
  }

  public String getId() {
    return this.id;
  }

  public void setId(String id) {
    this.id = CleanTool.cleanString(id);
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = CleanTool.cleanString(name);
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = CleanTool.cleanString(code);
  }

  public String getAcronym() {
    return this.acronym;
  }

  public void setAcronym(String acronym) {
    this.acronym = CleanTool.cleanString(acronym);
  }

  public boolean isDeleted() {
    return this.deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  public LocalDateTime getCreated() {
    return this.created;
  }

  public void setCreated(LocalDateTime created) {
    this.created = created;
  }

  public LocalDateTime getModified() {
    if (this.modified != null) {
      return this.modified;
    } else {
      return this.created;
    }
  }

  public void setModified(LocalDateTime modified) {
    this.modified = modified;
  }

  public String getOldName() {
    return oldName;
  }

  public void setOldName(String oldName) {
    this.oldName = oldName;
  }
}
