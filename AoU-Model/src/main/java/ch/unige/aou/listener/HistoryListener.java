/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - HistoryListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.listener;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostRemove;
import jakarta.persistence.PostUpdate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.StatusHistory;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.Publication.PublicationStatus;

public class HistoryListener {
  private static final Logger log = LoggerFactory.getLogger(HistoryListener.class);

  @PostPersist
  private void logCreation(Object object) {
    final Resource res = (Resource) object;

    /*
     * Send a message to store object creation in history
     */
    StatusHistory stsHistory;
    if (object instanceof Publication) {
      stsHistory = new StatusHistory(res.getClass().getSimpleName(), res.getResId(), res.getCreationTime(),
              this.getMessageWithImportInfo((Publication) object), res.getUpdatedBy());
    } else {
      stsHistory = new StatusHistory(res.getClass().getSimpleName(), res.getResId(), res.getCreationTime(),
              res.getUpdatedBy());
    }
    log.trace("Sending message {}", stsHistory);
    SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);

    /*
     * Send a message to store first object's status in history
     */
    this.logStatusHistory(object);
  }

  @PostUpdate
  @PostRemove
  private void logStatusHistory(Object object) {
    StatusHistory stsHistory = null;
    if (object instanceof Publication) {
      final Publication publication = (Publication) object;
      if (publication.getStatus() == PublicationStatus.IN_PROGRESS) {
        // Different steps during the creation process
        stsHistory = new StatusHistory(publication.getClass().getSimpleName(), publication.getResId(), publication.getUpdateTime(),
                publication.getStatus().toString(), publication.getStatusMessage(), publication.getFormStep().toString(),
                publication.getUpdatedBy());
      } else {
        stsHistory = new StatusHistory(publication.getClass().getSimpleName(), publication.getResId(), publication.getUpdateTime(),
                publication.getStatus().toString(), publication.getStatusMessage(), publication.getUpdatedBy());
      }

    } else if (object instanceof DocumentFile) {
      final DocumentFile df = (DocumentFile) object;
      String message;
      if (df.getIsImported() && df.getStatus().equals(DocumentFile.DocumentFileStatus.RECEIVED)) {
        if (df.getStatusMessage() == null) {
          message = "Imported from " + df.getSourceData().toString();
        } else {
          message = df.getStatusMessage() + ": Imported from " + df.getSourceData().toString();
        }
      } else {
        message = df.getStatusMessage();
      }
      stsHistory = new StatusHistory(df.getClass().getSimpleName(), df.getResId(), df.getUpdateTime(), df.getStatus().toString(),
              message, df.getUpdatedBy());

    }
    if (stsHistory != null) {
      log.trace("Sending event {}", stsHistory);
      SolidifyEventPublisher.getPublisher().publishEvent(stsHistory);
    }
  }

  public String getMessageWithImportInfo(Publication publication) {
    String message = StringTool.isNullOrEmpty(publication.getStatusMessage()) ? null : publication.getStatusMessage();
    if (publication.getImportSource() != null && !publication.getImportSource().equals(Publication.ImportSource.CLONE)) {
      return StringTool.isNullOrEmpty(message) ? "Imported from " + publication.getImportSource().toString()
              : message + " imported from " + publication.getImportSource().toString();
    } else if (publication.getImportSource() != null) { //clone
      return StringTool.isNullOrEmpty(message) ? publication.getImportSource().toString()
              : message + " " + publication.getImportSource().toString();
     } else {
      // deposit done manually
      return StringTool.isNullOrEmpty(message) ? "Created manually" : message + " created manually";
    }
  }
}
