/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AouActionName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.rest;

import ch.unige.solidify.SolidifyConstants;

public class AouActionName {

  public static final String APPROVE = "approve";
  public static final String AUTHENTICATED = "authenticated";
  public static final String ASK_CORRECTION = "ask-correction";
  public static final String GET_USER_DISPLAYABLE_INFOS = "get-displayable-infos";
  public static final String AUTHORIZE = "authorize";
  public static final String CHECK = "check";
  public static final String CHECK_TOKEN = "check_token";
  public static final String TOKEN = "token";
  public static final String REVOKE_ALL_TOKENS = "revoke-all-tokens";
  public static final String REVOKE_MY_TOKENS = "revoke-my-tokens";
  public static final String SEARCH_WITH_USER = "search-with-user";
  public static final String USER_ID = "externalUid";
  public static final String NEW_USER = "new-user";
  public static final String START_ORCID_AUTH = "start-orcid-auth";
  public static final String LANDING = "landing";
  public static final String LIST_OAUTH2_GRANT_TYPES = "list-grant-types";

  public static final String IMPORT_ALL_PROFILES_FROM_ORCID = "import-all-profiles-from-orcid";

  public static final String IMPORT_ALL_FROM_ORCID = "import-all-from-orcid";

  public static final String EXPORT_ALL_TO_ORCID = "export-all-to-orcid";
  public static final String EXPORT_TO_ORCID = "export-to-orcid";

  public static final String INITIALIZE = "initialize";
  public static final String LIST_DOCUMENT_FILE_TYPES = "list-document-file-types";
  public static final String LIST_CONTRIBUTOR_ROLES = "list-contributor-roles";
  public static final String LIST_VALIDABLE_PUBLICATIONS = "list-validable-publications";
  public static final String LIST_MY_PUBLICATIONS = "list-my-publications";
  public static final String LIST_MY_STORED_SEARCHES = "list-my-stored-searches";
  public static final String LIST_VERSION = "list-metadata-versions";
  public static final String SEARCH_UNIGE_PERSON = "search-unige-person";
  public static final String GET_ACTIVE = "get-active";
  public static final String GET_METADATA_FROM_DOI = "get-metadata-from-doi";
  public static final String GET_METADATA_FROM_ARXIV = "get-metadata-from-arxiv";
  public static final String GET_METADATA_FROM_ISBN = "get-metadata-from-isbn";
  public static final String IMPORT_FILE_ARXIV = "import-file-from-arxiv";
  public static final String GET_METADATA_FROM_PMID = "get-metadata-from-pmid";
  public static final String GET_ROMEO_INFOS = "get-romeo-infos";
  public static final String CHECK_IDENTIFIER_EXISTS = "identifier-exists";
  public static final String LIST_VALIDATOR_COMMENTS = "list-validator-comments";
  public static final String GET_MY_DEFAULT_PROFILE_DATA = "get-my-default-profile-data";
  public static final String GET_JOURNAL_TITLES = "get-journal-titles";
  public static final String GET_OPEN_AIRE_PROJECTS = "get-openaire-projects";
  public static final String IMPORT_PUBLICATIONS_FROM_FEDORA = "import-publications-from-fedora";
  public static final String SCHEDULE_ENABLED_TASKS = "schedule-enabled-tasks";
  public static final String DISABLE_TASKS_SCHEDULING = "disable-tasks-scheduling";
  public static final String KILL_TASK = "kill-task";
  public static final String UPDATE_PUBLICATIONS_RESEARCH_GROUP = "update-publications-researchGroup";
  public static final String UPDATE_PUBLICATIONS_STRUCTURE = "update-publications-structure";
  public static final String UPDATE_PUBLICATIONS_LANGUAGE = "update-publications-language";
  public static final String UPDATE_CONTRIBUTOR_DISPLAY_NAME = "update-contributor-display-name";
  public static final String CREATE_FROM_IDENTIFIER = "create-from-identifier";
  public static final String VALIDATE_METADATA = "validate-metadata";
  public static final String EXPORT = "export";
  public static final String EXPORT_DEPOSITS = "export-deposits";
  public static final String GET_EXPORT_FILE = "get-export-file";
  public static final String VALIDATE = "validate";
  public static final String SYNCHRONIZE = "synchronize";
  public static final String MERGE = "merge";
  public static final String LIST_NOTIFICATIONS_TYPES_MANDATORY = "list-notifications-types-mandatory";
  public static final String LIST_FACET_REQUESTS = "list-facet-requests";
  public static final String LIST_INDEX_FIELD_ALIASES = "list-index-field-aliases";
  public static final String LIST_ADVANCED_SEARCH_CRITERIA = "list-advanced-search-criteria";
  public static final String LIST_BIBLIOGRAPHY_FORMATS = "list-bibliography-formats";
  public static final String RUN_STORED_SEARCH = "run-stored-search";
  public static final String COMBINE_STORED_SEARCHES = "combine-stored-searches";
  public static final String GET_BY_NAME = "get-by-name";
  public static final String GET_BY_CN_INDIVIDU = "get-by-cn-individu";
  public static final String GET_BY_ORCID = "get-by-orcid";
  public static final String JAVASCRIPT = "javascript";
  public static final String JAVASCRIPT_BY_AUTHOR = "javascript-author";
  public static final String JAVASCRIPT_BY_STRUCTURE = "javascript-structure";
  public static final String JAVASCRIPT_BY_RESEARCH_GROUP = "javascript-research-group";

  public static final String CLONE = "clone";
  public static final String UPLOAD_LOGO = "upload-logo";
  public static final String DOWNLOAD_LOGO = "download-logo";
  public static final String DELETE_LOGO = "delete-logo";
  public static final String UPLOAD_AVATAR = "upload-avatar";
  public static final String DOWNLOAD_AVATAR = "download-avatar";
  public static final String DELETE_AVATAR = "delete-avatar";
  public static final String UPLOAD_THUMBNAIL = "upload-thumbnail";
  public static final String DOWNLOAD_THUMBNAIL = "download-thumbnail";
  public static final String LIST_PUBLICATION_USER_ROLES = "user-roles";
  public static final String DELETE_THUMBNAIL = "delete-thumbnail";
  public static final String THUMBNAIL = "thumbnail";
  public static final String STATISTICS = "statistics";
  public static final String INBOX = "inbox";
  public static final String SEARCH_INBOX = "search-inbox";
  public static final String SENT = "sent";
  public static final String SET_READ = "set-read";
  public static final String SET_UNREAD = "set-unread";
  public static final String SUBMIT_FOR_VALIDATION = "submit-for-validation";
  public static final String SEND_BACK_FOR_VALIDATION = "send-back-for-validation";
  public static final String ALLOWED_TO_SUBMIT = "allowed-to-submit";
  public static final String SUBMIT = "submit";
  public static final String REJECT = "reject";
  public static final String RESUME = "resume";
  public static final String REFRESH = "refresh";
  public static final String DELETE = "delete";
  public static final String EXISTS = "exists";
  public static final String ASK_FEEDBACK = "ask-feedback";
  public static final String ENABLE_REVISION = "enable-revision";
  public static final String UNLINK_CONTRIBUTOR = "unlink-contributor";
  public static final String UPGRADE_METADATA = "upgrade-metadata";
  public static final String STORE_AGAIN = "store-again";
  public static final String REINDEX = "reindex";
  public static final String REINDEX_ALL = "reindex-all";
  public static final String CLEAN_INDEXES = "clean-indexes";
  public static final String SEND_TO_CANONICAL = "send-to-canonical";
  public static final String SEND_BACK_TO_COMPLETED = "send-back-to-completed";
  public static final String START_METADATA_EDITING = "start-metadata-editing";
  public static final String CANCEL_METADATA_EDITING = "cancel-metadata-editing";
  public static final String GET_PENDING_METADATA_UPDATES = "get-pending-metadata-updates";
  public static final String IS_USER_MEMBER_OF_AUTHORIZED_INSTITUTION = "is-user-member-of-authorized-institution";
  public static final String LIST_STATUS = "list-status";
  public static final String GET_CONTACTABLE_CONTRIBUTORS = "contactable-contributors";
  public static final String CONTACT_CONTRIBUTOR = "contact-contributor";
  public static final String GET_UNIGE_CONTRIBUTOR_BY_ORCID = "unige-contributor-by-orcid";

  public static final String ADD_VIEW = "add-view";

  public static final String ADD_DOWNLOAD = "add-download";
  public static final String DOWNLOAD = "download";
  public static final String FULLTEXT = "fulltext";
  public static final String LIST_CURRENT_STATUS = "list-current-status";
  public static final String BY_EXTERNAL_UID = "by-external-uid";
  public static final String UPDATE_SORT_VALUES = "update-sort-values";
  public static final String DOWNLOAD_STATUS = "download-status";
  public static final String PREPARE_DOWNLOAD = "prepare-download";

  public static final String GET_MY_ACLS = "get-my-acls";

  public static final String PURGE_CONTENT_TEMP_FOLDER = "purge-temporary-folder";

  public static final String CONFIRM = "confirm";

  public static final String MONITOR_DATABASE_CONNECTION = "database-connection";
  public static final String CHECK_DUPLICATES = "check-duplicates";
  public static final String IMPORT = "import";

  public static final String CLEAN_CACHES = "clean-caches";

  private AouActionName() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
