/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ResourceName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.rest;

import ch.unige.solidify.SolidifyConstants;

public class ResourceName {

  public static final String APPLICATION_ROLES = "application-roles";
  public static final String PUBLICATION_DOWNLOAD = "publication-download";
  public static final String COMMENTS = "comments";
  public static final String CONTRIBUTOR_ROLE = "contributor-roles";
  public static final String CONTRIBUTORS = "contributors";
  public static final String CSTRUCT = "cStruct";
  public static final String DOCUMENT_FILE_TYPES = "document-file-types";
  public static final String DOCUMENT_FILES = "document-files";
  public static final String DOCUMENT = "document";
  public static final String DOMAIN_CONTENT = "domain_content";
  public static final String DOMAIN_DATA = "domain_data";
  public static final String DOMAIN_SOFTWARE = "domain_software";
  public static final String DOWNLOAD_TOKEN = "download-token";
  public static final String EVENTS = "events";
  public static final String EXTERNAL_DATA = "external-data";
  public static final String ID = "id";
  public static final String INSTITUTIONS = "institutions";
  public static final String IS_GENERIC = "is_generic";
  public static final String LANGUAGE = "language";
  public static final String LANGUAGES = "languages";
  public static final String GLOBAL_BANNERS = "global-banners";
  public static final String LICENSES = "licenses";
  public static final String LICENSE_GROUPS = "license-groups";
  public static final String SHIBLOGIN = "shiblogin";
  public static final String MODULES = "modules";
  public static final String MONITOR = "monitor";
  public static final String PUBLICATION_SUBTYPES_DOCUMENT_FILE_TYPES = "publication-subtypes-document-file-types";
  public static final String OD_CONFORMANCE = "od_conformance";
  public static final String OAUTH_2_CLIENTS = "oauth2-clients";
  public static final String ORCID = "orcid";
  public static final String OSD_CONFORMANCE = "osd_conformance";
  public static final String OTHER_NAMES = "other-names";
  public static final String PEOPLE = "people";
  public static final String PUBLICATIONS = "publications";
  public static final String PUBLICATION_TYPES = "publication-types";
  public static final String PUBLICATION_SUBTYPES = "publication-subtypes";
  public static final String PUBLICATION_SUB_SUBTYPES = "publication-sub-subtypes";
  public static final String ROLES = "roles";
  public static final String AUTHOR_ROLES = "author-roles";
  public static final String RESEARCH_GROUPS = "research-groups";
  public static final String NOTIFICATIONS = "notifications";
  public static final String NOTIFICATION_TYPES = "notification-types";
  public static final String STRUCTURES = "structures";
  public static final String HISTORY = "history";
  public static final String SCHEDULED_TASKS = "scheduled-tasks";
  public static final String SCHEMA = "schema";
  public static final String SYSTEM_PROPERTIES = "system-properties";
  public static final String USERS = "users";
  public static final String VALIDATION_RIGHTS_STRUCTURES = "validation-rights-structures";
  public static final String XSL = "xsl";
  public static final String METADATA = "metadata";
  public static final String INDEX_PROPERTIES = "index-properties";
  public static final String STORED_SEARCHES = "stored-searches";
  public static final String SETTINGS = "settings";
  public static final String BIBLIOGRAPHY = "bibliography";
  public static final String CITATION = "citation";
  public static final String EXPORT_METADATA = "export-metadata";
  
  private ResourceName() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
