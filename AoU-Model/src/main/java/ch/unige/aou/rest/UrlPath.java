/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - UrlPath.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.rest;

import ch.unige.solidify.SolidifyConstants;

public final class UrlPath {

  // @formatter:off

  // ==========
  // Access
  // ==========
  public static final String ACCESS = "/" + ModuleName.ACCESS;
  public static final String ACCESS_METADATA = ACCESS + "/" + ResourceName.METADATA;
  public static final String ACCESS_SETTINGS = ACCESS + "/" + ResourceName.SETTINGS;
  public static final String ACCESS_DOWNLOAD_TOKEN = ACCESS + "/" + ResourceName.DOWNLOAD_TOKEN;
  public static final String ACCESS_PUBLICATION_DOWNLOAD = ACCESS + "/" + ResourceName.PUBLICATION_DOWNLOAD;
  public static final String ACCESS_BIBLIOGRAPHY = ACCESS + "/" + ResourceName.BIBLIOGRAPHY;
  public static final String ACCESS_CONTRIBUTOR = ACCESS + "/" + ResourceName.CONTRIBUTORS;
  public static final String ACCESS_PUBLICATION = ACCESS + "/" + ResourceName.PUBLICATIONS;
  public static final String ACCESS_EXPORT = ACCESS + "/" + AouActionName.EXPORT;
  public static final String ACCESS_DOWNLOAD_FILE = ACCESS + "/" + ResourceName.DOCUMENT;
  public static final String ACCESS_MONITOR = ACCESS + "/" + ResourceName.MONITOR;

  // ==========
  // Admin
  // ==========
  public static final String ADMIN = "/" + ModuleName.ADMIN;
  public static final String ADMIN_DOCUMENT_FILES = ADMIN + "/" + ResourceName.DOCUMENT_FILES;
  public static final String ADMIN_DOWNLOAD_TOKEN = ADMIN + "/" + ResourceName.DOWNLOAD_TOKEN;
  public static final String ADMIN_INSTITUTIONS = ADMIN + "/" + ResourceName.INSTITUTIONS;
  public static final String ADMIN_LANGUAGES = ADMIN + "/" + ResourceName.LANGUAGES;
  public static final String ADMIN_LICENSES = ADMIN + "/" + ResourceName.LICENSES;
  public static final String ADMIN_LICENSE_GROUPS = ADMIN + "/" + ResourceName.LICENSE_GROUPS;
  public static final String ADMIN_OAUTH2_CLIENTS = ADMIN + "/" + ResourceName.OAUTH_2_CLIENTS;
  public static final String ADMIN_ORCID = ADMIN + "/" + ResourceName.ORCID;
  public static final String ADMIN_PEOPLE = ADMIN + "/" + ResourceName.PEOPLE;
  public static final String ADMIN_USERS = ADMIN + "/" + ResourceName.USERS;
  public static final String ADMIN_SYSTEM_PROPERTIES = ADMIN + "/" + ResourceName.SYSTEM_PROPERTIES;
  public static final String ADMIN_APPLICATION_ROLES = ADMIN + "/" + ResourceName.APPLICATION_ROLES;
  public static final String ADMIN_PUBLICATIONS = ADMIN + "/" + ResourceName.PUBLICATIONS;
  public static final String ADMIN_MODULES = ADMIN + "/" + ResourceName.MODULES;
  public static final String ADMIN_MONITOR = ADMIN + "/" + ResourceName.MONITOR;
  public static final String ADMIN_PUBLICATION_TYPES = ADMIN + "/" + ResourceName.PUBLICATION_TYPES;
  public static final String ADMIN_ROLES = ADMIN + "/" + ResourceName.ROLES;
  public static final String ADMIN_AUTHOR_ROLES = ADMIN + "/" + ResourceName.AUTHOR_ROLES;
  public static final String ADMIN_STRUCTURES = ADMIN + "/" + ResourceName.STRUCTURES;
  public static final String ADMIN_RESEARCH_GROUPS = ADMIN + "/" + ResourceName.RESEARCH_GROUPS;
  public static final String ADMIN_DOCUMENT_FILE_TYPES = ADMIN + "/" + ResourceName.DOCUMENT_FILE_TYPES;
  public static final String ADMIN_PUBLICATION_SUBTYPES_DOCUMENT_FILE_TYPES = ADMIN + "/" + ResourceName.PUBLICATION_SUBTYPES_DOCUMENT_FILE_TYPES;
  public static final String ADMIN_NOTIFICATIONS = ADMIN + "/" + ResourceName.NOTIFICATIONS;
  public static final String ADMIN_EVENTS = ADMIN + "/" + ResourceName.EVENTS;
  public static final String ADMIN_CONTRIBUTORS = ADMIN + "/" + ResourceName.CONTRIBUTORS;
  public static final String ADMIN_CONTRIBUTOR_ROLE = ADMIN + "/" + ResourceName.CONTRIBUTOR_ROLE;
  public static final String ADMIN_PUBLICATION_SUBTYPES = ADMIN + "/" + ResourceName.PUBLICATION_SUBTYPES;
  public static final String ADMIN_EXTERNAL_DATA = ADMIN + "/" + ResourceName.EXTERNAL_DATA;
  public static final String ADMIN_SCHEDULED_TASKS = ADMIN + "/" + ResourceName.SCHEDULED_TASKS;
  public static final String ADMIN_GLOBAL_BANNERS = ADMIN + "/" + ResourceName.GLOBAL_BANNERS;
  public static final String ADMIN_NOTIFICATION_TYPES = ADMIN + "/" + ResourceName.NOTIFICATION_TYPES;
  public static final String ADMIN_STORED_SEARCHES = ADMIN + "/" + ResourceName.STORED_SEARCHES;
  public static final String ADMIN_EXPORT_METADATA = ADMIN + "/" + ResourceName.EXPORT_METADATA;
  public static final String ADMIN_EXPORT_PUBLICATIONS = ADMIN + "/" + ResourceName.PUBLICATIONS;


  // ==========
  // Data Mgmt
  // ==========
  public static final String DATA_MGMT = "/" + ModuleName.DATA_MGMT;
  public static final String DATA_MGMT_INDEX_PROPERTIES = DATA_MGMT+ "/" + ResourceName.INDEX_PROPERTIES;

  // =====================
  // Authorization Server
  // =====================
  public static final String AUTH = "/" + ModuleName.AUTH;
  public static final String AUTH_AUTHORIZE = AUTH + "/" + AouActionName.AUTHORIZE;
  public static final String AUTH_CHECK_TOKEN = AUTH + "/" + AouActionName.CHECK_TOKEN;
  public static final String AUTH_SHIBLOGIN = "/" + ResourceName.SHIBLOGIN;
  public static final String AUTH_TOKEN = AUTH + "/" + AouActionName.TOKEN;
  public static final String AUTH_USER_ID = "{" + AouActionName.USER_ID + "}";

  // @formatter on

  // ================
  // Resource Server
  // ================
  public static final String RES_SRV = "/" + ModuleName.RES_SRV;
  private UrlPath() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
