/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AouMetadataVersion.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonValue;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public enum AouMetadataVersion {
  V1_0("1.0"), V2_0("2.0"), V2_1("2.1"), // structures stored with resId and resId is required
  V2_2("2.2"), // pages 'start' and 'end' are merged together -> 'paging', funder code is now a string
  V2_3("2.3"), // container conference_title renamed into container conference_subtitle
  V2_4("2.4"); // collection number as string, Report, Working paper and Master file types updates

  private static final String DEPOSIT_METADATA_SCHEMA_PREFIX = "aou-deposit-metadata-";

  private String version;

  AouMetadataVersion(String v) {
    this.version = v;
  }

  @JsonValue
  public String getVersion() {
    return this.version;
  }

  public static AouMetadataVersion getDefaultVersion() {
    return AouMetadataVersion.V2_4;
  }

  public static List<AouMetadataVersion> getAllVersions() {
    return List.of(AouMetadataVersion.V2_4, AouMetadataVersion.V2_3, AouMetadataVersion.V2_2, AouMetadataVersion.V2_1, AouMetadataVersion.V2_0,
            AouMetadataVersion.V1_0);
  }

  public String getDepositMetadataSchema() {
    switch (this.version) {
      case "1.0":
      case "2.0":
      case "2.1":
      case "2.2":
      case "2.3":
      case "2.4":
        return DEPOSIT_METADATA_SCHEMA_PREFIX + this.getVersion() + SolidifyConstants.XSD_EXT;
      default:
        throw new SolidifyRuntimeException("Wrong metadata version: " + this.version);
    }
  }

  public static AouMetadataVersion fromVersion(String version) {
    switch (version) {
      case "1.0":
        return V1_0;
      case "2.0":
        return V2_0;
      case "2.1":
        return V2_1;
      case "2.2":
        return V2_2;
      case "2.3":
        return V2_3;
      case "2.4":
        return V2_4;
      default:
        throw new SolidifyRuntimeException("Wrong metadata version: " + version);
    }
  }

  @Override
  public String toString() {
    return this.getVersion();
  }

  public String getRepresentationInfoSchema() {
    switch (this.version) {
      case "1.0":
      case "2.0":
      case "2.1":
      case "2.2":
      case "2.3":
      case "2.4":
        return DEPOSIT_METADATA_SCHEMA_PREFIX + this.getVersion() + SolidifyConstants.XSD_EXT;
      default:
        throw new SolidifyRuntimeException("Wrong metadata version: " + this.version);
    }
  }
}
