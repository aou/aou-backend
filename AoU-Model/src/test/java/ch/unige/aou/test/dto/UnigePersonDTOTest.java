/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - UnigePersonDTOTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.aou.model.rest.UnigePersonDTO;

class UnigePersonDTOTest {

  @Test
  void dtoTest() throws JsonProcessingException {
    String json = "{ \"firstname\" : \"JEAN-CLAUDE VAN DAMME\"," +
                    "\"lastname\" : \"A\"," +
                    "\"birth\" : \"1999-11-11\"," +
                    "\"indicators\" : { \"isCollab\" : true } } ";
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode jsonNode = objectMapper.readTree(json);
    UnigePersonDTO dto = new UnigePersonDTO(jsonNode);
    assertEquals("Jean-Claude Van Damme", dto.getFirstname());
    assertEquals("A", dto.getLastname());
    assertEquals(true, dto.isEmployee());
    assertEquals(1999, dto.getYearOfBirth());
  }

}
