/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - ValidationToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.tool;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import ch.unige.aou.model.tool.ValidationTool;

class ValidationToolTest {
  @Test
  void testNegativePMID() {
    assertFalse(ValidationTool.isValidPMID("-1"));
  }

  @Test
  void testAlphabeticalPMID() {
    assertFalse(ValidationTool.isValidPMID("abc"));
  }

  @Test
  void testZeroPMID() {
    assertFalse(ValidationTool.isValidPMID("0"));
  }

  @Test
  void testRegularPMID() {
    assertTrue(ValidationTool.isValidPMID("33740420"));
  }

  @Test
  void testBigPMID() {
    assertTrue(ValidationTool.isValidPMID("094215760941650195690145612049561240956109564129056129056910256914025691056901659012564105"
            + "275621049576041957601560124965109650942165094215691056910659102569205612654012650251025702475204756024967209467029679026661651"));
  }

  @Test
  void testMaxYear() {
    int currentYear = LocalDate.now().getYear();
    int maxYear = currentYear + 10;
    assertTrue(ValidationTool.isValidDate(String.valueOf(maxYear), new String[] { "d.m.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate(String.valueOf(maxYear + 1), new String[] { "d.m.yyyy" }, true));
  }

  @Test
  void testDateAsYear() {
    assertTrue(ValidationTool.isValidDate("2020", null, true));
    assertTrue(ValidationTool.isValidDate("2020", new String[] { "yyyy-MM-dd", "yyyy" }, true));
    assertTrue(ValidationTool.isValidDate("1801", new String[] { "yyyy-MM-dd", "yyyy" }, true));
    assertTrue(ValidationTool.isValidDate("2030", new String[] { "d.m.yyyy" }, true));

    assertFalse(ValidationTool.isValidDate("2055", new String[] { "d.m.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("2055", new String[] { "d.m.yyyy" }, false));
    assertFalse(ValidationTool.isValidDate("2030", new String[] { "d.m.yyyy" }, false));
    assertFalse(ValidationTool.isValidDate("1800", new String[] { "d.m.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("1700", new String[] { "d.m.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("202", new String[] { "d.m.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("-202", new String[] { "d.m.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("2500", new String[] { "d.m.yyyy" }, true));
  }

  @Test
  void testDateWithFormatSpecified() {
    int currentYear = LocalDate.now().getYear();
    int maxYear = currentYear + 10;

    assertTrue(ValidationTool.isValidDate("2020-08", new String[] { "yyyy-MM-dd", "yyyy-MM" }, true));
    assertTrue(ValidationTool.isValidDate("2020-11", new String[] { "yyyy-MM" }, true));
    assertTrue(ValidationTool.isValidDate("2020-9", new String[] { "yyyy-M" }, true));
    assertTrue(ValidationTool.isValidDate("2020-09-01", new String[] { "yyyy-MM-dd" }, true));
    assertTrue(ValidationTool.isValidDate("2020-09-01", new String[] { "yyyy-M-d" }, true));
    assertTrue(ValidationTool.isValidDate("2020-9-1", new String[] { "yyyy-M-d" }, true));
    assertTrue(ValidationTool.isValidDate("01.09.2020", new String[] { "dd.MM.yyyy" }, true));
    assertTrue(ValidationTool.isValidDate("01.09.2020", new String[] { "yyyy-MM", "d.M.yyyy" }, true));
    assertTrue(ValidationTool.isValidDate("1.9.2020", new String[] { "d.M.yyyy" }, true));
    assertTrue(ValidationTool.isValidDate("1.9." + maxYear, new String[] { "d.M.yyyy" }, true));

    assertFalse(ValidationTool.isValidDate("2030-9-", new String[] { "yyyy-MM-yy", "yyyy-MM" }, false));
    assertFalse(ValidationTool.isValidDate("10-2030", new String[] { "yyyy-MM" }, false));
    assertFalse(ValidationTool.isValidDate("2020-9-", new String[] { "yyyy-MM-yy", "yyyy-MM" }, true));
    assertFalse(ValidationTool.isValidDate("2020-9", new String[] { "yyyy-MM-yy" }, true));
    assertFalse(ValidationTool.isValidDate("2020-10-12", new String[] { "yyyy-MM" }, true));
    assertFalse(ValidationTool.isValidDate("2020-09-01", null, true));
    assertFalse(ValidationTool.isValidDate("2020-9-", new String[] { "yyyy-MM" }, true));
    assertFalse(ValidationTool.isValidDate("2020-9-ww", new String[] { "yyyy-MM-yy" }, true));
    assertFalse(ValidationTool.isValidDate("10-2020", new String[] { "yyyy-MM" }, true));
    assertFalse(ValidationTool.isValidDate("2020-09-01", new String[] { "" }, true));
    assertFalse(ValidationTool.isValidDate("2020-09-01", new String[] { "dd.MM.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("2030-09-01", new String[] { "dd.MM.yyyy" }, false));
    assertFalse(ValidationTool.isValidDate("2020-9-1", new String[] { "d.M.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("01.09.2020", null, true));
    assertFalse(ValidationTool.isValidDate("01.09.2020", new String[] { "" }, true));
    assertFalse(ValidationTool.isValidDate("01.09.2020", new String[] { "yyyy-MM-dd" }, true));
    assertFalse(ValidationTool.isValidDate("1.9.2020", new String[] { "dd.MM.yyyy" }, true));
    assertFalse(ValidationTool.isValidDate("1.9." + (maxYear + 1), new String[] { "d.M.yyyy" }, true));
  }

  @ParameterizedTest
  @ValueSource(strings = { "112", "112.456", "112.456.678", "112/456", "112.456/789", "112.456 / 789", "112.456/789.986",
          "112.456 / 789.986/45678", "112-456", "112 - 456", "112 - 456 /314.9" })
  void testValidDewey(String dewey) {
    assertTrue(ValidationTool.isValidDeweyCode(dewey));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "abcd", "1234\\789", "1234//789", "1234--789", "12345.568 / 12546 / test" })
  void testInvalidDewey(String dewey) {
    assertFalse(ValidationTool.isValidDeweyCode(dewey));
  }

  @ParameterizedTest
  @ValueSource(strings = { "0000-0001-2345-6789", "0000-0001-5109-3700", "0000-0002-1694-233X" })
  void testValidOrcid(String orcid) {
    assertTrue(ValidationTool.isValidOrcid(orcid));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "0000-0001-2345-678", "0000-0001-234-6789", "0000-001-2345-6789", "000-0001-2345-6789", "00000001-2345-6789",
          "0000-0001-2345-678Y" })
  void testInvalidOrcid(String orcid) {
    assertFalse(ValidationTool.isValidOrcid(orcid));
  }

  @ParameterizedTest
  @ValueSource(strings = { "1910.10964", "patt-sol/9311007", "cond-mat/9507074", "cond-mat/9507074v1", "1501.00001v1", "0706.0001v2" })
  void testValidArxivId(String arxivId) {
    assertTrue(ValidationTool.isValidArxivId(arxivId));
  }

  @ParameterizedTest
  @ValueSource(strings = { "", "aaaa+", "%v2v2v2v", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" })
  void testInvalidArxivId(String arxivId) {
    assertFalse(ValidationTool.isValidArxivId(arxivId));
  }

  @ParameterizedTest
  @ValueSource(strings = { "urn:nbn:ch:unige-1495076", "urn:nbn:ch:unige-6002", "urn:lex:eu:council:directive:2010-03-09;2010-19-UE",
          "urn:my-nid:my-custo@m-nss/test:yiha" })
  void testValidURN(String urn) {
    assertTrue(ValidationTool.isValidURN(urn));
  }

  @ParameterizedTest
  @ValueSource(strings = { "", "urn:::", "urn;nbn:ch:unige-1495076", "urn:nbn-ch;unige-1495076", "urn:my-nid:my-custo@m-nss/tes\nt:yiha" })
  void testInvalidURN(String urn) {
    assertFalse(ValidationTool.isValidURN(urn));
  }

  @ParameterizedTest
  @ValueSource(strings = { "10.1007/978-3-642-28108-2_19", "10.1016/S0735-1097(98)00347-7", "10.1109/5.771073", "10.1186/s40101-021-00255-z",
          "10.1002/1522-2675(20010131)84:1<141::AID-HLCA141>3.0.CO;2-R", "10.1002/1522-2675(20010228)84:2<416::AID-HLCA416>3.0.CO;2-K",
          "10.1021/ja002125+", "10.1021/ja0:02+125+" })
  void testValidDOI(String doi) {
    assertTrue(ValidationTool.isValidDOI(doi));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "0000-0001-2345-678", "11.1109/5.771073", "10.109/5.771073", "10.nature/34.77802", "10.10056.21/34.77802" })
  void testInvalidDOI(String doi) {
    assertFalse(ValidationTool.isValidDOI(doi));
  }

  @ParameterizedTest
  @ValueSource(strings = { "10/d9sggw", "10/abc" , "10/gp5vbs"})
  void testValidShortDOI(String doi) {
    assertTrue(ValidationTool.isValidShortDOI(doi));
  }

  @ParameterizedTest
  @ValueSource(strings = { "", "10.d9sggw", "12/abc", "10-90daa", "10/<89(9"})
  void testInValidShortDOI(String doi) {
    assertFalse(ValidationTool.isValidShortDOI(doi));
  }

  @ParameterizedTest
  @ValueSource(strings = { "Nicolas", "Nicolas Alexandre", "Hans-Joerg", "D'Abzac", "A Dell'Angela", "Dell'Angela A", "Marie Florence-Isabelle",
          "Florence-Isabelle Marie", "M P Z", "M P", "M", "M P Zwa", "Toto F Z", "MacLaine", "Del-Ambrozo-Mino", "Slof-Op 't Landt", "Doe M.P.",
          "Doe MP" })
  void testValidNames(String name) {
    assertTrue(ValidationTool.isValidName(name));
  }

  @ParameterizedTest
  @ValueSource(strings = { "NICOLAS", "NIColas", "NiCOLas", "nICOLAs", "NicoLAS", "Doe MPT", "Doe MPÜ" })
  void testInvalidNames(String name) {
    assertFalse(ValidationTool.isValidName(name));
  }

  @ParameterizedTest
  @ValueSource(strings = { "A123", "B6", "Q999999999", "Z0", "C10" })
  void testValidJEL(String jel) {
    assertTrue(ValidationTool.isValidJEL(jel));
  }

  @ParameterizedTest
  @ValueSource(strings = { "AA123", "BB6", "999999999", "Z0-1", "A123.45", "A", "AB", "2", "a123", "aa123", "A-12" })
  void testInvalidJEL(String jel) {
    assertFalse(ValidationTool.isValidJEL(jel));
  }
}
