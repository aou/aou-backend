/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - AbstractXMLTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.tool;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import ch.unige.solidify.SolidifyConstants;

public abstract class AbstractXMLTest {

  protected Path getPath(String f) {
    try {
      return new ClassPathResource(f).getFile().toPath();
    } catch (final IOException e) {
      return new FileSystemResource(f).getFile().toPath();
    }
  }

  protected List<URL> loadSchema(String xsdFile) {
    final List<URL> list = new ArrayList<>();
    try {
      final ClassPathResource xsd = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + xsdFile);
      list.add(xsd.getURL());
    } catch (final IOException e) {
      fail(e.getMessage());
    }
    return list;
  }

}
