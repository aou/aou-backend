/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - CleanToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.tool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.tool.CleanTool;

class CleanToolTest {

  @Test
  void cleanTitleTest() {
    assertNull(CleanTool.cleanTitle(null));
    assertEquals("", CleanTool.cleanTitle(""));

    assertEquals("A very interesting title", CleanTool.cleanTitle("A very interesting title"));
    assertEquals("A very interesting title", CleanTool.cleanTitle("A very interesting title."));
    assertEquals("A very interesting title", CleanTool.cleanTitle("    A very interesting title   "));
    assertEquals("A very interesting title", CleanTool.cleanTitle("    A    very    interesting    title   "));
    assertEquals("A very interesting title", CleanTool.cleanTitle("\nA very interesting title\n"));
    assertEquals("A very interesting title", CleanTool.cleanTitle("\n\nA very   interesting     title\n\n"));

    assertEquals("A very interesting title", CleanTool.cleanTitle("A very interesting title."));
    assertEquals("A very interesting title", CleanTool.cleanTitle("    A very interesting title.   "));
    assertEquals("A very interesting title", CleanTool.cleanTitle("    A    very    interesting    title.   "));
    assertEquals("A very interesting title", CleanTool.cleanTitle("\nA very interesting title.\n"));
    assertEquals("A very interesting title", CleanTool.cleanTitle("\n\nA very   interesting     title.\n\n"));
    assertEquals("A very interesting title", CleanTool.cleanTitle("\n\nA very \n  interesting  \n   title.\n\n"));

    assertEquals("A very interesting title", CleanTool.cleanTitle("A very&nbsp;interesting title"));

    assertEquals("A very <i>interesting</i> <em>title</em> <sup>sup</sup> <sub>sub</sub>",
            CleanTool.cleanTitle("<s>A</s> <b>very</b> <i>interesting</i> <em>title</em> <sup>sup</sup> <sub>sub</sub>"));
    assertEquals("A very <i>interesting</i> <em>title</em> <sup>sup</sup> <sub>sub</sub>",
            CleanTool.cleanTitle(
                    "<s>A</s> <b>very</b> <i>interesting</i> <script>alert()</script> <em>title</em> <sup>sup</sup> <sub>sub</sub>"));

    assertEquals("A very &gt; interesting title", CleanTool.cleanTitle("A very > interesting title"));
    assertEquals("A very &gt;&gt; interesting title", CleanTool.cleanTitle("A very >> interesting title"));
    assertEquals("A very &gt; interesting title", CleanTool.cleanTitle("A very      > interesting       title       \n"));
    assertEquals("A very &gt; interesting line title", CleanTool.cleanTitle("A very      > interesting <p>line</p>      title       \n"));
    assertEquals("A very &gt; interesting line title", CleanTool.cleanTitle("<p>A very      > interesting <p>line</p>      title       </p>\n"));
    assertEquals("A very &gt; interesting line title",
            CleanTool.cleanTitle("<p><p>A very      > interesting <p>line</p>      title       </p>\n</p>"));
    assertEquals("A very &lt; interesting title", CleanTool.cleanTitle("A very < interesting title"));

    assertEquals("A very interesting title", CleanTool.cleanTitle("A very <test>interesting</test> title"));
    assertEquals("A very interesting title", CleanTool.cleanTitle("A very <test>interesting title"));
    assertEquals("A very &lt;interesting&gt; title", CleanTool.cleanTitle("A very &lt;interesting&gt; title"));
    assertEquals("A very interesting title", CleanTool.cleanTitle("A very <test-script>interesting</test-script> title"));

    assertEquals("A very inte－resting title", CleanTool.cleanTitle("A very <test-script>inte－resting</test-script> title"));

    assertEquals("COVID-19 Vaccine Development - where do we stand?", CleanTool.cleanTitle("COVID-19 Vaccine Development - where do we stand?"));
  }

  @Test
  void cleanAbstractTest() {
    assertNull(CleanTool.cleanAbstract(null));
    assertEquals("", CleanTool.cleanAbstract(""));

    assertEquals("A very interesting abstract", CleanTool.cleanAbstract("A very interesting abstract"));
    assertEquals("A very interesting abstract.", CleanTool.cleanAbstract("A very interesting abstract."));
    assertEquals("A very <em>interesting</em> abstract.", CleanTool.cleanAbstract("A very <em>interesting</em> abstract."));

    assertEquals("A very <em>inter－esting</em> abstract.", CleanTool.cleanAbstract("A very <em>inter－esting</em> abstract."));

    assertEquals("A very <em>interesting</em> <a href=\"https://www.unige.ch\">abstract</a>.",
            CleanTool.cleanAbstract("A very <em>interesting</em> <a href=\"https://www.unige.ch\">abstract</a>."));
    assertEquals("A very <em>interesting</em> <a href=\"https://www.unige.ch\" rel=\"noopener noreferrer\" target=\"_blank\">abstract</a>.",
            CleanTool.cleanAbstract(
                    "A very <em>interesting</em> <a href=\"https://www.unige.ch\" rel=\"noopener noreferrer\" target=\"_blank\">abstract</a>."));
    assertEquals("A very <em>interesting</em> <a href=\"https://www.unige.ch\" rel=\"noopener noreferrer\" target=\"_blank\">abstract</a>.",
            CleanTool.cleanAbstract(
                    "A very <em>interesting</em> <a href=\"https://www.unige.ch\" rel=\"noopener noreferrer\" target=\"_blank\" onclick=\"alert();\">abstract</a>."));

    assertEquals("A <sup>very</sup> \n"
                    + "<ul>\n"
                    + " <li>interesting</li>\n"
                    + " <li>clever</li>\n"
                    + "</ul> abstract.",
            CleanTool.cleanAbstract("A <sup>very</sup> <ul><li>interesting</li><li>clever</li></ul> abstract."));

    assertEquals("A <sup>very</sup> \n"
                    + "<ul>\n"
                    + " <li>interesting</li>\n"
                    + " <li>clever</li>\n"
                    + "</ul> abstract.",
            CleanTool.cleanAbstract("      A <sup>very</sup>        <ul><li>interesting</li><li>clever</li></ul>       abstract.        "));

    assertEquals("A <sup>very</sup> \n"
                    + "<ul>\n"
                    + " <li>interesting</li>\n"
                    + " <li>clever</li>\n"
                    + "</ul> abstract. \n"
                    + "<p>with some <sub>new</sub> line</p>",
            CleanTool.cleanAbstract("      A <sup>very</sup>        <ul><li>interesting</li><li>clever</li></ul>       abstract.        \n"
                    + "\n"
                    + "<p>with some <sub>new</sub> line</p>\n"));

    assertEquals(
            "but stable T-cell responses that were less susceptible than antibodies to mutations - another sentence after dash － or long dash",
            CleanTool.cleanAbstract(
                    "but stable T-cell responses that were less susceptible than antibodies to mutations - another sentence after dash － or long dash"));
  }

  @Test
  void cleanStringTest() {
    assertNull(CleanTool.cleanString(null));
    assertEquals("", CleanTool.cleanString(""));

    assertEquals("A simple string", CleanTool.cleanString("A simple string"));
    assertEquals("A simple string.", CleanTool.cleanString("A simple string."));
    assertEquals("A simple string", CleanTool.cleanString("   A   simple   string"));
    assertEquals("A simple string.", CleanTool.cleanString("A   simple   string.   "));
    assertEquals("A simple string.", CleanTool.cleanString("\n\nA   simple   string.   \n"));
    assertEquals("A simple string.", CleanTool.cleanString("\n\nA   simple \n\n  string.   \n"));

    assertEquals("<sup>A</sup> s<em>imp</em>l<b>e<b> <p>string</p>.",
            CleanTool.cleanString("\n\n<sup>A</sup>   s<em>imp</em>l<b>e<b> \n\n  <p>string</p>.   \n"));
    assertEquals("A very > interesting <p>line</p> title", CleanTool.cleanString("A very      > interesting <p>line</p>      title       \n"));

  }

  @Test
  void capitalizeStringTest() {
    assertEquals("François-Xavier D'Abzac", CleanTool.capitalizeString("FRANÇOIS-XAVIER D'ABZAC"));
    assertEquals("A D", CleanTool.capitalizeString("A D"));
    assertEquals("A d", CleanTool.capitalizeString("A d"));
    assertEquals("Joao da Silva", CleanTool.capitalizeString("Joao da Silva"));
    assertEquals("Marie Dupont-Pictet", CleanTool.capitalizeString("MARIE DUPONT-PICTET"));
    assertEquals("Marie Dupont-Pict'E", CleanTool.capitalizeString("MARIE DUPONT-PICT'E"));
  }

  @Test
  void cleanPagingTest() {
    assertNull(CleanTool.cleanPaging(null));
    assertEquals("", CleanTool.cleanPaging(""));

    assertEquals("143-145", CleanTool.cleanPaging("143-145"));
    assertEquals("143-145", CleanTool.cleanPaging("      143-145"));
    assertEquals("143-145", CleanTool.cleanPaging("   143-145  "));
    assertEquals("143-145", CleanTool.cleanPaging("   143  -  145  "));
    assertEquals("143-145", CleanTool.cleanPaging("   143  -  45  "));
    assertEquals("143-145", CleanTool.cleanPaging("   143  -\t45  "));
    assertEquals("143-145", CleanTool.cleanPaging("   143 \t -\t 45  "));

    assertEquals("143-143", CleanTool.cleanPaging("143-43"));
    assertEquals("143-143", CleanTool.cleanPaging(" 143  - 43 "));
    assertEquals("143-143", CleanTool.cleanPaging(" 143  -\t 43 "));
    assertEquals("143-143", CleanTool.cleanPaging(" \t143 \t - 43 "));

    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143-145"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("p.      143-145"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("   p.   143-145"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("   p. \t  143-145\t"));

    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143-45"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143-   45"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143   -45"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143\t   -45\t"));

    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143-5"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143-   5"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143   -5"));
    assertEquals("p. 143-145", CleanTool.cleanPaging("p. 143\t\t-5\t\t\t"));

    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143-   5   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143-   45   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143-   145   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("   p. 143-   145   test  "));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging(" \t  p.\t143-\t\t\t145\t\t\ttest\t"));

    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143   -5   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143   -45   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143   -145   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("   p. 143   -145   test   "));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("   p.\t 143   -145   test   \t"));

    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143   -  5   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143   -  45   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("p. 143   -  145   test"));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("   p. 143   -  145   test  "));
    assertEquals("p. 143-145 test", CleanTool.cleanPaging("   p. 143\t   - \t 145   test  "));

    // other kinds of hyphens
    assertEquals("143-145", CleanTool.cleanPaging("   143  ‒  45  "));
    assertEquals("143-145", CleanTool.cleanPaging("   143  ‑  45  "));
    assertEquals("143-145", CleanTool.cleanPaging("   143  －  45  "));
    assertEquals("143-145", CleanTool.cleanPaging("  \t 143  － \t 45 \t "));
    assertEquals("143-145", CleanTool.cleanPaging("   143  ‒  45  "));

    assertEquals("123456785-123456789", CleanTool.cleanPaging("   123456785  ‒  9  "));
  }

  @Test
  void doNotCleanPagingTest() {
    assertEquals("143-27", CleanTool.cleanPaging("143-27"));
    assertEquals("1243-145", CleanTool.cleanPaging("1243-145"));
    assertEquals("1143-test", CleanTool.cleanPaging("1143-test"));
    assertEquals("bla 1-1 test", CleanTool.cleanPaging("bla 1-1 test"));
    assertEquals("lorem ipsum dolores", CleanTool.cleanPaging("lorem ipsum dolores"));
  }

  @Test
  void cleanHyphens() {
    assertNull(CleanTool.cleanHyphens(null, false));
    assertEquals("", CleanTool.cleanHyphens("", false));

    String expectedResult = "xxx - yy - z";
    assertEquals("xxx-yy-z", CleanTool.cleanHyphens("xxx-yy-z", false));
    assertEquals("xxx-yy-z", CleanTool.cleanHyphens("xxx－yy-z", false));
    assertEquals("xxx - yy - z", CleanTool.cleanHyphens(expectedResult, false));
    assertEquals("xxx - yy - z", CleanTool.cleanHyphens("xxx - yy - z", false));
    assertEquals("    xxx - yy - z  ", CleanTool.cleanHyphens("    xxx - yy - z  ", false));
    assertEquals("    xxx - yy - z  ", CleanTool.cleanHyphens("    xxx ‒ yy ‒ z  ", false));
    assertEquals("xxx - yy - z", CleanTool.cleanHyphens("xxx － yy － z", false));
    assertEquals("xxx -    yy -    z   ", CleanTool.cleanHyphens("xxx －    yy –    z   ", false));
  }

  @Test
  void cleanStringAndHyphens() {
    assertNull(CleanTool.cleanStringAndHyphens(null, false));
    assertEquals("", CleanTool.cleanStringAndHyphens("", false));

    String expectedResult = "xxx - yy - z";
    assertEquals("xxx-yy-z", CleanTool.cleanStringAndHyphens("xxx-yy-z", false));
    assertEquals("xxx-yy-z", CleanTool.cleanStringAndHyphens("xxx－yy-z", false));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens(expectedResult, false));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("xxx - yy - z", false));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("    xxx - yy - z  ", false));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("    xxx ‒ yy ‒ z  ", false));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("xxx － yy － z", false));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("xxx －    yy –    z   ", false));
  }

  @Test
  void cleanStringAndHyphensWithSurroundingSpaces() {
    assertNull(CleanTool.cleanStringAndHyphens(null, true));
    assertEquals("", CleanTool.cleanStringAndHyphens("", true));

    String expectedResult = "xxx-yy-z";
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens(expectedResult, true));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("xxx - yy - z", true));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("    xxx - yy - z  ", true));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("    xxx ‒ yy ‒ z  ", true));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("xxx － yy － z", true));
    assertEquals(expectedResult, CleanTool.cleanStringAndHyphens("xxx －    yy –    z   ", true));
  }

  @Test
  void cleanTrailingDots() {
    assertNull(CleanTool.removeTrailingDots(null));
    assertEquals("", CleanTool.removeTrailingDots(""));
    assertEquals("A", CleanTool.removeTrailingDots("A."));
    assertEquals("A", CleanTool.removeTrailingDots("A.."));
    assertEquals("A.B", CleanTool.removeTrailingDots("A.B"));
    assertEquals("A.B", CleanTool.removeTrailingDots("A.B."));
    assertEquals("A..B", CleanTool.removeTrailingDots("A..B."));
    assertEquals("A..B", CleanTool.removeTrailingDots("A..B...................."));
  }

  @Test
  void removeTrailingColons() {
    assertNull(CleanTool.removeTrailingColon(null));
    assertEquals("A simple string", CleanTool.removeTrailingColon("A simple string"));
    assertEquals("A simple string", CleanTool.removeTrailingColon("A simple string:"));
    assertEquals("A simple string", CleanTool.removeTrailingColon("A simple string:::"));
    assertEquals("A simple: string", CleanTool.removeTrailingColon("A simple: string:::"));
    assertEquals(":A simple: string", CleanTool.removeTrailingColon(":A simple: string:::"));
  }

  @Test
  void cleanTrailingParenthesis() {
    assertNull(CleanTool.removeTrailingParenthesisAndContent(null));
    assertEquals("", CleanTool.removeTrailingParenthesisAndContent(""));
    assertEquals("A", CleanTool.removeTrailingParenthesisAndContent("A()"));
    assertEquals("A", CleanTool.removeTrailingParenthesisAndContent("A(AVC)"));
    assertEquals("A(AVC)B", CleanTool.removeTrailingParenthesisAndContent("A(AVC)B"));
    assertEquals("A", CleanTool.removeTrailingParenthesisAndContent("A(AVC))"));
  }

  @Test
  void cleanInvisibleCharacters() {
    assertNull(CleanTool.removeInvisibleCharacters(null));
    assertEquals("", CleanTool.removeInvisibleCharacters(""));
    assertEquals("Die Volkswirtschaft", CleanTool.removeInvisibleCharacters("\u0098Die \u009CVolkswirtschaft\n"));
    assertEquals("I Quaderni dell'ingegnere", CleanTool.removeInvisibleCharacters("\u0098I \u009CQuaderni dell'ingegnere\n"));
  }

  @Test
  void cleanNumberSymbol() {
    assertNull(CleanTool.extractNumbersFromString(null));
    assertEquals("123", CleanTool.extractNumbersFromString("N° 123"));
    assertEquals("123", CleanTool.extractNumbersFromString("n° 123"));
    assertEquals("123", CleanTool.extractNumbersFromString("N°123"));
    assertEquals("123", CleanTool.extractNumbersFromString("N°123"));
    assertEquals("123", CleanTool.extractNumbersFromString("123"));
  }

  @Test
  void extractYearFromDate() {
    String[] metadataDateFormat = { "yyyy-MM-dd", "yyyy-MM", "yyyy" };
    assertEquals(2022, CleanTool.extractYearFromDate("2022", metadataDateFormat));
    assertEquals(2022, CleanTool.extractYearFromDate("2022-10", metadataDateFormat));
    assertEquals(2022, CleanTool.extractYearFromDate("2022-10-12", metadataDateFormat));
    assertEquals(2022, CleanTool.extractYearFromDate("  2022  ", metadataDateFormat));
    assertEquals(2022, CleanTool.extractYearFromDate("  2022-10  ", metadataDateFormat));
    assertEquals(2022, CleanTool.extractYearFromDate("  2022-10-12  ", metadataDateFormat));
    assertNull(CleanTool.extractYearFromDate("2022-10-12abc", metadataDateFormat));
    assertNull(CleanTool.extractYearFromDate("2022-31-31", metadataDateFormat));
    assertNull(CleanTool.extractYearFromDate("bad value", metadataDateFormat));
  }

  @Test
  void removeAllHtmlTagsTest() {
    assertNull(CleanTool.removeAllHtmlTags(null));
    assertEquals("", CleanTool.removeAllHtmlTags(""));
    assertEquals("Some text", CleanTool.removeAllHtmlTags("Some text", false));
    assertEquals("Some text", CleanTool.removeAllHtmlTags("Some <i>text</i>", false));
    assertEquals("Some text", CleanTool.removeAllHtmlTags("Some <p><i>text</i></p>", false));
    assertEquals("Some other text", CleanTool.removeAllHtmlTags("Some <p>other <i>text</i></p>", false));
    assertEquals("Some other text", CleanTool.removeAllHtmlTags("Some <p>other   <i>text</i></p>", false));
    assertEquals("Some other text again", CleanTool.removeAllHtmlTags("Some <p>other  <i>text  </i>  </p>    again   ", false));
    assertEquals("Some other text again2",
            CleanTool.removeAllHtmlTags("Some&nbsp;<p>other  <i>text  </i>  </p>    again<sup>2</sup>   ", false));
    assertEquals("Invalid HTML", CleanTool.removeAllHtmlTags("Invalid <p><i>HTML   <i><p>", false));
  }

  @Test
  void removeAllHtmlTagsKeepNewLinesTest() {
    assertNull(CleanTool.removeAllHtmlTags(null));
    assertEquals("", CleanTool.removeAllHtmlTags(""));
    assertEquals("Some text", CleanTool.removeAllHtmlTags("Some text"));
    assertEquals("Some text", CleanTool.removeAllHtmlTags("Some <i>text</i>"));
    assertEquals("Some\ntext", CleanTool.removeAllHtmlTags("Some <p><i>text</i></p>"));
    assertEquals("Some\nother text", CleanTool.removeAllHtmlTags("Some <p>other <i>text</i></p>"));
    assertEquals("Some\nother text", CleanTool.removeAllHtmlTags("Some <p>other   <i>text</i></p>"));
    assertEquals("Some\nother text\nagain", CleanTool.removeAllHtmlTags("Some <p>other  <i>text  </i>  </p>    again   "));
    assertEquals("Some\nother text\nagain2",
            CleanTool.removeAllHtmlTags("Some&nbsp;<p>other  <i>text  </i>  </p>    again<sup>2</sup>   "));
    assertEquals("Invalid\nHTML", CleanTool.removeAllHtmlTags("Invalid <p><i>HTML   <i><p>"));
    assertEquals("First paragraph\nSecond paragraph", CleanTool.removeAllHtmlTags("<p>First paragraph</p><p>Second paragraph</p>"));
    assertEquals("First paragraph\nSecond paragraph", CleanTool.removeAllHtmlTags(" <p>First paragraph</p>  <p>Second paragraph</p>  "));
    assertEquals("Test avec un indice23 et exposants34 & de l'italique puis non (4 < 10) (20 > 5)", CleanTool.removeAllHtmlTags(
            "Test <i>avec </i>un<i> indice</i><sub><i>23</i></sub><i> et exposants</i><sup><i>34</i></sup><i> &amp;</i> de l'i<i>talique</i> puis non (4 &lt; 10) (20 &gt; 5)"));
  }

  @Test
  void restoreAllowedHtmlTags() {
    List<String> titleTags = List.of(CleanTool.TAGS_ALLOWED_IN_TITLE);
    List<String> abstractTags = new ArrayList<>();
    abstractTags.addAll(List.of(CleanTool.TAGS_ALLOWED_IN_TITLE));
    abstractTags.addAll(List.of(CleanTool.TAGS_ALLOWED_IN_ABSTRACT));

    assertEquals("A simple string", CleanTool.restoreAllowedHtmlTags("A simple string", titleTags));
    assertEquals("A simple <i>string</i>", CleanTool.restoreAllowedHtmlTags("A simple &lt;i&gt;string&lt;/i&gt;", titleTags));
    // <p> is not supported in titles --> not restored
    assertEquals("A simple &lt;p&gt;string&lt;/p&gt;", CleanTool.restoreAllowedHtmlTags("A simple &lt;p&gt;string&lt;/p&gt;", titleTags));
    // When user wants greater than or less than symbol in title
    assertEquals("100 &gt; 90 but 10 &lt; 20", CleanTool.restoreAllowedHtmlTags("100 &amp;gt; 90 but 10 &amp;lt; 20", titleTags));

    assertEquals("A simple string", CleanTool.restoreAllowedHtmlTags("A simple string", abstractTags));
    assertEquals("A simple <i>string</i>", CleanTool.restoreAllowedHtmlTags("A simple &lt;i&gt;string&lt;/i&gt;", abstractTags));
    // <p> is supported in abstracts --> restored
    assertEquals("A simple <p>string</p>", CleanTool.restoreAllowedHtmlTags("A simple &lt;p&gt;string&lt;/p&gt;", abstractTags));

    // special case of &
    assertEquals("<i>Me</i> &amp; you", CleanTool.restoreAllowedHtmlTags("&lt;i&gt;Me&lt;/i&gt; &amp;amp; you", titleTags));
  }

  @Test
  void cleanQuotes() {
    assertEquals("Let's go !", CleanTool.cleanQuotes("Let's go !"));
    assertEquals("Let&quot;s go !", CleanTool.cleanQuotes("Let&quot;s go !"));
    assertEquals("Let&quot;s go !", CleanTool.cleanQuotes("Let&rsquo;s go !"));
    assertEquals("Let&quot;s go !", CleanTool.cleanQuotes("Let&lsquo;s go !"));
    assertEquals("Let&quot;s go !", CleanTool.cleanQuotes("Let&nbsp;&raquo;s go !"));
    assertEquals("Let&quot;s go !", CleanTool.cleanQuotes("Let&laquo;&nbsp;s go !"));
  }

  @Test
  void cleanFileName() {
    assertEquals("picture.jpeg", CleanTool.cleanFileName("picture.jpeg"));
    assertEquals("picture_geneve.jpeg", CleanTool.cleanFileName("picture+geneve.jpeg"));
    assertEquals("picture_geneve.jpeg", CleanTool.cleanFileName("picture_geneve.jpeg"));
    assertEquals("picture_geneve.jpeg", CleanTool.cleanFileName("picture geneve.jpeg"));
    assertEquals("picture_genève.jpeg", CleanTool.cleanFileName("picture genève.jpeg"));
    assertEquals("picture_geneve.jpeg", CleanTool.cleanFileName("picture genève.jpeg", true));
  }

  @Test
  void cleanFileNameWithAccents() {
    assertNull(CleanTool.cleanFileName(null, true));
    assertEquals("", CleanTool.cleanFileName("", true));
    assertEquals("Oliveira_Almeida_Fonseca_Laura_Do_Mar_Sc.Med.56.txt",
            CleanTool.cleanFileName("Oliveira Almeida Fonseca_Laura Do Mar_Sc.Méd.56.txt", true));
    assertEquals("Orel_Erol_Sc.Biomed.-S.Glob.042.txt", CleanTool.cleanFileName("Orel_Erol_Sc.Bioméd.-S.Glob.042.txt", true));
    assertEquals("Padmasola_Guru_Prasad_Neur.378.txt", CleanTool.cleanFileName("Padmasola_Guru Prasad_Neur.378.txt", true));
    assertEquals("Paez_Granados_Marcial_Octavio_L.1112.txt", CleanTool.cleanFileName("Paez Granados_Marcial Octavio_L.1112.txt", true));
    assertEquals("Pallis_Konstantinos_Med.11166.txt", CleanTool.cleanFileName("Pallis_Konstantinos_Méd.11166.txt", true));
    assertEquals("Piguet_Laure_L.1115.txt", CleanTool.cleanFileName("Piguet_Laure_L.1115.txt", true));
    assertEquals("Plumettaz_Maud_France_Nadege_FPSE865.txt", CleanTool.cleanFileName("Plumettaz_Maud France Nadège_FPSE865.txt", true));
    assertEquals("Poilane_Benjamin_GSEM131.txt", CleanTool.cleanFileName("Poilane_Benjamin_GSEM131.txt", true));
    assertEquals("Poisson_Philippe_Alexandre_Sc.5779.txt", CleanTool.cleanFileName("Poisson_Philippe Alexandre_Sc.5779.txt", true));
    assertEquals("Polak_Elias_Sc.5764.txt", CleanTool.cleanFileName("Polak_Elias_Sc.5764.txt", true));
    assertEquals("Poltier_Jeremie_Gautier_SdS248.txt", CleanTool.cleanFileName("Poltier_Jérémie Gautier_SdS248.txt", true));
  }

  @Test
  void cleanAccents() {
    assertNull(CleanTool.removeAccents(null));
    assertEquals("", CleanTool.removeAccents(""));
    assertEquals("test 123 john", CleanTool.removeAccents("test 123 john"));
    assertEquals("Jeremie Medecine", CleanTool.removeAccents("Jérémie Médecine"));
    assertEquals("aaaeiiiiggnnsssuuy", CleanTool.removeAccents("āăąēîïĩíĝġńñšŝśûůŷ"));
    assertEquals("Jeremie Medecine xxx - yy - z", CleanTool.removeAccents("Jérémie Médecine xxx － yy － z"));
  }

  @Test
  void getFullUnigeExternalUidTest() {
    assertThrows(SolidifyRuntimeException.class, () -> CleanTool.getFullUnigeExternalUid(null));
    assertThrows(SolidifyRuntimeException.class, () -> CleanTool.getFullUnigeExternalUid("test"));

    assertEquals("12345@unige.ch", CleanTool.getFullUnigeExternalUid("12345"));
    assertEquals("12345@unige.ch", CleanTool.getFullUnigeExternalUid("12345@unige.ch"));
  }

  @Test
  void externalUidToCnIndividuTest() {
    assertThrows(SolidifyRuntimeException.class, () -> CleanTool.externalUidToCnIndividu(null));
    assertThrows(SolidifyRuntimeException.class, () -> CleanTool.externalUidToCnIndividu("test"));

    assertEquals("12345", CleanTool.externalUidToCnIndividu("12345@unige.ch"));
    assertEquals("12345", CleanTool.externalUidToCnIndividu("12345"));
  }

  @Test
  void trimStringTest() {
    assertNull(CleanTool.trim(null));
    assertEquals("", CleanTool.trim(""));
    assertEquals("12345", CleanTool.trim("  12345  "));
    assertEquals("archive ouverte", CleanTool.trim("\t archive ouverte\t"));
    assertEquals("archive ouverte", CleanTool.trim("\r archive ouverte\r"));
    assertEquals("archive ouverte", CleanTool.trim("\n archive ouverte\n"));
    assertEquals("archive ouverte", CleanTool.trim("  \n \t \r archive ouverte \n \t \n  "));
    assertEquals("archive ouverte", CleanTool.trim("  \n\n\n \t \r archive ouverte \n \t \n  \n"));
  }
}
