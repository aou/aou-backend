/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - DocumentFileTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.publication.DocumentFile;

public class DocumentFileTest {

  @Test
  void getCurrentAccessLevel() {
    DocumentFile df = new DocumentFile();
    assertNull(df.getAccessLevel());
    assertNull(df.getEmbargoAccessLevel());
    assertNull(df.getEmbargoEndDate());

    // simple access level
    df = new DocumentFile();
    df.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);
    assertEquals(DocumentFile.AccessLevel.PUBLIC, df.getCurrentAccessLevel());
    assertNull(df.getEmbargoAccessLevel());
    assertNull(df.getEmbargoEndDate());

    // simple access level
    df = new DocumentFile();
    df.setAccessLevel(DocumentFile.AccessLevel.PRIVATE);
    assertEquals(DocumentFile.AccessLevel.PRIVATE, df.getCurrentAccessLevel());
    assertNull(df.getEmbargoAccessLevel());
    assertNull(df.getEmbargoEndDate());

    // running embargo
    df = new DocumentFile();
    df.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);
    df.setEmbargoAccessLevel(DocumentFile.AccessLevel.PRIVATE);
    df.setEmbargoEndDate(LocalDate.now().plusDays(5));
    assertEquals(DocumentFile.AccessLevel.PRIVATE, df.getCurrentAccessLevel());
    assertNotNull(df.getEmbargoAccessLevel());
    assertNotNull(df.getEmbargoEndDate());

    // embargo is over
    df = new DocumentFile();
    df.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);
    df.setEmbargoAccessLevel(DocumentFile.AccessLevel.PRIVATE);
    df.setEmbargoEndDate(LocalDate.now().minusDays(5));
    assertEquals(DocumentFile.AccessLevel.PUBLIC, df.getCurrentAccessLevel());
    assertNotNull(df.getEmbargoAccessLevel());
    assertNotNull(df.getEmbargoEndDate());

    // embargo access level without embargo end date --> exception
    final DocumentFile df2 = new DocumentFile();
    df2.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);
    df2.setEmbargoAccessLevel(DocumentFile.AccessLevel.PRIVATE);
    assertThrows(SolidifyRuntimeException.class, () -> df2.getCurrentAccessLevel());

    // running embargo without final access level --> returns embargo access level
    df = new DocumentFile();
    df.setEmbargoAccessLevel(DocumentFile.AccessLevel.PRIVATE);
    df.setEmbargoEndDate(LocalDate.now().plusDays(5));
    assertEquals(DocumentFile.AccessLevel.PRIVATE, df.getCurrentAccessLevel());
    assertNotNull(df.getEmbargoAccessLevel());
    assertNotNull(df.getEmbargoEndDate());

    // embargo is over but no final access level -> return null
    df = new DocumentFile();
    df.setEmbargoAccessLevel(DocumentFile.AccessLevel.PRIVATE);
    df.setEmbargoEndDate(LocalDate.now().minusDays(5));
    assertNull(df.getCurrentAccessLevel());
    assertNotNull(df.getEmbargoAccessLevel());
    assertNotNull(df.getEmbargoEndDate());
  }
}
