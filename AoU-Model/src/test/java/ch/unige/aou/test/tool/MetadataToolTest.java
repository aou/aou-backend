/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - MetadataToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.tool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.model.tool.MetadataTool;

public class MetadataToolTest {

  private final static String NBN_URN_PREFIX = "urn:nbn:ch:";

  @Test
  void generateSwissNationalLibraryUrnTest() {
    assertEquals("urn:nbn:ch:unige-1593703", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:159370"));
    assertEquals("urn:nbn:ch:unige-1593712", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:159371"));
    assertEquals("urn:nbn:ch:unige-1594184", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:159418"));
    assertEquals("urn:nbn:ch:unige-1595188", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:159518"));
    assertEquals("urn:nbn:ch:unige-346706", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:34670"));
    assertEquals("urn:nbn:ch:unige-354023", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:35402"));
    assertEquals("urn:nbn:ch:unige-354043", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:35404"));
    assertEquals("urn:nbn:ch:unige-355529", MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, "unige:35552"));

    assertThrows(SolidifyRuntimeException.class, () -> MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, ""));
    assertThrows(SolidifyRuntimeException.class, () -> MetadataTool.generateSwissNationalLibraryUrn(NBN_URN_PREFIX, null));
  }
}
