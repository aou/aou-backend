/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Model - JAXBTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.tool;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.FileReader;
import java.io.IOException;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

import ch.unige.aou.model.xml.fedora.foxml.v1.DigitalObject;
import ch.unige.aou.model.xml.fedora.repository.DatastreamProfile;
import ch.unige.aou.model.xml.fedora.repository.FedoraRepository;
import ch.unige.aou.model.xml.marc21.CollectionType;
import ch.unige.aou.model.xml.marc21.MarcCollection;

@ExtendWith(SpringExtension.class)
class JAXBTest extends AbstractXMLTest {
  private static final Logger log = LoggerFactory.getLogger(JAXBTest.class);
  private static final String FOXML_1_1_FILE = "foxml_reference_example.xml";
  private static final String FEDORA3_DS_PROFILE_FILE = "dataStreamProfile.xml";
  private static final String FEDORA3_REPOSITORY_FILE = "fedoraRepository.xml";
  private static final String MARC21_XML = "marc21.xml";

  @Test
  void readRepositoryInfoTest() throws JAXBException, IOException, XMLStreamException {
    this.checkXmlSerialization(FEDORA3_REPOSITORY_FILE, FedoraRepository.class, false, true);
  }

  @Test
  void readFoxmlTest() throws JAXBException, IOException, XMLStreamException {
    this.checkXmlSerialization(FOXML_1_1_FILE, DigitalObject.class, false, true);
  }

  @Test
  void readDatastreamTest() throws JAXBException, IOException, XMLStreamException {
    this.checkXmlSerialization(FEDORA3_DS_PROFILE_FILE, DatastreamProfile.class, false, false);
  }

  @Test
  void readMarcXmlTest() throws IOException, JAXBException, XMLStreamException {
    QName qName = new QName("http://www.loc.gov/MARC21/slim", "collection");
    JAXBElement<CollectionType> collectionType = new JAXBElement<>(qName, CollectionType.class, null);
    this.checkXmlSerialization(MARC21_XML, collectionType.getClass(), true, false);
  }

  @Test
  void readMarcXmlCollectionTest() throws IOException, JAXBException, XMLStreamException {
    this.checkXmlSerialization(MARC21_XML, MarcCollection.class, false, false);
  }

  @SuppressWarnings("unchecked")
  private <T> void checkXmlSerialization(String xml, Class<T> objType, boolean unmarshalWithType, boolean print)
          throws JAXBException, IOException, XMLStreamException {
    final JAXBContext jaxbContext = JAXBContext.newInstance(objType);
    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    T obj;
    try (FileReader fileReader = new FileReader(this.getPath(xml).toFile())) {
      if (unmarshalWithType) {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader someSource = factory.createXMLEventReader(fileReader);
        obj = (T) jaxbUnmarshaller.unmarshal(someSource, objType);
      } else {
        obj = (T) jaxbUnmarshaller.unmarshal(fileReader);
      }
      assertNotNull(obj);
      if (print) {
        jaxbMarshaller.marshal(obj, System.out);
      }
    }
  }
}
