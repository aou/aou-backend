/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - PersonDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

public class PersonDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getModelName() {
    return getCamelCase(Person.class.getSimpleName());
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_PEOPLE;
  }

  @Override
  public List<String> getResourceLinks() {
    return Arrays.asList(AouActionName.UPLOAD_AVATAR, AouActionName.DOWNLOAD_AVATAR, ResourceName.INSTITUTIONS, ResourceName.RESEARCH_GROUPS,
            ResourceName.STRUCTURES, ResourceName.NOTIFICATION_TYPES, ResourceName.VALIDATION_RIGHTS_STRUCTURES, ResourceName.ROLES);
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("firstName")
        .description("The first name of the person")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("lastName")
        .description("The last name of the person")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("fullName")
        .description("The full name of the person")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("email")
        .description("The email of the Person")
        .mandatory(true)
        .type(TypeField.STRING)
        .customConstraints("Valid email address")
        .testValue(TEST_EMAIL_VALUE)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("avatar")
        .description("Avatar of the person")
        .type(TypeField.UNDEFINED)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("orcid")
        .description("The ORCID of the person")
        .type(TypeField.STRING)
        .customConstraints(this.displayFormat("xxxx-xxxx-xxxx-xxxx"))
        .testValue(TEST_ORCID)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("verifiedOrcid")
        .description("Is the person's ORCID verified by orcid.org")
        .type(TypeField.BOOLEAN)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("roleIds")
        .description("List of eventual business role ids of the person")
        .type(TypeField.ARRAY)
        .readOnly(true)
        .build());
    // @formatter:on
  }
}
