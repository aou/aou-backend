/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - EventDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.snippet.Attributes;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.model.notification.EventType;
import ch.unige.aou.rest.UrlPath;

public class EventDoc extends AbstractResourceDoc {
  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_EVENTS;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    final List<FieldDescriptor> documentationFields = new ArrayList<>(Arrays.asList(
            new FieldDescriptorBuilder(this)
                    .name("message")
                    .description("The message of the event")
                    .mandatory(true)
                    .type(TypeField.STRING)
                    .build(),
            subsectionWithPath("triggerBy")
                    .description("The person who triggers of the event")
                    .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true))),
            new FieldDescriptorBuilder(this)
                    .name("eventType.resId")
                    .description("The type of the event")
                    .mandatory(true)
                    .type(TypeField.STRING)
                    .enumValues(EventType.getAllEventTypes().stream().map(t->t.toString()).collect(Collectors.toList()))
                    .customConstraints(CONSTRAINT_VALID_JSON)
                    .testValue("PUBLICATION_CREATED")
                    .readOnly(true)
                    .build(),
            subsectionWithPath("eventType").description("The type of the event")
                    .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true)),
                            new Attributes.Attribute(MANDATORY_VALUE, String.valueOf(true))),
            new FieldDescriptorBuilder(this)
                    .name("publication.resId")
                    .description("The publication linked to the event")
                    .mandatory(true)
                    .type(TypeField.STRING)
                    .testValue(TEST_STRING_VALUE)
                    .readOnly(true)
                    .build(),
            subsectionWithPath("publication")
                    .description("The publication of event")
                    .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true)),
                            new Attributes.Attribute(MANDATORY_VALUE, String.valueOf(true)))
    ));

    // @formatter:on
    return documentationFields;
  }

}
