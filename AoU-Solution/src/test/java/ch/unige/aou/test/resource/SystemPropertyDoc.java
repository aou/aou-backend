/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - SystemPropertyDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.snippet.Attributes;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.helper.RestDocsTemplates;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.rest.UrlPath;

public class SystemPropertyDoc extends AbstractResourceDoc {
  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_SYSTEM_PROPERTIES;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("defaultChecksum")
        .description("The default checksum type used by the system")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("defaultLicense")
        .description("The default license used by the system")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("fileSizeLimit")
        .description("The maximum file size, in bytes, up to which data and virus verification are done")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
         .name("orcidClientId")
         .description("1234-1234-1234-1234")
         .type(TypeField.STRING)
         .build(),
      new FieldDescriptorBuilder(this)
        .name("limitMaxContributors")
        .description("The maximum number of contributor to retrieve from deposit import service")
        .type(TypeField.INT32)
        .build(),
      subsectionWithPath("defaultContributorRoles")
        .description("The default contributor roles to use by deposit subtype when creating a deposit")
        .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true)),
              new Attributes.Attribute(MANDATORY_VALUE, String.valueOf(true))),
      subsectionWithPath("searchFacets")
        .description("Describes facet properties used on archives search page")
        .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true))),
      subsectionWithPath("staticPages")
        .description("List of static urls used to build the navigation menu")
        .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true)))
    );
    // @formatter:on
  }

  @Override
  public List<String> getCommonResourceFields() {
    return List.of(RestDocsTemplates.LINKS_FIELD);
  }

  @Override
  public boolean isNormalResource() {
    return false;
  }
}
