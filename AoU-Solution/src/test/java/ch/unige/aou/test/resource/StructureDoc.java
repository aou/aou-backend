/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - StructureDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

public class StructureDoc extends AbstractResourceDoc {
  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_STRUCTURES;
  }

  @Override
  public List<String> getResourceLinks() {
    return Arrays.asList(ResourceName.PEOPLE);
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
            new FieldDescriptorBuilder(this)
                    .name("parentStructure")
                    .description("The eventual parent of the structure")
                    .mandatory(false)
                    .type(TypeField.UNDEFINED)
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("status")
                    .description("The status of the structure")
                    .mandatory(true)
                    .enumValues(this.enumToList(Structure.StructureStatus.values()))
                    .testValue(Structure.StructureStatus.ACTIVE.toString())
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("name")
                    .description("The name of the structure")
                    .mandatory(true)
                    .type(TypeField.STRING)
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("sortValue")
                    .description("The sort value of the structure")
                    .mandatory(true)
                    .type(TypeField.INT32)
                    .testValue("10")
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("cnStructC")
                    .description("The cnStructC of the structure")
                    .mandatory(true)
                    .type(TypeField.STRING)
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("cStruct")
                    .description("The cStruct of the structure")
                    .mandatory(true)
                    .type(TypeField.STRING)
                    .testValue("10")
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("dewey")
                    .description("The dewey of the structure")
                    .type(TypeField.STRING)
                    .testValue("1234")
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("acronym")
                    .description("The acronym of the structure")
                    .mandatory(true)
                    .type(TypeField.STRING)
                    .testValue("UN")
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("validUntil")
                    .description("The date of validity of the structure")
                    .mandatory(false)
                    .type(TypeField.DATE)
                    .build()
  );


    // @formatter:on
  }
}
