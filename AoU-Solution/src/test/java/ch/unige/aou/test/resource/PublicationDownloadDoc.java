/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - PublicationDownloadDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.Arrays;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.rest.UrlPath;

public class PublicationDownloadDoc extends AbstractResourceDoc {
  @Override
  public List<String> getListLinks() {
    return Arrays.asList(ActionName.VALUES);
  }

  @Override
  public String getName() {
    return UrlPath.ACCESS_PUBLICATION_DOWNLOAD;
  }

  @Override
  public List<String> getResourceLinks() {
    return Arrays.asList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
            new FieldDescriptorBuilder(this)
                    .name("status")
                    .description("The status of the publication download")
                    .enumValues(this.enumToList(PublicationDownload.DownloadStatus.values()))
                    .build(),
            new FieldDescriptorBuilder(this)
                    .name("fileSize")
                    .description("The size of the archive")
                    .mandatory(false)
                    .type(TypeField.FLOAT)
                    .build().type(JsonFieldType.NUMBER),
            new FieldDescriptorBuilder(this)
                    .name("name")
                    .description("The name of the archive")
                    .mandatory(false)
                    .type(TypeField.STRING)
                    .build().type(JsonFieldType.STRING),
            new FieldDescriptorBuilder(this)
                    .name("finalData")
                    .description("Path where the archive has been downloaded")
                    .mandatory(false)
                    .type(TypeField.URL)
                    .build().type(JsonFieldType.STRING),
            new FieldDescriptorBuilder(this)
                    .name("publicationId")
                    .mandatory(false)
                    .description("The id of the linked publication")
                    .type(TypeField.STRING)
                    .build().type(JsonFieldType.STRING)
            );
    // @formatter:on
  }
}
