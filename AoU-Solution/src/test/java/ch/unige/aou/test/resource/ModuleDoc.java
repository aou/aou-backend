/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - ModuleDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.rest.UrlPath;

public class ModuleDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return new ArrayList<>();
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_MODULES;
  }

  @Override
  public List<String> getResourceLinks() {
    return new ArrayList<>();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    final List<FieldDescriptor> documentationFields = new ArrayList<>(Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("authorization")
        .description("The URL of _authoriration_ module")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("admin")
        .description("The URL of _admin_ module")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("access")
        .description("The URL of _access_ module")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("index")
        .description("The URL of _index_ module")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("oaiInfo")
        .description("The URL of _oaipmh_ module")
        .type(TypeField.STRING)
        .build()
    ));
    // @formatter:on
    return documentationFields;
  }

  @Override
  public List<String> getCommonResourceFields() {
    return new ArrayList<>();
  }

  @Override
  public boolean isNormalResource() {
    return false;
  }
}
