/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - GlobalBannerDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;

import ch.unige.solidify.model.GlobalBanner;
import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

public class GlobalBannerDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Arrays.asList(AouActionName.GET_ACTIVE);
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_GLOBAL_BANNERS;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    final List<FieldDescriptor> documentationFields =  new ArrayList<>(Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("name")
        .description("The name of the global banner")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("withDescription")
        .description("If the global banner has a description")
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN),
      new FieldDescriptorBuilder(this)
        .name("type")
        .description("The type of the global banner")
        .mandatory(true)
        .enumValues(this.enumToList(GlobalBanner.GlobalBannerType.values()))
        .testValue(GlobalBanner.GlobalBannerType.INFO.toString())
        .build(),
      new FieldDescriptorBuilder(this)
        .name("enabled")
        .description("If the global banner is enable")
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN),
      new FieldDescriptorBuilder(this)
        .name("titleLabels")
        .description("The title list by language of the global banner")
        .type(TypeField.ARRAY)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("descriptionLabels")
        .description("The description list by language of the global banner")
        .type(TypeField.ARRAY)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("startDate")
        .description("The start date of the global banner")
        .mandatory(true)
        .type(TypeField.DATE)
        .testValue("2022-06-10T12:00:00.000+0200")
        .build(),
      new FieldDescriptorBuilder(this)
        .name("endDate")
        .description("The end date of the global banner")
        .mandatory(true)
        .type(TypeField.DATE)
        .testValue("2022-06-15T12:00:00.000+0200")
        .build()));
    // @formatter:on
    return documentationFields;
  }

}
