/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - IndexFieldAliasDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;

import ch.unige.solidify.IndexUrlPath;
import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

public class IndexFieldAliasDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return IndexUrlPath.INDEX_INDEX_FIELD_ALIAS;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("indexName")
        .description("The name of the ElasticSearch index the field is for")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("alias")
        .description("The name that will be exposed to clients (as a field or a facet)")
        .mandatory(true)
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("field")
        .description("The field path in the index")
        .mandatory(true)
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("facet")
        .description("Should a facet be created for this field ? default to false")
        .mandatory(true)
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN),
      new FieldDescriptorBuilder(this)
        .name("system")
        .description("Is used by system")
        .mandatory(true)
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN),
      new FieldDescriptorBuilder(this)
        .name("facetMinCount")
        .description("Minimum number of occurrences of a facet value to be returned in facet results if the IndexFieldAlias is a facet")
        .type(TypeField.INT32)
        .build().type(JsonFieldType.NUMBER),
      new FieldDescriptorBuilder(this)
        .name("facetLimit")
        .description("Maximum number of facets values to return if the IndexFieldAlias is a facet")
        .type(TypeField.INT32)
        .build().type(JsonFieldType.NUMBER),
      new FieldDescriptorBuilder(this)
        .name("facetOrder")
        .description("Determines the position of a facet in the user interface")
        .type(TypeField.INT32)
        .build().type(JsonFieldType.NUMBER),
      new FieldDescriptorBuilder(this)
        .name("facetDefaultVisibleValues")
        .description("Number of facet values displayed by default in the user interface")
        .type(TypeField.INT32)
        .build().type(JsonFieldType.NUMBER),
      new FieldDescriptorBuilder(this)
        .name("labels")
        .description("Alias labels for each supported languages")
        .type(TypeField.ARRAY)
        .build()
    );
      // @formatter:on
  }
}
