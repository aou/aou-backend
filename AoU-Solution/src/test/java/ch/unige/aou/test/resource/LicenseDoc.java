/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - LicenseDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.snippet.Attributes;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.rest.UrlPath;

public class LicenseDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_LICENSES;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("licenseGroup.resId")
        .description("The group id of the license")
        .mandatory(true)
        .type(TypeField.STRING)
        .testValue("GROUP-CC0")
        .build(),
      subsectionWithPath("licenseGroup")
        .description("The group of the publication")
        .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true)),
                new Attributes.Attribute(MANDATORY_VALUE, String.valueOf(true))),
      new FieldDescriptorBuilder(this)
        .name("openLicenseId")
        .description("The open license identifier")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("title")
        .description("The title of the license")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("version")
        .description("The version of the license")
        .mandatory(false)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("url")
        .description("The URL source of the license")
        .type(TypeField.URL)
        .customConstraints(CONSTRAINT_VALID_URL)
        .testValue(TEST_URL_VALUE)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("sortValue")
        .description("The sort value of the license")
        .mandatory(true)
        .type(TypeField.INT32)
        .testValue("10")
        .build(),
      new FieldDescriptorBuilder(this)
        .name("visible")
        .description("Indicate if the license is visible in the select boxes")
        .mandatory(true)
        .type(TypeField.BOOLEAN)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("labels")
        .description("Translations of the license")
        .mandatory(true)
        .type(TypeField.ARRAY)
        .build());
    // @formatter:on
  }
}
