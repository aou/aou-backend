/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - UserDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.snippet.Attributes;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.model.security.User;
import ch.unige.aou.rest.UrlPath;

public class UserDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getModelName() {
    return getCamelCase(User.class.getSimpleName());
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_USERS;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("firstName")
        .description("The first name of the user")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("lastName")
        .description("The last name of the user")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("fullName")
        .description("The full name of the user")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("externalUid")
        .description("The external UID of the user")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("email")
        .description("The email of the user")
        .mandatory(true)
        .type(TypeField.STRING)
        .customConstraints("Valid email address")
        .testValue(TEST_EMAIL_VALUE)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("homeOrganization")
        .description("The home organization of the user")
        .mandatory(true)
        .type(TypeField.STRING)
        .testValue(TEST_STRING_VALUE)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("enabled")
        .description("If the user is enable")
        .mandatory(true)
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN),
      new FieldDescriptorBuilder(this)
        .name("person")
        .description("The linked person of the user")
        .type(TypeField.UNDEFINED)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("lastLoginTime")
        .description("Time of last login")
        .type(TypeField.UNDEFINED)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("lastLoginIpAddress")
        .description("The IP address from last login")
        .build(),
      subsectionWithPath("applicationRole")
        .description("The application role details of the user")
        .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true))));
  // @formatter:on

  }

}
