/*-
 * %%----------------------------------------------------------------------------------------------
 * hedera Technology - AoU Solution - SolutionOpenapi31Documentation.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.doc;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import ch.unige.solidify.test.doc.RestDocsOpenapiDocumentation;

@ActiveProfiles({ "index-inmemory", "sec-noauth", "client-standalone", "test", "openapi-3.1", "person-search-aou-prod", "storage-file",
        "orcid" })
@Import(SolutionRestTestsAndDocumentationConfig.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT, properties = { "spring.cloud.bootstrap.enabled=true" })
public class SolutionOpenapi31Documentation extends RestDocsOpenapiDocumentation {

  @Override
  protected String getFileName() {
    return "aou-openapi-3.1.json";
  }

}
