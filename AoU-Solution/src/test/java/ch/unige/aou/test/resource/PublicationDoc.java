/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - PublicationDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.snippet.Attributes;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.Publication.DepositFormStep;
import ch.unige.aou.model.publication.Publication.ImportSource;
import ch.unige.aou.model.publication.Publication.PublicationStatus;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

public class PublicationDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Arrays.asList(ResourceName.SCHEMA, ActionName.VALUES);
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_PUBLICATIONS;
  }

  @Override
  public List<String> getResourceLinks() {
    return Arrays.asList(ResourceName.COMMENTS, AouActionName.SUBMIT_FOR_VALIDATION, ActionName.HISTORY, ResourceName.DOCUMENT_FILES,
            ResourceName.RESEARCH_GROUPS, ResourceName.STRUCTURES, ResourceName.CONTRIBUTORS, ResourceName.PUBLICATION_SUBTYPES,
            ResourceName.PUBLICATION_TYPES, AouActionName.UPLOAD_THUMBNAIL, AouActionName.DOWNLOAD_THUMBNAIL, AouActionName.DELETE_THUMBNAIL);
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    final List<FieldDescriptor> documentationFields = new ArrayList<>(Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("title")
        .description("The title of the publication")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      subsectionWithPath("creator")
        .description("The creator of the publication")
        .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true))),
      subsectionWithPath("lastEditor")
        .description("The last editor of the publication")
        .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true))),
      new FieldDescriptorBuilder(this)
        .name("metadata")
        .description("The metadata of the publication stored in pivot format (XML)")
        .mandatory(true)
        .type(TypeField.STRING)
        .testValue("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><deposit_doc xmlns=\"https://archive-ouverte.unige.ch/deposit/v2.3\">"
                + "<type>Article</type><subtype>Article scientifique</subtype><subsubtype>Article</subsubtype><title>My article</title>"
                + "<languages><language>eng</language></languages>"
                + "<identifiers><doi>10.1126/science.1175088</doi></identifiers></deposit_doc>")
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("metadataBackup")
        .description("The backup of metadata of the publication when updating")
        .mandatory(false)
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("formData")
        .description("The metadata of the publication formatted in Json for the Angular portal")
        .mandatory(true)
        .type(TypeField.STRING)
        .testValue(AouConstants.TEST_DEPOSIT_JSON_FORM_DATA)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("importSource")
        .description("The eventual source used to import metadata")
        .mandatory(false)
        .enumValues(this.enumToList(ImportSource.values()))
        .testValue(ImportSource.PMID.name())
        .build(),
      new FieldDescriptorBuilder(this)
        .name("metadataVersion")
        .description("The metadata version of the publication")
        .enumValues(this.enumToList(AouMetadataVersion.values()))
        .testValue(AouMetadataVersion.getDefaultVersion().getVersion())
        .build(),
      new FieldDescriptorBuilder(this)
        .name("status")
        .description("The status of the publication")
        .mandatory(true)
        .enumValues(this.enumToList(PublicationStatus.values()))
        .testValue(PublicationStatus.IN_PROGRESS.toString())
        .build(),
      new FieldDescriptorBuilder(this)
         .name("formStep")
         .description("The step of the publication before validating")
         .mandatory(false)
         .enumValues(this.enumToList(Publication.DepositFormStep.values()))
         .testValue(DepositFormStep.TYPE.toString())
         .build(),
      new FieldDescriptorBuilder(this)
        .name("statusMessage")
        .description("The message related to the status")
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("numberOfComments")
        .description("The number of comments of the publication")
        .type(TypeField.INT32)
        .build(),
      new FieldDescriptorBuilder(this)
         .name("numberOfValidatorComments")
         .description("The number of comments for validators of the publication")
         .type(TypeField.INT32)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("numberOfFiles")
         .description("The number of files linked to the publication")
         .type(TypeField.INT32)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("doi")
         .description("The eventual DOI of the publication")
         .type(TypeField.STRING)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("pmid")
         .description("The eventual PMID of the publication")
         .type(TypeField.INT64)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("archiveId")
         .description("The archive ID of publication")
         .type(TypeField.STRING)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("thumbnail")
         .description("Thumbnail of the publication")
         .type(TypeField.UNDEFINED)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("metadataStructures")
         .description("Structures existing in metadata")
         .type(TypeField.UNDEFINED)
         .readOnly(true)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("validationStructures")
         .description("Structures used for Publication validation")
         .type(TypeField.UNDEFINED)
         .readOnly(true)
         .build(),
      new FieldDescriptorBuilder(this)
         .name("lastStatusUpdate")
         .description("Date of the last status update")
         .type(TypeField.DATE_TIME)
         .readOnly(true)
         .build(),
      subsectionWithPath("subtype").description("The subtype of the publication")
              .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true)),
                      new Attributes.Attribute(MANDATORY_VALUE, String.valueOf(true))),
      subsectionWithPath("subSubtype").description("The eventual subSubtype of the publication")
              .attributes(new Attributes.Attribute(READ_ONLY_VALUE, String.valueOf(true)),
                      new Attributes.Attribute(MANDATORY_VALUE, String.valueOf(false)))
      ));
      // @formatter:on
    documentationFields.addAll(new PublicationSubtypeDTODoc().getDocumentationFields());
    documentationFields.addAll(new PublicationSubSubtypeDTODoc().getDocumentationFields());
    return documentationFields;
  }
}
