/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - PublicationTypeDTODoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

public class PublicationTypeDTODoc extends AbstractResourceDoc {
  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return "publication-type-dto";
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    return this.getDocumentationFields(null);
  }

  protected List<FieldDescriptor> getDocumentationFields(String prefix) {
    // @formatter:off
    final List<FieldDescriptor> documentationFields =  new ArrayList<>(Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name(prefix + ".type.resId")
        .description("Id of the subtype")
        .mandatory(true)
        .type(TypeField.STRING)
        .testValue("A1")
        .build(),
      new FieldDescriptorBuilder(this)
        .name(prefix + ".type.name")
        .description("Name of the subtype")
        .mandatory(true)
        .type(TypeField.STRING)
        .build()
    ));
    // @formatter:on

    return documentationFields;
  }
}
