/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - OaiMetadataPrefixDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;

import ch.unige.solidify.OAIUrlPath;
import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

public class OaiMetadataPrefixDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return OAIUrlPath.OAI_METADATA_PREFIX;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    return new ArrayList<>(Arrays.asList(
    // @formatter:off
      new FieldDescriptorBuilder(this)
        .name("prefix")
        .description("The specification of the OAI metadata prefix")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("name")
        .description("The name of the OAI metadata prefix")
        .mandatory(true)
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("description")
        .description("The description of the OAI metadata prefix")
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("enabled")
        .description("If the OAI metadata prefix is enable")
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN),
      new FieldDescriptorBuilder(this)
        .name("reference")
        .description("If the OAI metadata prefix is the reference metadata")
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN),
      new FieldDescriptorBuilder(this)
        .name("schemaUrl")
        .description("The XML schema URL of the OAI metadata prefix")
        .mandatory(true)
        .type(TypeField.URL)
        .customConstraints(CONSTRAINT_VALID_URL)
        .testValue(TEST_URL_VALUE)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("schemaNamespace")
        .description("The XML schema namespace of the OAI metadata prefix")
        .mandatory(true)
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("metadataSchema")
        .description("The XML schema of the OAI metadata prefix")
        .mandatory(true)
        .type(TypeField.STRING)
        .customConstraints(CONSTRAINT_WELLFORMED_XML)
        .testValue(TEST_XML_VALUE)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("metadataXmlTransformation")
        .description("The XML transformation from reference metadata of the OAI metadata prefix")
        .mandatory(true)
        .type(TypeField.STRING)
        .customConstraints(CONSTRAINT_WELLFORMED_XML)
        .testValue(TEST_XML_VALUE)
        .build().type(JsonFieldType.STRING)
      ));
    // @formatter:on
  }
}
