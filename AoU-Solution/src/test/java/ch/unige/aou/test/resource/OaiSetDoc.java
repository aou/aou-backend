/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - OaiSetDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;

import ch.unige.solidify.OAIUrlPath;
import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

public class OaiSetDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return OAIUrlPath.OAI_SET;
  }

  @Override
  public List<String> getResourceLinks() {
    return Collections.emptyList();
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    return new ArrayList<>(Arrays.asList(
    // @formatter:off
      new FieldDescriptorBuilder(this)
        .name("spec")
        .description("The specification of the OAI set")
        .mandatory(true)
        .type(TypeField.STRING)
        .customConstraints("Valid URI in lower case")
        .build(),
      new FieldDescriptorBuilder(this)
        .name("name")
        .description("The name of the OAI set")
        .mandatory(true)
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("description")
        .description("The description of the OAI set")
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("query")
        .description("The query of the OAI set")
        .mandatory(true)
        .type(TypeField.STRING)
        .customConstraints("Query compliante with indexing search")
        .testValue(TEST_SEARCH_QUERY)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("enabled")
        .description("If the OAI set is enable")
        .type(TypeField.BOOLEAN)
        .build().type(JsonFieldType.BOOLEAN)));
    // @formatter:on
  }
}
