/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - InstitutionDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.resource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;

import ch.unige.solidify.test.helper.AbstractResourceDoc;
import ch.unige.solidify.test.helper.FieldDescriptorBuilder;
import ch.unige.solidify.test.model.TypeField;

import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.rest.UrlPath;

public class InstitutionDoc extends AbstractResourceDoc {

  @Override
  public List<String> getListLinks() {
    return Collections.emptyList();
  }

  @Override
  public String getName() {
    return UrlPath.ADMIN_INSTITUTIONS;
  }

  @Override
  public List<String> getResourceLinks() {
    return Arrays.asList(ResourceName.PEOPLE, AouActionName.UPLOAD_LOGO, AouActionName.DOWNLOAD_LOGO);
  }

  @Override
  protected List<FieldDescriptor> getDocumentationFields() {
    // @formatter:off
    return Arrays.asList(
      new FieldDescriptorBuilder(this)
        .name("name")
        .description("The name of the institution")
        .mandatory(true)
        .type(TypeField.STRING)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("description")
        .description("The description of the institution")
        .type(TypeField.STRING)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("url")
        .description("The URL of the institution website")
        .type(TypeField.URL)
        .customConstraints(CONSTRAINT_VALID_URL)
        .testValue(TEST_URL_VALUE)
        .build().type(JsonFieldType.STRING),
      new FieldDescriptorBuilder(this)
        .name("logo")
        .description("Logo of the institution")
        .type(TypeField.UNDEFINED)
        .build(),
      new FieldDescriptorBuilder(this)
        .name("emailSuffixes")
        .description("List of email suffixes")
        .type(TypeField.ARRAY)
        .build());
      // @formatter:on
  }
}
