ifdef::admin[]
//
// Admin module BEGIN
//
[[admin]]
=== Admin Module

[[admin-response-structure]]
==== Response Structure

include::{snippets}/application/admin/response-fields.adoc[]
include::{snippets}/application/admin/links.adoc[]

[[admin-resources]]
==== Resources

[[admin-application-roles]]
===== Application Roles

A role at the application level, one of USER, ADMIN, ROOT.

include::{snippets}/application/admin/application-roles/path-request.adoc[]
[[admin-application-roles-details]]
====== Details
include::{snippets}/application/admin/application-roles/response-fields.adoc[]

[[admin-application-roles-links]]
====== Resource Links
include::{snippets}/application/admin/application-roles/links.adoc[]

[[admin-application-roles-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/application-roles/list/links.adoc[]

[[admin-institutions]]
===== Institutions

An institution represents academic institutions, such as UNIGE and HES-SO.

include::{snippets}/application/admin/institutions/path-request.adoc[]
[[admin-institutions-details]]
====== Details
include::{snippets}/application/admin/institutions/response-fields.adoc[]

[[admin-institutions-links]]
====== Resource Links
include::{snippets}/application/admin/institutions/links.adoc[]

[[admin-institutions-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/institutions/list/links.adoc[]

[[admin-languages]]
===== Languages

include::{snippets}/application/admin/languages/path-request.adoc[]
[[admin-languages-details]]
====== Details
include::{snippets}/application/admin/languages/response-fields.adoc[]

[admin-languages-links]]
====== Resource Links
include::{snippets}/application/admin/languages/links.adoc[]

[admin-languages-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/languages/list/links.adoc[]

[[admin-licenses]]
===== Licenses

A license represents software licenses (Academic Free License 3.0, Design Science License, MIT License...).

include::{snippets}/application/admin/licenses/path-request.adoc[]
[[admin-licenses-details]]
====== Details
include::{snippets}/application/admin/licenses/response-fields.adoc[]

[admin-licenses-links]]
====== Resource Links
include::{snippets}/application/admin/licenses/links.adoc[]

[[admin-licenses-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/licenses/list/links.adoc[]

[[admin-people]]
===== People

A person is associated to a user on the platform. It contains information such as the person's ORCID, institution and organizational units it belongs to.

include::{snippets}/application/admin/people/path-request.adoc[]
[[admin-people-details]]
====== Details
include::{snippets}/application/admin/people/response-fields.adoc[]

[[admin-people-links]]
====== Resource Links
include::{snippets}/application/admin/people/links.adoc[]

[[admin-people-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/people/list/links.adoc[]



[[admin-publication]]
===== Publication

include::{snippets}/application/admin/publications/path-request.adoc[]
[[admin-publication-details]]
====== Details
include::{snippets}/application/admin/publications/response-fields.adoc[]

[[admin-publication-links]]
====== Resource Links
include::{snippets}/application/admin/publications/links.adoc[]

[[admin-publication-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/publications/list/links.adoc[]

[[admin-publication-types]]
===== Publication Types

include::{snippets}/application/admin/publication-types/path-request.adoc[]
[[admin-publication-types-details]]
====== Details
include::{snippets}/application/admin/publication-types/response-fields.adoc[]

[[admin-publication-types-links]]
====== Resource Links
include::{snippets}/application/admin/publication-types/links.adoc[]

[[admin-publication-types-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/publication-types/list/links.adoc[]

[[admin-system-properties]]
===== System properties

A system property is defined by the back-end administrators, for the whole system.

include::{snippets}/application/admin/system-properties/path-request.adoc[]
[[admin-system-properties-details]]
====== Details
include::{snippets}/application/admin/system-properties/response-fields.adoc[]

[[admin-system-properties-links]]

[[admin-users]]
===== Users

A user represents a user of the platform. A person and roles are associated to it.

include::{snippets}/application/admin/users/path-request.adoc[]
[[admin-users-details]]
====== Details
include::{snippets}/application/admin/users/response-fields.adoc[]

[[admin-users-links]]
====== Resource Links
include::{snippets}/application/admin/users/links.adoc[]

[[admin-users-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/admin/users/list/links.adoc[]
//
// Admin module END
//
endif::admin[]
s
