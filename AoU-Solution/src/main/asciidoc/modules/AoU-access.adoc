ifdef::access[]
//
// Access module BEGIN
//
[[access]]
=== Access Module

[[access-response-structure]]
==== Response Structure

include::{snippets}/application/access/response-fields.adoc[]
include::{snippets}/application/access/links.adoc[]

[[access-resources]]
==== Resources

[[access-metadata]]
===== Metadata

include::{snippets}/application/access/metadata/path-request.adoc[]

[[access-metadata-details]]
====== Details
include::{snippets}/application/access/metadata/response-fields.adoc[]

[[access-metadata-links]]
====== Resource Links
include::{snippets}/application/access/metadata/links.adoc[]

[[access-metadata-list]]
====== Collection Links
include::{includedirectory}/resources/collection.adoc[]
include::{snippets}/application/access/metadata/list/links.adoc[]

//
// Access module END
//
endif::access[]
