A sub-resource is a embedded or linked resource to a main resource (i.e. a REST resource).

[[change-info]]
=== Change Information Detail
include::{includedirectory}/resources/change-info.adoc[]

