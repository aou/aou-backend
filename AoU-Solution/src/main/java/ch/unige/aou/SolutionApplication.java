/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Solution - SolutionApplication.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import ch.unige.solidify.util.SolidifyApplication;

@SpringBootApplication(scanBasePackages = { "ch.unige.aou", "ch.unige.solidify" })
@EntityScan(basePackages = { "ch.unige.aou.converter", "ch.unige.aou.model", "ch.unige.solidify" })
@EnableJpaRepositories(basePackages = { "ch.unige.solidify.repository", "ch.unige.aou.repository" })
@EnableCaching
public class SolutionApplication extends SolidifyApplication {
  static {
    applicationName = SolutionApplication.class;
  }

  public static void main(String[] args) {
    SolidifyApplication.launch(args);
  }
}
