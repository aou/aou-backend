<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>ch.unige.solidify</groupId>
        <artifactId>solidify-parent</artifactId>
        <version>3.1.4</version>
        <relativePath />
    </parent>

    <groupId>ch.unige.aou</groupId>
    <artifactId>AoU-Parent</artifactId>
    <name>AoU Parent</name>
    <version>2.2.0-SNAPSHOT</version>
    <packaging>pom</packaging>
    <description>AoU Parent project. Manage dependencies and version for AoU modules/applications</description>
    <inceptionYear>2022</inceptionYear>
    <url>https://gitlab.unige.ch/aou/aou-backend</url>

    <licenses>
        <license>
            <name>GNU General Public License v2.0 or later</name>
            <url>https://spdx.org/licenses/GPL-2.0-or-later.html</url>
            <comments>SPDX-License-Identifier: GPL-2.0-or-later</comments>
        </license>
    </licenses>

    <modules>
        <!-- AoU Shared Modules -->
        <module>AoU-Model</module>
        <module>AoU-Common</module>
        <module>AoU-ResourceServerSecurity</module>
        <module>AoU-ResourceServerCommon</module>
        <!-- AoU Modules -->
        <module>AoU-Admin</module>
        <module>AoU-Access</module>
        <module>AoU-DataMgmt</module>
        <!-- AoU Clients -->
        <module>AoU-Client</module>
        <!-- AoU Tests -->
        <module>AoU-IntegrationTests</module>
        <module>AoU-IntegrationTestsRunner</module>
        <!-- AoU Applications -->
        <module>AoU-Solution</module>
    </modules>

    <properties>
        <!--General properties -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.build.timestamp.format>yyyy-MM-dd</maven.build.timestamp.format>
        <!-- Solidify Authorization Client properties -->
        <solidify.authorization.client.version>2.1.0</solidify.authorization.client.version>
        <!--AoU properties -->
        <aou.version>2.2.0-SNAPSHOT</aou.version>
        <aou.license>${solidify.license}</aou.license>
        <aou.tool.openapi.version>1.3.3</aou.tool.openapi.version>
        <aou.tool.asciidoctor.toc.version>1.3.3</aou.tool.asciidoctor.toc.version>
        <!--Libraries properties -->
        <fedora.repository.version>3.8.1</fedora.repository.version>
        <asciidoctor-maven-plugin.version>2.2.2</asciidoctor-maven-plugin.version>
        <asciidoctorj-pdf.version>2.0.8</asciidoctorj-pdf.version>
        <properties-maven-plugin.version>1.0.0</properties-maven-plugin.version>
        <commons-validator.version>1.7</commons-validator.version>
        <jna-platform.version>5.11.0</jna-platform.version>
        <skipTests>false</skipTests>
        <skipDocs>false</skipDocs>
        <skipTools>false</skipTools>
        <snippets.baseDir>${project.build.directory}/generated-snippets</snippets.baseDir>
        <documentation.host>localhost</documentation.host>
        <documentation.basePath />
        <documentation.specification>OPENAPI_V3</documentation.specification>
        <documentation.scheme>http</documentation.scheme>
        <documentation.server.servlet.context-path>/aou</documentation.server.servlet.context-path>
        <documentation.format>JSON</documentation.format>
        <itextpdf.version>7.2.5</itextpdf.version>
        <html2pdf.version>4.0.5</html2pdf.version>
        <!-- Mandatory property to prevent inheritance of the parent property. If not set the module in Sonar will be the same as the parent module -->
        <sonar.projectName>${project.name}</sonar.projectName>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!--AoU dependencies -->
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-Model</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-Common</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-ResourceServerSecurity</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-ResourceServerCommon</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-Admin</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-Access</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-DataMgmt</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-Client</artifactId>
                <version>${aou.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-IntegrationTests</artifactId>
                <version>${aou.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>ch.unige.aou</groupId>
                <artifactId>AoU-IntegrationTestsRunner</artifactId>
                <version>${aou.version}</version>
                <scope>test</scope>
            </dependency>
            <!-- Used for validating URLs -->
            <dependency>
                <groupId>commons-validator</groupId>
                <artifactId>commons-validator</artifactId>
                <version>${commons-validator.version}</version>
            </dependency>
            <!-- Spring Cloud Config Client -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-config-client</artifactId>
                <version>${spring-cloud-config-client.version}</version>
            </dependency>
            <!-- HTML2PDF -->
            <dependency>
                <groupId>com.itextpdf</groupId>
                <artifactId>kernel</artifactId>
                <version>${itextpdf.version}</version>
            </dependency>
            <dependency>
                <groupId>com.itextpdf</groupId>
                <artifactId>html2pdf</artifactId>
                <version>${html2pdf.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- Solidify-Authorization-Client -->
        <dependency>
            <groupId>ch.unige.solidify</groupId>
            <artifactId>solidify-authorization-client</artifactId>
            <version>${solidify.authorization.client.version}</version>
        </dependency>
        <!-- Spring Boot starter -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
        <!-- Generate Spring configuration metadata file -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
        <!-- Java Native Access allowing JDBC MariaDB driver to use Unix socket -->
        <dependency>
            <groupId>net.java.dev.jna</groupId>
            <artifactId>jna-platform</artifactId>
            <version>${jna-platform.version}</version>
        </dependency>
    </dependencies>
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>io.github.git-commit-id</groupId>
                    <artifactId>git-commit-id-maven-plugin</artifactId>
                    <configuration>
                        <dotGitDirectory>${project.basedir}/../.git</dotGitDirectory>
                        <offline>true</offline>
                        <verbose>false</verbose>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.asciidoctor</groupId>
                    <artifactId>asciidoctor-maven-plugin</artifactId>
                    <version>${asciidoctor-maven-plugin.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>org.asciidoctor</groupId>
                            <artifactId>asciidoctorj-pdf</artifactId>
                            <version>${asciidoctorj-pdf.version}</version>
                        </dependency>
                    </dependencies>
                    <executions>
                        <execution>
                            <id>generate-docs</id>
                            <phase>prepare-package</phase>
                            <goals>
                                <goal>process-asciidoc</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>generate-pdf-doc</id>
                            <phase>prepare-package</phase>
                            <goals>
                                <goal>process-asciidoc</goal>
                            </goals>
                            <configuration>
                                <backend>pdf</backend>
                                <attributes>
                                    <icons>font</icons>
                                    <pagenums />
                                    <toc />
                                </attributes>
                            </configuration>
                        </execution>
                    </executions>
                    <configuration>
                        <skip>${skipDocs}</skip>
                        <sourceDirectory>${project.basedir}/src/main/docs</sourceDirectory>
                        <backend>xhtml</backend>
                        <doctype>book</doctype>
                        <attributes>
                            <version>${project.version}</version>
                            <builddate>${maven.build.timestamp}</builddate>
                            <snippets>${snippets.baseDir}</snippets>
                            <includedirectory>${project.basedir}/src/main/asciidoc</includedirectory>
                            <includeDirectoryForExternalFiles>${project.basedir}/target/asciidoc</includeDirectoryForExternalFiles>
                            <imagesdir>images</imagesdir>
                        </attributes>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>properties-maven-plugin</artifactId>
                    <version>${properties-maven-plugin.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <configuration>
                    <licenseName>${aou.license}</licenseName>
                    <descriptionTemplate>./aouTemplate.ftl</descriptionTemplate>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <scm>
        <connection>scm:git:git@gitlab.unige.ch:aou/aou-backend.git</connection>
        <tag>HEAD</tag>
        <url>https://gitlab.unige.ch/aou/aou-backend</url>
    </scm>

    <organization>
        <name>University of Geneva</name>
        <url>https://www.unige.ch</url>
    </organization>

</project>

