/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - DocumentFileAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;

class DocumentFileAsRootIT extends AbstractDocumentFileIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void testGetFulltext() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFile documentFile = this.uploadDocumentFile(publication, new ClassPathResource(TEST_FILE_TO_UPLOAD));
    assertNotNull(documentFile);

    String documentFileResId = documentFile.getResId();
    String text = assertDoesNotThrow(() -> this.documentFileService.fulltext(documentFileResId));
    assertTrue(text.contains("Lorem ipsum dolor  sit  amet,  consectetur  adipisicing elit"));

    // try a second time to test cache
    text = assertDoesNotThrow(() -> this.documentFileService.fulltext(documentFileResId));
    assertTrue(text.contains("Lorem ipsum dolor  sit  amet,  consectetur  adipisicing elit"));
  }
}
