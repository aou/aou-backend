/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - SpiderIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.auth;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.PATCH;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

@Disabled("Run this manually")
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = { "spring.cloud.bootstrap.enabled=true" })
class SpiderIT {
  private static final Logger log = LoggerFactory.getLogger(SpiderIT.class);
  private final Pattern urlPattern = Pattern.compile("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
  private final List<HttpMethod> httpMethodToTest = Arrays.asList(GET, POST, PUT, PATCH, DELETE);
  private final File resultFile = new File("authResults.log");
  private final String authorizationUrl = "http://localhost:7777/aou/";
  private final String administrationUrl = "http://localhost:20200/aou/";
  private final String baseURL = "http://localhost";
  @Autowired
  private Environment env;
  private String userToken;
  private final List<String> applicationList = Arrays.asList(this.authorizationUrl, this.administrationUrl);

  @BeforeEach
  public void init() {
    this.userToken = this.env.getProperty("solidify.oauth2.accesstoken.root");
  }

  @Test
  void testApplication() {
    final Deque<String> toFetchURLs = new ArrayDeque<>();
    final Set<String> visitedURLs = new HashSet<>();
    this.resultFile.delete();
    for (String applicationUrl : this.applicationList) {
      toFetchURLs.push(applicationUrl);
      while (!toFetchURLs.isEmpty()) {
        final String resourceURL = toFetchURLs.pop();
        try {
          final PrintWriter printWriter = new PrintWriter(new FileWriter(this.resultFile, true), true);
          for (final HttpMethod httpMethod : this.httpMethodToTest) {
            final ClientResponse clientResponse = this.callURL(applicationUrl, resourceURL, httpMethod);
            this.registerResults(clientResponse, httpMethod, resourceURL, printWriter);
            visitedURLs.add(resourceURL);
            List<String> extractedURLs = this.extractURLs(clientResponse);
            extractedURLs.removeAll(visitedURLs);
            extractedURLs.removeAll(toFetchURLs);
            toFetchURLs.addAll(extractedURLs);
          }
        } catch (IOException e) {
          log.error("Error during testing {}", applicationUrl, e);
        }
      }
    }
  }

  private ClientResponse callURL(String applicationUrl, String resourceURL, HttpMethod httpMethod) {
    final WebClient client = this.buildWebClient(applicationUrl);
    final Consumer<HttpHeaders> headersConsumer = headers -> headers.setBearerAuth(this.userToken);
    final Object objectToSend = new Object();
    ClientResponse clientResponse;
    if (httpMethod.equals(POST) || httpMethod.equals(PATCH)) {
      clientResponse = this.restCallWithBody(client, httpMethod, resourceURL, headersConsumer, objectToSend);
    } else {
      clientResponse = this.restCallWithoutBody(client, httpMethod, resourceURL, headersConsumer);
    }
    return clientResponse;
  }

  private List<String> extractURLs(ClientResponse clientResponse) {
    final List<String> extractedUrls = new ArrayList<>();
    final String body = clientResponse.bodyToMono(String.class).block();
    if (body == null) {
      return extractedUrls;
    }
    final Matcher matcher = this.urlPattern.matcher(body);
    while (matcher.find()) {
      final String url = matcher.group();
      if (url.startsWith(this.baseURL)) {
        extractedUrls.add(matcher.group());
      }
    }
    return extractedUrls;
  }

  private ClientResponse restCallWithBody(WebClient client, HttpMethod httpMethod, String uri, Consumer<HttpHeaders> headersConsumer,
          Object objectToSend) {
    return client.method(httpMethod).uri(uri).headers(headersConsumer).bodyValue(objectToSend).exchange().block();
  }

  private ClientResponse restCallWithoutBody(WebClient client, HttpMethod httpMethod, String uri, Consumer<HttpHeaders> headersConsumer) {
    return client.method(httpMethod).uri(uri).headers(headersConsumer).exchange().block();
  }

  private WebClient buildWebClient(String applicationUrl) {
    final ConnectionProvider fixedPool = ConnectionProvider.builder("fixedPool")
            .maxConnections(20000)
            .pendingAcquireTimeout(Duration.ofMinutes(3))
            .build();
    return WebClient.builder()
            .baseUrl(applicationUrl)
            .clientConnector(new ReactorClientHttpConnector(HttpClient.create(fixedPool)))
            .codecs(clientCodecConfigurer -> clientCodecConfigurer
                    .defaultCodecs()
                    .jackson2JsonEncoder(new Jackson2JsonEncoder(
                            // Allow to send empty Json object for test purposes
                            new ObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false), MediaType.APPLICATION_JSON)))
            .build();
  }

  private void registerResults(ClientResponse clientResponse, HttpMethod httpMethod, String resourceURL, PrintWriter printWriter) {
    final String output = httpMethod + " " + resourceURL + " " + clientResponse.statusCode().value();
    System.out.println(output);
    printWriter.println(output);
  }

}
