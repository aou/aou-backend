/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - LicenseGroupAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.model.settings.LicenseGroup;

public class LicenseGroupAsAdminIT extends LicenseIT {
  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void findAllTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    // Find all
    final List<LicenseGroup> actualLicenseGroups = this.licenseGroupService.findAll();

    // Test at least
    assertFalse(actualLicenseGroups.isEmpty());
    assertTrue(actualLicenseGroups.stream().anyMatch(group -> group.getResId().equals(licenseGroup.getResId())));
  }

  @Test
  void creationTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    final LicenseGroup foundLicenseGroup = this.licenseGroupService.findOne(licenseGroup.getResId());
    assertNotNull(foundLicenseGroup);
    this.assertsLicenseGroup(licenseGroup, foundLicenseGroup);
  }

  @Test
  void deleteTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    LicenseGroup foundLicenseGroup = this.licenseGroupService.findOne(licenseGroup.getResId());
    assertNotNull(foundLicenseGroup);

    this.licenseGroupService.delete(licenseGroup.getResId());

    final String resId = licenseGroup.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.licenseGroupService.findOne(resId));
  }

  @Test
  void updateTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    LicenseGroup foundLicenseGroup = this.licenseGroupService.findOne(licenseGroup.getResId());
    assertNotNull(foundLicenseGroup);

    String newName = this.getTemporaryTestLabel("updated");
    foundLicenseGroup.setName(newName);

    LicenseGroup updatedLicenseGroup = this.licenseGroupService.update(foundLicenseGroup.getResId(), foundLicenseGroup);

    LicenseGroup refoundLicenseGroup = this.licenseGroupService.findOne(licenseGroup.getResId());
    assertNotNull(refoundLicenseGroup);

    assertEquals(newName, refoundLicenseGroup.getName());
  }

}
