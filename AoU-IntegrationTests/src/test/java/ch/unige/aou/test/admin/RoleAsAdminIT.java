/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - RoleAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.security.Role;
import ch.unige.aou.test.AouTestConstants;

class RoleAsAdminIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    final Role role = new Role();
    role.setName(AouTestConstants.getRandomNameWithTemporaryLabel("Role"));

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.roleService.create(role);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  private Role createRoleAsRoot(String name) {
    Role role = new Role();
    role.setName(AouTestConstants.getRandomNameWithTemporaryLabel(name));

    this.restClientTool.sudoRoot();
    Role roleCreated = this.roleService.create(role);
    this.restClientTool.exitSudo();

    assertNotNull(roleCreated);

    return roleCreated;
  }

  private void deleteRoleAsRoot(Role role) {
    this.restClientTool.sudoRoot();
    this.roleService.delete(role.getResId());
    this.restClientTool.exitSudo();
  }

  @Test
  void deleteTest() {
    Role role = this.createRoleAsRoot("Role for admin delete test");

    // Delete
    final String resId = role.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.roleService.delete(resId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    this.deleteRoleAsRoot(role);
  }

  @Test
  void findAllTest() {
    List<Role> actualRoles = this.roleService.findAll();
    assertEquals(0, actualRoles.size());

    Role role = this.createRoleAsRoot("Role for admin find all test");

    // Find all
    actualRoles = this.roleService.findAll();

    // Test all roles
    assertEquals(1, actualRoles.size());

    this.deleteRoleAsRoot(role);
  }

  @Test
  void updateTest() {
    Role role = this.createRoleAsRoot("Role for admin update test");

    // Update the role name
    final String newRoleName = AouTestConstants.getRandomNameWithTemporaryLabel("New Role name set by admin");
    role.setName(newRoleName);

    String localRoleId = role.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.roleService.update(localRoleId, role);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    this.deleteRoleAsRoot(role);
  }
}
