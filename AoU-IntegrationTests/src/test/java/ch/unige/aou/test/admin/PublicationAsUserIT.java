/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.display.StructureWithJoinInfosDTO;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationStructure;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

class PublicationAsUserIT extends PublicationIT {

  private final String CONTRIBUTOR1_LASTNAME = this.getTemporaryTestLabel("Doe");
  private final String CONTRIBUTOR1_FIRSTNAME = this.getTemporaryTestLabel("John");
  private final String CONTRIBUTOR1_CN_INDIVIDU = "1234-1234-1234-1555";

  private final String CONTRIBUTOR2_LASTNAME = this.getTemporaryTestLabel("Mou");
  private final String CONTRIBUTOR2_FIRSTNAME = this.getTemporaryTestLabel("Mika");
  private final String CONTRIBUTOR2_CN_INDIVIDU = "1234-1234-1234-2222";

  private final String CONTRIBUTOR3_LASTNAME = this.getTemporaryTestLabel("Levert");
  private final String CONTRIBUTOR3_FIRSTNAME = this.getTemporaryTestLabel("Albert");

  private final String CONTRIBUTOR4_LASTNAME = this.getTemporaryTestLabel("Cow");
  private final String CONTRIBUTOR4_FIRSTNAME = this.getTemporaryTestLabel("Harry");
  private final String CONTRIBUTOR4_CN_INDIVIDU = "1234-1234-1234-8888";

  private final String CONTRIBUTOR5_LASTNAME = this.getTemporaryTestLabel("Lee");
  private final String CONTRIBUTOR5_FIRSTNAME = this.getTemporaryTestLabel("Broco");

  private final String CONTRIBUTOR6_LASTNAME = this.getTemporaryTestLabel("Terminator");
  private final String CONTRIBUTOR6_FIRSTNAME = this.getTemporaryTestLabel("Spiderman");

  @Override
  protected String getCurrentUserId() {
    return this.TEST_USER_USER_ID;
  }

  @Test
  void createTest() {
    String title = "test as user publication";
    this.testPublicationCreation(title);
  }

  @Test
  void createWithTitleCleaningTest() {
    String title = "     test    as user publication.    ";
    String expectedTitle = "test as user publication";
    this.testPublicationCreation(title, expectedTitle);
  }

  @Test
  void createFromXmlMetadata() {
    String title = this.getTemporaryTestLabel("test as user publication");
    String personId = this.profilePerson.getResId();

    Publication publication = new Publication();
    String xmlMetadata = String.format(AouConstants.TEST_DEPOSIT_XML_METADATA_TEMPLATE_2_3, title);
    publication.setMetadata(xmlMetadata);
    Publication createdPublication = this.publicationService.create(publication);
    assertNotNull(createdPublication);
    assertEquals(this.PUBLICATION_SUBTYPE_THESE_ID, createdPublication.getSubtype().getResId());
    assertEquals(title, createdPublication.getTitle());
    assertEquals(personId, createdPublication.getCreator().getResId());
  }

  @Test
  void createFromXmlMetadataWithTitleCleaning() {
    String title = this.getTemporaryTestLabel("     test  \n  as user publication.   \n ");
    String expectedTitle = this.getTemporaryTestLabel("test as user publication");
    String personId = this.profilePerson.getResId();

    Publication publication = new Publication();
    String xmlMetadata = String.format(AouConstants.TEST_DEPOSIT_XML_METADATA_TEMPLATE_2_3, title);
    publication.setMetadata(xmlMetadata);
    Publication createdPublication = this.publicationService.create(publication);
    assertNotNull(createdPublication);
    assertEquals(this.PUBLICATION_SUBTYPE_THESE_ID, createdPublication.getSubtype().getResId());
    assertEquals(expectedTitle, createdPublication.getTitle());
    assertEquals(personId, createdPublication.getCreator().getResId());
  }

  @Test
  void createWithValidSubSubtypeTest() {
    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);
    String personId = this.profilePerson.getResId();

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID, createdPublication.getSubtype().getResId());
    assertEquals(this.PUBLICATION_SUBSUBTYPE_LETTRE_ID, createdPublication.getSubSubtype().getResId());
    assertEquals(title, createdPublication.getTitle());
    this.assertEqualsFormData(formData, createdPublication.getFormData());
    assertEquals(personId, createdPublication.getCreator().getResId());

    Publication foundPublication = this.publicationService.findOne(createdPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID, foundPublication.getSubtype().getResId());
    assertEquals(this.PUBLICATION_SUBSUBTYPE_LETTRE_ID, foundPublication.getSubSubtype().getResId());
    assertEquals(title, foundPublication.getTitle());
    this.assertEqualsFormData(formData, foundPublication.getFormData());
    assertEquals(personId, createdPublication.getCreator().getResId());
  }

  @Test
  void createWithInvalidSubSubtypeTest() {
    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);

    // A2-ARTICLE is invalid for the subtype Article scientifique which is the value in AouConstants.TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE
    formData = formData.replace("A1-LETTRE", "A2-BILLET-DE-BLOG");

    try {
      this.createNewTemporaryPublication(formData);
      fail("A BAD REQUEST response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void createWithValidStructureTest() {
    String title = this.getTemporaryTestLabel("test structure in publication as user");
    String formData = this.getTestFormData(title);

    //add the structure into the form data
    Structure structureToFill = this.getTopStructure();
    String structuresJson = String.format(AouConstants.STRUCTURE_JSON_V2_1, structureToFill.getResId());

    formData = formData.replace("\"description\": {", "\"description\": { " + structuresJson);

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);
    createdPublication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    List<StructureWithJoinInfosDTO> structurePublications = this.publicationService.getStructures(createdPublication.getResId());

    assertTrue(structurePublications.stream().anyMatch(st -> st.getName().equals(structureToFill.getName())));
    assertTrue(structurePublications.stream().anyMatch(st -> st.getCodeStruct().equals(structureToFill.getCodeStruct())));

    this.assertEqualsFormData(formData, createdPublication.getFormData());

    // check that the structure's dewey code is stored as a classification in Json metadata
    JSONObject createdPublicationFormData = new JSONObject(createdPublication.getFormData());
    assertEquals("Dewey",
            createdPublicationFormData.getJSONObject("description").getJSONArray("classifications").getJSONObject(0).getString("code"));
    assertEquals(this.TEST_DEWEY_CODE,
            createdPublicationFormData.getJSONObject("description").getJSONArray("classifications").getJSONObject(0).getString("item"));
  }

  @Test
  void createWithInvalidStructureTest() {
    String title = this.getTemporaryTestLabel("test invalid structure in publication as user");
    String formData = this.getTestFormData(title);

    //add the structure into the form data
    String structuresJson = String.format(AouConstants.STRUCTURE_JSON_V2_1, "xxxx");

    formData = formData.replace("\"description\": {", "\"description\": { " + structuresJson);

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);
    createdPublication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);

    final String createdPublicationId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.update(createdPublicationId, createdPublication);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Structure not found");
  }

  @Test
  void createWithValidResearchGroupTest() {
    String title = this.getTemporaryTestLabel("test research group in publication as user");
    String formData = this.getTestFormData(title);

    this.restClientTool.sudoRoot();
    ResearchGroup researchGroup1 = this.createResearchGroupFixture("research group 1", "12345");
    ResearchGroup researchGroup2 = this.createResearchGroupFixture("research group 2", "23456");
    this.restClientTool.exitSudo();

    //add the research groups into the form data
    formData = this.addResearchGroupInFormData(formData, researchGroup1.getResId());
    formData = this.addResearchGroupInFormData(formData, researchGroup2.getResId());

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);

    //In order to validate the research groups
    createdPublication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    List<ResearchGroup> researchGroups = this.getResearchGroupsList(createdPublication.getResId());

    assertEquals(2, researchGroups.size());
    assertTrue(researchGroups.stream().anyMatch(st -> st.getName().equals(researchGroup1.getName())));
    assertTrue(researchGroups.stream().anyMatch(st -> st.getCode().equals(researchGroup1.getCode())));
    assertTrue(researchGroups.stream().anyMatch(st -> st.getName().equals(researchGroup2.getName())));
    assertTrue(researchGroups.stream().anyMatch(st -> st.getCode().equals(researchGroup2.getCode())));
    this.assertEqualsFormData(formData, createdPublication.getFormData());
  }

  @Test
  void createWithSameNameResearchGroupsTest() {
    String title = this.getTemporaryTestLabel("test research group in publication as user");
    String formData = this.getTestFormData(title);

    this.restClientTool.sudoRoot();
    ResearchGroup researchGroup1 = this.createResearchGroupFixture("research group test", "12345", false, true);
    ResearchGroup researchGroup2 = this.createResearchGroupFixture("research group test", "23456", false, false);
    this.restClientTool.exitSudo();

    //add the research groups into the form data
    formData = this.addResearchGroupInFormData(formData, researchGroup1.getResId());
    formData = this.addResearchGroupInFormData(formData, researchGroup2.getResId());

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);

    //In order to validate the research groups
    createdPublication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    List<ResearchGroup> researchGroups = this.getResearchGroupsList(createdPublication.getResId());

    assertEquals(2, researchGroups.size());
    assertTrue(researchGroups.stream().anyMatch(st -> st.getName().equals(researchGroup1.getName())));
    assertTrue(researchGroups.stream().anyMatch(st -> st.getCode().equals(researchGroup1.getCode())));
    assertTrue(researchGroups.stream().anyMatch(st -> st.getName().equals(researchGroup2.getName())));
    assertTrue(researchGroups.stream().anyMatch(st -> st.getCode().equals(researchGroup2.getCode())));
    this.assertEqualsFormData(formData, createdPublication.getFormData());
  }

  @Test
  void createWithInvalidResearchGroupTest() {
    String title = this.getTemporaryTestLabel("test invalid research group in publication as user");
    String formData = this.getTestFormData(title);

    Publication createdPublication = this.createNewTemporaryPublication(formData);

    //In order to validate the research groups
    createdPublication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);

    //add the research group into the form data
    formData = this.addResearchGroupInFormData(formData, "555555555555");
    createdPublication.setFormData(formData);

    final String createdPublicationId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.update(createdPublicationId, createdPublication);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Research group not found");
  }

  @Test
  void createWithValidAbstractTest() {
    String title = this.getTemporaryTestLabel("test abstract in publication as user");
    String formData = this.getTestFormData(title);

    String text = "I am en english abstract without language";
    formData = this.addAbstractInFormData(formData, text, null);

    // create (doesn't validate abstracts)
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);

    // update (validate abstracts)
    createdPublication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication updatedPublication = this.publicationService.update(createdPublication.getResId(), createdPublication);
    assertNotNull(updatedPublication);
    JSONObject formDataJsonObject = new JSONObject(updatedPublication.getFormData());
    assertEquals(text, formDataJsonObject.getJSONObject("description").getJSONArray("abstracts").getJSONObject(0).getString("text"));
    assertTrue(formDataJsonObject.getJSONObject("description").getJSONArray("abstracts").getJSONObject(0).isNull("lang"));
  }

  @Test
  void createWithAbstractMissingLangTest() {
    String title = this.getTemporaryTestLabel("test abstract in publication as user");
    String formData = this.getTestFormData(title);

    formData = this.addAbstractInFormData(formData, "abstract 1", "ENG");
    formData = this.addAbstractInFormData(formData, "abstract 2", null);

    // create (doesn't validate abstracts)
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);

    // update (validate abstracts)
    createdPublication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    final String createdPublicationId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.update(createdPublicationId, createdPublication);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.abstracts.1.lang", "This field cannot be empty", validationErrors));
  }

  @Test
  void createWithAbstractDoubleLangTest() {
    String title = this.getTemporaryTestLabel("test abstract in publication as user");
    String formData = this.getTestFormData(title);

    formData = this.addAbstractInFormData(formData, "abstract 1", "ENG");
    formData = this.addAbstractInFormData(formData, "abstract 2", "FRE");
    formData = this.addAbstractInFormData(formData, "abstract 3", "ENG");

    // create (doesn't validate abstracts)
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);

    // update (validate abstracts)
    createdPublication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    final String createdPublicationId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.update(createdPublicationId, createdPublication);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.abstracts.2.lang", "This language appears more than once", validationErrors));
  }

  @Test
  void createWithAbstractMissingContentTest() {
    String title = this.getTemporaryTestLabel("test abstract in publication as user");
    String formData = this.getTestFormData(title);

    formData = this.addAbstractInFormData(formData, "", "ENG");

    // create (doesn't validate abstracts)
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);

    // update (validate abstracts)
    createdPublication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    final String createdPublicationId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.update(createdPublicationId, createdPublication);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.abstracts.0.text", "This field cannot be empty", validationErrors));
  }

  @Test
  void createThesisWithoutAbstract() {
    String title = this.getTemporaryTestLabel("test no abstract on thesis");
    String formData = this.getTestFormData(title);

    formData = formData.replace("\"subsubtype\": \"A1-LETTRE\",", "");
    formData = formData.replace("\"subtype\": \"A1\",", "\"subtype\": \"D1\",");
    formData = formData.replace("\"type\": \"ARTICLE\"", "\"type\": \"DIPLOME\"");

    // create (doesn't validate abstracts)
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication);

    // update (validate abstracts)
    createdPublication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    final String createdPublicationId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.update(createdPublicationId, createdPublication);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.abstracts.0.text", "An abstract is mandatory for diploma type deposits",
            validationErrors));
    assertTrue(this.validationErrorsContain("description.abstracts.0.lang", "An abstract is mandatory for diploma type deposits",
            validationErrors));
  }

  @Test
  void createWithValidDatesTest() {

    int currentYear = LocalDate.now().getYear();
    int maxYear = currentYear + 10;

    String[] validDates = new String[] { "01.05.2021", "05.2021", String.format("01.01.%s", currentYear), "01.01.1850",
            String.valueOf(currentYear), "1801" };

    for (String validDate : validDates) {

      String title = this.getTemporaryTestLabel("test valid date in publication as user");
      String formData = this.getTestFormData(title);
      formData = this.addDateInFormData(formData, "FIRST_ONLINE", validDate);

      // create (doesn't validate dates)
      Publication createdPublication = this.createNewTemporaryPublication(formData);
      assertNotNull(createdPublication);

      // update (validate dates)
      createdPublication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
      String createdPublicationId = createdPublication.getResId();
      assertDoesNotThrow(() -> {
        this.publicationService.update(createdPublicationId, createdPublication);
      }, String.format("'%s' should be considered as valid", validDate));
    }
  }

  @Test
  void createWithInvalidDatesTest() {

    int maxYear = LocalDate.now().getYear() + 10;
    int invalidYear = maxYear + 1;
    String[] invalidDates = new String[] { "xx.05.2021", "15.2021", "05.2021xx", "05-2021", "2021-05", "01.01.05.21", "05.21", "05..2021",
            "05.x05x.2021", String.format("01.05.%s", invalidYear), String.valueOf(invalidYear), "1", "01", "05" };

    for (String invalidDate : invalidDates) {

      String title = this.getTemporaryTestLabel("test invalid date in publication as user");
      String formData = this.getTestFormData(title);
      formData = this.addDateInFormData(formData, "FIRST_ONLINE", invalidDate);

      // create (doesn't validate dates)
      Publication createdPublication = this.createNewTemporaryPublication(formData);
      assertNotNull(createdPublication);

      // update (validate dates)
      createdPublication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
      final String createdPublicationId = createdPublication.getResId();
      HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
        this.publicationService.update(createdPublicationId, createdPublication);
      }, String.format("'%s' should be considered as invalid", invalidDate));
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
      assertTrue(this.validationErrorsContain("description.dates.first_online", "This date is not a valid date", validationErrors));
    }
  }

  @Test
  void createAndFilterWithDOITest() {
    String doi1 = "10.1007/978-3-030-53705-0_32";
    String doi2 = "10.1007/978-3-030-53705-0_35";

    // create 1
    String title = this.getTemporaryTestLabel("test DOI in publication as user 1");
    String formData = this.getTestFormData(title);
    formData = this.addDoiInFormData(formData, doi1);
    Publication createdPublication1 = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication1);
    assertEquals(doi1, createdPublication1.getDoi());
    assertNull(createdPublication1.getPmid());

    // create 2
    title = this.getTemporaryTestLabel("test DOI in publication as user 2");
    formData = this.getTestFormData(title);
    formData = this.addDoiInFormData(formData, doi2);
    Publication createdPublication2 = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication2);
    assertEquals(doi2, createdPublication2.getDoi());
    assertNull(createdPublication2.getPmid());

    // search by DOI 1
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("doi", doi1);
    List<Publication> publications = this.publicationService.searchByProperties(queryParams);
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(createdPublication1.getResId(), publications.get(0).getResId());
    assertEquals(doi1, publications.get(0).getDoi());

    // search by DOI 2
    queryParams = new HashMap<>();
    queryParams.put("doi", doi2);
    publications = this.publicationService.searchByProperties(queryParams);
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(createdPublication2.getResId(), publications.get(0).getResId());
    assertEquals(doi2, publications.get(0).getDoi());

    // search by DOI part
    queryParams = new HashMap<>();
    queryParams.put("doi", doi1.substring(0, 26));
    publications = this.publicationService.searchByProperties(queryParams);
    assertNotNull(publications);
    assertEquals(2, publications.size());
  }

  @Test
  void createAndFilterWithPMIDTest() {
    String pmid1 = "111888999444";
    String pmid2 = "111888999555";

    // create 1
    String title = this.getTemporaryTestLabel("test PMID in publication as user 1");
    String formData = this.getTestFormData(title);
    formData = this.addPmidInFormData(formData, pmid1);
    Publication createdPublication1 = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication1);
    assertEquals(pmid1, createdPublication1.getPmid());
    assertNull(createdPublication1.getDoi());

    // create 2
    title = this.getTemporaryTestLabel("test PMID in publication as user 2");
    formData = this.getTestFormData(title);
    formData = this.addPmidInFormData(formData, pmid2);
    Publication createdPublication2 = this.createNewTemporaryPublication(formData);
    assertNotNull(createdPublication2);
    assertEquals(pmid2, createdPublication2.getPmid());
    assertNull(createdPublication2.getDoi());

    // search by PMID 1
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("pmid", pmid1);
    List<Publication> publications = this.publicationService.searchByProperties(queryParams);
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(createdPublication1.getResId(), publications.get(0).getResId());
    assertEquals(pmid1, publications.get(0).getPmid());

    // search by PMID 2
    queryParams = new HashMap<>();
    queryParams.put("pmid", pmid2);
    publications = this.publicationService.searchByProperties(queryParams);
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(createdPublication2.getResId(), publications.get(0).getResId());
    assertEquals(pmid2, publications.get(0).getPmid());

    // search by PMID part
    queryParams = new HashMap<>();
    queryParams.put("pmid", pmid1.substring(0, 9));
    publications = this.publicationService.searchByProperties(queryParams);
    assertNotNull(publications);
    assertEquals(2, publications.size());
  }

  @Test
  void updateTest() {
    Publication createdPublication = this.testPublicationCreation("test publication update");

    String titleUpdate = this.getTemporaryTestLabel("Yet another title");
    createdPublication.setFormData(this.getTestFormData(titleUpdate));
    Publication updatePublication = this.publicationService.update(createdPublication.getResId(), createdPublication);
    assertNotNull(createdPublication);
    assertEquals(updatePublication.getTitle(), titleUpdate);
  }

  @Test
  void updateThesisPreserveDoi() {
    String unigeDoi = "10.13097/archive-ouverte/unige-1612075";

    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "Preserve DOI test");
    properties.put("type", this.PUBLICATION_TYPE_DIPLOME_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_THESE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    properties.put("doi", unigeDoi);
    List<ContributorDTO> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);
    ContributorDTO contributorDto2 = new ContributorDTO();
    contributorDto2.setFirstName(this.CONTRIBUTOR2_FIRSTNAME);
    contributorDto2.setLastName(this.CONTRIBUTOR2_LASTNAME);
    contributorDto2.setRole("director");
    contributorDto2.setCnIndividu(this.CONTRIBUTOR2_CN_INDIVIDU);
    contributorDTOs.add(contributorDto2);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);
    Publication createdPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(createdPublication);
    List<Contributor> contributors = this.getContributorsList(publication.getResId());
    assertEquals(2, contributors.size());
    assertTrue(publication.getMetadata().contains(unigeDoi));
    assertTrue(publication.getFormData().contains(unigeDoi));
    assertTrue(publication.getDoi().equals(unigeDoi));

    // UPDATE without sending the DOI, but it should be preserved
    String titleUpdate = this.getTemporaryTestLabel("Yet another title");
    String newFormData = this.getTestFormData(titleUpdate);
    assertFalse(newFormData.contains(unigeDoi));
    createdPublication.setFormData(newFormData);
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    Publication updatedPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(updatedPublication);
    assertEquals(updatedPublication.getTitle(), titleUpdate);
    assertTrue(updatedPublication.getMetadata().contains(unigeDoi));
    assertTrue(updatedPublication.getFormData().contains(unigeDoi));
    assertTrue(updatedPublication.getDoi().equals(unigeDoi));
  }

  @Test
  void updateArticleDoesNotPreserveDoi() {
    String unigeDoi = "10.13097/archive-ouverte/unige-1612075";

    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "Preserve URN test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    properties.put("doi", unigeDoi);
    List<ContributorDTO> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);
    Publication createdPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(createdPublication);
    assertTrue(createdPublication.getMetadata().contains(unigeDoi));
    assertTrue(createdPublication.getFormData().contains(unigeDoi));
    assertTrue(createdPublication.getDoi().equals(unigeDoi));

    // UPDATE without sending the DOI will make DOI lost
    String titleUpdate = this.getTemporaryTestLabel("Yet another title");
    String newFormData = this.getTestFormData(titleUpdate);
    assertFalse(newFormData.contains(unigeDoi));
    createdPublication.setFormData(newFormData);
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    Publication updatedPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(updatedPublication);
    assertEquals(updatedPublication.getTitle(), titleUpdate);
    assertFalse(updatedPublication.getMetadata().contains(unigeDoi));
    assertFalse(updatedPublication.getFormData().contains(unigeDoi));
    assertNull(updatedPublication.getDoi());
  }

  @Test
  void transformArticleToThesisDoiTest() {
    String nonUnigeDoi = "10.13097/non-unige/unige-1612075";

    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "Preserve URN test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    properties.put("doi", nonUnigeDoi);
    List<ContributorDTO> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);
    Publication createdPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(createdPublication);
    assertTrue(createdPublication.getMetadata().contains(nonUnigeDoi));
    assertTrue(createdPublication.getFormData().contains(nonUnigeDoi));
    assertTrue(createdPublication.getDoi().equals(nonUnigeDoi));

    String titleUpdate = this.getTemporaryTestLabel("Article transformed to thesis");
    JSONObject formDataJsonObject = new JSONObject(createdPublication.getFormData());
    formDataJsonObject.getJSONObject("type").put("type", this.PUBLICATION_TYPE_DIPLOME_ID);
    formDataJsonObject.getJSONObject("type").put("subtype", this.PUBLICATION_SUBTYPE_THESE_ID);
    formDataJsonObject.getJSONObject("type").getJSONObject("title").put("text", titleUpdate);

    // Same as frontend comportment on validation: DOI is not posted back for thesis
    formDataJsonObject.getJSONObject("description").getJSONObject("identifiers").put("doi", JSONObject.NULL);

    createdPublication.setFormData(formDataJsonObject.toString());
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    Publication updatedPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(updatedPublication);
    assertEquals(titleUpdate, updatedPublication.getTitle());
    assertEquals(updatedPublication.getSubtype().getResId(), this.PUBLICATION_SUBTYPE_THESE_ID);
    assertFalse(updatedPublication.getMetadata().contains(nonUnigeDoi));
    assertFalse(updatedPublication.getFormData().contains(nonUnigeDoi));
    assertNull(updatedPublication.getDoi());
  }

  @Test
  void updateThesisPreserveUrn() {
    String urn = "urn:nbn:ch:unige-1612075";

    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "Preserve URN test");
    properties.put("type", this.PUBLICATION_TYPE_DIPLOME_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_THESE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    properties.put("urn", urn);
    List<ContributorDTO> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);
    ContributorDTO contributorDto2 = new ContributorDTO();
    contributorDto2.setFirstName(this.CONTRIBUTOR2_FIRSTNAME);
    contributorDto2.setLastName(this.CONTRIBUTOR2_LASTNAME);
    contributorDto2.setRole("director");
    contributorDto2.setCnIndividu(this.CONTRIBUTOR2_CN_INDIVIDU);
    contributorDTOs.add(contributorDto2);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);
    Publication createdPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(createdPublication);
    List<Contributor> contributors = this.getContributorsList(publication.getResId());
    assertEquals(2, contributors.size());
    assertTrue(publication.getMetadata().contains(urn));
    assertTrue(publication.getFormData().contains(urn));

    // UPDATE without sending the URN, but it should be preserved
    String titleUpdate = this.getTemporaryTestLabel("Yet another title");
    String newFormData = this.getTestFormData(titleUpdate);
    assertFalse(newFormData.contains(urn));
    createdPublication.setFormData(newFormData);
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    Publication updatedPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(updatedPublication);
    assertEquals(updatedPublication.getTitle(), titleUpdate);
    assertTrue(updatedPublication.getMetadata().contains(urn));
    assertTrue(updatedPublication.getFormData().contains(urn));
  }

  @Test
  void updateArticleDoesNotPreserveUrn() {
    String urn = "urn:nbn:ch:unige-1612075";

    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "Preserve URN test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    properties.put("urn", urn);
    List<ContributorDTO> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);
    Publication createdPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(createdPublication);
    assertTrue(createdPublication.getMetadata().contains(urn));
    assertTrue(createdPublication.getFormData().contains(urn));

    // UPDATE without sending the URN will make URN lost
    String titleUpdate = this.getTemporaryTestLabel("Yet another title");
    String newFormData = this.getTestFormData(titleUpdate);
    assertFalse(newFormData.contains(urn));
    createdPublication.setFormData(newFormData);
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    Publication updatedPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(updatedPublication);
    assertEquals(updatedPublication.getTitle(), titleUpdate);
    assertFalse(updatedPublication.getMetadata().contains(urn));
    assertFalse(updatedPublication.getFormData().contains(urn));
  }

  @Test
  void transformArticleToThesisUrnTest() {
    String nonUnigeUrn = "urn:example:com:tst-123";

    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "Preserve URN test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    properties.put("urn", nonUnigeUrn);
    List<ContributorDTO> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);
    Publication createdPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(createdPublication);
    assertTrue(createdPublication.getMetadata().contains(nonUnigeUrn));
    assertTrue(createdPublication.getFormData().contains(nonUnigeUrn));

    String titleUpdate = this.getTemporaryTestLabel("Article transformed to thesis");
    JSONObject formDataJsonObject = new JSONObject(createdPublication.getFormData());
    formDataJsonObject.getJSONObject("type").put("type", this.PUBLICATION_TYPE_DIPLOME_ID);
    formDataJsonObject.getJSONObject("type").put("subtype", this.PUBLICATION_SUBTYPE_THESE_ID);
    formDataJsonObject.getJSONObject("type").getJSONObject("title").put("text", titleUpdate);

    // Same as frontend comportment on validation: URN is not posted back for thesis
    formDataJsonObject.getJSONObject("description").getJSONObject("identifiers").put("urn", JSONObject.NULL);

    createdPublication.setFormData(formDataJsonObject.toString());
    this.publicationService.update(createdPublication.getResId(), createdPublication);

    Publication updatedPublication = this.publicationService.findOne(publication.getResId());
    assertNotNull(updatedPublication);
    assertEquals(titleUpdate, updatedPublication.getTitle());
    assertEquals(updatedPublication.getSubtype().getResId(), this.PUBLICATION_SUBTYPE_THESE_ID);
    assertFalse(updatedPublication.getMetadata().contains(nonUnigeUrn));
    assertFalse(updatedPublication.getFormData().contains(nonUnigeUrn));
  }

  @Test
  void listMyPublicationsTest() {

    Map<String, String> params = new HashMap<>();
    params.put("title", this.getTemporaryTestLabel(""));
    params.put("sort", "title");

    // No Publication exists for user before creation
    List<Publication> myPublications = this.publicationService.listMyPublications(params);
    assertNotNull(myPublications);
    assertTrue(myPublications.isEmpty());

    String title1 = this.getTemporaryTestLabel("test publication 1");
    String formData1 = this.getTestFormData(title1);
    this.createNewTemporaryPublication(formData1);

    String title2 = this.getTemporaryTestLabel("test publication 2");
    String formData2 = this.getTestFormData(title2);
    this.createNewTemporaryPublication(formData2);

    String title3 = this.getTemporaryTestLabel("test publication 3");
    String formData3 = this.getTestFormData(title3);
    this.createNewTemporaryPublication(formData3);

    myPublications = this.publicationService.listMyPublications(params);
    assertNotNull(myPublications);
    assertFalse(myPublications.isEmpty());
    assertEquals(3, myPublications.size());

    String personId = this.profilePerson.getResId();
    assertEquals(title1, myPublications.get(0).getTitle());
    assertEquals(title2, myPublications.get(1).getTitle());
    assertEquals(title3, myPublications.get(2).getTitle());
    assertEquals(personId, myPublications.get(0).getCreator().getResId());
    assertEquals(personId, myPublications.get(1).getCreator().getResId());
    assertEquals(personId, myPublications.get(2).getCreator().getResId());

    this.restClientTool.sudoAdmin();
    List<Publication> adminPublications = this.publicationService.listMyPublications(params);
    assertNotNull(adminPublications);
    assertTrue(adminPublications.isEmpty());
    this.restClientTool.exitSudo();

    this.restClientTool.sudoRoot();
    List<Publication> rootPublications = this.publicationService.listMyPublications(params);
    assertNotNull(rootPublications);
    assertTrue(rootPublications.isEmpty());
    this.restClientTool.exitSudo();

    /*
     * create publication with contributor
     */
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    List<AbstractContributor> contributorDTOs = new ArrayList<>();

    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.TEST_USER_USER_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    CollaborationDTO collaborationDTO = new CollaborationDTO();
    collaborationDTO.setName("My collaboration");
    contributorDTOs.add(collaborationDTO);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    //create publication with different user but the same contributor
    this.restClientTool.sudoAdmin();
    this.publicationService.create(publication);
    this.restClientTool.exitSudo();

    // check that user doesn't see a deposit that has been created by someone else even if he is part of the contributors
    // if the asContributor parameter is not passed to backend
    myPublications = this.publicationService.listMyPublications(params);
    assertEquals(3, myPublications.size());

    // check that user sees a deposit that has been created by someone else if he is part of the contributors and the asContributor parameter
    // is passed to the backend
    params.put("asContributor", "true");
    myPublications = this.publicationService.listMyPublications(params);
    assertEquals(1, myPublications.size());
  }

  @Test
  void listMyPublicationsWithoutContributorYetTest() {
    Map<String, String> params = new HashMap<>();
    params.put("title", this.getTemporaryTestLabel(""));
    params.put("sort", "title");

    // No Publication exists for user before creation
    List<Publication> myPublications = this.publicationService.listMyPublications(params);
    assertNotNull(myPublications);
    assertTrue(myPublications.isEmpty());

    String title1 = this.getTemporaryTestLabel("test publication 1");
    String formData1 = this.getTestFormData(title1);

    //remove contributors from Json
    JSONObject formDataJsonObject = new JSONObject(formData1);
    formDataJsonObject.getJSONObject("contributors").put("contributors", new JSONArray());
    formData1 = formDataJsonObject.toString();

    this.createNewTemporaryPublication(formData1);
    myPublications = this.publicationService.listMyPublications(params);
    assertNotNull(myPublications);
    assertFalse(myPublications.isEmpty());
  }

  @Test
  void addAndRemoveStructureTest() {
    String title = this.getTemporaryTestLabel("test add/remove structure publication");
    String formData = this.getTestFormData(title);

    Publication publication = this.createNewTemporaryPublication(formData);
    final Structure structure = this.getPermanentStructure();

    List<StructureWithJoinInfosDTO> structures = this.publicationService.getStructures(publication.getResId());
    final int initialSize = structures.size();

    final String publicationResId = publication.getResId();
    final String structureResId = structure.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.publicationService.addStructure(publicationResId, structureResId));
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    structures = this.publicationService.getStructures(publication.getResId());

    assertEquals(initialSize, structures.size());
  }

  @Test
  void createWithContributorsTest() {
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    /************************
     * create with 3 contributors
     */
    List<AbstractContributor> contributorDTOs = new ArrayList<>();

    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    CollaborationDTO collaborationDTO = new CollaborationDTO();
    collaborationDTO.setName("My collaboration");
    contributorDTOs.add(collaborationDTO);

    ContributorDTO contributorDto2 = new ContributorDTO();
    contributorDto2.setFirstName(this.CONTRIBUTOR2_FIRSTNAME);
    contributorDto2.setLastName(this.CONTRIBUTOR2_LASTNAME);
    contributorDto2.setRole("translator");
    contributorDto2.setCnIndividu(this.CONTRIBUTOR2_CN_INDIVIDU);
    contributorDTOs.add(contributorDto2);

    ContributorDTO contributorDto3 = new ContributorDTO();
    contributorDto3.setFirstName(this.CONTRIBUTOR3_FIRSTNAME);
    contributorDto3.setLastName(this.CONTRIBUTOR3_LASTNAME);
    contributorDto3.setRole("collaborator");
    contributorDto3.setCnIndividu(null);
    contributorDTOs.add(contributorDto3);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS); // to validate contributors roles

    publication = this.publicationService.create(publication);

    List<Contributor> contributors = this.getContributorsList(publication.getResId());
    assertEquals(3, contributors.size());
    this.checkFirstContributors(contributors);

    /************************
     * Add 3 new contributors
     */
    ContributorDTO contributorDto4 = new ContributorDTO();
    contributorDto4.setFirstName(this.CONTRIBUTOR4_FIRSTNAME);
    contributorDto4.setLastName(this.CONTRIBUTOR4_LASTNAME);
    contributorDto4.setRole("author");
    contributorDto4.setCnIndividu(this.CONTRIBUTOR4_CN_INDIVIDU);
    contributorDTOs.add(contributorDto4);

    ContributorDTO contributorDto5 = new ContributorDTO();
    contributorDto5.setFirstName(this.CONTRIBUTOR5_FIRSTNAME);
    contributorDto5.setLastName(this.CONTRIBUTOR5_LASTNAME);
    contributorDto5.setRole("author");
    contributorDto5.setCnIndividu(null);
    contributorDTOs.add(contributorDto5);

    ContributorDTO contributorDto6 = new ContributorDTO();
    contributorDto6.setFirstName(this.CONTRIBUTOR6_FIRSTNAME);
    contributorDto6.setLastName(this.CONTRIBUTOR6_LASTNAME);
    contributorDto6.setRole("author");
    contributorDto6.setCnIndividu(null);
    contributorDTOs.add(contributorDto6);

    properties.put("contributors", contributorDTOs);

    xmlMetadata = this.getMetadata(properties);
    publication.setMetadata(xmlMetadata);

    this.publicationService.update(publication.getResId(), publication);
    contributors = this.getContributorsList(publication.getResId());

    assertEquals(6, contributors.size());
    this.checkFirstContributors(contributors);

    Optional<Contributor> contributorOptional4 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR4_CN_INDIVIDU.equals(contributor.getCnIndividu())).findFirst();
    Optional<Contributor> contributorOptional5 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR5_LASTNAME.equals(contributor.getLastName())).findFirst();
    Optional<Contributor> contributorOptional6 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR6_LASTNAME.equals(contributor.getLastName())).findFirst();

    assertTrue(contributorOptional4.isPresent());
    assertEquals(this.CONTRIBUTOR4_CN_INDIVIDU, contributorOptional4.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR4_FIRSTNAME, contributorOptional4.get().getFirstName());
    assertEquals(this.CONTRIBUTOR4_LASTNAME, contributorOptional4.get().getLastName());

    assertTrue(contributorOptional5.isPresent());
    assertEquals(null, contributorOptional5.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR5_FIRSTNAME, contributorOptional5.get().getFirstName());
    assertEquals(this.CONTRIBUTOR5_LASTNAME, contributorOptional5.get().getLastName());

    assertTrue(contributorOptional6.isPresent());
    assertEquals(null, contributorOptional6.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR6_FIRSTNAME, contributorOptional6.get().getFirstName());
    assertEquals(this.CONTRIBUTOR6_LASTNAME, contributorOptional6.get().getLastName());

    /************************
     * Remove 2 contributors
     */
    contributorDTOs.remove(contributorDto2);
    contributorDTOs.remove(contributorDto3);
    properties.put("contributors", contributorDTOs);

    xmlMetadata = this.getMetadata(properties);
    publication.setMetadata(xmlMetadata);

    this.publicationService.update(publication.getResId(), publication);
    contributors = this.getContributorsList(publication.getResId());

    assertEquals(4, contributors.size());

    Optional<Contributor> contributorOptional1 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR1_CN_INDIVIDU.equals(contributor.getCnIndividu())).findFirst();
    Optional<Contributor> contributorOptional2 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR2_CN_INDIVIDU.equals(contributor.getCnIndividu())).findFirst();
    Optional<Contributor> contributorOptional3 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR3_LASTNAME.equals(contributor.getLastName())).findFirst();
    contributorOptional4 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR4_CN_INDIVIDU.equals(contributor.getCnIndividu())).findFirst();
    contributorOptional5 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR5_LASTNAME.equals(contributor.getLastName())).findFirst();
    contributorOptional6 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR6_LASTNAME.equals(contributor.getLastName())).findFirst();

    assertTrue(contributorOptional1.isPresent());
    assertEquals(this.CONTRIBUTOR1_CN_INDIVIDU, contributorOptional1.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR1_FIRSTNAME, contributorOptional1.get().getFirstName());
    assertEquals(this.CONTRIBUTOR1_LASTNAME, contributorOptional1.get().getLastName());

    assertFalse(contributorOptional2.isPresent());
    assertFalse(contributorOptional3.isPresent());

    assertTrue(contributorOptional4.isPresent());
    assertEquals(this.CONTRIBUTOR4_CN_INDIVIDU, contributorOptional4.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR4_FIRSTNAME, contributorOptional4.get().getFirstName());
    assertEquals(this.CONTRIBUTOR4_LASTNAME, contributorOptional4.get().getLastName());

    assertTrue(contributorOptional5.isPresent());
    assertEquals(null, contributorOptional5.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR5_FIRSTNAME, contributorOptional5.get().getFirstName());
    assertEquals(this.CONTRIBUTOR5_LASTNAME, contributorOptional5.get().getLastName());

    assertTrue(contributorOptional6.isPresent());
    assertEquals(null, contributorOptional6.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR6_FIRSTNAME, contributorOptional6.get().getFirstName());
    assertEquals(this.CONTRIBUTOR6_LASTNAME, contributorOptional6.get().getLastName());
  }

  @Test
  void transformUnigeContributorToNonUnigeContributorTest() {
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", "(non)unige contributor test");
    properties.put("type", this.PUBLICATION_TYPE_DIPLOME_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_THESE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    /************************
     * create with 2 UNIGE contributors (= with cnIndividu)
     */
    List<ContributorDTO> contributorDTOs = new ArrayList<>();

    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    ContributorDTO contributorDto2 = new ContributorDTO();
    contributorDto2.setFirstName(this.CONTRIBUTOR2_FIRSTNAME);
    contributorDto2.setLastName(this.CONTRIBUTOR2_LASTNAME);
    contributorDto2.setRole("director");
    contributorDto2.setCnIndividu(this.CONTRIBUTOR2_CN_INDIVIDU);
    contributorDTOs.add(contributorDto2);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);

    List<Contributor> contributors = this.getContributorsList(publication.getResId());
    assertEquals(2, contributors.size());

    Optional<Contributor> contributorOptional1 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR1_FIRSTNAME.equals(contributor.getFirstName())).findFirst();
    Optional<Contributor> contributorOptional2 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR2_FIRSTNAME.equals(contributor.getFirstName())).findFirst();

    assertTrue(contributorOptional1.isPresent());
    Contributor contributor1 = contributorOptional1.get();
    assertEquals(this.CONTRIBUTOR1_CN_INDIVIDU, contributor1.getCnIndividu());
    assertEquals(this.CONTRIBUTOR1_FIRSTNAME, contributor1.getFirstName());
    assertEquals(this.CONTRIBUTOR1_LASTNAME, contributor1.getLastName());

    assertTrue(contributorOptional2.isPresent());
    Contributor contributor2 = contributorOptional2.get();
    assertEquals(this.CONTRIBUTOR2_CN_INDIVIDU, contributor2.getCnIndividu());
    assertEquals(this.CONTRIBUTOR2_FIRSTNAME, contributor2.getFirstName());
    assertEquals(this.CONTRIBUTOR2_LASTNAME, contributor2.getLastName());

    /************************
     * Remove cnIndividu from first UNIGE contributor
     */

    //check that the first contributor can be found
    this.restClientTool.sudoRoot();
    Contributor previousContributor = this.contributorService.findOne(contributor1.getResId());
    this.restClientTool.exitSudo();
    assertNotNull(previousContributor);
    assertEquals(contributor1.getResId(), previousContributor.getResId());

    contributorDTOs.get(0).setCnIndividu(null);

    xmlMetadata = this.getMetadata(properties);
    publication.setMetadata(xmlMetadata);

    this.publicationService.update(publication.getResId(), publication);
    List<Contributor> contributors2 = this.getContributorsList(publication.getResId());

    assertEquals(2, contributors2.size());

    Optional<Contributor> updatedContributorOptional1 = contributors2.stream()
            .filter(contributor -> this.CONTRIBUTOR1_FIRSTNAME.equals(contributor.getFirstName())).findFirst();
    Optional<Contributor> updatedContributorOptional2 = contributors2.stream()
            .filter(contributor -> this.CONTRIBUTOR2_FIRSTNAME.equals(contributor.getFirstName())).findFirst();

    assertTrue(updatedContributorOptional1.isPresent());
    Contributor updatedContributor1 = updatedContributorOptional1.get();
    assertNull(updatedContributor1.getCnIndividu());
    assertEquals(this.CONTRIBUTOR1_FIRSTNAME, updatedContributor1.getFirstName());
    assertEquals(this.CONTRIBUTOR1_LASTNAME, updatedContributor1.getLastName());
    // check that a new contributor homonym has been created in database
    assertNotEquals(contributor1.getResId(), updatedContributor1.getResId());

    assertTrue(updatedContributorOptional2.isPresent());
    Contributor updatedContributor2 = updatedContributorOptional2.get();
    assertEquals(this.CONTRIBUTOR2_CN_INDIVIDU, updatedContributor2.getCnIndividu());
    assertEquals(this.CONTRIBUTOR2_FIRSTNAME, updatedContributor2.getFirstName());
    assertEquals(this.CONTRIBUTOR2_LASTNAME, updatedContributor2.getLastName());

    //check that the first contributor can not be found anymore:  he has been deleted as he was not linked to any other publication
    this.restClientTool.sudoRoot();
    final String contributorId1 = contributor1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.contributorService.findOne(contributorId1));
    this.restClientTool.exitSudo();

    /************************
     * Make first contributor UNIGE member again --> must use the same existing contributor
     */
    contributorDTOs.get(0).setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);

    xmlMetadata = this.getMetadata(properties);
    publication.setMetadata(xmlMetadata);

    this.publicationService.update(publication.getResId(), publication);
    List<Contributor> contributors3 = this.getContributorsList(publication.getResId());

    assertEquals(2, contributors3.size());

    Optional<Contributor> updatedAgainContributorOptional1 = contributors3.stream()
            .filter(contributor -> this.CONTRIBUTOR1_FIRSTNAME.equals(contributor.getFirstName())).findFirst();
    Optional<Contributor> updatedAgainContributorOptional2 = contributors3.stream()
            .filter(contributor -> this.CONTRIBUTOR2_FIRSTNAME.equals(contributor.getFirstName())).findFirst();

    assertTrue(updatedAgainContributorOptional1.isPresent());
    Contributor updatedAgainContributor1 = updatedAgainContributorOptional1.get();
    assertEquals(this.CONTRIBUTOR1_CN_INDIVIDU, updatedAgainContributor1.getCnIndividu());
    assertEquals(this.CONTRIBUTOR1_FIRSTNAME, updatedAgainContributor1.getFirstName());
    assertEquals(this.CONTRIBUTOR1_LASTNAME, updatedAgainContributor1.getLastName());

    assertTrue(updatedAgainContributorOptional2.isPresent());
    Contributor updatedAgainContributor2 = updatedAgainContributorOptional2.get();
    assertEquals(this.CONTRIBUTOR2_CN_INDIVIDU, updatedAgainContributor2.getCnIndividu());
    assertEquals(this.CONTRIBUTOR2_FIRSTNAME, updatedAgainContributor2.getFirstName());
    assertEquals(this.CONTRIBUTOR2_LASTNAME, updatedAgainContributor2.getLastName());
  }

  @Test
  void deleteContributorsTest() {
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    /************************
     * create with 3 contributors
     */
    List<AbstractContributor> contributorDTOs = new ArrayList<>();

    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    CollaborationDTO collaborationDTO = new CollaborationDTO();
    collaborationDTO.setName("My collaboration");
    contributorDTOs.add(collaborationDTO);

    ContributorDTO contributorDto2 = new ContributorDTO();
    contributorDto2.setFirstName(this.CONTRIBUTOR2_FIRSTNAME);
    contributorDto2.setLastName(this.CONTRIBUTOR2_LASTNAME);
    contributorDto2.setRole("director");
    contributorDto2.setCnIndividu(this.CONTRIBUTOR2_CN_INDIVIDU);
    contributorDTOs.add(contributorDto2);

    ContributorDTO contributorDto3 = new ContributorDTO();
    contributorDto3.setFirstName(this.CONTRIBUTOR3_FIRSTNAME);
    contributorDto3.setLastName(this.CONTRIBUTOR3_LASTNAME);
    contributorDto3.setRole("director");
    contributorDto3.setCnIndividu(null);
    contributorDTOs.add(contributorDto3);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);

    publication = this.publicationService.create(publication);

    List<Contributor> contributors = this.getContributorsList(publication.getResId());
    assertEquals(3, contributors.size());
    this.checkFirstContributors(contributors);

    /************************
     * create publication 2 with same contributors + a new one
     */
    ContributorDTO contributorDto4 = new ContributorDTO();
    contributorDto4.setFirstName(this.CONTRIBUTOR4_FIRSTNAME);
    contributorDto4.setLastName(this.CONTRIBUTOR4_LASTNAME);
    contributorDto4.setRole("author");
    contributorDto4.setCnIndividu(this.CONTRIBUTOR4_CN_INDIVIDU);
    contributorDTOs.add(contributorDto4);

    properties.put("contributors", contributorDTOs);

    xmlMetadata = this.getMetadata(properties);
    publication.setMetadata(xmlMetadata);

    Publication publication2 = new Publication();
    publication2.setMetadata(xmlMetadata);
    publication2 = this.publicationService.create(publication2);

    List<Contributor> contributors2 = this.getContributorsList(publication2.getResId());
    assertEquals(4, contributors2.size());
    this.checkFirstContributors(contributors2);

    /************************
     * remove first 3 contributors from the publication 2
     */
    contributorDTOs.remove(contributorDto1);
    contributorDTOs.remove(contributorDto2);
    contributorDTOs.remove(contributorDto3);
    properties.put("contributors", contributorDTOs);

    xmlMetadata = this.getMetadata(properties);
    publication2.setMetadata(xmlMetadata);

    this.publicationService.update(publication2.getResId(), publication2);
    contributors2 = this.getContributorsList(publication2.getResId());
    assertEquals(1, contributors2.size());

    /************************
     * fetch contributors from the publication 1 again and check the list has not been updated
     */
    contributors = this.getContributorsList(publication.getResId());
    assertEquals(3, contributors.size());
    this.checkFirstContributors(contributors);
  }

  @Test
  void submitForApprovalWithoutStructureTest() {
    Publication createdPublication = this.testPublicationCreation("test publication submit for approval");

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.publicationService.submitForApproval(createdPublication.getResId()));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("structure", "No validation structure could be automatically retrieved from your profile.",
            validationErrors));
  }

  @Test
  void submitForApprovalWithValidationStructureTest() {
    Publication createdPublication = this.testPublicationCreation("test publication submit for approval with validation structure");
    final Structure structure = this.getPermanentStructure();

    this.publicationService.submitForApproval(createdPublication.getResId(), structure.getResId());

    Publication foundPublication = this.publicationService.findOne(createdPublication.getResId());
    assertEquals(Publication.PublicationStatus.IN_VALIDATION, foundPublication.getStatus());

    List<StructureWithJoinInfosDTO> structures = this.publicationService.getStructures(createdPublication.getResId());

    assertEquals(1, structures.size());
    assertEquals(PublicationStructure.LinkType.VALIDATION, structures.get(0).getJoinResource().getLinkType());

    this.restClientTool.sudoAdmin();
    this.publicationService.askFeedback(foundPublication.getResId(), "To allow to delete the fixture");
    this.restClientTool.exitSudo();
  }

  @Test
  void askFeedbackTest() {
    final Structure structure = this.getPermanentStructure();
    Publication createdPublication = this.testPublicationCreation("test publication ask for feedback");
    this.publicationService.submitForApproval(createdPublication.getResId(), structure.getResId());

    Publication foundPublication = this.publicationService.findOne(createdPublication.getResId());
    assertEquals(Publication.PublicationStatus.IN_VALIDATION, foundPublication.getStatus());

    // try to ask feedback as user
    final String createdPublicationId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.publicationService.askFeedback(createdPublicationId, "I disagree"));
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    // try to ask feedback without reason
    this.restClientTool.sudoAdmin();
    final String createdPublicationId2 = createdPublication.getResId();
    e = assertThrows(HttpClientErrorException.class, () -> this.publicationService.askFeedback(createdPublicationId2, null));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());

    // ask feedback with correct rights and a reason
    this.publicationService.askFeedback(createdPublication.getResId(), "I disagree");
    foundPublication = this.publicationService.findOne(createdPublication.getResId());
    assertEquals(Publication.PublicationStatus.FEEDBACK_REQUIRED, foundPublication.getStatus());

    List<Comment> comments = this.commentClientService.findAll(foundPublication.getResId());
    assertEquals(1, comments.size());
    assertEquals("I disagree", comments.get(0).getText());

    this.restClientTool.exitSudo();
  }

  @Test
  void updateStatusToDeleteTest() {
    String title = this.getTemporaryTestLabel("test publication to be deleted");
    String formData = this.getTestFormData(title);

    Publication createdPublication = this.createNewTemporaryPublication(formData, Publication.PublicationStatus.COMPLETED);
    Publication foundPublication = this.publicationService.findOne(createdPublication.getResId());
    assertEquals(Publication.PublicationStatus.COMPLETED, foundPublication.getStatus());

    // try to update status
    this.restClientTool.sudoAdmin();
    this.publicationService.updateStatusToDelete(createdPublication.getResId(), null);
    foundPublication = this.publicationService.findOne(createdPublication.getResId());
    assertEquals(Publication.PublicationStatus.DELETED, foundPublication.getStatus());
    this.restClientTool.exitSudo();
  }

  @Test
  void deleteTest() {
    Publication publication = this.testPublicationCreation("test publication to delete");
    assertNotNull(publication);
    this.publicationService.delete(publication.getResId());

    final String publicationId = publication.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.publicationService.findOne(publicationId));
  }

  @Test
  void deleteListTest() {
    Publication publication1 = this.testPublicationCreation("test publication to delete 1");
    assertNotNull(publication1);

    Publication publication2 = this.testPublicationCreation("test publication to delete 2");
    assertNotNull(publication2);

    String[] ids = new String[2];
    ids[0] = publication1.getResId();
    ids[1] = publication2.getResId();

    this.publicationService.deleteList(ids);

    final String publicationId1 = publication1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.publicationService.findOne(publicationId1));

    final String publicationId2 = publication2.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.publicationService.findOne(publicationId2));
  }

  @Test
  void deleteListNoPermissionTest() {
    Publication publication1 = this.testPublicationCreation("test publication to delete 1");
    assertNotNull(publication1);

    this.restClientTool.sudoAdmin();
    Person previousProfilePerson = this.profilePerson;
    User currentUser = this.userService.findOne(this.TEST_USER_ADMIN_ID);
    this.profilePerson = currentUser.getPerson();
    Publication publication2 = this.testPublicationCreation("test publication to delete 2");
    assertNotNull(publication2);
    this.restClientTool.exitSudo();
    this.profilePerson = previousProfilePerson;

    String[] ids = new String[2];
    ids[0] = publication1.getResId();
    ids[1] = publication2.getResId();

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.publicationService.deleteList(ids));
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    publication1 = this.publicationService.findOne(publication1.getResId());
    assertNotNull(publication1);

    this.restClientTool.sudoAdmin();
    publication2 = this.publicationService.findOne(publication2.getResId());
    assertNotNull(publication2);
    this.restClientTool.exitSudo();
  }

  @Test
  void updatePublicationAsContributorTest() {
    User myUser = this.userService.getAuthenticatedUser();
    Person myPerson = this.getLinkedPersonToUser(myUser);

    //add the current person as contributor
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", this.getTemporaryTestLabel("update test"));
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    List<AbstractContributor> contributorDTOs = new ArrayList<>();

    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(myPerson.getFirstName());
    contributorDto1.setLastName(myPerson.getLastName());
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(StringTool.substringBefore(myUser.getExternalUid(), "@"));
    contributorDTOs.add(contributorDto1);

    properties.put("contributors", contributorDTOs);

    this.restClientTool.sudoAdmin();
    Publication publicationCreated = this
            .createPublicationWithContributor("publication for testing download permission for contributors", properties);
    this.restClientTool.exitSudo();

    String titleToUpdate = this.getTemporaryTestLabel("title updated by contributor");
    publicationCreated.setFormData(this.getTestFormData(titleToUpdate));
    Publication publicationUpdate = this.publicationService.update(publicationCreated.getResId(), publicationCreated);
    assertNotNull(publicationUpdate);
    assertEquals(publicationUpdate.getTitle(), titleToUpdate);
  }

  @Test
  void contributorsCreateWithoutCnIndividuThenWithCnIndividuTest() {

    /**
     * First create a publication with a contributor without ORCID nor cnIndividu
     */
    String title = this.getTemporaryTestLabel("test publication contributors");
    String formData = this.getTestFormData(title);
    JSONObject formDataJsonObject = new JSONObject(formData);

    JSONArray contributorsArray = new JSONArray();

    JSONObject contributor = new JSONObject();
    contributor.put("firstname", this.CONTRIBUTOR1_FIRSTNAME);
    contributor.put("lastname", this.CONTRIBUTOR1_LASTNAME);
    contributor.put("role", "author");
    JSONObject contributorContainer = new JSONObject();
    contributorContainer.put("contributor", contributor);
    contributorsArray.put(contributorContainer);
    formDataJsonObject.getJSONObject("contributors").put("contributors", contributorsArray);
    formData = formDataJsonObject.toString();
    this.createNewTemporaryPublication(formData);

    Map<String, String> myPublicationsParams = new HashMap<>();
    myPublicationsParams.put("title", this.getTemporaryTestLabel(""));
    myPublicationsParams.put("sort", "title");
    List<Publication> myPublications = this.publicationService.listMyPublications(myPublicationsParams);
    assertNotNull(myPublications);
    assertEquals(1, myPublications.size());

    this.restClientTool.sudoRoot();
    Map<String, String> contributorsParams = new HashMap<>();
    contributorsParams.put("firstName", this.getTemporaryTestLabel(""));
    List<Contributor> contributorList = this.contributorService.searchByProperties(contributorsParams);
    assertNotNull(contributorList);
    assertEquals(1, contributorList.size());
    Contributor contributor1 = contributorList.get(0);
    assertEquals(this.CONTRIBUTOR1_FIRSTNAME, contributor1.getFirstName());
    assertEquals(this.CONTRIBUTOR1_LASTNAME, contributor1.getLastName());
    assertNull(contributor1.getCnIndividu());
    this.restClientTool.exitSudo();

    /**
     * Then create another publication with a contributor with cnIndividu
     * Check that it creates another contributor with the same name, but with cnIndividu
     */
    formDataJsonObject = new JSONObject(formData);
    formDataJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
            .put("cnIndividu", this.CONTRIBUTOR1_CN_INDIVIDU);
    formData = formDataJsonObject.toString();
    this.createNewTemporaryPublication(formData);
    myPublications = this.publicationService.listMyPublications(myPublicationsParams);
    assertNotNull(myPublications);
    assertEquals(2, myPublications.size());

    this.restClientTool.sudoRoot();
    contributorList = this.contributorService.searchByProperties(contributorsParams);
    assertNotNull(contributorList);
    assertEquals(2, contributorList.size());
    Optional<Contributor> contributorNoId = contributorList.stream().filter(c -> c.getCnIndividu() == null).findFirst();
    assertTrue(contributorNoId.isPresent());
    Optional<Contributor> contributorid = contributorList.stream().filter(c -> this.CONTRIBUTOR1_CN_INDIVIDU.equals(c.getCnIndividu()))
            .findFirst();
    assertTrue(contributorid.isPresent());
    this.restClientTool.exitSudo();
  }

  @Test
  void updatePublicationListWithResearchGroupTest() {
    String title1 = this.getTemporaryTestLabel("test research group in publication1 as user");
    String title2 = this.getTemporaryTestLabel("test research group in publication2 as user");
    String title3 = this.getTemporaryTestLabel("test research group in publication3 as user");

    String formData1 = this.getTestFormData(title1);
    String formData2 = this.getTestFormData(title2);
    String formData3 = this.getTestFormData(title3);

    this.restClientTool.sudoRoot();
    ResearchGroup researchGroupFixture = this.createResearchGroupFixture("research group 1", "12345");
    ResearchGroup addedResearchGroup = this.createResearchGroupFixture("research group 2", "98763");
    this.restClientTool.exitSudo();

    //add the research groups into the form data
    formData1 = this.addResearchGroupInFormData(formData1, researchGroupFixture.getResId());
    formData2 = this.addResearchGroupInFormData(formData2, researchGroupFixture.getResId());
    formData3 = this.addResearchGroupInFormData(formData3, researchGroupFixture.getResId());

    String[] publicationIds = new String[3];
    Publication tmpPublication1 = this.createNewTemporaryPublication(formData1);
    publicationIds[0] = tmpPublication1.getResId();
    Publication tmpPublication2 = this.createNewTemporaryPublication(formData2);
    publicationIds[1] = tmpPublication2.getResId();
    Publication tmpPublication3 = this.createNewTemporaryPublication(formData3);
    publicationIds[2] = tmpPublication3.getResId();

    assertNotNull(tmpPublication1);
    assertNotNull(tmpPublication2);
    assertNotNull(tmpPublication3);

    List<ResearchGroup> researchGroups1 = this.getResearchGroupsList(tmpPublication1.getResId());
    List<ResearchGroup> researchGroups2 = this.getResearchGroupsList(tmpPublication2.getResId());
    List<ResearchGroup> researchGroups3 = this.getResearchGroupsList(tmpPublication3.getResId());

    // test that all publications have only one research group
    assertEquals(1, researchGroups1.size());
    assertEquals(1, researchGroups2.size());
    assertEquals(1, researchGroups3.size());

    this.publicationService.updatePublicationListWithResearchGroup(addedResearchGroup.getResId(), publicationIds);

    researchGroups1 = this.getResearchGroupsList(tmpPublication1.getResId());
    researchGroups2 = this.getResearchGroupsList(tmpPublication2.getResId());
    researchGroups3 = this.getResearchGroupsList(tmpPublication3.getResId());

    // test that all publications have only one research group
    assertEquals(2, researchGroups1.size());
    assertEquals(2, researchGroups2.size());
    assertEquals(2, researchGroups3.size());

    assertTrue(researchGroups1.stream().anyMatch(st -> st.getCode().equals(addedResearchGroup.getCode())));
    assertTrue(researchGroups2.stream().anyMatch(st -> st.getCode().equals(addedResearchGroup.getCode())));
    assertTrue(researchGroups3.stream().anyMatch(st -> st.getCode().equals(addedResearchGroup.getCode())));
  }

  @Test
  void updatePublicationListWithStructureTest() {
    this.restClientTool.sudoRoot();
    this.createStructuresHierarchy();
    this.restClientTool.exitSudo();

    String title1 = this.getTemporaryTestLabel("test structure in publication1 as user");
    String title2 = this.getTemporaryTestLabel("test structure in publication2 as user");
    String title3 = this.getTemporaryTestLabel("test structure in publication3 as user");

    String formData1 = this.getTestFormData(title1);
    String formData2 = this.getTestFormData(title2);
    String formData3 = this.getTestFormData(title3);

    String[] publicationIds = new String[3];
    Publication tmpPublication1 = this.createNewTemporaryPublication(formData1);
    publicationIds[0] = tmpPublication1.getResId();
    Publication tmpPublication2 = this.createNewTemporaryPublication(formData2);
    publicationIds[1] = tmpPublication2.getResId();
    Publication tmpPublication3 = this.createNewTemporaryPublication(formData3);
    publicationIds[2] = tmpPublication3.getResId();

    assertNotNull(tmpPublication1);
    assertNotNull(tmpPublication2);
    assertNotNull(tmpPublication3);

    List<StructureWithJoinInfosDTO> structurePublications1 = this.publicationService.getStructures(tmpPublication1.getResId());
    List<StructureWithJoinInfosDTO> structurePublications2 = this.publicationService.getStructures(tmpPublication2.getResId());
    List<StructureWithJoinInfosDTO> structurePublications3 = this.publicationService.getStructures(tmpPublication3.getResId());

    // test that all publications have only one research group
    assertEquals(0, structurePublications1.size());
    assertEquals(0, structurePublications2.size());
    assertEquals(0, structurePublications3.size());

    Structure topStructure = this.getTopStructure();
    this.publicationService.updatePublicationListWithStructure(topStructure.getResId(), publicationIds);

    structurePublications1 = this.publicationService.getStructures(tmpPublication1.getResId());
    structurePublications2 = this.publicationService.getStructures(tmpPublication2.getResId());
    structurePublications3 = this.publicationService.getStructures(tmpPublication3.getResId());

    // test that all publications have only one structure
    assertEquals(1, structurePublications1.size());
    assertEquals(1, structurePublications2.size());
    assertEquals(1, structurePublications3.size());
    assertTrue(structurePublications1.stream().anyMatch(st -> st.getCodeStruct().equals(topStructure.getCodeStruct())));
    assertTrue(structurePublications1.stream().anyMatch(st -> st.getCodeStruct().equals(topStructure.getCodeStruct())));
    assertTrue(structurePublications1.stream().anyMatch(st -> st.getCodeStruct().equals(topStructure.getCodeStruct())));

    // Add a 2 child structures
    Structure structure11 = this.findStructureByName(this.getTemporaryTestLabel("child11"));
    Structure structure12 = this.findStructureByName(this.getTemporaryTestLabel("child12"));
    this.publicationService.updatePublicationListWithStructure(structure11.getResId(), publicationIds);
    this.publicationService.updatePublicationListWithStructure(structure12.getResId(), publicationIds);

    // test that all publications have both child structures and the parent has been removed
    structurePublications1 = this.publicationService.getStructures(tmpPublication1.getResId());
    structurePublications2 = this.publicationService.getStructures(tmpPublication2.getResId());
    structurePublications3 = this.publicationService.getStructures(tmpPublication3.getResId());
    assertEquals(2, structurePublications1.size());
    assertEquals(2, structurePublications2.size());
    assertEquals(2, structurePublications3.size());
    assertEquals(2, structurePublications1.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());
    assertEquals(2, structurePublications2.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());
    assertEquals(2, structurePublications3.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());

    // add the parent again which must have no effect
    this.publicationService.updatePublicationListWithStructure(topStructure.getResId(), publicationIds);
    structurePublications1 = this.publicationService.getStructures(tmpPublication1.getResId());
    structurePublications2 = this.publicationService.getStructures(tmpPublication2.getResId());
    structurePublications3 = this.publicationService.getStructures(tmpPublication3.getResId());
    assertEquals(2, structurePublications1.size());
    assertEquals(2, structurePublications2.size());
    assertEquals(2, structurePublications3.size());
    assertEquals(2, structurePublications1.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());
    assertEquals(2, structurePublications2.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());
    assertEquals(2, structurePublications3.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());

    // add same child again which must have no effect
    this.publicationService.updatePublicationListWithStructure(structure11.getResId(), publicationIds);
    structurePublications1 = this.publicationService.getStructures(tmpPublication1.getResId());
    structurePublications2 = this.publicationService.getStructures(tmpPublication2.getResId());
    structurePublications3 = this.publicationService.getStructures(tmpPublication3.getResId());
    assertEquals(2, structurePublications1.size());
    assertEquals(2, structurePublications2.size());
    assertEquals(2, structurePublications3.size());
    assertEquals(2, structurePublications1.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());
    assertEquals(2, structurePublications2.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());
    assertEquals(2, structurePublications3.stream()
            .filter(s -> s.getName().equals(this.getTemporaryTestLabel("child11")) || s.getName().equals(this.getTemporaryTestLabel("child12")))
            .count());
  }

  @Test
  void createFromDOITest() {
    Publication publication = this.createFromDOI(DOI_IMPORT_TEST);
    assertNotNull(publication);
    final String personId = this.getPersonIdFromUserId(this.TEST_USER_USER_ID);
    assertEquals(personId, publication.getCreatorId());

    // Wait to prevent a race condition (deleteFixtures() being called to early prevents indexes to be cleaned)
    SolidifyTime.waitInSeconds(2);
  }

  @Test
  void createFromDuplicateDOITest() {
    Publication publication = this.createFromDOI(DOI_IMPORT_TEST);
    assertNotNull(publication);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.createFromDOI(DOI_IMPORT_TEST));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "A publication with this DOI already exists");
    assertTrue(e.getResponseBodyAsString().contains("The DOI '" + DOI_IMPORT_TEST + "' already exists in Archive ouverte UNIGE"));

    // Wait to prevent a race condition (deleteFixtures() being called to early prevents indexes to be cleaned)
    SolidifyTime.waitInSeconds(2);
  }

  @Test
  void createFromDuplicateInCorrectionDOITest() {
    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "DOI in correction");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    Map<String, Object> correctionProperties = new HashMap<>();
    correctionProperties.put("note", "Add DOI to test duplicate detection");
    correctionProperties.put("doi", DOI_IMPORT_TEST);
    properties.put("correction", correctionProperties);

    List<ContributorDTO> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.CONTRIBUTOR1_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);
    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    this.publicationService.create(publication);

    // Wait the time necessary for the publication to be indexed
    SolidifyTime.waitInSeconds(5);

    // Test that a Publication with the same DOI cannot be created anymore
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.createFromDOI(DOI_IMPORT_TEST));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "A publication with this DOI already exists");
    assertTrue(e.getResponseBodyAsString().contains("The DOI '" + DOI_IMPORT_TEST + "' already exists in Archive ouverte UNIGE"));
  }

  @Test
  void createFromInvalidDOITest() {
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.createFromDOI("invalid doi");
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Test
  void createFromDOIForSomeoneElseTest() {
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.createFromDOI(DOI_IMPORT_TEST, this.TEST_USER_ADMIN_ID);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode(), "A user is not allowed to create deposits for someone else");
  }

  @Test
  void createFromPMIDTest() {
    Publication publication = this.createFromPMID(PMID_IMPORT_TEST);
    assertNotNull(publication);
    final String personId = this.getPersonIdFromUserId(this.TEST_USER_USER_ID);
    assertEquals(personId, publication.getCreatorId());
  }

  @Test
  void createFromInvalidPMIDTest() {
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.createFromPMID("invalid PMID");
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Test
  void validateMetadataTest() throws IOException {
    String title = this.getTemporaryTestLabel("validate as user publication");
    String formData = this.getTestFormData(title);

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    this.uploadDocumentFile(createdPublication, new ClassPathResource(TEST_FILE_TO_UPLOAD));
    assertNotNull(createdPublication);

    String result = this.publicationService.validateMetadata(createdPublication.getResId());
    assertEquals("Publication is valid", result);
  }

  @Test
  void validateMetadataInvalidTest() {
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    // add contributor without role
    List<AbstractContributor> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole(null);
    contributorDTOs.add(contributorDto1);
    properties.put("contributors", contributorDTOs);

    // add invalid issn
    String invalidIssn = "some invalid value";
    properties.put("issn", invalidIssn);

    Publication publication = new Publication();
    publication.setMetadata(this.getMetadata(properties));

    Publication createdPublication = this.publicationService.create(publication);

    assertNotNull(createdPublication);

    String createdPublicationResId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.validateMetadata(createdPublicationResId);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());

    // check that validation errors from different form steps are returned
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("contributors.contributors.0.role", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("description.identifiers.issn", "The value '" + invalidIssn + "' is not a valid ISSN",
            validationErrors));
  }

  @Test
  void validateMetadataNoContributorInvalidTest() {
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    Publication publication = new Publication();
    publication.setMetadata(this.getMetadata(properties));

    Publication createdPublication = this.publicationService.create(publication);
    assertNotNull(createdPublication);

    String createdPublicationResId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.validateMetadata(createdPublicationResId);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());

    // check that validation errors from different form steps are returned
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("contributors.contributors", "The publication doesn't contain any contributor", validationErrors));
    assertTrue(this.validationErrorsContain("description.dates", "At least one date is mandatory", validationErrors));
    assertTrue(this.validationErrorsContain("files.files", "File missing in this publication", validationErrors));
  }

  @Test
  void validateMetadataForSomeoneElseDepositTest() throws IOException {
    this.restClientTool.sudoAdmin();
    String title = this.getTemporaryTestLabel("validate someone else publication");
    String formData = this.getTestFormData(title);

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    this.uploadDocumentFile(createdPublication, new ClassPathResource(TEST_FILE_TO_UPLOAD));
    assertNotNull(createdPublication);
    this.restClientTool.exitSudo();

    String createdPublicationResId = createdPublication.getResId();
    String result = this.publicationService.validateMetadata(createdPublicationResId);
    assertEquals("Publication is valid", result);
  }

  @Test
  void uploadThumbnailTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource thumbnailResource = new ClassPathResource(logoName);
    final Long sizeLogo = thumbnailResource.contentLength();

    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);
    Publication createdPublication = this.createNewTemporaryPublication(formData);

    String createdPublicationResId = createdPublication.getResId();

    final Publication publicationWithThumbnail = this.publicationService.uploadThumbnail(createdPublicationResId, thumbnailResource);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);

    final Publication foundPublicationWithThumbnail = this.publicationService.findOne(createdPublicationResId);
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);

    this.publicationService.deleteThumbnail(createdPublicationResId);
    final Publication publicationWithoutThumbnail = this.publicationService.findOne(createdPublicationResId);
    assertNull(publicationWithoutThumbnail.getThumbnail());

  }

  @Test
  void uploadThumbnailOnSomeoneElsePublicationTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource thumbnailResource = new ClassPathResource(logoName);

    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);
    this.restClientTool.sudoAdmin();
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    this.restClientTool.exitSudo();

    final String createdPublicationResId = createdPublication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.uploadThumbnail(createdPublicationResId, thumbnailResource);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void downloadThumbnailTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource thumbnailResource = new ClassPathResource(logoName);
    final Long sizeLogo = thumbnailResource.contentLength();

    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);
    Publication createdPublication = this.createNewTemporaryPublication(formData);

    String createdPublicationResId = createdPublication.getResId();

    final Publication publicationWithThumbnail = this.publicationService.uploadThumbnail(createdPublicationResId, thumbnailResource);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.publicationService.downloadThumbnail(createdPublicationResId, path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), sizeLogo);
  }

  @Test
  void downloadThumbnailFromSomeoneElsePublicationTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource thumbnailResource = new ClassPathResource(logoName);
    final Long sizeLogo = thumbnailResource.contentLength();

    this.restClientTool.sudoAdmin();
    String title = this.getTemporaryTestLabel("test as admin publication");
    String formData = this.getTestFormData(title);
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    String createdPublicationResId = createdPublication.getResId();

    final Publication publicationWithThumbnail = this.publicationService.uploadThumbnail(createdPublicationResId, thumbnailResource);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);
    this.restClientTool.exitSudo();

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.publicationService.downloadThumbnail(createdPublicationResId, path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), sizeLogo);
  }

  @Test
  void deleteThumbnailTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource thumbnailResource = new ClassPathResource(logoName);
    final Long sizeLogo = thumbnailResource.contentLength();

    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);
    Publication createdPublication = this.createNewTemporaryPublication(formData);

    String createdPublicationResId = createdPublication.getResId();

    final Publication publicationWithThumbnail = this.publicationService.uploadThumbnail(createdPublicationResId, thumbnailResource);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);

    this.publicationService.deleteThumbnail(createdPublicationResId);

    // Thumbnail cannot be downloaded anymore
    final Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.publicationService.downloadThumbnail(createdPublicationResId, path));

    // Thumbnail is null on publication
    Publication foundPublication = this.publicationService.findOne(createdPublicationResId);
    assertNotNull(foundPublication);
    assertNull(foundPublication.getThumbnail());
  }

  @Test
  void deleteThumbnailFromSomeoneElsePublicationTest() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource thumbnailResource = new ClassPathResource(logoName);
    final Long sizeLogo = thumbnailResource.contentLength();

    this.restClientTool.sudoAdmin();
    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);
    Publication createdPublication = this.createNewTemporaryPublication(formData);

    final String createdPublicationResId = createdPublication.getResId();

    final Publication publicationWithThumbnail = this.publicationService.uploadThumbnail(createdPublicationResId, thumbnailResource);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(publicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);
    this.restClientTool.exitSudo();

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.deleteThumbnail(createdPublicationResId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void cloneDepositWithFileTest() {
    Publication publicationToCopy = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to clone with a document"));
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publicationToCopy);
    documentFile.setDocumentFileType(documentFileType);
    documentFile.setSourceData(URI.create(TEST_FILE_URL));
    documentFile.setAccessLevel(DocumentFile.AccessLevel.PUBLIC);

    this.publicationDocumentFileService.create(publicationToCopy.getResId(), documentFile);

    Publication clonePublication = this.publicationService.clonePublication(publicationToCopy.getResId());

    assertEquals(publicationToCopy.getTitle(), clonePublication.getTitle());
    assertEquals(publicationToCopy.getCreator().getResId(), clonePublication.getCreator().getResId());
    assertEquals(publicationToCopy.getSubtype().getResId(), clonePublication.getSubtype().getResId());
    assertNotEquals(publicationToCopy.getResId(), clonePublication.getResId());
    assertEquals(publicationToCopy.getPublicationStructures(), clonePublication.getPublicationStructures());
    assertEquals(publicationToCopy.getResearchGroups(), clonePublication.getResearchGroups());
    assertEquals(0, clonePublication.getDocumentFiles().size());
    assertNull(clonePublication.getDoi());
    assertNull(clonePublication.getPmid());
  }

  @Test
  void cloneDepositTest() {
    String title = "test as user publication to clone deposit";
    Publication publicationToCopy = this.testPublicationCreation(title);

    Publication clonePublication = this.publicationService.clonePublication(publicationToCopy.getResId());

    assertEquals(publicationToCopy.getTitle(), clonePublication.getTitle());
    assertEquals(publicationToCopy.getCreator().getResId(), clonePublication.getCreator().getResId());
    assertEquals(publicationToCopy.getSubtype().getResId(), clonePublication.getSubtype().getResId());
    assertNotEquals(publicationToCopy.getResId(), clonePublication.getResId());
    assertEquals(publicationToCopy.getPublicationStructures(), clonePublication.getPublicationStructures());
    assertEquals(publicationToCopy.getResearchGroups(), clonePublication.getResearchGroups());
    assertEquals(0, clonePublication.getDocumentFiles().size());
    assertNull(clonePublication.getDoi());
    assertNull(clonePublication.getPmid());

  }

  @Test
  void doiNotCopyWhenCloningDepositTest() {
    String doi = "10.1007/978-3-030-53705-0_32";

    String title = this.getTemporaryTestLabel("test as user publication to clone deposit");
    String formData = this.getTestFormData(title);
    formData = this.addDoiInFormData(formData, doi);

    Publication publicationToCopy = this.createNewTemporaryPublication(formData);

    Publication clonePublication = this.publicationService.clonePublication(publicationToCopy.getResId());

    assertEquals(publicationToCopy.getTitle(), clonePublication.getTitle());
    assertEquals(publicationToCopy.getCreator().getResId(), clonePublication.getCreator().getResId());
    assertEquals(publicationToCopy.getSubtype().getResId(), clonePublication.getSubtype().getResId());
    assertNotEquals(publicationToCopy.getResId(), clonePublication.getResId());
    assertEquals(publicationToCopy.getPublicationStructures(), clonePublication.getPublicationStructures());
    assertEquals(publicationToCopy.getResearchGroups(), clonePublication.getResearchGroups());
    assertEquals(0, clonePublication.getDocumentFiles().size());
    assertNull(clonePublication.getDoi());
  }

  @Test
  void cloneWithThumbnailAndDeleteThumbnailTest() throws IOException {

    // Create 1st publication with thumbnail
    final String logoName = "geneve.jpg";
    final ClassPathResource thumbnailResource = new ClassPathResource(logoName);
    final Long sizeLogo = thumbnailResource.contentLength();

    String title = this.getTemporaryTestLabel("test as user publication");
    String formData = this.getTestFormData(title);
    Publication createdPublication = this.createNewTemporaryPublication(formData);
    String createdPublicationResId = createdPublication.getResId();
    this.publicationService.uploadThumbnail(createdPublicationResId, thumbnailResource);
    final Publication foundPublicationWithThumbnail = this.publicationService.findOne(createdPublicationResId);
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);

    // Clone 1st publication. Thumbnail ids must be different as they must have been cloned as well
    Publication clonePublication = this.publicationService.clonePublication(foundPublicationWithThumbnail.getResId());
    assertEquals(foundPublicationWithThumbnail.getTitle(), clonePublication.getTitle());
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileName(), clonePublication.getThumbnail().getFileName());
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileSize(), clonePublication.getThumbnail().getFileSize());
    assertNotEquals(foundPublicationWithThumbnail.getThumbnail().getResId(), clonePublication.getThumbnail().getResId());

    // Delete thumbnail on 2nd publication
    this.publicationService.deleteThumbnail(clonePublication.getResId());
    final Publication clonePublicationWithoutThumbnail = this.publicationService.findOne(clonePublication.getResId());
    assertNull(clonePublicationWithoutThumbnail.getThumbnail());

    // 1st publication still has its thumbnail
    final Publication publication1 = this.publicationService.findOne(createdPublicationResId);
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileName(), logoName);
    assertEquals(foundPublicationWithThumbnail.getThumbnail().getFileSize(), sizeLogo);
  }

  @Test
  void itsNotMyPublicationTest() {

    /*
     * create Publication with 2 contributors. The first contributor is created with the cnIndividu of the User test user
     */
    this.restClientTool.sudoAdmin();
    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    List<AbstractContributor> contributorDTOs = new ArrayList<>();
    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(this.CONTRIBUTOR1_FIRSTNAME);
    contributorDto1.setLastName(this.CONTRIBUTOR1_LASTNAME);
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(this.TEST_USER_USER_CN_INDIVIDU);
    contributorDTOs.add(contributorDto1);

    ContributorDTO contributorDto2 = new ContributorDTO();
    contributorDto2.setFirstName(this.CONTRIBUTOR2_FIRSTNAME);
    contributorDto2.setLastName(this.CONTRIBUTOR2_LASTNAME);
    contributorDto2.setRole("director");
    contributorDto2.setCnIndividu(this.CONTRIBUTOR2_CN_INDIVIDU);
    contributorDTOs.add(contributorDto2);

    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    Publication createdPublication = this.publicationService.create(publication);

    String createdPublicationResId = createdPublication.getResId();

    // check creation
    List<Contributor> contributorsInDB = this.getContributorsList(createdPublicationResId);
    assertEquals(2, contributorsInDB.size());
    Optional<Contributor> dbContributorOpt1 = contributorsInDB.stream()
            .filter(contributor -> contributor.getCnIndividu().equals(this.TEST_USER_USER_CN_INDIVIDU)).findFirst();
    assertTrue(dbContributorOpt1.isPresent());
    assertEquals(this.TEST_USER_USER_CN_INDIVIDU, dbContributorOpt1.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR1_FIRSTNAME, dbContributorOpt1.get().getFirstName());
    assertEquals(this.CONTRIBUTOR1_LASTNAME, dbContributorOpt1.get().getLastName());
    this.restClientTool.exitSudo();

    /*
     * check that user still doesn't see a deposit that has been created by someone else even if he is part of the contributors
     * if the asContributor parameter is not passed to the backend
     */
    Map<String, String> params = new HashMap<>();
    List<Publication> myPublications = this.publicationService.listMyPublications(params);
    Optional<Publication> matchingPublicationOpt = myPublications.stream().filter(pub -> pub.getResId().equals(createdPublicationResId))
            .findFirst();
    assertFalse(matchingPublicationOpt.isPresent());

    /*
     * check that user sees a deposit that has been created by someone else if he is part of the contributors
     * and the asContributor parameter is passed to the backend
     */
    params.put("asContributor", "true");
    myPublications = this.publicationService.listMyPublications(params);
    matchingPublicationOpt = myPublications.stream().filter(pub -> pub.getResId().equals(createdPublicationResId)).findFirst();
    assertTrue(matchingPublicationOpt.isPresent());

    /*
     * Call the endpoint "it is not my publication" to remove the cnIndividu from the Publication
     */
    this.publicationService.unlinkContributor(createdPublicationResId);

    /*
     * Check that user doesn't see the deposit anymore
     */
    myPublications = this.publicationService.listMyPublications(params);
    matchingPublicationOpt = myPublications.stream().filter(pub -> pub.getResId().equals(createdPublicationResId)).findFirst();
    assertTrue(matchingPublicationOpt.isEmpty());

    /*
     * Check that the admin still sees the publication, with 2 contributors, but the first one has no more cnIndividu
     */
    this.restClientTool.sudoAdmin();
    myPublications = this.publicationService.listMyPublications(Collections.emptyMap());
    matchingPublicationOpt = myPublications.stream().filter(pub -> pub.getResId().equals(createdPublicationResId)).findFirst();
    assertTrue(matchingPublicationOpt.isPresent());

    // check there is still 2 contributors
    contributorsInDB = this.getContributorsList(createdPublicationResId);
    assertEquals(2, contributorsInDB.size());

    // contributor with 1st cnIndividu cannot be found anymore
    dbContributorOpt1 = contributorsInDB.stream().filter(contributor -> this.TEST_USER_USER_CN_INDIVIDU.equals(contributor.getCnIndividu()))
            .findFirst();
    assertFalse(dbContributorOpt1.isPresent());

    // but 2nd contributor has still its cnIndividu
    Optional<Contributor> dbContributorOpt2 = contributorsInDB.stream()
            .filter(contributor -> this.CONTRIBUTOR2_CN_INDIVIDU.equals(contributor.getCnIndividu())).findFirst();
    assertTrue(dbContributorOpt2.isPresent());
    assertEquals(this.CONTRIBUTOR2_FIRSTNAME, dbContributorOpt2.get().getFirstName());
    assertEquals(this.CONTRIBUTOR2_LASTNAME, dbContributorOpt2.get().getLastName());
    assertEquals(this.CONTRIBUTOR2_CN_INDIVIDU, dbContributorOpt2.get().getCnIndividu());
    this.restClientTool.exitSudo();
  }

  private void checkFirstContributors(List<Contributor> contributors) {
    Optional<Contributor> contributorOptional1 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR1_CN_INDIVIDU.equals(contributor.getCnIndividu())).findFirst();
    Optional<Contributor> contributorOptional2 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR2_CN_INDIVIDU.equals(contributor.getCnIndividu())).findFirst();
    Optional<Contributor> contributorOptional3 = contributors.stream()
            .filter(contributor -> this.CONTRIBUTOR3_LASTNAME.equals(contributor.getLastName())).findFirst();

    assertTrue(contributorOptional1.isPresent());
    assertEquals(this.CONTRIBUTOR1_CN_INDIVIDU, contributorOptional1.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR1_FIRSTNAME, contributorOptional1.get().getFirstName());
    assertEquals(this.CONTRIBUTOR1_LASTNAME, contributorOptional1.get().getLastName());

    assertTrue(contributorOptional2.isPresent());
    assertEquals(this.CONTRIBUTOR2_CN_INDIVIDU, contributorOptional2.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR2_FIRSTNAME, contributorOptional2.get().getFirstName());
    assertEquals(this.CONTRIBUTOR2_LASTNAME, contributorOptional2.get().getLastName());

    assertTrue(contributorOptional3.isPresent());
    assertNull(contributorOptional3.get().getCnIndividu());
    assertEquals(this.CONTRIBUTOR3_FIRSTNAME, contributorOptional3.get().getFirstName());
    assertEquals(this.CONTRIBUTOR3_LASTNAME, contributorOptional3.get().getLastName());
  }

  private List<Contributor> getContributorsList(String publicationId) {
    this.restClientTool.sudoRoot();
    List<Contributor> contributors = this.publicationService.getContributors(publicationId);
    this.restClientTool.exitSudo();
    return contributors;
  }

  private List<ResearchGroup> getResearchGroupsList(String publicationId) {
    this.restClientTool.sudoRoot();
    List<ResearchGroup> researchGroups = this.publicationService.getResearchGroups(publicationId);
    this.restClientTool.exitSudo();
    return researchGroups;
  }
}
