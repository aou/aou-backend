/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PersonUnigeServiceIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;

public class PersonUnigeServiceIT extends AbstractAdminIT {

  @Autowired
  private Environment env;

  @Test
  public void searchPersonWithLastnameOnlyTest() {
    RestTemplate restTemplate = this.restClientTool.getClient();
    String url = this.getUrl() + "?lastname=Salihi";
    ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
    this.checkResults(response);
  }

  @Test
  public void searchPersonWithFirstnameOnlyTest() {
    RestTemplate restTemplate = this.restClientTool.getClient();
    String url = this.getUrl() + "?firstname=Dardan";
    ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
    this.checkResults(response);
  }

  @Test
  public void searchPersonWithFirstnameAndLastnameTest() {
    RestTemplate restTemplate = this.restClientTool.getClient();
    String url = this.getUrl() + "?lastname=Salihi&firstname=Dardan";
    ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
    this.checkResults(response);
  }

  @Test
  public void searchPersonWithoutAnyNameTest() {
    RestTemplate restTemplate = this.restClientTool.getClient();
    String url = this.getUrl();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> restTemplate.getForEntity(url, String.class));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Test
  public void searchPersonWithOneLetterTest() {
    RestTemplate restTemplate = this.restClientTool.getClient();
    String url = this.getUrl() + "?lastname=S&firstname=D";
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> restTemplate.getForEntity(url, String.class));
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("firstname", "The firstname must contain a minimum of 2 characters to perform the search",
            validationErrors));
    assertTrue(this.validationErrorsContain("lastname", "The lastname must contain a minimum of 2 characters to perform the search",
            validationErrors));
  }

  private String getUrl() {
    return this.env.getProperty("aou.module.admin.url") + "/" + ResourceName.EXTERNAL_DATA + "/" + AouActionName.SEARCH_UNIGE_PERSON;
  }

  private void checkResults(ResponseEntity<String> response) {
    ObjectMapper mapper = new ObjectMapper();
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertNotNull(response);
    assertNotNull(response.getBody());
    try {
      JsonNode collectionNode = mapper.readValue(response.getBody(), JsonNode.class);
      ArrayNode arrayNode = (ArrayNode) collectionNode.get("_data");
      List<JsonNode> listNode = new ArrayList<>();
      arrayNode.iterator().forEachRemaining(listNode::add);
      assertNotNull(arrayNode);
      assertTrue(listNode.stream()
              .anyMatch(node -> node.get("firstname").textValue().equals("Dardan") && node.get("lastname").textValue().equals("Salihi")));
      Optional<JsonNode> optionalResult = listNode.stream()
              .filter(node -> node.get("firstname").textValue().equals("Dardan") && node.get("lastname").textValue().equals("Salihi"))
              .findFirst();
      assertTrue(optionalResult.isPresent());
      JsonNode result = optionalResult.get();
      assertEquals("747918", result.get("cnIndividu").textValue());
      assertEquals("Salihi", result.get("lastname").textValue());
      assertEquals("Informaticien-ne de développement", result.get("function").textValue());
      assertEquals("Pilotage et e-administration (PILADM)", result.get("structure").textValue());
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("Unable to read JSON response", e);
    }
  }
}
