/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ErrorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.error;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.security.User;
import ch.unige.aou.rest.ResourceName;

@ActiveProfiles("client-sudo")
@ExtendWith(SpringExtension.class)
@RestClientTest(properties = { "spring.cloud.bootstrap.enabled=true" })
class ErrorIT {

  private static final Logger log = LoggerFactory.getLogger(ErrorIT.class);

  @Value("${solidify.oauth2.accesstoken.root:#{null}}")
  private String rootToken;

  @Value("${solidify.oauth2.accesstoken.admin:#{null}}")
  private String adminToken;

  @Value("${solidify.oauth2.accesstoken.user:#{null}}")
  private String userToken;

  @Value("${solidify.oauth2.accesstoken.thirdPartyUser:#{null}}")
  private String thirdPartyUserToken;

  @Value("${aou.module.admin.url}")
  private String adminUrl;

  protected SudoRestClientTool restClientTool;

  @BeforeEach
  void init() {
    this.restClientTool = new SudoRestClientTool(this.rootToken, this.adminToken, this.userToken, this.thirdPartyUserToken);
  }

  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  void badRequest() {
    final String url = this.adminUrl + "/" + ResourceName.LICENSES;
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.postForObject(url, null, String.class);
      fail("A BAD_REQUEST should be received");
    } catch (final HttpClientErrorException e) {
      log.info("400 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void forbidden() {
    final String url = this.adminUrl + "/" + ResourceName.LANGUAGES;
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(new User()), User.class);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("403 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void methodNotAllowed() {
    final String url = this.adminUrl;
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(""), String.class);
      fail("A METHOD_NOT_ALLOWED Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("405 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
    }
  }

  @Test
  void notFound() {
    final String url = this.adminUrl + "/doesntexists";
    try {
      final RestTemplate restTemplate = this.restClientTool.getClient();
      restTemplate.getForObject(url, String.class);
      fail("A NOT_FOUND Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("404 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
    }
  }

  @Test
  void unauthorized() {
    try {
      final URL url = new URL(this.adminUrl);
      String port = "";
      if (url.getPort() != -1) {
        port = ":" + url.getPort();
      }

      final String urlWithoutUserInfo = url.getProtocol() + "://" + url.getHost() + port + url.getPath();
      final RestTemplate restTemplate = new RestTemplate();
      restTemplate.getForObject(urlWithoutUserInfo, String.class);
      fail("An UNAUTHORIZED Http response should be received");
    } catch (final HttpClientErrorException e) {
      log.info("401 test : response body : " + e.getResponseBodyAsString());
      assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
    } catch (final MalformedURLException e) {
      e.printStackTrace();
    }
  }

}
