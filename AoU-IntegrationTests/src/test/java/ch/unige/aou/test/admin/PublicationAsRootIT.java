/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import ch.unige.aou.model.publication.Publication;

class PublicationAsRootIT extends PublicationIT {

  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Override
  protected String getCurrentUserId() {
    return this.TEST_USER_ROOT_ID;
  }

  @Test
  void createTest() {
    String title = "test as root publication";
    this.testPublicationCreation(title);
  }

  @Test
  void listMyPublicationsTest() {

    Map<String, String> params = new HashMap<>();
    params.put("title", this.getTemporaryTestLabel(""));
    params.put("sort", "title");

    // No Publication exists for user before creation
    List<Publication> myPublications = this.publicationService.listMyPublications(params);
    assertNotNull(myPublications);
    assertTrue(myPublications.isEmpty());

    String title1 = this.getTemporaryTestLabel("test publication 1");
    String formData1 = this.getTestFormData(title1);
    this.createNewTemporaryPublication(formData1);

    String title2 = this.getTemporaryTestLabel("test publication 2");
    String formData2 = this.getTestFormData(title2);
    this.createNewTemporaryPublication(formData2);

    String title3 = this.getTemporaryTestLabel("test publication 3");
    String formData3 = this.getTestFormData(title3);
    this.createNewTemporaryPublication(formData3);

    myPublications = this.publicationService.listMyPublications(params);
    assertNotNull(myPublications);
    assertFalse(myPublications.isEmpty());
    assertEquals(3, myPublications.size());

    String personId = this.profilePerson.getResId();
    assertEquals(title1, myPublications.get(0).getTitle());
    assertEquals(title2, myPublications.get(1).getTitle());
    assertEquals(title3, myPublications.get(2).getTitle());
    assertEquals(personId, myPublications.get(0).getCreator().getResId());
    assertEquals(personId, myPublications.get(1).getCreator().getResId());
    assertEquals(personId, myPublications.get(2).getCreator().getResId());

    this.restClientTool.sudoUser();
    List<Publication> userPublications = this.publicationService.listMyPublications(params);
    assertNotEquals(myPublications, userPublications);
    this.restClientTool.exitSudo();

    this.restClientTool.sudoAdmin();
    List<Publication> adminPublications = this.publicationService.listMyPublications(params);
    assertNotEquals(myPublications, adminPublications);
    this.restClientTool.exitSudo();
  }
}
