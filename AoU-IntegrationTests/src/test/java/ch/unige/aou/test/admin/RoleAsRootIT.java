/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - RoleAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.model.security.Role;
import ch.unige.aou.test.AouTestConstants;

class RoleAsRootIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void creationTest() {
    Role role = new Role();
    role.setName(AouTestConstants.getRandomNameWithTemporaryLabel("Role creation"));
    final Role role1 = this.roleService.create(role);

    // Test the creation
    assertNotNull(role1);
    assertEquals(role.getName(), role1.getName());
  }

  @Test
  void updateTest() {
    Role role = new Role();
    role.setName(AouTestConstants.getRandomNameWithTemporaryLabel("Role creation"));
    role.setSortValue(20);
    final Role createdRole = this.roleService.create(role);

    // Test the creation
    assertNotNull(createdRole);
    assertEquals(role.getName(), createdRole.getName());

    // Update the role name
    final String newRoleName = AouTestConstants.getRandomNameWithTemporaryLabel("Role update");
    role.setName(newRoleName);
    Role updatedRole = this.roleService.update(createdRole.getResId(), role);

    // Test the update
    Role fetchedRole = this.roleService.findOne(createdRole.getResId());
    assertNotNull(fetchedRole);
    assertEquals(newRoleName, fetchedRole.getName());
  }

  @Test
  void deleteTest() {
    // Create a role
    Role role = new Role();
    role.setName(AouTestConstants.getRandomNameWithTemporaryLabel("Role to delete"));
    final Role role1 = this.roleService.create(role);

    // Test the creation
    assertNotNull(role1);
    assertEquals(role.getName(), role1.getName());

    // Delete Role
    this.roleService.delete(role1.getResId());

    // Check if role deleted
    final String roleId1 = role1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.roleService.findOne(roleId1));
  }

  @Test
  void deleteListTest() {
    // Create a role
    Role role = new Role();
    role.setName(AouTestConstants.getRandomNameWithTemporaryLabel("Role to delete 1"));
    final Role roleCreated1 = this.roleService.create(role);

    // Test the creation
    assertNotNull(roleCreated1);
    assertEquals(role.getName(), roleCreated1.getName());

    // Create a second role
    Role role2 = new Role();
    role2.setName(AouTestConstants.getRandomNameWithTemporaryLabel("Role to delete 2"));
    final Role roleCreated2 = this.roleService.create(role2);

    // Test the creation
    assertNotNull(roleCreated2);
    assertEquals(role2.getName(), roleCreated2.getName());

    // Search Roles
    final Map<String, String> properties = new HashMap<>();
    properties.put("name", "Role");
    final List<Role> roleList = this.roleService.searchByProperties(properties);
    assertEquals(2, roleList.size());

    // Delete roles
    final String[] ids = new String[2];
    int i = 0;
    for (final Role r : roleList) {
      ids[i] = r.getResId();
      i++;
    }
    this.roleService.deleteList(ids);

    // Check if role deleted
    for (final Role r : roleList) {
      final String roleId = r.getResId();
      assertThrows(HttpClientErrorException.NotFound.class, () -> this.roleService.findOne(roleId));
    }
  }

  @Override
  protected void deleteFixtures() {
    this.clearRoleFixtures();
  }
}
