/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ValidationRightsIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.display.StructureValidationRight;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;

class ValidationRightsIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.restClientTool.exitSudo();
  }

  /********************************************************************************/

  @Test
  void addDefaultValidationRightsOn1StructureTest() {
    final Structure topStructure = this.getTopStructure();
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give default validation rights to the person on the structure
    this.personService.addValidationRightStructure(person.getResId(), topStructure.getResId());

    final List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    assertEquals(1, validationRightsList.size()); // validation rights given on one structure only

    List<PublicationSubtype> publicationSubtypeList = this.publicationSubtypeService.findAll();
    assertEquals(validationRightsList.get(0).getPublicationSubtypes().size(), publicationSubtypeList.size());
  }

  @Test
  void addDefaultValidationRightsOn2StructuresTest() {
    final Structure topStructure1 = this.getTopStructure();
    final Structure topStructure2 = this.createRemoteStructure("2nd top structure");
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give default validation rights to the person on 2 structures
    this.personService.addValidationRightStructure(person.getResId(), topStructure1.getResId(), topStructure2.getResId());

    final List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    assertEquals(2, validationRightsList.size()); // validation rights given on 2 structures

    List<PublicationSubtype> publicationSubtypeList = this.publicationSubtypeService.findAll();

    StructureValidationRight structureValidationRight1 = validationRightsList.get(0);
    assertTrue(structureValidationRight1.getPublicationSubtypes().size() > 0);
    assertEquals(publicationSubtypeList.size(), structureValidationRight1.getPublicationSubtypes().size());

    StructureValidationRight structureValidationRight2 = validationRightsList.get(1);
    assertTrue(structureValidationRight2.getPublicationSubtypes().size() > 0);
    assertEquals(publicationSubtypeList.size(), structureValidationRight2.getPublicationSubtypes().size());

    assertNotEquals(structureValidationRight1.getResId(), structureValidationRight2.getResId());
    assertNotEquals(structureValidationRight1.getCnStructC(), structureValidationRight2.getCnStructC());
    assertNotEquals(structureValidationRight1.getCodeStruct(), structureValidationRight2.getCodeStruct());
  }

  @Test
  void addSpecificValidationRightsOn1StructureTest() {
    List<PublicationSubtype> publicationSubtypeList = this.publicationSubtypeService.findAll();

    final Structure topStructure = this.getTopStructure();
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give specific validation rights to the person on the structure
    String subtypeId1 = publicationSubtypeList.get(0).getResId();
    String subtypeId2 = publicationSubtypeList.get(1).getResId();
    String subtypeId3 = publicationSubtypeList.get(2).getResId();
    this.personService.addValidationRightStructureWithSubtypes(person.getResId(), topStructure.getResId(), subtypeId1, subtypeId2, subtypeId3);

    final List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    assertEquals(1, validationRightsList.size()); // validation rights given on one structure only

    assertEquals(3, validationRightsList.get(0).getPublicationSubtypes().size());
    List<String> fetchedSubtypeIds = validationRightsList.get(0).getPublicationSubtypes().stream().map(PublicationSubtype::getResId).collect(
            Collectors.toList());
    assertTrue(fetchedSubtypeIds.contains(subtypeId1));
    assertTrue(fetchedSubtypeIds.contains(subtypeId2));
    assertTrue(fetchedSubtypeIds.contains(subtypeId3));
  }

  @Test
  void addSpecificValidationRightsOn2StructuresTest() {
    List<PublicationSubtype> publicationSubtypeList = this.publicationSubtypeService.findAll();

    final Structure topStructure1 = this.getTopStructure();
    final Structure topStructure2 = this.createRemoteStructure("2nd top structure");
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give specific validation rights to the person on the structure
    String subtypeId1 = publicationSubtypeList.get(0).getResId();
    String subtypeId2 = publicationSubtypeList.get(1).getResId();
    String subtypeId3 = publicationSubtypeList.get(2).getResId();
    this.personService.addValidationRightStructureWithSubtypes(person.getResId(), topStructure1.getResId(), subtypeId1);
    this.personService.addValidationRightStructureWithSubtypes(person.getResId(), topStructure2.getResId(), subtypeId2, subtypeId3);

    final List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    assertEquals(2, validationRightsList.size()); // validation rights given on 2 structures

    StructureValidationRight structureValidationRight1 = validationRightsList.stream()
            .filter(structureValidationRight -> structureValidationRight.getResId().equals(topStructure1.getResId())).findFirst().get();
    assertEquals(1, structureValidationRight1.getPublicationSubtypes().size());
    List<String> fetchedSubtypeIds1 = structureValidationRight1.getPublicationSubtypes().stream().map(PublicationSubtype::getResId).collect(
            Collectors.toList());
    assertTrue(fetchedSubtypeIds1.contains(subtypeId1));
    assertFalse(fetchedSubtypeIds1.contains(subtypeId2));
    assertFalse(fetchedSubtypeIds1.contains(subtypeId3));

    StructureValidationRight structureValidationRight2 = validationRightsList.stream()
            .filter(structureValidationRight -> structureValidationRight.getResId().equals(topStructure2.getResId())).findFirst().get();
    assertEquals(2, structureValidationRight2.getPublicationSubtypes().size());
    List<String> fetchedSubtypeIds2 = structureValidationRight2.getPublicationSubtypes().stream().map(PublicationSubtype::getResId).collect(
            Collectors.toList());
    assertFalse(fetchedSubtypeIds2.contains(subtypeId1));
    assertTrue(fetchedSubtypeIds2.contains(subtypeId2));
    assertTrue(fetchedSubtypeIds2.contains(subtypeId3));
  }

  @Test
  void getValidationRightsStructureTest() {
    final Structure topStructure = this.getTopStructure();
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give default validation rights to the person on the structure
    this.personService.addValidationRightStructure(person.getResId(), topStructure.getResId());

    StructureValidationRight structureValidationRight = this.personService
            .getValidationRightsStructure(person.getResId(), topStructure.getResId());

    List<PublicationSubtype> publicationSubtypeList = this.publicationSubtypeService.findAll();

    assertNotNull(structureValidationRight);
    assertEquals(topStructure.getResId(), structureValidationRight.getResId());
    assertEquals(publicationSubtypeList.size(), structureValidationRight.getPublicationSubtypes().size());
  }

  @Test
  void removeAllValidationRightsFormStructureTest() {
    List<PublicationSubtype> publicationSubtypeList = this.publicationSubtypeService.findAll();

    final Structure topStructure = this.getTopStructure();
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give default validation rights to the person on the structure
    this.personService.addValidationRightStructure(person.getResId(), topStructure.getResId());

    List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    List<String> fetchedSubtypeIds1 = validationRightsList.get(0).getPublicationSubtypes().stream().map(PublicationSubtype::getResId)
            .collect(Collectors.toList());
    assertEquals(1, validationRightsList.size()); // validation rights given on one structure only
    assertTrue(fetchedSubtypeIds1.size() > 0);
    assertEquals(publicationSubtypeList.size(), fetchedSubtypeIds1.size());

    // remove all rights from structure
    this.personService.removeValidationRightStructure(person.getResId(), topStructure.getResId());

    validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    assertTrue(validationRightsList.isEmpty()); // validation rights were given on 1 structure only --> no more exists
  }

  @Test
  void removeSpecificValidationRightsFormStructureTest() {
    List<PublicationSubtype> publicationSubtypeList = this.publicationSubtypeService.findAll();
    String subtypeId1 = publicationSubtypeList.get(0).getResId();
    String subtypeId2 = publicationSubtypeList.get(1).getResId();
    String subtypeId3 = publicationSubtypeList.get(2).getResId();

    final Structure topStructure = this.getTopStructure();
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give default validation rights to the person on the structure
    this.personService.addValidationRightStructure(person.getResId(), topStructure.getResId());

    List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    List<String> fetchedSubtypeIds1 = validationRightsList.get(0).getPublicationSubtypes().stream().map(PublicationSubtype::getResId)
            .collect(Collectors.toList());
    assertEquals(1, validationRightsList.size()); // validation rights given on one structure only
    assertEquals(publicationSubtypeList.size(), fetchedSubtypeIds1.size());
    assertTrue(fetchedSubtypeIds1.contains(subtypeId1));
    assertTrue(fetchedSubtypeIds1.contains(subtypeId2));
    assertTrue(fetchedSubtypeIds1.contains(subtypeId3));

    // remove specific rights on structure
    this.personService
            .removeValidationRightStructureWithSubtypes(person.getResId(), topStructure.getResId(), subtypeId1, subtypeId2, subtypeId3);

    validationRightsList = this.personService.getValidationRightStructuresList(person.getResId());
    List<String> fetchedSubtypeIds2 = validationRightsList.get(0).getPublicationSubtypes().stream().map(PublicationSubtype::getResId)
            .collect(Collectors.toList());

    assertEquals(1, validationRightsList.size()); // validation rights given on one structure only
    assertTrue(fetchedSubtypeIds2.size() > 0);
    assertEquals(fetchedSubtypeIds1.size() - 3, fetchedSubtypeIds2.size());
    assertFalse(fetchedSubtypeIds2.contains(subtypeId1));
    assertFalse(fetchedSubtypeIds2.contains(subtypeId2));
    assertFalse(fetchedSubtypeIds2.contains(subtypeId3));
  }

  @Test
  void addDefaultValidationRightsOn1StructureAsUserTest() {
    final Structure topStructure = this.getTopStructure();
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int validationRightsStartSize = this.personService.getValidationRightStructuresList(person.getResId()).size();
    assertEquals(0, validationRightsStartSize);

    // Give default validation rights to the person on the structure
    this.restClientTool.sudoUser();
    String personId = person.getResId();
    String topStructureId = topStructure.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personService.addValidationRightStructure(personId, topStructureId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    this.restClientTool.exitSudo();
  }

  @Test
  void removeAllValidationRightsFormStructureAsUserTest() {
    final Structure topStructure = this.getTopStructure();
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);

    // remove all rights from structure
    this.restClientTool.sudoUser();
    String personId = person.getResId();
    String topStructureId = topStructure.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personService.removeValidationRightStructure(personId, topStructureId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    this.restClientTool.exitSudo();
  }

  /********************************************************************************/

  @Test
  void listValidationRightsAsUserTest() {
    Person userPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_USER_ID));
    Person adminPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_ADMIN_ID));

    this.restClientTool.sudoUser();

    // user can list his validation rights
    final List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(userPerson.getResId());
    assertNotNull(validationRightsList);

    // user cannot list another person rights
    String adminPersonId = adminPerson.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personService.getValidationRightStructuresList(adminPersonId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    this.restClientTool.exitSudo();
  }

  @Test
  void listValidationRightsAsAdminTest() {
    Person userPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_USER_ID));
    Person adminPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_ADMIN_ID));

    this.restClientTool.sudoAdmin();

    // admin can list his validation rights
    List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(adminPerson.getResId());
    assertNotNull(validationRightsList);

    // admin can list another person rights
    validationRightsList = this.personService.getValidationRightStructuresList(userPerson.getResId());
    assertNotNull(validationRightsList);

    this.restClientTool.exitSudo();
  }

  @Test
  void listValidationRightsAsRootTest() {
    Person userPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_USER_ID));
    Person rootPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_ROOT_ID));

    this.restClientTool.sudoAdmin();

    // root can list his validation rights
    List<StructureValidationRight> validationRightsList = this.personService.getValidationRightStructuresList(rootPerson.getResId());
    assertNotNull(validationRightsList);

    // admin can list another person rights
    validationRightsList = this.personService.getValidationRightStructuresList(userPerson.getResId());
    assertNotNull(validationRightsList);

    this.restClientTool.exitSudo();
  }

  /********************************************************************************/

  @Test
  void findValidablePublicationsWithRightOnParentStructureTest() {
    this.restClientTool.sudoRoot();
    this.createStructuresHierarchy();
    this.restClientTool.exitSudo();

    /**
     * Create a publication linked to child structure11
     */
    Structure structure11 = this.findStructureByName(this.getTemporaryTestLabel("child11"));
    Publication savedPublication = this.createPublicationInStructure(structure11);

    /**
     * Find validable deposits
     */
    List<Publication> publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());

    /**
     * Give validation rights on parent structure for the wrong subtype ---> the deposit is still
     * invisible
     */
    Person userPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_USER_ID));
    Structure structure1 = this.findStructureByName(this.getTemporaryTestLabel("parent1"));
    PublicationSubtype subtype1 = this.publicationSubtypeService.findOne(this.PUBLICATION_SUBTYPE_ARTICLE_PROFESSIONNEL_ID);
    this.giveValidationRight(userPerson, structure1, subtype1);
    publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());

    /**
     * Give validation rights on parent structure for the correct subtype ---> the deposit gets visible
     */
    PublicationSubtype subtype2 = this.publicationSubtypeService.findOne(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID);
    this.giveValidationRight(userPerson, structure1, subtype2);
    publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(savedPublication.getResId(), publications.get(0).getResId());
  }

  @Test
  void findValidablePublicationsWithRightOnOtherStructureBranchTest() {
    this.restClientTool.sudoRoot();
    this.createStructuresHierarchy();
    this.restClientTool.exitSudo();

    /**
     * Create a publication linked to child structure11
     */
    Structure structure11 = this.findStructureByName(this.getTemporaryTestLabel("child11"));
    Publication savedPublication = this.createPublicationInStructure(structure11);

    /**
     * Find validable deposits
     */
    List<Publication> publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());

    /**
     * Give validation rights on wrong parent structure for the correct subtype ---> the deposit is
     * still invisible
     */
    Person userPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_USER_ID));
    Structure structure2 = this.findStructureByName(this.getTemporaryTestLabel("parent2"));
    PublicationSubtype subtype1 = this.publicationSubtypeService.findOne(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID);
    this.giveValidationRight(userPerson, structure2, subtype1);
    publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());
  }

  @Test
  void findValidablePublicationsWithRightOnSameStructureTest() {
    this.restClientTool.sudoRoot();
    this.createStructuresHierarchy();
    this.restClientTool.exitSudo();

    /**
     * Create a publication linked to child structure11
     */
    Structure structure11 = this.findStructureByName(this.getTemporaryTestLabel("child11"));
    Publication savedPublication = this.createPublicationInStructure(structure11);

    /**
     * Find validable deposits
     */
    List<Publication> publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());

    /**
     * Give validation rights on same structure for the wrong subtype ---> the deposit is still
     * invisible
     */
    Person userPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_USER_ID));
    PublicationSubtype subtype1 = this.publicationSubtypeService.findOne(this.PUBLICATION_SUBTYPE_ARTICLE_PROFESSIONNEL_ID);
    this.giveValidationRight(userPerson, structure11, subtype1);
    publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());

    /**
     * Give validation rights on parent structure for the correct subtype ---> the deposit gets visible
     */
    PublicationSubtype subtype2 = this.publicationSubtypeService.findOne(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID);
    this.giveValidationRight(userPerson, structure11, subtype2);
    publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(savedPublication.getResId(), publications.get(0).getResId());
  }

  @Test
  void findValidablePublicationsWithRightOnChildStructureBranchTest() {
    this.restClientTool.sudoRoot();
    this.createStructuresHierarchy();
    this.restClientTool.exitSudo();

    /**
     * Create a publication linked to child parent1
     */
    Structure structure1 = this.findStructureByName(this.getTemporaryTestLabel("parent1"));
    Publication savedPublication = this.createPublicationInStructure(structure1);

    /**
     * Find validable deposits
     */
    List<Publication> publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());

    /**
     * Give validation rights on child structure for the correct subtype ---> the deposit is still
     * invisible
     */
    Person userPerson = this.personService.findOne(this.getPersonIdFromUserId(this.TEST_USER_USER_ID));
    Structure structure11 = this.findStructureByName(this.getTemporaryTestLabel("child11"));
    PublicationSubtype subtype1 = this.publicationSubtypeService.findOne(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID);
    this.giveValidationRight(userPerson, structure11, subtype1);
    publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());
  }

  @Test
  void findValidablePublicationsAsRolesTest() {
    this.restClientTool.sudoRoot();
    this.createStructuresHierarchy();
    this.restClientTool.exitSudo();

    /**
     * Create a publication linked to child structure11
     */
    Structure structure11 = this.findStructureByName(this.getTemporaryTestLabel("child11"));
    Publication savedPublication = this.createPublicationInStructure(structure11);

    /**
     * Find validable deposits as USER
     */
    List<Publication> publications = this.findValidablePublicationsAsUser();
    assertNotNull(publications);
    assertEquals(0, publications.size());

    /**
     * Find validable deposits as ADMIN
     */
    publications = this.findValidablePublicationsAsAdmin(savedPublication.getTitle(), false);
    assertNotNull(publications);
    assertEquals(0, publications.size());

    publications = this.findValidablePublicationsAsAdmin(savedPublication.getTitle(), true);
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(savedPublication.getResId(), publications.get(0).getResId());

    /**
     * Find validable deposits as ROOT
     */
    publications = this.findValidablePublicationsAsRoot(savedPublication.getTitle(), false);
    assertNotNull(publications);
    assertEquals(0, publications.size());

    publications = this.findValidablePublicationsAsRoot(savedPublication.getTitle(), true);
    assertNotNull(publications);
    assertEquals(1, publications.size());
    assertEquals(savedPublication.getResId(), publications.get(0).getResId());
  }

  /********************************************************************************/

  @Test
  void getOwnPublicationAsUserTest() {
    final Structure topStructure = this.getTopStructure();
    this.restClientTool.sudoUser();
    Publication savedPublication = this.createPublicationInStructure(topStructure);

    Publication foundPublication = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(savedPublication.getResId(), foundPublication.getResId());
    this.restClientTool.exitSudo();
  }

  @Test
  void getOtherPublicationAsUserTest() {
    final Structure topStructure = this.getTopStructure();
    Publication savedPublication = this.createPublicationInStructure(topStructure);

    this.restClientTool.sudoUser();
    Publication publication = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(publication);
    assertEquals(savedPublication.getResId(), publication.getResId());
    this.restClientTool.exitSudo();
  }

  @Test
  void getValidablePublicationAsUserTest() {
    final Structure topStructure = this.getTopStructure();
    Publication savedPublication = this.createPublicationInStructure(topStructure);

    // user can access publication created by admin without validation right
    this.restClientTool.sudoUser();
    Publication foundPublicationWithoutValidationRight = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(foundPublicationWithoutValidationRight);
    assertEquals(savedPublication.getResId(), foundPublicationWithoutValidationRight.getResId());
    this.restClientTool.exitSudo();

    // give validation right
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    this.personService.addValidationRightStructureWithSubtypes(person.getResId(), topStructure.getResId(),
            this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID);

    // user can now access
    this.restClientTool.sudoUser();
    Publication foundPublication = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(savedPublication.getResId(), foundPublication.getResId());
    this.restClientTool.exitSudo();
  }

  @Test
  void getOwnPublicationAsAdminTest() {
    final Structure topStructure = this.getTopStructure();
    Publication savedPublication = this.createPublicationInStructure(topStructure);

    Publication foundPublication = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(savedPublication.getResId(), foundPublication.getResId());
  }

  @Test
  void getOtherPublicationAsAdminTest() {
    final Structure topStructure = this.getTopStructure();
    this.restClientTool.sudoUser();
    Publication savedPublication = this.createPublicationInStructure(topStructure);
    this.restClientTool.exitSudo();

    Publication foundPublication = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(savedPublication.getResId(), foundPublication.getResId());
  }

  @Test
  void getOwnPublicationAsRootTest() {
    this.restClientTool.sudoRoot();
    final Structure topStructure = this.getTopStructure();
    Publication savedPublication = this.createPublicationInStructure(topStructure);

    Publication foundPublication = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(savedPublication.getResId(), foundPublication.getResId());
    this.restClientTool.exitSudo();
  }

  @Test
  void getOtherPublicationAsRootTest() {
    final Structure topStructure = this.getTopStructure();
    this.restClientTool.sudoUser();
    Publication savedPublication = this.createPublicationInStructure(topStructure);
    this.restClientTool.exitSudo();

    this.restClientTool.sudoRoot();
    Publication foundPublication = this.publicationService.findOne(savedPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(savedPublication.getResId(), foundPublication.getResId());
    this.restClientTool.exitSudo();
  }

  /********************************************************************************/

  private Publication createPublicationInStructure(Structure structure) {
    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    List<Structure> structures = new ArrayList<>();
    structures.add(structure);
    properties.put("structures", structures);
    String xmlMetadata = this.getMetadata(properties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    return savedPublication;
  }

  private List<Publication> findValidablePublicationsAsUser() {
    this.restClientTool.sudoUser();
    Map<String, String> props = new HashMap<>();
    List<Publication> publications = this.publicationService.listValidablePublications(props);
    this.restClientTool.exitSudo();
    return publications;
  }

  private List<Publication> findValidablePublicationsAsAdmin(String title, boolean showAllDeposits) {
    this.restClientTool.sudoAdmin();
    List<Publication> publications = this.findValidablePublications(title, showAllDeposits);
    this.restClientTool.exitSudo();
    return publications;
  }

  private List<Publication> findValidablePublicationsAsRoot(String title, boolean showAllDeposits) {
    this.restClientTool.sudoRoot();
    List<Publication> publications = this.findValidablePublications(title, showAllDeposits);
    this.restClientTool.exitSudo();
    return publications;
  }

  private List<Publication> findValidablePublications(String title, boolean showAllDeposits) {
    Map<String, String> props = new HashMap<>();
    props.put("title", title);
    if (showAllDeposits) {
      props.put("showAllDeposits", "true");
    }
    return this.publicationService.listValidablePublications(props);
  }

  private void giveValidationRight(Person person, Structure structure, PublicationSubtype publicationSubtype) {
    this.restClientTool.sudoAdmin();
    this.personService.addValidationRightStructureWithSubtypes(person.getResId(), structure.getResId(), publicationSubtype.getResId());
    this.restClientTool.exitSudo();
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearPublicationFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.deleteGrandChildrenStructures();
    this.clearStructureFixtures();
    this.restClientTool.exitSudo();
  }
}
