/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - NotificationAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.security.User;

class NotificationAsUserIT extends AbstractNotificationIT {

  @Test
  void createNotificationTest() {
    List<Notification> notifications = this.personNotificationClientService.findAll(this.getCurrentPerson().getResId());
    assertNotNull(notifications);
    int initialSize = notifications.size();

    this.createNotificationForCurrentUser();
    notifications = this.personNotificationClientService.findAll(this.getCurrentPerson().getResId());
    assertNotNull(notifications);
    assertEquals(initialSize + 1, notifications.size());
  }

  @Test
  void getNotificationWithEventAndPublicationIdTest() {
    Publication notifiedPublication = this.createNotificationForCurrentUser();

    Map<String, String> queryParameters = new HashMap<>();
    queryParameters.put("sort", "lastUpdate.when,desc");
    Notification notification = this.getLastNotificationForCurrentUser();
    assertNotNull(notification);
    Event event = notification.getEvent();
    assertNotNull(event);
    assertEquals(notifiedPublication.getResId(), event.getPublication().getResId());
  }

  @Test
  void markNotificationAsReadTest() {
    this.createNotificationForCurrentUser();
    Notification notification = this.getLastNotificationForCurrentUser();
    assertNotNull(notification);
    assertNull(notification.getReadTime(), "Initial notification read time should be null");

    OffsetDateTime beforeReadTime = OffsetDateTime.now();
    this.personNotificationClientService.markNotificationAsRead(this.getCurrentPerson().getResId(), notification.getResId());
    notification = this.getLastNotificationForCurrentUser();
    assertNotNull(notification);
    assertNotNull(notification.getReadTime(), "Notification should have a read time set at this step");
    assertTrue(notification.getReadTime().isAfter(beforeReadTime));
  }

  @Test
  void cannotReadOtherUserNotificationTest() {
    this.restClientTool.sudoAdmin();
    Publication notifiedPublication = this.createNotificationForCurrentUser();
    this.restClientTool.exitSudo();

    // user has no notification yet
    Notification notification = this.getLastNotificationForCurrentUser();
    assertNull(notification);

    // although the notification exists and is readable by the admin
    this.restClientTool.sudoRoot();
    User adminUser = this.userService.findOne(this.TEST_USER_ADMIN_ID);
    this.restClientTool.exitSudo();
    this.restClientTool.sudoAdmin();
    Map<String, String> queryParameters = new HashMap<>();
    queryParameters.put("sort", "lastUpdate.when,desc");
    List<Notification> notifications = this.personNotificationClientService.findAll(adminUser.getPerson().getResId(), queryParameters);
    assertNotNull(notifications);
    assertFalse(notifications.isEmpty());
    assertEquals(notifiedPublication.getResId(), notifications.get(0).getEvent().getPublication().getResId());
    this.restClientTool.exitSudo();

    // try to read the admin notification directly
    Notification adminNotification = notifications.get(0);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personNotificationClientService.findOne(adminUser.getPerson().getResId(), adminNotification.getResId());
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void cannotDeleteNotificationTest() {
    this.createNotificationForCurrentUser();
    Notification notification = this.getLastNotificationForCurrentUser();
    assertNotNull(notification);

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personNotificationClientService.delete(this.getCurrentPerson().getResId(), notification.getResId());
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Override
  protected String getCurrentUserId() {
    return this.TEST_USER_USER_ID;
  }
}
