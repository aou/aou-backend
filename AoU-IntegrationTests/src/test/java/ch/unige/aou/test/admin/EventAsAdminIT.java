/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - EventAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static ch.unige.aou.test.AouTestConstants.EVENT_MESSAGE_TEST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.notification.EventType;

class EventAsAdminIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    final Event event = new Event();
    event.setMessage(EVENT_MESSAGE_TEST);
    event.setPublication(this.getPermanentPublication());
    event.setTriggerBy(this.getPermanentPerson(AuthApplicationRole.USER));
    event.setEventType(EventType.PUBLICATION_CREATED);

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.eventService.create(event);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void findAllTest() {
    final Event event = new Event();
    event.setMessage(EVENT_MESSAGE_TEST);
    event.setPublication(this.getPermanentPublication());
    event.setTriggerBy(this.getPermanentPerson(AuthApplicationRole.USER));
    event.setEventType(EventType.PUBLICATION_CREATED);

    // to create an event there is need to use root token
    this.restClientTool.sudoRoot();
    Event eventCreated = this.eventService.create(event);
    this.restClientTool.exitSudo();

    assertNotNull(eventCreated);

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.eventService.findAll();
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearEventsFixtures();
    this.restClientTool.exitSudo();
  }
}
