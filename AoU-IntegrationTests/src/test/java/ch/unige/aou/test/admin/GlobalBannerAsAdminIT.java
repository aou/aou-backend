/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - GlobalBannerAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.OffsetDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.model.GlobalBanner;

class GlobalBannerAsAdminIT extends GlobalBannerIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    // Create a globalBanner
    final GlobalBanner savedGlobalBanner = this.createTestGlobalBanner("My banner", GlobalBanner.GlobalBannerType.CRITICAL, false,
            OffsetDateTime.now(), OffsetDateTime.now().plusDays(1));

    // Test the creation
    final GlobalBanner foundGlobalBanner = this.globalBannerService.findOne(savedGlobalBanner.getResId());
    assertNotNull(foundGlobalBanner);
    this.assertsGlobalBanner(savedGlobalBanner, foundGlobalBanner);

    // Test unique name
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.createTestGlobalBanner("My banner", GlobalBanner.GlobalBannerType.INFO, false, OffsetDateTime.now(),
              OffsetDateTime.now().plusDays(1));
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Name is unique");
  }

  @Test
  void creationTestConflictingPeriod() {
    OffsetDateTime now = OffsetDateTime.now();
    OffsetDateTime startDate = now.minusYears(10);
    OffsetDateTime endDate = startDate.plusDays(1);
    // Create a globalBanner
    final GlobalBanner savedOriginalGlobalBanner = this.createTestGlobalBanner("Original banner", GlobalBanner.GlobalBannerType.CRITICAL, true,
            startDate, endDate);

    // Test the creation
    final GlobalBanner foundOriginalGlobalBanner = this.globalBannerService.findOne(savedOriginalGlobalBanner.getResId());
    assertNotNull(foundOriginalGlobalBanner);
    this.assertsGlobalBanner(savedOriginalGlobalBanner, foundOriginalGlobalBanner);

    // Test conflicting date inside original global banner
    OffsetDateTime startDateConflicting1 = startDate.plusHours(2);
    OffsetDateTime endDateConflicting1 = endDate.minusHours(1);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.createTestGlobalBanner("Conflicting banner", GlobalBanner.GlobalBannerType.INFO, true, startDateConflicting1, endDateConflicting1);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Conflicting date 1");

    // Test conflicting date end during original global banner
    OffsetDateTime startDateConflicting2 = startDate.minusHours(2);
    OffsetDateTime endDateConflicting2 = endDate.minusHours(2);
    HttpClientErrorException e2 = assertThrows(HttpClientErrorException.class, () -> {
      this.createTestGlobalBanner("Conflicting banner 2", GlobalBanner.GlobalBannerType.INFO, true, startDateConflicting2, endDateConflicting2);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e2.getStatusCode(), "Conflicting date 2");

    // Test conflicting date start during original global banner
    OffsetDateTime startDateConflicting3 = startDate.plusHours(2);
    OffsetDateTime endDateConflicting3 = endDate.plusHours(2);
    HttpClientErrorException e3 = assertThrows(HttpClientErrorException.class, () -> {
      this.createTestGlobalBanner("Conflicting banner 3", GlobalBanner.GlobalBannerType.INFO, true, startDateConflicting3, endDateConflicting3);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e3.getStatusCode(), "Conflicting date 3");

    // Test non-conflicting date with original global banner
    OffsetDateTime startDateNonConflicting = startDate.plusDays(2);
    OffsetDateTime endDateNonConflicting = endDate.plusDays(2);
    final GlobalBanner savedNonConflictingBanner = this.createTestGlobalBanner("Non conflicting banner", GlobalBanner.GlobalBannerType.INFO,
            true, startDateNonConflicting, endDateNonConflicting);
    final GlobalBanner foundNonConflictingBanner = this.globalBannerService.findOne(savedNonConflictingBanner.getResId());
    assertNotNull(foundNonConflictingBanner);
    this.assertsGlobalBanner(savedNonConflictingBanner, foundNonConflictingBanner);
  }
}
