/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - GlobalBannerAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.model.GlobalBanner;

class GlobalBannerAsUserIT extends GlobalBannerIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  void getActive() {
    this.restClientTool.sudoRoot();
    List<GlobalBanner> listGlobalBanner = this.globalBannerService.findAll();

    OffsetDateTime now = OffsetDateTime.now();

    List<GlobalBanner> listMatchingGlobalBanner = listGlobalBanner.stream().filter(globalBanner ->
            globalBanner.getEnabled()
                    && globalBanner.getStartDate().isBefore(now)
                    && globalBanner.getEndDate().isAfter(now)
    ).collect(Collectors.toList());
    this.restClientTool.exitSudo();

    GlobalBanner expectedActiveGlobalBanner = null;
    if (listMatchingGlobalBanner.size() == 0) {
      // If no banner is already active create a temporary banner
      expectedActiveGlobalBanner = this.createTestGlobalBanner("Active banner", GlobalBanner.GlobalBannerType.CRITICAL, true,
              now.minusMinutes(2), now.plusMinutes(2));
    } else {
      expectedActiveGlobalBanner = listMatchingGlobalBanner.get(0);
    }

    GlobalBanner activeGlobalBanner = this.globalBannerService.getActive();
    this.assertsGlobalBanner(expectedActiveGlobalBanner, activeGlobalBanner);
  }
}
