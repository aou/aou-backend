/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - EventAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static ch.unige.aou.test.AouTestConstants.EVENT_MESSAGE_TEST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.notification.EventType;

class EventAsRootIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void creationTest() {
    final Event event = new Event();
    event.setMessage(EVENT_MESSAGE_TEST);
    event.setPublication(this.getPermanentPublication());
    event.setTriggerBy(this.getPermanentPerson(AuthApplicationRole.USER));
    event.setEventType(EventType.PUBLICATION_CREATED);

    Event eventCreated = this.eventService.create(event);

    assertNotNull(eventCreated);
    assertEquals(eventCreated.getMessage(), event.getMessage());
    assertEquals(eventCreated.getEventType(), event.getEventType());
    assertEquals(eventCreated.getTriggerBy().getResId(), event.getTriggerBy().getResId());
    assertEquals(eventCreated.getPublication().getResId(), event.getPublication().getResId());
  }

  @Test
  void findOneTest() {
    final Event event = new Event();
    event.setMessage(EVENT_MESSAGE_TEST);
    event.setPublication(this.getPermanentPublication());
    event.setTriggerBy(this.getPermanentPerson(AuthApplicationRole.USER));
    event.setEventType(EventType.PUBLICATION_CREATED);
    Event eventCreated = this.eventService.create(event);
    assertNotNull(eventCreated);

    List<Event> eventList = this.eventService.findAll();
    assertFalse(eventList.isEmpty());
  }

  @Test
  void updateEventTest() {
    final Event event = new Event();
    event.setMessage(EVENT_MESSAGE_TEST);
    event.setPublication(this.getPermanentPublication());
    event.setTriggerBy(this.getPermanentPerson(AuthApplicationRole.USER));
    event.setEventType(EventType.PUBLICATION_CREATED);
    Event eventCreated = this.eventService.create(event);

    Event eventUpdate = new Event();
    eventUpdate.setEventType(EventType.PUBLICATION_TO_VALIDATE);

    final String eventId = eventCreated.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.eventService.update(eventId, eventUpdate));
  }

  @Test
  void deleteEventTest() {
    final Event event = new Event();
    event.setMessage(EVENT_MESSAGE_TEST);
    event.setPublication(this.getPermanentPublication());
    event.setTriggerBy(this.getPermanentPerson(AuthApplicationRole.USER));
    event.setEventType(EventType.PUBLICATION_CREATED);
    Event eventCreated = this.eventService.create(event);

    this.eventService.delete(eventCreated.getResId());

    final String eventId = eventCreated.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.eventService.findOne(eventId));
  }

  @Override
  protected void deleteFixtures() {
    this.clearEventsFixtures();
  }

}
