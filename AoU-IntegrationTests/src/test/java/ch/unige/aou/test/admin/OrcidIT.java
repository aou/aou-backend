/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - OrcidIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;

class OrcidIT extends AbstractContributorIT {

  private static final String TEST_ORCID = "1111-2222-3333-4444";

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void testSyncOrcidFromPersonToContributor() {
    /*
     * Find the person linked to the user connected
     */
    final User currentUser = this.userService.getAuthenticatedUser();
    final Person currentPerson = this.getLinkedPersonToUser(currentUser);
    assertNull(currentPerson.getOrcid());
    String cnIndividu = currentUser.getExternalUid();
    List<Contributor> contributors = this.contributorService.searchByProperties(Collections.singletonMap("cnIndividu", cnIndividu));
    assertTrue(contributors.isEmpty());

    // Create contributor with same cnIndividu as current user, without ORCID
    final Contributor contributor = new Contributor();
    contributor.setLastName(currentPerson.getLastName());
    contributor.setFirstName(currentPerson.getFirstName());
    contributor.setCnIndividu(cnIndividu);
    final Contributor savedContributor = this.contributorService.create(contributor);
    assertNotNull(savedContributor);
    assertNull(savedContributor.getOrcid());

    // Check contributor can now be found and doesn't have any ORCID
    contributors = this.contributorService.searchByProperties(Collections.singletonMap("cnIndividu", cnIndividu));
    assertEquals(1, contributors.size());
    assertEquals(cnIndividu, contributors.get(0).getCnIndividu());
    assertNull(contributors.get(0).getOrcid());

    // Updating the person must propagate the ORCID to the contributor with the same cnIndividu
    currentPerson.setOrcid(TEST_ORCID);
    final Person savedPerson = this.personService.update(currentPerson.getResId(), currentPerson);
    assertEquals(TEST_ORCID, savedPerson.getOrcid());

    // Check contributor can now be found and has the same ORCID
    contributors = this.contributorService.searchByProperties(Collections.singletonMap("cnIndividu", cnIndividu));
    assertEquals(1, contributors.size());
    assertEquals(cnIndividu, contributors.get(0).getCnIndividu());
    assertEquals(TEST_ORCID, contributors.get(0).getOrcid());
  }

  @Override
  protected void deleteFixtures() {
    super.deleteFixtures();
    User currentUser = this.userService.getAuthenticatedUser();
    List<Contributor> contributorsList = this.contributorService.searchByProperties(
            Collections.singletonMap("cnIndividu", currentUser.getExternalUid()));
    for (final ch.unige.aou.model.publication.Contributor contributor : contributorsList) {
      this.contributorService.delete(contributor.getResId());
    }

    // clean current person orcid
    final Person currentPerson = this.getLinkedPersonToUser(currentUser);
    currentPerson.setOrcid(null);
    this.personService.update(currentPerson.getResId(), currentPerson);
  }
}
