/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - StoredSearchAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.model.search.StoredSearch;

class StoredSearchAsUserIT extends StoredSearchIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  void createAndDeleteTest() {
    StoredSearch newSearch = this.testCreate();

    this.storedSearchClientService.delete(newSearch.getResId());

    final String resId = newSearch.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.storedSearchClientService.findOne(resId));
  }

  @Test
  void listMySearchesTest() {
    List<StoredSearch> mySearches = this.storedSearchClientService.listMySearches(Map.of());
    assertNotNull(mySearches);
    assertTrue(mySearches.isEmpty());

    StoredSearch search1 = this.getSearch("search1");
    StoredSearch search2 = this.getSearch("search2");
    this.storedSearchClientService.create(search1);
    this.storedSearchClientService.create(search2);

    // create StoredSearch for another user that should not appear in user list
    this.restClientTool.sudoAdmin();
    StoredSearch search3 = this.getSearch("search3");
    this.storedSearchClientService.create(search3);
    this.restClientTool.exitSudo();

    mySearches = this.storedSearchClientService.listMySearches(Map.of());
    assertNotNull(mySearches);
    assertEquals(2, mySearches.size());
  }

  @Test
  void updateTest() {
    this.testUpdate();
  }

  @Test
  void updateSomeoneElseTest() {
    this.restClientTool.sudoAdmin();
    StoredSearch newSearch = this.testCreate();
    this.restClientTool.exitSudo();

    newSearch.setName("name updated");
    final String resId = newSearch.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.storedSearchClientService.update(resId, newSearch));
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void nameConstraintTest() {
    String existingName = this.testNameConstraint();

    // creating a StoredSearch with the same name for someone else is possible
    this.restClientTool.sudoAdmin();
    StoredSearch search = this.getSearch("search");
    search.setName(existingName);
    StoredSearch someoneElseSearch = assertDoesNotThrow(() -> this.storedSearchClientService.create(search));
    assertEquals(existingName, someoneElseSearch.getName());
    this.restClientTool.exitSudo();
  }

  @Test
  void listAllSearchesTest() {
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.storedSearchClientService.findAll());
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void getSomeoneElseSearch() {
    this.restClientTool.sudoAdmin();
    StoredSearch search = this.getSearch("search");
    this.storedSearchClientService.create(search);
    this.restClientTool.exitSudo();

    final String resId = search.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.storedSearchClientService.findOne(resId));
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void deleteSomeoneElseSearch() {
    this.restClientTool.sudoAdmin();
    StoredSearch search = this.getSearch("search");
    this.storedSearchClientService.create(search);
    final String resId = search.getResId();
    StoredSearch fetchedSearch1 = this.storedSearchClientService.findOne(resId);
    assertNotNull(fetchedSearch1);
    this.restClientTool.exitSudo();

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.storedSearchClientService.delete(resId));
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    this.restClientTool.sudoAdmin();
    StoredSearch fetchedSearch = this.storedSearchClientService.findOne(resId);
    assertNotNull(fetchedSearch);
    this.restClientTool.exitSudo();
  }

  @Test
  void deleteListSomeoneElseSearches() {
    this.restClientTool.sudoAdmin();
    StoredSearch search1 = this.getSearch("search1");
    this.storedSearchClientService.create(search1);
    StoredSearch search2 = this.getSearch("search2");
    this.storedSearchClientService.create(search2);
    String resId1 = search1.getResId();
    String resId2 = search2.getResId();
    StoredSearch fetchedSearch1 = this.storedSearchClientService.findOne(resId1);
    assertNotNull(fetchedSearch1);
    StoredSearch fetchedSearch2 = this.storedSearchClientService.findOne(resId2);
    assertNotNull(fetchedSearch2);
    this.restClientTool.exitSudo();

    final String[] resIds = { resId1, resId2 };
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.storedSearchClientService.deleteList(resIds));
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    this.restClientTool.sudoAdmin();
    fetchedSearch1 = this.storedSearchClientService.findOne(resId1);
    assertNotNull(fetchedSearch1);
    fetchedSearch2 = this.storedSearchClientService.findOne(resId2);
    assertNotNull(fetchedSearch2);
    this.restClientTool.exitSudo();
  }
}
