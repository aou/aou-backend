/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationSubtypeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileTypeDTO;
import ch.unige.aou.model.publication.PublicationType;

class PublicationSubtypeIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Override
  protected void createFixtures() {
    this.createPublicationTypeAndSubtypeFixtures(false);
  }

  @Test
  void listTest() {
    PublicationType type = this.publicationTypeService.findByName(this.PUBLICATION_TYPE_TEST_NAME);
    List<PublicationSubtype> publicationSubtypes = this.publicationSubtypeCompositeService.findAll(type.getResId());

    assertEquals(3, publicationSubtypes.size());
    for (PublicationSubtype subtype : publicationSubtypes) {
      assertEquals(type.getResId(), subtype.getPublicationType().getResId());
      assertTrue(subtype.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL));
    }
  }

  @Test
  void findOneTest() {
    PublicationType type = this.publicationTypeService.findByName(this.PUBLICATION_TYPE_TEST_NAME);
    List<PublicationSubtype> publicationSubtypes = this.publicationSubtypeCompositeService.findAll(type.getResId());

    assertEquals(3, publicationSubtypes.size());

    PublicationSubtype subtype = publicationSubtypes.get(0);
    PublicationSubtype foundSubtype = this.publicationSubtypeCompositeService.findOne(type.getResId(), subtype.getResId());

    assertNotNull(foundSubtype.getPublicationType());
    assertEquals(type.getResId(), foundSubtype.getPublicationType().getResId());
    assertEquals(subtype.getName(), foundSubtype.getName());
    assertEquals(subtype.getCode(), foundSubtype.getCode());
    assertEquals(subtype.getSortValue(), foundSubtype.getSortValue());
  }

  @Test
  void createTest() {
    PublicationType type = this.publicationTypeService.findByName(this.PUBLICATION_TYPE_TEST_NAME);

    PublicationSubtype subtype = new PublicationSubtype();
    subtype.setPublicationType(type);
    subtype.setName(AouConstants.TEMPORARY_TEST_DATA_LABEL + "lorem ipsum");
    subtype.setCode("TEST");
    subtype.setSortValue(100);
    PublicationSubtype createdSubtype = this.publicationSubtypeCompositeService.create(type.getResId(), subtype);

    assertNotNull(createdSubtype);
    assertEquals(type.getResId(), createdSubtype.getPublicationType().getResId());
    assertEquals(subtype.getName(), createdSubtype.getName());
    assertEquals(subtype.getCode(), createdSubtype.getCode());
    assertEquals(subtype.getSortValue(), createdSubtype.getSortValue());
  }

  @Test
  void createUniqueName() {

    String subtypeName = AouConstants.TEMPORARY_TEST_DATA_LABEL + "unique name";

    PublicationType type = this.publicationTypeService.findByName(this.PUBLICATION_TYPE_TEST_NAME);

    PublicationSubtype subtype = new PublicationSubtype();
    subtype.setPublicationType(type);
    subtype.setName(subtypeName);
    subtype.setCode("TEST1");
    subtype.setSortValue(100);
    PublicationSubtype createdSubtype1 = this.publicationSubtypeCompositeService.create(type.getResId(), subtype);

    assertNotNull(createdSubtype1);
    assertEquals(type.getResId(), createdSubtype1.getPublicationType().getResId());
    assertEquals(subtype.getName(), createdSubtype1.getName());
    assertEquals(subtype.getCode(), createdSubtype1.getCode());
    assertEquals(subtype.getSortValue(), createdSubtype1.getSortValue());

    PublicationSubtype subtype2 = new PublicationSubtype();
    subtype2.setPublicationType(type);
    subtype2.setName(subtypeName);
    subtype2.setCode("TEST2");
    subtype.setSortValue(200);

    String typeId = type.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationSubtypeCompositeService.create(typeId, subtype2);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Test
  void createUniqueCode() {

    String subtypeCode = "UNIQUE";

    PublicationType type = this.publicationTypeService.findByName(this.PUBLICATION_TYPE_TEST_NAME);

    PublicationSubtype subtype = new PublicationSubtype();
    subtype.setPublicationType(type);
    subtype.setName(AouConstants.TEMPORARY_TEST_DATA_LABEL + "unique code 1");
    subtype.setCode(subtypeCode);
    subtype.setSortValue(100);
    PublicationSubtype createdSubtype1 = this.publicationSubtypeCompositeService.create(type.getResId(), subtype);

    assertNotNull(createdSubtype1);
    assertEquals(type.getResId(), createdSubtype1.getPublicationType().getResId());
    assertEquals(subtype.getName(), createdSubtype1.getName());
    assertEquals(subtype.getCode(), createdSubtype1.getCode());
    assertEquals(subtype.getSortValue(), createdSubtype1.getSortValue());

    PublicationSubtype subtype2 = new PublicationSubtype();
    subtype2.setPublicationType(type);
    subtype2.setName(AouConstants.TEMPORARY_TEST_DATA_LABEL + "unique code 2");
    subtype2.setCode(subtypeCode);
    subtype.setSortValue(200);

    String typeId = type.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationSubtypeCompositeService.create(typeId, subtype2);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Test
  void createIncomplete() {
    PublicationType type = this.publicationTypeService.findByName(this.PUBLICATION_TYPE_TEST_NAME);

    /**
     * missing name
     */
    PublicationSubtype subtype = new PublicationSubtype();
    subtype.setPublicationType(type);
    subtype.setCode("TEST3");
    subtype.setSortValue(100);
    String typeId = type.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationSubtypeCompositeService.create(typeId, subtype);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());

    /**
     * missing code
     */
    PublicationSubtype subtype2 = new PublicationSubtype();
    subtype2.setPublicationType(type);
    subtype2.setName(AouConstants.TEMPORARY_TEST_DATA_LABEL + "un nom mais pas de code");
    subtype2.setSortValue(100);
    String subtype2Id = subtype2.getResId();
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationSubtypeCompositeService.create(subtype2Id, subtype2);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Test
  void listDocumentFileTypes() {
    List<PublicationSubtypeDocumentFileTypeDTO> fileTypes = this.publicationSubtypeService
            .findDocumentFileTypes(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID);

    assertEquals(14, fileTypes.size());
    List<String> fileTypeNames = fileTypes.stream().map(fileTypeDTO -> fileTypeDTO.getValue()).collect(Collectors.toList());
    assertTrue(fileTypeNames.contains("Article (Published version)"));
    assertTrue(fileTypeNames.contains("Article (Accepted version)"));
    assertTrue(fileTypeNames.contains("Article (Submitted version)"));
    assertTrue(fileTypeNames.contains("Extract"));
    assertTrue(fileTypeNames.contains("Appendix"));
    assertTrue(fileTypeNames.contains("Erratum"));
    assertFalse(fileTypeNames.contains("Book (Published version)"));
    assertFalse(fileTypeNames.contains("Book (Accepted version)"));
    assertFalse(fileTypeNames.contains("Book (Submitted version)"));
    assertFalse(fileTypeNames.contains("Book"));
  }

  @Test
  void listFilterByLevelDocumentFileTypes() {
    List<PublicationSubtypeDocumentFileTypeDTO> fileTypes = this.publicationSubtypeService
            .findDocumentFileTypes(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID, DocumentFileType.FileTypeLevel.PRINCIPAL);

    assertEquals(3, fileTypes.size());
    assertEquals("Article (Published version)", fileTypes.get(0).getValue());
    assertEquals("Article (Accepted version)", fileTypes.get(1).getValue());
    assertEquals("Article (Submitted version)", fileTypes.get(2).getValue());

    fileTypes = this.publicationSubtypeService
            .findDocumentFileTypes(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID, DocumentFileType.FileTypeLevel.SECONDARY);

    assertEquals(6, fileTypes.size());
    assertEquals("Appendix", fileTypes.get(0).getValue());
    assertEquals("Supplemental data", fileTypes.get(1).getValue());
    assertEquals("Recording", fileTypes.get(2).getValue());
    assertEquals("Extract", fileTypes.get(3).getValue());
    assertEquals("Summary", fileTypes.get(4).getValue());
    assertEquals("Translation", fileTypes.get(5).getValue());

    fileTypes = this.publicationSubtypeService
            .findDocumentFileTypes(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID, DocumentFileType.FileTypeLevel.CORRECTIVE);

    assertEquals(4, fileTypes.size());
    assertEquals("Addendum", fileTypes.get(0).getValue());
    assertEquals("Corrigendum", fileTypes.get(1).getValue());
    assertEquals("Erratum", fileTypes.get(2).getValue());
    assertEquals("Retraction", fileTypes.get(3).getValue());
  }

  @Override
  protected void deleteFixtures() {
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearPublicationFixtures();
  }
}
