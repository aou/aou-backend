/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - UserAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.security.User;
import ch.unige.aou.test.AouTestConstants;

class UserAsRootIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void verifyUserPerson() {
    Map<String, String> properties = new HashMap<>();
    properties.put(AouConstants.DB_FIRST_NAME, AouConstants.PERMANENT_TEST_DATA_LABEL);
    final List<User> userList = this.userService.searchByProperties(properties);
    final List<String> roles = new ArrayList<>(Arrays.asList("ADMIN", "USER", "ROOT"));
    for (String role : roles) {
      List<User> roleUsers = userList.stream()
              .filter(user -> user.getFirstName().contains(AouConstants.PERMANENT_TEST_DATA_LABEL) &&
                      user.getApplicationRole().getResId().contains(role))
              .collect(Collectors.toList());
      for (User user : roleUsers) {
        assertNotNull(user.getPerson(), "User " + user + " has no linked person");
      }
      int expectedNumber;
      if (role.equals("USER")) {
        expectedNumber = 2;
      } else {
        expectedNumber = 1;
      }
      assertEquals(expectedNumber, roleUsers.size(), "There must be exactly " + expectedNumber + " " + role + " test user");
      assertNotNull(roleUsers.get(0).getPerson(), "The role " + role + " must have a person connected to its user");
      assertEquals(role, roleUsers.get(0).getApplicationRole().getResId(), "User for " + role + " has not a " + role + " role");
    }
  }

  @Test
  void testCreate() {

    final String externalUid = AouTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = AouTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = AouTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = AouTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    /*
     * Create a new User
     */
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("ungie.ch");

    this.userService.create(newUser);

    /*
     * Fetch it from server and test it has been created
     */
    final User fetchedUser = this.userService.findOne(newUser.getResId());
    assertNotNull(fetchedUser.getResId());
    assertEquals(firstname, fetchedUser.getFirstName());
  }

  @Test
  void testDelete() {

    final String externalUid = AouTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = AouTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = AouTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = AouTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    /*
     * Create a new User
     */
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("unige.ch");

    this.userService.create(newUser);

    /*
     * Checks saving succeeded
     */
    final User fetchedUser = this.userService.findOne(newUser.getResId());
    assertEquals(fetchedUser.getFirstName(), firstname);

    /*
     * Delete it
     */
    this.userService.delete(fetchedUser.getResId());

    /*
     * Fetching again returns nothing
     */
    final String resId = newUser.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.userService.findOne(resId));
  }

  @Test
  void testUnicity() {

    final String externalUid = AouTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = AouTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = AouTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = AouTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    // Creating a new User
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("unige.ch");

    this.userService.create(newUser);

    final String firstname2 = AouTestConstants.getRandomNameWithTemporaryLabel("Billy");
    final String lastname2 = AouTestConstants.getRandomNameWithTemporaryLabel("Bob");
    final String email2 = AouTestConstants.getRandomNameWithTemporaryLabel("billybob@example.com");

    // Creating second user with the same externalUID as the first's
    final User newUser2 = new User();
    newUser2.setExternalUid(externalUid);
    newUser2.setFirstName(firstname2);
    newUser2.setLastName(lastname2);
    newUser2.setEmail(email2);
    newUser2.setHomeOrganization("unige.ch");

    // Testing the unicity of the UIDs
    try {
      this.userService.create(newUser2);
      fail("A BAD_REQUEST Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "Unicity constraint not satisfied");
    }
  }

  @Test
  void testUpdate() {

    final String externalUid = AouTestConstants.getRandomNameWithTemporaryLabel("12345@example.com");
    final String firstname = AouTestConstants.getRandomNameWithTemporaryLabel("John");
    final String lastname = AouTestConstants.getRandomNameWithTemporaryLabel("Smith");
    final String email = AouTestConstants.getRandomNameWithTemporaryLabel("john@example.com");

    /*
     * Create a new User
     */
    final User newUser = new User();

    newUser.setExternalUid(externalUid);
    newUser.setFirstName(firstname);
    newUser.setLastName(lastname);
    newUser.setEmail(email);
    newUser.setHomeOrganization("unige.ch");

    this.userService.create(newUser);

    /*
     * Checks saving succeeded
     */
    final User fetchedUser = this.userService.findOne(newUser.getResId());
    assertEquals(fetchedUser.getFirstName(), firstname);

    /*
     * Does the update
     */
    final String firstname2 = AouTestConstants.getRandomNameWithTemporaryLabel("Albert");
    final String lastname2 = AouTestConstants.getRandomNameWithTemporaryLabel("Levert");

    fetchedUser.setFirstName(firstname2);
    fetchedUser.setLastName(lastname2);
    this.userService.update(fetchedUser.getResId(), fetchedUser);

    /*
     * Checks update succeeded
     */
    final User refetchedUser = this.userService.findOne(fetchedUser.getResId());
    assertEquals(refetchedUser.getFirstName(), firstname2);
    assertEquals(refetchedUser.getLastName(), lastname2);
  }

  /*************************************************/

  @Override
  protected void deleteFixtures() {
    this.clearUserFixtures();
  }
}
