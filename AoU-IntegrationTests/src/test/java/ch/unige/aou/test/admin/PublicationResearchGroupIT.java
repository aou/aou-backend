/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationResearchGroupIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.ResearchGroup;

@Disabled("Link between Publication and ResearchGroup is now based on metadata content")
class PublicationResearchGroupIT extends AbstractAdminIT {

  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.createPublicationFixture();
    this.restClientTool.exitSudo();
  }

  @Test
  void addResearchGroupToPublicationTest() {
    final Publication publication = this.getTestPublication();
    final int sizeBeforeAdding = this.publicationService.getResearchGroups(publication.getResId()).size();

    //create research group
    this.restClientTool.sudoRoot();
    ResearchGroup researchGroup = this.createResearchGroupFixture("research group 1", "9999");
    this.restClientTool.exitSudo();
    // Add the ResearchGroup to the publication
    this.publicationService.addResearchGroup(publication.getResId(), researchGroup.getResId());
    final List<ResearchGroup> researchGroupList = this.publicationService.getResearchGroups(publication.getResId());

    assertEquals(researchGroupList.size(), sizeBeforeAdding + 1);

    for (final ResearchGroup rs : researchGroupList) {
      assertEquals(rs.getName(), researchGroup.getName());
    }
  }

  @Test
  void removeResearchGroupToPublicationTest() {
    final Publication publication = this.getTestPublication();
    final int sizeBeforeAdding = this.publicationService.getResearchGroups(publication.getResId()).size();

    this.restClientTool.sudoRoot();
    ResearchGroup researchGroup1 = this.createResearchGroupFixture("research group 1", "1234");
    ResearchGroup researchGroup2 = this.createResearchGroupFixture("research group 2", "4567");
    ResearchGroup researchGroup3 = this.createResearchGroupFixture("research group 3", "6789");
    this.restClientTool.exitSudo();

    // Add the ResearchGroup to the publication
    this.publicationService.addResearchGroup(publication.getResId(), researchGroup1.getResId());
    this.publicationService.addResearchGroup(publication.getResId(), researchGroup2.getResId());
    this.publicationService.addResearchGroup(publication.getResId(), researchGroup3.getResId());

    final int sizeAfterAdding = this.publicationService.getResearchGroups(publication.getResId()).size();

    assertEquals(sizeAfterAdding, sizeBeforeAdding + 3);

    this.publicationService.removeResearchGroup(publication.getResId(), researchGroup2.getResId());
    final int sizeAfterRemoving = this.publicationService.getResearchGroups(publication.getResId()).size();

    assertEquals(sizeAfterRemoving, sizeBeforeAdding + 2);
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearPublicationFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearStructureFixtures();
    this.clearResearchGroupFixtures();
    this.restClientTool.exitSudo();
  }
}
