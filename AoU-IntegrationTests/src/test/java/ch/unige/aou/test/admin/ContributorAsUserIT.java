/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ContributorAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorOtherName;

class ContributorAsUserIT extends AbstractContributorIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  void testCreate() {
    Contributor contributor = this.getContributor();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.contributorService.create(contributor));
  }

  @Test
  void testAddOtherName() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    this.restClientTool.exitSudo();

    ContributorOtherName otherName = new ContributorOtherName();
    otherName.setFirstName(this.testOtherFirstName);
    otherName.setLastName(this.testOtherLastName);
    String contributorId = savedContributor.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.contributorOtherNameService.create(contributorId, otherName));
  }

  @Test
  void testRemoveOtherName() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    ContributorOtherName savedOtherName = this.testAddOtherNameOnContributor(savedContributor, this.testOtherFirstName, this.testOtherLastName);
    this.restClientTool.exitSudo();

    String contributorId = savedContributor.getResId();
    String otherNameId = savedOtherName.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.contributorOtherNameService.delete(contributorId, otherNameId));
  }

  @Override
  @Test
  void testSearchUnigeContributorWithFirstnameAndLastname() {
    super.testSearchUnigeContributorWithFirstnameAndLastname();
  }

  @Override
  @Test
  void testSearchUnigeContributorWithLooseName() {
    super.testSearchUnigeContributorWithLooseName();
  }
}
