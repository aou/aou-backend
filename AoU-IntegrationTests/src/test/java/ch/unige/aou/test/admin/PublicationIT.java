/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;

abstract class PublicationIT extends AbstractDocumentFileIT {

  protected final static String DOI_IMPORT_TEST = "10.1126/science.1175088";
  protected final static String PMID_IMPORT_TEST = "23193287";

  protected List<String> importedPublicationIds = new ArrayList<>();

  protected Person profilePerson;

  @Override
  protected void createFixtures() {
    this.importedPublicationIds.clear();
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.fillCurrentUserPerson();
    this.restClientTool.exitSudo();
  }

  protected abstract String getCurrentUserId();

  private void fillCurrentUserPerson() {
    User currentUser = this.userService.findOne(this.getCurrentUserId());
    this.profilePerson = currentUser.getPerson();
  }

  protected PublicationType getPublicationType() {
    return this.publicationTypeService.findByName(AouConstants.DEPOSIT_TYPE_ARTICLE_NAME);
  }

  protected PublicationSubtype getPublicationSubtype() {
    return this.publicationSubtypeService.findByName(AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
  }

  protected Publication getTemporaryPublicationInstance(String formData) {
    Publication publication = new Publication();
    publication.setFormData(formData);
    return publication;
  }

  protected Publication createNewTemporaryPublication(String formData) {
    Publication publication = this.getTemporaryPublicationInstance(formData);
    return this.publicationService.create(publication);
  }

  protected Publication createNewTemporaryPublication(String formData, Publication.PublicationStatus status) {
    Publication publication = this.getTemporaryPublicationInstance(formData);
    publication.setStatus(status);
    return this.publicationService.create(publication);
  }

  protected Publication testPublicationCreation(String titleParam) {
    return this.testPublicationCreation(titleParam, null);
  }

  protected Publication testPublicationCreation(String titleParam, String expectedTitle) {
    if (StringTool.isNullOrEmpty(expectedTitle)) {
      expectedTitle = titleParam;
    }
    expectedTitle = this.getTemporaryTestLabel(expectedTitle);

    String title = this.getTemporaryTestLabel(titleParam);
    String formData = this.getTestFormData(title);
    PublicationType type = this.getPublicationType();
    PublicationSubtype subtype = this.getPublicationSubtype();
    String personId = this.profilePerson.getResId();

    // Subtype is taken from formData on backend
    formData = formData.replace("\"" + this.PUBLICATION_TYPE_ARTICLE_ID + "\"", "\"" + type.getResId() + "\"");
    formData = formData.replace("\"" + this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID + "\"", "\"" + subtype.getResId() + "\"");
    formData = formData.replace("\"A1-LETTRE\"", "null");

    Publication createdPublication = this.createNewTemporaryPublication(formData);
    String expectedFormData = formData.replace(title, expectedTitle);
    assertNotNull(createdPublication);
    assertEquals(subtype.getResId(), createdPublication.getSubtype().getResId());
    assertNull(createdPublication.getSubSubtype());
    assertEquals(expectedTitle, createdPublication.getTitle());
    this.assertEqualsFormData(expectedFormData, createdPublication.getFormData());
    assertEquals(personId, createdPublication.getCreator().getResId());

    Publication foundPublication = this.publicationService.findOne(createdPublication.getResId());
    assertNotNull(foundPublication);
    assertEquals(subtype.getResId(), foundPublication.getSubtype().getResId());
    assertNull(foundPublication.getSubSubtype());
    assertEquals(expectedTitle, foundPublication.getTitle());
    this.assertEqualsFormData(expectedFormData, foundPublication.getFormData());
    assertEquals(personId, createdPublication.getCreator().getResId());
    return createdPublication;
  }

  protected Publication createFromDOI(String doi) {
    Publication publication = this.publicationService.createFromDOI(doi);
    this.importedPublicationIds.add(publication.getResId());
    return publication;
  }

  protected Publication createFromDOI(String doi, String creatorId) {
    Publication publication = this.publicationService.createFromDOI(doi, creatorId);
    this.importedPublicationIds.add(publication.getResId());
    return publication;
  }

  protected Publication createFromPMID(String pmid) {
    Publication publication = this.publicationService.createFromPMID(pmid);
    this.importedPublicationIds.add(publication.getResId());
    return publication;
  }

  protected void assertEqualsFormData(String expectedJson, String actualJson) {

    if (StringTool.isNullOrEmpty(expectedJson) || StringTool.isNullOrEmpty(actualJson)) {
      assertEquals(expectedJson, actualJson);
    } else {
      JSONObject expectedJsonObject = new JSONObject(expectedJson);
      JSONObject actualJsonObject = new JSONObject(actualJson);

      assertNotNull(expectedJsonObject.getJSONObject("type"));
      assertNotNull(actualJsonObject.getJSONObject("type"));
      assertEquals(
              expectedJsonObject.getJSONObject("type").getString("type"),
              actualJsonObject.getJSONObject("type").getString("type"));
      assertEquals(
              expectedJsonObject.getJSONObject("type").getString("subtype"),
              actualJsonObject.getJSONObject("type").getString("subtype"));
      assertEquals(
              expectedJsonObject.getJSONObject("type").get("subsubtype"),
              actualJsonObject.getJSONObject("type").get("subsubtype"));
      assertEquals(
              expectedJsonObject.getJSONObject("type").getJSONObject("title").getString("text"),
              actualJsonObject.getJSONObject("type").getJSONObject("title").getString("text"));
      assertEquals(
              expectedJsonObject.getJSONObject("type").getJSONObject("title").getString("lang"),
              actualJsonObject.getJSONObject("type").getJSONObject("title").getString("lang"));

      assertFalse(expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").isEmpty());
      assertFalse(actualJsonObject.getJSONObject("contributors").getJSONArray("contributors").isEmpty());
      assertEquals(
              expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("cnIndividu"),
              actualJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("cnIndividu"));
      assertEquals(
              expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("email"),
              actualJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("email"));
      assertEquals(
              expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("firstname"),
              actualJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("firstname"));
      assertEquals(
              expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("lastname"),
              actualJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("lastname"));
      assertEquals(
              expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("role"),
              actualJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0).getJSONObject("contributor")
                      .getString("role"));

      assertNotNull(expectedJsonObject.getJSONObject("description"));
      assertNotNull(actualJsonObject.getJSONObject("description"));
      assertNotNull(expectedJsonObject.getJSONObject("description").getJSONObject("container"));
      assertNotNull(actualJsonObject.getJSONObject("description").getJSONObject("container"));
      assertEquals(
              expectedJsonObject.getJSONObject("description").getJSONObject("container").getString("issue"),
              actualJsonObject.getJSONObject("description").getJSONObject("container").getString("issue"));
      assertEquals(
              expectedJsonObject.getJSONObject("description").getJSONObject("container").getString("title"),
              actualJsonObject.getJSONObject("description").getJSONObject("container").getString("title"));

      assertEquals(
              expectedJsonObject.getJSONObject("description").getJSONArray("collections").getJSONObject(0).getString("name"),
              actualJsonObject.getJSONObject("description").getJSONArray("collections").getJSONObject(0).getString("name")
      );
      assertTrue(actualJsonObject.getJSONObject("description").getJSONArray("collections").getJSONObject(0).isNull("number"));

      assertEquals(
              expectedJsonObject.getJSONObject("description").getJSONArray("collections").getJSONObject(1).getString("name"),
              actualJsonObject.getJSONObject("description").getJSONArray("collections").getJSONObject(1).getString("name")
      );
      assertEquals("888", actualJsonObject.getJSONObject("description").getJSONArray("collections").getJSONObject(1).getString("number"));

      if (actualJsonObject.getJSONObject("description").getJSONObject("container").has("identifiers")) {
        // case of metadata V1
        assertEquals(
                expectedJsonObject.getJSONObject("description").getJSONObject("container").getJSONObject("identifiers").getString("issn"),
                actualJsonObject.getJSONObject("description").getJSONObject("container").getJSONObject("identifiers").getString("issn"));
      }

    }
  }

  protected void clearImportedPublicationFixtures() {
    for (String publicationId : this.importedPublicationIds) {
      this.publicationService.delete(publicationId);
    }
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.deleteGrandChildrenStructures();
    this.clearStructureFixtures();
    this.clearPublicationFixtures();
    this.clearImportedPublicationFixtures();
    this.clearResearchGroupFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearStructureFixtures();
    this.clearContributorFixtures();
    this.restClientTool.exitSudo();
  }
}
