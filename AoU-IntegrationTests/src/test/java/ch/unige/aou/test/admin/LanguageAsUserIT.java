/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - LanguageAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.settings.LabelDTO;
import ch.unige.aou.model.settings.LanguageDTO;

class LanguageAsUserIT extends AbstractAdminIT {

  @Test
  void findAllTest() {
    List<LanguageDTO> languages = this.languageService.listAll();
    assertNotNull(languages);
    assertFalse(languages.isEmpty());

    /**
     * French
     */
    LanguageDTO language = languages.stream().filter(lang -> lang.getCode().equals(AouConstants.LANG_CODE_FRENCH))
            .collect(Collectors.toList()).get(0);
    assertNotNull(language);

    String frenchLanguageName = language.getLabels().stream()
            .filter(languageLabel -> languageLabel.getLanguageCode().equals(AouConstants.LANG_CODE_FRENCH)).map(LabelDTO::getText)
            .collect(Collectors.toList()).get(0);
    assertNotNull(frenchLanguageName);
    assertEquals("Français", frenchLanguageName);

    String englishLanguageName = language.getLabels().stream()
            .filter(languageLabel -> languageLabel.getLanguageCode().equals(AouConstants.LANG_CODE_ENGLISH)).map(LabelDTO::getText)
            .collect(Collectors.toList()).get(0);
    assertNotNull(englishLanguageName);
    assertEquals("French", englishLanguageName);

    /**
     * English
     */
    language = languages.stream().filter(lang -> lang.getCode().equals(AouConstants.LANG_CODE_ENGLISH))
            .collect(Collectors.toList()).get(0);
    assertNotNull(language);

    frenchLanguageName = language.getLabels().stream()
            .filter(languageLabel -> languageLabel.getLanguageCode().equals(AouConstants.LANG_CODE_FRENCH)).map(LabelDTO::getText)
            .collect(Collectors.toList()).get(0);
    assertNotNull(frenchLanguageName);
    assertEquals("Anglais", frenchLanguageName);

    englishLanguageName = language.getLabels().stream()
            .filter(languageLabel -> languageLabel.getLanguageCode().equals(AouConstants.LANG_CODE_ENGLISH)).map(LabelDTO::getText)
            .collect(Collectors.toList()).get(0);
    assertNotNull(englishLanguageName);
    assertEquals("English", englishLanguageName);

    /**
     * German
     */
    language = languages.stream().filter(lang -> lang.getCode().equals(AouConstants.LANG_CODE_GERMAN))
            .collect(Collectors.toList()).get(0);
    assertNotNull(language);

    frenchLanguageName = language.getLabels().stream()
            .filter(languageLabel -> languageLabel.getLanguageCode().equals(AouConstants.LANG_CODE_FRENCH)).map(LabelDTO::getText)
            .collect(Collectors.toList()).get(0);
    assertNotNull(frenchLanguageName);
    assertEquals("Allemand", frenchLanguageName);

    englishLanguageName = language.getLabels().stream()
            .filter(languageLabel -> languageLabel.getLanguageCode().equals(AouConstants.LANG_CODE_ENGLISH))
            .map(LabelDTO::getText).collect(Collectors.toList()).get(0);
    assertNotNull(englishLanguageName);
    assertEquals("German", englishLanguageName);
  }

}
