/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - StoredSearchIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.search.StoredSearch;
import ch.unige.aou.model.search.StoredSearchCriteria;

public abstract class StoredSearchIT extends AbstractAdminIT {

  protected static final String TITLE_FIELD = "title";
  protected static final String TITLE_FIELD_VALUE = "My title";

  protected static final String CONTRIBUTOR_FIELD = "contributor";
  protected static final String CONTRIBUTOR_FIELD_VALUE = "Doe, John";

  protected StoredSearch testCreate() {
    StoredSearch search = this.getSearch("search");

    StoredSearch savedSearch = this.storedSearchClientService.create(search);
    StoredSearch fetchedSearch = this.storedSearchClientService.findOne(savedSearch.getResId());

    assertNotNull(fetchedSearch);
    assertEquals(search.getName(), fetchedSearch.getName());
    assertFalse(fetchedSearch.getWithRestrictedAccessMasters());
    assertFalse(fetchedSearch.getCriteria().isEmpty());
    assertFalse(fetchedSearch.getUsedForBibliography());
    assertEquals(2, fetchedSearch.getCriteria().size());

    Optional<StoredSearchCriteria> criteriaTitle = fetchedSearch.getCriteria().stream().filter(c -> c.getField().equals(TITLE_FIELD))
            .findFirst();
    assertTrue(criteriaTitle.isPresent());
    assertEquals(TITLE_FIELD, criteriaTitle.get().getField());
    assertEquals(TITLE_FIELD_VALUE, criteriaTitle.get().getValue());

    Optional<StoredSearchCriteria> criteriaContributor = fetchedSearch.getCriteria().stream().filter(c -> c.getField().equals(CONTRIBUTOR_FIELD))
            .findFirst();
    assertTrue(criteriaContributor.isPresent());
    assertEquals(CONTRIBUTOR_FIELD, criteriaContributor.get().getField());
    assertEquals(CONTRIBUTOR_FIELD_VALUE, criteriaContributor.get().getValue());

    return fetchedSearch;
  }

  protected StoredSearch testUpdate() {
    StoredSearch newSearch = this.testCreate();

    String newSearchName = newSearch.getName() + " (updated)";
    String newTitleValue = TITLE_FIELD_VALUE + " updated";
    String newContributorValue = CONTRIBUTOR_FIELD_VALUE + " updated";

    newSearch.setName(newSearchName);
    newSearch.setWithRestrictedAccessMasters(true);
    newSearch.setUsedForBibliography(true);

    List<StoredSearchCriteria> criteria = newSearch.getCriteria();
    Optional<StoredSearchCriteria> criteriaTitle = criteria.stream().filter(c -> c.getField().equals(TITLE_FIELD)).findFirst();
    criteriaTitle.get().setValue(newTitleValue);
    Optional<StoredSearchCriteria> criteriaContributor = criteria.stream().filter(c -> c.getField().equals(CONTRIBUTOR_FIELD)).findFirst();
    criteriaContributor.get().setValue(newContributorValue);

    this.storedSearchClientService.update(newSearch.getResId(), newSearch);
    StoredSearch fetchedSearch = this.storedSearchClientService.findOne(newSearch.getResId());
    assertNotNull(fetchedSearch);
    assertEquals(newSearchName, fetchedSearch.getName());
    assertTrue(fetchedSearch.getWithRestrictedAccessMasters());
    assertTrue(fetchedSearch.getUsedForBibliography());

    Optional<StoredSearchCriteria> updatedCriteriaTitle = fetchedSearch.getCriteria().stream().filter(c -> c.getField().equals(TITLE_FIELD))
            .findFirst();
    assertTrue(updatedCriteriaTitle.isPresent());
    assertEquals(newTitleValue, updatedCriteriaTitle.get().getValue());

    Optional<StoredSearchCriteria> updatedCriteriaContributor = fetchedSearch.getCriteria().stream()
            .filter(c -> c.getField().equals(CONTRIBUTOR_FIELD))
            .findFirst();
    assertTrue(updatedCriteriaContributor.isPresent());
    assertEquals(newContributorValue, updatedCriteriaContributor.get().getValue());

    return fetchedSearch;
  }

  protected String testNameConstraint() {
    StoredSearch newSearch = this.testCreate();

    StoredSearch sameNameSearch = this.getSearch("search");
    sameNameSearch.setName(newSearch.getName());
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.storedSearchClientService.create(sameNameSearch));
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("name", "You already have a search whose name is '" + sameNameSearch.getName() + "'",
            validationErrors));

    return sameNameSearch.getName();
  }

  protected StoredSearch getSearch(String name) {
    String searchName = this.getTemporaryTestLabel(name);
    StoredSearch search = new StoredSearch();
    search.setName(searchName);
    StoredSearchCriteria searchCriteria1 = new StoredSearchCriteria();
    searchCriteria1.setField(TITLE_FIELD);
    searchCriteria1.setValue(TITLE_FIELD_VALUE);
    search.getCriteria().add(searchCriteria1);
    StoredSearchCriteria searchCriteria2 = new StoredSearchCriteria();
    searchCriteria2.setField(CONTRIBUTOR_FIELD);
    searchCriteria2.setValue(CONTRIBUTOR_FIELD_VALUE);
    search.getCriteria().add(searchCriteria2);
    return search;
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.cleanSearchFixtures();
    this.restClientTool.exitSudo();
  }

  private void cleanSearchFixtures() {
    final Map<String, String> properties = new HashMap<>();
    properties.put("name", AouConstants.TEMPORARY_TEST_DATA_LABEL);
    final List<StoredSearch> tmpSearches = this.storedSearchClientService.searchByProperties(properties);
    for (StoredSearch search : tmpSearches) {
      this.storedSearchClientService.delete(search.getResId());
    }
  }
}
