/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - LicenseIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;

import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.LicenseGroup;

public abstract class LicenseIT extends AbstractAdminIT {

  protected LicenseGroup createTestLicenseGroup(String name) {
    this.restClientTool.sudoAdmin();
    LicenseGroup licenseGroup = new LicenseGroup();
    licenseGroup.setName(this.getTemporaryTestLabel(name));
    licenseGroup.setVisible(true);
    licenseGroup = this.licenseGroupService.create(licenseGroup);
    this.restClientTool.exitSudo();
    return licenseGroup;
  }

  protected License createTestLicense(LicenseGroup licenseGroup, String title, String openLicenseId, Integer sortValue) {
    this.restClientTool.sudoAdmin();
    License license = new License();
    license.setLicenseGroup(licenseGroup);
    license.setTitle(this.getTemporaryTestLabel(title));
    license.setOpenLicenseId(openLicenseId);
    license.setSortValue(sortValue);
    license = this.licenseService.create(license);
    this.restClientTool.exitSudo();
    return license;
  }

  protected void assertsLicense(License expectedLicense, License actualLicense) {
    assertEquals(expectedLicense.getLicenseGroup().getResId(), actualLicense.getLicenseGroup().getResId());
    assertEquals(expectedLicense.getTitle(), actualLicense.getTitle());
    this.assertEqualsWithoutNanoSeconds(expectedLicense.getCreationTime(), actualLicense.getCreationTime());
    assertEquals(expectedLicense.getOpenLicenseId(), actualLicense.getOpenLicenseId());
    assertEquals(expectedLicense.getSortValue(), actualLicense.getSortValue());
    assertEquals(expectedLicense.getUrl(), actualLicense.getUrl());
  }

  protected void assertsLicenseGroup(LicenseGroup expectedLicenseGroup, LicenseGroup actualLicenseGroup) {
    assertEquals(expectedLicenseGroup.getResId(), actualLicenseGroup.getResId());
    assertEquals(expectedLicenseGroup.getName(), actualLicenseGroup.getName());
    assertEquals(expectedLicenseGroup.getSortValue(), actualLicenseGroup.getSortValue());
    assertEquals(expectedLicenseGroup.getVisible(), actualLicenseGroup.getVisible());
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    this.clearLicenceFixtures();
    this.clearLicenceGroupFixtures();
    this.restClientTool.exitSudo();
  }
}
