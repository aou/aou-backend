/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - GlobalBannerIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.OffsetDateTime;

import ch.unige.solidify.model.GlobalBanner;

public abstract class GlobalBannerIT extends AbstractAdminIT {

  protected GlobalBanner createTestGlobalBanner(String name, GlobalBanner.GlobalBannerType type, boolean enabled, OffsetDateTime startDate,
          OffsetDateTime endDate) {
    this.restClientTool.sudoAdmin();
    GlobalBanner globalBanner = new GlobalBanner();
    globalBanner.setName(this.getTemporaryTestLabel(name));
    globalBanner.setType(type);
    globalBanner.setEnabled(enabled);
    globalBanner.setStartDate(startDate);
    globalBanner.setEndDate(endDate);
    globalBanner = this.globalBannerService.create(globalBanner);
    this.restClientTool.exitSudo();
    return globalBanner;
  }

  protected void assertsGlobalBanner(GlobalBanner expectedGlobalBanner, GlobalBanner actualGlobalBanner) {
    assertEquals(expectedGlobalBanner.getName(), actualGlobalBanner.getName());
    this.assertEqualsWithoutNanoSeconds(expectedGlobalBanner.getCreationTime(), actualGlobalBanner.getCreationTime());
    assertEquals(expectedGlobalBanner.getType(), actualGlobalBanner.getType());
    assertEquals(expectedGlobalBanner.getEnabled(), actualGlobalBanner.getEnabled());
    assertEquals(expectedGlobalBanner.getStartDate(), actualGlobalBanner.getStartDate());
    assertEquals(expectedGlobalBanner.getEndDate(), actualGlobalBanner.getEndDate());
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoAdmin();
    this.clearGlobalBannerFixtures();
    this.restClientTool.exitSudo();
  }
}
