/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - LicenseAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.LicenseGroup;

class LicenseAsAdminIT extends LicenseIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    // Create a license
    License license = new License();
    license.setLicenseGroup(licenseGroup);
    license.setTitle(this.getTemporaryTestLabel("GNU Free Documentation License"));
    license.setSortValue(10);
    final String gnuLicense = "GNU" + ThreadLocalRandom.current().nextInt();
    license.setOpenLicenseId(gnuLicense);
    License savedLicense = this.licenseService.create(license);

    // Test the creation
    final License foundLicense = this.licenseService.findOne(license.getResId());
    assertNotNull(foundLicense);
    this.assertsLicense(savedLicense, foundLicense);

    // Test unique openLicenseId
    final License sameLicense = new License();
    license.setLicenseGroup(licenseGroup);
    sameLicense.setTitle(this.getTemporaryTestLabel("GNU Free Documentation License"));
    sameLicense.setOpenLicenseId(gnuLicense);

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.licenseService.create(sameLicense);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode(), "OpenLicenseId is unique");
  }

  @Test
  void creationWithoutLicenseGroupTest() {
    // Create a license
    License license = new License();
    license.setTitle(this.getTemporaryTestLabel("Creative Commons Attribution (CC BY)"));
    license.setSortValue(10);
    license.setOpenLicenseId("CC BY");
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.licenseService.create(license);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("licenseGroup", "The license group is mandatory", validationErrors));
  }

  @Test
  void deleteTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    // Create a new licence
    License license = new License();
    license.setLicenseGroup(licenseGroup);
    license.setTitle(this.getTemporaryTestLabel("License  " + ThreadLocalRandom.current().nextInt()));
    license.setOpenLicenseId("License  " + ThreadLocalRandom.current().nextInt());
    license.setSortValue(10);
    License savedLicense = this.licenseService.create(license);

    // check creation
    final License foundLicense = this.licenseService.findOne(savedLicense.getResId());
    assertNotNull(foundLicense);
    this.assertsLicense(savedLicense, foundLicense);

    // Delete
    final String resId = license.getResId();
    this.licenseService.delete(resId);

    // Test deletion
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.licenseService.findOne(resId));
  }

  @Test
  void findAllTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    // Create a new licence
    final License license = this.createTestLicense(licenseGroup, "License test", "OL-ID", 10);

    // Find all
    final List<License> actualLicenses = this.licenseService.findAll();

    // Test at least
    assertFalse(actualLicenses.isEmpty());
  }

  @Test
  void findByGroupTest() {
    LicenseGroup group1 = this.createTestLicenseGroup("group1");
    LicenseGroup group2 = this.createTestLicenseGroup("group2");
    LicenseGroup group3 = this.createTestLicenseGroup("group3");

    License license1 = this.createTestLicense(group1, "license1", "OL1", 10);
    License license2 = this.createTestLicense(group1, "license2", "OL2", 20);
    License license3 = this.createTestLicense(group1, "license3", "OL3", 30);

    License license4 = this.createTestLicense(group2, "license4", "OL4", 40);
    License license5 = this.createTestLicense(group2, "license5", "OL5", 50);

    Map<String, String> params = new HashMap<>();
    params.put("licenseGroup.resId", group1.getResId());
    List<License> group1Licenses = this.licenseService.searchByProperties(params);
    assertNotNull(group1Licenses);
    assertEquals(3, group1Licenses.size());
    assertTrue(group1Licenses.stream().anyMatch(license -> license.getResId().equals(license1.getResId())));
    assertTrue(group1Licenses.stream().anyMatch(license -> license.getResId().equals(license2.getResId())));
    assertTrue(group1Licenses.stream().anyMatch(license -> license.getResId().equals(license3.getResId())));

    params.put("licenseGroup.resId", group2.getResId());
    List<License> group2Licenses = this.licenseService.searchByProperties(params);
    assertNotNull(group2Licenses);
    assertEquals(2, group2Licenses.size());
    assertTrue(group2Licenses.stream().anyMatch(license -> license.getResId().equals(license4.getResId())));
    assertTrue(group2Licenses.stream().anyMatch(license -> license.getResId().equals(license5.getResId())));

    params.put("licenseGroup.resId", group3.getResId());
    List<License> group3Licenses = this.licenseService.searchByProperties(params);
    assertNotNull(group3Licenses);
    assertEquals(0, group3Licenses.size());
  }

  @Test
  void updateTest() throws Exception {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    // Create a licence
    License license = new License();
    license.setLicenseGroup(licenseGroup);
    license.setTitle(this.getTemporaryTestLabel("A" + ThreadLocalRandom.current().nextInt()));
    license.setOpenLicenseId("A" + ThreadLocalRandom.current().nextInt());
    license.setSortValue(10);
    License savedLicense = this.licenseService.create(license);

    // check creation
    License foundLicense = this.licenseService.findOne(savedLicense.getResId());
    assertNotNull(foundLicense);
    this.assertsLicense(savedLicense, foundLicense);

    // Update the licence
    String title = this.getTemporaryTestLabel("B" + ThreadLocalRandom.current().nextInt());
    final String openLicenseId = "B" + ThreadLocalRandom.current().nextInt();
    final URL url = new URL("https://archive-ouverte.unige.ch/");
    license.setTitle(title);
    license.setOpenLicenseId(openLicenseId);
    license.setUrl(url);
    final License actualLicense = this.licenseService.update(license.getResId(), license);

    // Test the update
    foundLicense = this.licenseService.findOne(savedLicense.getResId());
    assertNotNull(foundLicense);
    assertEquals(title, foundLicense.getTitle());
    assertEquals(openLicenseId, foundLicense.getOpenLicenseId());
    assertEquals(url, foundLicense.getUrl());
  }

}
