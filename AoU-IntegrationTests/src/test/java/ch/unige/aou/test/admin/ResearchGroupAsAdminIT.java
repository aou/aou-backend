/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ResearchGroupAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;

class ResearchGroupAsAdminIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void createTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroup.getResId());
    assertEquals(researchGroup.getName(), researchGroupFound.getName());
    assertEquals(researchGroup.getCode(), researchGroupFound.getCode());
    assertEquals(researchGroup.getAcronym(), researchGroupFound.getAcronym());
    assertTrue(researchGroupFound.isValidated(), "'validated' property should be true by default when created by an ADMIN user");
  }

  @Test
  void createTestWithAlphabeticalCode() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("abc");

    try {
      this.researchGroupService.create(researchGroup);
      fail("A BAD_REQUEST Http response should be received. Research group code must be a positive integer or zero");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void createSameNameTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    this.researchGroupService.create(researchGroup);

    ResearchGroup researchGroup2 = new ResearchGroup();
    researchGroup2.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup2.setCode("1234");
    try {
      this.researchGroupService.create(researchGroup2);
      fail("A BAD_REQUEST Http response should be received. Unique constraint on 'name' not satisfied");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void findAllTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    List<ResearchGroup> researchGroups = this.researchGroupService.findAll();
    assertTrue(researchGroups.size() >= 1);
  }

  @Test
  void findOneTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroup.getResId());
    assertNotNull(researchGroupFound);
    assertEquals(createdResearchGroup.getName(), researchGroupFound.getName());
    assertEquals(createdResearchGroup.getCode(), researchGroupFound.getCode());

  }

  @Test
  void updateResearchGroupTest() {
    String newName = this.getTemporaryTestLabel("Unilab2");
    final String acronym = "ULB";

    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1111");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);

    ResearchGroup researchGroupUpdate = new ResearchGroup();
    researchGroupUpdate.setName(newName);
    researchGroupUpdate.setCode("2222");
    researchGroupUpdate.setAcronym(acronym);
    this.researchGroupService.update(createdResearchGroup.getResId(), researchGroupUpdate);

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroup.getResId());
    assertNotNull(researchGroupFound);
    assertEquals(newName, researchGroupFound.getName());
    assertEquals(acronym, researchGroupFound.getAcronym());
  }

  @Test
  void deleteResearchGroupTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    this.researchGroupService.delete(createdResearchGroup.getResId());

    final String groupId = createdResearchGroup.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.structureService.findOne(groupId));
  }

  @Test
  void deleteAllResearchGroupTest() {

    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup1 = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup1);

    ResearchGroup researchGroup2 = new ResearchGroup();
    researchGroup2.setName(this.getTemporaryTestLabel("Unilab2"));
    researchGroup2.setCode("9999");
    researchGroup2.setAcronym("UL2");
    ResearchGroup createdResearchGroup2 = this.researchGroupService.create(researchGroup2);
    assertNotNull(createdResearchGroup2);

    /**
     * Find before delete
     */
    ResearchGroup researchGroupFound1 = this.researchGroupService.findOne(createdResearchGroup1.getResId());
    assertNotNull(researchGroupFound1);
    ResearchGroup researchGroupFound2 = this.researchGroupService.findOne(createdResearchGroup2.getResId());
    assertNotNull(researchGroupFound2);

    /**
     * Delete
     */
    String[] ids = Arrays.asList(researchGroupFound1.getResId(), researchGroupFound2.getResId()).toArray(new String[0]);
    this.researchGroupService.deleteList(ids);

    /**
     * Find after delete
     */
    final String groupId1 = researchGroupFound1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.structureService.findOne(groupId1));

    final String groupId2 = researchGroupFound2.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.structureService.findOne(groupId2));
  }

  @Test
  void validateResearchGroupTest() {
    this.restClientTool.sudoUser();
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    String createdResearchGroupId = createdResearchGroup.getResId();

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroupId);
    assertEquals(researchGroup.getName(), researchGroupFound.getName());
    assertEquals(researchGroup.getCode(), researchGroupFound.getCode());
    assertEquals(researchGroup.getAcronym(), researchGroupFound.getAcronym());
    assertFalse(researchGroupFound.isValidated(), "'validated' property should be false by default when created by a USER user");
    this.restClientTool.exitSudo();

    this.researchGroupService.validate(createdResearchGroupId);

    researchGroupFound = this.researchGroupService.findOne(createdResearchGroupId);
    assertTrue(researchGroupFound.isValidated(), "'validated' property should now be true");
  }

  @Test
  void addAndRemovePersonTest() {

    /*
     * Create ResearchGroup
     */
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    ResearchGroup createdResearchGroup1 = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup1);

    /*
     * Check number of person before adding one
     */
    List<Person> units = this.researchGroupService.getPeople(createdResearchGroup1.getResId());
    final int sizeBefore = units.size();

    /*
     * Add person to research group
     */
    Person person = this.getPermanentPerson(AuthApplicationRole.ROOT);
    this.researchGroupService.addPerson(createdResearchGroup1.getResId(), person.getResId());

    /*
     * Check it has been added
     */
    units = this.researchGroupService.getPeople(createdResearchGroup1.getResId());
    assertNotNull(units);
    assertEquals(sizeBefore + 1, units.size());

    /*
     * Remove the user
     */
    this.researchGroupService.removePerson(createdResearchGroup1.getResId(), person.getResId());

    /*
     * Check it has been removed
     */
    units = this.researchGroupService.getPeople(createdResearchGroup1.getResId());
    assertNotNull(units);
    assertEquals(sizeBefore, units.size());

    /*
     * Add two People and link them to the ResearchGroup
     */
    final int beforeAddingUsers = this.researchGroupService.getPeople(createdResearchGroup1.getResId()).size();
    this.researchGroupService.addPerson(createdResearchGroup1.getResId(), person.getResId());

    Person person1 = this.getPermanentPerson(AuthApplicationRole.ADMIN);
    this.researchGroupService.addPerson(createdResearchGroup1.getResId(), person1.getResId());

    /*
     * Check they are all linked correctly
     */
    units = this.researchGroupService.getPeople(createdResearchGroup1.getResId());
    assertNotNull(units);
    assertEquals(beforeAddingUsers + 2, units.size());

    /*
     * Delete one specifically and check it is not in the list anymore
     */
    this.researchGroupService.removePerson(createdResearchGroup1.getResId(), person1.getResId());
    units = this.researchGroupService.getPeople(createdResearchGroup1.getResId());
    assertNotNull(units);
    assertEquals(beforeAddingUsers + 1, units.size());
    for (final Person fetched_p : units) {
      assertNotEquals(person1.getResId(), fetched_p.getResId());
    }

    this.researchGroupService.removePerson(createdResearchGroup1.getResId(), person.getResId());
    units = this.researchGroupService.getPeople(createdResearchGroup1.getResId());
    assertEquals(beforeAddingUsers, units.size());

  }

  @Override
  protected void deleteFixtures() {
    this.clearResearchGroupFixtures();
  }
}
