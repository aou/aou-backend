/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PersonIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import java.util.List;

import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.test.AouTestConstants;

public abstract class PersonIT extends AbstractAdminIT {

  protected final String ROLE_IDS_PROPERTY = "roleIds";

  protected Person createPerson(String firstName, String lastName, boolean orcid) {
    final Person person = new Person();
    person.setFirstName(AouTestConstants.getRandomNameWithTemporaryLabel(firstName));
    person.setLastName(AouTestConstants.getRandomNameWithTemporaryLabel(lastName));
    if (orcid) {
      person.setOrcid(AouTestConstants.getRandomOrcId());
    }
    return person;
  }

  protected boolean found(String search, String matchType, Person personToFound) {
    final List<Person> people = this.personService.search(search, matchType);
    for (final Person p : people) {
      if (p.getResId().equals(personToFound.getResId())) {
        return true;
      }
    }
    return false;
  }

  protected String getCurrentPersonId() {
    User currentUser = this.userService.getAuthenticatedUser();
    Person currentPerson = this.getLinkedPersonToUser(currentUser);
    return currentPerson.getResId();
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearPeopleFixtures();
    this.clearStructureFixtures();
    this.clearPersonNotificationTypes();
    this.restClientTool.exitSudo();
  }
}
