/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PersonAsRootIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;

class PersonAsRootIT extends PersonIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Test
  void uploadLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Find the person linked to the user connected
     */
    User myUser = this.userService.getAuthenticatedUser();

    Person myPerson = this.getLinkedPersonToUser(myUser);

    final Person fetchedPerson = this.personService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);
  }

  @Test
  void uploadLogoToThirdPartyUser() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    final User thirdPartyUser = this.findThirdPartyUser();

    Person thirdPartyPerson = this.getLinkedPersonToUser(thirdPartyUser);

    final Person fetchedPerson = this.personService.uploadAvatar(thirdPartyPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);
  }

  @Test
  void downloadLogo() throws IOException, URISyntaxException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();
    /*
     * Find the person linked to the user connected
     */
    User myUser = this.userService.getAuthenticatedUser();

    Person myPerson = this.getLinkedPersonToUser(myUser);

    final Person fetchedPerson = this.personService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.personService.downloadAvatar(refetchedPerson.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedPerson.getAvatar().getFileSize());
  }

  @Test
  void downloadLogoToThirdPartyUser() throws IOException, URISyntaxException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    final User thirdPartyUser = this.findThirdPartyUser();
    Person thirdPartyPerson = this.getLinkedPersonToUser(thirdPartyUser);

    final Person fetchedPerson = this.personService.uploadAvatar(thirdPartyPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.personService.downloadAvatar(refetchedPerson.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedPerson.getAvatar().getFileSize());
  }

  @Test
  void deleteLogo() throws IOException, URISyntaxException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    User myUser = this.userService.getAuthenticatedUser();
    Person myPerson = this.getLinkedPersonToUser(myUser);

    this.personService.uploadAvatar(myPerson.getResId(), logoToUpload);
    assertNotNull(myPerson.getAvatar());
    assertEquals(myPerson.getAvatar().getFileName(), logoName);
    assertEquals(myPerson.getAvatar().getFileSize(), sizeLogo);
    this.personService.deleteAvatar(myPerson.getResId());

    Person person = this.personService.findOne(myPerson.getResId());
    assertNull(person.getAvatar());
  }

  @Test
  void deleteLogoToThirdPartyUser() throws IOException, URISyntaxException {
    User thirdPartyUser = this.findThirdPartyUser();
    Person thirdPartyPerson = this.getLinkedPersonToUser(thirdPartyUser);

    this.personService.deleteAvatar(thirdPartyPerson.getResId());

    Person person = this.personService.findOne(thirdPartyPerson.getResId());
    assertNull(person.getAvatar());
  }

  @Test
  void addAndRemoveRoleOnPersonTest() {
    this.restClientTool.sudoRoot();
    Person person = this.createPerson("Jean-Claude", "Dusse", false);
    Person createdPerson = this.personService.create(person);
    this.restClientTool.exitSudo();

    // New Person hasn't any Role
    JsonNode personJsonNode = this.personService.findOneAsJsonNode(createdPerson.getResId());
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    JsonNode roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(((ArrayNode) roleIdsNode).isEmpty());

    // The Person hasn't any Role anymore
    personJsonNode = this.personService.findOneAsJsonNode(createdPerson.getResId());
    assertNotNull(personJsonNode);
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(((ArrayNode) roleIdsNode).isEmpty());
  }
}
