/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - UserAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.model.SolidifyApplicationRole;

import ch.unige.aou.model.security.User;

class UserAsAdminIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Disabled("Reactivate this test when a decision about this behavior has been made")
  @Test
  void adminCanSetUserToAdminAndRevert() {
    User user = this.getUserWithRole(AuthApplicationRole.USER);
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
    User updatedUser = this.userService.update(user.getResId(), user);
    assertEquals(AuthApplicationRole.ADMIN, updatedUser.getApplicationRole(), "User must be set to ADMIN");
    // Restore user
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
    updatedUser = this.userService.update(user.getResId(), user);
    assertEquals(AuthApplicationRole.USER, updatedUser.getApplicationRole(), "User must be restored to USER");
  }

  @Test
  void adminCannotSetUserToRoot() {
    User user = this.getUserWithRole(AuthApplicationRole.USER);
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
    String userId = user.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.userService.update(userId, user);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void adminCannotSetAdminToRoot() {
    User user = this.getUserWithRole(AuthApplicationRole.ADMIN);
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ROOT));
    String userId = user.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.userService.update(userId, user);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void adminCannotSetRootToAdmin() {
    User user = this.getUserWithRole(AuthApplicationRole.ROOT);
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.ADMIN));
    String userId = user.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.userService.update(userId, user);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void adminCannotSetRootToUser() {
    User user = this.getUserWithRole(AuthApplicationRole.ROOT);
    user.setApplicationRole(new SolidifyApplicationRole(AuthApplicationRole.USER));
    String userId = user.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.userService.update(userId, user);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing
  }
}
