/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - UserAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.unige.aou.model.security.User;

class UserAsUserIT extends AbstractAdminIT {

  @Test
  void userCannotCreateUser() {
    User user = new User();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.userService.create(user);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void userCannotUpdateUser() {
    this.restClientTool.sudoRoot();
    User user = this.getUserWithRole(AuthApplicationRole.USER);
    this.restClientTool.exitSudo();
    user.setLastName("Grogou");
    String userId = user.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.userService.update(userId, user);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void userCannotDeleteUser() {
    this.restClientTool.sudoRoot();
    User user = this.getUserWithRole(AuthApplicationRole.USER);
    this.restClientTool.exitSudo();
    String userId = user.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.userService.delete(userId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Override
  protected void deleteFixtures() {
    // Do nothing
  }
}
