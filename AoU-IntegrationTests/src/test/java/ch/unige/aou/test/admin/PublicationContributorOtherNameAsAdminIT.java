/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationContributorOtherNameAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.exception.SolidifyRuntimeException;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.ObjectFactory;
import ch.unige.aou.model.xml.deposit.v2_4.OtherName;
import ch.unige.aou.model.xml.deposit.v2_4.OtherNames;

class PublicationContributorOtherNameAsAdminIT extends PublicationIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Override
  protected String getCurrentUserId() {
    return this.TEST_USER_ADMIN_ID;
  }

  @Test
  void updateAndResetContributorDisplayNameTest() {
    String firstName = "John";
    String lastName = "Doe";
    String cnIndividu = "12345";

    String title = "test to force contributor display name 1";
    Publication newPublication = this.testPublicationCreation(title);
    String resId = newPublication.getResId();
    Publication fetchedPublication = this.publicationService.findOne(resId);
    assertNotNull(fetchedPublication);
    this.checkContributorInFormData(fetchedPublication, cnIndividu, firstName, lastName);
    this.checkContributorInMetadata(fetchedPublication, cnIndividu, firstName, lastName, null, null);

    // Forces the contributor name for this publication (Contributor other name doesn't exists yet)
    String newFirstName = "Johny";
    String newLastName = "Dude";
    this.publicationService.updateContributorDisplayName(new String[] { resId }, cnIndividu, newFirstName, newLastName);

    fetchedPublication = this.publicationService.findOne(resId);
    assertNotNull(fetchedPublication);
    this.checkContributorInFormData(fetchedPublication, cnIndividu, newFirstName, newLastName);
    this.checkContributorInMetadata(fetchedPublication, cnIndividu, newFirstName, newLastName, firstName, lastName);

    // Create another Publication
    String title2 = "test to force contributor display name 2";
    Publication newPublication2 = this.testPublicationCreation(title2);
    String resId2 = newPublication2.getResId();
    Publication fetchedPublication2 = this.publicationService.findOne(resId2);
    assertNotNull(fetchedPublication2);
    this.checkContributorInFormData(fetchedPublication2, cnIndividu, firstName, lastName);
    this.checkContributorInMetadata(fetchedPublication2, cnIndividu, firstName, lastName, null, null);

    // Forces the contributor name for this 2nd publication (Contributor other name now already exists)
    this.publicationService.updateContributorDisplayName(new String[] { resId2 }, cnIndividu, newFirstName, newLastName);

    fetchedPublication2 = this.publicationService.findOne(resId2);
    assertNotNull(fetchedPublication2);
    this.checkContributorInFormData(fetchedPublication2, cnIndividu, newFirstName, newLastName);
    this.checkContributorInMetadata(fetchedPublication2, cnIndividu, newFirstName, newLastName, firstName, lastName);

    // Reset contributor name of the first publication
    this.publicationService.updateContributorDisplayName(new String[] { resId }, cnIndividu, firstName, lastName);
    fetchedPublication = this.publicationService.findOne(resId);
    assertNotNull(fetchedPublication);
    this.checkContributorInFormData(fetchedPublication, cnIndividu, firstName, lastName);
    this.checkContributorInMetadata(fetchedPublication, cnIndividu, firstName, lastName, null, null);
  }

  @Test
  void createFromXmlWithContributorOtherNameTest() {
    String firstName = "John";
    String lastName = "Doe";
    String cnIndividu = "12345";
    String otherFirstName = "Johny";
    String otherLastName = "Dude";
    String title = this.getTemporaryTestLabel("test creation with contributor other name from XML");

    ObjectFactory factory = new ObjectFactory();
    String xmlMetadata = String.format(AouConstants.TEST_DEPOSIT_XML_METADATA_TEMPLATE_2_4, title);
    final DepositDoc depositDoc = JAXB.unmarshal(new StringReader(xmlMetadata), DepositDoc.class);
    Contributor contributor = new Contributor();
    contributor.setFirstname(firstName);
    contributor.setLastname(lastName);
    contributor.setCnIndividu(cnIndividu);
    OtherNames otherNames = factory.createOtherNames();
    OtherName otherName = factory.createOtherName();
    otherName.setFirstname(otherFirstName);
    otherName.setLastname(otherLastName);
    otherNames.getOtherName().add(otherName);
    contributor.setOtherNames(otherNames);
    depositDoc.setContributors(factory.createContributors());
    depositDoc.getContributors().getContributorOrCollaboration().add(contributor);

    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(DepositDoc.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(depositDoc, sw);
      xmlMetadata = sw.toString();

      Publication publication = new Publication();
      publication.setMetadata(xmlMetadata);
      Publication savedPublication = this.publicationService.create(publication);
      Publication fetchedPublication = this.publicationService.findOne(savedPublication.getResId());
      assertNotNull(fetchedPublication);
      this.checkContributorInFormData(fetchedPublication, cnIndividu, firstName, lastName);
      this.checkContributorInMetadata(fetchedPublication, cnIndividu, firstName, lastName, otherFirstName, otherLastName);

    } catch (Exception e) {
      throw new SolidifyRuntimeException("unable to serialize DepositDoc to XML", e);
    }
  }

  private void checkContributorInFormData(Publication publication, String expectedCnIndividu, String expectedFirstName,
          String expectedLastName) {
    JSONObject expectedJsonObject = new JSONObject(publication.getFormData());
    assertFalse(expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").isEmpty());
    JSONObject contributorJsonObject = expectedJsonObject.getJSONObject("contributors").getJSONArray("contributors").getJSONObject(0)
            .getJSONObject("contributor");
    assertEquals(expectedCnIndividu, contributorJsonObject.getString("cnIndividu"));
    assertEquals(expectedFirstName, contributorJsonObject.getString("firstname"));
    assertEquals(expectedLastName, contributorJsonObject.getString("lastname"));
  }

  private void checkContributorInMetadata(Publication publication, String expectedCnIndividu, String expectedFirstName,
          String expectedLastName, String expectedOtherFirstName, String expectedOtherLastName) {
    final DepositDoc depositDoc = JAXB.unmarshal(new StringReader(publication.getMetadata()), DepositDoc.class);
    Contributor contributor = (Contributor) depositDoc.getContributors().getContributorOrCollaboration().get(0);
    assertEquals(expectedCnIndividu, contributor.getCnIndividu());
    assertEquals(expectedFirstName, contributor.getFirstname());
    assertEquals(expectedLastName, contributor.getLastname());

    if (expectedOtherFirstName == null && expectedOtherLastName == null) {
      assertNull(contributor.getOtherNames());
    } else {
      assertNotNull(contributor.getOtherNames());
      OtherName otherName = contributor.getOtherNames().getOtherName().get(0);
      assertEquals(expectedOtherFirstName, otherName.getFirstname());
      assertEquals(expectedOtherLastName, otherName.getLastname());
    }
  }

  @Override
  protected void deleteFixtures() {
    super.deleteFixtures();
    List<ch.unige.aou.model.publication.Contributor> contributors = this.contributorService.searchByProperties(
            Collections.singletonMap("cnIndividu", "12345"));
    if (contributors.size() == 1) {
      String contributorId = contributors.get(0).getResId();
      List<ContributorOtherName> otherNames = this.contributorOtherNameService.findAll(contributorId);
      for (ContributorOtherName contributorOtherName : otherNames) {
        this.contributorOtherNameService.delete(contributorId, contributorOtherName.getResId());
      }
    }

  }
}
