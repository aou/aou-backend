/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - AbstractContributorIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorOtherName;

public abstract class AbstractContributorIT extends AbstractAdminIT {

  protected final String testFirstName = this.getTemporaryTestLabelWithoutSpaces(
          "Johnnnn"); // "Johnnnn" to ensure name is unique even when split on space
  protected final String testLastName = this.getTemporaryTestLabelWithoutSpaces(
          "Dooooe"); // "Dooooe" to ensure name is unique even when split on space
  protected final String testCnIndividu = "1234567890";
  protected final String testOtherFirstName = "Abelard";
  protected final String testOtherLastName = "Ducovitch";

  protected Contributor getContributor() {
    Contributor contributor = new Contributor();
    String lastName = this.testLastName;
    String firstName = this.testFirstName;
    String cnIndividu = this.testCnIndividu;
    contributor.setLastName(lastName);
    contributor.setFirstName(firstName);
    contributor.setCnIndividu(cnIndividu);
    return contributor;
  }

  protected Contributor testContributorCreate() {
    Contributor contributor = this.getContributor();
    Contributor savedContributor = this.contributorService.create(contributor);
    assertNotNull(savedContributor);
    Contributor fetchedContributor = this.contributorService.findOne(savedContributor.getResId());
    assertEquals(this.testFirstName, fetchedContributor.getFirstName());
    assertEquals(this.testLastName, fetchedContributor.getLastName());
    assertEquals(this.testCnIndividu, fetchedContributor.getCnIndividu());
    return fetchedContributor;
  }

  protected ContributorOtherName testAddOtherNameOnContributor(Contributor contributor, String firstName, String lastName) {
    ContributorOtherName otherName = new ContributorOtherName();
    otherName.setFirstName(firstName);
    otherName.setLastName(lastName);
    ContributorOtherName savedOtherName = this.contributorOtherNameService.create(contributor.getResId(), otherName);
    assertNotNull(savedOtherName);
    assertEquals(firstName, savedOtherName.getFirstName());
    assertEquals(lastName, savedOtherName.getLastName());

    Contributor fetchedContributor = this.contributorService.findOne(contributor.getResId());
    Optional<ContributorOtherName> otherNameOpt = fetchedContributor.getOtherNames().stream().filter(a -> a.getLastName().equals(lastName))
            .findFirst();
    assertTrue(otherNameOpt.isPresent());
    assertEquals(firstName, otherNameOpt.get().getFirstName());
    assertEquals(lastName, otherNameOpt.get().getLastName());

    return savedOtherName;
  }

  void testSearchUnigeContributorWithFirstnameAndLastname() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    this.testAddOtherNameOnContributor(savedContributor, this.testOtherFirstName, this.testOtherLastName);
    this.restClientTool.exitSudo();

    // Search with Contributor name
    List<Contributor> contributors = this.contributorService.searchByTerm(this.testLastName + ", " + this.testFirstName);
    this.checkContributorFound(contributors, "Search 1 failed");

    // Search with Other name
    contributors = this.contributorService.searchByTerm(this.testOtherLastName + ", " + this.testOtherFirstName);
    this.checkContributorFound(contributors, "Search 2 failed");

    // Search with Contributor firstname and other name lastname
    contributors = this.contributorService.searchByTerm(this.testLastName + ", " + this.testOtherFirstName);
    assertNotNull(contributors);
    assertTrue(contributors.isEmpty());
  }

  void testSearchUnigeContributorWithLooseName() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    this.testAddOtherNameOnContributor(savedContributor, this.testOtherFirstName, this.testOtherLastName);
    this.testAddOtherNameOnContributor(savedContributor, "Cibelus", "Kan");
    this.restClientTool.exitSudo();

    String expectedDisplayName =
            this.testLastName + ", " + this.testFirstName + " (" + this.testOtherLastName + ", " + this.testOtherFirstName
                    + AouConstants.CONTRIBUTOR_OTHER_NAMES_SEPARATOR + "Kan, Cibelus)";

    // Search with Contributor name
    List<Contributor> contributors = this.contributorService.searchByTerm(this.testLastName + " " + this.testFirstName);
    this.checkContributorFound(contributors, "Search 1 failed", expectedDisplayName);

    contributors = this.contributorService.searchByTerm(this.testLastName);
    this.checkContributorFound(contributors, "Search 2 failed", expectedDisplayName);

    contributors = this.contributorService.searchByTerm(this.testFirstName);
    this.checkContributorFound(contributors, "Search 3 failed", expectedDisplayName);

    // Search with Other name
    contributors = this.contributorService.searchByTerm(this.testOtherLastName + " " + this.testOtherFirstName);
    this.checkContributorFound(contributors, "Search 4 failed", expectedDisplayName);

    contributors = this.contributorService.searchByTerm(this.testOtherLastName);
    this.checkContributorFound(contributors, "Search 5 failed", expectedDisplayName);

    contributors = this.contributorService.searchByTerm(this.testOtherFirstName);
    this.checkContributorFound(contributors, "Search 6 failed", expectedDisplayName);

    // Search with Contributor name and other names
    contributors = this.contributorService.searchByTerm(this.testLastName + " " + this.testOtherFirstName);
    this.checkContributorFound(contributors, "Search 7 failed", expectedDisplayName);

    contributors = this.contributorService.searchByTerm(this.testOtherLastName + " " + this.testFirstName);
    this.checkContributorFound(contributors, "Search 8 failed", expectedDisplayName);
  }

  private void checkContributorFound(List<Contributor> contributors, String errorMessage) {
    this.checkContributorFound(contributors, errorMessage,
            this.testLastName + ", " + this.testFirstName + " (" + this.testOtherLastName + ", " + this.testOtherFirstName + ")");
  }

  private void checkContributorFound(List<Contributor> contributors, String errorMessage, String expectedDisplayName) {
    assertNotNull(contributors, errorMessage);
    assertEquals(1, contributors.size(), errorMessage);
    assertEquals(expectedDisplayName, contributors.get(0).getDisplayName(), errorMessage);
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearContributorFixtures();
    this.restClientTool.exitSudo();
  }
}
