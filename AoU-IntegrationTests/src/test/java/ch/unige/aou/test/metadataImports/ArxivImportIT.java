/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ArxivImportIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.metadataImports;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.test.AbstractIT;

public class ArxivImportIT extends AbstractIT {
  private static final String JSON_FORM_DATA_FILE = "angular-form-data-with-arxiv.json";

  private String publicationIdToDelete = null;

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.restClientTool.sudoRoot();

    this.publicationIdToDelete = null;
  }

  @Test()
  public void testImportFile() {
    //create a publication
    String jsonFormData = this.getJsonFormData();
    Publication publication = this.createPublicationWithSubType(this.getTemporaryTestLabel("Publication to import arxiv file"));
    publication.setImportSource(Publication.ImportSource.ARXIV);
    publication.setFormData(jsonFormData);
    Publication createdPublication = this.publicationService.create(publication);
    this.publicationIdToDelete = createdPublication.getResId();

    //check document files for this publication
    List<DocumentFile> documentFileList = this.publicationDocumentFileService.findAll(publication.getResId());
    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(documentFileList.get(0).getPublication().getResId(), documentFileList.get(0).getResId());

    assertTrue(documentFileList.size() > 0);

    assertEquals(DocumentFile.AccessLevel.PUBLIC, documentFileList.get(0).getAccessLevel());
  }

  private String getJsonFormData() {
    try (InputStream jsonStream = new ClassPathResource(JSON_FORM_DATA_FILE).getInputStream()) {
      return FileTool.toString(jsonStream);
    } catch (IOException e) {
      fail("Unable to read " + JSON_FORM_DATA_FILE + ": " + e.getMessage());
      return null;
    }
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearPublicationFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    if (!StringTool.isNullOrEmpty(this.publicationIdToDelete)) {
      this.publicationService.delete(this.publicationIdToDelete);
    }
    this.restClientTool.exitSudo();
  }
}
