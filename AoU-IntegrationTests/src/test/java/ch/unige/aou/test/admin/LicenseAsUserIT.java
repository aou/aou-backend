/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - LicenseAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.LicenseGroup;

class LicenseAsUserIT extends LicenseIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  void findAllTest() {
    LicenseGroup licenseGroup = this.createTestLicenseGroup("Test group");

    // Create a new licence
    final License license = this.createTestLicense(licenseGroup, "License test", "OL-ID", 10);

    // Find all
    final List<License> actualLicenses = this.licenseService.findAll();

    // Test at least
    assertFalse(actualLicenses.isEmpty());
  }

  @Test
  void findByGroupTest() {
    LicenseGroup group1 = this.createTestLicenseGroup("group1");
    LicenseGroup group2 = this.createTestLicenseGroup("group2");
    LicenseGroup group3 = this.createTestLicenseGroup("group3");

    License license1 = this.createTestLicense(group1, "license1", "OL1", 10);
    License license2 = this.createTestLicense(group1, "license2", "OL2", 20);
    License license3 = this.createTestLicense(group1, "license3", "OL3", 30);

    License license4 = this.createTestLicense(group2, "license4", "OL4", 40);
    License license5 = this.createTestLicense(group2, "license5", "OL5", 50);

    Map<String, String> params = new HashMap<>();
    params.put("licenseGroup.resId", group1.getResId());
    List<License> group1Licenses = this.licenseService.searchByProperties(params);
    assertNotNull(group1Licenses);
    assertEquals(3, group1Licenses.size());
    assertTrue(group1Licenses.stream().anyMatch(license -> license.getResId().equals(license1.getResId())));
    assertTrue(group1Licenses.stream().anyMatch(license -> license.getResId().equals(license2.getResId())));
    assertTrue(group1Licenses.stream().anyMatch(license -> license.getResId().equals(license3.getResId())));

    params.put("licenseGroup.resId", group2.getResId());
    List<License> group2Licenses = this.licenseService.searchByProperties(params);
    assertNotNull(group2Licenses);
    assertEquals(2, group2Licenses.size());
    assertTrue(group2Licenses.stream().anyMatch(license -> license.getResId().equals(license4.getResId())));
    assertTrue(group2Licenses.stream().anyMatch(license -> license.getResId().equals(license5.getResId())));

    params.put("licenseGroup.resId", group3.getResId());
    List<License> group3Licenses = this.licenseService.searchByProperties(params);
    assertNotNull(group3Licenses);
    assertEquals(0, group3Licenses.size());
  }
}
