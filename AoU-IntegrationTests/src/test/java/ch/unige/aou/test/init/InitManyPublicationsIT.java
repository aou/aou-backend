/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - InitManyPublicationsIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.init;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.test.admin.AbstractAdminIT;

@Disabled("Run this manually")
class InitManyPublicationsIT extends AbstractAdminIT {

  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  private static final Logger log = LoggerFactory.getLogger(InitManyPublicationsIT.class);

  private static final int NUMBER_OF_PUBLICATIONS_TO_CREATE = 1000;
  private static final int MIN_STRUCTURE_ID = 1;
  private static final int MAX_STRUCTURE_ID = 218;

  private Map<String, Structure> fetchedStructures = new HashMap<>();

  /**
   * Structures must have been initialized with the structures.json file in ADMIN module before running this method
   */
  @Test
  void createPublications() {

    for (int i = 1; i <= NUMBER_OF_PUBLICATIONS_TO_CREATE; i++) {
      this.createPublication("Random publication " + i);
      log.info("Publication " + i + " created");
    }
  }

  private Publication createPublication(String title) {

    List<Structure> structures = new ArrayList<>();
    this.addRandomStructure(structures);
    this.addRandomStructure(structures);

    Map<String, Object> properties = new HashMap<>();
    properties.put("title", title);
    this.setRandomTypeAndSubtype(properties);
    properties.put("structures", structures);
    String xmlMetadata = this.getMetadata(properties);

    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);

    this.restClientTool.sudoUser();
    Publication savedPublication = this.publicationService.create(publication);
    this.restClientTool.exitSudo();
    assertNotNull(savedPublication);
    return savedPublication;
  }

  private void addRandomStructure(List<Structure> structures) {
    String structureId = this.getRandomStructureId();
    Structure structure;
    if (!this.fetchedStructures.containsKey(structureId)) {
      structure = this.structureService.findOne(structureId);
      this.fetchedStructures.put(structureId, structure);
    } else {
      structure = this.fetchedStructures.get(structureId);
    }
    if (structure != null && !structures.contains(structure)) {
      structures.add(structure);
    }
  }

  private String getRandomStructureId() {
    Random r = new Random();
    return String.valueOf(r.nextInt(MAX_STRUCTURE_ID) + MIN_STRUCTURE_ID);
  }

  private void setRandomTypeAndSubtype(Map<String, Object> properties) {
    Random r = new Random();
    int typeInt = r.nextInt(3);

    switch (typeInt) {
      case 0:
        properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
        properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
        break;
      case 1:
        properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
        properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME);
        break;
      default:
        properties.put("type", this.PUBLICATION_TYPE_DIPLOME_NAME);
        properties.put("subtype", this.PUBLICATION_SUBTYPE_THESE_NAME);
        break;
    }
  }
}
