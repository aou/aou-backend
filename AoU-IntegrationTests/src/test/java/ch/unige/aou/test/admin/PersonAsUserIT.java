/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PersonAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

class PersonAsUserIT extends PersonIT {

  private User thirdPartyUser;

  @Override
  protected void createFixtures() {
    if (this.thirdPartyUser == null) {
      this.restClientTool.sudoAdmin();
      this.thirdPartyUser = this.findThirdPartyUser();
      this.restClientTool.exitSudo();
    }
  }

  @Test
  void creationTest() {
    // Create a person
    Person person1 = this.createPerson("Jean-Claude", "Dusse", false);
    try {
      this.personService.create(person1);
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void deleteTest() {
    // Create a person
    Person person1 = this.createPerson("Jérôme", "Tarayre", false);
    // Delete the person
    String person1Id = person1.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personService.delete(person1Id);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void updateTest() {
    // Create a person
    Person person1 = this.createPerson("Jérôme", "Tarayre", false);

    // Update the person
    final Person person2 = this.createPerson("Robert", "Lespinass", true);
    String person1Id = person1.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personService.update(person1Id, person2);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void uploadLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    /*
     * Find the person linked to the user connected
     */
    User myUser = this.userService.getAuthenticatedUser();

    Person myPerson = this.getLinkedPersonToUser(myUser);

    final Person fetchedPerson = this.personService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);
  }

  @Test
  void uploadLogoToThirdPartyUser() {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final String personId = this.thirdPartyUser.getPerson().getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> {
      this.personService.uploadAvatar(personId, logoToUpload);
    });
  }

  @Test
  void downloadLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();
    /*
     * Find the person linked to the user connected
     */
    User myUser = this.userService.getAuthenticatedUser();

    Person myPerson = this.getLinkedPersonToUser(myUser);

    final Person fetchedPerson = this.personService.uploadAvatar(myPerson.getResId(), logoToUpload);

    /*
     * Check logo has been uploaded
     */
    final Person refetchedPerson = this.personService.findOne(fetchedPerson.getResId());
    assertEquals(refetchedPerson.getAvatar().getFileName(), logoName);
    assertEquals(refetchedPerson.getAvatar().getFileSize(), sizeLogo);

    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.personService.downloadAvatar(refetchedPerson.getResId(), path);
    assertNotNull(path.toFile());
    assertEquals(Long.valueOf(path.toFile().length()), refetchedPerson.getAvatar().getFileSize());
  }

  @Test
  void downloadLogoToThirdPartyUser() {
    this.restClientTool.sudoAdmin();
    Person thirdPartyPerson = this.getLinkedPersonToUser(this.thirdPartyUser);
    this.restClientTool.exitSudo();

    try {
      Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
      this.personService.downloadAvatar(thirdPartyPerson.getResId(), path);
      assertNotNull(path.toFile());
      assertEquals(Long.valueOf(path.toFile().length()), thirdPartyPerson.getAvatar().getFileSize());
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
    }
  }

  @Test
  void deleteLogo() throws IOException {
    final String logoName = "geneve.jpg";
    final ClassPathResource logoToUpload = new ClassPathResource(logoName);
    final Long sizeLogo = logoToUpload.contentLength();

    User myUser = this.userService.getAuthenticatedUser();
    Person myPerson = this.getLinkedPersonToUser(myUser);

    this.personService.uploadAvatar(myPerson.getResId(), logoToUpload);
    assertNotNull(myPerson.getAvatar());
    assertEquals(myPerson.getAvatar().getFileName(), logoName);
    assertEquals(myPerson.getAvatar().getFileSize(), sizeLogo);
    this.personService.deleteAvatar(myPerson.getResId());

    Person person = this.personService.findOne(myPerson.getResId());
    assertNull(person.getAvatar());
  }

  @Test
  void deleteLogoToThirdPartyUser() {
    final String personId = this.getLinkedPersonToUser(this.thirdPartyUser).getResId();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> {
      this.personService.deleteAvatar(personId);
    });
  }

  @Test
  void addAndRemoveNotificationTypeTest() {
    /*
     * Find the person linked to the user connected
     */
    User myUser = this.userService.getAuthenticatedUser();

    Person myPerson = this.getLinkedPersonToUser(myUser);
    // check the notification types that has been already described
    List<NotificationType> subscribedNotificationTypes = this.personService.getSubscribedNotificationTypes(myPerson.getResId());
    final int initialState = subscribedNotificationTypes.size();

    // Remove notification
    this.personService.removeNotificationType(myPerson.getResId(), NotificationType.MY_PUBLICATION_COMMENTED.getResId());
    subscribedNotificationTypes = this.personService.getSubscribedNotificationTypes(myPerson.getResId());

    assertEquals(initialState - 1, subscribedNotificationTypes.size());

    // Add new notification type to person
    this.personService.addNotificationType(myPerson.getResId(), NotificationType.MY_PUBLICATION_COMMENTED.getResId());
    subscribedNotificationTypes = this.personService.getSubscribedNotificationTypes(myPerson.getResId());

    assertEquals(initialState, subscribedNotificationTypes.size());

    // Test added person on the OrganizationalUnit
    for (final NotificationType notificationType : subscribedNotificationTypes) {
      if (notificationType.getResId().equals(NotificationType.MY_PUBLICATION_COMMENTED.getResId())) {
        assertNotNull(notificationType);
        assertEquals(NotificationType.MY_PUBLICATION_COMMENTED.getEventType(), notificationType.getEventType());
      }
    }
  }

  @Test
  void searchTest() {
    try {
      this.personService.search("ab", "ba");
      fail("A FORBIDDEN Http response should be received");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
    }
  }

  @Test
  void listProfileResearchGroupsTest() {
    User authenticatedUser = this.userService.getAuthenticatedUser();
    List<ResearchGroup> researchGroups = this.personService.getResearchGroup(authenticatedUser.getPerson().getResId());
    assertNotNull(researchGroups);
  }

  @Test
  void listProfileStructuresTest() {
    User authenticatedUser = this.userService.getAuthenticatedUser();
    List<Structure> structures = this.personService.getStructures(authenticatedUser.getPerson().getResId());
    assertNotNull(structures);
  }

  @Test
  void cannotAddRoleOnOtherPersonTest() {
    this.restClientTool.sudoRoot();
    Person person = this.createPerson("Jean-Claude", "Dusse", false);
    Person createdPerson = this.personService.create(person);
    this.restClientTool.exitSudo();

    String createdPersonResId = createdPerson.getResId();

    // New Person hasn't any Role
    JsonNode personJsonNode = this.personService.findOneAsJsonNode(createdPersonResId);
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    JsonNode roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(roleIdsNode.isEmpty());

    // No role has been added
    personJsonNode = this.personService.findOneAsJsonNode(createdPersonResId);
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(roleIdsNode.isEmpty());
  }

  @Test
  void cannotAddRoleOnOneselfTest() {
    String currentPersonId = this.getCurrentPersonId();

    // Current Person hasn't any Role
    JsonNode personJsonNode = this.personService.findOneAsJsonNode(currentPersonId);
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    JsonNode roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(roleIdsNode.isEmpty());

    // No role has been added
    personJsonNode = this.personService.findOneAsJsonNode(currentPersonId);
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(roleIdsNode.isEmpty());
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearPeopleFixtures();
    this.clearStructureFixtures();
    this.restClientTool.exitSudo();
  }

}
