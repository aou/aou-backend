/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationCommentIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.Publication;

class PublicationCommentIT extends AbstractAdminIT {

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.createPublicationFixture();
    this.createCommentInPublicationTestFixture();
    this.restClientTool.exitSudo();
  }

  @Test
  void createInOtherPublicationTest() {
    //get publication
    Publication publication = this.getTestPublication();
    // Create a new Comment
    Comment comment = new Comment();
    comment.setText(this.getTemporaryTestLabel("comment test"));
    comment.setPublication(publication);
    comment.setOnlyForValidators(false);
    comment = this.commentClientService.create(publication.getResId(), comment);
    // Test creation
    assertNotNull(comment.getResId());
  }

  @Test
  void findAllInOtherPublicationTest() {
    //get publication
    Publication publication = this.getTestPublication();

    // findAll
    int sizeOfComments = this.commentClientService.findAll(publication.getResId()).size();

    // Test at least
    assertEquals(1, sizeOfComments);
  }

  @Test
  void getOtherCommentTest() {
    // create publication and comments
    Publication publication = this.getTestPublication();
    this.restClientTool.sudoRoot();
    // Create a new Comment
    Comment comment = new Comment();
    comment.setText(this.getTemporaryTestLabel("comment test"));
    comment.setPublication(publication);
    comment.setOnlyForValidators(false);
    this.commentClientService.create(publication.getResId(), comment);
    this.restClientTool.exitSudo();

    //Get comments from this publication
    String publicationId = publication.getResId();
    String commentId = comment.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.commentClientService.findOne(publicationId, commentId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

  }

  @Test
  void createTest() {
    //get publication
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to add comment"));
    // Create a new Comment
    Comment comment = new Comment();
    comment.setText(this.getTemporaryTestLabel("comment test"));
    comment.setPublication(publication);
    comment.setOnlyForValidators(false);
    comment = this.commentClientService.create(publication.getResId(), comment);
    // Test creation
    assertNotNull(comment.getResId());
  }

  @Test
  void createForValidatorTest() {
    //get publication
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to add comment"));
    // Create a new Comment
    Comment comment = new Comment();
    comment.setText(this.getTemporaryTestLabel("comment test"));
    comment.setPublication(publication);
    comment.setOnlyForValidators(true);
    String publicationId = publication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.commentClientService.create(publicationId, comment);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Test
  void findAllTest() {
    //get publication
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to find comment"));
    // Create a new Comment
    Comment comment = new Comment();
    comment.setText(this.getTemporaryTestLabel("comment test"));
    comment.setPublication(publication);
    comment.setOnlyForValidators(false);
    comment = this.commentClientService.create(publication.getResId(), comment);

    // findAll
    int sizeOfComments = this.commentClientService.findAll(publication.getResId()).size();

    // Test at least
    assertEquals(1, sizeOfComments);
  }

  @Test
  void deleteTest() {
    //get publication
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to delete comment"));
    // Create a new Comment
    Comment comment = new Comment();
    comment.setText(this.getTemporaryTestLabel("comment test"));
    comment.setPublication(publication);
    comment.setOnlyForValidators(false);

    comment = this.commentClientService.create(publication.getResId(), comment);

    // findAll
    int sizeOfCommentsBefore = this.commentClientService.findAll(publication.getResId()).size();

    // Test at least
    assertEquals(1, sizeOfCommentsBefore);

    // Delete
    this.commentClientService.delete(publication.getResId(), comment.getResId());
    int sizeOfCommentsAfter = this.commentClientService.findAll(publication.getResId()).size();

    // Test deletion
    assertEquals(0, sizeOfCommentsAfter);
  }

  private void clearCommentFixtures(String publicationId) {
    this.commentClientService.findAll(publicationId).stream()
            .filter(comment -> comment.getText().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(comment -> this.commentClientService.delete(publicationId, comment.getResId()));
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearCommentFixtures();
    this.clearPublicationFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearStructureFixtures();
    this.restClientTool.exitSudo();
  }

}
