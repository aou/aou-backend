/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ContactMessageIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.rest.Result;
import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.contact.ContactableContributor;
import ch.unige.aou.model.contact.PublicationContactMessage;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.Publication;

class ContactMessageIT extends AbstractAdminIT {

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.createPublicationFixture();
    this.restClientTool.exitSudo();
  }

  @Test
  void getContactableContributorsNone() {
    Publication publication = this.getTestPublication();
    List<ContactableContributor> contributors = this.publicationService.getContributorsContactableByEmail(publication.getResId());
    assertNotNull(contributors);
    assertTrue(contributors.isEmpty());
  }

  @Test
  void getContactableContributors() {
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", "validation test");
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    /************************
     * create with an existing UNIGE contributor
     */
    List<AbstractContributor> contributorDTOs = new ArrayList<>();

    ContributorDTO contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Nicolas");
    contributorDTO.setLastName("Rod");
    contributorDTO.setRole("author");
    contributorDTO.setCnIndividu("102427");
    contributorDTOs.add(contributorDTO);
    properties.put("contributors", contributorDTOs);

    String xmlMetadata = this.getMetadata(properties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS); // to validate contributors roles

    publication = this.publicationService.create(publication);

    List<ContactableContributor> contributors = this.publicationService.getContributorsContactableByEmail(publication.getResId());
    assertNotNull(contributors);
    assertEquals(1, contributors.size());
    assertEquals("102427", contributors.get(0).getCnIndividu());
  }

  @Test
  @Disabled("Run this manually")
  void sendContactMessage() {
    Publication publication = this.getTestPublication();
    PublicationContactMessage contactMessage = new PublicationContactMessage();
    contactMessage.setSenderName("John Doe");
    contactMessage.setSenderEmail("john.doe@example.com");
    contactMessage.setRecipientCnIndividu("102427");
    contactMessage.setMessageContent("Hello, I am the message content");

    Result result = this.publicationService.sendContactMessage(publication.getResId(), contactMessage);
    assertEquals(Result.ActionStatus.EXECUTED, result.getStatus());
  }

  @Test
  void sendEmptyContactMessage() {
    Publication publication = this.getTestPublication();
    PublicationContactMessage contactMessage = new PublicationContactMessage();

    String publicationId = publication.getResId();
    HttpClientErrorException.BadRequest e = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.publicationService.sendContactMessage(publicationId, contactMessage));
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("senderName", "The sender's name cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("senderEmail", "The sender's email cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("recipientCnIndividu", "The recipient cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("messageContent", "The message cannot be empty", validationErrors));
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearPublicationFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearStructureFixtures();
    this.restClientTool.exitSudo();
  }
}
