/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - AbstractNotificationIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.service.admin.PersonNotificationClientService;

public abstract class AbstractNotificationIT extends AbstractAdminIT {

  @Autowired
  protected PersonNotificationClientService personNotificationClientService;

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.restClientTool.exitSudo();
  }

  protected Publication createNotificationForCurrentUser() {
    // create a publication and send it to validation
    final Structure structure = this.getPermanentStructure();
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Test notifications"));
    this.publicationService.submitForApproval(publication.getResId(), structure.getResId());

    // ask feedback on publication --> it creates an event and a notification
    this.restClientTool.sudoAdmin();
    this.publicationService.askFeedback(publication.getResId(), "ask feedback to test notification creation");
    this.restClientTool.exitSudo();

    return publication;
  }

  protected Notification getLastNotificationForCurrentUser() {
    Map<String, String> queryParameters = new HashMap<>();
    queryParameters.put("sort", "lastUpdate.when,desc");
    List<Notification> notifications = this.personNotificationClientService.findAll(this.getCurrentPerson().getResId(), queryParameters);
    if (notifications != null && !notifications.isEmpty()) {
      return notifications.get(0);
    }

    return null;
  }

  protected Person getCurrentPerson() {
    this.restClientTool.sudoRoot();
    User user = this.userService.findOne(this.getCurrentUserId());
    this.restClientTool.exitSudo();
    return user.getPerson();
  }

  protected abstract String getCurrentUserId();

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearNotificationFixtures();
    this.clearEventsFixtures();
    this.clearPublicationFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearStructureFixtures();
    this.restClientTool.exitSudo();
  }
}
