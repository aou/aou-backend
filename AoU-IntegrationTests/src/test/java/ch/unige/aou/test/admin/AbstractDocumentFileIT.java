/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - AbstractDocumentFileIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.AfterAll;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;

public class AbstractDocumentFileIT extends AbstractAdminIT {

  private static final Logger log = LoggerFactory.getLogger(AbstractDocumentFileIT.class);

  protected static final String TEST_FILE_NAME2 = "test2.pdf";
  protected static final String TEST_FILE_NAME3 = "test3.pdf";

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.createPublicationFixture();
    this.restClientTool.exitSudo();
  }
  
  protected DocumentFile uploadDocumentFile(Publication publication, Resource fileToUpload) throws IOException {
    return this.uploadDocumentFile(publication, fileToUpload, fileToUpload.getFilename());
  }

  protected DocumentFile uploadDocumentFile(Publication publication, Resource fileToUpload, String expectedFileName) throws IOException {
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);
    final String label = "complement of description";

    MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add("documentFileTypeId", documentFileType.getResId());
    parameters.add("accessLevel", DocumentFile.AccessLevel.PUBLIC.name());
    parameters.add("label", label);

    final long fileToUploadSize = fileToUpload.contentLength();

    DocumentFile uploadedDocumentFile = this.publicationDocumentFileService.upload(publication.getResId(), fileToUpload, parameters);

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(uploadedDocumentFile.getPublication().getResId(), uploadedDocumentFile.getResId());

    assertEquals(DocumentFile.DocumentFileStatus.READY, foundDocumentFile.getStatus());
    assertFalse(foundDocumentFile.getFinalData() == null || StringTool.isNullOrEmpty(foundDocumentFile.getFinalData().toString()));
    assertFalse(StringTool.isNullOrEmpty(foundDocumentFile.getFileName()));
    assertEquals(expectedFileName, foundDocumentFile.getFileName());
    assertEquals(fileToUploadSize, (long) foundDocumentFile.getFileSize());
    assertEquals(DocumentFile.AccessLevel.PUBLIC, foundDocumentFile.getAccessLevel());
    assertEquals(label, foundDocumentFile.getLabel());
    assertNull(foundDocumentFile.getEmbargoAccessLevel());
    assertNull(foundDocumentFile.getEmbargoEndDate());

    return foundDocumentFile;
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearPublicationFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearStructureFixtures();
    this.restClientTool.exitSudo();
  }

  @AfterAll
  public void cleanResources() {
    //Delete all files used for test
    File nonIsoTestFile = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_TO_UPLOAD_NON_ISO_8859_1_NAME);
    File testPdf = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_TO_UPLOAD);
    File file2 = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_NAME2);
    File file3 = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_NAME3);
    try {
      FileTool.deleteFile(nonIsoTestFile.toPath());
      FileTool.deleteFile(testPdf.toPath());
      FileTool.deleteFile(file2.toPath());
      FileTool.deleteFile(file3.toPath());
    } catch (IOException e) {
      log.error("Error when deleting test files");
    }
  }
}
