/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - AbstractAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static ch.unige.aou.test.AouTestConstants.EVENT_MESSAGE_TEST;
import static ch.unige.aou.test.AouTestConstants.RESEARCH_GROUP_PERMANENT;
import static ch.unige.aou.test.AouTestConstants.STRUCTURE_PERMANENT;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.notification.Event;
import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.Comment;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.License;
import ch.unige.aou.model.settings.LicenseGroup;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructure;
import ch.unige.aou.model.xml.deposit.v2_4.AcademicStructures;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Collection;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.Contributors;
import ch.unige.aou.model.xml.deposit.v2_4.Correction;
import ch.unige.aou.model.xml.deposit.v2_4.Corrections;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DateWithType;
import ch.unige.aou.model.xml.deposit.v2_4.Dates;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;
import ch.unige.aou.model.xml.deposit.v2_4.DepositIdentifiers;
import ch.unige.aou.model.xml.deposit.v2_4.Languages;
import ch.unige.aou.model.xml.deposit.v2_4.ObjectFactory;
import ch.unige.aou.model.xml.deposit.v2_4.Text;
import ch.unige.aou.model.xml.deposit.v2_4.TextLang;
import ch.unige.aou.service.admin.CommentClientService;
import ch.unige.aou.service.admin.ContributorClientService;
import ch.unige.aou.service.admin.ContributorOtherNameClientService;
import ch.unige.aou.service.admin.DocumentFileTypeClientService;
import ch.unige.aou.service.admin.EventClientService;
import ch.unige.aou.service.admin.InstitutionClientService;
import ch.unige.aou.service.admin.LanguageClientService;
import ch.unige.aou.service.admin.NotificationClientService;
import ch.unige.aou.service.admin.ResearchGroupClientService;
import ch.unige.aou.service.admin.RoleClientService;
import ch.unige.aou.test.AbstractIT;
import ch.unige.aou.test.AouTestConstants;

public abstract class AbstractAdminIT extends AbstractIT {

  protected final String PUBLICATION_TEST_TITLE = this.getTemporaryTestLabel("Test publication");
  protected final String PUBLICATION_TEST_LANGUAGE = AouConstants.LANG_CODE_ENGLISH;
  protected final String PUBLICATION_TYPE_ARTICLE_ID = "ARTICLE";
  protected final String PUBLICATION_TYPE_ARTICLE_NAME = "Article";
  protected final String PUBLICATION_TYPE_DIPLOME_ID = "DIPLOME";
  protected final String PUBLICATION_TYPE_DIPLOME_NAME = "Diplôme";
  protected final String PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_ID = "A1";
  protected final String PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME = "Article scientifique";
  protected final String PUBLICATION_SUBTYPE_ARTICLE_PROFESSIONNEL_ID = "A2";
  protected final String PUBLICATION_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME = "Article professionnel";
  protected final String PUBLICATION_SUBTYPE_THESE_ID = "D1";
  protected final String PUBLICATION_SUBTYPE_THESE_NAME = "Thèse";
  protected final String PUBLICATION_SUBTYPE_MASTER_NAME = "Master";
  protected final String PUBLICATION_SUBSUBTYPE_LETTRE_ID = "A1-LETTRE";
  protected final String PUBLICATION_SUBSUBTYPE_LETTRE_NAME = "Lettre";
  protected final String TOP_STRUCTURE_NAME = this.getTemporaryTestLabel("Top structure");
  protected final String TOP_STRUCTURE_CN_STRUCTC = "59584763";
  protected final String TOP_STRUCTURE_CODE_STRUCTC = "AA6Z7U";
  protected final String DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE = "Article (Published version)";
  protected final String DOCUMENT_FILE_TYPE_TRANSLATION = "Translation";
  protected final String DOCUMENT_FILE_TYPE_MASTER_THESIS = "Master thesis";

  @Autowired
  protected CommentClientService commentClientService;

  @Autowired
  protected InstitutionClientService institutionService;

  @Autowired
  protected RoleClientService roleService;

  @Autowired
  protected NotificationClientService notificationService;

  @Autowired
  protected EventClientService eventService;

  @Autowired
  protected DocumentFileTypeClientService documentFileTypeService;

  @Autowired
  protected ResearchGroupClientService researchGroupService;

  @Autowired
  protected LanguageClientService languageService;

  @Autowired
  protected ContributorClientService contributorService;

  @Autowired
  protected ContributorOtherNameClientService contributorOtherNameService;

  protected ObjectFactory factory = new ObjectFactory();

  /***********************************/

  protected Person getLinkedPersonToUser(User user) {
    return this.personService.findOne(user.getPerson().getResId());
  }

  protected Structure getTopStructure() {
    final Map<String, String> properties = new HashMap<>();
    properties.put("name", this.TOP_STRUCTURE_NAME);
    List<Structure> structures = this.structureService.searchByProperties(properties);
    if (structures.size() == 1) {
      return structures.get(0);
    } else if (structures.isEmpty()) {
      throw new SolidifyRuntimeException("no structure with name '" + this.TOP_STRUCTURE_NAME + "' found");
    } else {
      throw new SolidifyRuntimeException("multiple structures with name '" + this.TOP_STRUCTURE_NAME + "' found");
    }
  }

  protected Publication getTestPublication() {
    return this.getTestPublication(this.PUBLICATION_TEST_TITLE);
  }

  protected Publication getTestPublication(String title) {
    final Map<String, String> properties = new HashMap<>();
    properties.put("title", title);
    List<Publication> publications = this.publicationService.searchByProperties(properties);
    if (publications.size() == 1) {
      return publications.get(0);
    } else if (publications.isEmpty()) {
      throw new SolidifyRuntimeException("no publication with title '" + this.PUBLICATION_TEST_TITLE + "' found");
    } else {
      throw new SolidifyRuntimeException("multiple publications with title '" + this.PUBLICATION_TEST_TITLE + "' found");
    }
  }

  protected DocumentFileType getDocumentFileType(String value) {
    final Map<String, String> properties = new HashMap<>();
    properties.put("value", value);
    List<DocumentFileType> documentFileTypes = this.documentFileTypeService.searchByProperties(properties);
    if (documentFileTypes.size() == 1) {
      return documentFileTypes.get(0);
    } else if (documentFileTypes.size() > 1) {
      throw new SolidifyRuntimeException("multiple DocumentFileType with value " + value + " found");
    } else {
      throw new SolidifyRuntimeException("no DocumentFileType with value " + value + " found");
    }
  }

  /***********************************/

  protected String getTestFormData(String title) {
    return String.format(AouConstants.TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE, title);
  }

  protected void createTopStructureFixture() {
    Structure topStructure = this.findStructureByName(this.TOP_STRUCTURE_NAME);
    if (topStructure == null) {
      topStructure = new Structure();
      topStructure.setName(this.TOP_STRUCTURE_NAME);
      topStructure.setCnStructC(this.TOP_STRUCTURE_CN_STRUCTC);
      topStructure.setCodeStruct(this.TOP_STRUCTURE_CODE_STRUCTC);
      topStructure.setDewey(this.TEST_DEWEY_CODE);
      topStructure.setSortValue(10);
      this.structureService.create(topStructure);
    }
  }

  protected Structure createRemoteStructure(String name) {
    Structure structure = new Structure();
    structure.setName(AouTestConstants.getRandomNameWithTemporaryLabel(name));
    structure.setCnStructC("234");
    structure.setCodeStruct("rts");
    structure.setDewey("113");
    structure.setSortValue(20);
    return this.structureService.create(structure);
  }

  protected Structure getStructure(Structure parentStructure, String name, String cnStructC, String codeStruct, String dewey,
          Structure.StructureStatus status, Integer sortValue, String acronym) {
    Structure structure = new Structure();
    structure.setParentStructure(parentStructure);
    structure.setName(name);
    structure.setCnStructC(cnStructC);
    structure.setCodeStruct(codeStruct);
    structure.setDewey(dewey);
    structure.setStatus(status);
    structure.setSortValue(sortValue);
    structure.setAcronym(acronym);
    return structure;
  }

  protected Structure createStructure(Structure parentStructure, String name, String cnStructC, String codeStruct, String dewey,
          Structure.StructureStatus status, Integer sortValue, String acronym) {
    Structure structure = this.getStructure(parentStructure, name, cnStructC, codeStruct, dewey, status, sortValue, acronym);
    return this.structureService.create(structure);
  }

  protected void createStructuresHierarchy() {
    /**
     * Creates the following structures hierarchy:
     *
     * --- top --- parent 1 --- child 11 --- child 12 --- child 13 --- parent 2 --- child 21 --- child
     * 22 --- child 23 --- parent 3
     */
    Structure topStructure = this.getTopStructure();

    Structure s1 = this.createStructure(topStructure, this.getTemporaryTestLabel("parent1"), "tabc", "t123", "123", null, 10, "TABC");
    Structure s2 = this.createStructure(topStructure, this.getTemporaryTestLabel("parent2"), "tdef", "t456", "234", null, 20, "TDEF");
    this.createStructure(topStructure, this.getTemporaryTestLabel("parent3"), "tghi", "t789", "345", null, 30, "TGHI");

    this.createStructure(s1, this.getTemporaryTestLabel("child11"), "tqwe", "t741", "456", null, 20, "TQWE");
    this.createStructure(s1, this.getTemporaryTestLabel("child12"), "tert", "t896", "567", null, 10, "TERT");
    this.createStructure(s1, this.getTemporaryTestLabel("child13"), "tiut", "t892", "123", null, 30, "TIUT");

    this.createStructure(s2, this.getTemporaryTestLabel("child21"), "tikl", "t326", "678", null, 50, "TIKL");
    this.createStructure(s2, this.getTemporaryTestLabel("child22"), "trko", "t485", "789", null, 40, "TRKO");
    this.createStructure(s2, this.getTemporaryTestLabel("child23"), "teop", "t159", "890", null, 30, "TEOP");
  }

  protected Structure findStructureByName(String name) {
    Map<String, String> searchProperties = new HashMap<>();
    searchProperties.put("name", name);
    List<Structure> foundStructures = this.structureService.searchByProperties(searchProperties);
    assertTrue(foundStructures.size() < 2);
    if (foundStructures.size() == 1) {
      return foundStructures.get(0);
    } else {
      return null;
    }
  }

  protected ResearchGroup createResearchGroupFixture(String name, String code) {
    return this.createResearchGroupFixture(name, code, true, true);
  }

  protected ResearchGroup createResearchGroupFixture(String name, String code, boolean randomName, boolean active) {
    ResearchGroup researchGroup = new ResearchGroup();
    if (randomName) {
      name = AouTestConstants.getRandomNameWithTemporaryLabel(name);
    } else {
      name = AouConstants.TEMPORARY_TEST_DATA_LABEL + name;
    }
    researchGroup.setName(name);
    researchGroup.setCode(code);
    researchGroup.setActive(active);
    return this.researchGroupService.create(researchGroup);
  }

  protected Publication createPublicationFixture() {
    return this.createPublicationFixture(this.PUBLICATION_TEST_TITLE);
  }

  protected Publication createPublicationFixture(String title) {
    Publication publication = this.createPublicationBase(title);
    return this.publicationService.create(publication);
  }

  protected Publication createPublicationWithContributor(String title, Map<String, Object> properties) {
    Publication publication = this.createPublicationBase(title);

    String xmlMetadata = this.getMetadata(properties);
    publication.setMetadata(xmlMetadata);
    return this.publicationService.create(publication);
  }

  protected Publication createPublicationBase(String title) {
    // publication subtype
    Publication publication = this.createPublicationWithSubType(title);

    // structure
    Structure structure = this.getTopStructure();

    publication.addStructure(structure);

    return publication;
  }

  protected void createCommentInPublicationTestFixture() {
    Publication publication = this.publicationService.findByTitle(this.PUBLICATION_TEST_TITLE);
    // Create a new Comment
    Comment comment = new Comment();
    comment.setText(this.getTemporaryTestLabel("comment test"));
    comment.setPublication(publication);
    comment.setOnlyForValidators(false);
    this.commentClientService.create(publication.getResId(), comment);
  }

  /***********************************/

  protected String getMetadata(Map<String, Object> properties) {

    String title = (String) properties.get("title");
    List<String> languagesList = (List<String>) properties.get("languages");
    String type = (String) properties.get("type");
    String subtype = (String) properties.get("subtype");
    String subSubtype = (String) properties.get("subSubtype");
    String isbn = (String) properties.get("isbn");
    String issn = (String) properties.get("issn");
    String urn = (String) properties.get("urn");
    String doi = (String) properties.get("doi");
    List<AbstractContributor> contributors = (List<AbstractContributor>) properties.get("contributors");
    List<Structure> structures = (List<Structure>) properties.get("structures");
    List<MetadataDates> metadataDates = (List<MetadataDates>) properties.get("dates");
    Map<String, Object> correctionProperties = (Map<String, Object>) properties.get("correction");
    Map<String, Object> collectionProperties = (Map<String, Object>) properties.get("collection");

    String originalTitle = (String) properties.get("originalTitle");
    String mandator = (String) properties.get("mandator");
    String edition = (String) properties.get("edition");
    String award = (String) properties.get("award");
    String editor = (String) properties.get("editor");
    String volume = (String) properties.get("volume");
    String conferencePlace = (String) properties.get("conferencePlace");
    String specialIssue = (String) properties.get("specialIssue");

    DepositDoc depositDoc = new DepositDoc();

    if (!StringTool.isNullOrEmpty(title)) {
      Text titleText = new Text();
      titleText.setContent(this.getTemporaryTestLabel(title));
      titleText.setLang(AouConstants.LANG_CODE_ENGLISH);
      depositDoc.setTitle(titleText);
    }

    if (languagesList != null) {
      Languages languages = new Languages();
      languages.getLanguage().addAll(languagesList);
      depositDoc.setLanguages(languages);
    }

    if (!StringTool.isNullOrEmpty(type)) {
      depositDoc.setType(type);
    }

    if (!StringTool.isNullOrEmpty(subtype)) {
      depositDoc.setSubtype(subtype);
    }

    if (!StringTool.isNullOrEmpty(subSubtype)) {
      depositDoc.setSubsubtype(subSubtype);
    }

    if (!StringTool.isNullOrEmpty(isbn)) {
      if (depositDoc.getIdentifiers() == null) {
        depositDoc.setIdentifiers(new DepositIdentifiers());
      }
      depositDoc.getIdentifiers().setIsbn(isbn);
    }

    if (!StringTool.isNullOrEmpty(issn)) {
      if (depositDoc.getIdentifiers() == null) {
        depositDoc.setIdentifiers(new DepositIdentifiers());
      }
      depositDoc.getIdentifiers().setIssn(issn);
    }

    if (!StringTool.isNullOrEmpty(urn)) {
      if (depositDoc.getIdentifiers() == null) {
        depositDoc.setIdentifiers(new DepositIdentifiers());
      }
      depositDoc.getIdentifiers().setUrn(urn);
    }

    if (!StringTool.isNullOrEmpty(doi)) {
      if (depositDoc.getIdentifiers() == null) {
        depositDoc.setIdentifiers(new DepositIdentifiers());
      }
      depositDoc.getIdentifiers().setDoi(doi);
    }

    if (metadataDates != null) {
      if (depositDoc.getDates() == null) {
        depositDoc.setDates(new Dates());
      }
      for (MetadataDates mdate : metadataDates) {
        DateWithType dateToAdd = new DateWithType();
        dateToAdd.setContent(mdate.getContent());
        dateToAdd.setType(DateTypes.fromValue(mdate.getType()));
        depositDoc.getDates().getDate().add(dateToAdd);
      }
    }

    if (contributors != null) {
      if (depositDoc.getContributors() == null) {
        depositDoc.setContributors(new Contributors());
      }

      for (AbstractContributor abstractContributor : contributors) {
        if (abstractContributor instanceof ContributorDTO) {
          Contributor contributor = new Contributor();
          contributor.setFirstname(((ContributorDTO) abstractContributor).getFirstName());
          contributor.setLastname(((ContributorDTO) abstractContributor).getLastName());
          if (!StringTool.isNullOrEmpty(((ContributorDTO) abstractContributor).getRole())) {
            contributor.setRole(AuthorRole.fromValue(((ContributorDTO) abstractContributor).getRole()));
          }
          contributor.setCnIndividu(((ContributorDTO) abstractContributor).getCnIndividu());
          depositDoc.getContributors().getContributorOrCollaboration().add(contributor);
        } else {
          Collaboration collaboration = new Collaboration();
          collaboration.setName(((CollaborationDTO) abstractContributor).getName());
          depositDoc.getContributors().getContributorOrCollaboration().add(collaboration);
        }
      }
    }

    if (structures != null) {
      if (depositDoc.getAcademicStructures() == null) {
        depositDoc.setAcademicStructures(new AcademicStructures());
      }

      for (Structure structure : structures) {
        AcademicStructure academicStructure = new AcademicStructure();
        academicStructure.setResId(structure.getResId());
        academicStructure.setCode(structure.getCodeStruct());
        academicStructure.setName(structure.getName());
        depositDoc.getAcademicStructures().getAcademicStructure().add(academicStructure);
      }
    }

    if (correctionProperties != null) {
      if (depositDoc.getCorrections() == null) {
        depositDoc.setCorrections(new Corrections());
      }
      Correction correction = new Correction();
      correction.setNote(correctionProperties.get("note").toString());
      if (correctionProperties.get("doi") != null) {
        correction.setDoi(correctionProperties.get("doi").toString());
      }
      if (correctionProperties.get("pmid") != null) {
        correction.setPmid(correctionProperties.get("pmid").toString());
      }
      depositDoc.getCorrections().getCorrection().add(correction);
    }

    if (collectionProperties != null) {
      if (depositDoc.getCollections() == null) {
        depositDoc.setCollections(new ch.unige.aou.model.xml.deposit.v2_4.Collections());
      }
      Collection collection = new Collection();
      if (collectionProperties.get("name") != null) {
        collection.setName(collectionProperties.get("name").toString());
      }
      if (collectionProperties.get("number") != null) {
        collection.setNumber(collectionProperties.get("number").toString());
      }
      depositDoc.getCollections().getCollection().add(collection);
    }

    if (!StringTool.isNullOrEmpty(originalTitle)) {
      TextLang originalTitleText = new TextLang();
      originalTitleText.setContent(originalTitle);
      originalTitleText.setLang(AouConstants.LANG_CODE_ENGLISH);
      depositDoc.setOriginalTitle(originalTitleText);
    }

    if (!StringTool.isNullOrEmpty(mandator)) {
      depositDoc.setMandator(mandator);
    }

    if (!StringTool.isNullOrEmpty(edition)) {
      depositDoc.setEdition(edition);
    }

    if (!StringTool.isNullOrEmpty(award)) {
      depositDoc.setAward(award);
    }

    if (!StringTool.isNullOrEmpty(editor)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setEditor(editor);
    }

    if (!StringTool.isNullOrEmpty(volume)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setVolume(volume);
    }

    if (!StringTool.isNullOrEmpty(conferencePlace)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setConferencePlace(conferencePlace);
    }

    if (!StringTool.isNullOrEmpty(specialIssue)) {
      this.ensureContainerExist(depositDoc);
      depositDoc.getContainer().setSpecialIssue(specialIssue);
    }

    return this.serializeDepositDocToXml(depositDoc);
  }

  protected void ensureContainerExist(DepositDoc depositDoc) {
    if (depositDoc.getContainer() == null) {
      depositDoc.setContainer(this.factory.createContainer());
    }
  }

  protected String serializeDepositDocToXml(DepositDoc depositDoc) {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(DepositDoc.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(depositDoc, sw);
      return sw.toString();

    } catch (Exception e) {
      throw new SolidifyRuntimeException("unable to serialize DepositDoc to XML", e);
    }
  }

  protected DepositDoc createDepositDocFromXml(String xmlData) {
    return JAXB.unmarshal(new StringReader(xmlData), DepositDoc.class);
  }

  /***********************************/

  protected void clearLicenceFixtures() {
    final List<License> licenses = this.licenseService.findAll();

    for (final License license : licenses) {
      if (license.getTitle().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.licenseService.delete(license.getResId());
      }
    }
  }

  protected void clearLicenceGroupFixtures() {
    final Map<String, String> properties = new HashMap<>();
    properties.put("name", AouConstants.TEMPORARY_TEST_DATA_LABEL);
    final List<LicenseGroup> licenseGroups = this.licenseGroupService.searchByProperties(properties);

    for (final LicenseGroup licenseGroup : licenseGroups) {
      this.licenseGroupService.delete(licenseGroup.getResId());
    }
  }

  protected void clearPublicationSubSubtypeFixtures() {
    final List<PublicationSubtype> subtypes = this.publicationSubtypeService.findAll();
    for (PublicationSubtype subtype : subtypes) {
      final List<PublicationSubSubtype> subSubtypes = this.publicationSubSubtypeService.findAll(subtype.getResId());
      for (PublicationSubSubtype subSubtype : subSubtypes) {
        if (subtype.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
          this.publicationSubSubtypeService.delete(subtype.getResId(), subSubtype.getResId());
        }
      }
    }
  }

  protected void clearCommentFixtures() {

    final Map<String, String> properties = new HashMap<>();
    properties.put("title", AouConstants.TEMPORARY_TEST_DATA_LABEL);
    final List<Publication> testPublications = this.publicationService.searchByProperties(properties);

    for (Publication publication : testPublications) {
      final List<Comment> comments = this.commentClientService.findAll(publication.getResId());
      for (Comment comment : comments) {
        if (comment.getText().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
          this.commentClientService.delete(publication.getResId(), comment.getResId());
        }
      }
    }
  }

  protected void clearStructureFixtures() {

    final Map<String, String> properties = new HashMap<>();
    properties.put("name", AouConstants.TEMPORARY_TEST_DATA_LABEL);
    final List<Structure> testStructures = this.structureService.searchByProperties(properties);

    /**
     * delete child structures first to prevent foreign key constraint errors in database
     */
    final List<Structure> childTestStructures = testStructures.stream().filter(s -> s.getParentStructure() != null)
            .collect(Collectors.toList());
    final List<Structure> topTestStructures = testStructures.stream().filter(s -> s.getParentStructure() == null).collect(Collectors.toList());
    final List<Structure> structures = new ArrayList<>();
    structures.addAll(childTestStructures);
    structures.addAll(topTestStructures);

    for (Structure structure : structures) {
      List<Person> personList = this.structureService.getPeople(structure.getResId());
      for (Person person : personList) {
        this.structureService.removePerson(structure.getResId(), person.getResId());
      }
      this.structureService.delete(structure.getResId());
    }
  }

  /**
   * Manual delete of 3rd level test structures created by createStructuresHierarchy() to prevent
   * errors when clearing fixtures
   */
  protected void deleteGrandChildrenStructures() {

    List<String> structureIds = new ArrayList<>();

    Structure s11 = this.findStructureByName(this.getTemporaryTestLabel("child11"));
    this.addStructureIdToList(structureIds, s11);
    Structure s12 = this.findStructureByName(this.getTemporaryTestLabel("child12"));
    this.addStructureIdToList(structureIds, s12);
    Structure s13 = this.findStructureByName(this.getTemporaryTestLabel("child13"));
    this.addStructureIdToList(structureIds, s13);
    Structure s21 = this.findStructureByName(this.getTemporaryTestLabel("child21"));
    this.addStructureIdToList(structureIds, s21);
    Structure s22 = this.findStructureByName(this.getTemporaryTestLabel("child22"));
    this.addStructureIdToList(structureIds, s22);
    Structure s23 = this.findStructureByName(this.getTemporaryTestLabel("child23"));
    this.addStructureIdToList(structureIds, s23);

    String[] ids = new String[structureIds.size()];
    structureIds.toArray(ids);
    this.structureService.deleteList(ids);
  }

  private void addStructureIdToList(List<String> structureIds, Structure structure) {
    if (structure != null) {
      structureIds.add(structure.getResId());
    }
  }

  protected void clearResearchGroupFixtures() {

    final List<ResearchGroup> testResearchGroups = this.researchGroupService.findAll();

    for (ResearchGroup researchGroup : testResearchGroups) {
      if (researchGroup.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
        // delete links to people
        List<Person> personList = this.researchGroupService.getPeople(researchGroup.getResId());
        for (Person person : personList) {
          this.researchGroupService.removePerson(researchGroup.getResId(), person.getResId());
        }
        this.researchGroupService.delete(researchGroup.getResId());
      }
    }
  }

  protected void clearUserFixtures() {
    final List<User> users = this.userService.findAll();
    for (final User u : users) {
      if (u.getFirstName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.userService.delete(u.getResId());
      }
    }
  }

  protected void clearRoleFixtures() {
    this.roleService.findAll().stream()
            .filter(role -> role.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(role -> this.roleService.delete(role.getResId()));
  }

  protected void clearNotificationFixtures() {
    final List<Notification> notificationList = this.notificationService.findAll();

    for (final Notification notification : notificationList) {
      if (notification.getMessage().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.notificationService.delete(notification.getResId());
      }
    }
  }

  protected void clearRolesInStructuresOrResearchGroupsFixtures() {
    // delete the person linked to this user from permanent structure and researchgroup
    List<ResearchGroup> researchGroupList = this.researchGroupService
            .searchByProperties(Collections.singletonMap("name", RESEARCH_GROUP_PERMANENT));
    if (!researchGroupList.isEmpty()) {
      // Remove all the person linked to this permanent research group (in order to start clean for the
      // following test)
      this.researchGroupService.getPeople(researchGroupList.get(0).getResId())
              .forEach(p -> this.researchGroupService.removePerson(researchGroupList.get(0).getResId(), p.getResId()));
    }

    List<Structure> structureList = this.structureService.searchByProperties(Collections.singletonMap("name", STRUCTURE_PERMANENT));
    if (!structureList.isEmpty()) {
      // Remove all the person linked to this permanent structure (in order to start clean for the
      // following test)
      this.structureService.getPeople(structureList.get(0).getResId())
              .forEach(p -> this.structureService.removePerson(structureList.get(0).getResId(), p.getResId()));
    }
  }

  protected void clearEventsFixtures() {
    List<Event> eventList = this.eventService.searchByProperties(Collections.singletonMap("message", EVENT_MESSAGE_TEST));
    if (!eventList.isEmpty()) {
      eventList.forEach(e -> this.eventService.delete(e.getResId()));
    }
  }

  protected void clearPersonNotificationTypes() {
    User myUser = this.userService.getAuthenticatedUser();
    Person myPerson = this.getLinkedPersonToUser(myUser);
    List<String> mandatoryNotificationTypeList = this.notificationTypeClientService.getListNotificationsTypesMandatory();

    List<NotificationType> notifications = this.personService.getSubscribedNotificationTypes(myPerson.getResId());
    for (NotificationType ntp : notifications) {
      if (!mandatoryNotificationTypeList.contains(ntp.getResId())) {
        this.personService.removeNotificationType(myPerson.getResId(), ntp.getResId());
      }
    }
  }

  protected void clearContributorFixtures() {
    List<ch.unige.aou.model.publication.Contributor> contributorList = this.contributorService
            .searchByProperties(Collections.singletonMap("lastName", AouConstants.TEMPORARY_TEST_DATA_LABEL));

    for (final ch.unige.aou.model.publication.Contributor contributor : contributorList) {
      this.contributorService.delete(contributor.getResId());
    }

    contributorList = this.contributorService.searchByProperties(
            Collections.singletonMap("lastName", AouConstants.TEMPORARY_TEST_DATA_LABEL.replace(" ", "_")));

    for (final ch.unige.aou.model.publication.Contributor contributor : contributorList) {
      this.contributorService.delete(contributor.getResId());
    }
  }

  public DocumentFile createDocumentFileForPublication(Publication publication, DocumentFile.AccessLevel accessLevel) {
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setDocumentFileType(documentFileType);
    documentFile.setSourceData(URI.create(TEST_FILE_URL));
    documentFile.setAccessLevel(accessLevel);

    return this.publicationDocumentFileService.create(publication.getResId(), documentFile);
  }

}
