/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PersonAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.util.SearchOperation;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.test.AouTestConstants;

class PersonAsAdminIT extends PersonIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void creationTest() {
    Person person1 = this.createPerson("Jean-Claude", "Dusse", false);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personService.create(person1);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void deleteTest() {
    Person person1 = this.createPerson("Jean-Claude", "Dusse", false);
    String personId = person1.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.personService.delete(personId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void searchTest() {
    // Create a person
    Person person = new Person();
    person.setFirstName(AouTestConstants.getRandomNameWithTemporaryLabel("SearchFirstName"));
    person.setLastName(AouTestConstants.getRandomNameWithTemporaryLabel("SearchLastName"));
    this.restClientTool.sudoRoot();
    person = this.personService.create(person);
    this.restClientTool.exitSudo();

    /*
     * Test case sensitive research REM: as the database stores firstname and lastname in
     * utf8_general_ci, the searchTerm finds results even with wrong case
     */
    String searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("firstName~%s, lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Test case insensitive research
     */
    searchTerm = "archfirs"; // substring of firstname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
    searchTerm = "archlas"; // substring of lastname
    assertTrue(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));

    /*
     * Search on both firstName AND lastName (by default on backend)
     */
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), null, person));

    /*
     * search on not existing term
     */
    searchTerm = "--notexisting--";
    assertFalse(this.found(String.format("i-firstName~%s, i-lastName~%s", searchTerm, searchTerm), SearchOperation.OR_PREDICATE_FLAG, person));
  }

  @Test
  void updateTest() {
    // Create a person
    Person person1 = this.createPerson("Jérôme", "Tarayre", false);
    this.restClientTool.sudoRoot();
    person1 = this.personService.create(person1);
    this.restClientTool.exitSudo();

    // Update the person
    final Person person2 = this.createPerson("Robert", "Lespinass", true);
    final Person person3 = this.personService.update(person1.getResId(), person2);

    // Test the update
    assertEquals(person2.getFirstName(), person3.getFirstName());
    assertEquals(person2.getLastName(), person3.getLastName());
    assertEquals(person2.getOrcid(), person3.getOrcid());
  }

  @Test
  void addStructureToPersonTest() {
    final Structure structure = this.createRemoteStructure("Structure 1");
    Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int structuresBeforeAdding = this.personService.getStructures(person.getResId()).size();

    // Add the structure to the person with default roles
    this.personService.addStructure(person.getResId(), structure.getResId());
    final List<Structure> structuresList = this.personService.getStructures(person.getResId());

    assertEquals(structuresBeforeAdding + 1, structuresList.size());

    // Test added structure to the Person
    for (final Structure structure1 : structuresList) {
      if (structure1.getResId().equals(structure.getResId())) {
        assertNotNull(structure1);
        assertEquals(structure.getName(), structure1.getName());
        assertEquals(structure.getCnStructC(), structure1.getCnStructC());
        assertEquals(structure.getCodeStruct(), structure1.getCodeStruct());
        assertEquals(structure.getDewey(), structure1.getDewey());
      }
    }
  }

  @Test
  void addAndRemoveRoleOnPersonTest() {
    this.restClientTool.sudoRoot();
    Person person = this.createPerson("Jean-Claude", "Dusse", false);
    Person createdPerson = this.personService.create(person);
    this.restClientTool.exitSudo();

    // New Person hasn't any Role
    JsonNode personJsonNode = this.personService.findOneAsJsonNode(createdPerson.getResId());
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    JsonNode roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(((ArrayNode) roleIdsNode).isEmpty());

    // The Person hasn't any Role anymore
    personJsonNode = this.personService.findOneAsJsonNode(createdPerson.getResId());
    assertNotNull(personJsonNode);
    assertTrue(personJsonNode.has(this.ROLE_IDS_PROPERTY));
    roleIdsNode = personJsonNode.get(this.ROLE_IDS_PROPERTY);
    assertTrue(roleIdsNode.isArray());
    assertTrue(((ArrayNode) roleIdsNode).isEmpty());
  }
}
