/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - StructureIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;

import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.service.admin.StructureClientService;

class StructureIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Override
  protected void createFixtures() {
    this.clearStructureFixtures();
    this.createTopStructureFixture();
  }

  @Test
  void createTest() {
    /**
     * Create top structure
     */
    String name = this.getTemporaryTestLabel("Another top structure");
    String cnStructC = "741";
    String cStruct = "rtz";
    String dewey = "112";
    String acronym = "UN";
    Structure createdTopStructure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, 0, acronym);
    assertNotNull(createdTopStructure);
    assertEquals(name, createdTopStructure.getName());
    assertEquals(cnStructC, createdTopStructure.getCnStructC());
    assertEquals(cStruct, createdTopStructure.getCodeStruct());
    assertEquals(acronym, createdTopStructure.getAcronym());

    /**
     * Create child structure
     */
    String name2 = this.getTemporaryTestLabel("A child structure");
    String cnStructC2 = "456";
    String cStruct2 = "def";
    String dewey2 = "110";
    String acronym2 = "PUN";
    Structure createdChildStructure = this.createStructure(createdTopStructure, name2, cnStructC2, cStruct2, dewey2, null, 0, acronym2);
    assertNotNull(createdChildStructure);
    assertEquals(name2, createdChildStructure.getName());
    assertEquals(cnStructC2, createdChildStructure.getCnStructC());
    assertEquals(cStruct2, createdChildStructure.getCodeStruct());
    assertNotNull(createdChildStructure.getParentStructure());
    assertEquals(createdTopStructure.getResId(), createdChildStructure.getParentStructure().getResId());
    assertEquals(name, createdChildStructure.getParentStructure().getName());
    assertEquals(cnStructC, createdChildStructure.getParentStructure().getCnStructC());
    assertEquals(cStruct, createdChildStructure.getParentStructure().getCodeStruct());
    assertEquals(acronym2, createdChildStructure.getAcronym());
  }

  @Test
  void createSameValueTest() {

    Structure topStructure = this.getTopStructure();

    /**
     * Try to create another structure with same CnStructC
     */
    String temporaryTestLabel = this.getTemporaryTestLabel("A 2nd Structure");
    String structureCnStructC = topStructure.getCnStructC();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, temporaryTestLabel, structureCnStructC, "def", "200", null, 0, "STR");
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("This value already exists"));
    assertTrue(e.getResponseBodyAsString().contains("\"fieldName\":\"cnStructC\""));

    /**
     * Try to create another structure with same CnStructC
     */
    String temporaryTestLabel3 = this.getTemporaryTestLabel("A 3rd Structure");
    String topStructureCode = topStructure.getCodeStruct();
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, temporaryTestLabel3, "456", topStructureCode, "300", null, 0, "STR1");
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("This value already exists"));
    assertTrue(e.getResponseBodyAsString().contains("\"fieldName\":\"cStruct\""));

    /**
     * Try to create another structure with same name at the top
     */
    String topStructureName = topStructure.getName();
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, topStructureName, "456", "def", "400", null, 0, "STR2");
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("This name already exists for a top structure"));

    /**
     * Try to create another structure with same name for a parent structure
     */
    String sameName = this.getTemporaryTestLabel("Child Structure");
    Structure createdChildStructure1 = this.createStructure(topStructure, sameName, "456", "def", "110", null, 0, "STR3");
    assertNotNull(createdChildStructure1);
    assertEquals(topStructure.getName(), createdChildStructure1.getParentStructure().getName());
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(topStructure, sameName, "003", "emy", "180", null, 0, "PE");
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("This name already exists in the parent structure"));
  }

  @Test
  void createSameNameAtTopWithObsoleteStatusTest() {
    Structure topStructure = this.getTopStructure();

    /**
     * Try to create another structure with same name but obsolete status at the top
     */
    Structure createdTopStructure = this
            .createStructure(null, topStructure.getName(), "456", "def", "400", Structure.StructureStatus.OBSOLETE, 0, "STR4");
    assertNotNull(createdTopStructure);
    assertEquals(topStructure.getName(), createdTopStructure.getName());
    assertEquals(Structure.StructureStatus.OBSOLETE, createdTopStructure.getStatus());

    /**
     * Create another top structure with a new name and obsolete
     */
    String newName = this.getTemporaryTestLabel("Une faculté");

    Structure createdTopStructure3 = this.createStructure(null, newName, "789", "poi", "565", Structure.StructureStatus.OBSOLETE, 0, "STR5");
    assertNotNull(createdTopStructure3);
    assertEquals(newName, createdTopStructure3.getName());
    assertEquals(Structure.StructureStatus.OBSOLETE, createdTopStructure3.getStatus());

    /**
     * Create another top structure with the same name but not obsolete
     */
    Structure createdTopStructure4 = this.createStructure(null, newName, "147", "iku", "123", null, 0, "STR6");
    assertNotNull(createdTopStructure4);
    assertEquals(newName, createdTopStructure4.getName());
    assertEquals(Structure.StructureStatus.ACTIVE, createdTopStructure4.getStatus());

    /**
     * try again, this time it should fail
     */
    try {
      this.createStructure(null, newName, "486", "efbn", "444", null, 0, "STR7");
      // this.structureService.create(structure5);
      fail("A BAD_REQUEST Http response should be received. Unique constraint on 'name' not satisfied");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      assertTrue(e.getResponseBodyAsString().contains("This name already exists for a top structure"));
    }
  }

  @Test
  void createSameNameAsChildWithObsoleteStatusTest() {
    Structure topStructure = this.getTopStructure();

    /**
     * Create a structure with a new name
     */
    String newName1 = this.getTemporaryTestLabel("Child1");
    Structure createdChildStructure1 = this.createStructure(topStructure, newName1, "4567", "defsd", "487", null, 0, "ADD");
    assertNotNull(createdChildStructure1);
    assertEquals(newName1, createdChildStructure1.getName());
    assertEquals(Structure.StructureStatus.ACTIVE, createdChildStructure1.getStatus());

    /**
     * Create another structure with the same name but obsolete status
     */
    Structure createdChildStructure2 = this
            .createStructure(topStructure, newName1, "456", "def", "400", Structure.StructureStatus.OBSOLETE, 0, "BCD");
    assertNotNull(createdChildStructure2);
    assertEquals(newName1, createdChildStructure2.getName());
    assertEquals(Structure.StructureStatus.OBSOLETE, createdChildStructure2.getStatus());

    /****************************************************************************************/

    /**
     * Create a structure with a new name and obsolete status
     */
    String newName2 = this.getTemporaryTestLabel("Child 2");
    Structure createdTopStructure3 = this
            .createStructure(topStructure, newName2, "1112", "oopo", "888", Structure.StructureStatus.OBSOLETE, 0, "DEF");
    assertNotNull(createdTopStructure3);
    assertEquals(newName2, createdTopStructure3.getName());
    assertEquals(Structure.StructureStatus.OBSOLETE, createdTopStructure3.getStatus());

    /**
     * Create another top structure with the same name but not obsolete
     */
    Structure createdTopStructure4 = this.createStructure(topStructure, newName2, "7878", "wewe", "888", null, 0, "HUG");
    assertNotNull(createdTopStructure4);
    assertEquals(newName2, createdTopStructure4.getName());
    assertEquals(Structure.StructureStatus.ACTIVE, createdTopStructure4.getStatus());

    /**
     * try again, this time it should fail
     */
    try {
      this.createStructure(topStructure, newName2, "999", "eeer", "888", null, 0, "RMN");
      // this.structureService.create(structure5);
      fail("A BAD_REQUEST Http response should be received. Unique constraint on 'name' not satisfied");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      assertTrue(e.getResponseBodyAsString().contains("This name already exists in the parent structure"));
    }
  }

  @Test
  void parentStructureTest() {
    Structure topStructure = this.getTopStructure();

    Structure s1 = this.createStructure(topStructure, this.getTemporaryTestLabel("child1"), "tabc", "t123", "111", null, 0, "TABC");
    Structure s2 = this.createStructure(topStructure, this.getTemporaryTestLabel("child2"), "tdef", "t456", "222", null, 0, "TDEF");
    Structure s3 = this.createStructure(topStructure, this.getTemporaryTestLabel("child3"), "tghi", "t789", "333", null, 0, "TGHI");
    assertNotNull(s1);
    assertEquals(topStructure.getResId(), s1.getParentStructure().getResId());
    assertNotNull(s2);
    assertEquals(topStructure.getResId(), s2.getParentStructure().getResId());
    assertNotNull(s3);
    assertEquals(topStructure.getResId(), s3.getParentStructure().getResId());

    Structure s11 = this.createStructure(s1, this.getTemporaryTestLabel("child11"), "tqwe", "t741", "444", null, 0, "TQWE");
    Structure s12 = this.createStructure(s1, this.getTemporaryTestLabel("child12"), "tert", "t896", "555", null, 0, "TERT");
    Structure s13 = this.createStructure(s1, this.getTemporaryTestLabel("child13"), "tiut", "t892", "666", null, 0, "TIUT");
    assertNotNull(s11);
    assertEquals(s1.getResId(), s11.getParentStructure().getResId());
    assertNotNull(s12);
    assertEquals(s1.getResId(), s12.getParentStructure().getResId());
    assertNotNull(s13);
    assertEquals(s1.getResId(), s13.getParentStructure().getResId());

    Structure s21 = this.createStructure(s2, this.getTemporaryTestLabel("child21"), "tikl", "t326", "777", null, 0, "TIKL");
    Structure s22 = this.createStructure(s2, this.getTemporaryTestLabel("child22"), "trko", "t485", "888", null, 0, "TRKO");
    Structure s23 = this.createStructure(s2, this.getTemporaryTestLabel("child23"), "teop", "t159", "999", null, 0, "TEOP");
    assertNotNull(s21);
    assertEquals(s2.getResId(), s21.getParentStructure().getResId());
    assertNotNull(s22);
    assertEquals(s2.getResId(), s22.getParentStructure().getResId());
    assertNotNull(s23);
    assertEquals(s2.getResId(), s23.getParentStructure().getResId());

    /**
     * --- top --- child 1 --- child 11 --- child 12 --- child 13 --- child 2 --- child 21 --- child 22
     * --- child 23 --- child 3
     */

    try {

      /**
       * child 1 cannot have itself as parent
       */
      String s1Id = s1.getResId();
      HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
        ((StructureClientService) this.structureService)
                .update(s1Id, "{\"parentStructure\" : {\"resId\" : \"" + s1Id + "\"} }");
      });
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      assertTrue(e.getResponseBodyAsString().contains("The parent structure is not valid for this structure"));

      /**
       * child 1 cannot have child 11 as parent
       */
      s1.setParentStructure(s11);
      e = assertThrows(HttpClientErrorException.class, () -> {
        this.structureService.update(s1Id, s1);
      });
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
      assertTrue(e.getResponseBodyAsString().contains("The parent structure is not valid for this structure"));

      /**
       * child 1 can have child 21 as parent
       */
      s1.setParentStructure(s21);
      Structure savedS1 = this.structureService.update(s1.getResId(), s1);
      assertNotNull(savedS1);
      assertNotNull(savedS1.getParentStructure());
      assertEquals(savedS1.getParentStructure().getResId(), s21.getResId());

      /**
       * child 1 can be reset to have no parent
       */
      s1.setParentStructure(null);
      savedS1 = this.structureService.update(s1.getResId(), s1);
      assertNull(savedS1.getParentStructure());

    } finally {

      /**
       * Manual delete of 3rd level test structures to prevent errors when clearing fixtures
       */
      this.structureService
              .deleteList(new String[] { s11.getResId(), s12.getResId(), s13.getResId(), s21.getResId(), s22.getResId(), s23.getResId() });
    }
  }

  @Test
  void findAllTest() {
    /**
     * Create 2nd top structure
     */
    Structure createdTopStructure = this
            .createStructure(null, this.getTemporaryTestLabel("Another top structure"), "741", "rtz", "112", null, 0, "RTZ");
    assertNotNull(createdTopStructure);
    List<Structure> structures = this.structureService.findAll();
    assertTrue(structures.size() >= 2);
  }

  @Test
  void findOneTest() {

    Structure topStructure = this.getTopStructure();

    /**
     * Create child structure
     */
    String name = this.getTemporaryTestLabel("Child structure");
    String cnStructC = "4567";
    String cStruct = "defr";
    String dewey = "12345.456";
    String acronym = "ABC";
    Structure createdChildStructure = this.createStructure(topStructure, name, cnStructC, cStruct, dewey, null, 0, acronym);
    assertNotNull(createdChildStructure);

    /**
     * Find created structure
     */
    Structure structureFound = this.structureService.findOne(createdChildStructure.getResId());
    assertNotNull(structureFound);
    assertEquals(name, structureFound.getName());
    assertEquals(cnStructC, structureFound.getCnStructC());
    assertEquals(cStruct, structureFound.getCodeStruct());
    assertEquals(dewey, structureFound.getDewey());
    assertNotNull(createdChildStructure.getParentStructure());
    assertEquals(topStructure.getResId(), structureFound.getParentStructure().getResId());
    assertEquals(topStructure.getName(), structureFound.getParentStructure().getName());
    assertEquals(topStructure.getCnStructC(), structureFound.getParentStructure().getCnStructC());
    assertEquals(topStructure.getCodeStruct(), structureFound.getParentStructure().getCodeStruct());
    assertEquals(topStructure.getAcronym(), structureFound.getParentStructure().getAcronym());

  }

  @Test
  void updateStructureTest() {
    String newName = this.getTemporaryTestLabel("updated name");

    Structure topStructure = this.getTopStructure();
    topStructure.setName(newName);

    /**
     * Update top structure
     */
    Structure savedStructure = this.structureService.update(topStructure.getResId(), topStructure);
    assertNotNull(savedStructure);
    assertEquals(topStructure.getResId(), savedStructure.getResId());
    assertEquals(newName, savedStructure.getName());
    assertEquals(topStructure.getCnStructC(), savedStructure.getCnStructC());
    assertEquals(topStructure.getCodeStruct(), savedStructure.getCodeStruct());
    assertEquals(topStructure.getAcronym(), savedStructure.getAcronym());

    Structure structureFound = this.structureService.findOne(topStructure.getResId());
    assertNotNull(structureFound);
    assertEquals(topStructure.getResId(), structureFound.getResId());
    assertEquals(newName, structureFound.getName());
    assertEquals(topStructure.getCnStructC(), structureFound.getCnStructC());
    assertEquals(topStructure.getCodeStruct(), structureFound.getCodeStruct());
    assertEquals(topStructure.getAcronym(), savedStructure.getAcronym());

    /**
     * Create child structure
     */
    // Structure childStructure = new Structure();
    // childStructure.setName(this.getTemporaryTestLabel("Child Structure"));
    // childStructure.setCnStructC("456");
    // childStructure.setCodeStruct("def");
    // childStructure.setDewey("110");
    // childStructure.setParentStructure(topStructure);
    // Structure createdChildStructure = this.structureService.create(childStructure);
    Structure createdChildStructure = this
            .createStructure(topStructure, this.getTemporaryTestLabel("Child Structure"), "456", "def", "110", null, 0, "DEF");
    assertNotNull(createdChildStructure);

    /**
     * Update child structure
     */
    createdChildStructure.setName(newName);
    Structure savedChildStructure = this.structureService.update(createdChildStructure.getResId(), createdChildStructure);
    assertNotNull(savedChildStructure);
    Structure childStructureFound = this.structureService.findOne(savedChildStructure.getResId());
    assertNotNull(childStructureFound);
    assertEquals(savedChildStructure.getResId(), childStructureFound.getResId());
    assertEquals(newName, childStructureFound.getName());
    assertEquals(savedChildStructure.getCnStructC(), childStructureFound.getCnStructC());
    assertEquals(savedChildStructure.getCodeStruct(), childStructureFound.getCodeStruct());
    assertEquals(savedChildStructure.getParentStructure().getResId(), childStructureFound.getParentStructure().getResId());
    assertEquals(savedChildStructure.getAcronym(), childStructureFound.getAcronym());

  }

  @Test
  void deleteStructureTest() {
    Structure topStructure = this.getTopStructure();
    assertNotNull(topStructure);

    this.structureService.delete(topStructure.getResId());

    String resId = topStructure.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.structureService.findOne(resId));
  }

  @Test
  void deleteAllStructureTest() {

    /**
     * Create top structure 1
     */
    Structure createdTopStructure1 = this.createStructure(null, this.getTemporaryTestLabel("structure 1"), "741", "rtz", "112", null, 0, "RTZ");
    assertNotNull(createdTopStructure1);

    /**
     * Create top structure 2
     */
    Structure createdTopStructure2 = this
            .createStructure(null, this.getTemporaryTestLabel("structure 2"), "789", "ziui", "115", null, 0, "ZIUI");
    assertNotNull(createdTopStructure2);

    /**
     * Find before delete
     */
    Structure topStructure = this.getTopStructure();
    assertNotNull(topStructure);
    Structure structure1 = this.structureService.findOne(createdTopStructure1.getResId());
    assertNotNull(structure1);
    Structure structure2 = this.structureService.findOne(createdTopStructure2.getResId());
    assertNotNull(structure2);

    /**
     * Delete
     */
    String[] ids = Arrays.asList(structure1.getResId(), structure2.getResId()).toArray(new String[0]);
    this.structureService.deleteList(ids);

    /**
     * Find after delete
     */
    topStructure = this.getTopStructure();
    assertNotNull(topStructure);

    final String resId1 = structure1.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.structureService.findOne(resId1));
    final String resId2 = structure2.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.structureService.findOne(resId2));
  }

  @Test
  void addPersonToStructureTest() {
    final Structure structure = this.createRemoteStructure("Structure to add");
    final Person person = this.getPermanentPerson(AuthApplicationRole.USER);
    final int peopleBeforeAdding = this.structureService.getPeople(structure.getResId()).size();

    // Add the person to the Structure with default role
    this.structureService.addPerson(structure.getResId(), person.getResId());

    final List<Person> people = this.structureService.getPeople(structure.getResId());

    assertEquals(people.size(), peopleBeforeAdding + 1);

    // Test added person on the OrganizationalUnit
    for (final Person person2 : people) {
      if (person2.getResId().equals(person.getResId())) {
        assertNotNull(person2);
        assertEquals(person.getFirstName(), person2.getFirstName());
        assertEquals(person.getLastName(), person2.getLastName());
        assertEquals(person.getOrcid(), person2.getOrcid());
      }
    }
  }

  @Test
  void removePersonFromStructureTest() {
    final Structure structure = this.createRemoteStructure("Structure to delete");
    final Person person = this.getPermanentPerson(AuthApplicationRole.USER);

    // Add the person to the structure with default role
    this.structureService.addPerson(structure.getResId(), person.getResId());
    List<Person> people = this.structureService.getPeople(structure.getResId());

    final int nbPepole = people.size();
    this.structureService.removePerson(structure.getResId(), person.getResId());
    people = this.structureService.getPeople(structure.getResId());
    assertEquals(nbPepole - 1, people.size());
  }

  @Test
  void validateDeweyTest() {
    int index = 123456;

    String name = this.getTemporaryTestLabel("dewey structure " + index);
    String cnStructC = String.valueOf(index);
    String cStruct = String.valueOf(index++);
    String dewey = "112";
    String acronym = "ABC";
    Structure structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112.456";
    acronym = "WM";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112.456.678";
    acronym = "SAP";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112/456";
    acronym = "SU";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112.456/789";
    acronym = "TOP";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112.456 / 789";
    acronym = "MN";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112.456/789.986";
    acronym = "PN";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112.456 / 789.986/45678";
    acronym = "COP";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112-456";
    acronym = "DEF";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112 - 456";
    acronym = "SAQ";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    name = this.getTemporaryTestLabel("dewey structure " + index);
    cnStructC = String.valueOf(index);
    cStruct = String.valueOf(index++);
    dewey = "112 - 456 /314.9";
    acronym = "GIP";
    structure = this.createStructure(null, name, cnStructC, cStruct, dewey, null, index, acronym);
    this.assertStructureEquals(structure, name, cnStructC, cStruct, dewey);

    String name1 = this.getTemporaryTestLabel("dewey structure " + index);
    String cnStructC1 = String.valueOf(index);
    String cStruct1 = String.valueOf(index++);
    String dewey1 = "";
    int index1 = 123456;
    String acronym1 = "";
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, name1, cnStructC1, cStruct1, dewey1, null, index1, acronym1);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("dewey"));

    String name2 = this.getTemporaryTestLabel("dewey structure " + index);
    String cnStructC2 = String.valueOf(index);
    String cStruct2 = String.valueOf(index++);
    String dewey2 = "abcd";
    String acronym2 = "UMN";
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, name2, cnStructC2, cStruct2, dewey2, null, index1, acronym2);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("dewey"));

    String name3 = this.getTemporaryTestLabel("dewey structure " + index);
    String cnStructC3 = String.valueOf(index);
    String cStruct3 = String.valueOf(index++);
    String dewey3 = "1234\\789";
    String acronym3 = "RET";
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, name3, cnStructC3, cStruct3, dewey3, null, index1, acronym3);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("dewey"));

    String name4 = this.getTemporaryTestLabel("dewey structure " + index);
    String cnStructC4 = String.valueOf(index);
    String cStruct4 = String.valueOf(index++);
    String dewey4 = "1234//789";
    String acronym4 = "RED";
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, name4, cnStructC4, cStruct4, dewey4, null, index1, acronym4);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("dewey"));

    String name5 = this.getTemporaryTestLabel("dewey structure " + index);
    String cnStructC5 = String.valueOf(index);
    String cStruct5 = String.valueOf(index++);
    String dewey5 = "1234--789";
    String acronym5 = "RET";
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, name5, cnStructC5, cStruct5, dewey5, null, index1, acronym5);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("dewey"));

    String name6 = this.getTemporaryTestLabel("dewey structure " + index);
    String cnStructC6 = String.valueOf(index);
    String cStruct6 = String.valueOf(index++);
    String dewey6 = "12345.568 / 12546 / test";
    String acronym6 = "RTS";
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.createStructure(null, name6, cnStructC6, cStruct6, dewey6, null, index1, acronym6);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    assertTrue(e.getResponseBodyAsString().contains("dewey"));

  }

  private void assertStructureEquals(Structure structure, String name, String cnStructC, String cStruct, String dewey) {
    assertNotNull(structure);
    assertEquals(name, structure.getName());
    assertEquals(cnStructC, structure.getCnStructC());
    assertEquals(cStruct, structure.getCodeStruct());
    assertEquals(dewey, structure.getDewey());
  }

  @Override
  protected void deleteFixtures() {
    this.clearStructureFixtures();
  }

}
