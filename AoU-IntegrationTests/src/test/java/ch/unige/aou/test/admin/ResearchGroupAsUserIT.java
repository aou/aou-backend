/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ResearchGroupAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

class ResearchGroupAsUserIT extends AbstractAdminIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Override
  protected void createFixtures() {
    this.restClientTool.sudoRoot();
    this.createTopStructureFixture();
    this.createPublicationTypeAndSubtypeFixtures(false);
    this.restClientTool.exitSudo();
  }

  @Test
  void createTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroup.getResId());
    assertEquals(researchGroup.getName(), researchGroupFound.getName());
    assertEquals(researchGroup.getCode(), researchGroupFound.getCode());
    assertEquals(researchGroup.getAcronym(), researchGroupFound.getAcronym());
    assertFalse(researchGroupFound.isValidated(), "'validated' property should be false by default when created by a USER user");
  }

  @Test
  void createTestWithAlphabeticalCode() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("abc");

    try {
      this.researchGroupService.create(researchGroup);
      fail("A BAD_REQUEST Http response should be received. Research group code must be a positive integer or zero");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void createSameNameTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setResId("tmp1");
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setActive(true);
    this.researchGroupService.create(researchGroup);
    ResearchGroup savedResearchGroup1 = this.researchGroupService.findOne("tmp1");
    assertNotNull(savedResearchGroup1);
    assertEquals(savedResearchGroup1.getName(), researchGroup.getName());
    assertEquals(savedResearchGroup1.getCode(), researchGroup.getCode());
    assertEquals(savedResearchGroup1.getAcronym(), researchGroup.getAcronym());
    assertEquals(savedResearchGroup1.isActive(), true);

    ResearchGroup researchGroup2 = new ResearchGroup();
    researchGroup2.setResId("tmp2");
    researchGroup2.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup2.setCode("12345");
    researchGroup2.setActive(true);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.BadRequest.class, () -> this.researchGroupService.create(researchGroup2));
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("name", "An active research group with the name '" + researchGroup2.getName() + "' already exists",
            validationErrors));

    //same name can exist if active is different
    researchGroup2.setActive(false);

    this.researchGroupService.create(researchGroup2);
    ResearchGroup savedResearchGroup2 = this.researchGroupService.findOne("tmp2");
    assertNotNull(savedResearchGroup2);
    assertEquals(savedResearchGroup2.getName(), researchGroup2.getName());
    assertEquals(savedResearchGroup2.getCode(), researchGroup2.getCode());
    assertEquals(savedResearchGroup2.getAcronym(), researchGroup2.getAcronym());
    assertEquals(savedResearchGroup2.isActive(), false);
  }

  @Test
  void createSameCodeTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    this.researchGroupService.create(researchGroup);

    ResearchGroup researchGroup2 = new ResearchGroup();
    researchGroup2.setName(this.getTemporaryTestLabel("Unilab other"));
    researchGroup2.setCode("1234");
    try {
      this.researchGroupService.create(researchGroup2);
      fail("A BAD_REQUEST Http response should be received. Unique constraint on 'name' not satisfied");
    } catch (final HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }
  }

  @Test
  void findAllTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    List<ResearchGroup> researchGroups = this.researchGroupService.findAll();
    assertTrue(researchGroups.size() >= 1);
  }

  @Test
  void findOneTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroup.getResId());
    assertNotNull(researchGroupFound);
    assertEquals(createdResearchGroup.getName(), researchGroupFound.getName());
    assertEquals(createdResearchGroup.getCode(), researchGroupFound.getCode());

  }

  @Test
  void updateResearchGroupTest() {
    String newName = this.getTemporaryTestLabel("Unilab2");
    final String acronym = "ULB";

    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1111");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);

    ResearchGroup researchGroupUpdate = new ResearchGroup();
    researchGroupUpdate.setName(newName);
    researchGroupUpdate.setCode("2222");
    researchGroupUpdate.setAcronym(acronym);

    String createdResearchGroupId = createdResearchGroup.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.researchGroupService.update(createdResearchGroupId, researchGroupUpdate);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  void deleteResearchGroupTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    String createdResearchGroupId = createdResearchGroup.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.researchGroupService.delete(createdResearchGroupId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroupId);
    assertNotNull(researchGroupFound);
  }

  @Test
  void deleteAllResearchGroupTest() {

    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup1 = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup1);

    ResearchGroup researchGroup2 = new ResearchGroup();
    researchGroup2.setName(this.getTemporaryTestLabel("Unilab2"));
    researchGroup2.setCode("9999");
    researchGroup2.setAcronym("UL2");
    ResearchGroup createdResearchGroup2 = this.researchGroupService.create(researchGroup2);
    assertNotNull(createdResearchGroup2);

    /**
     * Find before delete
     */
    ResearchGroup researchGroupFound1 = this.researchGroupService.findOne(createdResearchGroup1.getResId());
    assertNotNull(researchGroupFound1);
    ResearchGroup researchGroupFound2 = this.researchGroupService.findOne(createdResearchGroup2.getResId());
    assertNotNull(researchGroupFound2);

    /**
     * Delete fails
     */
    String[] ids = Arrays.asList(researchGroupFound1.getResId(), researchGroupFound2.getResId()).toArray(new String[0]);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.researchGroupService.deleteList(ids);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    /**
     * Find after delete
     */
    researchGroupFound1 = this.researchGroupService.findOne(researchGroupFound1.getResId());
    assertNotNull(researchGroupFound1);
    researchGroupFound2 = this.researchGroupService.findOne(researchGroupFound2.getResId());
    assertNotNull(researchGroupFound2);
  }

  @Test
  void validateResearchGroupTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    String createdResearchGroupId = createdResearchGroup.getResId();

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroupId);
    assertEquals(researchGroup.getName(), researchGroupFound.getName());
    assertEquals(researchGroup.getCode(), researchGroupFound.getCode());
    assertEquals(researchGroup.getAcronym(), researchGroupFound.getAcronym());
    assertFalse(researchGroupFound.isValidated(), "'validated' property should be false by default when created by a USER user");

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.researchGroupService.validate(createdResearchGroupId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    researchGroupFound = this.researchGroupService.findOne(createdResearchGroupId);
    assertFalse(researchGroupFound.isValidated(), "'validated' property should still be false");
  }

  @Test
  void validateResearchGroupAsValidatorTest() {
    ResearchGroup researchGroup = new ResearchGroup();
    researchGroup.setName(this.getTemporaryTestLabel("Unilab"));
    researchGroup.setCode("1234");
    researchGroup.setAcronym("UL");
    ResearchGroup createdResearchGroup = this.researchGroupService.create(researchGroup);
    assertNotNull(createdResearchGroup);

    String createdResearchGroupId = createdResearchGroup.getResId();

    ResearchGroup researchGroupFound = this.researchGroupService.findOne(createdResearchGroupId);
    assertEquals(researchGroup.getName(), researchGroupFound.getName());
    assertEquals(researchGroup.getCode(), researchGroupFound.getCode());
    assertEquals(researchGroup.getAcronym(), researchGroupFound.getAcronym());
    assertFalse(researchGroupFound.isValidated(), "'validated' property should be false by default when created by a USER user");

    // give validation rights to user on a structure
    this.giveValidationRightsToUser();

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.researchGroupService.validate(createdResearchGroupId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());

    researchGroupFound = this.researchGroupService.findOne(createdResearchGroupId);
    assertFalse(researchGroupFound.isValidated(), "'validated' property should still be false");
  }

  private void giveValidationRightsToUser() {
    this.restClientTool.sudoRoot();
    final Structure topStructure = this.getTopStructure();
    Person person = this.getUserWithRole(AuthApplicationRole.USER).getPerson();

    // Give default validation rights to the person on the structure
    this.personService.addValidationRightStructure(person.getResId(), topStructure.getResId());
    this.restClientTool.exitSudo();
  }

  @Override
  protected void deleteFixtures() {
    this.restClientTool.sudoRoot();
    this.clearResearchGroupFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.deleteGrandChildrenStructures();
    this.clearStructureFixtures();
    this.restClientTool.exitSudo();
  }
}
