/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - AbstractIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static ch.unige.aou.AouConstants.NO_REPLY_PREFIX;
import static ch.unige.aou.test.AouTestConstants.PUBLICATION_PERMANENT_RESEARCH_GROUP;
import static ch.unige.aou.test.AouTestConstants.PUBLICATION_PERMANENT_STRUCTURE;
import static ch.unige.aou.test.AouTestConstants.PUBLICATION_SUB_TYPE_PERMANENT;
import static ch.unige.aou.test.AouTestConstants.PUBLICATION_TYPE_PERMANENT;
import static ch.unige.aou.test.AouTestConstants.RESEARCH_GROUP_PERMANENT;
import static ch.unige.aou.test.AouTestConstants.STRUCTURE_PERMANENT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.util.SudoRestClientTool;
import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.service.admin.DocumentFileClientService;
import ch.unige.aou.service.admin.GlobalBannerClientService;
import ch.unige.aou.service.admin.LicenseClientService;
import ch.unige.aou.service.admin.LicenseGroupClientService;
import ch.unige.aou.service.admin.NotificationTypeClientService;
import ch.unige.aou.service.admin.PersonClientService;
import ch.unige.aou.service.admin.PublicationClientService;
import ch.unige.aou.service.admin.PublicationDocumentFileClientService;
import ch.unige.aou.service.admin.PublicationSubSubtypeClientService;
import ch.unige.aou.service.admin.PublicationSubtypeClientService;
import ch.unige.aou.service.admin.PublicationSubtypeCompositionClientService;
import ch.unige.aou.service.admin.PublicationTypeClientService;
import ch.unige.aou.service.admin.ResearchGroupClientService;
import ch.unige.aou.service.admin.StoredSearchClientService;
import ch.unige.aou.service.admin.StructureClientService;
import ch.unige.aou.service.admin.UserClientService;
import ch.unige.aou.service.imports.ArxivImportClientService;

@ActiveProfiles({"client-sudo", "test"})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = { "spring.cloud.bootstrap.enabled=true" })
public abstract class AbstractIT {

  protected final static String TEST_FILE_URL = "http://www.unige.ch/rectorat/static/budget-2013.pdf";
  protected final static long TEST_FILE_URL_SIZE = 917380;
  protected final static String TEST_FILE_TO_UPLOAD = "test.pdf";
  protected final static String TEST_FILE_TO_UPLOAD_WITH_PASSWORD = "password_protected.pdf";
  protected final static String TEST_FILE_TO_UPLOAD_EMPTY_TEXT = "empty_text.pdf";
  protected final static String TEST_FILE_TO_UPLOAD_NON_ISO_8859_1_NAME = "non-iso-8859-1-éé.pdf";
  protected static final String FILE_NAME_REPLACEMENT = "file.pdf";

  protected final String PUBLICATION_TYPE_TEST_NAME = this.getTemporaryTestLabel("Publication type");
  protected final String PUBLICATION_SUBTYPE_TEST_NAME = this.getTemporaryTestLabel("Publication subtype");
  protected final String TEST_USER_USER_ID = "user";
  protected final String TEST_USER_ADMIN_ID = "admin";
  protected final String TEST_USER_ROOT_ID = "root";
  protected final String TEST_USER_USER_CN_INDIVIDU = "USER_externalUid";
  protected final String TEST_DEWEY_CODE = "100";

  private static final Logger log = LoggerFactory.getLogger(AbstractIT.class);

  private Map<String, String> mapUserIdPersonId = new HashMap<>();

  @Autowired
  protected LicenseClientService licenseService;

  @Autowired
  protected LicenseGroupClientService licenseGroupService;

  @Autowired
  protected GlobalBannerClientService globalBannerService;

  @Autowired
  protected PersonClientService personService;

  @Autowired
  protected NotificationTypeClientService notificationTypeClientService;

  @Autowired
  protected PublicationSubtypeCompositionClientService publicationSubtypeCompositeService;

  @Autowired
  protected PublicationSubtypeClientService publicationSubtypeService;

  @Autowired
  protected PublicationSubSubtypeClientService publicationSubSubtypeService;

  @Autowired
  protected PublicationTypeClientService publicationTypeService;

  @Autowired
  protected PublicationClientService publicationService;

  @Autowired
  protected UserClientService userService;

  @Autowired
  protected StructureClientService structureService;

  @Autowired
  protected ResearchGroupClientService researchGroupService;

  @Autowired
  protected SudoRestClientTool restClientTool;

  @Autowired
  protected ArxivImportClientService arxivImportService;

  @Autowired
  protected PublicationDocumentFileClientService publicationDocumentFileService;

  @Autowired
  protected DocumentFileClientService documentFileService;

  @Autowired
  protected StoredSearchClientService storedSearchClientService;

  /***********************************/

  @BeforeAll
  public void beforeAll() {
    this.initData();
  }

  @BeforeEach
  public void startUp() {
    this.setUser();
    this.createFixtures();
  }

  /**
   * Create fixtures required by test
   */
  protected void createFixtures() {
  }

  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @AfterEach
  public void tearDown() {
    this.deleteFixtures();
    this.restClientTool.exitSudo();
  }

  /**
   * Delete fixtures created by tests
   */
  protected void deleteFixtures() {
    // this.clearLicenseFixtures();
  }

  /***********************************/

  public void initData() {
    // Make a REST call with test users, in order to present their tokens to the AoU application and
    // create their users
    this.restClientTool.sudoAdmin();
    this.researchGroupService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoUser();
    this.researchGroupService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoThirdPartyUser();
    this.researchGroupService.findAll();
    this.restClientTool.exitSudo();

    this.restClientTool.sudoRoot();
    this.createPermanentStructureForTesting();
    this.createPermanentResearchGroupForTesting();
    this.createPermanentPublicationForTesting();
    this.createPermanentPersonForTesting();
    this.restClientTool.exitSudo();
  }

  public String getPersonIdFromUserId(String userId) {
    return this.mapUserIdPersonId.computeIfAbsent(userId, id -> {
      this.restClientTool.sudoRoot();
      final String personId = this.userService.findOne(id).getPerson().getResId();
      this.restClientTool.exitSudo();
      return personId;
    });
  }

  public void createPermanentPublicationForTesting() {
    List<PublicationType> publicationTypeList = this.publicationTypeService
            .searchByProperties(Collections.singletonMap("name", PUBLICATION_TYPE_PERMANENT));
    if (publicationTypeList.isEmpty()) {
      PublicationType type = new PublicationType();
      type.setName(AouTestConstants.getNameWithPermanentLabel(PUBLICATION_TYPE_PERMANENT));
      PublicationType createdType = this.publicationTypeService.create(type);

      PublicationSubtype subtype = new PublicationSubtype();
      subtype.setPublicationType(createdType);
      subtype.setName(AouTestConstants.getNameWithPermanentLabel(PUBLICATION_SUB_TYPE_PERMANENT));
      subtype.setCode("CODE TESTING");
      this.publicationSubtypeCompositeService.create(createdType.getResId(), subtype);

      Publication publication = new Publication();
      String title = AouTestConstants.getNameWithPermanentLabel(PUBLICATION_PERMANENT_STRUCTURE);
      publication.setFormData(String.format(AouConstants.TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE, title));
      publication.setTitle(title);
      publication.setSubtype(subtype);
      this.publicationService.create(publication);

      final Structure structure = this.getPermanentStructure();
      this.publicationService.addStructure(publication.getResId(), structure.getResId());

      Publication publication2 = new Publication();
      String title2 = AouTestConstants.getNameWithPermanentLabel(PUBLICATION_PERMANENT_RESEARCH_GROUP);
      String formData = String.format(AouConstants.TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE, title2);

      // Linked this publication with a research group
      final ResearchGroup researchGroup = this.getPermanentResearchGroup();
      formData = this.addResearchGroupInFormData(formData, researchGroup.getResId());

      publication2.setFormData(formData);
      publication2.setTitle(title2);
      publication2.setSubtype(subtype);
      this.publicationService.create(publication2);
    }
  }

  public void createPermanentResearchGroupForTesting() {
    List<ResearchGroup> researchGroupList = this.researchGroupService
            .searchByProperties(Collections.singletonMap("name", AouTestConstants.getNameWithPermanentLabel(RESEARCH_GROUP_PERMANENT)));
    if (researchGroupList.isEmpty()) {
      ResearchGroup researchGroup = new ResearchGroup();
      researchGroup.setName(AouTestConstants.getNameWithPermanentLabel(RESEARCH_GROUP_PERMANENT));
      researchGroup.setCode("111111");
      this.researchGroupService.create(researchGroup);
    }
  }

  public void createPermanentStructureForTesting() {
    List<Structure> listStructure = this.structureService.searchByProperties(Collections.singletonMap("name", STRUCTURE_PERMANENT));
    if (listStructure.isEmpty()) {
      Structure structure = new Structure();
      structure.setName(AouTestConstants.getNameWithPermanentLabel(STRUCTURE_PERMANENT));
      structure.setCnStructC("1234567");
      structure.setCodeStruct("aaa");
      structure.setDewey("112");
      structure.setSortValue(10);
      this.structureService.create(structure);
    }
  }

  public void createPermanentPersonForTesting() {
    this.getPermanentPerson(AuthApplicationRole.USER);
  }

  protected Publication createPublicationWithSubType(String title) {
    PublicationType type = this.publicationTypeService.findByName(this.PUBLICATION_TYPE_TEST_NAME);
    List<PublicationSubtype> publicationSubtypes = this.publicationSubtypeCompositeService.findAll(type.getResId());
    PublicationSubtype subtype = publicationSubtypes.get(0);

    Publication publication = new Publication();
    publication.setSubtype(subtype);
    publication.setTitle(title);
    publication.setFormData(String.format(AouConstants.TEST_DEPOSIT_JSON_FORM_DATA_TEMPLATE, title));
    return publication;
  }

  protected void createPublicationTypeAndSubtypeFixtures(boolean subSubtypeNeeded) {

    PublicationType type = new PublicationType();
    type.setName(this.PUBLICATION_TYPE_TEST_NAME);
    PublicationType createdType = this.publicationTypeService.create(type);

    PublicationSubtype subtype = new PublicationSubtype();
    subtype.setPublicationType(createdType);
    subtype.setName(this.getTemporaryTestLabel("First publication subtype"));
    subtype.setCode("TST1");
    this.publicationSubtypeCompositeService.create(createdType.getResId(), subtype);

    subtype = new PublicationSubtype();
    subtype.setPublicationType(createdType);
    subtype.setName(this.getTemporaryTestLabel("Second publication subtype"));
    subtype.setCode("TST2");
    this.publicationSubtypeCompositeService.create(createdType.getResId(), subtype);

    subtype = new PublicationSubtype();
    subtype.setPublicationType(createdType);
    subtype.setName(this.getTemporaryTestLabel("Third publication subtype"));
    subtype.setCode("TST3");
    this.publicationSubtypeCompositeService.create(createdType.getResId(), subtype);

    if (subSubtypeNeeded) {
      subtype = new PublicationSubtype();
      subtype.setPublicationType(createdType);
      subtype.setName(this.PUBLICATION_SUBTYPE_TEST_NAME);
      subtype.setCode("TST4");
      this.publicationSubtypeCompositeService.create(createdType.getResId(), subtype);

      PublicationSubSubtype subSubtype = new PublicationSubSubtype();
      subSubtype.setName("First Sub subtype");
      subSubtype.setPublicationSubtype(subtype);
      this.publicationSubSubtypeService.create(subtype.getResId(), subSubtype);
    }
  }

  protected DocumentFile waitForDocumentFileProcessingToBeOver(String publicationId, String documentFileId) {
    int count = 0;
    DocumentFile foundDocumentFile;
    do {
      foundDocumentFile = this.publicationDocumentFileService.findOne(publicationId, documentFileId);

      AouTestConstants.failAfterNAttempt(count,
              "DocumentFile " + foundDocumentFile.getResId() + "should now be READY but is " + foundDocumentFile.getStatus().name());

      count++;
    } while (foundDocumentFile.getStatus() != DocumentFile.DocumentFileStatus.READY
            && foundDocumentFile.getStatus() != DocumentFile.DocumentFileStatus.TO_BE_CONFIRMED
            && foundDocumentFile.getStatus() != DocumentFile.DocumentFileStatus.IN_ERROR
            && foundDocumentFile.getStatus() != DocumentFile.DocumentFileStatus.IMPOSSIBLE_TO_DOWNLOAD);

    return foundDocumentFile;
  }

  protected boolean waitPublicationAskFeedback(String publicationId) {
    int count = 0;
    Publication deposit;

    do {
      deposit = this.publicationService.findOne(publicationId);
      AouTestConstants
              .failAfterNAttempt(count, "Publication " + publicationId + " should have status " + Publication.PublicationStatus.FEEDBACK_REQUIRED
                      + " instead of " + deposit.getStatus());
      count++;
    } while (deposit.getStatus() != Publication.PublicationStatus.FEEDBACK_REQUIRED
            && deposit.getStatus() != Publication.PublicationStatus.IN_ERROR);
    return (Publication.PublicationStatus.FEEDBACK_REQUIRED == deposit.getStatus());
  }

  /***********************************/

  protected void assertEqualsWithoutNanoSeconds(OffsetDateTime time1, OffsetDateTime time2) {
    assertEquals(time1.withNano(0), time2.withNano(0));
  }

  protected void assertEqualsWithoutNanoSeconds(String message, OffsetDateTime time1, OffsetDateTime time2) {
    assertEquals(time1.withNano(0), time2.withNano(0), message);
  }

  protected void clearLicenseFixtures() {
    this.licenseService.findAll().stream()
            .filter(license -> license.getOpenLicenseId().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(license -> this.licenseService.delete(license.getResId()));
  }

  protected void clearGlobalBannerFixtures() {
    this.globalBannerService.findAll().stream()
            .filter(globalBanner -> globalBanner.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(globalBanner -> this.globalBannerService.delete(globalBanner.getResId()));
  }

  protected void clearPeopleFixtures() {
    this.personService.findAll().stream()
            .filter(person -> person.getLastName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL))
            .forEach(person -> this.personService.delete(person.getResId()));
  }

  protected void clearPublicationFixtures() {

    final Map<String, String> properties = new HashMap<>();
    properties.put("title", AouConstants.TEMPORARY_TEST_DATA_LABEL);
    final List<Publication> testPublications = this.publicationService.searchByProperties(properties);

    for (Publication publication : testPublications) {
      if (publication.getTitle().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.publicationService.delete(publication.getResId());
      }
    }
  }

  protected void clearPublicationSubtypeFixtures() {
    final List<PublicationType> types = this.publicationTypeService.findAll();
    for (PublicationType type : types) {
      final List<PublicationSubtype> subtypes = this.publicationSubtypeCompositeService.findAll(type.getResId());
      for (PublicationSubtype subtype : subtypes) {
        if (subtype.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
          this.publicationSubtypeCompositeService.delete(type.getResId(), subtype.getResId());
        }
      }
    }
  }

  protected void clearPublicationTypeFixtures() {
    final List<PublicationType> types = this.publicationTypeService.findAll();
    for (PublicationType type : types) {
      if (type.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL)) {
        this.publicationTypeService.delete(type.getResId());
      }
    }
  }

  protected Person getPermanentPerson(ApplicationRole applicationRole) {
    String userId;
    switch (applicationRole.getResId()) {
      case AuthApplicationRole.USER_ID:
        userId = this.TEST_USER_USER_ID;
        break;
      case AuthApplicationRole.ADMIN_ID:
        userId = this.TEST_USER_ADMIN_ID;
        break;
      case AuthApplicationRole.ROOT_ID:
        userId = this.TEST_USER_ROOT_ID;
        break;
      default:
        throw new IllegalArgumentException(applicationRole + " is not a known role");
    }
    return this.personService.findOne(this.getPersonIdFromUserId(userId));
  }

  protected Structure getPermanentStructure() {
    List<Structure> listStructure = this.structureService.searchByProperties(Collections.singletonMap("name", STRUCTURE_PERMANENT));
    if (listStructure.isEmpty()) {
      throw new SolidifyResourceNotFoundException("Permanent Structure is missing.");
    } else {
      return listStructure.get(0);
    }
  }

  protected ResearchGroup getPermanentResearchGroup() {
    List<ResearchGroup> researchGroupList = this.researchGroupService
            .searchByProperties(Collections.singletonMap("name", RESEARCH_GROUP_PERMANENT));
    if (researchGroupList.isEmpty()) {
      throw new SolidifyResourceNotFoundException("Permanent Research Group is missing.");
    } else {
      return researchGroupList.get(0);
    }
  }

  protected Publication getPermanentPublication() {
    Publication publication = this.publicationService.findByTitle(AouTestConstants.PUBLICATION_PERMANENT_RESEARCH_GROUP);
    if (publication == null) {
      throw new SolidifyResourceNotFoundException("Permanent Publication is missing.");
    } else {
      return publication;
    }
  }

  protected String addResearchGroupInFormData(String formData, String groupId) {
    JSONObject formDataJsonObject = new JSONObject(formData);

    if (!formDataJsonObject.getJSONObject("contributors").has("groups")) {
      formDataJsonObject.getJSONObject("contributors").put("groups", new JSONArray());
    }

    formDataJsonObject.getJSONObject("contributors").getJSONArray("groups").put(groupId);

    return formDataJsonObject.toString();
  }

  protected String addAbstractInFormData(String formData, String text, String lang) {
    JSONObject formDataJsonObject = new JSONObject(formData);

    if (!formDataJsonObject.getJSONObject("description").has("abstracts")) {
      formDataJsonObject.getJSONObject("description").put("abstracts", new JSONArray());
    }

    JSONObject abstractJsonObject = new JSONObject();
    abstractJsonObject.put("text", text);
    abstractJsonObject.put("lang", lang);
    formDataJsonObject.getJSONObject("description").getJSONArray("abstracts").put(abstractJsonObject);

    return formDataJsonObject.toString();
  }

  protected String addDateInFormData(String formData, String type, String date) {
    JSONObject formDataJsonObject = new JSONObject(formData);

    if (!formDataJsonObject.getJSONObject("description").has("dates")) {
      formDataJsonObject.getJSONObject("description").put("dates", new JSONArray());
    }

    JSONObject dateJsonObject = new JSONObject();
    dateJsonObject.put("type", type);
    dateJsonObject.put("date", date);
    formDataJsonObject.getJSONObject("description").getJSONArray("dates").put(dateJsonObject);

    return formDataJsonObject.toString();
  }

  protected String addDoiInFormData(String formData, String doi) {
    JSONObject formDataJsonObject = new JSONObject(formData);

    if (!formDataJsonObject.getJSONObject("description").has("identifiers")) {
      formDataJsonObject.getJSONObject("description").put("identifiers", new JSONObject());
    }
    formDataJsonObject.getJSONObject("description").getJSONObject("identifiers").put("doi", doi);

    return formDataJsonObject.toString();
  }

  protected String addPmidInFormData(String formData, String pmid) {
    JSONObject formDataJsonObject = new JSONObject(formData);

    if (!formDataJsonObject.getJSONObject("description").has("identifiers")) {
      formDataJsonObject.getJSONObject("description").put("identifiers", new JSONObject());
    }
    formDataJsonObject.getJSONObject("description").getJSONObject("identifiers").put("pmid", pmid);

    return formDataJsonObject.toString();
  }

  /***********************************/

  protected User getUserWithRole(ApplicationRole role) {
    final Map<String, String> properties = new HashMap<>();
    properties.put("email", NO_REPLY_PREFIX + role.getName().toLowerCase() + "@unige.ch");
    return this.userService.searchByProperties(properties).get(0);
  }

  protected User findThirdPartyUser() {
    final Map<String, String> properties = new HashMap<>();
    properties.put("email", NO_REPLY_PREFIX + "THIRD-PARTY-USER@unige.ch");
    return this.userService.searchByProperties(properties).get(0);
  }

  protected String getTemporaryTestLabel(String label) {
    return AouConstants.TEMPORARY_TEST_DATA_LABEL + label;
  }

  protected String getTemporaryTestLabelWithoutSpaces(String label) {
    return this.getTemporaryTestLabel(label).replace(" ", "_");
  }

  protected boolean validationErrorsContain(String fieldname, String errorMessage, List<FieldValidationError> validationErrors) {
    for (FieldValidationError fieldValidationError : validationErrors) {
      if (fieldValidationError.getFieldName().equals(fieldname)) {
        for (String errorMsg : fieldValidationError.getErrorMessages()) {
          if (errorMsg.equals(errorMessage)) {
            return true;
          }
        }
        break;
      }
    }
    return false;
  }

  protected List<FieldValidationError> getValidationErrors(String responseBody) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      JsonNode rootNode = mapper.readTree(responseBody);
      return mapper.convertValue(rootNode.get("validationErrors"), new TypeReference<>() {
      });
    } catch (JsonProcessingException e) {
      fail("unable to parse response body");
      return null;
    }
  }
}
