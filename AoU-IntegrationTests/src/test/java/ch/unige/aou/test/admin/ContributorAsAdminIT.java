/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - ContributorAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorOtherName;

class ContributorAsAdminIT extends AbstractContributorIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void testCreate() {
    Contributor contributor = this.getContributor();
    assertThrows(HttpClientErrorException.Forbidden.class, () -> this.contributorService.create(contributor));
  }

  @Test
  void testAddOtherName() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    this.restClientTool.exitSudo();

    this.testAddOtherNameOnContributor(savedContributor, this.testOtherFirstName, this.testOtherLastName);
    this.testAddOtherNameOnContributor(savedContributor, "Cibelus", "Kan");
  }

  @Test
  void testAddSameOtherNameTwice() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    this.restClientTool.exitSudo();

    this.testAddOtherNameOnContributor(savedContributor, this.testOtherFirstName, this.testOtherLastName);

    ContributorOtherName otherName = new ContributorOtherName();
    otherName.setFirstName(this.testOtherFirstName);
    otherName.setLastName(this.testOtherLastName);
    String contributorId = savedContributor.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.contributorOtherNameService.create(contributorId, otherName));
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    String errorMessage = "The other name '" + otherName.getFullName() + "' already exists for this contributor";
    assertTrue(this.validationErrorsContain("firstName", errorMessage, validationErrors));
    assertTrue(this.validationErrorsContain("lastName", errorMessage, validationErrors));
  }

  @Test
  void testAddSameNameAsContributor() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    this.restClientTool.exitSudo();

    ContributorOtherName otherName = new ContributorOtherName();
    otherName.setFirstName(savedContributor.getFirstName());
    otherName.setLastName(savedContributor.getLastName());
    String contributorId = savedContributor.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.BadRequest.class,
            () -> this.contributorOtherNameService.create(contributorId, otherName));
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    String errorMessage = "The name '" + otherName.getFullName() + "' is identical to the contributor's one";
    assertTrue(this.validationErrorsContain("firstName", errorMessage, validationErrors));
    assertTrue(this.validationErrorsContain("lastName", errorMessage, validationErrors));
  }

  @Test
  void testRemoveOtherName() {
    this.restClientTool.sudoRoot();
    Contributor savedContributor = this.testContributorCreate();
    this.restClientTool.exitSudo();

    ContributorOtherName otherName = this.testAddOtherNameOnContributor(savedContributor, this.testOtherFirstName, this.testOtherLastName);

    this.contributorOtherNameService.delete(savedContributor.getResId(), otherName.getResId());

    Contributor fetchedContributor = this.contributorService.findOne(savedContributor.getResId());
    Optional<ContributorOtherName> otherNameOpt = fetchedContributor.getOtherNames().stream()
            .filter(a -> a.getLastName().equals(this.testOtherLastName))
            .findFirst();
    assertFalse(otherNameOpt.isPresent());
  }

  @Override
  @Test
  void testSearchUnigeContributorWithFirstnameAndLastname() {
    super.testSearchUnigeContributorWithFirstnameAndLastname();
  }

  @Override
  @Test
  void testSearchUnigeContributorWithLooseName() {
    super.testSearchUnigeContributorWithLooseName();
  }
}
