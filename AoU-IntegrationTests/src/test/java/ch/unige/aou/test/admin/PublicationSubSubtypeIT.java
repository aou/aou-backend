/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - PublicationSubSubtypeIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.PublicationSubSubtype;
import ch.unige.aou.model.publication.PublicationSubtype;

class PublicationSubSubtypeIT extends AbstractAdminIT {

  protected void setUser() {
    this.restClientTool.sudoRoot();
  }

  @Override
  protected void createFixtures() {
    this.createPublicationTypeAndSubtypeFixtures(true);
  }

  @Test
  void listTest() {
    PublicationSubtype subtype = this.publicationSubtypeService.findByName(this.PUBLICATION_SUBTYPE_TEST_NAME);
    List<PublicationSubSubtype> publicationSubSubtypes = this.publicationSubSubtypeService.findAll(subtype.getResId());

    assertEquals(1, publicationSubSubtypes.size());
    for (PublicationSubSubtype subSubtype : publicationSubSubtypes) {
      assertEquals(subtype.getResId(), subSubtype.getPublicationSubtype().getResId());
      assertTrue(subtype.getName().startsWith(AouConstants.TEMPORARY_TEST_DATA_LABEL));
    }
  }

  @Test
  void findOneTest() {
    PublicationSubtype subtype = this.publicationSubtypeService.findByName(this.PUBLICATION_SUBTYPE_TEST_NAME);
    List<PublicationSubSubtype> publicationSubSubtypes = this.publicationSubSubtypeService.findAll(subtype.getResId());

    assertEquals(1, publicationSubSubtypes.size());

    PublicationSubSubtype subSubtype = publicationSubSubtypes.get(0);
    PublicationSubSubtype foundSubSubtype = this.publicationSubSubtypeService.findOne(subtype.getResId(), subSubtype.getResId());

    assertNotNull(foundSubSubtype.getPublicationSubtype());
    assertEquals(subtype.getResId(), foundSubSubtype.getPublicationSubtype().getResId());
    assertEquals(subSubtype.getName(), foundSubSubtype.getName());
    assertEquals(subSubtype.getSortValue(), foundSubSubtype.getSortValue());
  }

  @Test
  void createTest() {
    PublicationSubtype subtype = this.publicationSubtypeService.findByName(this.PUBLICATION_SUBTYPE_TEST_NAME);

    PublicationSubSubtype subSubtype = new PublicationSubSubtype();
    subSubtype.setPublicationSubtype(subtype);
    subSubtype.setName(AouConstants.TEMPORARY_TEST_DATA_LABEL + "lorem ipsum");
    subSubtype.setSortValue(500);
    PublicationSubSubtype createdSubSubtype = this.publicationSubSubtypeService.create(subtype.getResId(), subSubtype);

    assertNotNull(createdSubSubtype);
    assertEquals(subtype.getResId(), createdSubSubtype.getPublicationSubtype().getResId());
    assertEquals(subSubtype.getName(), createdSubSubtype.getName());
    assertEquals(subSubtype.getSortValue(), createdSubSubtype.getSortValue());
  }

  @Test
  void createIncomplete() {
    PublicationSubtype subtype = this.publicationSubtypeService.findByName(this.PUBLICATION_SUBTYPE_TEST_NAME);

    /**
     * missing name
     */
    PublicationSubSubtype subSubtype = new PublicationSubSubtype();
    subSubtype.setPublicationSubtype(subtype);
    subSubtype.setSortValue(100);
    String subTypeId = subSubtype.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationSubSubtypeService.create(subTypeId, subSubtype);
    });
    assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
  }

  @Override
  protected void deleteFixtures() {
    this.clearPublicationSubSubtypeFixtures();
    this.clearPublicationSubtypeFixtures();
    this.clearPublicationTypeFixtures();
    this.clearPublicationFixtures();
  }
}
