/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - AouTestConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static ch.unige.aou.AouConstants.PERMANENT_TEST_DATA_LABEL;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.concurrent.ThreadLocalRandom;

import ch.unige.solidify.util.SolidifyTime;

import ch.unige.aou.AouConstants;

public class AouTestConstants {
  public final static int TRIES = 120;
  public final static String STRUCTURE_PERMANENT = "Permanent Structure";
  public final static String RESEARCH_GROUP_PERMANENT = "Permanent Research Group";
  public final static String PUBLICATION_PERMANENT_STRUCTURE = "Permanent publication structure";
  public final static String PUBLICATION_PERMANENT_RESEARCH_GROUP = "Permanent publication research group";
  public final static String PUBLICATION_TYPE_PERMANENT = "Permanent publication type";
  public final static String PUBLICATION_SUB_TYPE_PERMANENT = "Permanent publication subtype";
  public final static String EVENT_MESSAGE_TEST = "Event message for testing";

  public static void failAfterNAttempt(int count, String failMessage) {
    if (count > AouTestConstants.TRIES) {
      fail(failMessage);
    }
    SolidifyTime.waitOneSecond();
  }

  public static String getNameWithPermanentLabel(String name) {
    return PERMANENT_TEST_DATA_LABEL + name;
  }

  public static String getRandomNameWithTemporaryLabel(String name) {
    return AouConstants.TEMPORARY_TEST_DATA_LABEL + name + ThreadLocalRandom.current().nextInt();
  }

  public static String getRandomOrcId() {
    String orcId = "";
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        orcId = orcId + ThreadLocalRandom.current().nextInt(10);
      }
      orcId = orcId + "-";
    }
    return orcId.substring(0, orcId.length() - 1);
  }
}
