/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - DocumentFileAsUserIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ch.unige.solidify.util.SolidifyTime;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;
import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFile.AccessLevel;
import ch.unige.aou.model.publication.DocumentFile.DocumentFileStatus;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;

class DocumentFileAsUserIT extends AbstractDocumentFileIT {

  private final int WAIT_FOR_METADATA_TO_BE_COMPLETED = 3;

  @Override
  protected void setUser() {
    this.restClientTool.sudoUser();
  }

  @Test
  public void testCreate() {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));

    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setDocumentFileType(documentFileType);
    documentFile.setSourceData(URI.create(TEST_FILE_URL));
    documentFile.setAccessLevel(AccessLevel.PUBLIC);

    DocumentFile createdDocumentFile = this.publicationDocumentFileService.create(publication.getResId(), documentFile);

    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());

    DocumentFile foundDocumentFile = this.publicationDocumentFileService.findOne(publication.getResId(), createdDocumentFile.getResId());
    assertNotNull(foundDocumentFile);
    assertEquals(foundDocumentFile.getPublication().getResId(), documentFile.getPublication().getResId());
    assertEquals(foundDocumentFile.getDocumentFileType().getResId(), documentFile.getDocumentFileType().getResId());
    assertEquals(foundDocumentFile.getSourceData(), documentFile.getSourceData());
    assertEquals(foundDocumentFile.getAccessLevel(), documentFile.getAccessLevel());
  }

  @Test
  public void testCreateInOtherPublication() {
    //create publication as another user(root in this case)
    this.restClientTool.sudoRoot();
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    this.restClientTool.exitSudo();

    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setDocumentFileType(documentFileType);
    documentFile.setSourceData(URI.create(TEST_FILE_URL));
    documentFile.setAccessLevel(AccessLevel.PUBLIC);

    final String publicationId = publication.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationDocumentFileService.create(publicationId, documentFile);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  @Test
  public void testList() {
    Publication publication1 = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFileType documentFileType1 = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    String publication2_title = this.getTemporaryTestLabel("Test publication 2");
    Publication publication2 = this.createPublicationFixture(publication2_title);
    DocumentFileType documentFileType2 = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_TRANSLATION);

    // check that no document files exist
    List<DocumentFile> documentFiles1 = this.publicationDocumentFileService.findAll(publication1.getResId());
    assertTrue(documentFiles1.isEmpty());
    List<DocumentFile> documentFiles2 = this.publicationDocumentFileService.findAll(publication2.getResId());
    assertTrue(documentFiles2.isEmpty());

    // create document file 1 on publication 1
    DocumentFile documentFile1 = new DocumentFile();
    documentFile1.setPublication(publication1);
    documentFile1.setDocumentFileType(documentFileType1);
    documentFile1.setSourceData(URI.create(TEST_FILE_URL));
    documentFile1.setAccessLevel(AccessLevel.PUBLIC);
    this.publicationDocumentFileService.create(publication1.getResId(), documentFile1);

    // create document file 2 on publication 2
    DocumentFile documentFile2 = new DocumentFile();
    documentFile2.setPublication(publication2);
    documentFile2.setDocumentFileType(documentFileType2);
    documentFile2.setSourceData(URI.create(TEST_FILE_URL));
    documentFile2.setAccessLevel(AccessLevel.PRIVATE);
    this.publicationDocumentFileService.create(publication2.getResId(), documentFile2);

    // check that document files appear in each respective list
    documentFiles1 = this.publicationDocumentFileService.findAll(publication1.getResId());
    assertEquals(1, documentFiles1.size());
    DocumentFile foundDocumentFile1 = documentFiles1.get(0);
    assertEquals(publication1.getResId(), documentFile1.getPublication().getResId());
    assertEquals(documentFileType1.getResId(), documentFile1.getDocumentFileType().getResId());
    assertEquals(foundDocumentFile1.getSourceData(), documentFile1.getSourceData());
    assertEquals(AccessLevel.PUBLIC, documentFile1.getAccessLevel());

    documentFiles2 = this.publicationDocumentFileService.findAll(publication2.getResId());
    assertEquals(1, documentFiles2.size());
    DocumentFile foundDocumentFile2 = documentFiles2.get(0);
    assertEquals(publication2.getResId(), documentFile2.getPublication().getResId());
    assertEquals(documentFileType2.getResId(), documentFile2.getDocumentFileType().getResId());
    assertEquals(foundDocumentFile2.getSourceData(), documentFile2.getSourceData());
    assertEquals(AccessLevel.PRIVATE, documentFile2.getAccessLevel());
  }

  @Test
  public void testCreateByDownload() {
    DocumentFile createdDocumentFile = this.createPublicDocumentFile();
    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());

    assertEquals(DocumentFileStatus.READY, foundDocumentFile.getStatus());
    assertTrue(foundDocumentFile.getFinalData() != null && !StringTool.isNullOrEmpty(foundDocumentFile.getFinalData().toString()));
    assertFalse(StringTool.isNullOrEmpty(foundDocumentFile.getFileName()));
    String fileName = TEST_FILE_URL.substring(TEST_FILE_URL.lastIndexOf('/') + 1);
    assertEquals(fileName, foundDocumentFile.getFileName());
    assertEquals(TEST_FILE_URL_SIZE, (long) foundDocumentFile.getFileSize());
  }

  @Test
  public void testMetadataAfterUploadFile() {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add("documentFileTypeId", documentFileType.getResId());
    parameters.add("accessLevel", AccessLevel.PUBLIC.name());

    final ClassPathResource fileToUpload = new ClassPathResource(TEST_FILE_TO_UPLOAD);

    DocumentFile uploadedDocumentFile = this.publicationDocumentFileService.upload(publication.getResId(), fileToUpload, parameters);

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(uploadedDocumentFile.getPublication().getResId(), uploadedDocumentFile.getResId());

    assertEquals(DocumentFileStatus.READY, foundDocumentFile.getStatus());

    //update publication in order to recreate the metadata
    String titleToUpdate = this.getTemporaryTestLabel("Publication to update metadata");
    publication.setFormData(this.getTestFormData(titleToUpdate));
    publication.setMetadata(null);

    Publication publicationUpdated = this.publicationService.update(publication.getResId(), publication);

    assertEquals(titleToUpdate, publicationUpdated.getTitle());

    assertTrue(this.checkFileName(publicationUpdated.getMetadata(), TEST_FILE_TO_UPLOAD));
  }

  @Test
  public void testMetadataAfterFileUpdates() throws IOException {

    // New Publication doesn't have any file in metadata
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    assertFalse(this.checkFileName(publication.getMetadata(), TEST_FILE_TO_UPLOAD));
    assertFalse(this.checkFileAccessLevel(publication.getMetadata(), AccessLevel.PUBLIC.metadataValue()));

    // Adding a file automatically completes Publication's metadata
    DocumentFile documentFile = this.uploadDocumentFile(publication, new ClassPathResource(TEST_FILE_TO_UPLOAD));
    assertNotNull(documentFile);
    SolidifyTime.waitInSeconds(this.WAIT_FOR_METADATA_TO_BE_COMPLETED); // wait for publication's metadata to be completed
    publication = this.publicationService.findOne(publication.getResId());
    assertTrue(this.checkFileName(publication.getMetadata(), TEST_FILE_TO_UPLOAD));
    assertTrue(this.checkFileAccessLevel(publication.getMetadata(), AccessLevel.PUBLIC.metadataValue()));

    // Updating a file makes Publication's metadata updated accordingly
    documentFile.setAccessLevel(AccessLevel.PRIVATE);
    this.publicationDocumentFileService.update(publication.getResId(), documentFile.getResId(), documentFile);
    SolidifyTime.waitInSeconds(this.WAIT_FOR_METADATA_TO_BE_COMPLETED); // wait for publication's metadata to be completed
    publication = this.publicationService.findOne(publication.getResId());
    assertTrue(this.checkFileName(publication.getMetadata(), TEST_FILE_TO_UPLOAD));
    assertFalse(this.checkFileAccessLevel(publication.getMetadata(), AccessLevel.PUBLIC.metadataValue()));
    assertTrue(this.checkFileAccessLevel(publication.getMetadata(), AccessLevel.PRIVATE.metadataValue()));

    // Deleting a file makes Publication's metadata updated
    this.publicationDocumentFileService.delete(publication.getResId(), documentFile.getResId());
    SolidifyTime.waitInSeconds(this.WAIT_FOR_METADATA_TO_BE_COMPLETED); // wait for publication's metadata to be completed
    publication = this.publicationService.findOne(publication.getResId());
    assertFalse(this.checkFileName(publication.getMetadata(), TEST_FILE_TO_UPLOAD));
    assertFalse(this.checkFileAccessLevel(publication.getMetadata(), AccessLevel.PUBLIC.metadataValue()));
    assertFalse(this.checkFileAccessLevel(publication.getMetadata(), AccessLevel.PRIVATE.metadataValue()));
  }

  private boolean checkFileName(String xmlContent, String fileName) {
    return this.checkFileElement(xmlContent, "name", fileName);
  }

  private boolean checkFileAccessLevel(String xmlContent, String fileName) {
    return this.checkFileElement(xmlContent, "accessLevel", fileName);
  }

  private boolean checkFileElement(String xmlContent, String propertyName, String valueToCheck) {
    try {
      DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
      Document inputDoc = documentBuilderFactory.newDocumentBuilder().parse(new InputSource(new StringReader(xmlContent)));
      Node propertyNode = XMLTool.getFirstNode(inputDoc, "//files/file/" + propertyName);
      if (propertyNode != null) {
        return valueToCheck.equals(propertyNode.getTextContent());
      } else {
        return false;
      }
    } catch (SAXException | ParserConfigurationException | IOException e) {
      return false;
    }
  }

  @Test
  public void testCreateByUpload() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFile documentFile = this.uploadDocumentFile(publication, new ClassPathResource(TEST_FILE_TO_UPLOAD));
    assertNotNull(documentFile);
  }

  @Test
  public void testCreateTwiceByUpload() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));

    final ClassPathResource fileToUpload = new ClassPathResource(TEST_FILE_TO_UPLOAD);

    this.uploadDocumentFile(publication, fileToUpload);

    // upload the same file a second time: it should create a 2nd documentFile with filename suffix 1 "test1.pdf"
    final String label2 = "label2";
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);
    final long fileToUploadSize = fileToUpload.contentLength();

    MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add("documentFileTypeId", documentFileType.getResId());
    parameters.add("accessLevel", AccessLevel.PRIVATE.name());
    parameters.add("label", label2);
    DocumentFile uploadedDocumentFile2 = this.publicationDocumentFileService.upload(publication.getResId(), fileToUpload, parameters);
    DocumentFile foundDocumentFile2 = this
            .waitForDocumentFileProcessingToBeOver(uploadedDocumentFile2.getPublication().getResId(), uploadedDocumentFile2.getResId());
    assertEquals(DocumentFileStatus.READY, foundDocumentFile2.getStatus());
    assertFalse(foundDocumentFile2.getFinalData() == null || StringTool.isNullOrEmpty(foundDocumentFile2.getFinalData().toString()));
    assertFalse(StringTool.isNullOrEmpty(foundDocumentFile2.getFileName()));
    assertEquals("test1.pdf", foundDocumentFile2.getFileName());
    assertEquals(fileToUploadSize, (long) foundDocumentFile2.getFileSize());
    assertEquals(AccessLevel.PRIVATE, foundDocumentFile2.getAccessLevel());
    assertEquals(label2, foundDocumentFile2.getLabel());
  }

  @Test
  public void createAndDeleteDuplicatedFiles() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    //Get the inputStream and copy the file first
    InputStream fileTestInputStream = new ClassPathResource(TEST_FILE_TO_UPLOAD).getInputStream();
    File testPdf = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_TO_UPLOAD); //Path of file after copy
    this.copyInputStreamToFile(fileTestInputStream, testPdf);

    FileSystemResource pdfFileResource = new FileSystemResource(testPdf);
    this.uploadDocumentFile(publication, pdfFileResource);

    this.copyFileToUploadToOtherName(TEST_FILE_NAME2);

    File file2 = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_NAME2); //Path of file after copy
    this.uploadDocumentFile(publication, new FileSystemResource(file2));

    this.copyFileToUploadToOtherName(TEST_FILE_NAME3);

    File file3 = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_NAME3); //Path of file after copy
    this.uploadDocumentFile(publication, new FileSystemResource(file3));

    // trigger the validation that should make all document files detected as DUPLICATE
    assertThrows(HttpClientErrorException.class, () -> this.publicationService.validateMetadata(publication.getResId()));

    // there are 3 files and their status are all DUPLICATE
    List<DocumentFile> documentFiles = this.publicationDocumentFileService.findAll(publication.getResId());
    assertNotNull(documentFiles);
    assertEquals(3, documentFiles.size());
    assertEquals(3, documentFiles.stream().filter(df -> df.getStatus().equals(DocumentFileStatus.DUPLICATE)).count());

    // delete one file -> there are 2 files left, both with DUPLICATE status
    this.publicationDocumentFileService.delete(publication.getResId(), documentFiles.get(0).getResId());
    documentFiles = this.publicationDocumentFileService.findAll(publication.getResId());
    assertNotNull(documentFiles);
    assertEquals(2, documentFiles.size());
    assertEquals(2, documentFiles.stream().filter(df -> df.getStatus().equals(DocumentFileStatus.DUPLICATE)).count());

    // delete a 2nd file -> there is 1 file left and its status has been updated to READY
    this.publicationDocumentFileService.delete(publication.getResId(), documentFiles.get(0).getResId());
    documentFiles = this.publicationDocumentFileService.findAll(publication.getResId());
    assertNotNull(documentFiles);
    assertEquals(1, documentFiles.size());
    assertEquals(DocumentFileStatus.READY, documentFiles.get(0).getStatus());
  }

  @Test
  public void testCreateNonIsoFileNameTwiceByUpload() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));

    boolean fileCopied = this.copyFileToUploadToNonIsoFilename();
    assertTrue(fileCopied);

    // The file is not inside the JAR, we need to load it with full path and avoid using ClassPathReource class
    final File file = new File(Path.of("").toAbsolutePath() + File.separator + TEST_FILE_TO_UPLOAD_NON_ISO_8859_1_NAME);
    final long fileToUploadSize = file.length();
    final FileSystemResource fileToUpload = new FileSystemResource(file.getAbsolutePath());

    this.uploadDocumentFile(publication, fileToUpload, FILE_NAME_REPLACEMENT);

    // upload the same file a second time: it should create a 2nd documentFile with filename suffix 1 "file1.pdf"
    final String label2 = "label2";
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add("documentFileTypeId", documentFileType.getResId());
    parameters.add("accessLevel", AccessLevel.PRIVATE.name());
    parameters.add("label", label2);
    DocumentFile uploadedDocumentFile2 = this.publicationDocumentFileService.upload(publication.getResId(), fileToUpload, parameters);
    DocumentFile foundDocumentFile2 = this
            .waitForDocumentFileProcessingToBeOver(uploadedDocumentFile2.getPublication().getResId(), uploadedDocumentFile2.getResId());
    assertEquals(DocumentFileStatus.READY, foundDocumentFile2.getStatus());
    assertFalse(foundDocumentFile2.getFinalData() == null || StringTool.isNullOrEmpty(foundDocumentFile2.getFinalData().toString()));
    assertFalse(StringTool.isNullOrEmpty(foundDocumentFile2.getFileName()));
    assertEquals("file1.pdf", foundDocumentFile2.getFileName());
    assertEquals(fileToUploadSize, (long) foundDocumentFile2.getFileSize());
    assertEquals(AccessLevel.PRIVATE, foundDocumentFile2.getAccessLevel());
    assertEquals(label2, foundDocumentFile2.getLabel());
  }

  @Test
  public void testCreateByUploadPasswordProtected() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add("documentFileTypeId", documentFileType.getResId());
    parameters.add("accessLevel", AccessLevel.PUBLIC.name());

    final ClassPathResource fileToUpload = new ClassPathResource(TEST_FILE_TO_UPLOAD_WITH_PASSWORD);
    final long fileToUploadSize = fileToUpload.contentLength();

    DocumentFile uploadedDocumentFile = this.publicationDocumentFileService.upload(publication.getResId(), fileToUpload, parameters);

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(uploadedDocumentFile.getPublication().getResId(), uploadedDocumentFile.getResId());

    assertEquals(DocumentFileStatus.TO_BE_CONFIRMED, foundDocumentFile.getStatus());
    assertEquals("The content of the file cannot be read as it is protected by a password.", foundDocumentFile.getStatusMessage());
    assertFalse(foundDocumentFile.getFinalData() == null || StringTool.isNullOrEmpty(foundDocumentFile.getFinalData().toString()));
    assertFalse(StringTool.isNullOrEmpty(foundDocumentFile.getFileName()));
    assertEquals(TEST_FILE_TO_UPLOAD_WITH_PASSWORD, foundDocumentFile.getFileName());
    assertEquals(fileToUploadSize, (long) foundDocumentFile.getFileSize());
    assertEquals(AccessLevel.PUBLIC, foundDocumentFile.getAccessLevel());
    assertNull(foundDocumentFile.getEmbargoAccessLevel());
    assertNull(foundDocumentFile.getEmbargoEndDate());
  }

  @Test
  public void testConfirmADocumentFile() {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add("documentFileTypeId", documentFileType.getResId());
    parameters.add("accessLevel", AccessLevel.PUBLIC.name());

    final ClassPathResource fileToUpload = new ClassPathResource(TEST_FILE_TO_UPLOAD_WITH_PASSWORD);

    DocumentFile uploadedDocumentFile = this.publicationDocumentFileService.upload(publication.getResId(), fileToUpload, parameters);

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(uploadedDocumentFile.getPublication().getResId(), uploadedDocumentFile.getResId());

    assertEquals(DocumentFileStatus.TO_BE_CONFIRMED, foundDocumentFile.getStatus());
    assertEquals("The content of the file cannot be read as it is protected by a password.", foundDocumentFile.getStatusMessage());

    this.publicationDocumentFileService.confirm(publication.getResId(), uploadedDocumentFile.getResId());

    DocumentFile documentFile2 = this.publicationDocumentFileService.findOne(publication.getResId(), uploadedDocumentFile.getResId());

    assertEquals(DocumentFileStatus.CONFIRMED, documentFile2.getStatus());
  }

  @Test
  public void testCreateByUploadEmptyText() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFileType documentFileType = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add("documentFileTypeId", documentFileType.getResId());
    parameters.add("accessLevel", AccessLevel.PUBLIC.name());

    final ClassPathResource fileToUpload = new ClassPathResource(TEST_FILE_TO_UPLOAD_EMPTY_TEXT);
    final long fileToUploadSize = fileToUpload.contentLength();

    DocumentFile uploadedDocumentFile = this.publicationDocumentFileService.upload(publication.getResId(), fileToUpload, parameters);

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(uploadedDocumentFile.getPublication().getResId(), uploadedDocumentFile.getResId());

    assertEquals(DocumentFileStatus.TO_BE_CONFIRMED, foundDocumentFile.getStatus());
    assertEquals("The file doesn't seem to contain text.", foundDocumentFile.getStatusMessage());
    assertTrue(foundDocumentFile.getFinalData() != null && !StringTool.isNullOrEmpty(foundDocumentFile.getFinalData().toString()));
    assertFalse(StringTool.isNullOrEmpty(foundDocumentFile.getFileName()));
    assertEquals(TEST_FILE_TO_UPLOAD_EMPTY_TEXT, foundDocumentFile.getFileName());
    assertEquals(fileToUploadSize, (long) foundDocumentFile.getFileSize());
    assertEquals(AccessLevel.PUBLIC, foundDocumentFile.getAccessLevel());
    assertNull(foundDocumentFile.getEmbargoAccessLevel());
    assertNull(foundDocumentFile.getEmbargoEndDate());
  }

  @Test
  public void testUpdate() {
    DocumentFile createdDocumentFile = this.createPublicDocumentFile();
    assertNotNull(createdDocumentFile);

    DocumentFileType documentFileTypePublishedArticle = this
            .getDocumentFileType(this.DOCUMENT_FILE_TYPE_PUBLISHED_VERSION_ARTICLE);

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());
    assertNotNull(foundDocumentFile);
    assertEquals(foundDocumentFile.getPublication().getResId(), createdDocumentFile.getPublication().getResId());
    assertEquals(foundDocumentFile.getDocumentFileType().getResId(), documentFileTypePublishedArticle.getResId());
    assertEquals(AccessLevel.PUBLIC, foundDocumentFile.getAccessLevel());
    assertNull(foundDocumentFile.getEmbargoAccessLevel());
    assertNull(foundDocumentFile.getEmbargoEndDate());

    // update
    DocumentFileType documentFileTypeTranslation = this.getDocumentFileType(this.DOCUMENT_FILE_TYPE_TRANSLATION);
    foundDocumentFile.setDocumentFileType(documentFileTypeTranslation);
    foundDocumentFile.setAccessLevel(AccessLevel.RESTRICTED);
    foundDocumentFile.setEmbargoAccessLevel(AccessLevel.PRIVATE);
    foundDocumentFile.setEmbargoEndDate(LocalDate.parse("2025-10-01"));

    DocumentFile updatedDocumentFile = this.publicationDocumentFileService
            .update(foundDocumentFile.getPublication().getResId(), foundDocumentFile.getResId(), foundDocumentFile);

    // check updated values
    DocumentFile foundAgainDocumentFile = this.publicationDocumentFileService
            .findOne(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());

    assertNotNull(foundAgainDocumentFile);
    assertEquals(updatedDocumentFile.getPublication().getResId(), foundAgainDocumentFile.getPublication().getResId());
    assertEquals(documentFileTypeTranslation.getResId(), foundAgainDocumentFile.getDocumentFileType().getResId());
    assertEquals(AccessLevel.RESTRICTED, foundAgainDocumentFile.getAccessLevel());
    assertEquals(AccessLevel.PRIVATE, foundAgainDocumentFile.getEmbargoAccessLevel());
    assertEquals(LocalDate.parse("2025-10-01"), foundAgainDocumentFile.getEmbargoEndDate());
  }

  @Test
  public void testDelete() {
    DocumentFile createdDocumentFile = this.createPublicDocumentFile();
    assertNotNull(createdDocumentFile);

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());
    assertNotNull(foundDocumentFile);

    this.publicationDocumentFileService.delete(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());

    final String publicationId = createdDocumentFile.getPublication().getResId();
    final String documentFileId = createdDocumentFile.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.publicationDocumentFileService.findOne(publicationId, documentFileId));
  }

  @Test
  public void testDownload() {
    DocumentFile createdDocumentFile = this.createPublicDocumentFile();
    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());

    assertEquals(DocumentFileStatus.READY, foundDocumentFile.getStatus());

    // Download the file
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.publicationDocumentFileService.downloadDocumentFile(foundDocumentFile.getPublication().getResId(), foundDocumentFile.getResId(), path);
    assertEquals(TEST_FILE_URL_SIZE, path.toFile().length());
  }

  @Test
  public void testDownloadPermissionOfPublicFile() {
    this.restClientTool.sudoAdmin();
    DocumentFile createdDocumentFile = this.createPublicDocumentFile();

    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());
    this.restClientTool.exitSudo();

    assertEquals(DocumentFileStatus.READY, foundDocumentFile.getStatus());

    // Download the file
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    String publicationId1 = foundDocumentFile.getPublication().getResId();
    String publicationId2 = foundDocumentFile.getResId();
    this.publicationDocumentFileService.downloadDocumentFile(publicationId1, publicationId2, path);
    assertEquals(TEST_FILE_URL_SIZE, path.toFile().length());
  }

  @Test
  public void testDownloadPermissionOfRestrictedFile() {
    this.restClientTool.sudoAdmin();
    DocumentFile createdDocumentFile = this.createRestrictedDocumentFile();

    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());
    this.restClientTool.exitSudo();

    assertEquals(DocumentFileStatus.READY, foundDocumentFile.getStatus());

    // Download the file
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    String publicationId1 = foundDocumentFile.getPublication().getResId();
    String publicationId2 = foundDocumentFile.getResId();
    this.publicationDocumentFileService.downloadDocumentFile(publicationId1, publicationId2, path);
    assertEquals(TEST_FILE_URL_SIZE, path.toFile().length());
  }

  @Test
  public void testDownloadPermissionOfPrivateFile() {
    this.restClientTool.sudoAdmin();
    DocumentFile createdDocumentFile = this.createPrivateDocumentFile();

    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());
    this.restClientTool.exitSudo();

    assertEquals(DocumentFileStatus.READY, foundDocumentFile.getStatus());

    // Download the file
    final Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    final String publicationId1 = foundDocumentFile.getPublication().getResId();
    final String publicationId2 = foundDocumentFile.getResId();
    assertThrows(HttpClientErrorException.Forbidden.class,
            () -> this.publicationDocumentFileService.downloadDocumentFile(publicationId1, publicationId2, path));
  }

  @Test
  public void testDownloadPermissionAsContributor() {
    User myUser = this.userService.getAuthenticatedUser();
    Person myPerson = this.getLinkedPersonToUser(myUser);

    //add the current person as contributor
    Map<String, Object> properties = new HashMap<>();

    properties.put("title", this.getTemporaryTestLabel("download test"));
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));

    List<AbstractContributor> contributorDTOs = new ArrayList<>();

    ContributorDTO contributorDto1 = new ContributorDTO();
    contributorDto1.setFirstName(myPerson.getFirstName());
    contributorDto1.setLastName(myPerson.getLastName());
    contributorDto1.setRole("author");
    contributorDto1.setCnIndividu(StringTool.substringBefore(myUser.getExternalUid(), "@"));
    contributorDTOs.add(contributorDto1);

    properties.put("contributors", contributorDTOs);

    this.restClientTool.sudoAdmin();
    Publication publication = this.createPublicationWithContributor("publication for testing download permission for contributors", properties);
    DocumentFile createdDocumentFile = this.createDocumentFileForPublication(publication, DocumentFile.AccessLevel.PUBLIC);

    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());

    DocumentFile foundDocumentFile = this
            .waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());
    this.restClientTool.exitSudo();

    assertEquals(DocumentFileStatus.READY, foundDocumentFile.getStatus());
    this.restClientTool.exitSudo();

    // Download the file
    Path path = Paths.get(System.getProperty("java.io.tmpdir"), "download.tmp");
    this.publicationDocumentFileService.downloadDocumentFile(foundDocumentFile.getPublication().getResId(), foundDocumentFile.getResId(), path);
    assertEquals(TEST_FILE_URL_SIZE, path.toFile().length());
  }

  @Test
  public void testInvalidDocumentFileForPublicationSubtype() {
    // create 'Article' publication
    final Publication createdPublication = this.createPublication(this.PUBLICATION_TYPE_ARTICLE_NAME,
            this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);

    // add 'Master thesis' document file
    this.addDocumentFile(this.DOCUMENT_FILE_TYPE_MASTER_THESIS, createdPublication);

    // update publication to update metadata which triggers validation
    createdPublication.setMetadata(this.getMetadata(Map.of("title", this.getTemporaryTestLabel("validation test updated"))));
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class,
            () -> this.publicationService.update(createdPublication.getResId(), createdPublication));
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("files.files.0.type",
            "The document file type 'Master thesis' is not allowed for the publication type 'Article scientifique'", validationErrors));
  }

  @Test
  public void testValidDocumentFileForPublicationSubtype() {
    // create 'Master' publication
    final Publication createdPublication = this.createPublication(this.PUBLICATION_TYPE_DIPLOME_NAME, this.PUBLICATION_SUBTYPE_MASTER_NAME);

    // add 'Master thesis' document file
    this.addDocumentFile(this.DOCUMENT_FILE_TYPE_MASTER_THESIS, createdPublication);

    // update publication to update metadata which triggers validation
    createdPublication.setMetadata(this.getMetadata(Map.of("title", this.getTemporaryTestLabel("validation test updated"))));
    assertDoesNotThrow(() -> this.publicationService.update(createdPublication.getResId(), createdPublication));
  }

  @Test
  void testGetFulltext() throws IOException {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    DocumentFile documentFile = this.uploadDocumentFile(publication, new ClassPathResource(TEST_FILE_TO_UPLOAD));
    assertNotNull(documentFile);

    final String documentFileResId = documentFile.getResId();
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.documentFileService.fulltext(documentFileResId);
    });
    assertEquals(HttpStatus.FORBIDDEN, e.getStatusCode());
  }

  private DocumentFile createPublicDocumentFile() {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    return this.createDocumentFileForPublication(publication, DocumentFile.AccessLevel.PUBLIC);
  }

  private DocumentFile createRestrictedDocumentFile() {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    return this.createDocumentFileForPublication(publication, AccessLevel.RESTRICTED);
  }

  private DocumentFile createPrivateDocumentFile() {
    Publication publication = this.createPublicationFixture(this.getTemporaryTestLabel("Publication to create document"));
    return this.createDocumentFileForPublication(publication, AccessLevel.PRIVATE);
  }

  private boolean copyFileToUploadToNonIsoFilename() throws IOException {
    return this.copyFileToUploadToOtherName(TEST_FILE_TO_UPLOAD_NON_ISO_8859_1_NAME);
  }

  private boolean copyFileToUploadToOtherName(String otherFileName) throws IOException {
    InputStream inputStream = new ClassPathResource(
            TEST_FILE_TO_UPLOAD).getInputStream(); // File is inside a Jar need to get the Inputstream firts
    File file = new File(Path.of("").toAbsolutePath() + File.separator + otherFileName);
    return this.copyInputStreamToFile(inputStream, file); // copy the file in the working directory
  }

  private boolean copyInputStreamToFile(InputStream input, File file) throws IOException {
    // append = false
    try (OutputStream output = new FileOutputStream(file, false)) {
      input.transferTo(output);
    }
    return true;
  }

  private Publication createPublication(String type, String subtype) {
    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "validation test");
    properties.put("type", type);
    properties.put("subtype", subtype);
    properties.put("languages", List.of(AouConstants.LANG_CODE_ENGLISH));
    List<MetadataDates> metadataDates = new ArrayList<>();
    MetadataDates date = new MetadataDates();
    date.setType("first_online");
    date.setContent("2022");
    metadataDates.add(date);
    properties.put("dates", metadataDates);
    String xmlMetadata = this.getMetadata(properties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.FILES); // to validate document file types
    return this.publicationService.create(publication);
  }

  private void addDocumentFile(String documentFileTypeValue, Publication publication) {
    DocumentFileType documentFileType = this.getDocumentFileType(documentFileTypeValue);
    DocumentFile documentFile = new DocumentFile();
    documentFile.setPublication(publication);
    documentFile.setDocumentFileType(documentFileType);
    documentFile.setSourceData(URI.create(TEST_FILE_URL));
    documentFile.setAccessLevel(AccessLevel.PUBLIC);
    DocumentFile createdDocumentFile = this.publicationDocumentFileService.create(publication.getResId(), documentFile);
    assertNotNull(createdDocumentFile);
    assertEquals(DocumentFileStatus.RECEIVED, createdDocumentFile.getStatus());
    this.waitForDocumentFileProcessingToBeOver(createdDocumentFile.getPublication().getResId(), createdDocumentFile.getResId());
  }

}
