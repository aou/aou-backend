/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - DepositMetadataValidationV2IT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.solidify.validation.FieldValidationError;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.xml.deposit.v2_4.AuthorRole;
import ch.unige.aou.model.xml.deposit.v2_4.Collaboration;
import ch.unige.aou.model.xml.deposit.v2_4.Contributor;
import ch.unige.aou.model.xml.deposit.v2_4.DateTypes;
import ch.unige.aou.model.xml.deposit.v2_4.DepositDoc;

class DepositMetadataValidationV2IT extends PublicationIT {

  private static final String VALID_ISBN = "2-7654-1005-4";
  private static final String VALID_ISSN_1 = "1661-464X";
  private static final String VALID_ISSN_2 = "1661-4645";
  private static final String VALID_METADATA_DATE_1 = "2021";
  private static final String VALID_FORM_DATE_1 = "2021";
  private static final String VALID_METADATA_DATE_2 = "2020-10-04";
  private static final String VALID_FORM_DATE_2 = "04.10.2020";
  private static final String VALID_METADATA_DATE_3 = "2020-10";

  @Override
  protected String getCurrentUserId() {
    return this.TEST_USER_USER_ID;
  }

  @Test
  void testMinimumData() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication.getSubtype().getName());
    assertNull(savedPublication.getSubSubtype());
  }

  @Test
  void testNoTitle() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.remove("title");
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("type.title", "This field cannot be empty", validationErrors));
  }

  @Test
  void testNoLanguage() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.remove("languages");
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.languages", "At least one language is mandatory", validationErrors));
  }

  @Test
  void testNoType() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.remove("type");
    metadataProperties.remove("subtype");
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("type.type", "The type cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("type.subtype", "The subtype cannot be empty", validationErrors));
  }

  @Test
  void testInvalidTypeOrSubtype() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("type", "INVALID");

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("type.type", "This type doesn't exist", validationErrors));

    metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("subtype", "INVALID");
    xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication2 = new Publication();
    publication2.setMetadata(xmlMetadata);
    publication2.setFormStep(Publication.DepositFormStep.TYPE);
    e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication2);
    });
    validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("type.subtype", "This subtype doesn't exist", validationErrors));
  }

  @Test
  void testValidSubSubType() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("subSubtype", this.PUBLICATION_SUBSUBTYPE_LETTRE_NAME);
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBSUBTYPE_LETTRE_NAME, savedPublication.getSubSubtype().getName());
  }

  @Test
  void testInvalidSubSubType() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("subSubtype", "Billet de blog");
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.TYPE);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("type.subsubtype", "This sub-subtype cannot be associated with the selected subtype",
            validationErrors));
  }

  @Test
  void testContributorsProperties() {
    Map<String, Object> metadataProperties = this.getBasicThesisMetadataProperties();

    List<AbstractContributor> contributors = new ArrayList<>();

    ContributorDTO contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("John");
    contributorDTO.setLastName("Doe");
    contributorDTO.setRole("author");
    contributorDTO.setCnIndividu("12345");
    contributors.add(contributorDTO);

    CollaborationDTO collaborationDTO = new CollaborationDTO();
    collaborationDTO.setName("My collaboration");
    contributors.add(collaborationDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Mika");
    contributorDTO.setLastName("Mou");
    contributorDTO.setRole("director");
    contributorDTO.setCnIndividu("8596");
    contributors.add(contributorDTO);

    metadataProperties.put("contributors", contributors);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());

    DepositDoc savedDepositDoc = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertEquals("John", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
    assertEquals("Doe", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
    assertEquals(AuthorRole.AUTHOR.name(),
            ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getRole().name());
    assertEquals("12345", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getCnIndividu());

    assertEquals("My collaboration", ((Collaboration) savedDepositDoc.getContributors().getContributorOrCollaboration().get(1)).getName());

    assertEquals("Mika", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getFirstname());
    assertEquals("Mou", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getLastname());
    assertEquals(AuthorRole.DIRECTOR.name(),
            ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getRole().name());
    assertEquals("8596", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getCnIndividu());
  }

  @Test
  void testContributorsInvalidProperties() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();

    List<AbstractContributor> contributors = new ArrayList<>();

    ContributorDTO contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("");
    contributorDTO.setLastName("Doe");
    contributorDTO.setRole("author");
    contributorDTO.setCnIndividu("12345");
    contributors.add(contributorDTO);

    CollaborationDTO collaborationDTO = new CollaborationDTO();
    collaborationDTO.setName("");
    contributors.add(collaborationDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Mika");
    contributorDTO.setLastName("");
    contributorDTO.setRole("");
    contributorDTO.setCnIndividu("147258");
    contributors.add(contributorDTO);

    metadataProperties.put("contributors", contributors);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertFalse(this.validationErrorsContain("contributors.contributors.0.firstname", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("contributors.contributors.1.name", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("contributors.contributors.2.lastname", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("contributors.contributors.2.role", "This field cannot be empty", validationErrors));
  }

  @Test
  void testAuthorAndDirectorPresentForThesis() {

    Map<String, Object> metadataProperties = new HashMap<>();
    metadataProperties.put("title", "validation test");
    metadataProperties.put("languages", List.of(this.PUBLICATION_TEST_LANGUAGE));
    metadataProperties.put("type", this.PUBLICATION_TYPE_DIPLOME_NAME);
    metadataProperties.put("subtype", this.PUBLICATION_SUBTYPE_THESE_NAME);

    List<AbstractContributor> contributors = new ArrayList<>();

    ContributorDTO contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("John");
    contributorDTO.setLastName("Doe");
    contributorDTO.setRole("author");
    contributorDTO.setCnIndividu("12345");
    contributors.add(contributorDTO);

    CollaborationDTO collaborationDTO = new CollaborationDTO();
    collaborationDTO.setName("My collaboration");
    contributors.add(collaborationDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Mika");
    contributorDTO.setLastName("Mou");
    contributorDTO.setRole("director");
    contributorDTO.setCnIndividu("8596");
    contributors.add(contributorDTO);

    metadataProperties.put("contributors", contributors);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());

    DepositDoc savedDepositDoc = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertEquals("John", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getFirstname());
    assertEquals("Doe", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getLastname());
    assertEquals(AuthorRole.AUTHOR.name(),
            ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getRole().name());
    assertEquals("12345", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(0)).getCnIndividu());

    assertEquals("My collaboration", ((Collaboration) savedDepositDoc.getContributors().getContributorOrCollaboration().get(1)).getName());

    assertEquals("Mika", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getFirstname());
    assertEquals("Mou", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getLastname());
    assertEquals(AuthorRole.DIRECTOR.name(),
            ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getRole().name());
    assertEquals("8596", ((Contributor) savedDepositDoc.getContributors().getContributorOrCollaboration().get(2)).getCnIndividu());
  }

  @Test
  void testContributorsMissingAuthorForThesis() {
    Map<String, Object> metadataProperties = new HashMap<>();
    metadataProperties.put("title", "validation test");
    metadataProperties.put("languages", List.of(this.PUBLICATION_TEST_LANGUAGE));
    metadataProperties.put("type", this.PUBLICATION_TYPE_DIPLOME_NAME);
    metadataProperties.put("subtype", this.PUBLICATION_SUBTYPE_THESE_NAME);

    List<AbstractContributor> contributors = new ArrayList<>();

    ContributorDTO contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("John");
    contributorDTO.setLastName("Doe");
    contributorDTO.setRole("director");
    contributorDTO.setCnIndividu("12345");
    contributors.add(contributorDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Mika");
    contributorDTO.setLastName("Mou");
    contributorDTO.setRole("director");
    contributorDTO.setCnIndividu("8596");
    contributors.add(contributorDTO);

    metadataProperties.put("contributors", contributors);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("contributors.contributors",
            "Missing author for this subtype of publication: '" + this.PUBLICATION_SUBTYPE_THESE_NAME + "'", validationErrors));
  }

  @Test
  void testContributorInvalidRole() {
    Map<String, Object> metadataProperties = new HashMap<>();
    metadataProperties.put("title", "validation test");
    metadataProperties.put("languages", List.of(this.PUBLICATION_TEST_LANGUAGE));
    metadataProperties.put("type", this.PUBLICATION_TYPE_DIPLOME_NAME);
    metadataProperties.put("subtype", this.PUBLICATION_SUBTYPE_THESE_NAME);

    List<AbstractContributor> contributors = new ArrayList<>();

    ContributorDTO contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("John");
    contributorDTO.setLastName("Doe");
    contributorDTO.setRole("author");
    contributorDTO.setCnIndividu("12345");
    contributors.add(contributorDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Mika");
    contributorDTO.setLastName("Mou");
    contributorDTO.setRole("director");
    contributorDTO.setCnIndividu("8596");
    contributors.add(contributorDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Albert");
    contributorDTO.setLastName("Levert");
    contributorDTO.setRole("editor");
    contributorDTO.setCnIndividu("45789");
    contributors.add(contributorDTO);

    metadataProperties.put("contributors", contributors);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("contributors.contributors.2.role",
            "The author type 'editor' is not allowed for the publication type 'Thèse'", validationErrors));
  }

  @Test
  void testRepeatedContributors() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();

    List<AbstractContributor> contributors = new ArrayList<>();

    ContributorDTO contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("");
    contributorDTO.setLastName("Doe");
    contributorDTO.setRole("author");
    contributorDTO.setCnIndividu("12345");
    contributors.add(contributorDTO);

    CollaborationDTO collaborationDTO = new CollaborationDTO();
    collaborationDTO.setName("");
    contributors.add(collaborationDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Mika");
    contributorDTO.setLastName("");
    contributorDTO.setRole("");
    contributorDTO.setCnIndividu("12345");
    contributors.add(contributorDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("Dalton");
    contributorDTO.setLastName("Jack");
    contributorDTO.setRole("author");
    contributors.add(contributorDTO);

    contributorDTO = new ContributorDTO();
    contributorDTO.setFirstName("");
    contributorDTO.setLastName("Averell");
    contributorDTO.setRole("author");
    contributors.add(contributorDTO);

    metadataProperties.put("contributors", contributors);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.CONTRIBUTORS);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertFalse(this.validationErrorsContain("contributors.contributors.0.firstname", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("contributors.contributors.1.name", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("contributors.contributors.2.lastname", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("contributors.contributors.2.role", "This field cannot be empty", validationErrors));
    assertTrue(this.validationErrorsContain("contributors.contributors.2.cnIndividu", "This contributor appears more than once",
            validationErrors));
    assertFalse(this.validationErrorsContain("contributors.contributors.3.cnIndividu", "This contributor appears more than once",
            validationErrors));
    assertFalse(this.validationErrorsContain("contributors.contributors.4.firstname", "This field cannot be empty", validationErrors));
    assertFalse(this.validationErrorsContain("contributors.contributors.4.cnIndividu", "This contributor appears more than once",
            validationErrors));
  }

  @Test
  void testValidISBN() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("isbn", VALID_ISBN);
    metadataProperties.put("dates", this.getValidDatesList());
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication.getSubtype().getName());

    DepositDoc savedDepositDoc = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertEquals(VALID_ISBN, savedDepositDoc.getIdentifiers().getIsbn());
  }

  @Test
  void testInvalidISBN() {
    String invalidIsbn = "INVALID";
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("isbn", invalidIsbn);
    metadataProperties.put("dates", this.getValidDatesList());
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.identifiers.isbn", "The value '" + invalidIsbn + "' is not a valid ISBN",
            validationErrors));
  }

  @Test
  void testValidISSN1() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("issn", VALID_ISSN_1);
    metadataProperties.put("dates", this.getValidDatesList());
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication.getSubtype().getName());

    DepositDoc savedDepositDoc = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertEquals(VALID_ISSN_1, savedDepositDoc.getIdentifiers().getIssn());
  }

  @Test
  void testValidISSN2() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("issn", VALID_ISSN_2);
    metadataProperties.put("dates", this.getValidDatesList());
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication.getSubtype().getName());

    DepositDoc savedDepositDoc = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertEquals(VALID_ISSN_2, savedDepositDoc.getIdentifiers().getIssn());
  }

  @Test
  void testInvalidISSN() {
    String invalidIssn = "some invalid value";
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("issn", invalidIssn);
    metadataProperties.put("dates", this.getValidDatesList());
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.identifiers.issn", "The value '" + invalidIssn + "' is not a valid ISSN",
            validationErrors));
  }

  @Test
  void testValidDates() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();

    List<MetadataDates> metadataDates = this.getValidDatesList();

    MetadataDates date = new MetadataDates();
    date.setType(AouConstants.DATE_TYPE_FIRST_ONLINE);
    date.setContent(VALID_METADATA_DATE_2);
    metadataDates.add(date);
    metadataProperties.put("dates", metadataDates);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication.getSubtype().getName());

    DepositDoc savedDepositDoc1 = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertTrue(savedDepositDoc1.getDates().getDate().stream()
            .anyMatch(d -> d.getType().equals(DateTypes.fromValue(AouConstants.DATE_TYPE_PUBLICATION))));
    assertTrue(savedDepositDoc1.getDates().getDate().stream().anyMatch(d -> d.getContent().equals(VALID_METADATA_DATE_1)));
    assertTrue(savedDepositDoc1.getDates().getDate().stream()
            .anyMatch(d -> d.getType().equals(DateTypes.fromValue(AouConstants.DATE_TYPE_FIRST_ONLINE))));
    assertTrue(savedDepositDoc1.getDates().getDate().stream().anyMatch(d -> d.getContent().equals(VALID_METADATA_DATE_2)));

    // dates formatted for frontend
    assertTrue(savedPublication.getFormData().contains(VALID_FORM_DATE_1));
    assertTrue(savedPublication.getFormData().contains(VALID_FORM_DATE_2));

    List<MetadataDates> metadataDates2 = this.getValidDatesList();

    MetadataDates dates1 = new MetadataDates();
    dates1.setType(AouConstants.DATE_TYPE_FIRST_ONLINE);
    dates1.setContent(VALID_METADATA_DATE_3);
    metadataDates2.add(dates1);
    metadataProperties.put("dates", metadataDates2);

    String xmlMetadata2 = this.getMetadata(metadataProperties);
    Publication publication2 = new Publication();
    publication2.setMetadata(xmlMetadata2);
    publication2.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication savedPublication2 = this.publicationService.create(publication2);
    assertNotNull(savedPublication2);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication2.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication2.getSubtype().getName());

    DepositDoc savedDepositDoc2 = this.createDepositDocFromXml(savedPublication2.getMetadata());
    assertTrue(savedDepositDoc2.getDates().getDate().stream()
            .anyMatch(d -> d.getType().equals(DateTypes.fromValue(AouConstants.DATE_TYPE_PUBLICATION))));
    assertTrue(savedDepositDoc2.getDates().getDate().stream().anyMatch(d -> d.getContent().equals(VALID_METADATA_DATE_1)));
    assertTrue(savedDepositDoc2.getDates().getDate().stream()
            .anyMatch(d -> d.getType().equals(DateTypes.fromValue(AouConstants.DATE_TYPE_FIRST_ONLINE))));
    assertTrue(savedDepositDoc2.getDates().getDate().stream().anyMatch(d -> d.getContent().equals(VALID_METADATA_DATE_3)));
  }

  @Test
  void testInvalidDates() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();

    List<MetadataDates> metadataDates = new ArrayList<>();

    MetadataDates date1 = new MetadataDates();
    date1.setType(AouConstants.DATE_TYPE_PUBLICATION);
    date1.setContent("2021-12-34");
    metadataDates.add(date1);

    MetadataDates date2 = new MetadataDates();
    date2.setType(AouConstants.DATE_TYPE_FIRST_ONLINE);
    date2.setContent("110");
    metadataDates.add(date2);
    metadataProperties.put("dates", metadataDates);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);

    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> this.publicationService.create(publication));
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.dates.publication", "This date is not a valid date", validationErrors));
    assertTrue(this.validationErrorsContain("description.dates.first_online", "This date is not a valid date", validationErrors));
  }

  @Test
  void testMissingDates() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.dates", "At least one date is mandatory", validationErrors));
  }

  @Test
  void testEmptyDates() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();

    List<MetadataDates> metadataDates = new ArrayList<>();
    MetadataDates date1 = new MetadataDates();
    date1.setType(AouConstants.DATE_TYPE_PUBLICATION);
    date1.setContent("");
    metadataDates.add(date1);
    MetadataDates date2 = new MetadataDates();
    date2.setType(AouConstants.DATE_TYPE_FIRST_ONLINE);
    date2.setContent(null);
    metadataDates.add(date2);
    metadataProperties.put("dates", metadataDates);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    HttpClientErrorException e = assertThrows(HttpClientErrorException.class, () -> {
      this.publicationService.create(publication);
    });
    List<FieldValidationError> validationErrors = this.getValidationErrors(e.getResponseBodyAsString());
    assertTrue(this.validationErrorsContain("description.dates.publication", "At least one date is mandatory", validationErrors));
    assertTrue(this.validationErrorsContain("description.dates.first_online", "At least one date is mandatory", validationErrors));
  }

  @Test
  void testCorrections() {
    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();

    String correctionNote = "A correction note";
    String correctionDoi = "new-doi.123";

    Map<String, Object> correction = new HashMap<>();
    correction.put("note", correctionNote);
    correction.put("doi", correctionDoi);
    metadataProperties.put("correction", correction);

    metadataProperties.put("dates", this.getValidDatesList());

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication savedPublication = this.publicationService.create(publication);
    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel("validation test"), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication.getSubtype().getName());

    DepositDoc savedDepositDoc = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertEquals(correctionNote, savedDepositDoc.getCorrections().getCorrection().get(0).getNote());
    assertEquals(correctionDoi, savedDepositDoc.getCorrections().getCorrection().get(0).getDoi());
  }

  @Test
  void testCleanValues() {
    final String title = "my title";
    final String originalTitle = "original title";
    final String mandator = "my mandator";
    final String edition = "edition 22";
    final String award = "some award";
    final String editor = "an editor";
    final String volume = "volume 1";
    final String conferencePlace = "conference place";
    final String specialIssue = "special issue";
    final String collectionName = "collection name";
    final String collectionNumber = "collection number";

    Map<String, Object> metadataProperties = this.getBasicMetadataProperties();
    metadataProperties.put("isbn", this.getValueToClean(VALID_ISBN));
    metadataProperties.put("dates", this.getValidDatesList());
    metadataProperties.put("title", this.getValueToClean(title));
    metadataProperties.put("originalTitle", this.getValueToClean(originalTitle));
    metadataProperties.put("mandator", this.getValueToClean(mandator));
    metadataProperties.put("edition", this.getValueToClean(edition));
    metadataProperties.put("award", this.getValueToClean(award));
    metadataProperties.put("editor", this.getValueToClean(editor));
    metadataProperties.put("volume", this.getValueToClean(volume));
    metadataProperties.put("conferencePlace", this.getValueToClean(conferencePlace));
    metadataProperties.put("specialIssue", this.getValueToClean(specialIssue));
    Map<String, String> collectionProperties = new HashMap<>();
    collectionProperties.put("name", this.getValueToClean(collectionName));
    collectionProperties.put("number", this.getValueToClean(collectionNumber));
    metadataProperties.put("collection", collectionProperties);

    String xmlMetadata = this.getMetadata(metadataProperties);
    Publication publication = new Publication();
    publication.setMetadata(xmlMetadata);
    publication.setFormStep(Publication.DepositFormStep.DESCRIPTION);
    Publication savedPublication = this.publicationService.create(publication);

    assertNotNull(savedPublication);
    assertEquals(this.getTemporaryTestLabel(title), savedPublication.getTitle());
    assertEquals(this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME, savedPublication.getSubtype().getName());

    DepositDoc savedDepositDoc = this.createDepositDocFromXml(savedPublication.getMetadata());
    assertEquals(VALID_ISBN, savedDepositDoc.getIdentifiers().getIsbn());
    assertEquals(originalTitle, savedDepositDoc.getOriginalTitle().getContent());
    assertEquals(mandator, savedDepositDoc.getMandator());
    assertEquals(edition, savedDepositDoc.getEdition());
    assertEquals(award, savedDepositDoc.getAward());
    assertEquals(editor, savedDepositDoc.getContainer().getEditor());
    assertEquals(volume, savedDepositDoc.getContainer().getVolume());
    assertEquals(conferencePlace, savedDepositDoc.getContainer().getConferencePlace());
    assertEquals(specialIssue, savedDepositDoc.getContainer().getSpecialIssue());
    assertEquals(collectionName, savedDepositDoc.getCollections().getCollection().get(0).getName());
    assertEquals(collectionNumber, savedDepositDoc.getCollections().getCollection().get(0).getNumber());
  }

  private String getValueToClean(String value) {
    value = value.replaceAll(" ", "  ");
    value = "    " + value + "      ";
    return value;
  }

  private List<MetadataDates> getValidDatesList() {
    List<MetadataDates> metadataDates = new ArrayList<>();

    MetadataDates date1 = new MetadataDates();
    date1.setType(AouConstants.DATE_TYPE_PUBLICATION);
    date1.setContent(VALID_METADATA_DATE_1);
    metadataDates.add(date1);

    return metadataDates;
  }

  private Map<String, Object> getBasicMetadataProperties() {
    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "validation test");
    properties.put("languages", List.of(this.PUBLICATION_TEST_LANGUAGE));
    properties.put("type", this.PUBLICATION_TYPE_ARTICLE_NAME);
    properties.put("subtype", this.PUBLICATION_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME);
    return properties;
  }

  private Map<String, Object> getBasicThesisMetadataProperties() {
    Map<String, Object> properties = new HashMap<>();
    properties.put("title", "validation test");
    properties.put("languages", List.of(this.PUBLICATION_TEST_LANGUAGE));
    properties.put("type", AouConstants.DEPOSIT_TYPE_DIPLOME_NAME);
    properties.put("subtype", AouConstants.DEPOSIT_SUBTYPE_THESE_NAME);
    return properties;
  }
}
