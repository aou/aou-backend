/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Integration Tests - StoredSearchAsAdminIT.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.admin;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import ch.unige.aou.model.search.StoredSearch;
import ch.unige.aou.model.security.User;

class StoredSearchAsAdminIT extends StoredSearchIT {

  @Override
  protected void setUser() {
    this.restClientTool.sudoAdmin();
  }

  @Test
  void createAndDeleteTest() {
    StoredSearch newSearch = this.testCreate();

    this.storedSearchClientService.delete(newSearch.getResId());

    final String resId = newSearch.getResId();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.storedSearchClientService.findOne(resId));
  }

  @Test
  void listMySearchesTest() {
    List<StoredSearch> mySearches = this.storedSearchClientService.listMySearches(Map.of());
    assertNotNull(mySearches);
    assertTrue(mySearches.isEmpty());

    StoredSearch search1 = this.getSearch("search1");
    StoredSearch search2 = this.getSearch("search2");
    this.storedSearchClientService.create(search1);
    this.storedSearchClientService.create(search2);

    // create StoredSearch for another user that should not appear in list
    this.restClientTool.sudoUser();
    StoredSearch search3 = this.getSearch("search3");
    this.storedSearchClientService.create(search3);
    this.restClientTool.exitSudo();

    mySearches = this.storedSearchClientService.listMySearches(Map.of());
    assertNotNull(mySearches);
    assertEquals(2, mySearches.size());
  }

  @Test
  void updateTest() {
    this.testUpdate();
  }

  @Test
  void updateSomeoneElseTest() {
    this.restClientTool.sudoUser();
    StoredSearch newSearch = this.testCreate();
    this.restClientTool.exitSudo();

    String newName = newSearch.getName() + " updated";

    newSearch.setName(newName);
    this.storedSearchClientService.update(newSearch.getResId(), newSearch);
    StoredSearch fetchedSearch = this.storedSearchClientService.findOne(newSearch.getResId());
    assertNotNull(fetchedSearch);
    assertEquals(newName, fetchedSearch.getName());
    User currentUser = this.userService.findOne(this.TEST_USER_USER_ID);
    assertEquals(currentUser.getPerson().getResId(), fetchedSearch.getCreator().getResId());
  }

  @Test
  void nameConstraintTest() {
    String existingName = this.testNameConstraint();

    // creating a StoredSearch with the same name for someone else is possible
    this.restClientTool.sudoUser();
    StoredSearch search = this.getSearch("search");
    search.setName(existingName);
    StoredSearch someoneElseSearch = assertDoesNotThrow(() -> this.storedSearchClientService.create(search));
    assertEquals(existingName, someoneElseSearch.getName());
    this.restClientTool.exitSudo();
  }

  @Test
  void listAllSearchesTest() {
    List<StoredSearch> searches = this.storedSearchClientService.findAll();
    assertNotNull(searches);
    int totalBefore = searches.size();

    StoredSearch search1 = this.getSearch("admin's search");
    this.storedSearchClientService.create(search1);

    // create a StoredSearch as someone else
    this.restClientTool.sudoUser();
    StoredSearch search2 = this.getSearch("user's search");
    this.storedSearchClientService.create(search2);
    this.restClientTool.exitSudo();

    // admin see all of them
    searches = this.storedSearchClientService.findAll();
    assertNotNull(searches);
    assertEquals(totalBefore + 2, searches.size());
  }

  @Test
  void getSomeoneElseSearch() {
    this.restClientTool.sudoUser();
    StoredSearch search = this.getSearch("search");
    this.storedSearchClientService.create(search);
    this.restClientTool.exitSudo();

    StoredSearch fetchedSearch = this.storedSearchClientService.findOne(search.getResId());
    User currentUser = this.userService.findOne(this.TEST_USER_USER_ID);
    assertEquals(currentUser.getPerson().getResId(), fetchedSearch.getCreator().getResId());
  }

  @Test
  void deleteSomeoneElseSearch() {
    this.restClientTool.sudoUser();
    StoredSearch search = this.getSearch("search");
    this.storedSearchClientService.create(search);
    this.restClientTool.exitSudo();

    final String resId = search.getResId();
    StoredSearch fetchedSearch = this.storedSearchClientService.findOne(resId);
    assertNotNull(fetchedSearch);
    assertEquals(search.getName(), fetchedSearch.getName());
    User currentUser = this.userService.findOne(this.TEST_USER_USER_ID);
    assertEquals(currentUser.getPerson().getResId(), fetchedSearch.getCreator().getResId());

    assertDoesNotThrow(() -> this.storedSearchClientService.delete(resId));

    assertThrows(HttpClientErrorException.NotFound.class, () -> this.storedSearchClientService.findOne(resId));
  }

  @Test
  void deleteListSomeoneElseSearches() {
    this.restClientTool.sudoUser();
    StoredSearch search1 = this.getSearch("search1");
    this.storedSearchClientService.create(search1);
    StoredSearch search2 = this.getSearch("search2");
    this.storedSearchClientService.create(search2);
    final String resId1 = search1.getResId();
    final String resId2 = search2.getResId();
    StoredSearch fetchedSearch1 = this.storedSearchClientService.findOne(resId1);
    assertNotNull(fetchedSearch1);
    StoredSearch fetchedSearch2 = this.storedSearchClientService.findOne(resId2);
    assertNotNull(fetchedSearch2);
    this.restClientTool.exitSudo();

    String[] resIds = { resId1, resId2 };
    assertDoesNotThrow(() -> this.storedSearchClientService.deleteList(resIds));

    this.restClientTool.sudoUser();
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.storedSearchClientService.findOne(resId1));
    assertThrows(HttpClientErrorException.NotFound.class, () -> this.storedSearchClientService.findOne(resId2));
    this.restClientTool.exitSudo();
  }
}
