# DLCM Client

## For running integration test on your IDE

1. Create a `integration-test-config-local.properties` file with the following content :
    ```
    config.server.protocol=http
    config.server.domain=localhost
    config.server.port=8897
    spring.cloud.config.uri=${config.server.protocol:https}://${config.server.login:}:${config.server.password:}@${config.server.domain:config.dlcm.ch}:${config.server.port:8887}
    spring.cloud.config.enabled=true
    spring.cloud.config.fail-fast=true
    spring.cloud.config.label=yourname (ex : `poittevf` in my case)
    spring.profiles.include=ff-notool,vir-notool
    ```
2. Create a `integration-test-config-remote.properties` file with the following content :
    ```
    config.server.login=sandbox
    config.server.password=gs343GwrvdsFGV32r425SDVGdsFG2442DSVsdXV3234DSGFwdfg
    spring.cloud.config.uri=${config.server.protocol:https}://${config.server.login:}:${config.server.password:}@${config.server.domain:config.dlcm.ch}:${config.server.port:8887}
    spring.cloud.config.enabled=true
    spring.cloud.config.fail-fast=true
    spring.cloud.config.label=integration_test
    spring.profiles.include=ff-notool,vir-notool
    ```
3. Create a `Run/Configuration` profile of type JUnit
4. Add the following flag on the VM options of your JUnit Run/Configuration profile :
- `-Dspring.application.name=DLCM-Client`
- `-Dspring.cloud.bootstrap.location=/path/to/your/integration-test-[config-local or config-remote].properties`
