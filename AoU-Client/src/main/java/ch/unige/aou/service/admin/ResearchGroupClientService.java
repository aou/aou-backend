/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - ResearchGroupClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class ResearchGroupClientService extends ResourceClientService<ResearchGroup> {

  public ResearchGroupClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.RESEARCH_GROUPS, ResearchGroup.class, restClientTool,
            env);
  }

  public void addPerson(String researchGroupId, String personId) {
    this.addItem(researchGroupId, ResourceName.PEOPLE, personId, Person.class);
  }

  public List<Person> getPeople(String researchGroupId) {
    return this.findAllLinkedResources(researchGroupId, ResourceName.PEOPLE, Person.class);
  }

  public void removePerson(String researchGroupId, String personId) {
    this.removeItem(researchGroupId, ResourceName.PEOPLE, personId);
  }

  public ResearchGroup validate(String resId) {
    String url = this.getResourceUrl() + "/" + resId + "/" + AouActionName.VALIDATE;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.exchange(url, HttpMethod.POST, null, ResearchGroup.class).getBody();
  }

  @Override
  public List<ResearchGroup> findAll() {
    return super.findAll(ResearchGroup.class);
  }

  @Override
  public List<ResearchGroup> searchByProperties(Map<String, String> properties) {
    return super.searchByProperties(properties, ResearchGroup.class);
  }

}
