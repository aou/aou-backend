/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - PersonClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.display.StructureValidationRight;
import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.security.Role;
import ch.unige.aou.model.settings.Institution;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class PersonClientService extends ResourceClientService<Person> {

  private static final Logger log = LoggerFactory.getLogger(PersonClientService.class);

  public PersonClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.PEOPLE, Person.class, restClientTool, env);
  }

  @Override
  public List<Person> findAll() {
    return this.findAll(Person.class);
  }

  public List<Person> searchWithUser(String search) {
    String url = this.getResourceUrl() + "/" + AouActionName.SEARCH_WITH_USER + "?search=" + search;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Person.class);
  }

  public List<Person> search(String search, String matchType) {
    String url = this.getResourceUrl() + "/search?search=" + search;
    if (!StringTool.isNullOrEmpty(matchType)) {
      url += "&match=" + matchType;
    }
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Person.class);
  }

  @Override
  public List<Person> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Person.class);
  }

  /****************************************************************************************************/

  public Person uploadAvatar(String resId, Resource file) {
    return this.uploadFile(resId, AouActionName.UPLOAD_AVATAR, file);
  }

  public void downloadAvatar(String resId, Path path) {
    this.downloadFile(resId, AouActionName.DOWNLOAD_AVATAR, path);
  }

  public void deleteAvatar(String resId) {
    this.deleteAction(resId, AouActionName.DELETE_AVATAR);
  }

  /****************************************************************************************************/

  public void addResearchGroup(String personId, String researchGroupId) {
    this.addItem(personId, ResourceName.RESEARCH_GROUPS, researchGroupId, ResearchGroup.class);
  }

  public List<ResearchGroup> getResearchGroup(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.RESEARCH_GROUPS, ResearchGroup.class);
  }

  public void removeResearchGroup(String personId, String researchGroupId) {
    this.removeItem(personId, ResourceName.RESEARCH_GROUPS, researchGroupId);
  }

  /****************************************************************************************************/

  public List<Structure> getStructures(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.STRUCTURES, Structure.class);
  }

  public void addStructure(String personResId, String structureId) {
    this.addItem(personResId, ResourceName.STRUCTURES, structureId, Structure.class);
  }

  public void removeStructure(String personId, String structureId) {
    this.removeItem(personId, ResourceName.STRUCTURES, structureId);
  }

  /****************************************************************************************************/

  public void addValidationRightStructure(String personId, String structureId) {
    this.addItem(personId, ResourceName.VALIDATION_RIGHTS_STRUCTURES, structureId, Structure.class);
  }

  public void addValidationRightStructure(String personId, String... structureIds) {
    final Map<String, String> params = new HashMap<>();
    params.put(ResourceClientService.ID_LABEL, personId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    restTemplate.postForObject(this.getAssociationUrl(ResourceName.VALIDATION_RIGHTS_STRUCTURES), structureIds, String.class, params);
  }

  public void addValidationRightStructureWithSubtypes(String personId, String structureId, String... publicationSubtypeIds) {
    this.addRelationItem(personId, ResourceName.VALIDATION_RIGHTS_STRUCTURES + "/" + structureId, PublicationSubtype.class,
            publicationSubtypeIds);
  }

  public void removeValidationRightStructure(String personId, String structureId) {
    this.removeItem(personId, ResourceName.VALIDATION_RIGHTS_STRUCTURES, structureId);
  }

  public void removeValidationRightStructureWithSubtypes(String personId, String structureId, String... publicationSubtypeIds) {
    this.removeItem(personId, ResourceName.VALIDATION_RIGHTS_STRUCTURES, structureId, publicationSubtypeIds);
  }

  public List<StructureValidationRight> getValidationRightStructuresList(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.VALIDATION_RIGHTS_STRUCTURES, StructureValidationRight.class);
  }

  public StructureValidationRight getValidationRightsStructure(String personId, String structureId) {
    final String url = this.getResourceUrl() + "/" + personId + "/" + ResourceName.VALIDATION_RIGHTS_STRUCTURES + "/" + structureId;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, StructureValidationRight.class);
  }

  /****************************************************************************************************/

  public List<NotificationType> getSubscribedNotificationTypes(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.NOTIFICATION_TYPES, NotificationType.class);
  }

  /*
   * Add notification type with default frequency: DAILY
   */

  public void addNotificationType(String personId, String notificationTypeId) {
    this.addItem(personId, ResourceName.NOTIFICATION_TYPES, notificationTypeId, NotificationType.class);
  }

  public void removeNotificationType(String personId, String notificationTypeId) {
    this.removeItem(personId, ResourceName.NOTIFICATION_TYPES, notificationTypeId);
  }

  public void addRole(String personId, String roleId) {
    this.addItem(personId, ResourceName.ROLES, roleId, Role.class);
  }

  public void removeRole(String personId, String roleId) {
    this.removeItem(personId, ResourceName.ROLES, roleId);
  }

  /****************************************************************************************************/

  public List<Institution> getInstitutions(String personId) {
    return this.findAllLinkedResources(personId, ResourceName.INSTITUTIONS, Institution.class);
  }

  public void addInstitution(String personId, String institutionId) {
    this.addItem(personId, ResourceName.INSTITUTIONS, institutionId, Institution.class);
  }

  /****************************************************************************************************/

  public JsonNode findOneAsJsonNode(String resId) {
    final String url = this.getResourceUrl() + SolidifyConstants.URL_ID;
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_ID_FIELD, resId);

    JsonNode result = null;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    try {
      final String jsonString = restTemplate.getForObject(url, String.class, params);
      if (jsonString == null) {
        return null;
      }
      ObjectMapper mapper = new ObjectMapper();
      result = mapper.readValue(jsonString, JsonNode.class);
    } catch (final IOException e) {
      log.error("Error getting object from " + url, e);
    } catch (final HttpClientErrorException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        // No item found return null
        return null;
      } else {
        throw e;
      }
    }
    return result;
  }
}
