/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - ArxivImportClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.imports;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class ArxivImportClientService {

  protected AbstractRestClientTool restClientTool;

  private Environment env;

  private String module;

  private String resourcePath;

  public ArxivImportClientService(SudoRestClientTool restClientTool, Environment env) {
    this.env = env;
    this.restClientTool = restClientTool;
    this.module = ModuleName.ADMIN;
    this.resourcePath = "/" + ResourceName.EXTERNAL_DATA;
  }

  protected String getBaseUrl(String module) {
    String propertyName = "aou.module." + module + ".url";
    String url = this.env.getProperty(propertyName);
    if (url == null) {
      throw new IllegalStateException("Property aou.module." + module + ".url has to be defined.");
    }
    // Manage if there is a list of URLs
    if (url.contains(",")) {
      url = url.substring(0, url.indexOf(','));
    }
    return url;
  }

  public String importFileFromArxiv(String arxivId, String publicationId, String format) {
    String url = this.getBaseUrl(this.module) + this.resourcePath + "/" + AouActionName.IMPORT_FILE_ARXIV + "?arxivId=" + arxivId
            + "&publicationId=" + publicationId + "&format=" + format;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, String.class);
  }
}
