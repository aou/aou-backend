/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - PublicationSubtypeClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileTypeDTO;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class PublicationSubtypeClientService extends ResourceClientService<PublicationSubtype> {

  public PublicationSubtypeClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.PUBLICATION_SUBTYPES, PublicationSubtype.class,
            restClientTool, env);
  }

  public PublicationSubtype findByName(String name) {
    final Map<String, String> properties = new HashMap<>();
    properties.put("name", name);
    List<PublicationSubtype> publicationSubtypes = this.searchByProperties(properties);
    if (publicationSubtypes.size() == 1) {
      return publicationSubtypes.get(0);
    }
    throw new SolidifyRuntimeException("Too many publication subtype with name '" + name + "' found");
  }

  public List<PublicationSubtypeDocumentFileTypeDTO> findDocumentFileTypes(String resId) {
    return this.findDocumentFileTypes(resId, null);
  }

  public List<PublicationSubtypeDocumentFileTypeDTO> findDocumentFileTypes(String resId, DocumentFileType.FileTypeLevel level) {

    String url = this.getResourceUrl() + SolidifyConstants.URL_ID + "/" + AouActionName.LIST_DOCUMENT_FILE_TYPES;
    if (level != null) {
      url += "?level=" + level.name();
    }

    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_ID_FIELD, resId);

    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class, params);
    return CollectionTool.getList(jsonString, PublicationSubtypeDocumentFileTypeDTO.class);
  }

  @Override
  public List<PublicationSubtype> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, PublicationSubtype.class);
  }

  @Override
  public List<PublicationSubtype> findAll() {
    return this.findAll(PublicationSubtype.class);
  }

}
