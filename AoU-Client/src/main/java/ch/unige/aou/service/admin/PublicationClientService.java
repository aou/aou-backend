/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - PublicationClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.contact.ContactableContributor;
import ch.unige.aou.model.contact.PublicationContactMessage;
import ch.unige.aou.model.display.StructureWithJoinInfosDTO;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.rest.PublicationImportDTO;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class PublicationClientService extends ResourceClientService<Publication> {

  public PublicationClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.PUBLICATIONS, Publication.class, restClientTool, env);
  }

  @Override
  public List<Publication> findAll() {
    return this.findAll(Publication.class);
  }

  @Override
  public List<Publication> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Publication.class);
  }

  public Publication findByTitle(String title) {
    final Map<String, String> properties = new HashMap<>();
    properties.put("title", title);
    List<Publication> publications = this.searchByProperties(properties);
    if (publications.size() == 1) {
      return publications.get(0);
    } else if (publications.size() > 1) {
      throw new SolidifyRuntimeException("Too many publication with title '" + title + "' found");
    } else {
      throw new SolidifyRuntimeException("No publication with title '" + title + "' found");
    }
  }

  public List<Publication> listMyPublications(Map<String, String> params) {
    String url = this.getResourceUrl() + "/" + AouActionName.LIST_MY_PUBLICATIONS + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    return this.listPublications(url, params);
  }

  private List<Publication> listPublications(String url, Map<String, String> params) {
    StringBuilder urlBuilder = new StringBuilder(url);
    for (final Map.Entry<String, String> entry : params.entrySet()) {
      urlBuilder.append("&")
              .append(entry.getKey())
              .append("=")
              .append(entry.getValue());
    }
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(urlBuilder.toString(), String.class);
    return CollectionTool.getList(jsonString, Publication.class);
  }

  public Result submit(String publicationId) {
    return this.postAction(publicationId, AouActionName.SUBMIT);
  }

  public Result submitForApproval(String publicationId) {
    return this.postAction(publicationId, AouActionName.SUBMIT_FOR_VALIDATION);
  }

  public Result submitForApproval(String publicationId, String validationStructureId) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.SUBMIT_FOR_VALIDATION;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    HttpEntity<String[]> requestEntity = new HttpEntity<>(new String[] { validationStructureId });
    return restTemplate.postForObject(url, requestEntity, Result.class);
  }

  public Result askFeedback(String publicationId, String reason) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.ASK_FEEDBACK + "?reason={reason}";
    Map<String, String> params = new HashMap<>();
    params.put("reason", reason);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, null, Result.class, params);
  }

  public Result updateStatusToDelete(String publicationId, String reason) {
    return this.postAction(publicationId, AouActionName.DELETE);
  }

  public Result resume(String publicationId) {
    return this.postAction(publicationId, AouActionName.RESUME);
  }

  public Publication clonePublication(String publicationId) {
    final String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.CLONE;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, null, Publication.class);
  }

  /**
   * Imports
   */

  public Publication createFromDOI(String doi) {
    PublicationImportDTO publicationImportDTO = new PublicationImportDTO();
    publicationImportDTO.setDoi(doi);
    return this.createFromIdentifier(publicationImportDTO);
  }

  public Publication createFromDOI(String doi, String creatorId) {
    PublicationImportDTO publicationImportDTO = new PublicationImportDTO();
    publicationImportDTO.setDoi(doi);
    publicationImportDTO.setCreatorId(creatorId);
    return this.createFromIdentifier(publicationImportDTO);
  }

  public Publication createFromPMID(String pmid) {
    PublicationImportDTO publicationImportDTO = new PublicationImportDTO();
    publicationImportDTO.setPmid(pmid);
    return this.createFromIdentifier(publicationImportDTO);
  }

  private Publication createFromIdentifier(PublicationImportDTO publicationImportDTO) {
    String url = this.getResourceUrl() + "/" + AouActionName.CREATE_FROM_IDENTIFIER;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, publicationImportDTO, this.getResourceClass());
  }

  /**
   * Validation
   */

  public List<Publication> listValidablePublications(Map<String, String> params) {
    String url = this.getResourceUrl() + "/" + AouActionName.LIST_VALIDABLE_PUBLICATIONS + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    return this.listPublications(url, params);
  }

  public String validateMetadata(String publicationId) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.VALIDATE_METADATA;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, null, String.class);
  }

  /**
   * Thumbnails
   */

  public Publication uploadThumbnail(String publicationId, Resource resource) {
    return this.uploadFile(publicationId, AouActionName.UPLOAD_THUMBNAIL, resource);
  }

  public void downloadThumbnail(String publicationId, Path path) {
    this.downloadFile(publicationId, AouActionName.DOWNLOAD_THUMBNAIL, path);
  }

  public void deleteThumbnail(String publicationId) {
    this.deleteAction(publicationId, AouActionName.DELETE_THUMBNAIL);
  }

  /**
   * ResearchGroups
   */

  public Result updatePublicationListWithResearchGroup(String researchGroupId, String[] publicationIds) {
    String url = this.getResourceUrl() + "/" + AouActionName.UPDATE_PUBLICATIONS_RESEARCH_GROUP + "?" + AouConstants.DB_RESEARCH_GROUP_ID + "="
            + researchGroupId;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.patchForObject(url, new HttpEntity<>(publicationIds), Result.class);
  }

  public List<ResearchGroup> getResearchGroups(String publicationId) {
    return this.findAllLinkedResources(publicationId, ResourceName.RESEARCH_GROUPS, ResearchGroup.class);
  }

  public void addResearchGroup(String publicationId, String researchGroupId) {
    this.addItem(publicationId, ResourceName.RESEARCH_GROUPS, researchGroupId, ResearchGroup.class);
  }

  public void removeResearchGroup(String publicationId, String researchGroupId) {
    this.removeItem(publicationId, ResourceName.RESEARCH_GROUPS, researchGroupId);
  }

  /**
   * Structures
   */

  public Result updatePublicationListWithStructure(String structureId, String[] publicationIds) {
    String url =
            this.getResourceUrl() + "/" + AouActionName.UPDATE_PUBLICATIONS_STRUCTURE + "?" + AouConstants.DB_STRUCTURE_ID + "=" + structureId;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.patchForObject(url, new HttpEntity<>(publicationIds), Result.class);
  }

  public List<StructureWithJoinInfosDTO> getStructures(String publicationId) {
    return this.findAllLinkedResources(publicationId, ResourceName.STRUCTURES, StructureWithJoinInfosDTO.class);
  }

  public void addStructure(String publicationId, String structureId) {
    this.addItem(publicationId, ResourceName.STRUCTURES, structureId, Structure.class);
  }

  public void removeStructure(String publicationId, String structureId) {
    this.removeItem(publicationId, ResourceName.STRUCTURES, structureId);
  }

  /**
   * Contributors
   */

  public List<Contributor> getContributors(String publicationId) {
    return this.findAllLinkedResources(publicationId, ResourceName.CONTRIBUTORS, Contributor.class);
  }

  public void unlinkContributor(String publicationId) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.UNLINK_CONTRIBUTOR;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    restTemplate.postForObject(url, null, Result.class);
  }

  public Result updateContributorDisplayName(String[] publicationIds, String cnIndividu, String firstName, String lastName) {
    String url = this.getResourceUrl() + "/" + AouActionName.UPDATE_CONTRIBUTOR_DISPLAY_NAME + "?" + "cnIndividu=" + cnIndividu + "&firstname="
            + firstName + "&lastname=" + lastName;

    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.patchForObject(url, new HttpEntity<>(publicationIds), Result.class);
  }

  public List<ContactableContributor> getContributorsContactableByEmail(String publicationId) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.GET_CONTACTABLE_CONTRIBUTORS;

    final RestTemplate restTemplate = this.restClientTool.getClient();
    String jsonString = restTemplate.postForObject(url, null, String.class);
    final ObjectMapper mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    final JavaType listItemType = mapper.getTypeFactory().constructParametricType(List.class, ContactableContributor.class);
    try {
      return mapper.readValue(jsonString, listItemType);
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException("unable to get contactable contributors", e);
    }
  }

  public Result sendContactMessage(String publicationId, PublicationContactMessage contactMessage) {
    String url = this.getResourceUrl() + "/" + publicationId + "/" + AouActionName.CONTACT_CONTRIBUTOR;

    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, contactMessage, Result.class);
  }
}
