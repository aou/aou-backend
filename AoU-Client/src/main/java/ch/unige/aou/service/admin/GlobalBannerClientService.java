/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - GlobalBannerClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.model.GlobalBanner;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class GlobalBannerClientService extends ResourceClientService<GlobalBanner> {

  public GlobalBannerClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.GLOBAL_BANNERS, GlobalBanner.class, restClientTool,
            env);
  }

  public GlobalBanner getActive() {
    final String url = this.getResourceUrl() + "/" + AouActionName.GET_ACTIVE;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, GlobalBanner.class);
  }

  @Override
  public List<GlobalBanner> findAll() {
    return this.findAll(GlobalBanner.class);
  }

  @Override
  public List<GlobalBanner> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, GlobalBanner.class);
  }
}
