/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - LanguageClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.settings.LabeledLanguage;
import ch.unige.aou.model.settings.LanguageDTO;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class LanguageClientService extends ResourceClientService<LabeledLanguage> {

  public LanguageClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.LANGUAGES, LabeledLanguage.class, restClientTool,
            env);
  }

  public List<LanguageDTO> listAll() {
    final String url = this.getResourceUrl() + "?size=" + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, LanguageDTO.class);
  }

  @Override
  public List<LabeledLanguage> findAll() {
    return super.findAll(LabeledLanguage.class);
  }

  @Override
  public List<LabeledLanguage> searchByProperties(Map<String, String> properties) {
    return super.searchByProperties(properties, LabeledLanguage.class);
  }
}
