/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - StructureClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class StructureClientService extends ResourceClientService<Structure> {

  public StructureClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.STRUCTURES, Structure.class, restClientTool, env);
  }

  public List<Person> getPeople(String structureId) {
    return this.findAllLinkedResources(structureId, ResourceName.PEOPLE, Person.class);
  }

  public Person getPerson(String structureId, String personId) {
    return this.findOneLinkedResource(structureId, ResourceName.PEOPLE, Person.class, personId);
  }

  public void addPerson(String structureId, String personId) {
    this.addItem(structureId, ResourceName.PEOPLE, personId, Person.class);
  }

  public void removePerson(String structureId, String personId) {
    this.removeItem(structureId, ResourceName.PEOPLE, personId);
  }

  @Override
  public List<Structure> findAll() {
    return super.findAll(Structure.class);
  }

  @Override
  public List<Structure> searchByProperties(Map<String, String> properties) {
    return super.searchByProperties(properties, Structure.class);
  }

  public Structure update(String resId, String json) {
    final String url = this.getResourceUrl() + SolidifyConstants.URL_ID;
    final RestTemplate restTemplate = this.restClientTool.getClient();

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

    HttpEntity<String> entity = new HttpEntity<>(json, headers);

    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_ID_FIELD, resId);
    return restTemplate.patchForObject(url, entity, Structure.class, params);
  }

}
