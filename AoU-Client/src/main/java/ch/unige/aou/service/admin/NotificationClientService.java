/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - NotificationClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class NotificationClientService extends ResourceClientService<Notification> {

  public NotificationClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.NOTIFICATIONS, Notification.class, restClientTool,
            env);
  }

  public List<Notification> getInboxNotifications() {
    return this.doActionOnList(AouActionName.INBOX, Notification.class);
  }

  public Notification getInboxNotification(String notificationId) {
    return this.doActionWithId(notificationId, AouActionName.INBOX);
  }

  public List<Notification> getInboxNotificationsByStructure(String structureId) {
    final String url = this.getResourceUrl() + "/" + AouActionName.SEARCH_INBOX + "?structureId=" + structureId;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Notification.class);
  }

  public List<Notification> getInboxNotificationsByResearchGroup(String researchGroupId) {
    final String url = this.getResourceUrl() + "/" + AouActionName.SEARCH_INBOX + "?researchGroupId=" + researchGroupId;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, Notification.class);
  }

  public List<Notification> getSentNotifications() {
    return this.doActionOnList(AouActionName.SENT, Notification.class);
  }

  public Notification markAsRead(String notificationId) {
    final String url = this.getResourceUrl() + "/" + notificationId + "/" + AouActionName.SET_READ;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, null, Notification.class);
  }

  @Override
  public List<Notification> findAll() {
    return this.findAll(Notification.class);
  }

  @Override
  public List<Notification> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Notification.class);
  }

}
