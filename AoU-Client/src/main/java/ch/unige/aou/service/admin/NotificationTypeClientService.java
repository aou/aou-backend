/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - NotificationTypeClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.notification.NotificationType;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class NotificationTypeClientService extends ResourceClientService<NotificationType> {

  public NotificationTypeClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.NOTIFICATION_TYPES, NotificationType.class,
            restClientTool,
            env);
  }

  public List<String> getListNotificationsTypesMandatory() {
    final String url = this.getResourceUrl() + "/" + AouActionName.LIST_NOTIFICATIONS_TYPES_MANDATORY;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, List.class);
  }

  @Override
  public List<NotificationType> findAll() {
    return null;
  }

  @Override
  public List<NotificationType> searchByProperties(Map<String, String> properties) {
    return null;
  }
}
