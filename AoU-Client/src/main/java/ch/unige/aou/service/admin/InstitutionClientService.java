/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - InstitutionClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.settings.Institution;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class InstitutionClientService extends ResourceClientService<Institution> {

  public InstitutionClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.INSTITUTIONS, Institution.class, restClientTool, env);
  }

  public void addPerson(String institutionId, String personId) {
    this.addItem(institutionId, ResourceName.PEOPLE, personId, Person.class);
  }

  public List<Person> getPersons(String institutionId) {
    return this.findAllLinkedResources(institutionId, ResourceName.PEOPLE, Person.class);
  }

  public void removePerson(String institutionId, String personId) {
    this.removeItem(institutionId, ResourceName.PEOPLE, personId);
  }

  public Institution uploadLogo(String resId, Resource file) {
    return this.uploadFile(resId, AouActionName.UPLOAD_LOGO, file);
  }

  public void downloadLogo(String resId, Path path) {
    this.downloadFile(resId, AouActionName.DOWNLOAD_LOGO, path);
  }

  public void deleteLogo(String resId) {
    this.deleteAction(resId, AouActionName.DELETE_LOGO);
  }

  @Override
  public List<Institution> findAll() {
    return this.findAll(Institution.class);
  }

  @Override
  public List<Institution> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, Institution.class);
  }

}
