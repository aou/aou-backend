/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - PersonNotificationClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.CompositionResourceClientService;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.notification.Notification;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class PersonNotificationClientService extends CompositionResourceClientService<Person, Notification> {

  private static final String PEOPLE_NOTIFICATION_TYPES_PATH =
          ResourceName.PEOPLE + SolidifyConstants.URL_PARENT_ID + ResourceName.NOTIFICATIONS;

  public PersonNotificationClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, PEOPLE_NOTIFICATION_TYPES_PATH, Notification.class, restClientTool,
            env);
  }

  @Override
  public List<Notification> findAll(String personId) {
    return this.findAll(personId, Notification.class);
  }

  public List<Notification> findAll(String personId, Map<String, String> queryParameters) {
    if (!queryParameters.containsKey("size")) {
      queryParameters.put("size", String.valueOf(RestCollectionPage.MAX_SIZE_PAGE));
    }
    String url = this.getResourceUrl() + "?";
    for (final Map.Entry<String, String> entry : queryParameters.entrySet()) {
      url += "&" + entry.getKey() + "=" + entry.getValue();
    }

    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, personId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class, params);
    return CollectionTool.getList(jsonString, Notification.class);
  }

  public Notification markNotificationAsRead(String personId, String notificationId) {
    final String url = this.getResourceUrl() + SolidifyConstants.URL_ID + "/" + AouActionName.SET_READ;
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, personId);
    params.put(SolidifyConstants.URL_ID_FIELD, notificationId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, null, Notification.class, params);
  }

}
