/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - PublicationTypeClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.service.ResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.publication.PublicationType;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class PublicationTypeClientService extends ResourceClientService<PublicationType> {

  public PublicationTypeClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, ResourceName.PUBLICATION_TYPES, PublicationType.class,
            restClientTool,
            env);
  }

  public PublicationType findByName(String name) {
    final Map<String, String> properties = new HashMap<>();
    properties.put("name", name);
    List<PublicationType> publicationTypes = this.searchByProperties(properties);
    if (publicationTypes.size() == 1) {
      return publicationTypes.get(0);
    } else if (publicationTypes.isEmpty()) {
      throw new SolidifyRuntimeException("No publication type with name '" + name + "' found");
    } else {
      throw new SolidifyRuntimeException("Too many publication type with name '" + name + "' found");
    }
  }

  @Override
  public List<PublicationType> searchByProperties(Map<String, String> properties) {
    return this.searchByProperties(properties, PublicationType.class);
  }

  @Override
  public List<PublicationType> findAll() {
    return this.findAll(PublicationType.class);
  }

}
