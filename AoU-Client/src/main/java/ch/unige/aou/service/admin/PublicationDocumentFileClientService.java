/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - PublicationDocumentFileClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.service.CompositionResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class PublicationDocumentFileClientService extends CompositionResourceClientService<Publication, DocumentFile> {

  private static final String PUBLICATION_DOCUMENT_FILES_PATH =
          ResourceName.PUBLICATIONS + SolidifyConstants.URL_PARENT_ID + ResourceName.DOCUMENT_FILES;

  public PublicationDocumentFileClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, PUBLICATION_DOCUMENT_FILES_PATH, DocumentFile.class,
            restClientTool, env);
  }

  public DocumentFile upload(String parentId, Resource file, MultiValueMap<String, Object> parameters) {
    String url = this.getResourceUrl();
    url = url.replace(SolidifyConstants.URL_PARENT_ID, "/" + parentId + "/");
    url += "/" + ActionName.UPLOAD;

    return this.uploadFile(url, file, DocumentFile.class, parameters);
  }

  public void confirm(String parentId, String id) {
    String url = this.getResourceUrl() + "/" + id + "/" + AouActionName.CONFIRM;

    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, parentId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    restTemplate.postForObject(url, null, Result.class, params);
  }

  public void downloadDocumentFile(String parentId, String id, Path path) {
    String url = this.getResourceUrl();
    url = url.replace(SolidifyConstants.URL_PARENT_ID, "/" + parentId + "/");
    url += "/" + id + "/" + ActionName.DOWNLOAD;

    this.download(url, path);
  }

  @Override
  public List<DocumentFile> findAll(String parentId) {
    return this.findAll(parentId, DocumentFile.class);
  }

}
