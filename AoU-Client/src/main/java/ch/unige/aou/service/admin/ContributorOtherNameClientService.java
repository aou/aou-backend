/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Client - ContributorOtherNameClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.admin;

import java.util.List;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyClientApplication;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.service.CompositionResourceClientService;
import ch.unige.solidify.util.SudoRestClientTool;

import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.ContributorOtherName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.ResourceName;

@Service
public class ContributorOtherNameClientService extends CompositionResourceClientService<Contributor, ContributorOtherName> {

  private static final String CONTRIBUTOR_OTHER_NAMES_PATH =
          ResourceName.CONTRIBUTORS + SolidifyConstants.URL_PARENT_ID + ResourceName.OTHER_NAMES;

  public ContributorOtherNameClientService(SudoRestClientTool restClientTool, Environment env) {
    super(SolidifyClientApplication.getApplicationPrefix(), ModuleName.ADMIN, CONTRIBUTOR_OTHER_NAMES_PATH, ContributorOtherName.class,
            restClientTool,
            env);
  }

  @Override
  public List<ContributorOtherName> findAll(String parentId) {
    return this.findAll(parentId, ContributorOtherName.class);
  }
}
