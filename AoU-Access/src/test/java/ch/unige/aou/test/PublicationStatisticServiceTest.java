/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationStatisticServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.Download;
import ch.unige.aou.model.publication.Hit;
import ch.unige.aou.model.publication.PublicationStatistic;
import ch.unige.aou.repository.DownloadRepository;
import ch.unige.aou.repository.HitRepository;
import ch.unige.aou.repository.PublicationStatisticRepository;
import ch.unige.aou.service.PublicationStatisticService;

@Profile({ "test", "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class PublicationStatisticServiceTest {

  @Mock
  private DownloadRepository downloadRepository;

  @Mock
  private HitRepository hitRepository;

  @Mock
  private PublicationStatisticRepository publicationStatisticRepository;

  private PublicationStatisticService publicationStatisticService;

  private DocumentFile documentFile;

  private final LocalDateTime now = LocalDateTime.now();

  @BeforeEach
  public void setUp() {
    this.publicationStatisticService = new PublicationStatisticService(this.downloadRepository, this.hitRepository, this.publicationStatisticRepository);
    // document two
    this.documentFile = new DocumentFile();
    this.documentFile.setFileName("test_file_1.pdf");
    this.documentFile.setResId("1111-2222-3333");
  }

  @Test
  void createDownloadStatTest_NewRecord() {
    Download download1 = new Download();
    download1.setFilename("test_file_1.pdf");
    download1.setDocumentFileId("1111-2222-3333");
    download1.setMonth(this.now.getMonthValue());
    download1.setYear(this.now.getYear());
    download1.setDownloadNumber(1);

    PublicationDownload archiveDownload = new PublicationDownload();
    archiveDownload.setResId("1111-2222-3333");
    archiveDownload.setName("test_file_1.pdf");

    when(this.downloadRepository.findByDocumentFileAndYearAndMonth(this.documentFile.getResId(), this.now.getYear(), this.now.getMonthValue())).thenReturn(
            Optional.empty());
    when(this.downloadRepository.save(any(Download.class))).thenReturn(download1);

    Download downloadStats = this.publicationStatisticService.createDownloadStats("1111-2222-3333", archiveDownload);
    assertEquals(1, downloadStats.getDownloadNumber());
  }

  @Test
  void createDownloadStatTest_AppendRecord() {
    Download download2 = new Download();
    download2.setFilename("test_file_1.pdf");
    download2.setDocumentFileId("1111-2222-3333");
    download2.setDownloadNumber(8);

    when(this.downloadRepository.findByDocumentFileAndYearAndMonth(this.documentFile.getResId(), this.now.getYear(), this.now.getMonthValue())).thenReturn(
            Optional.of(download2));
    when(this.downloadRepository.save(any(Download.class))).thenAnswer((Answer<Download>) invocationOnMock -> {
      Object[] arguments = invocationOnMock.getArguments();
      if (arguments != null && arguments.length > 0 && arguments[0] != null) {
        Download localDownload = (Download) arguments[0];
        localDownload.setDownloadNumber(9);
        return localDownload;
      }
      return new Download();
    });

    PublicationDownload archiveDownload = new PublicationDownload();
    archiveDownload.setResId("1111-2222-3333");
    archiveDownload.setName("test_file_1.pdf");

    Download downloadStats = this.publicationStatisticService.createDownloadStats("1111-2222-3333", archiveDownload);
    assertEquals(9, downloadStats.getDownloadNumber());
  }

  @Test
  void createViewStatsTest_NewRecord() {
    Hit hit = new Hit();
    hit.setPublicationId("47df-we78-rt88");
    hit.setType("Article");
    hit.setArchiveId("unige:1420");
    hit.setViewNumber(1);

    when(this.hitRepository.findByPublicationId("47df-we78-rt88")).thenReturn(Optional.empty());
    when(this.hitRepository.save(any(Hit.class))).thenReturn(hit);

    PublicationIndexEntry publication = new PublicationIndexEntry();
    publication.setResId("47df-we78-rt88");
    Map<String, Object> metadata = new HashMap<>();
    metadata.put(AouConstants.INDEX_FIELD_ARCHIVE_ID, "unige:1420");
    metadata.put("type", "Article");

    publication.setMetadata(metadata);
    Hit hitStats = this.publicationStatisticService.createViewStats(publication);
    assertEquals(1, hitStats.getViewNumber());
    assertEquals("unige:1420", hitStats.getArchiveId());
    assertEquals("Article", hitStats.getType());
  }

  @Test
  void createViewStatsTest_AppendRecord() {
    Hit hit = new Hit();
    hit.setPublicationId("47df-we78-rt88");
    hit.setType("Article");
    hit.setArchiveId("unige:1422");
    hit.setViewNumber(7);

    when(this.hitRepository.findByPublicationId("47df-zx88-bf38")).thenReturn(Optional.of(hit));
    when(this.hitRepository.save(any(Hit.class))).thenAnswer((Answer<Hit>) invocationOnMock -> {
      Object[] arguments = invocationOnMock.getArguments();
      if (arguments != null && arguments.length > 0 && arguments[0] != null) {
        Hit localHit = (Hit) arguments[0];
        localHit.setViewNumber(8);
        return localHit;
      }
      return new Hit();
    });

    PublicationIndexEntry publication = new PublicationIndexEntry();
    publication.setResId("47df-we78-rt88");
    Map<String, Object> metadata = new HashMap<>();
    metadata.put(AouConstants.INDEX_FIELD_ARCHIVE_ID, "unige:1422");
    metadata.put("type", "Article");

    publication.setMetadata(metadata);

    Hit hitStats = this.publicationStatisticService.createViewStats(publication);
    assertEquals(8, hitStats.getViewNumber());
    assertEquals("unige:1422", hitStats.getArchiveId());
    assertEquals("Article", hitStats.getType());
  }

}
