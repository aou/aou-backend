/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - OaiPmhTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.test.access;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.JAXBException;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.service.OAIService;
import ch.unige.solidify.test.oai.OAIServiceTest;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.config.AouRepositoryDescription;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtype;
import ch.unige.aou.model.publication.PublicationSubtypeDTO;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileType;
import ch.unige.aou.model.publication.PublicationSubtypeDocumentFileTypeDTO;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.service.CommonMetadataService;
import ch.unige.aou.service.query.QueryBuilderService;
import ch.unige.aou.service.SearchService;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileTypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedLicenseRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedStructureRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;

@ActiveProfiles({ "sec-noauth" })
@ExtendWith(SpringExtension.class)
public class OaiPmhTest extends OAIServiceTest {

  protected List<AouMetadataVersion> versions;
  protected AouProperties config;
  protected AouRepositoryDescription repositoryDescription;
  protected SearchService searchService;
  protected CommonMetadataService metadataService;

  @Mock
  protected IndexResourceService<String, PublicationIndexEntry> indexService;
  @Mock
  protected TrustedStructureRemoteResourceService structureRemoteService;
  @Mock
  protected TrustedPublicationRemoteResourceService publicationRemoteService;
  @Mock
  protected TrustedDocumentFileRemoteResourceService documentFileRemoteService;
  @Mock
  protected TrustedPublicationSubtypeRemoteResourceService publicationSubtypeRemoteService;
  @Mock
  protected TrustedDocumentFileTypeRemoteResourceService documentFileTypeRemoteService;
  @Mock
  protected TrustedContributorRemoteResourceService contributorRemoteService;
  @Mock
  protected TrustedLicenseRemoteResourceService licenseRemoteService;
  @Mock
  protected TrustedUserRemoteResourceService userRemoteService;

  @Mock
  protected QueryBuilderService queryBuilderService;

  @Override
  protected void localSetup() {
    this.versions = List.of(AouMetadataVersion.V2_4);
    this.config = new AouProperties(new SolidifyProperties(this.gitInfoProperties));
    this.repositoryDescription = new AouRepositoryDescription(this.gitInfoProperties);
    this.repositoryDescription.setArchiveHomePage(HOST_URL + "/");
    // Git Info Service
    this.gitInfoProperties.getBuild().setVersion("1.x");

    // Search service
    this.searchService = new SearchService(
            this.config,
            this.messageService,
            this.publicationRemoteService,
            this.indexService,
            this.queryBuilderService);
    // Metadata service
    this.metadataService = new CommonMetadataService(
            this.config,
            this.messageService,
            this.structureRemoteService,
            this.publicationRemoteService,
            this.documentFileRemoteService,
            this.publicationSubtypeRemoteService,
            this.documentFileTypeRemoteService,
            this.contributorRemoteService,
            this.licenseRemoteService,
            this.userRemoteService);

    // OAI Service
    this.oaiService = new OAIService(
            this.oaiConfig,
            this.repositoryDescription,
            this.oaiMetadataPrefixService,
            this.oaiSetService,
            this.searchService,
            this.searchService,
            this.metadataService);

    // Mock remote service
    final String documentFileTypeResId1 = "a7644ab4-df93-4adc-b066-96127d8aad8b";
    final String documentFileTypeValue1 = "Article (Accepted version)";
    final String documentFileTypeResId2 = "c06982ae-27e9-4de8-821a-2899531ce6b3";
    final String documentFileTypeValue2 = "Appendix";
    final String publicationSubtypeResId = "A1";
    final String publicationSubtypeName = "Article scientifique";

    // Publication sub-types
    final List<PublicationSubtypeDTO> publicationSubtypes = new ArrayList<>();
    PublicationSubtype publicationSubtype = new PublicationSubtype();
    publicationSubtype.setResId(publicationSubtypeResId);
    publicationSubtype.setName(publicationSubtypeName);
    publicationSubtypes.add(new PublicationSubtypeDTO(publicationSubtype));
    RestCollection<PublicationSubtypeDTO> publicationSubtypesRestCollection = new RestCollection<>(publicationSubtypes);
    when(this.publicationSubtypeRemoteService.findAllDTO(any())).thenReturn(publicationSubtypesRestCollection);

    // Document file type
    final List<DocumentFileType> documentFileTypes = new ArrayList<>();
    DocumentFileType documentFileType1 = new DocumentFileType();
    documentFileType1.setResId(documentFileTypeResId1);
    documentFileType1.setValue(documentFileTypeValue1);
    documentFileTypes.add(documentFileType1);
    DocumentFileType documentFileType2 = new DocumentFileType();
    documentFileType2.setResId(documentFileTypeResId2);
    documentFileType2.setValue(documentFileTypeValue2);
    documentFileTypes.add(documentFileType2);
    RestCollection<DocumentFileType> documentFileTypesRestCollection = new RestCollection<>(documentFileTypes);
    when(this.documentFileTypeRemoteService.findAllWithCache(any())).thenReturn(documentFileTypesRestCollection);

    // Publication sub-type
    final List<PublicationSubtypeDocumentFileTypeDTO> publicationSubtypeDocumentFileTypes = new ArrayList<>();
    PublicationSubtypeDocumentFileType publicationSubtypeDocumentFileType1 = new PublicationSubtypeDocumentFileType();
    publicationSubtypeDocumentFileType1.setLevel(DocumentFileType.FileTypeLevel.PRINCIPAL);
    publicationSubtypeDocumentFileType1.setDocumentFileType(documentFileType1);
    publicationSubtypeDocumentFileType1.setPublicationSubtype(publicationSubtype);
    publicationSubtypeDocumentFileTypes.add(new PublicationSubtypeDocumentFileTypeDTO(publicationSubtypeDocumentFileType1));
    PublicationSubtypeDocumentFileType publicationSubtypeDocumentFileType2 = new PublicationSubtypeDocumentFileType();
    publicationSubtypeDocumentFileType2.setLevel(DocumentFileType.FileTypeLevel.SECONDARY);
    publicationSubtypeDocumentFileType2.setDocumentFileType(documentFileType2);
    publicationSubtypeDocumentFileType2.setPublicationSubtype(publicationSubtype);
    publicationSubtypeDocumentFileTypes.add(new PublicationSubtypeDocumentFileTypeDTO(publicationSubtypeDocumentFileType2));
    RestCollection<PublicationSubtypeDocumentFileTypeDTO> psdftCollection = new RestCollection<>(publicationSubtypeDocumentFileTypes);
    when(this.publicationSubtypeRemoteService.findAllPublicationSubtypeDocumentFileTypesDTO(publicationSubtypeResId,
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by("sortValue")))).thenReturn(psdftCollection);

    when(this.documentFileTypeRemoteService.findAllWithCache(any())).thenReturn(documentFileTypesRestCollection);
    when(this.contributorRemoteService.getByCnIndividu(any())).thenReturn(this.getContributor());
    when(this.documentFileRemoteService.listByPublication(any(), any())).thenReturn(this.getDocumentFiles());
    when(this.documentFileRemoteService.fulltext(any())).thenReturn("text-to-index");

    AouSearchCondition notFoundSearchCondition = this.getFindByArchiveIdConditions("xxx");
    when(this.queryBuilderService.getFindByArchiveIdCondition("xxx")).thenReturn(notFoundSearchCondition);
    AouSearchCondition notFoundSearchCondition2 = this.getFindByArchiveIdConditions(AouMetadataVersion.V2_4.getVersion());
    when(this.queryBuilderService.getFindByArchiveIdCondition(AouMetadataVersion.V2_4.getVersion())).thenReturn(notFoundSearchCondition2);

    FieldsRequest defaultFieldsRequest = new FieldsRequest();
    defaultFieldsRequest.getExcludes().addAll(this.config.getIndexing().getDefaultExcludedFields());
    when(this.queryBuilderService.getDefaultFieldsRequest()).thenReturn(defaultFieldsRequest);

    // Mock Rest Client Service
    try {
      FacetPage<PublicationIndexEntry> publicationIndexList = this.getPublicationFacetList();
      when(this.indexService.search(any(), any(), any(), any(Pageable.class), any())).thenReturn(publicationIndexList);
      when(this.indexService.search(any(), any(), any(), any(Pageable.class))).thenReturn(publicationIndexList);
      FacetPage<PublicationIndexEntry> emptyPublicationIndexList = this.getEmptyPublicationFacetList();
      when(this.indexService.search("datamgmt-publications", List.of(notFoundSearchCondition), null, Pageable.ofSize(10), defaultFieldsRequest))
              .thenReturn(emptyPublicationIndexList);
    } catch (IOException | JAXBException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  private List<DocumentFile> getDocumentFiles() {
    DocumentFile docfile = new DocumentFile();
    return List.of(docfile);
  }

  private Contributor getContributor() {
    Contributor contributor = new Contributor();
    contributor.setCnIndividu("xxx");
    return contributor;
  }

  private AouSearchCondition getFindByArchiveIdConditions(String value) {
    AouSearchCondition aouSearchCondition = new AouSearchCondition();
    aouSearchCondition.setType(SearchConditionType.TERM);
    aouSearchCondition.setField(AouConstants.INDEX_FIELD_ARCHIVE_ID);
    aouSearchCondition.setValue(value);
    return aouSearchCondition;
  }

  @Override
  protected String[] getMetadataList() {
    return new String[] { OAIConstants.OAI_DC, AouConstants.OAI_AOU };
  }

  private PublicationIndexEntry getPublicationMetadata(AouMetadataVersion version) throws IOException, JAXBException {
    Path xml = this.getPath("aou-deposit-" + version.getVersion() + SolidifyConstants.XML_EXT);
    Publication publication = new Publication();
    publication.setMetadata(FileTool.toString(xml));

    OffsetDateTime now = OffsetDateTime.now();
    publication.setStatus(Publication.PublicationStatus.IN_PROGRESS);
    publication.setLastStatusUpdate(now);
    publication.getCreation().setWhen(now);
    publication.getCreation().setWho("Albert Dupontel");
    publication.getLastUpdate().setWhen(now);
    publication.getLastUpdate().setWho("Albert Dupontel");
    Person creator = new Person();
    creator.setFirstName("Joe");
    creator.setLastName("Dohn");
    publication.setCreator(creator);
    publication.setArchiveId("unige-test:1");

    return this.metadataService.getPublicationIndexEntry(publication, publication.getMetadata());
  }

  private RestCollection<PublicationIndexEntry> getPublicationMetadataList() throws IOException, JAXBException {
    List<PublicationIndexEntry> list = new ArrayList<>();
    for (AouMetadataVersion version : this.versions) {
      PublicationIndexEntry archiveMetadata = this.getPublicationMetadata(version);
      list.add(archiveMetadata);
    }
    return new RestCollection<>(list);
  }

  private FacetPage<PublicationIndexEntry> getPublicationFacetList() throws IOException, JAXBException {
    return new FacetPage<>(this.getPublicationMetadataList().getData());
  }

  private FacetPage<PublicationIndexEntry> getEmptyPublicationFacetList() throws IOException, JAXBException {
    List<PublicationIndexEntry> list = new ArrayList<>();
    return new FacetPage<>(list);
  }

  @Override
  protected List<String> getIdentifierList() {
    List<String> list = new ArrayList<>();
    for (AouMetadataVersion version : this.versions) {
      list.add(version.getVersion());
    }
    return list;
  }

  @Override
  protected String getMetadataXmlTransformation() throws IOException {
    ClassPathResource xslResource = new ClassPathResource(SolidifyConstants.XSL_HOME + "/aou2oai_dc.xsl");
    return FileTool.toString(xslResource.getInputStream());
  }

  @Override
  protected String getReferenceOAIMetadataPrefixName() {
    return AouConstants.OAI_AOU;
  }

  @Override
  protected OAIMetadataPrefix getReferenceOAIMetadataPrefix() throws MalformedURLException {
    OAIMetadataPrefix oaiMetadataPrefix = new OAIMetadataPrefix();
    oaiMetadataPrefix.setPrefix(AouConstants.OAI_AOU);
    oaiMetadataPrefix.setName(AouConstants.OAI_AOU_NAME);
    oaiMetadataPrefix.setReference(true);
    oaiMetadataPrefix.setSchemaUrl(new URL(AouConstants.OAI_AOU_SCHEMA_2_4));
    oaiMetadataPrefix.setSchemaNamespace(AouConstants.OAI_AOU_NAMESPACE_2_4);
    oaiMetadataPrefix.setEnabled(true);
    return oaiMetadataPrefix;
  }

}
