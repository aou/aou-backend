/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationIndexEntryDataProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.citation;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import de.undercouch.citeproc.csl.CSLItemData;
import de.undercouch.citeproc.csl.CSLItemDataBuilder;
import de.undercouch.citeproc.csl.CSLName;
import de.undercouch.citeproc.csl.CSLNameBuilder;
import de.undercouch.citeproc.csl.CSLType;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.Citation;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.SolidifyItemDataProvider;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.publication.AbstractContributor;
import ch.unige.aou.model.publication.CollaborationDTO;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.MetadataCollection;
import ch.unige.aou.model.publication.MetadataDates;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.model.publication.PublicationSubtypeDTO;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.service.CommonMetadataService;
import ch.unige.aou.service.metadata.DepositDocAdapter;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;

public class PublicationIndexEntryDataProvider implements SolidifyItemDataProvider {

  private final TrustedPublicationRemoteResourceService publicationRemoteResourceService;
  private final TrustedPublicationSubtypeRemoteResourceService trustedPublicationSubtypeRemoteResourceService;

  private final CommonMetadataService commonMetadataService;

  private final String[] metadataDateFormat;

  public PublicationIndexEntryDataProvider(AouProperties aouProperties, TrustedPublicationRemoteResourceService publicationRemoteResourceService,
          TrustedPublicationSubtypeRemoteResourceService trustedPublicationSubtypeRemoteResourceService,
          CommonMetadataService commonMetadataService) {
    this.publicationRemoteResourceService = publicationRemoteResourceService;
    this.trustedPublicationSubtypeRemoteResourceService = trustedPublicationSubtypeRemoteResourceService;
    this.commonMetadataService = commonMetadataService;

    this.metadataDateFormat = aouProperties.getParameters().getMetadataDateFormat();
  }

  /**************************************************************************************/

  @Override
  public CSLItemData retrieveItem(String publicationId, String style, String language, Citation.OutputFormat outputFormat) {

    final Publication publication = this.publicationRemoteResourceService.findOne(publicationId);
    Object depositDoc = this.commonMetadataService.getDepositDoc(publication.getMetadata());
    DepositDocAdapter depositDocAdapter = this.commonMetadataService
            .getDepositDocAdapter(this.commonMetadataService.detectVersionFromXmlMetadata(publication.getMetadata()));

    CSLItemDataBuilder builder = new CSLItemDataBuilder().id(publicationId);

    this.setType(builder, depositDocAdapter, depositDoc);
    this.setGenre(builder, depositDocAdapter, depositDoc, language);
    this.setTitle(builder, depositDocAdapter, depositDoc);
    this.setOriginalTitle(builder, depositDocAdapter, depositDoc);
    this.setAuthor(builder, depositDocAdapter, depositDoc);
    this.setTranslator(builder, depositDocAdapter, depositDoc);
    this.setEditor(builder, depositDocAdapter, depositDoc);
    this.setIssued(builder, depositDocAdapter, depositDoc);
    this.setPagination(builder, depositDocAdapter, depositDoc);
    this.setEdition(builder, depositDocAdapter, depositDoc);
    this.setVolume(builder, depositDocAdapter, depositDoc);
    this.setIssue(builder, depositDocAdapter, depositDoc);
    this.setContainerTitle(builder, depositDocAdapter, depositDoc);
    this.setPublisher(builder, depositDocAdapter, depositDoc);
    this.setPublisherPlace(builder, depositDocAdapter, depositDoc);
    this.setEvent(builder, depositDocAdapter, depositDoc);
    this.setEventPlace(builder, depositDocAdapter, depositDoc);
    this.setCollectionTitle(builder, depositDocAdapter, depositDoc);
    this.setDOI(builder, depositDocAdapter, depositDoc);
    this.setISBN(builder, depositDocAdapter, depositDoc);
    this.setAbstract(builder, depositDocAdapter, depositDoc);
    this.setNote(builder, depositDocAdapter, depositDoc);

    /*
     * NOT SURE : - collation - containerAuthor - institution - unige
     */

    return builder.build();
  }

  /**************************************************************************************/

  private void setType(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    builder.type(this.getCSLType(depositDocAdapter, depositDoc));
  }

  private void setGenre(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc, String language) {
    String subtype = depositDocAdapter.getSubtype(depositDoc);
    PublicationSubtypeDTO publicationSubtypeDTO = this.getPublicationSubtypeDTO(subtype);
    String langCode = CleanTool.getLanguageCode(language);
    Optional<String> labelOpt = publicationSubtypeDTO.getLabels().stream().filter(labelDTO -> labelDTO.getLanguageCode().equals(langCode))
            .map(labelDTO -> labelDTO.getText()).findFirst();
    if (labelOpt.isPresent()) {
      subtype = labelOpt.get();
    }
    builder.genre(subtype);
  }

  private PublicationSubtypeDTO getPublicationSubtypeDTO(String name) {
    List<PublicationSubtypeDTO> sortedPublicationSubtypes = this.trustedPublicationSubtypeRemoteResourceService.findAllDTO(
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(AouConstants.BIBLIOGRAPHY_SORT_VALUE))).getData();
    Optional<PublicationSubtypeDTO> publicationSubtypeDtoOpt = sortedPublicationSubtypes.stream()
            .filter(subtypeDto -> subtypeDto.getName().equals(name)).findFirst();
    if (publicationSubtypeDtoOpt.isPresent()) {
      return publicationSubtypeDtoOpt.get();
    } else {
      throw new SolidifyRuntimeException("Unable to get PublicationSubtype for name '" + name + "'");
    }
  }

  private void setTitle(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    builder.title(depositDocAdapter.getTitleContent(depositDoc));
  }

  private void setOriginalTitle(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getOriginalTitleContent(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.originalTitle(value);
    }
  }

  private void setAuthor(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    // Set authors with contributors having the role 'author' 
    CSLName[] cslAuthorNames = this.getContributorsWithRole(depositDocAdapter, depositDoc, AouConstants.CONTRIBUTOR_ROLE_AUTHOR);
    if (cslAuthorNames.length > 0) {
      builder.author(cslAuthorNames);
    }

    // If no collaborator with the role 'author' have been found, check if there are collaborations
    if (cslAuthorNames.length == 0) {
      CSLName[] cslCollaborationNames = this.getCollaborationsCSLNames(builder, depositDocAdapter, depositDoc);
      if (cslCollaborationNames.length > 0) {
        builder.author(cslCollaborationNames);
      }
    }
  }

  private CSLName[] getCollaborationsCSLNames(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    List<CollaborationDTO> collaborations = new ArrayList<>();
    for (AbstractContributor abstractContributor : depositDocAdapter.getContributors(depositDoc)) {
      if (abstractContributor instanceof CollaborationDTO) {
        collaborations.add((CollaborationDTO) abstractContributor);
      }
    }

    List<CSLName> cslNames = new ArrayList<>();
    for (CollaborationDTO collaborationDTO : collaborations) {
      CSLName cslName = new CSLNameBuilder().literal(collaborationDTO.getName()).build();
      cslNames.add(cslName);
    }

    return cslNames.toArray(new CSLName[0]);
  }

  private void setTranslator(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    CSLName[] cslNames = this.getContributorsWithRole(depositDocAdapter, depositDoc, AouConstants.CONTRIBUTOR_ROLE_TRANSLATOR);
    if (cslNames.length > 0) {
      builder.translator(cslNames);
    }
  }

  private void setEditor(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    CSLName[] editors = this.getContributorsWithRole(depositDocAdapter, depositDoc, AouConstants.CONTRIBUTOR_ROLE_EDITOR);
    if (editors.length > 0) {
      builder.editor(editors);
    } else {
      String containerEditor = depositDocAdapter.getContainerEditor(depositDoc);
      if (!StringTool.isNullOrEmpty(containerEditor)) {
        CSLName cslName = new CSLNameBuilder().literal(containerEditor).build();
        builder.editor(cslName);
      }
    }
  }

  private void setIssued(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    Integer year = this.getYear(depositDocAdapter, depositDoc,
            List.of(AouConstants.DATE_TYPE_PUBLICATION, AouConstants.DATE_TYPE_IMPRIMATUR, AouConstants.DATE_TYPE_FIRST_ONLINE,
                    AouConstants.DATE_TYPE_DEFENSE));
    if (year != null) {
      builder.issued(year);
    }
  }

  private void setPagination(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String paging = depositDocAdapter.getPaging(depositDoc);
    if (!StringTool.isNullOrEmpty(paging)) {
      builder.page(paging);
    } else {
      String pagingOther = depositDocAdapter.getPagingOther(depositDoc);
      if (!StringTool.isNullOrEmpty(pagingOther)) {
        builder.page(pagingOther);
      }
    }
  }

  private void setEdition(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getEdition(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.edition(value);
    }
  }

  private void setVolume(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getContainerVolume(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.volume(value);
    }
  }

  private void setIssue(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getContainerIssue(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.issue(value);
    }
  }

  private void setContainerTitle(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getContainerTitle(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.containerTitle(value);
    }
  }

  private void setPublisher(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getPublisherName(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.publisher(value);
    }
  }

  private void setPublisherPlace(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getPublisherPlace(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.publisherPlace(value);
    }
  }

  private void setEvent(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getContainerTitle(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.event(value);
    }
  }

  private void setEventPlace(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getContainerConferencePlace(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.eventPlace(value);
    }
  }

  private void setCollectionTitle(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    if (!depositDocAdapter.getCollections(depositDoc).isEmpty()) {
      MetadataCollection collection = depositDocAdapter.getCollections(depositDoc).get(0);
      builder.collectionTitle(collection.getName());
    }
  }

  private void setDOI(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String doi = depositDocAdapter.getDOI(depositDoc);
    if (!StringTool.isNullOrEmpty(doi)) {
      doi = doi.replaceAll("^http(s)*://doi\\.org/", "");
      builder.DOI(doi);
    }
  }

  private void setISBN(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String value = depositDocAdapter.getISBN(depositDoc);
    if (!StringTool.isNullOrEmpty(value)) {
      builder.ISBN(value);
    }
  }

  private void setAbstract(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    if (!depositDocAdapter.getAbstracts(depositDoc).isEmpty()) {
      builder.abstrct(depositDocAdapter.getAbstracts(depositDoc).get(0).getContent());
    }
  }

  private void setNote(CSLItemDataBuilder builder, DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String note = depositDocAdapter.getNote(depositDoc);
    if (!StringTool.isNullOrEmpty(note)) {
      builder.note(note);
    }
  }

  private CSLName[] getContributorsWithRole(DepositDocAdapter depositDocAdapter, Object depositDoc, String role) {
    List<CSLName> cslNames = new ArrayList<>();
    for (ContributorDTO contributorDTO : this.getContributors(depositDocAdapter, depositDoc)) {
      if (contributorDTO.getRole().equals(role)) {
        CSLName cslName = new CSLNameBuilder().given(contributorDTO.getFirstName()).family(contributorDTO.getLastName()).build();
        cslNames.add(cslName);
      }
    }
    return cslNames.toArray(new CSLName[0]);
  }

  private List<ContributorDTO> getContributors(DepositDocAdapter depositDocAdapter, Object depositDoc) {
    List<ContributorDTO> contributors = new ArrayList<>();
    for (AbstractContributor abstractContributor : depositDocAdapter.getContributors(depositDoc)) {
      if (abstractContributor instanceof ContributorDTO) {
        contributors.add((ContributorDTO) abstractContributor);
      }
    }
    return contributors;
  }

  private Integer getYear(DepositDocAdapter depositDocAdapter, Object depositDoc, List<String> dateTypes) {
    List<MetadataDates> dates = depositDocAdapter.getDates(depositDoc);
    for (String dateType : dateTypes) {
      for (MetadataDates date : dates) {
        if (date.getType().equals(dateType)) {
          return this.getYear(date.getContent());
        }
      }
    }
    return null;
  }

  private Integer getYear(String dateStr) {
    for (String dateFormat : this.metadataDateFormat) {
      try {
        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendPattern(dateFormat)
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                .toFormatter();
        LocalDate date = LocalDate.parse(dateStr, formatter);
        return date.getYear();
      } catch (DateTimeParseException e) {
        // do nothing, will try with next format
      }
    }
    return null;
  }

  private CSLType getCSLType(DepositDocAdapter depositDocAdapter, Object depositDoc) {
    String subtypeCode = depositDocAdapter.getSubtype(depositDoc);
    if (!StringTool.isNullOrEmpty(subtypeCode)) {
      switch (subtypeCode) {

        case AouConstants.DEPOSIT_SUBTYPE_ARTICLE_PROFESSIONNEL_NAME:
          return CSLType.ARTICLE_MAGAZINE;

        case AouConstants.DEPOSIT_SUBTYPE_AUTRE_ARTICLE_NAME:
          return CSLType.ARTICLE_NEWSPAPER;

        case AouConstants.DEPOSIT_SUBTYPE_PREPRINT_NAME:
          return CSLType.ARTICLE;

        case AouConstants.DEPOSIT_SUBTYPE_LIVRE_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_OUVRAGE_COLLECTIF_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_ACTES_DE_CONFERENCE_NAME:
          return CSLType.BOOK;

        case AouConstants.DEPOSIT_SUBTYPE_PRESENTATION_INTERVENTION_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_POSTER_NAME:
          return CSLType.PAPER_CONFERENCE;

        case AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_DE_LIVRE_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_CONTRIBUTION_DICTIONNAIRE_ENCYCLOPEDIE_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_CHAPITRE_D_ACTES_NAME:
          return CSLType.CHAPTER;

        case AouConstants.DEPOSIT_SUBTYPE_THESE_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME:
          return CSLType.THESIS;

        case AouConstants.DEPOSIT_SUBTYPE_RAPPORT_DE_RECHERCHE_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_RAPPORT_TECHNIQUE_NAME:
        case AouConstants.DEPOSIT_SUBTYPE_WORKING_PAPER_NAME:
          return CSLType.REPORT;

        case AouConstants.DEPOSIT_SUBTYPE_NUMERO_DE_REVUE_NAME:

        case AouConstants.DEPOSIT_SUBTYPE_ARTICLE_SCIENTIFIQUE_NAME:
        default:
          return CSLType.ARTICLE_JOURNAL;
      }
    }
    return null;
  }
}
