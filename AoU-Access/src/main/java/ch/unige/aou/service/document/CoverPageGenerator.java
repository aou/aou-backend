/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - CoverPageGenerator.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.document;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.itextpdf.kernel.exceptions.BadPasswordException;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.utils.PdfMerger;
import com.itextpdf.layout.font.FontProvider;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.bibliography.BibliographyParameters;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.ContributorDTO;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.service.BibliographyGeneratorService;

/**
 * CoverPageGenerator will generate Cover PAge as PDF and merge it with document for downloading
 */
@Service
@ConditionalOnBean(AccessController.class)
public class CoverPageGenerator {

  private static final Logger log = LoggerFactory.getLogger(CoverPageGenerator.class);

  public static final String FILE_TYPE_SECTION = "fileTypeSection";

  final ObjectMapper objectMapper = new ObjectMapper();

  private final SpringTemplateEngine templateEngine;
  private final BibliographyGeneratorService bibliographyService;
  private final String portalHomePage;

  public CoverPageGenerator(SpringTemplateEngine templateEngine, BibliographyGeneratorService bibliographyService, AouProperties aouProperties) {
    this.templateEngine = templateEngine;
    this.bibliographyService = bibliographyService;
    this.templateEngine.addTemplateResolver(this.getTemplateResolver());
    this.portalHomePage = aouProperties.getParameters().getHomepage();

    this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  public synchronized URL generateCoverPage(String documentFileId, Path originalFilePath, PublicationIndexEntry publication)
          throws MalformedURLException {
    final Context thymeleafContext = new Context();
    final Map<String, Object> parameters = new HashMap<>();

    MetadataFile currentFile = null;
    Optional<MetadataFile> currentFileOpt = publication.getFiles().stream().filter(f -> f.getResId().equals(documentFileId)).findFirst();
    if (currentFileOpt.isPresent()) {
      currentFile = currentFileOpt.get();
    }

    // If file is not a pdf, is an agreement or imprimatur or mode publication we don't generate a cover page
    if (currentFile == null || currentFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_AGREEMENT_VALUE) ||
            currentFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE) ||
            currentFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_PUBLICATION_MODE_VALUE) ||
            !currentFile.getMimeType().equals("application/pdf")) {
      return originalFilePath.toUri().toURL();
    }

    // Get directory where the main file is stored, we decode using UTF8 to avoid encoding characters in the filename
    Path parentFolderPath = originalFilePath.getParent();
    String filename = originalFilePath.getFileName().toString();

    //Check if there is already an existing document with cover page
    String filenameWithCoverPage = AouConstants.FILE_WITH_COVER_PAGE_PREFIX + filename;
    Path fileWithCoverPagePath = parentFolderPath.resolve(filenameWithCoverPage);
    if (fileWithCoverPagePath.toFile().exists()) {
      return fileWithCoverPagePath.toUri().toURL();
    }

    // Check if file is PRINCIPAL in order to load the correspondant html template
    String htmlCoverTemplate = currentFile.getTypeLevel().contains(DocumentFileType.FileTypeLevel.PRINCIPAL.name()) ?
            AouConstants.COVER_PAGE_TEMPLATE :
            AouConstants.COVER_PAGE_SIMPLE_TEMPLATE;

    // populate parameters for html template
    this.populateParametersForDocument(publication, currentFile, parameters);
    thymeleafContext.setVariables(parameters);

    //process html template
    String htmlBody = this.templateEngine.process(htmlCoverTemplate + SolidifyConstants.HTML_EXT, thymeleafContext);

    Path coverPagePath = parentFolderPath.resolve(AouConstants.COVER_PAGE_FILENAME);

    try {
      ConverterProperties properties = new ConverterProperties();
      FontProvider fontProvider = new FontProvider();
      fontProvider.addStandardPdfFonts();
      fontProvider.addSystemFonts(); //for fallback
      properties.setFontProvider(fontProvider);
      // convert html to pdf file
      HtmlConverter.convertToPdf(htmlBody, Files.newOutputStream(coverPagePath), properties);
      // Merge the generated pdf cover page with original pdf file
      this.manipulatePdf(coverPagePath, originalFilePath, fileWithCoverPagePath);
      // return the stream
    } catch (FileNotFoundException e) {
      log.error("Error when generating cover page file, document {} is not found. Exception: ({})", currentFile.getResId(), e.getMessage());
      this.cleanCoverPageWhenProcessFail(List.of(coverPagePath, fileWithCoverPagePath));
      return originalFilePath.toUri().toURL();
    } catch (BadPasswordException ex) {
      log.error("Error when generating cover page file, document {} is password protected. Exception: ({})", currentFile.getResId(),
              ex.getMessage());
      this.cleanCoverPageWhenProcessFail(List.of(coverPagePath, fileWithCoverPagePath));
      return originalFilePath.toUri().toURL();
    } catch (Exception e) {
      log.error("Could not create cover page for publication: {}.", publication.getResId(), e);
      this.cleanCoverPageWhenProcessFail(List.of(coverPagePath, fileWithCoverPagePath));
      return originalFilePath.toUri().toURL();
    }
    return fileWithCoverPagePath.toUri().toURL();
  }

  private void populateParametersForDocument(PublicationIndexEntry publication, MetadataFile documentFile, Map<String, Object> parameters) {
    parameters.put("fileSubType", publication.getSubtype());
    parameters.put("fileSubSubType", publication.getMetadata().get("subsubtype"));
    parameters.put("fileYear", publication.getYear());

    String diffusion;
    if (this.isUnderEmbargo(documentFile)) {
      diffusion = "Embargoed Access";
    } else if (documentFile.getAccessLevel().equals(DocumentFile.AccessLevel.PRIVATE.name())) {
      diffusion = "Closed Access";
    } else if (documentFile.getAccessLevel().equals(DocumentFile.AccessLevel.RESTRICTED.name())) {
      diffusion = "Restricted Access";
    } else if (documentFile.getAccessLevel().equals(DocumentFile.AccessLevel.PUBLIC.name())) {
      diffusion = "Open Access";
    } else {
      throw new IllegalStateException("Unexpected value for file access level: " + documentFile.getAccessLevel());
    }
    parameters.put("diffusion", diffusion);

    parameters.put("title", publication.getTitle());

    // check if file is secondary and use the type as fileVersion
    if (documentFile.getTypeLevel().contains(DocumentFileType.FileTypeLevel.SECONDARY.name()) ||
            documentFile.getTypeLevel().contains(DocumentFileType.FileTypeLevel.CORRECTIVE.name())) {
      parameters.put("fileVersion", documentFile.getType());
    } else if (documentFile.getType().contains("(")) { //Check if the value contains () , us the version is formatted inside
      String version = documentFile.getType().substring(documentFile.getType().indexOf("(") + 1, documentFile.getType().indexOf(")"));
      parameters.put("fileVersion", version);
    }

    this.fillDocumentTypeSection(documentFile, parameters);
    this.fillContributorsAndCollaborator((List<LinkedHashMap<String, Object>>) publication.getMetadata().get("contributors"), parameters);

    // fill citations
    BibliographyParameters bibliographyParameters = new BibliographyParameters();
    bibliographyParameters.setLang(BibliographyParameters.Lang.ENGLISH);
    String citationText = this.bibliographyService.getCitationText(publication, "bibliography/styles/unige-long", bibliographyParameters);
    citationText = CleanTool.restoreAllowedHtmlTags(citationText, List.of(CleanTool.TAGS_ALLOWED_IN_TITLE));
    parameters.put("citation", citationText);

    // Link and DOI
    parameters.put("link", this.portalHomePage + "/" + publication.getMetadata(AouConstants.INDEX_FIELD_ARCHIVE_ID));
    parameters.put("doi", publication.getMetadata("doi"));

    // corrections
    if (publication.getMetadata("corrections") != null) {
      for (MetadataFile metadataFile : publication.getFiles()) {
        if (metadataFile.getTypeLevel().contains(DocumentFileType.FileTypeLevel.CORRECTIVE.name())) {
          //corrigendum
          parameters.put("corrigendum", metadataFile.getType());
        }
      }
    }

    //Licence
    parameters.put("licenseTitle", documentFile.getLicenseTitle());
    parameters.put("licenseUrl", documentFile.getLicenseUrl());
  }

  private boolean isUnderEmbargo(MetadataFile metadataFile) {
    return metadataFile.getEmbargoAccessLevel() != null
            && metadataFile.getEmbargoEndDate() != null
            && metadataFile.getEmbargoEndDate().isAfter(LocalDate.now());
  }

  private void fillDocumentTypeSection(MetadataFile document, Map<String, Object> parameters) {
    if (document.getType().contains(AouConstants.DOCUMENT_FILE_TYPE_POSTER)
            || document.getType().contains(AouConstants.DOCUMENT_FILE_TYPE_PREPRINT)
            || document.getType().contains(AouConstants.DOCUMENT_FILE_TYPE_REPORT)
            || document.getType().contains(AouConstants.DOCUMENT_FILE_TYPE_WORKING_PAPER)
            || document.getType().contains(AouConstants.DOCUMENT_FILE_TYPE_THESIS)
            || document.getType().contains(AouConstants.DOCUMENT_FILE_TYPE_MASTER_THESIS)) {
      parameters.put(FILE_TYPE_SECTION, "This publication is distributed with a layout created by the author(s).");
    } else if (document.getType().contains(AouConstants.DOCUMENT_SUBMITTED_VERSION_EN)) {
      parameters.put(FILE_TYPE_SECTION, "This is an author manuscript pre-peer-reviewing (submitted version) of the original publication. "
              + "The layout of the published version may differ.");
    } else if (document.getType().contains(AouConstants.DOCUMENT_ACCEPTED_VERSION_EN)) {
      parameters.put(FILE_TYPE_SECTION, "This is an author manuscript post-peer-reviewing (accepted version) of the original publication. "
              + "The layout of the published version may differ.");
    } else if (document.getType().contains(AouConstants.DOCUMENT_PUBLISHED_VERSION_EN)) {
      parameters.put(FILE_TYPE_SECTION, "This document is made available in accordance with the publisher’s policy.");
    }
  }

  private void fillContributorsAndCollaborator(List<LinkedHashMap<String, Object>> contributors, Map<String, Object> parameters) {
    List<ContributorDTO> contributorDTOS = new ArrayList<>();
    List<ContributorDTO> collaboratorDTOS = new ArrayList<>();

    for (LinkedHashMap<String, Object> contributor : contributors) {
      ContributorDTO crt = this.objectMapper.convertValue(contributor, ContributorDTO.class);
      if (crt.getType().equals("contributor")) {
        if (crt.getRole().equals("collaborator")) {
          collaboratorDTOS.add(crt);
        } else {
          contributorDTOS.add(crt);
        }
      }
    }
    parameters.put("contributors", this.formatContributorList(contributorDTOS));
    parameters.put("collaborators", this.formatCollaboratorList(collaboratorDTOS));
  }

  private StringBuilder formatContributorList(List<ContributorDTO> contributorDTOS) {
    StringBuilder contributorStr = new StringBuilder();
    for (int idx = 0; idx < contributorDTOS.size(); idx++) {
      if (idx == 14) {
        contributorStr.deleteCharAt(contributorStr.lastIndexOf(";")); //remove last ; char
        contributorStr.append(" [<b>and ").append(contributorDTOS.size() - 14).append(" more</b>]");
        break;
      } else {

        String role = contributorDTOS.get(idx).getRole();
        String roleLabel = "";
        if (!role.equals(AouConstants.CONTRIBUTOR_ROLE_AUTHOR) && !role.equals(AouConstants.CONTRIBUTOR_ROLE_DIRECTOR)) {
          roleLabel = "&nbsp;(" + this.getContributorRoleLabel(role) + ")";
        }

        if (!role.equals(AouConstants.CONTRIBUTOR_ROLE_DIRECTOR)) { // ignore director
          contributorStr.append(contributorDTOS.get(idx).getFullName().replace(" ", "&nbsp;")).append(roleLabel).append("; ");
        }
      }
    }
    //remove last ; char if present
    if (contributorStr.lastIndexOf(";") != -1) {
      contributorStr.deleteCharAt(contributorStr.lastIndexOf(";"));
    }
    return contributorStr;
  }

  private String getContributorRoleLabel(String role) {
    if (role == null) {
      role = "";
    }
    switch (role) {
      case AouConstants.CONTRIBUTOR_ROLE_EDITOR:
        return "ed.";
      case AouConstants.CONTRIBUTOR_ROLE_TRANSLATOR:
        return "transl.";
      case AouConstants.CONTRIBUTOR_ROLE_GUEST_EDITOR:
        return "guest ed.";
      case AouConstants.CONTRIBUTOR_ROLE_DIRECTOR:
        return "dir.";
      case AouConstants.CONTRIBUTOR_ROLE_PHOTOGRAPHER:
        return "photogr.";
      case AouConstants.CONTRIBUTOR_ROLE_COLLABORATOR:
        return "collab.";
      default:
        return "";
    }
  }

  private StringBuilder formatCollaboratorList(List<ContributorDTO> contributorDTOS) {
    StringBuilder collaboratorStr = new StringBuilder();
    for (int idx = 0; idx < contributorDTOS.size(); idx++) {
      if (idx == 13) {
        collaboratorStr.deleteCharAt(collaboratorStr.lastIndexOf(";")); //remove last ; char
        collaboratorStr.append(" [<b>and ").append(contributorDTOS.size() - 13).append(" more</b>]");
        break;
      } else {
        collaboratorStr.append(contributorDTOS.get(idx).getFullName()).append("; ");
      }
    }
    //remove last ; char if present
    if (collaboratorStr.lastIndexOf(";") != -1) {
      collaboratorStr.deleteCharAt(collaboratorStr.lastIndexOf(";"));
    }
    return collaboratorStr;
  }

  public void manipulatePdf(Path coverPagePath, Path originalFilePath, Path fileWithCoverPagePath) throws BadPasswordException, IOException {
    try (
            PdfDocument pdfDoc = new PdfDocument(new PdfWriter(fileWithCoverPagePath.toFile()));
            PdfDocument cover = new PdfDocument(new PdfReader(coverPagePath.toFile()));
            PdfDocument resource = new PdfDocument(new PdfReader(originalFilePath.toFile()))
    ) {
      PdfMerger merger = new PdfMerger(pdfDoc);
      merger.merge(cover, 1, 1);
      merger.merge(resource, 1, resource.getNumberOfPages());
    }
  }

  private void cleanCoverPageWhenProcessFail(List<Path> files) {
    for (Path filePath : files) {
      try {
        FileTool.deleteFile(filePath);
      } catch (IOException e) {
        log.error("Error when trying to delete cover page file '{}'", filePath, e);
      }
    }
  }

  private ITemplateResolver getTemplateResolver() {
    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
    templateResolver.setPrefix("documents/");
    templateResolver.setSuffix(SolidifyConstants.HTML_EXT);
    templateResolver.setTemplateMode(TemplateMode.HTML);
    templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
    templateResolver.setOrder(1);
    templateResolver.setCheckExistence(true);
    templateResolver.setCacheable(false);
    return templateResolver;
  }

}
