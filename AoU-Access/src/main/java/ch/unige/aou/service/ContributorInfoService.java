/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - ContributorInfoService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.service;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.ByteResponseDto;
import ch.unige.aou.model.PublicContributorDTO;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.model.security.User;
import ch.unige.aou.model.settings.Person;
import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPersonRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class ContributorInfoService {

  private final TrustedUserRemoteResourceService trustedUserRemoteResourceService;
  private final TrustedContributorRemoteResourceService trustedContributorRemoteResourceService;
  private final TrustedPersonRemoteResourceService trustedPersonRemoteResourceService;

  public ContributorInfoService(TrustedUserRemoteResourceService trustedUserRemoteResourceService,
          TrustedContributorRemoteResourceService trustedContributorRemoteResourceService,
          TrustedPersonRemoteResourceService trustedPersonRemoteResourceService) {
    this.trustedUserRemoteResourceService = trustedUserRemoteResourceService;
    this.trustedContributorRemoteResourceService = trustedContributorRemoteResourceService;
    this.trustedPersonRemoteResourceService = trustedPersonRemoteResourceService;
  }

  public PublicContributorDTO getPublicInfos(String cnIndividu) {
    String fullUnigeExternalUid = CleanTool.getFullUnigeExternalUid(cnIndividu);
    PublicContributorDTO publicContributorDTO = new PublicContributorDTO();
    publicContributorDTO.setCnIndividu(cnIndividu);
    User user = this.trustedUserRemoteResourceService.findByExternalUid(fullUnigeExternalUid);
    if (user != null) {
      String personId = user.getPerson().getResId();
      Person person = this.trustedPersonRemoteResourceService.findOneWithCache(personId);
      List<Structure> structures = this.trustedPersonRemoteResourceService.getPersonStructures(personId);
      List<ResearchGroup> researchGroups = this.trustedPersonRemoteResourceService.getPersonResearchGroups(personId);
      publicContributorDTO.setLastName(person.getLastName());
      publicContributorDTO.setFirstName(person.getFirstName());
      publicContributorDTO.setResearchGroups(researchGroups);
      publicContributorDTO.setStructures(structures);
      publicContributorDTO.setOrcid(person.getOrcid());
      publicContributorDTO.setHasAvatar(person.getAvatar() != null);
      publicContributorDTO.setHasEmail(person.getEmail() != null && !person.getEmail().isEmpty());
      return publicContributorDTO;
    }
    Contributor contributor = this.trustedContributorRemoteResourceService.getByCnIndividu(cnIndividu);
    if (contributor != null) {
      publicContributorDTO.setLastName(contributor.getLastName());
      publicContributorDTO.setFirstName(contributor.getFirstName());
      publicContributorDTO.setOrcid(contributor.getOrcid());
      publicContributorDTO.setHasAvatar(false);
      publicContributorDTO.setHasEmail(false);
      return publicContributorDTO;
    }
    return null;
  }

  public ByteResponseDto getAvatar(String cnIndividu) throws URISyntaxException {
    User user = this.trustedUserRemoteResourceService.findByExternalUid(CleanTool.getFullUnigeExternalUid(cnIndividu));
    if (user != null) {
      String personId = user.getPerson().getResId();
      return this.trustedPersonRemoteResourceService.getPersonAvatar(personId);
    }
    return null;
  }

  public PublicContributorDTO getUnigeContributorPublicInfosByOrcid(String orcid) {
    Optional<Contributor> contributorOpt = this.trustedContributorRemoteResourceService.getByOrcid(orcid);
    if (contributorOpt.isPresent() && !StringTool.isNullOrEmpty(contributorOpt.get().getCnIndividu())) {
      return this.getPublicInfos(contributorOpt.get().getCnIndividu());
    }
    return null;
  }

}
