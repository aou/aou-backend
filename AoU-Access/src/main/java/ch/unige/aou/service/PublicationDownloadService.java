/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationDownloadService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.exception.SolidifyFileDeleteException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.SolidifyTime;

import ch.unige.aou.AouConstants;
import ch.unige.aou.business.AouResourceService;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.message.PublicationDownloadMessage;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.repository.PublicationDownloadRepository;
import ch.unige.aou.service.rest.trusted.TrustedPublicationDownloadRemoteResourceService;
import ch.unige.aou.specification.PublicationDownloadSpecification;

@Service
@ConditionalOnBean(AccessController.class)
public class PublicationDownloadService extends AouResourceService<PublicationDownload> {

  private static final Logger log = LoggerFactory.getLogger(PublicationDownloadService.class);

  private final TrustedPublicationDownloadRemoteResourceService trustedPublicationDownloadRemoteResourceService;

  public PublicationDownloadService(TrustedPublicationDownloadRemoteResourceService trustedPublicationDownloadRemoteResourceService) {
    this.trustedPublicationDownloadRemoteResourceService = trustedPublicationDownloadRemoteResourceService;
  }

  @Override
  public SolidifySpecification<PublicationDownload> getSpecification(PublicationDownload resource) {
    return new PublicationDownloadSpecification(resource);
  }

  public Optional<PublicationDownload> findByFinalData(URI finalData) {
    return ((PublicationDownloadRepository) this.itemRepository).findByFinalData(finalData);
  }

  public List<PublicationDownload> findByPublicationId(String resId) {
    return ((PublicationDownloadRepository) this.itemRepository).findByPublicationId(resId);
  }

  public PublicationDownload createAndSubmitPublicationDownload(String resId) {
    PublicationDownload publicationDownload = this.createOrFindPublicationDownload(resId);
    if (publicationDownload.getStatus() != PublicationDownload.DownloadStatus.READY) {
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationDownloadMessage(publicationDownload.getResId()));
    }
    return publicationDownload;
  }

  public synchronized PublicationDownload createOrFindPublicationDownload(String resId) {
    if (!this.existsById(resId)) {
      PublicationDownload publicationDownload = new PublicationDownload();
      publicationDownload.setResId(resId);
      publicationDownload.setStatus(PublicationDownload.DownloadStatus.SUBMITTED);
      return this.save(publicationDownload);
    } else {
      return this.findOne(resId);
    }
  }

  @Override
  public void delete(String resId) {
    PublicationDownload publicationDownload = this.findOne(resId);
    super.delete(resId);

    if (publicationDownload.getFinalData() != null) {
      Path parent = Path.of(publicationDownload.getFinalData()).getParent();
      try (Stream<Path> fileStream = Files.walk(parent)) {
        fileStream.sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
      } catch (IOException e) {
        throw new SolidifyFileDeleteException(e.getMessage());
      }
    }
  }

  public boolean exists(String documentFileId) {
    return this.trustedPublicationDownloadRemoteResourceService.exists(documentFileId);
  }

  public void prepareDownload(String documentFileId) {
    try {
      this.trustedPublicationDownloadRemoteResourceService.prepareDownload(documentFileId);
    } catch (SolidifyRestException e) {
      // in case of multiple calls to prepare download in parallel, a bad request may be received. We can just ignore it.
      log.debug("unable to prepare download for file " + documentFileId, e);
    }
  }

  public PublicationDownload waitForPublicationDownloadReady(String documentFileId) {
    PublicationDownload publicationDownload = this.trustedPublicationDownloadRemoteResourceService.findOne(documentFileId);
    int pauseDelay = 250;
    int maxWaitTimeInSeconds = 60;
    int maxLoops = maxWaitTimeInSeconds * 1000 / pauseDelay;
    int currentLoop = 0;
    while (publicationDownload.getStatus() != PublicationDownload.DownloadStatus.READY) {
      publicationDownload = this.trustedPublicationDownloadRemoteResourceService.findOne(documentFileId);
      SolidifyTime.waitInMilliSeconds(250);
      currentLoop++;
      log.debug("waiting for PublicationDownload {} to be ready (current status: {}) (loop {})", documentFileId, publicationDownload.getStatus(),
              currentLoop);
      if (currentLoop > maxLoops) {
        throw new SolidifyRuntimeException("Unable to prepare file to download " + documentFileId);
      }
    }
    return publicationDownload;
  }

  /**
   * Return the path of the file merged with cover page if it exists. Else return the original file's path.
   *
   * @param publicationDownload
   * @return
   */
  public Path getFileToDownloadPath(PublicationDownload publicationDownload) {
    URI finalDataUri = publicationDownload.getFinalData();
    Path finalDataPath = Path.of(finalDataUri);
    Path parentFolderPath = finalDataPath.getParent();
    String filename = finalDataPath.getFileName().toString();
    String filenameWithCoverPage = AouConstants.FILE_WITH_COVER_PAGE_PREFIX + filename;
    Path fileWithCoverPagePath = parentFolderPath.resolve(filenameWithCoverPage);
    if (fileWithCoverPagePath.toFile().exists()) {
      return fileWithCoverPagePath;
    } else {
      return finalDataPath;
    }
  }
}
