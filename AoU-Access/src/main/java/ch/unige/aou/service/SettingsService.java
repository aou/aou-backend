/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - SettingsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.bibliography.BibliographyFormat;
import ch.unige.aou.model.publication.PublicationSubtypeDTO;
import ch.unige.aou.model.search.AdvancedSearchCriteria;
import ch.unige.aou.model.settings.LabelDTO;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class SettingsService {

  private final TrustedPublicationSubtypeRemoteResourceService trustedPublicationSubtypeRemoteResourceService;
  private final MessageService messageService;

  private final List<AdvancedSearchCriteria> advancedSearchCriteriaProperties;
  private List<AdvancedSearchCriteria> completedAdvancedSearchCriteriaList = new ArrayList<>();

  private final List<String> bibliographyFormats;
  private List<BibliographyFormat> translatedBibliographyFormats = new ArrayList<>();

  public SettingsService(AouProperties aouProperties,
          TrustedPublicationSubtypeRemoteResourceService trustedPublicationSubtypeRemoteResourceService,
          MessageService messageService) {
    this.trustedPublicationSubtypeRemoteResourceService = trustedPublicationSubtypeRemoteResourceService;
    this.messageService = messageService;
    this.advancedSearchCriteriaProperties = aouProperties.getIndexing().getAdvancedSearchCriteria();
    this.bibliographyFormats = List.of(aouProperties.getBibliography().getCslFormats());
  }

  public List<AdvancedSearchCriteria> getAdvancedSearchCriteriaList() {

    if (this.completedAdvancedSearchCriteriaList.isEmpty()) {
      this.completedAdvancedSearchCriteriaList = this.buildAdvancedSearchCriteria();
    }

    return this.completedAdvancedSearchCriteriaList;
  }

  public List<BibliographyFormat> getBibliographyFormats() {
    if (this.translatedBibliographyFormats.isEmpty()) {
      this.translatedBibliographyFormats = this.buildTranslatedBibliographyFormats();
    }
    return this.translatedBibliographyFormats;
  }

  private List<AdvancedSearchCriteria> buildAdvancedSearchCriteria() {
    List<AdvancedSearchCriteria> advancedSearchCriteriaList = new ArrayList<>();

    for (AdvancedSearchCriteria advancedSearchCriteria : this.advancedSearchCriteriaProperties) {

      if (advancedSearchCriteria.getBuildType() == AdvancedSearchCriteria.BuildType.DOCUMENT_TYPE) {
        this.fillDocumentTypes(advancedSearchCriteria);
      }

      advancedSearchCriteriaList.add(advancedSearchCriteria);
    }

    return advancedSearchCriteriaList;
  }

  private void fillDocumentTypes(AdvancedSearchCriteria advancedSearchCriteria) {
    List<PublicationSubtypeDTO> publicationSubtypes = this.trustedPublicationSubtypeRemoteResourceService.findAllDTO(
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by("sortValue"))).getData();

    for (PublicationSubtypeDTO publicationSubtype : publicationSubtypes) {
      AdvancedSearchCriteria.EnumValue enumValue = new AdvancedSearchCriteria.EnumValue();
      enumValue.setValue(publicationSubtype.getName());
      for (LabelDTO label : publicationSubtype.getLabels()) {
        AdvancedSearchCriteria.Label asLabel = new AdvancedSearchCriteria.Label();
        asLabel.setLanguageCode(label.getLanguageCode());
        asLabel.setText(label.getText());
        enumValue.getLabels().add(asLabel);
      }
      advancedSearchCriteria.getSelectableValues().add(enumValue);
    }
  }

  private List<BibliographyFormat> buildTranslatedBibliographyFormats() {
    List<BibliographyFormat> translatedBibliographyFormatsList = new ArrayList<>();

    for (String formatName : this.bibliographyFormats) {
      BibliographyFormat bibliographyFormat = new BibliographyFormat();
      bibliographyFormat.setName(formatName);

      String engName = this.messageService.get("bibliography.format." + formatName.toLowerCase().replace("-", "_"), Locale.ENGLISH);
      if (StringTool.isNullOrEmpty(engName)) {
        engName = formatName;
      }
      LabelDTO engLabel = new LabelDTO();
      engLabel.setLanguageCode("eng");
      engLabel.setText(engName);
      bibliographyFormat.getLabels().add(engLabel);

      String freName = this.messageService.get("bibliography.format." + formatName.toLowerCase().replace("-", "_"), Locale.FRENCH);
      if (StringTool.isNullOrEmpty(freName)) {
        freName = formatName;
      }
      LabelDTO freLabel = new LabelDTO();
      freLabel.setLanguageCode("fre");
      freLabel.setText(freName);
      bibliographyFormat.getLabels().add(freLabel);

      translatedBibliographyFormatsList.add(bibliographyFormat);
    }

    return translatedBibliographyFormatsList;
  }
}
