/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - AouCitationGenerationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.citation;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.business.CitationService;
import ch.unige.solidify.config.CitationProperties;
import ch.unige.solidify.model.Citation;
import ch.unige.solidify.model.config.FormatsConfig;
import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.service.CitationGenerationService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.bibliography.BibliographyParameters;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.service.CommonMetadataService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class AouCitationGenerationService extends CitationGenerationService {

  private final String defaultStyle;
  private final String[] customCslStylePrefixes;

  protected AouCitationGenerationService(AouProperties aouProperties, CitationService citationService,
          TrustedPublicationRemoteResourceService publicationRemoteResourceService,
          TrustedPublicationSubtypeRemoteResourceService trustedPublicationSubtypeRemoteResourceService,
          CommonMetadataService commonMetadataService,
          CitationProperties citationProperties) {

    super(citationService, new PublicationIndexEntryDataProvider(aouProperties, publicationRemoteResourceService,
                    trustedPublicationSubtypeRemoteResourceService, commonMetadataService),
            new FormatsConfig(citationProperties.getBibliographies().getStyles(),
                    citationProperties.getBibliographies().getLanguages(),
                    citationProperties.getBibliographies().getOutputFormats()),
            new FormatsConfig(citationProperties.getCitations().getStyles(),
                    citationProperties.getCitations().getLanguages(),
                    citationProperties.getCitations().getOutputFormats()));

    this.defaultStyle = aouProperties.getBibliography().getDefaultFormat();
    this.customCslStylePrefixes = aouProperties.getBibliography().getCustomCslStylePrefixes();
  }

  public CitationDto getBibliographyDto(String publicationId, String style, String language, Citation.OutputFormat outputFormat) {
    if (StringTool.isNullOrEmpty(language)) {
      language = BibliographyParameters.Lang.ENGLISH.getCslCode();
    }
    if (StringTool.isNullOrEmpty(style)) {
      style = this.defaultStyle;
    }

    for (String prefix : this.customCslStylePrefixes) {
      if (style.startsWith(prefix)) {
        // custom classpath for UNIGE formats
        style = "bibliography/styles/" + style;
        break;
      }
    }
    String bibliographyText = this.getBibliographyText(publicationId, style, language, outputFormat);
    bibliographyText = CleanTool.restoreAllowedHtmlTags(bibliographyText, List.of(CleanTool.TAGS_ALLOWED_IN_TITLE));

    Citation citation = new Citation();
    citation.setText(bibliographyText);
    citation.setLanguage(language);
    citation.setStyle(style);
    citation.setOutputFormat(outputFormat);
    return new CitationDto(citation);
  }
}
