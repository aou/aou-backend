/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - ContributorSitemapService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.sitemap;

import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.service.sitemap.SolidifySitemapService;

import ch.unige.aou.AouConstants;
import ch.unige.aou.model.publication.Contributor;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.rest.trusted.TrustedContributorRemoteResourceService;

@Service
public class ContributorSitemapService extends SolidifySitemapService {

  private final TrustedContributorRemoteResourceService remoteContributorService;

  public ContributorSitemapService(
          SolidifyProperties config,
          TrustedContributorRemoteResourceService remoteContributorService) {
    super(config, "contributor/", "");
    this.remoteContributorService = remoteContributorService;
  }

  @Override
  public String getName() {
    return ResourceName.CONTRIBUTORS;
  }

  @Override
  public long getItemsTotal() {
    RestCollection<Contributor> result = this.getContributors(PageRequest.of(0, 1));
    return result.getPage().getTotalItems();
  }

  @Override
  public List<String> getItemsFrom(int from) {
    int page = from / this.getPageSize();
    Pageable pageable = PageRequest.of(page, this.getPageSize(), Sort.by(AouConstants.DB_CN_INDIVIDU));
    RestCollection<Contributor> result = this.getContributors(pageable);
    return result.getData().stream().map(p -> this.getItemPartialUrl(p.getCnIndividu())).toList();
  }

  private RestCollection<Contributor> getContributors(Pageable pageable) {
    return this.remoteContributorService.listUnigeContributors(pageable);
  }

}
