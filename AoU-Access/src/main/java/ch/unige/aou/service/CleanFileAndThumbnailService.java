/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - CleanFileAndThumbnailService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.config.AouProperties.WorkingDirectory;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.access.PublicationDownload;

@Service
@ConditionalOnBean(AccessController.class)
public class CleanFileAndThumbnailService {

  private static final Logger log = LoggerFactory.getLogger(CleanFileAndThumbnailService.class);

  private static final int DELETE_FILES_IN_BATCH = 20;
  private final PublicationDownloadService publicationDownloadService;
  private final String publicationsFolder;
  private final long maxDiskSpaceUsage;

  public CleanFileAndThumbnailService(AouProperties aouProperties, PublicationDownloadService publicationDownloadService) {
    this.maxDiskSpaceUsage = aouProperties.getMaxPercentageDiskSpaceUsage();
    this.publicationDownloadService = publicationDownloadService;
    this.publicationsFolder = aouProperties.getPublicationsFolder();
  }

  @Scheduled(fixedDelayString = "${aou.cleaningFilesAndThumbnailInterval:86400000}")
  public void cleanFilesAndThumbnailsFolder() throws IOException {
    log.info("start cleaning of files and thumbnails cache folders");

    ArrayList<File> filesList = new ArrayList<>();
    this.getAllFilesRecursively(new File(this.publicationsFolder + File.separator + WorkingDirectory.PUBLICATIONS.getName()), filesList);
    filesList.sort(Comparator.comparingLong(File::lastModified));
    this.deleteFilesWhileFreeSpaceReachLimit(filesList, Paths.get(this.publicationsFolder, WorkingDirectory.PUBLICATIONS.getName()), false);

    ArrayList<File> thumbnailList = new ArrayList<>();
    this.getAllFilesRecursively(new File(this.publicationsFolder + File.separator + WorkingDirectory.THUMBNAILS.getName()), thumbnailList);
    thumbnailList.sort(Comparator.comparingLong(File::lastModified));
    this.deleteFilesWhileFreeSpaceReachLimit(thumbnailList, Paths.get(this.publicationsFolder, WorkingDirectory.THUMBNAILS.getName()), true);

    log.info("finished cleaning of files and thumbnails cache folders");
  }

  private boolean isUsedSpaceReachingLimit() throws IOException {
    long freeSpace = Files.getFileStore(Paths.get(this.publicationsFolder).getParent()).getUsableSpace();
    long totalSpace = Files.getFileStore(Paths.get(this.publicationsFolder).getParent()).getTotalSpace();
    long freeSpacePercent = freeSpace * 100 / totalSpace;
    long usedSpacePercent = 100 - freeSpacePercent;
    long usedSpace = totalSpace - freeSpace;
    log.info("Total used space for folder {}: {} ({}) on {} allowed", this.publicationsFolder, usedSpacePercent + "%",
            StringTool.formatSmartSize(usedSpace), this.maxDiskSpaceUsage + "%");
    return usedSpacePercent > this.maxDiskSpaceUsage;
  }

  public void getAllFilesRecursively(File path, List<File> listFiles) {
    if (path.isFile()) {
      listFiles.add(path);
    } else {
      File[] files = path.listFiles();
      if (files != null) {
        for (File dirOrFile : files) {
          this.getAllFilesRecursively(dirOrFile, listFiles);
        }
      }
    }
  }

  /**
   * This method will delete the files in the path passed as parameter including also the directories if they are empty. Files will be deleted
   * in batches until one of the following conditions is met: there are no more files in the path or the usable space in disk reaches a certain
   * limit (defined in the conf).
   *
   * @param listFiles
   * @param rootPath
   * @throws IOException
   */
  public void deleteFilesWhileFreeSpaceReachLimit(List<File> listFiles, Path rootPath, boolean isThumbnail) throws IOException {
    int filesDeleted = 0;
    while (this.isUsedSpaceReachingLimit() && !listFiles.isEmpty()) {
      for (int i = 0; i < DELETE_FILES_IN_BATCH && !listFiles.isEmpty(); i++) {
        Path filePath = Paths.get(listFiles.get(0).getAbsolutePath());
        Optional<PublicationDownload> publicationDownloadOpt = this.publicationDownloadService.findByFinalData(filePath.toUri());
        if (publicationDownloadOpt.isPresent()) {
          this.publicationDownloadService.delete(publicationDownloadOpt.get().getResId());
          filesDeleted++;
        }
        if (isThumbnail) {
          this.deleteFile(listFiles.get(0));
          filesDeleted++;
        }
        listFiles.remove(0);
      }
    }

    if (filesDeleted > 0 && isThumbnail) {
      log.info("{} thumbnail(s) have been deleted", filesDeleted);
    } else if (filesDeleted > 0) {
      log.info("{} file(s) have been deleted", filesDeleted);
    }

    try (Stream<Path> filesStream = Files.walk(rootPath)) {
      filesStream.sorted(Comparator.reverseOrder())
              .map(Path::toFile)
              .filter(File::isDirectory)
              .filter(f -> Objects.requireNonNull(f.listFiles()).length == 0)
              .forEach(File::delete);
    } catch (IOException e) {
      // Exception throws when rootPath no longer exist
    }
  }

  public void deleteFile(File file) throws IOException {
    Files.delete(file.getAbsoluteFile().toPath());
  }
}
