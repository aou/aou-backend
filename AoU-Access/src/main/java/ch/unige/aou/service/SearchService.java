/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - SearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.OAIException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.model.oai.OAIRecord;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHerrorcodeType;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OAIRecordService;
import ch.unige.solidify.service.OAISearchService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.config.AouProperties.WorkingDirectory;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.service.query.QueryBuilderService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class SearchService extends AouService implements OAISearchService, OAIRecordService {

  private final String indexName;
  private final String publicationsFolder;
  private static final String THUMBNAIL_FILE_NAME = "thumbnail";

  private final TrustedPublicationRemoteResourceService publicationRemoteService;
  private final IndexResourceService<String, PublicationIndexEntry> searchRemoteService;
  private final QueryBuilderService queryBuilderService;

  public SearchService(
          AouProperties aouProperties,
          MessageService messageService,
          TrustedPublicationRemoteResourceService publicationRemoteService,
          IndexResourceService<String, PublicationIndexEntry> searchRemoteService,
          QueryBuilderService queryBuilderService) {
    super(messageService);
    this.publicationRemoteService = publicationRemoteService;
    this.publicationsFolder = aouProperties.getPublicationsFolder();
    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.searchRemoteService = searchRemoteService;
    this.queryBuilderService = queryBuilderService;
  }

  public FileSystemResource downloadThumbnail(String publicationId) throws IOException {
    Path thumbnailFolderPath = this.getThumbnailGeneralPath(publicationId);
    Path thumbnailPath = this.getThumbnailPathIfExist(thumbnailFolderPath);
    if (thumbnailPath == null) {
      this.publicationRemoteService.downloadThumbnail(publicationId, thumbnailFolderPath);
      // Rename file with content-type once it is downloaded
      InputStream is = new BufferedInputStream(new FileInputStream(thumbnailFolderPath.toFile()));
      String mimeType = URLConnection.guessContentTypeFromStream(is);
      String subtype = MediaType.valueOf(mimeType).getSubtype();
      thumbnailPath = Paths.get(thumbnailFolderPath.toString() + "." + subtype);
      Files.move(thumbnailFolderPath, thumbnailPath);
    }
    return new FileSystemResource(thumbnailPath);
  }

  private Path getThumbnailGeneralPath(String publicationId) {
    Path thumbnailFolderPath = Paths.get(this.publicationsFolder, WorkingDirectory.THUMBNAILS.getName(), publicationId);
    if (!thumbnailFolderPath.toFile().exists()) {
      try {
        Files.createDirectories(thumbnailFolderPath);
      } catch (final IOException e) {
        throw new SolidifyProcessingException(
                this.messageService
                        .get("archive.error.path_not_found", new Object[] { publicationId, this.publicationsFolder }));
      }
    }
    return thumbnailFolderPath.resolve(THUMBNAIL_FILE_NAME);
  }

  private Path getThumbnailPathIfExist(Path thumbnailPath) {
    return Arrays.stream(Objects.requireNonNull(thumbnailPath.getParent().toFile().listFiles())).filter(f -> f.getName().startsWith(
            THUMBNAIL_FILE_NAME)).map(File::toPath).findFirst().orElse(null);
  }

  @Override
  public RestCollection<OAIRecord> searchOAIRecords(String from, String until, String setQuery, Pageable pageable) {
    List<SearchCondition> searchConditionList = this.getSearchParameters(null, from, until, setQuery);
    if (!pageable.getSort().isSorted()) {
      pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("archiveIdInt"));
    }
    // Get fields to include/exclude from ES response
    FieldsRequest fieldsRequest = this.queryBuilderService.getDefaultFieldsRequest();
    FacetPage<PublicationIndexEntry> publicationIndexList = this.searchRemoteService.search(this.indexName, searchConditionList, null, pageable,
            fieldsRequest);
    final List<OAIRecord> oaiRecordList = new ArrayList<>();
    for (final PublicationIndexEntry publication : publicationIndexList.getContent()) {
      oaiRecordList.add(publication);
    }
    return new RestCollection<>(oaiRecordList, new RestCollectionPage(publicationIndexList.getNumber(),
            publicationIndexList.getNumberOfElements(), publicationIndexList.getTotalPages(), publicationIndexList.getTotalElements()));
  }

  @Override
  public boolean isInSet(String resId, String setQuery, Pageable pageable) {
    final String query = "_id:" + resId;
    List<SearchCondition> searchConditionList = this.getSearchParameters(query, null, null, setQuery);
    // Get fields to include/exclude from ES response
    FieldsRequest fieldsRequest = this.queryBuilderService.getDefaultFieldsRequest();
    FacetPage<PublicationIndexEntry> result = this.searchRemoteService.search(this.indexName, searchConditionList, null, pageable,
            fieldsRequest);
    return (result.getContent().size() == 1);
  }

  @Override
  public OAIRecord getRecord(String archiveId) {
    AouSearchCondition archiveIdCondition = this.queryBuilderService.getFindByArchiveIdCondition(archiveId);
    // Get fields to include/exclude from ES response
    FieldsRequest fieldsRequest = this.queryBuilderService.getDefaultFieldsRequest();
    FacetPage<PublicationIndexEntry> results = this.searchRemoteService.search(this.indexName, List.of(archiveIdCondition), null,
            Pageable.ofSize(10), fieldsRequest);
    if (results.getContent().size() == 1) {
      return results.getContent().get(0);
    } else if (!results.hasContent()) {
      throw new OAIException(OAIPMHerrorcodeType.ID_DOES_NOT_EXIST, "No publication found");
    } else {
      throw new OAIException(OAIPMHerrorcodeType.BAD_ARGUMENT, "Too many publications found");
    }
  }

  private List<SearchCondition> getSearchParameters(String query, String from, String to, String set) {
    List<SearchCondition> parameters = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(query)) {
      SearchCondition querySearch = new SearchCondition(SearchConditionType.QUERY);
      querySearch.setValue(query);
      parameters.add(querySearch);
    }
    if (!StringTool.isNullOrEmpty(from) || !StringTool.isNullOrEmpty(to)) {
      SearchCondition dateRange = new SearchCondition(SearchConditionType.RANGE);
      dateRange.setField("technicalInfos.updateTime");
      if (!StringTool.isNullOrEmpty(from)) {
        dateRange.setLowerValue(from);
      }
      if (!StringTool.isNullOrEmpty(to)) {
        dateRange.setUpperValue(to);
      }
      parameters.add(dateRange);
    }
    if (!StringTool.isNullOrEmpty(set)) {
      SearchCondition setSearch = new SearchCondition(SearchConditionType.QUERY);
      setSearch.setValue(set);
      parameters.add(setSearch);
    }
    return parameters;
  }

}
