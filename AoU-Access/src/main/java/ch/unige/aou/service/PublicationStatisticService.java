/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationStatisticService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ch.unige.aou.AouConstants;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.message.IncrementDownloadStatMessage;
import ch.unige.aou.message.PublicationStatMessage;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.model.access.StatisticsInfoDto;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.Download;
import ch.unige.aou.model.publication.Hit;
import ch.unige.aou.model.publication.PublicationStatistic;
import ch.unige.aou.repository.DownloadRepository;
import ch.unige.aou.repository.HitRepository;
import ch.unige.aou.repository.PublicationStatisticRepository;

@Service
@ConditionalOnBean(AccessController.class)
public class PublicationStatisticService {

  private static final Logger log = LoggerFactory.getLogger(PublicationStatisticService.class);
  private final DownloadRepository downloadRepository;
  private final HitRepository hitRepository;
  private final PublicationStatisticRepository publicationStatisticRepository;

  public PublicationStatisticService(DownloadRepository downloadRepository, HitRepository hitRepository,
          PublicationStatisticRepository publicationStatisticRepository) {
    this.downloadRepository = downloadRepository;
    this.hitRepository = hitRepository;
    this.publicationStatisticRepository = publicationStatisticRepository;
  }

  @EventListener
  @Async
  public void listenToCreateDownloadStats(IncrementDownloadStatMessage incrementDownloadStatMessage) {
    this.createDownloadStats(incrementDownloadStatMessage.getDocumentFileId(), incrementDownloadStatMessage.getPublicationDownload());
  }

  @EventListener
  @Async
  public void listenToPublicationStats(PublicationStatMessage publicationStatMessage) {
    this.createOrUpdatePublicationStatistics(publicationStatMessage.getArchive(), publicationStatMessage.isDownload(),
            publicationStatMessage.isView());
  }

  public synchronized Download createDownloadStats(String documentFileId, PublicationDownload document) {
    LocalDateTime now = LocalDateTime.now();
    Download documentStats = this.downloadRepository.findByDocumentFileAndYearAndMonth(documentFileId, now.getYear(), now.getMonthValue())
            .orElse(null);
    if (documentStats == null) {
      // create new record
      Download download = new Download();
      download.setDownloadNumber(1);
      download.setFilename(document.getName());
      download.setDocumentFileId(document.getResId());
      download.setPublicationId(document.getPublicationId());
      download.setYear(now.getYear());
      download.setMonth(now.getMonthValue());
      return this.downloadRepository.save(download);
    } else {
      // append additional downloadNumber for the current month
      documentStats.setDownloadNumber(documentStats.getDownloadNumber() + 1);
      log.debug("will increment download number for documentFileId " + documentFileId + " to " + documentStats.getDownloadNumber());
      return this.downloadRepository.save(documentStats);
    }
  }

  public Hit createViewStats(PublicationIndexEntry indexEntry) {
    Hit existingHit = this.hitRepository.findByPublicationId(indexEntry.getResId()).orElse(null);
    if (existingHit != null) {
      existingHit.setViewNumber(existingHit.getViewNumber() + 1);
      return this.hitRepository.save(existingHit);
    } else {
      Hit newHit = new Hit();
      newHit.setPublicationId(indexEntry.getResId());
      newHit.setArchiveId((String) indexEntry.getMetadata().get(AouConstants.INDEX_FIELD_ARCHIVE_ID));
      newHit.setType((String) indexEntry.getMetadata().get("type"));
      newHit.setViewNumber(1);
      return this.hitRepository.save(newHit);
    }
  }

  public StatisticsInfoDto getPublicationStatistics(String publicationId) {
    Hit hit = this.hitRepository.findByPublicationId(publicationId).orElse(null);
    Integer viewNumber = hit != null ? hit.getViewNumber() : 0;
    List<Download> downloadList = this.downloadRepository.findByPublicationId(publicationId);
    return new StatisticsInfoDto(viewNumber, downloadList.stream().map(Download::getDownloadNumber).reduce(0, Integer::sum));
  }

  public synchronized PublicationStatistic createOrUpdatePublicationStatistics(PublicationIndexEntry indexEntry, boolean isDownload,
          boolean isView) {
    PublicationStatistic publicationStatistic;
    String archiveId = indexEntry.getArchiveId();
    publicationStatistic = this.publicationStatisticRepository.findByArchiveId(archiveId);
    if (publicationStatistic != null) {
      // Update publication statistic
      publicationStatistic.setDownloadNumber(
              isDownload ? publicationStatistic.getDownloadNumber() + 1 : publicationStatistic.getDownloadNumber());
      publicationStatistic.setViewNumber(isView ? publicationStatistic.getViewNumber() + 1 : publicationStatistic.getViewNumber());
      publicationStatistic.setTitle(indexEntry.getTitle());
      publicationStatistic.setContainerTitle(indexEntry.getContainerTitle());
      this.setAccessLevel(publicationStatistic, indexEntry.getPrincipalFileAccessLevel(), indexEntry.getOpenAccess());
      return this.publicationStatisticRepository.save(publicationStatistic);
    } else {
      // Create publication statistic
      int hits = 0;
      Optional<Hit> hitOpt = this.hitRepository.findByArchiveId(archiveId);
      if (hitOpt.isPresent()) {
        hits = hitOpt.get().getViewNumber();
      }
      int downloads = this.downloadRepository.countDownloadByPublicationId(indexEntry.getResId());
      return this.createPublicationStatistic(archiveId, indexEntry, downloads, hits);
    }
  }

  public Page<PublicationStatistic> findByArchivesIds(List<String> archivesIds, Pageable pageable) {
    return this.publicationStatisticRepository.findByArchivesIds(archivesIds, pageable);
  }

  public PublicationStatistic createPublicationStatistic(String archiveId, PublicationIndexEntry indexEntry, int downloads, int hits) {
    PublicationStatistic publicationStatistic = new PublicationStatistic();
    publicationStatistic.setArchiveId(archiveId);
    this.setAccessLevel(publicationStatistic, indexEntry.getPrincipalFileAccessLevel(), indexEntry.getOpenAccess());
    publicationStatistic.setPublicationYear(indexEntry.getYear());
    publicationStatistic.setTitle(indexEntry.getTitle());
    publicationStatistic.setContainerTitle(indexEntry.getContainerTitle());
    publicationStatistic.setDownloadNumber(downloads);
    publicationStatistic.setViewNumber(hits);
    return this.publicationStatisticRepository.save(publicationStatistic);
  }

  public void setAccessLevel(PublicationStatistic publicationStatistic, String accessLevel, String openAccess) {
    if (accessLevel == null) {
      publicationStatistic.setAccessLevel(AouConstants.NO_FULLTEXT_TEXT);
      publicationStatistic.setOpenAccess(AouConstants.NOT_OPEN_ACCESS_TEXT);
    } else {
      publicationStatistic.setAccessLevel(accessLevel);
      publicationStatistic.setOpenAccess(openAccess);
    }
  }
}
