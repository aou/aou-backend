/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - BibliographyGeneratorService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import static ch.unige.aou.AouConstants.EMBARGOED_ACCESS_TEXT;
import static ch.unige.aou.AouConstants.INDEX_FIELD_YEARS;
import static ch.unige.aou.AouConstants.NOT_OPEN_ACCESS_TEXT;
import static ch.unige.aou.AouConstants.OPEN_ACCESS_TEXT;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.Citation;
import ch.unige.solidify.rest.BooleanClauseType;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.HashTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.bibliography.BibliographyParameters;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.model.publication.PublicationSubSubtypeDTO;
import ch.unige.aou.model.publication.PublicationSubtypeDTO;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.model.tool.CleanTool;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ResourceName;
import ch.unige.aou.service.citation.AouCitationGenerationService;
import ch.unige.aou.service.query.QueryBuilderService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationSubtypeRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class BibliographyGeneratorService {

  private static final Logger log = LoggerFactory.getLogger(BibliographyGeneratorService.class);

  private static final String BIBLIOGRAPHY_ACCESS_LEVEL_ON = "bibliography.access_level";
  private static final String BIBLIOGRAPHY_EMPTY_ACCESS_LEVEL = "bibliography.empty_access_level";
  private static final String BIBLIOGRAPHY_PUBLIC_ACCESS = "bibliography.public_access";
  private static final String BIBLIOGRAPHY_RESTRICTED_ACCESS = "bibliography.restricted_access";
  private static final String BIBLIOGRAPHY_CLOSED_ACCESS = "bibliography.closed_access";
  private static final String BIBLIOGRAPHY_OPEN_ACCESS = "bibliography.open_access";
  private static final String BIBLIOGRAPHY_EMBARGOED_OPEN_ACCESS = "bibliography.embargoed_open_access";

  private static final String BIBLIOGRAPHY_FORMATTING_ERROR = "bibliography.formatting_error";

  private static final String BIBLIOGRAPHY_JAVASCRIPT_TEMPLATE_FILE = "bibliography/bibliography_js_subtype_template.txt";
  private static final String ARCHIVE_ID = "{archiveId}";
  private static final String THESIS_SUPERVISION = "Thesis supervisions";
  private static final String MASTER_THESIS_SUPERVISION = "Master thesis supervisions";
  private static final String MASTER_OF_ADVANCED_THESIS_SUPERVISION = "Master of advanced studies supervisions";

  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;
  private final QueryBuilderService queryBuilderService;
  private final TrustedPublicationSubtypeRemoteResourceService trustedPublicationSubtypeRemoteResourceService;
  private final AouCitationGenerationService aouCitationGenerationService;
  private final MessageService messageService;

  private final String indexName;

  private final BibliographyParameters defaultParameters;

  private final String bibliographyJsTemplate;
  private final String publicationUrlTemplate;
  private final String publicationThumbnailUrlTemplate;
  private final String[] customCslStylePrefixes;

  private final String bibliographiesFolder;
  private final int bibliographiesCacheDuration;

  public BibliographyGeneratorService(AouProperties aouProperties, IndexResourceService<String, PublicationIndexEntry> indexResourceService,
          QueryBuilderService queryBuilderService, TrustedPublicationSubtypeRemoteResourceService trustedPublicationSubtypeRemoteResourceService,
          AouCitationGenerationService aouCitationGenerationService, MessageService messageService) {
    this.indexResourceService = indexResourceService;
    this.queryBuilderService = queryBuilderService;
    this.trustedPublicationSubtypeRemoteResourceService = trustedPublicationSubtypeRemoteResourceService;
    this.aouCitationGenerationService = aouCitationGenerationService;
    this.messageService = messageService;

    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.publicationUrlTemplate = aouProperties.getParameters().getPublicationUrlTemplate();
    this.publicationThumbnailUrlTemplate =
            aouProperties.getModule().getAccess().getPublicUrl() + "/" + ResourceName.METADATA + "/{archiveId}/" + AouActionName.THUMBNAIL;
    this.customCslStylePrefixes = aouProperties.getBibliography().getCustomCslStylePrefixes();

    this.defaultParameters = new BibliographyParameters();
    this.defaultParameters.setLimit(aouProperties.getBibliography().getLimit());
    this.defaultParameters.setFormat(aouProperties.getBibliography().getDefaultFormat());

    this.bibliographiesFolder = aouProperties.getBibliographiesFolder();
    this.bibliographiesCacheDuration = aouProperties.getParameters().getBibliographiesCacheDuration();

    try {
      this.bibliographyJsTemplate = FileTool.toString(new ClassPathResource(BIBLIOGRAPHY_JAVASCRIPT_TEMPLATE_FILE).getInputStream());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Unable to init bibliography JS template", e);
    }
  }

  public String buildJavascript(List<String> searchIds, List<String> publicationIds, String cnIndividu, Map<String, Object> parameters,
          String combinationType) {
    AouSearchCondition searchCondition = this.queryBuilderService.getSearchesOrPublicationsSearchCondition(searchIds, publicationIds,
            combinationType, cnIndividu);
    List<AouSearchCondition> searchConditions = new ArrayList<>();
    searchConditions.add(searchCondition);
    return this.buildJavascript(searchConditions, parameters);
  }

  public String buildJavascript(List<AouSearchCondition> searchConditions, Map<String, Object> parametersMap) {

    BibliographyParameters parameters = this.getBibliographyParameters(parametersMap);
    Pageable pageable = PageRequest.of(0, parameters.getLimit(), Sort.by(AouConstants.INDEX_FIELD_FIRST_VALIDATION_DATE).descending());
    this.fillYearsCondition(searchConditions, parameters);

    List<FacetRequest> facetRequests = new ArrayList<>(); // not interested in facets here
    List<SearchCondition> searchConditionList = this.queryBuilderService.replaceAliasesByIndexField(searchConditions);

    log.debug("will check cache for bibliography");
    String cacheKey = this.getCacheKey(searchConditionList, parameters);
    Optional<List<PublicationIndexEntry>> entriesOpt = this.readCacheFile(cacheKey);
    List<PublicationIndexEntry> entries;
    if (entriesOpt.isEmpty()) {
      log.debug("bibliography cache does not exist");
      // Get fields to include/exclude from ES response
      FieldsRequest fieldsRequest = this.queryBuilderService.getDefaultFieldsRequest();
      fieldsRequest.getExcludes().add(AouConstants.INDEX_FIELD_XML);
      FacetPage<PublicationIndexEntry> results = this.indexResourceService.search(this.indexName, searchConditionList, facetRequests, pageable,
              fieldsRequest);
      log.debug("bibliography content read from index");
      entries = results.getContent();
      this.writeCacheFile(cacheKey, results.getContent());
      log.debug("bibliography cache written to disk in file {}", cacheKey);
    } else {
      entries = entriesOpt.get();
      log.debug("bibliography content read from cache file");
    }

    this.replaceSubtypeForDirectors(searchConditionList, entries);

    return this.buildJavascript(entries, parameters);
  }

  private String getCacheKey(List<SearchCondition> searchConditionList, BibliographyParameters parameters) {
    StringBuilder sb = new StringBuilder();
    for (SearchCondition condition : searchConditionList) {
      sb.append("type:").append(condition.getType());
      sb.append(",boolClause:").append(condition.getBooleanClauseType());
      sb.append(",field:").append(condition.getField());
      if (condition.getMultiMatchFields() != null) {
        sb.append(",multimatch:").append(String.join(",", condition.getMultiMatchFields()));
      }
      sb.append(",value:").append(condition.getValue());
      if (condition.getTerms() != null) {
        sb.append(",terms:").append(String.join(",", condition.getTerms()));
      }
      sb.append(",upper:").append(condition.getUpperValue());
      sb.append(",lower:").append(condition.getLowerValue());
      if (condition.getNestedConditions() != null) {
        sb.append(",nested:").append(this.getCacheKey(condition.getNestedConditions(), null));
      }
    }
    if (parameters != null) {
      sb.append(",limit:").append(parameters.getLimit());
    }
    return HashTool.hash(sb.toString());
  }

  private Optional<List<PublicationIndexEntry>> readCacheFile(String cacheKey) {
    Path cacheFilePath = Path.of(this.bibliographiesFolder, cacheKey);
    Instant minusCreationTime = Instant.now().minusSeconds(this.bibliographiesCacheDuration);
    try {
      if (Files.exists(cacheFilePath)) {
        Instant lastModifiedTime = Files.getLastModifiedTime(cacheFilePath).toInstant();
        if (lastModifiedTime.isAfter(minusCreationTime)) {
          log.debug("Bibliography cache file {} exists (created at {})", cacheKey, lastModifiedTime);
        } else {
          Files.delete(cacheFilePath);
          log.debug("Bibliography cache file {} existed but was too old (created at {}). It has been deleted.", cacheKey, lastModifiedTime);
          return Optional.empty();
        }

        ObjectMapper mapper = new ObjectMapper();

        List<PublicationIndexEntry> entries = new ArrayList<>();
        List<Map<String, Object>> entryMaps = mapper.readValue(new File(String.valueOf(cacheFilePath)), List.class);
        for (Map<String, Object> metadataMap : entryMaps) {
          PublicationIndexEntry indexEntry = new PublicationIndexEntry();
          indexEntry.setMetadata(metadataMap);
          indexEntry.setResId(metadataMap.get("_resId").toString());
          indexEntry.setIndex(metadataMap.get("_index").toString());
          entries.add(indexEntry);
        }
        return Optional.of(entries);
      }
    } catch (IOException e) {
      log.error("An error occurred while reading cache file", e);
    }
    return Optional.empty();
  }

  private void writeCacheFile(String cacheKey, List<PublicationIndexEntry> entries) {
    Path cacheFilePath = Path.of(this.bibliographiesFolder, cacheKey);
    List<Map<String, Object>> metadataList = entries.stream().map(indexEntry -> {
      Map<String, Object> map = indexEntry.getMetadata();
      map.put("_resId", indexEntry.getResId());
      map.put("_index", indexEntry.getIndex());
      return map;
    }).toList();
    String resultsStr = JSONTool.convert2JsonString(metadataList);
    try {
      Files.write(cacheFilePath, resultsStr.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      log.error("An error occurred while creating cache file", e);
    }
  }

  private void replaceSubtypeForDirectors(List<SearchCondition> searchConditionList, List<PublicationIndexEntry> entries) {
    Set<String> directorsCnIndividus = new HashSet<>();
    this.completeDirectors(directorsCnIndividus, searchConditionList);

    if (!directorsCnIndividus.isEmpty()) {
      List<String> diplomeSubtypes = List.of(AouConstants.DEPOSIT_SUBTYPE_THESE_NAME, AouConstants.DEPOSIT_SUBTYPE_MASTER_D_ETUDES_AVANCEES_NAME,
              AouConstants.DEPOSIT_SUBTYPE_MASTER_NAME, AouConstants.DEPOSIT_SUBTYPE_THESE_DE_PRIVAT_DOCENT_NAME,
              AouConstants.DEPOSIT_SUBTYPE_THESE_DOCTORAL_PROFESSIONAL_NAME);
      for (PublicationIndexEntry indexEntry : entries) {
        if (diplomeSubtypes.contains(indexEntry.getSubtype())) {
          List<String> entryDirectors = indexEntry.getDirectorsCnIndividus();
          for (String directorCn : directorsCnIndividus) {
            if (entryDirectors.contains(directorCn)) {
              // Entry is a thesis, master, ... direction
              indexEntry.getMetadata().put("subtype", THESIS_SUPERVISION);
              break;
            }
          }
        }
      }
    }
  }

  private void completeDirectors(Set<String> directorsCnIndividus, List<SearchCondition> searchConditionList) {
    for (SearchCondition searchCondition : searchConditionList) {
      if (searchCondition.getType() == SearchConditionType.NESTED_BOOLEAN) {
        this.completeDirectors(directorsCnIndividus, searchCondition.getNestedConditions());
      } else if (searchCondition.getField().equals(AouConstants.INDEX_FIELD_UNIGE_DIRECTORS)) {
        directorsCnIndividus.add(searchCondition.getValue());
      }
    }
  }

  private List<PublicationIndexEntry> sortEntries(List<PublicationIndexEntry> entries, BibliographyParameters parameters) {
    Comparator<PublicationIndexEntry> comparator = null;
    // Group publications
    comparator = this.groupPublicationIndexEntries(parameters, comparator);

    // Sort in each group
    comparator = this.sortGroupingPublicationIndexEntries(parameters, comparator);

    return entries.stream().sorted(comparator).toList();
  }

  private Comparator<PublicationIndexEntry> groupPublicationIndexEntries(BibliographyParameters parameters,
          Comparator<PublicationIndexEntry> comparator) {

    if (parameters.isGroupByYear() && parameters.isGroupByType()) {
      comparator = Comparator.comparing(PublicationIndexEntry::getYear).reversed();
      comparator = comparator.thenComparing(entry -> this.getSubtypeSortValueByName(entry.getSubtype()));
      if (parameters.isGroupBySubtype()) {
        comparator = comparator.thenComparing(entry -> this.getSubSubtypeSortValueByName(entry.getSubtype(), entry.getSubSubtype()));
      }
    } else if (parameters.isGroupByType()) {
      comparator = Comparator.comparing(entry -> this.getSubtypeSortValueByName(entry.getSubtype()));
      if (parameters.isGroupBySubtype()) {
        comparator = comparator.thenComparing(entry -> this.getSubSubtypeSortValueByName(entry.getSubtype(), entry.getSubSubtype()));
      }
    } else if (parameters.isGroupByYear()) {
      comparator = Comparator.comparing(PublicationIndexEntry::getYear).reversed();
    }
    return comparator;
  }

  private Comparator<PublicationIndexEntry> sortGroupingPublicationIndexEntries(BibliographyParameters parameters,
          Comparator<PublicationIndexEntry> comparator) {

    switch (parameters.getSort()) {
      case AUTHORS -> {
        if (comparator == null) {
          comparator = Comparator.comparing(PublicationIndexEntry::getFirstContributorFullname);
        } else {
          comparator = comparator.thenComparing(PublicationIndexEntry::getFirstContributorFullname);
        }
      }
      case YEAR -> {
        if (comparator == null) {
          comparator = Comparator.comparing(PublicationIndexEntry::getYear).reversed();
        } else {
          comparator = comparator.thenComparing(Comparator.comparing(PublicationIndexEntry::getYear).reversed());
        }
      }
      case VALIDATION_DATE -> {
        if (comparator == null) {
          comparator = Comparator.comparing(PublicationIndexEntry::getValidationDate).reversed();
        } else {
          comparator = comparator.thenComparing(Comparator.comparing(PublicationIndexEntry::getValidationDate).reversed());
        }
      }
      case TITLE -> {
        if (comparator == null) {
          comparator = Comparator.comparing(PublicationIndexEntry::getTitle, String.CASE_INSENSITIVE_ORDER);
        } else {
          comparator = comparator.thenComparing(PublicationIndexEntry::getTitle, String.CASE_INSENSITIVE_ORDER);
        }
      }
    }
    return comparator;
  }

  private Integer getSubtypeSortValueByName(String name) {
    return switch (name) {
      case THESIS_SUPERVISION -> 100000;
      case MASTER_OF_ADVANCED_THESIS_SUPERVISION -> 100010;
      case MASTER_THESIS_SUPERVISION -> 100020;
      default -> this.getPublicationSubtypeDTO(name).getBibliographySortValue();
    };
  }

  private Integer getSubSubtypeSortValueByName(String subtypeName, String subSubtypeName) {
    int defaultSort = 100000;
    if (StringTool.isNullOrEmpty(subSubtypeName)) {
      return defaultSort;
    }

    try {
      return this.getPublicationSubSubtypeDTO(subtypeName, subSubtypeName).getBibliographySortValue();
    } catch (Exception e) {
      log.warn("Unable to retrieve publication subsubtype: {} ({} will be returned as default sort)", e.getMessage(), defaultSort);
      return defaultSort;
    }
  }

  private String getSubtypeLabelByName(String name, BibliographyParameters.Lang lang) {
    switch (name) {
      case THESIS_SUPERVISION -> {
        return this.messageService.get("bibliography.thesis_supervisions", lang.getLocale());
      }
      case MASTER_OF_ADVANCED_THESIS_SUPERVISION -> {
        return this.messageService.get("bibliography.master_of_advanced_thesis_supervisions", lang.getLocale());
      }
      case MASTER_THESIS_SUPERVISION -> {
        return this.messageService.get("bibliography.master_thesis_supervisions", lang.getLocale());
      }
      default -> {
        PublicationSubtypeDTO publicationSubtypeDTO = this.getPublicationSubtypeDTO(name);
        return this.messageService.get("bibliography.subtype." + publicationSubtypeDTO.getCode().toLowerCase() + ".plural", lang.getLocale());
      }
    }
  }

  private String getSubSubtypeLabelByName(String subtypeName, String subSubtypeName, BibliographyParameters.Lang lang) {
    if (!StringTool.isNullOrEmpty(subtypeName) && !StringTool.isNullOrEmpty(subSubtypeName)) {
      try {
        PublicationSubSubtypeDTO publicationSubSubtypeDTO = this.getPublicationSubSubtypeDTO(subtypeName, subSubtypeName);
        return this.messageService.get("bibliography.subsubtype." + publicationSubSubtypeDTO.getResId().toLowerCase() + ".plural",
                lang.getLocale());
      } catch (Exception e) {
        log.warn("Unable to retrieve publication subsubtype: {} (no label will be returned)", e.getMessage());
        return null;
      }
    } else {
      return null;
    }
  }

  private PublicationSubtypeDTO getPublicationSubtypeDTO(String name) {
    List<PublicationSubtypeDTO> sortedPublicationSubtypes = this.trustedPublicationSubtypeRemoteResourceService.findAllDTO(
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(AouConstants.BIBLIOGRAPHY_SORT_VALUE))).getData();
    Optional<PublicationSubtypeDTO> publicationSubtypeDtoOpt = sortedPublicationSubtypes.stream()
            .filter(subtypeDto -> subtypeDto.getName().equals(name)).findFirst();
    if (publicationSubtypeDtoOpt.isPresent()) {
      return publicationSubtypeDtoOpt.get();
    } else {
      throw new SolidifyRuntimeException("Unable to get PublicationSubtype for name '" + name + "'");
    }
  }

  private PublicationSubSubtypeDTO getPublicationSubSubtypeDTO(String subtypeName, String subSubtypeName) {
    PublicationSubtypeDTO publicationSubtypeDTO = this.getPublicationSubtypeDTO(subtypeName);

    List<PublicationSubSubtypeDTO> sortedPublicationSubSubtypes = this.trustedPublicationSubtypeRemoteResourceService.findAllPublicationSubSubtypesDTO(
                    publicationSubtypeDTO.getResId(), PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(AouConstants.BIBLIOGRAPHY_SORT_VALUE)))
            .getData();
    Optional<PublicationSubSubtypeDTO> publicationSubSubtypeDTO = sortedPublicationSubSubtypes.stream()
            .filter(subtypeDto -> subtypeDto.getName().equals(subSubtypeName)).findFirst();
    if (publicationSubSubtypeDTO.isPresent()) {
      return publicationSubSubtypeDTO.get();
    } else {
      throw new SolidifyRuntimeException(
              "Unable to get PublicationSubSubtype with name '" + subSubtypeName + "' for PublicationSubtype '" + publicationSubtypeDTO.getName()
                      + "'");
    }
  }

  private String buildJavascript(List<PublicationIndexEntry> entries, BibliographyParameters parameters) {
    List<PublicationIndexEntry> sortedEntries = this.sortEntries(entries, parameters);

    String javascript = this.bibliographyJsTemplate;

    // JS code is generated with a unique id to prevent collisions if multiple bibliographies are inserted in the same web page
    if (!StringTool.isNullOrEmpty(parameters.getUid())) {
      javascript = javascript.replace("[[unique_js_id]]", parameters.getUid());
    } else {
      throw new SolidifyRuntimeException("uid parameter is required");
    }

    StringBuilder sb = new StringBuilder();
    for (PublicationIndexEntry indexEntry : sortedEntries) {
      String text = this.getEntryJs(indexEntry, parameters);
      Integer year = indexEntry.getYear();
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
      String entryDate = indexEntry.getValidationDate().format(formatter);

      String i18nSubtype = this.getSubtypeLabelByName(indexEntry.getSubtype(), parameters.getLang());
      String subtypeHtml = StringTool.encodeNonAsciiCharsIntoHtmlHexadecimalEntities(i18nSubtype);
      subtypeHtml = subtypeHtml.replace("\"", "\\\"");

      String i18nSubSubtype = this.getSubSubtypeLabelByName(indexEntry.getSubtype(), indexEntry.getSubSubtype(), parameters.getLang());
      String subSubtypeHtml = null;
      if (i18nSubSubtype != null) {
        subSubtypeHtml = StringTool.encodeNonAsciiCharsIntoHtmlHexadecimalEntities(i18nSubSubtype);
        subSubtypeHtml = subSubtypeHtml.replace("\"", "\\\"");
      }

      sb.append("{\n")
              .append("text:\"" + text + "\",\n")
              .append("year:" + year + ",\n")
              .append("entryDate:" + entryDate + ",\n")
              .append("type:\"" + subtypeHtml + "\",\n");
      if (subSubtypeHtml != null) {
        sb.append("subtype:\"" + subSubtypeHtml + "\"\n");
      } else {
        sb.append("subtype:null\n");
      }
      sb.append("},\n");
    }

    javascript = javascript.replace("[[records_array]]", sb.toString());
    javascript = javascript.replace("[[group_by_year]]", (parameters.isGroupByYear() ? "true" : "false"));
    javascript = javascript.replace("[[group_by_type]]", (parameters.isGroupByType() ? "true" : "false"));
    javascript = javascript.replace("[[group_by_subtype]]", (parameters.isGroupBySubtype() ? "true" : "false"));

    return javascript;
  }

  private String getEntryJs(PublicationIndexEntry indexEntry, BibliographyParameters parameters) {
    StringBuilder format = new StringBuilder(parameters.getFormat());
    for (String prefix : this.customCslStylePrefixes) {
      if (parameters.getFormat().startsWith(prefix)) {
        // custom classpath for UNIGE formats
        format.setLength(0); // clear the string builder
        format.append("bibliography/styles/").append(parameters.getFormat());
        break;
      }
    }

    String citationText;
    try {
      citationText = this.getCitationText(indexEntry, format.toString(), parameters);
    } catch (Exception e) {
      log.error("Unable to get citation text for publication '{}' with format '{}'", indexEntry.getArchiveId(), format, e);
      Locale locale = this.getLocale(parameters);
      String formattingErrorMsg = this.messageService.get(BIBLIOGRAPHY_FORMATTING_ERROR, locale);
      citationText = indexEntry.getTitle().replace("\"", "\\\"") + " (" + formattingErrorMsg + ")";
    }

    boolean mustAddLink = true;
    if (parameters.isWithAccessLevel()) {
      String citationTextBeforeAccessLevel = citationText;
      citationText = this.addAccessLevel(citationText, indexEntry, parameters);
      mustAddLink = citationText.equals(citationTextBeforeAccessLevel);
    }
    if (mustAddLink) {
      citationText = this.addLink(citationText, indexEntry, parameters);
    } else {
      // allowed tags in title are not formatted yet
      citationText = CleanTool.restoreAllowedHtmlTags(citationText, List.of(CleanTool.TAGS_ALLOWED_IN_TITLE));
    }
    citationText = this.addAbstract(citationText, indexEntry, parameters);
    citationText = this.addThumbnail(citationText, indexEntry, parameters);

    citationText = StringTool.encodeNonAsciiCharsIntoHtmlHexadecimalEntities(citationText);

    return citationText;
  }

  public String getCitationText(PublicationIndexEntry indexEntry, String cslStyle, BibliographyParameters parameters) {
    String citationText = this.aouCitationGenerationService.getBibliographyText(indexEntry.getResId(), cslStyle,
            parameters.getLang().getCslCode(), Citation.OutputFormat.HTML);
    return this.removeDiv(citationText);
  }

  /**
   * AoU citations are on one line. Therefore we clean HTML div from generated text.
   *
   * @param citationText
   * @return
   */
  private String removeDiv(String citationText) {
    citationText = citationText.replace("\"", "\\\"");
    citationText = citationText.replace("\n", "");

    citationText = citationText.replace("<div class=\\\"csl-bib-body\\\">", "");
    citationText = citationText.replace("<div class=\\\"csl-entry\\\">", "");
    citationText = citationText.trim();
    citationText = StringTool.removeTrailing(citationText, "</div>");
    citationText = citationText.trim();
    citationText = StringTool.removeTrailing(citationText, "</div>");
    citationText = citationText.trim();

    if (citationText.contains("<div class=\\\"csl-left-margin\\\">")) {
      citationText = citationText.replace("<div class=\\\"csl-left-margin\\\">", "");
      citationText = citationText.replaceFirst("</div>", "");
      citationText = citationText.trim();
    }

    if (citationText.contains("<div class=\\\"csl-right-inline\\\">")) {
      citationText = citationText.replace("<div class=\\\"csl-right-inline\\\">", "");
      citationText = StringTool.removeTrailing(citationText, "</div>");
    }

    return citationText;
  }

  private String addAccessLevel(String citationText, PublicationIndexEntry indexEntry, BibliographyParameters parameters) {
    if (parameters.isWithAccessLevel() && !StringTool.isNullOrEmpty(indexEntry.getPrincipalFileAccessLevel())) {

      String startLinkTag = this.getHtmlLinkStart(indexEntry, parameters);
      String url = this.publicationUrlTemplate.replace(ARCHIVE_ID, indexEntry.getArchiveId());
      String linkTag = startLinkTag + url + "</a>";

      Locale locale = this.getLocale(parameters);

      String accessLevelText = null;
      String openAccess = indexEntry.getOpenAccess();
      String principalFileAccessLevel = indexEntry.getPrincipalFileAccessLevel();

      if (DocumentFile.AccessLevel.PUBLIC.metadataValue().equals(principalFileAccessLevel)) {
        if (OPEN_ACCESS_TEXT.equals(openAccess)) {
          accessLevelText =
                  this.messageService.get(BIBLIOGRAPHY_PUBLIC_ACCESS, locale) + " " + this.messageService.get(BIBLIOGRAPHY_OPEN_ACCESS, locale);
        } else if (NOT_OPEN_ACCESS_TEXT.equals(openAccess)) {
          accessLevelText = this.messageService.get(BIBLIOGRAPHY_PUBLIC_ACCESS, locale);
        }
      } else if (DocumentFile.AccessLevel.RESTRICTED.metadataValue().equals(principalFileAccessLevel)) {
        if (EMBARGOED_ACCESS_TEXT.equals(openAccess)) {
          accessLevelText = this.messageService.get(BIBLIOGRAPHY_RESTRICTED_ACCESS, locale) + " " + this.messageService.get(
                  BIBLIOGRAPHY_EMBARGOED_OPEN_ACCESS, locale);
        } else if (NOT_OPEN_ACCESS_TEXT.equals(openAccess)) {
          accessLevelText = this.messageService.get(BIBLIOGRAPHY_RESTRICTED_ACCESS, locale);
        }
      } else if (DocumentFile.AccessLevel.PRIVATE.metadataValue().equals(principalFileAccessLevel)) {
        if (EMBARGOED_ACCESS_TEXT.equals(openAccess)) {
          accessLevelText =
                  this.messageService.get(BIBLIOGRAPHY_CLOSED_ACCESS, locale) + " " + this.messageService.get(BIBLIOGRAPHY_EMBARGOED_OPEN_ACCESS,
                          locale);
        } else if (NOT_OPEN_ACCESS_TEXT.equals(openAccess)) {
          accessLevelText = this.messageService.get(BIBLIOGRAPHY_CLOSED_ACCESS, locale);
        }
      }

      if (!StringTool.isNullOrEmpty(accessLevelText)) {
        citationText =
                citationText + " " + this.messageService.get(BIBLIOGRAPHY_ACCESS_LEVEL_ON, new Object[] { accessLevelText, linkTag }, locale);
      } else {
        citationText = citationText + " " + this.messageService.get(BIBLIOGRAPHY_EMPTY_ACCESS_LEVEL, new Object[] { linkTag }, locale);
      }
    }
    return citationText;
  }

  private Locale getLocale(BibliographyParameters parameters) {
    return switch (parameters.getLang()) {
      case FRENCH -> Locale.FRENCH;
      case SPANISH -> new Locale("ES");
      case GERMAN -> Locale.GERMAN;
      default -> Locale.ENGLISH;
    };
  }

  private String addLink(String citationText, PublicationIndexEntry indexEntry, BibliographyParameters parameters) {

    String startLinkTag = this.getHtmlLinkStart(indexEntry, parameters);

    if (parameters.isLinkOnAllText()) {
      citationText = CleanTool.restoreAllowedHtmlTags(citationText, List.of(CleanTool.TAGS_ALLOWED_IN_TITLE));
      return startLinkTag + citationText + "</a>";
    } else {
      String title = indexEntry.getTitle();
      String htmlTitle = HtmlUtils.htmlEscape(title);
      htmlTitle = htmlTitle.replace("&#39;", "&rsquo;");

      // try to find the title in the citation text which may have a different case from the original after the CSL formatting applied
      String cslTitle = StringTool.findIgnoreCase(citationText, htmlTitle);
      if (StringTool.isNullOrEmpty(cslTitle)) {
        // Try to search again after having replaced quotes which have been replaced by CSL
        String quoteCitationText = CleanTool.cleanQuotes(citationText);
        String quoteHtmlTitle = CleanTool.cleanQuotes(htmlTitle);
        cslTitle = StringTool.findIgnoreCase(quoteCitationText, quoteHtmlTitle);
        if (!StringTool.isNullOrEmpty(cslTitle)) {
          citationText = quoteCitationText;
          htmlTitle = quoteHtmlTitle;
        } else {
          // If we cannot find it, we build the link with the original title case
          cslTitle = htmlTitle;
        }
      }
      String finalTitle = CleanTool.restoreAllowedHtmlTags(cslTitle, List.of(CleanTool.TAGS_ALLOWED_IN_TITLE));
      String titleLink = startLinkTag + finalTitle + "</a>";
      return StringTool.replaceIgnoreCase(citationText, htmlTitle, titleLink);
    }
  }

  private String getHtmlLinkStart(PublicationIndexEntry indexEntry, BibliographyParameters parameters) {
    String url = this.publicationUrlTemplate.replace(ARCHIVE_ID, indexEntry.getArchiveId());
    String startLinkTag = "<a href=\\\"" + url + "\\\">";
    if (parameters.isLinkTargetBlank()) {
      startLinkTag = "<a target=\\\"_blank\\\" href=\\\"" + url + "\\\">";
    }
    return startLinkTag;
  }

  private String addAbstract(String citationText, PublicationIndexEntry indexEntry, BibliographyParameters parameters) {
    if (parameters.isWithAbstract() && !StringTool.isNullOrEmpty(indexEntry.getFirstAbstract())) {
      String abstrct = indexEntry.getFirstAbstract();
      abstrct = StringTool.truncateOnSpaceWithEllipsis(abstrct, parameters.getAbstractLength());
      abstrct = abstrct.replace("\n", "");
      abstrct = HtmlUtils.htmlEscape(abstrct);
      abstrct = CleanTool.restoreAllowedHtmlTags(abstrct, List.of(CleanTool.TAGS_ALLOWED_IN_ABSTRACT));
      citationText = citationText + "<blockquote>" + abstrct + "</blockquote>";
    }
    return citationText;
  }

  private String addThumbnail(String citationText, PublicationIndexEntry indexEntry, BibliographyParameters parameters) {
    if (parameters.isWithThumbnail() && indexEntry.hasThumbnail()) {

      String url = this.publicationThumbnailUrlTemplate.replace(ARCHIVE_ID, indexEntry.getResId());
      citationText = citationText + "<div class=\\\"vignette\\\"><img src=\\\"" + url
              + "\\\" style=\\\"max-width: 500px; max-height: 400px; margin: 20px 20px 20px 100px;\\\"></div>";
    }
    return citationText;
  }

  private void fillYearsCondition(List<AouSearchCondition> searchConditions, BibliographyParameters parameters) {

    AouSearchCondition aouSearchCondition = new AouSearchCondition();
    aouSearchCondition.setBooleanClauseType(BooleanClauseType.MUST);
    aouSearchCondition.setType(SearchConditionType.RANGE);

    if (parameters.getYears() != null) {
      // Number of years in past
      int todayYear = LocalDate.now().getYear();
      int minYear = todayYear - parameters.getYears() + 1; // if years is 1, take current year only
      aouSearchCondition.setLowerValue(String.valueOf(minYear));
      aouSearchCondition.setField(INDEX_FIELD_YEARS);
      searchConditions.add(aouSearchCondition);
    } else if (parameters.getFromYear() != null || parameters.getToYear() != null) {
      // From and/or To year
      if (parameters.getFromYear() != null) {
        aouSearchCondition.setLowerValue(parameters.getFromYear().toString());
      }
      if (parameters.getToYear() != null) {
        aouSearchCondition.setUpperValue(parameters.getToYear().toString());
      }
      aouSearchCondition.setField(INDEX_FIELD_YEARS);
      searchConditions.add(aouSearchCondition);
    }
  }

  private BibliographyParameters getBibliographyParameters(Map<String, Object> parameters) {
    BibliographyParameters params = this.defaultParameters.clone();
    for (Map.Entry<String, Object> entry : parameters.entrySet()) {
      switch (entry.getKey()) {
        case "uid":
          params.setUid((String) entry.getValue());
          break;
        case "lang":
          params.setLang((String) entry.getValue());
          break;
        case "fromYear", "sinceYear":
          if (this.isInt(entry.getValue().toString())) {
            params.setFromYear(Integer.parseInt(entry.getValue().toString()));
          }
          break;
        case "toYear":
          if (this.isInt(entry.getValue().toString())) {
            params.setToYear(Integer.parseInt(entry.getValue().toString()));
          }
          break;
        case "years":
          if (this.isInt(entry.getValue().toString())) {
            params.setYears(Integer.parseInt(entry.getValue().toString()));
          }
          break;
        case "limit":
          if (this.isInt(entry.getValue().toString())) {
            params.setLimit(Integer.parseInt(entry.getValue().toString()));
          }
          break;
        case "format":
          params.setFormat((String) entry.getValue());
          break;
        case "sort":
          params.setSort(BibliographyParameters.SortType.valueOf((String) entry.getValue()));
          break;
        case "groupByType":
          params.setGroupByType(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        case "groupBySubtype":
          params.setGroupBySubtype(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        case "groupByYear":
          params.setGroupByYear(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        case "linkOnAllText":
          params.setLinkOnAllText(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        case "linkTargetBlank":
          params.setLinkTargetBlank(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        case "withAbstract":
          params.setWithAbstract(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        case "abstractLength":
          if (this.isInt(entry.getValue().toString())) {
            params.setAbstractLength(Integer.parseInt(entry.getValue().toString()));
          }
          break;
        case "withThumbnail":
          params.setWithThumbnail(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        case "withAccessLevel":
          params.setWithAccessLevel(Boolean.parseBoolean(entry.getValue().toString()));
          break;
        default:
          log.debug("No bibliography found");
          break;
      }
    }
    return params;
  }

  public Map<String, Object> buildParametersFromLegacyParameters(Map<String, Object> parameters) {
    if (parameters.containsKey("lang")) {
      switch (parameters.get("lang").toString()) {
        case "en":
          parameters.put("lang", "eng");
          break;
        case "fr":
          parameters.put("lang", "fre");
          break;
        case "es":
          parameters.put("lang", "spa");
          break;
        default:
          log.debug("no code language found");
          break;
      }
    }

    if (parameters.containsKey("sort")) {
      switch (parameters.get("sort").toString()) {
        case "authors":
          parameters.put("sort", BibliographyParameters.SortType.AUTHORS.toString());
          break;
        case "year":
          parameters.put("sort", BibliographyParameters.SortType.YEAR.toString());
          break;
        case "title":
          parameters.put("sort", BibliographyParameters.SortType.TITLE.toString());
          break;
        default:
          log.debug("no sort criteria found");
          break;
      }
    }

    if (parameters.containsKey("csl")) {
      switch (parameters.get("csl").toString()) {
        case "american-chemical-society":
          parameters.put("format", "unige-american-chemical-society");
          break;
        case "nlm":
          parameters.put("format", "unige-nlm");
          break;
        case "vancouver-brackets-no-et-al":
          parameters.put("format", "unige-vancouver-brackets-no-et-al");
          break;
        case "vancouver":
          parameters.put("format", "unige-vancouver");
          break;
        case "unige-long-with-abstract":
          parameters.put("format", "unige-long");
          parameters.put("withAbstract", "true");
          break;
        default:
          parameters.put("format", parameters.get("csl").toString());
          break;
      }
    }

    if (parameters.containsKey("link_all")) {
      parameters.put("linkOnAllText", parameters.get("link_all"));
    }

    if (parameters.containsKey("target_blank")) {
      parameters.put("linkTargetBlank", parameters.get("target_blank"));
    }

    if (parameters.containsKey("group_year")) {
      parameters.put("groupByYear", parameters.get("group_year"));
    }

    if (parameters.containsKey("group_type")) {
      parameters.put("groupByType", parameters.get("group_type"));
    }

    if (parameters.containsKey("from_year")) {
      parameters.put("fromYear", parameters.get("from_year"));
    }

    if (parameters.containsKey("since_year")) {
      parameters.put("sinceYear", parameters.get("since_year"));
    }

    if (parameters.containsKey("to_year")) {
      parameters.put("toYear", parameters.get("to_year"));
    }

    if (parameters.containsKey("with_abstract") && !parameters.containsKey("withAbstract")) {
      parameters.put("withAbstract", parameters.get("with_abstract"));
    }

    if (parameters.containsKey("with_vignette")) {
      parameters.put("withThumbnail", parameters.get("with_vignette"));
    }

    if (parameters.containsKey("with_access_level")) {
      parameters.put("withAccessLevel", parameters.get("with_access_level"));
    }

    return parameters;
  }

  private boolean isInt(String str) {
    try {
      Integer.parseInt(str);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}
