/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationSitemapService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.sitemap;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.service.sitemap.SolidifySitemapService;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.rest.ResourceName;

@Service
public class PublicationSitemapService extends SolidifySitemapService {

  private final String indexName;
  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;

  public PublicationSitemapService(
          SolidifyProperties config,
          AouProperties aouProperties,
          IndexResourceService<String, PublicationIndexEntry> indexResourceService) {
    super(config, "", "");
    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.indexResourceService = indexResourceService;
  }

  @Override
  public String getName() {
    return ResourceName.PUBLICATIONS;
  }

  @Override
  public long getItemsTotal() {
    FacetPage<PublicationIndexEntry> result = this.getPublications(PageRequest.of(0, 1));
    return result.getTotalElements();
  }

  @Override
  public List<String> getItemsFrom(int from) {
    int page = from / this.getPageSize();
    Pageable pageable = PageRequest.of(page, this.getPageSize(), Sort.by(AouConstants.INDEX_FIELD_ARCHIVE_ID_INT));
    FacetPage<PublicationIndexEntry> result = this.getPublications(pageable);
    return result.stream().map(p -> this.getItemPartialUrl(p.getArchiveId())).toList();
  }

  private FacetPage<PublicationIndexEntry> getPublications(Pageable pageable) {
    List<SearchCondition> searchConditionList = new ArrayList<>();
    FieldsRequest fieldsRequest = new FieldsRequest();
    fieldsRequest.getIncludes().add(AouConstants.INDEX_FIELD_ARCHIVE_ID);
    return this.indexResourceService.search(this.indexName, searchConditionList, null, pageable, fieldsRequest);
  }

}
