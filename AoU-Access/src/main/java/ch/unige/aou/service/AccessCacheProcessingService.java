/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - AccessCacheProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.FileTool;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.config.AouProperties.WorkingDirectory;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.message.PublicationUpdateMessage;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.service.citation.AouCitationGenerationService;

@Service
@ConditionalOnBean(AccessController.class)
public class AccessCacheProcessingService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(AccessCacheProcessingService.class);

  private final String publicationsFolder;

  private final PublicationDownloadService publicationDownloadService;
  private final AouCitationGenerationService aouCitationGenerationService;

  public AccessCacheProcessingService(AouProperties aouProperties, MessageService messageService,
          PublicationDownloadService publicationDownloadService, AouCitationGenerationService aouCitationGenerationService) {
    super(messageService);
    this.publicationsFolder = aouProperties.getPublicationsFolder();
    this.publicationDownloadService = publicationDownloadService;
    this.aouCitationGenerationService = aouCitationGenerationService;
  }

  @Transactional
  public boolean processPublicationUpdateMessage(PublicationUpdateMessage publicationUpdateMessage) {
    this.clearPublicationDownloadsForPublication(publicationUpdateMessage);
    this.clearThumbnailForPublication(publicationUpdateMessage);
    this.clearCitationsForPublication(publicationUpdateMessage);
    return true;
  }

  private void clearPublicationDownloadsForPublication(PublicationUpdateMessage publicationUpdateMessage) {
    List<PublicationDownload> publicationDownloads = this.publicationDownloadService.findByPublicationId(publicationUpdateMessage.getResId());
    for (PublicationDownload publicationDownload : publicationDownloads) {
      this.publicationDownloadService.delete(publicationDownload.getResId());
      log.info("PublicationDownload '{}' linked to publication '{}' deleted", publicationDownload.getName(),
              publicationDownload.getPublicationId());
    }
  }

  private void clearThumbnailForPublication(PublicationUpdateMessage publicationUpdateMessage) {
    Path publicationThumbnailFolderPath = Paths.get(this.publicationsFolder, WorkingDirectory.THUMBNAILS.getName(),
            publicationUpdateMessage.getResId());
    if (FileTool.isFolder(publicationThumbnailFolderPath)) {
      FileTool.deleteFolder(publicationThumbnailFolderPath);
    }
  }

  private void clearCitationsForPublication(PublicationUpdateMessage publicationUpdateMessage) {
    this.aouCitationGenerationService.deleteAllStoredCitationsForItem(publicationUpdateMessage.getResId());
    log.info("Citations texts linked to publication '{}' deleted", publicationUpdateMessage.getResId());
  }
}
