/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationDownloadPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service.security;

import java.util.NoSuchElementException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRestException;

import ch.unige.aou.controller.AccessController;
import ch.unige.aou.controller.AouControllerAction;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.service.rest.propagate.PropagatePublicationRemoteResourceService;
import ch.unige.aou.service.rest.propagate.PropagateUserRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class PublicationDownloadPermissionService extends AbstractPermissionService {

  private final PropagatePublicationRemoteResourceService publicationRemoteService;
  private final TrustedDocumentFileRemoteResourceService documentFileRemoteResourceService;
  private final PropagateUserRemoteResourceService userRemoteService;

  public PublicationDownloadPermissionService(PropagatePublicationRemoteResourceService publicationRemoteResourceService,
          TrustedDocumentFileRemoteResourceService documentFileRemoteResourceService, PropagateUserRemoteResourceService userService) {
    this.publicationRemoteService = publicationRemoteResourceService;
    this.documentFileRemoteResourceService = documentFileRemoteResourceService;
    this.userRemoteService = userService;
  }

  public boolean isAllowed(String documentFileId, String actionString) {
    if (this.isRootOrTrustedOrAdminRole()) {
      return true;
    }
    return this.isAllowedToDoAction(documentFileId, actionString);
  }

  public boolean isAllowedToDoAction(String documentFileId, String actionString) {
    DocumentFile existingResource = this.documentFileRemoteResourceService.findOne(documentFileId);
    if (existingResource == null) {
      throw new NoSuchElementException(documentFileId);
    }
    AouControllerAction action = this.getControllerAction(actionString);
    return this.isAllowedToPerformActionOnResource(existingResource, action);
  }

  protected boolean isAllowedToPerformActionOnResource(DocumentFile resourceToCheck, AouControllerAction action) {
    try {
      if (resourceToCheck != null) {
        final DocumentFile.AccessLevel currentAccessLevel = resourceToCheck.getCurrentAccessLevel();
        if (currentAccessLevel == DocumentFile.AccessLevel.PUBLIC) {
          if (action == AouControllerAction.DOWNLOAD_FILE || action == AouControllerAction.PREPARE_DOWNLOAD_ARCHIVE) {
            return true;
          }
        } else if (currentAccessLevel == DocumentFile.AccessLevel.RESTRICTED) {
          return this.isAllowedActionWithRestrictedAccess(action);
        } else if (currentAccessLevel == DocumentFile.AccessLevel.PRIVATE) {
          return this.isAllowedActionWithClosedAccess(resourceToCheck, action);
        }
      }
    } catch (SolidifyRestException e) {
      // Needed to catch for request made with downloadToken in cookie since request are made with propagateUser and propagatePublication
      // which are made with anonymous user.
    }
    return false;
  }

  public boolean isAllowedActionWithRestrictedAccess(AouControllerAction action) {
    if (action == AouControllerAction.DOWNLOAD_FILE || action == AouControllerAction.PREPARE_DOWNLOAD_ARCHIVE) {
      return this.userRemoteService.isUserConnectedMemberOfAuthorizedInstitution();
    }
    return false;
  }

  private boolean isAllowedActionWithClosedAccess(DocumentFile resourceToCheck, AouControllerAction action) {
    if (action == AouControllerAction.DOWNLOAD_FILE || action == AouControllerAction.PREPARE_DOWNLOAD_ARCHIVE) {
      return this.publicationRemoteService.isCreatorOrContributorOrValidator(resourceToCheck.getPublication().getResId());
    }
    return false;
  }
}
