/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - AccessCacheService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.message.PublicationUpdateMessage;

@Service
@ConditionalOnBean(AccessController.class)
public class AccessCacheService extends MessageProcessor<PublicationUpdateMessage> {

  private final AccessCacheProcessingService accessCacheProcessingService;

  public AccessCacheService(AouProperties aouProperties, AccessCacheProcessingService accessCacheProcessingService) {
    super(aouProperties);
    this.accessCacheProcessingService = accessCacheProcessingService;
  }

  @Override
  @JmsListener(destination = "${aou.topic.publicationsUpdate}", containerFactory = "topicListenerFactory")
  public void receiveMessage(PublicationUpdateMessage publicationUpdateMessage) {
    this.sendForParallelProcessing(publicationUpdateMessage);
  }

  @Override
  public void processMessage(PublicationUpdateMessage publicationUpdateMessage) {
    this.accessCacheProcessingService.processPublicationUpdateMessage(publicationUpdateMessage);
  }
}
