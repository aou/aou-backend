/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationDownloadProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.SolidifyTime;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.config.AouProperties.WorkingDirectory;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.DocumentFile;
import ch.unige.aou.service.document.CoverPageGenerator;
import ch.unige.aou.service.rest.trusted.TrustedDocumentFileRemoteResourceService;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;

@Service
@ConditionalOnBean(AccessController.class)
public class PublicationDownloadProcessingService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(PublicationDownloadProcessingService.class);

  private final PublicationDownloadService publicationDownloadService;
  private final TrustedDocumentFileRemoteResourceService documentFileRemoteService;
  private final TrustedPublicationRemoteResourceService publicationRemoteService;

  private String indexName;
  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;
  private final CoverPageGenerator coverPageGenerator;
  private final String publicationsFolder;
  private final FieldsRequest defaultFieldsRequestWithXml;

  public PublicationDownloadProcessingService(AouProperties aouProperties, MessageService messageService,
          PublicationDownloadService publicationDownloadService, TrustedDocumentFileRemoteResourceService documentFileRemoteService,
          TrustedPublicationRemoteResourceService publicationRemoteService,
          IndexResourceService<String, PublicationIndexEntry> indexResourceService, CoverPageGenerator coverPageGenerator) {
    super(messageService);
    this.publicationDownloadService = publicationDownloadService;
    this.documentFileRemoteService = documentFileRemoteService;
    this.publicationRemoteService = publicationRemoteService;
    this.indexResourceService = indexResourceService;
    this.coverPageGenerator = coverPageGenerator;
    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.publicationsFolder = aouProperties.getPublicationsFolder();
    this.defaultFieldsRequestWithXml = aouProperties.getIndexing().getDefaultFieldsRequestWithXml();
  }

  @Transactional
  public PublicationDownload processArchiveDownloadUntilReady(PublicationDownload publicationDownload) {
    while (this.isArchiveInProgress(publicationDownload)) {
      publicationDownload = this.processArchiveDownload(publicationDownload.getResId());
      SolidifyTime.waitInMilliSeconds(200);
    }
    return publicationDownload;
  }

  private PublicationDownload processArchiveDownload(String id) {
    PublicationDownload publicationDownload = this.publicationDownloadService.findOne(id);

    this.logPublicationMessage(LogLevel.INFO, publicationDownload, "will be processed");

    try {
      switch (publicationDownload.getStatus()) {
        case SUBMITTED:
          this.processSubmittedArchive(publicationDownload);
          break;
        case IN_PREPARATION:
          this.processInPreparationArchive(publicationDownload);
          break;
        case DOWNLOADING:
          this.processDownloadingArchive(publicationDownload);
          break;
        case GENERATING_COVER_PAGE:
          this.processGeneratingCoverPageArchive(publicationDownload);
          break;
      }
    } catch (Exception e) {
      publicationDownload.setStatus(PublicationDownload.DownloadStatus.IN_ERROR);
      log.error("Unable to process publication download for PublicationDownload " + id, e);
    }
    return this.publicationDownloadService.save(publicationDownload);
  }

  private void processGeneratingCoverPageArchive(PublicationDownload publicationDownload) throws MalformedURLException {
    // Get publication metadata from index
    PublicationIndexEntry indexEntry = this.indexResourceService.findOne(this.indexName, publicationDownload.getPublicationId(),
            this.defaultFieldsRequestWithXml);
    // Generate cover page
    this.coverPageGenerator.generateCoverPage(publicationDownload.getResId(), Path.of(publicationDownload.getFinalData()), indexEntry);
    publicationDownload.setStatus(PublicationDownload.DownloadStatus.READY);
  }

  private void processDownloadingArchive(PublicationDownload publicationDownload) {
    //Check if the file has been downloaded
    Path downloadPath = this.getDownloadFolderPath(publicationDownload);
    Path filePath = downloadPath.resolve(publicationDownload.getName());
    if (Files.exists(filePath)) {
      publicationDownload.setFinalData(filePath.toUri());
      publicationDownload.setStatus(PublicationDownload.DownloadStatus.GENERATING_COVER_PAGE);
    }
  }

  private void processInPreparationArchive(PublicationDownload publicationDownload) {
    Path downloadPath = this.getDownloadFolderPath(publicationDownload);
    Path filePath = downloadPath.resolve(publicationDownload.getName());
    this.publicationRemoteService.downloadDocumentFile(publicationDownload.getPublicationId(), publicationDownload.getResId(), filePath);
    publicationDownload.setStatus(PublicationDownload.DownloadStatus.DOWNLOADING);
  }

  private Path getDownloadFolderPath(PublicationDownload publicationDownload) {
    final Path downloadFolderPath = Paths.get(this.publicationsFolder, WorkingDirectory.PUBLICATIONS.getName(),
            publicationDownload.getPublicationId(), publicationDownload.getResId());
    if (!downloadFolderPath.toFile().exists()) {
      try {
        Files.createDirectories(downloadFolderPath);
      } catch (final IOException e) {
        this.logPublicationMessage(LogLevel.WARN, publicationDownload, "Problem creating publication directory");
        throw new SolidifyProcessingException(
                this.messageService
                        .get("archive.error.path_not_found", new Object[] { publicationDownload.getResId(), this.publicationsFolder }));
      }
    }
    return downloadFolderPath;
  }

  private void processSubmittedArchive(PublicationDownload publicationDownload) {
    //Find the publication from the document file that is about to download
    DocumentFile documentFile = this.documentFileRemoteService.findOne(publicationDownload.getResId());
    publicationDownload.setPublicationId(documentFile.getPublication().getResId());
    publicationDownload.setStatus(PublicationDownload.DownloadStatus.IN_PREPARATION);
    publicationDownload.setName(documentFile.getFileName());
    publicationDownload.setContentType(documentFile.getContentType());
    publicationDownload.setFileSize(documentFile.getFileSize());
  }

  protected void logPublicationMessage(LogLevel logLevel, PublicationDownload publicationDownload, String message, Exception... e) {
    this.logObjectWithStatusMessage(logLevel, publicationDownload.getClass().getSimpleName(), publicationDownload.getResId(),
            publicationDownload.getStatus().toString(), publicationDownload.getName() == null ? null : publicationDownload.getName(), message,
            e);
  }

  private boolean isArchiveInProgress(PublicationDownload publicationDownload) {
    return (publicationDownload.getStatus() == PublicationDownload.DownloadStatus.DOWNLOADING
            || publicationDownload.getStatus() == PublicationDownload.DownloadStatus.IN_PREPARATION
            || publicationDownload.getStatus() == PublicationDownload.DownloadStatus.IN_PROGRESS
            || publicationDownload.getStatus() == PublicationDownload.DownloadStatus.SUBMITTED
            || publicationDownload.getStatus() == PublicationDownload.DownloadStatus.GENERATING_COVER_PAGE
    );
  }
}
