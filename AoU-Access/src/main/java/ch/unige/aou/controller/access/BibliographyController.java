/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - BibliographyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.access;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.model.Citation;
import ch.unige.solidify.model.dto.CitationDto;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.BibliographyGeneratorService;
import ch.unige.aou.service.citation.AouCitationGenerationService;
import ch.unige.aou.service.query.QueryBuilderService;

@RestController
@EveryonePermissions
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_BIBLIOGRAPHY)
public class BibliographyController {

  private final BibliographyGeneratorService bibliographyService;
  private final QueryBuilderService queryBuilderService;
  private final AouCitationGenerationService aouCitationGenerationService;

  public BibliographyController(BibliographyGeneratorService bibliographyService, QueryBuilderService queryBuilderService,
          AouCitationGenerationService aouCitationGenerationService) {
    this.bibliographyService = bibliographyService;
    this.queryBuilderService = queryBuilderService;
    this.aouCitationGenerationService = aouCitationGenerationService;
  }

  @GetMapping(value = "/" + AouActionName.JAVASCRIPT)
  public HttpEntity<String> getJavascript(
          @RequestParam(name = "searches", required = false) String commaSeparatedSearchIds,
          @RequestParam(name = "publicationIds", required = false) String commaSeparatedPublicationIds,
          @RequestParam(name = "contributor", required = false) String cnIndividu,
          @RequestParam(name = "combinationType", required = false, defaultValue = "or") String combinationType,
          @RequestParam(required = false) Map<String, Object> queryParameters
  ) {

    if (StringTool.isNullOrEmpty(commaSeparatedSearchIds)
            && StringTool.isNullOrEmpty(commaSeparatedPublicationIds)
            && StringTool.isNullOrEmpty(cnIndividu)) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              "At least one search id, one publication id or a contributor cnIndividu is required");
    }

    List<String> searchIds = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(commaSeparatedSearchIds)) {
      searchIds.addAll(Arrays.stream(commaSeparatedSearchIds.split(",")).map(String::trim).toList());
    }

    List<String> publicationIds = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(commaSeparatedPublicationIds)) {
      publicationIds.addAll(Arrays.stream(commaSeparatedPublicationIds.split(",")).map(String::trim).toList());
    }

    String javascript = this.bibliographyService.buildJavascript(searchIds, publicationIds, cnIndividu, queryParameters, combinationType);

    final HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.CONTENT_TYPE, "text/javascript");
    return new ResponseEntity<>(javascript, headers, HttpStatus.OK);
  }

  @GetMapping(value = "/" + AouActionName.JAVASCRIPT_BY_AUTHOR)
  public HttpEntity<String> getJavascriptByAuthor(@RequestParam(name = "id") String cnIndividu,
          @RequestParam(required = false) Map<String, Object> queryParameters) {

    queryParameters.put("cnIndividu", cnIndividu);
    return this.getJavascriptFromLegacyParameters(queryParameters);
  }

  @GetMapping(value = "/" + AouActionName.JAVASCRIPT_BY_STRUCTURE)
  public HttpEntity<String> getJavascriptByStructure(@RequestParam(name = "cn_structc") String cnStructC,
          @RequestParam(required = false) Map<String, Object> queryParameters) {

    queryParameters.put("cnStructC", cnStructC);
    return this.getJavascriptFromLegacyParameters(queryParameters);
  }

  @GetMapping(value = "/" + AouActionName.JAVASCRIPT_BY_RESEARCH_GROUP)
  public HttpEntity<String> getJavascriptByResearchGroup(@RequestParam(name = "id") String resId,
          @RequestParam(required = false) Map<String, Object> queryParameters) {

    queryParameters.put("researchGroupId", resId);
    return this.getJavascriptFromLegacyParameters(queryParameters);
  }

  private HttpEntity<String> getJavascriptFromLegacyParameters(Map<String, Object> queryParameters) {

    // Build search conditions from legacy query parameters
    List<AouSearchCondition> searchConditions = this.queryBuilderService.buildSearchConditionsFromLegacyParameters(queryParameters);

    // Transform legacy query parameters to new ones
    Map<String, Object> parameters = this.bibliographyService.buildParametersFromLegacyParameters(queryParameters);

    String javascript = this.bibliographyService.buildJavascript(searchConditions, parameters);

    final HttpHeaders headers = new HttpHeaders();
    headers.set(HttpHeaders.CONTENT_TYPE, "text/javascript");
    return new ResponseEntity<>(javascript, headers, HttpStatus.OK);
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<List<CitationDto>> get(@PathVariable String id,
          @RequestParam(name = "style", required = false) String style,
          @RequestParam(name = "language", required = false) String language,
          @RequestParam(name = "outputFormat", defaultValue = "HTML") Citation.OutputFormat outputFormat) {

    List<CitationDto> bibliographies = new ArrayList<>();
    CitationDto bibliographyDto = this.aouCitationGenerationService.getBibliographyDto(id, style, language, outputFormat);
    bibliographies.add(bibliographyDto);

    return new ResponseEntity<>(bibliographies, HttpStatus.OK);
  }

}
