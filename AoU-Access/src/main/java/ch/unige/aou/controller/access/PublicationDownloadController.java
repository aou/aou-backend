/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationDownloadController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.access;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.security.TrustedUserPermissions;
import ch.unige.solidify.security.UserPermissions;

import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.model.access.PublicationDownload.DownloadStatus;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@UserPermissions
@RestController
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_PUBLICATION_DOWNLOAD)
public class PublicationDownloadController extends ResourceController<PublicationDownload> {

  @GetMapping("/" + AouActionName.LIST_STATUS)
  public HttpEntity<DownloadStatus[]> listStatus() {
    return new ResponseEntity<>(DownloadStatus.values(), HttpStatus.OK);
  }

  @GetMapping("/{resId}/" + AouActionName.EXISTS)
  public boolean exists(@PathVariable String resId) {
    return this.itemService.existsById(resId);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).listStatus()).withRel(ActionName.VALUES));
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<PublicationDownload> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  @TrustedUserPermissions
  public HttpEntity<PublicationDownload> create(@RequestBody PublicationDownload publicationDownload) {
    return super.create(publicationDownload);
  }
}
