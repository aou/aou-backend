/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - AccessSettingsController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.access;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.security.EveryonePermissions;

import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.bibliography.BibliographyFormat;
import ch.unige.aou.model.search.AdvancedSearchCriteria;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.SettingsService;

@RestController
@EveryonePermissions
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_SETTINGS)
public class AccessSettingsController {

  private final SettingsService settingsService;

  public AccessSettingsController(SettingsService settingsService) {
    this.settingsService = settingsService;
  }

  @GetMapping(value = "/" + AouActionName.LIST_ADVANCED_SEARCH_CRITERIA)
  public HttpEntity<List<AdvancedSearchCriteria>> listAdvancedSearchCriteria() {
    return new HttpEntity<>(this.settingsService.getAdvancedSearchCriteriaList());
  }

  @GetMapping(value = "/" + AouActionName.LIST_BIBLIOGRAPHY_FORMATS)
  public HttpEntity<List<BibliographyFormat>> listBibliographyFormats() {
    return new HttpEntity<>(this.settingsService.getBibliographyFormats());
  }
}
