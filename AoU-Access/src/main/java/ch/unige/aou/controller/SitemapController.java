/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - SitemapController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.SolidifySitemapController;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.service.sitemap.ContributorSitemapService;
import ch.unige.aou.service.sitemap.PublicationSitemapService;

@RestController
@ConditionalOnBean(AccessController.class)
public class SitemapController extends SolidifySitemapController {

  public SitemapController(SolidifyProperties config,
          AouProperties aouProperties,
          PublicationSitemapService publicationSitemapService,
          ContributorSitemapService contributorSitemapService) {
    super(config, aouProperties.getParameters().getHomepage());
    this.getSitemapServiceList().add(publicationSitemapService);
    this.getSitemapServiceList().add(contributorSitemapService);
  }

  @Override
  protected List<String> getExtraUrls() {
    return Collections.emptyList();
  }
}
