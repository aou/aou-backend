/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - SearchController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller.access;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.index.IndexDataReadOnlyController;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.DownloadTokenService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.message.IncrementDownloadStatMessage;
import ch.unige.aou.message.PublicationStatMessage;
import ch.unige.aou.model.access.PublicationDownload;
import ch.unige.aou.model.access.StatisticsInfoDto;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.DocumentFileType;
import ch.unige.aou.model.publication.MetadataFile;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.model.security.DownloadTokenType;
import ch.unige.aou.model.security.User;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.PublicationDownloadService;
import ch.unige.aou.service.PublicationStatisticService;
import ch.unige.aou.service.SearchService;
import ch.unige.aou.service.query.QueryBuilderService;
import ch.unige.aou.service.rest.trusted.TrustedUserRemoteResourceService;

@RestController
@EveryonePermissions
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_METADATA)
public class SearchController extends IndexDataReadOnlyController<String, PublicationIndexEntry> implements ApplicationRoleListService {
  private static final Logger log = LoggerFactory.getLogger(SearchController.class);

  private static final String WITH_RESTRICTED_ACCESS_MASTERS_PARAM = "with-restricted-access-masters";

  private final String indexName;
  private final QueryBuilderService queryBuilderService;
  private final PublicationDownloadService publicationDownloadService;
  private final PublicationStatisticService publicationStatisticService;
  private final TrustedUserRemoteResourceService trustedUserRemoteResourceService;
  private final DownloadTokenService downloadTokenService;
  private final SearchService searchService;

  protected SearchController(AouProperties aouProperties, IndexingService<PublicationIndexEntry> indexResourceService,
          QueryBuilderService queryBuilderService, PublicationDownloadService archiveDownloadService,
          PublicationStatisticService publicationStatisticService,
          TrustedUserRemoteResourceService trustedUserRemoteResourceService,
          DownloadTokenService downloadTokenService,
          SearchService searchService) {
    super(indexResourceService);
    this.queryBuilderService = queryBuilderService;
    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.publicationStatisticService = publicationStatisticService;
    this.publicationDownloadService = archiveDownloadService;
    this.searchService = searchService;
    this.trustedUserRemoteResourceService = trustedUserRemoteResourceService;
    this.downloadTokenService = downloadTokenService;
  }

  @Override
  public HttpEntity<RestCollection<PublicationIndexEntry>> list(@ModelAttribute PublicationIndexEntry search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<PublicationIndexEntry> get(@PathVariable String id) {
    return super.get(id);
  }

  @GetMapping(params = "resId")
  public HttpEntity<PublicationIndexEntry> getByResId(@RequestParam(name = "resId") String resId) {
    return super.get(resId);
  }

  @GetMapping(params = "archiveId")
  public HttpEntity<PublicationIndexEntry> getByArchiveId(@RequestParam(name = "archiveId") String archiveId) {

    // Get fields to include/exclude from ES response
    FieldsRequest fieldsRequest = this.queryBuilderService.getDefaultFieldsRequest();

    AouSearchCondition archiveIdCondition = this.queryBuilderService.getFindByArchiveIdCondition(archiveId);
    FacetPage<PublicationIndexEntry> results = this.indexResourceService.search(this.getIndex(), List.of(archiveIdCondition), null,
            Pageable.ofSize(10), fieldsRequest);

    if (results.getContent().size() == 1) {
      PublicationIndexEntry t = results.stream().findFirst().orElseThrow();
      // Do not count ADMIN, ROOT and TRUSTED CLIENTS user for statistics
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      if (authentication == null || !this.isRootOrTrustedOrAdminRole()) {
        this.publicationStatisticService.createViewStats(t);
        SolidifyEventPublisher.getPublisher().publishEvent(new PublicationStatMessage(t, false, true));
      }
      this.addLinks(t);
      return new ResponseEntity<>(t, HttpStatus.OK);
    } else if (!results.hasContent()) {
      log.info("Publication with archiveId: {} was not found", archiveId);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      throw new SolidifyRuntimeException("Too many publications found");
    }
  }

  @PostMapping(value = "/" + ActionName.SEARCH)
  public HttpEntity<RestCollection<PublicationIndexEntry>> searchPost(@RequestBody List<AouSearchCondition> searchConditions,
          @RequestParam(value = WITH_RESTRICTED_ACCESS_MASTERS_PARAM, defaultValue = "false") boolean withRestrictedAccessMaster,
          @RequestParam(value = "facets", required = false) String facets,
          Pageable pageable) {

    // Replaces field aliases by field names, set AND operator on specific fields
    List<SearchCondition> searchConditionList = this.queryBuilderService.completeSearchConditions(searchConditions);
    if (!withRestrictedAccessMaster) {
      // discard masters that are not public
      searchConditionList = this.queryBuilderService.addDiscardNonPublicMastersCondition(searchConditionList);
    }

    // Get fields to include/exclude from ES response
    FieldsRequest fieldsRequest = this.queryBuilderService.getDefaultFieldsRequest();

    // Get facets
    List<FacetRequest> facetRequests;
    if (!StringTool.isNullOrEmpty(facets)) {
      facetRequests = this.queryBuilderService.getFacetsList(facets);
    } else {
      facetRequests = this.queryBuilderService.getFacetsList();
    }

    FacetPage<PublicationIndexEntry> results = this.indexResourceService.search(this.getIndex(), searchConditionList, facetRequests, pageable,
            fieldsRequest);
    return this.page2Collection(results, pageable);
  }

  @GetMapping(value = "/" + AouActionName.RUN_STORED_SEARCH + "/{storedSearchId}")
  public HttpEntity<RestCollection<PublicationIndexEntry>> runStoredSearch(@PathVariable String storedSearchId,
          @RequestParam(value = WITH_RESTRICTED_ACCESS_MASTERS_PARAM, defaultValue = "false") boolean withRestrictedAccessMaster,
          Pageable pageable) {

    List<AouSearchCondition> searchConditions = this.queryBuilderService.getStoredSearchConditions(storedSearchId);

    return this.searchPost(searchConditions, withRestrictedAccessMaster, null, pageable);
  }

  @GetMapping(value = "/" + AouActionName.COMBINE_STORED_SEARCHES)
  public HttpEntity<RestCollection<PublicationIndexEntry>> combineStoredResearches(
          @RequestParam(name = "searches", required = false) String commaSeparatedSearchIds,
          @RequestParam(name = "publicationIds", required = false) String commaSeparatedPublicationIds,
          @RequestParam(name = "contributor", required = false) String cnIndividu,
          @RequestParam(name = "combinationType", required = false, defaultValue = "or") String combinationType,
          @RequestParam(value = WITH_RESTRICTED_ACCESS_MASTERS_PARAM, defaultValue = "false") boolean withRestrictedAccessMaster,
          Pageable pageable) {

    if (StringTool.isNullOrEmpty(commaSeparatedSearchIds)
            && StringTool.isNullOrEmpty(commaSeparatedPublicationIds)
            && StringTool.isNullOrEmpty(cnIndividu)) {
      throw new SolidifyHttpErrorException(HttpStatus.BAD_REQUEST,
              "At least one search id, one publication id or a contributor cnIndividu is required");
    }

    List<String> searchIds = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(commaSeparatedSearchIds)) {
      searchIds.addAll(Arrays.stream(commaSeparatedSearchIds.split(",")).map(String::trim).toList());
    }

    List<String> publicationIds = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(commaSeparatedPublicationIds)) {
      publicationIds.addAll(Arrays.stream(commaSeparatedPublicationIds.split(",")).map(String::trim).toList());
    }

    AouSearchCondition searchCondition = this.queryBuilderService.getSearchesOrPublicationsSearchCondition(searchIds, publicationIds,
            combinationType, cnIndividu);

    return this.searchPost(List.of(searchCondition), withRestrictedAccessMaster, null, pageable);
  }

  @GetMapping("/{id}/" + AouActionName.DOWNLOAD_STATUS)
  public HttpEntity<String> getDownloadStatus(@PathVariable String id) {
    final PublicationDownload publicationDownload = this.publicationDownloadService.findOne(id);
    if (publicationDownload == null) {
      throw new NoSuchElementException("Archive Download not found");
    }
    return new ResponseEntity<>(publicationDownload.getStatus().toString(), HttpStatus.OK);
  }

  @PostMapping("/{id}/" + AouActionName.PREPARE_DOWNLOAD)
  @PreAuthorize("@publicationDownloadPermissionService.isAllowed(#id, 'PREPARE_DOWNLOAD_ARCHIVE')")
  public HttpEntity<String> prepareDownload(@PathVariable String id) {
    try {
      this.publicationDownloadService.findOne(id);
      /*
       * Archive already downloaded
       */
      return new ResponseEntity<>("Archive Download already exists", HttpStatus.BAD_REQUEST);
    } catch (NoSuchElementException e) {
      this.publicationDownloadService.createAndSubmitPublicationDownload(id);
      return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
  }

  @GetMapping("/{documentFileId}/" + ActionName.DOWNLOAD)
  @PreAuthorize("@publicationDownloadPermissionService.isAllowed(#documentFileId, 'DOWNLOAD_FILE') "
          + "|| @downloadTokenPermissionService.isAllowed(#documentFileId, T(ch.unige.aou.model.security.DownloadTokenType).DOCUMENT)")
  public HttpEntity<StreamingResponseBody> download(@PathVariable String documentFileId) throws IOException {

    if (!this.publicationDownloadService.exists(documentFileId)) {
      this.publicationDownloadService.prepareDownload(documentFileId);
    }
    PublicationDownload publicationDownload = this.publicationDownloadService.waitForPublicationDownloadReady(documentFileId);

    // get publication metadata from index
    PublicationIndexEntry indexEntry = this.indexResourceService.findOne(this.getIndex(), publicationDownload.getPublicationId(),
            this.queryBuilderService.getDefaultFieldsRequest());

    // Increment number of downloads (only if file has a PRINCIPAL type level and is not an Imprimatur)
    // Do not count ADMIN, ROOT and TRUSTED CLIENTS user for statistics
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null || (!this.checkUserInDownloadTokenIsAdminOrRootOrTrusted(documentFileId) && !this.isRootOrTrustedOrAdminRole())) {
      List<MetadataFile> metadataFiles = indexEntry.getFiles();
      Optional<MetadataFile> downloadedFileOpt = metadataFiles.stream().filter(f -> f.getResId().equals(documentFileId)).findFirst();
      if (downloadedFileOpt.isPresent()) {
        MetadataFile downloadedFile = downloadedFileOpt.get();
        if (!StringTool.isNullOrEmpty(downloadedFile.getTypeLevel())
                && !StringTool.isNullOrEmpty(downloadedFile.getType())
                && downloadedFile.getTypeLevel().equals(DocumentFileType.FileTypeLevel.PRINCIPAL.name())
                && !downloadedFile.getType().equals(AouConstants.DOCUMENT_FILE_TYPE_IMPRIMATUR_VALUE)) {
          // Use an event to make this call to database async, thus preventing the endpoint itself to use database a connection
          SolidifyEventPublisher.getPublisher().publishEvent(new IncrementDownloadStatMessage(documentFileId, publicationDownload));
          SolidifyEventPublisher.getPublisher().publishEvent(new PublicationStatMessage(indexEntry, true, false));
        }
      }
    }

    final Path filePath = this.publicationDownloadService.getFileToDownloadPath(publicationDownload);

    String contentTypeFile = publicationDownload.getContentType() == null ? FileTool.getContentType(filePath).toString()
            : publicationDownload.getContentType();

    String fileName = filePath.getFileName().toString();

    // If the filename isn't ISO_8859_1 compatible, rename it "file.xxx"
    CharsetEncoder iso8859Encoder = StandardCharsets.ISO_8859_1.newEncoder();
    if (!iso8859Encoder.canEncode(fileName)) {
      String dotExtension = "";
      if (fileName.contains(".")) {
        dotExtension = fileName.substring(fileName.lastIndexOf("."));
      }
      fileName = AouConstants.FILE_NAME_REPLACEMENT + dotExtension;
    }

    long fileSize = FileTool.getSize(filePath);
    log.info("File '{}' ({}) for publication '{}' will be downloaded", fileName, StringTool.formatSmartSize(fileSize),
            publicationDownload.getPublicationId());

    if (fileName.startsWith(AouConstants.FILE_WITH_COVER_PAGE_PREFIX)) {
      fileName = fileName.substring(AouConstants.FILE_WITH_COVER_PAGE_PREFIX.length());
    }

    InputStream fileStream = filePath.toUri().toURL().openStream();
    return this.buildDownloadResponseEntity(fileStream, fileName, contentTypeFile, fileSize);
  }

  @GetMapping("/{publicationId}/" + AouActionName.STATISTICS)
  public HttpEntity<StatisticsInfoDto> statistics(@PathVariable String publicationId) {
    return new ResponseEntity<>(this.publicationStatisticService.getPublicationStatistics(publicationId), HttpStatus.OK);
  }

  @GetMapping("/{id}/" + AouActionName.THUMBNAIL)
  public HttpEntity<FileSystemResource> downloadThumbnail(@PathVariable String id) throws IOException {
    FileSystemResource thumbnailFSR = this.searchService.downloadThumbnail(id);
    String contentTypeFile = FileTool.getContentType(thumbnailFSR.getURI()).toString();
    return this.buildFileSystemResource(thumbnailFSR, contentTypeFile, 0);
  }

  protected ResponseEntity<FileSystemResource> buildFileSystemResource(FileSystemResource inputStream, String fileContentType,
          long fileSize) {
    HttpHeaders respHeaders = new HttpHeaders();
    respHeaders.setContentType(MediaType.valueOf(fileContentType));
    if (fileSize > 0) {
      respHeaders.setContentLength(fileSize);
    }
    return new ResponseEntity<>(inputStream, respHeaders, HttpStatus.OK);
  }

  private boolean checkUserInDownloadTokenIsAdminOrRootOrTrusted(String documentFileId) {
    String tokenHash = this.downloadTokenService.getTokenHashFromRequest(documentFileId, DownloadTokenType.DOCUMENT);
    if (!StringTool.isNullOrEmpty(tokenHash)) {
      DownloadToken downloadToken = this.downloadTokenService.findByTokenHash(tokenHash);
      if (downloadToken != null) {
        try {
          User user = this.trustedUserRemoteResourceService.findByExternalUid(downloadToken.getUserId());
          return user != null && (user.getApplicationRole().getResId().equals(AuthApplicationRole.ADMIN_ID) || user.getApplicationRole()
                  .getResId()
                  .equals(AuthApplicationRole.ROOT_ID)
                  || user.getApplicationRole().getResId().equals(AuthApplicationRole.TRUSTED_CLIENT_ID));
        } catch (SolidifyRestException e) {
          // If the user does not exist, an exception will be thrown meaning the user has no role ADMIN, ROOT or TRUSTED
        }
      }
    }
    return false;
  }

  @Override
  protected String getIndex() {
    return this.indexName;
  }
}
