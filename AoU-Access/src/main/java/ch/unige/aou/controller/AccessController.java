/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - AccessController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import java.io.IOException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.OAIMetadataPrefixService;
import ch.unige.solidify.business.OAISetService;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAISet;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.RootPermissions;

import ch.unige.aou.AouConstants;
import ch.unige.aou.AouMetadataVersion;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.CacheService;

@RestController
@ConditionalOnProperty(prefix = "aou.module.access", name = "enable")
@RequestMapping(UrlPath.ACCESS)
public class AccessController extends ModuleController {

  private final String unigeDoiPrefix;

  private final CacheService cacheService;

  AccessController(AouProperties aouProperties, OAIMetadataPrefixService oaiMetadataPrefixService, OAISetService oaiSetService,
          CacheService cacheService) {
    super(ModuleName.ACCESS);

    this.unigeDoiPrefix = aouProperties.getMetadata().getUnigeDoiPrefix();
    this.cacheService = cacheService;

    if (aouProperties.getData().isInit()) {
      try {
        this.initDefaultMetadataPrefixes(oaiMetadataPrefixService);
        this.initDefaultOAISets(oaiSetService);
      } catch (IOException e) {
        throw new SolidifyRuntimeException(e.getMessage(), e);
      }
    }
  }

  @RootPermissions
  @PostMapping("/" + AouActionName.CLEAN_CACHES)
  public HttpEntity<Result> cleanAllCaches() {
    this.cacheService.cleanAllCaches();
    final Result result = new Result();
    result.setStatus(Result.ActionStatus.EXECUTED);
    result.setMesssage("All caches were cleaned");
    return new ResponseEntity<>(result, HttpStatus.OK);
  }

  private void initDefaultMetadataPrefixes(OAIMetadataPrefixService oaiMetadataPrefixService) throws IOException {
    // Init OAI Metadata Prefix
    oaiMetadataPrefixService.initDefaultData();
    // Update OAI DC
    oaiMetadataPrefixService.updateOaiDc("aou2oai_dc.xsl");

    // OAI Metadata Prefix: AoU
    oaiMetadataPrefixService.createIfNotExists(
            AouConstants.OAI_AOU,
            true,
            AouConstants.OAI_AOU_NAME,
            AouConstants.OAI_AOU_NAME + " " + AouMetadataVersion.getDefaultVersion().getVersion(),
            AouMetadataVersion.getDefaultVersion().getRepresentationInfoSchema(),
            null,
            AouConstants.OAI_AOU_SCHEMA_2_4,
            AouConstants.OAI_AOU_NAMESPACE_2_4,
            null);

    // OAI Metadata Prefix: MARCXML
    oaiMetadataPrefixService.createIfNotExists(
            AouConstants.OAI_MARC21,
            false,
            AouConstants.OAI_MARC21_NAME,
            AouConstants.OAI_MARC21_NAME,
            AouConstants.OAI_MARC21_SCHEMA,
            "aou2oai_marcxml.xsl",
            AouConstants.OAI_MARC21_SCHEMA_URL,
            AouConstants.OAI_MARC21_NAMESPACE,
            "ch.unige.aou.model.xml.marc21.MarcCollection");

    // OAI Metadata Prefix: EPICUR
    oaiMetadataPrefixService.createIfNotExists(
            AouConstants.OAI_EPICUR,
            false,
            AouConstants.OAI_EPICUR_NAME,
            AouConstants.OAI_EPICUR_NAME,
            AouConstants.OAI_EPICUR_SCHEMA,
            "aou2oai_xepicur.xsl",
            AouConstants.OAI_EPICUR_SCHEMA_URL,
            AouConstants.OAI_EPICUR_NAMESPACE,
            "ch.unige.aou.model.xml.xepicur.Epicur");

    // OAI Metadata Prefix: OpenAIRE
    oaiMetadataPrefixService.createIfNotExists(
            AouConstants.OAI_OPENAIRE,
            false,
            AouConstants.OAI_OPENAIRE_NAME,
            "Metadata formatted according to guidelines 4.0",
            AouConstants.OAI_OPENAIRE_SCHEMA,
            "aou2oai_openaire.xsl",
            AouConstants.OAI_OPENAIRE_SCHEMA_URL,
            AouConstants.OAI_OPENAIRE_NAMESPACE,
            "ch.unige.solidify.model.xml.openaire.v40.Resource");

  }

  private void initDefaultOAISets(OAISetService oaiSetService) throws IOException {
    // UNIGE DOI
    oaiSetService.initDefaultData();
    OAISet oaiSet = oaiSetService.findBySpec("doi-registration");
    String escapedDoi = this.unigeDoiPrefix.replace("/", "\\/");
    oaiSet.setQuery("doi:" + escapedDoi + "*");
    oaiSetService.save(oaiSet);

    // All publications
    oaiSetService.createIfNotExists("full-archive-ouverte", "All publications", "All publications", "*:*");

    // All publications with fulltext
    oaiSetService.createIfNotExists("archive-ouverte", "Publications with fulltext", "All publications with fulltext",
            "!fulltextVersions:noFulltext");

    // All publications with open access fulltext
    oaiSetService.createIfNotExists("openaccess", "Publications with fulltext in openaccess", "All publications with fulltext in openaccess",
            "!fulltextVersions:noFulltext AND " + AouConstants.INDEX_FIELD_DIFFUSION + ":" + AouConstants.OPEN_ACCESS_TEXT);
  }
}
