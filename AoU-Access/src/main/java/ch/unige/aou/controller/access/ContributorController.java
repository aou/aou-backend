/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - ContributorController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.controller.access;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.IndexResourceService;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.ByteResponseDto;
import ch.unige.aou.model.PublicContributorDTO;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.PublicationStatistic;
import ch.unige.aou.model.rest.AouSearchCondition;
import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;
import ch.unige.aou.service.ContributorInfoService;
import ch.unige.aou.service.PublicationStatisticService;
import ch.unige.aou.service.query.QueryBuilderService;

@RestController
@EveryonePermissions
@ConditionalOnBean(AccessController.class)
@RequestMapping(UrlPath.ACCESS_CONTRIBUTOR)
public class ContributorController extends SolidifyController {

  private final ContributorInfoService contributorInfoService;
  private final QueryBuilderService queryBuilderService;
  private final IndexResourceService<String, PublicationIndexEntry> indexResourceService;
  private final PublicationStatisticService publicationStatisticService;
  private final String indexName;

  public ContributorController(AouProperties aouProperties, ContributorInfoService contributorInfoService,
          QueryBuilderService queryBuilderService,
          IndexResourceService<String, PublicationIndexEntry> indexResourceService, PublicationStatisticService publicationStatisticService) {
    this.contributorInfoService = contributorInfoService;
    this.queryBuilderService = queryBuilderService;
    this.indexResourceService = indexResourceService;
    this.publicationStatisticService = publicationStatisticService;
    this.indexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
  }

  @GetMapping("/{cnIndividu}")
  public HttpEntity<PublicContributorDTO> get(@PathVariable String cnIndividu) {
    PublicContributorDTO publicContributorDTO = this.contributorInfoService.getPublicInfos(cnIndividu);
    if (publicContributorDTO == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(publicContributorDTO, HttpStatus.OK);
  }

  @GetMapping("/{cnIndividu}/" + AouActionName.DOWNLOAD_AVATAR)
  @ResponseBody
  public ResponseEntity<StreamingResponseBody> downloadAvatar(@PathVariable String cnIndividu) throws URISyntaxException {
    ByteResponseDto byteResponseDto = this.contributorInfoService.getAvatar(cnIndividu);
    return this.buildDownloadResponseEntity(byteResponseDto);
  }

  private ResponseEntity<StreamingResponseBody> buildDownloadResponseEntity(ByteResponseDto byteResponseDto) {
    if (byteResponseDto == null || byteResponseDto.getContent() == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    HttpHeaders respHeaders = new HttpHeaders();
    respHeaders.setContentType(MediaType.valueOf(byteResponseDto.getContentType()));
    InputStream inputStream = new ByteArrayInputStream(byteResponseDto.getContent());
    StreamingResponseBody responseBody = outputStream -> {
      byte[] data = new byte[SolidifyConstants.BUFFER_SIZE];
      int numberOfBytesToWrite;
      while ((numberOfBytesToWrite = inputStream.read(data, 0, data.length)) != -1) {
        outputStream.write(data, 0, numberOfBytesToWrite);
      }
      inputStream.close();
    };
    return new ResponseEntity<>(responseBody, respHeaders, HttpStatus.OK);
  }

  @GetMapping("/{cnIndividu}/" + AouActionName.STATISTICS)
  public HttpEntity<RestCollection<PublicationStatistic>> getPublicationsWithStatistics(@PathVariable String cnIndividu, Pageable pageable) {
    // Get fields to include/exclude from ES response
    FieldsRequest fieldsRequest = this.queryBuilderService.getDefaultFieldsRequest();

    AouSearchCondition contributorButNotDirectorSearchCondition = this.queryBuilderService.getFindByCnIndividuWhereContributorButNotDirectorCondition(
            cnIndividu);

    Pageable page = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);

    FacetPage<PublicationIndexEntry> indexResults = this.indexResourceService.search(this.indexName,
            List.of(contributorButNotDirectorSearchCondition), null, page, fieldsRequest);

    if (!indexResults.getContent().isEmpty()) {
      // First check if there is an entry in the database for each publication
      List<String> archivesIdList = indexResults.getContent().stream().map(PublicationIndexEntry::getArchiveId).toList();
      Page<PublicationStatistic> publicationStatisticPage = this.publicationStatisticService.findByArchivesIds(archivesIdList, page);
      indexResults.getContent().forEach(indexEntry -> {
        // Verify if there is an entry in the database for this archive
        if (publicationStatisticPage.stream().noneMatch(p -> p.getArchiveId().equals(indexEntry.getArchiveId()))) {
          this.publicationStatisticService.createOrUpdatePublicationStatistics(indexEntry, false, false);
        }
        // Verify if there is a need to update the title or access level
        else if (publicationStatisticPage.stream().filter(p -> p.getArchiveId().equals(indexEntry.getArchiveId()))
                .noneMatch(p -> p.getTitle().equals(indexEntry.getTitle())
                        && p.getAccessLevel().equals(indexEntry.getPrincipalFileAccessLevel())
                        && p.getOpenAccess().equals(indexEntry.getOpenAccess()))) {
          this.publicationStatisticService.createOrUpdatePublicationStatistics(indexEntry, false, false);
        }
      });

      // Once the publicationStatistics table is up to date, get the results.
      Page<PublicationStatistic> publicationStatisticsTotal = this.publicationStatisticService.findByArchivesIds(archivesIdList, pageable);
      final RestCollection<PublicationStatistic> collection = new RestCollection<>(publicationStatisticsTotal, pageable);
      return new ResponseEntity<>(collection, HttpStatus.OK);
    }
    return new ResponseEntity<>(new RestCollection<>(), HttpStatus.OK);
  }

  @GetMapping(AouActionName.GET_UNIGE_CONTRIBUTOR_BY_ORCID + "/{orcid}")
  public HttpEntity<PublicContributorDTO> getUnigeContributorByOrcid(@PathVariable String orcid) {
    PublicContributorDTO publicContributorDTO = this.contributorInfoService.getUnigeContributorPublicInfosByOrcid(orcid);
    if (publicContributorDTO == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(publicContributorDTO, HttpStatus.OK);
  }
}
