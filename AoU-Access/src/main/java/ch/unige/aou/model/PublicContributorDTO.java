/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicContributorDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.aou.model;

import java.util.ArrayList;
import java.util.List;

import ch.unige.aou.model.settings.ResearchGroup;
import ch.unige.aou.model.settings.Structure;

public class PublicContributorDTO {
  private String cnIndividu;
  private String firstName;
  private String lastName;
  private String orcid;
  private Boolean hasAvatar;
  private Boolean hasEmail;
  List<Structure> structures = new ArrayList<>();
  List<ResearchGroup> researchGroups = new ArrayList<>();

  public String getCnIndividu() {
    return this.cnIndividu;
  }

  public void setCnIndividu(String cnIndividu) {
    this.cnIndividu = cnIndividu;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getOrcid() {
    return this.orcid;
  }

  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public List<Structure> getStructures() {
    return this.structures;
  }

  public void setStructures(List<Structure> structures) {
    this.structures = structures;
  }

  public List<ResearchGroup> getResearchGroups() {
    return this.researchGroups;
  }

  public void setResearchGroups(List<ResearchGroup> researchGroups) {
    this.researchGroups = researchGroups;
  }

  public void setHasAvatar(boolean hasAvatar) {
    this.hasAvatar = hasAvatar;
  }

  public boolean getHasAvatar() {
    return this.hasAvatar;
  }

  public void setHasEmail(boolean hasEmail) {
    this.hasEmail = hasEmail;
  }

  public boolean getHasEmail() {
    return this.hasEmail;
  }
}
