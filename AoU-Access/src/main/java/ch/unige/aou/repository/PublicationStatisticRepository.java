/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - PublicationStatisticRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.publication.PublicationStatistic;

@Repository
@ConditionalOnBean(AccessController.class)
public interface PublicationStatisticRepository extends SolidifyRepository<PublicationStatistic> {

  PublicationStatistic findByArchiveId(String archiveId);

  @Query("SELECT p FROM PublicationStatistic p WHERE p.archiveId IN :archivesIds")
  Page<PublicationStatistic> findByArchivesIds(List<String> archivesIds, Pageable pageable);

}
