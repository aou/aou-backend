/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU Access - DownloadRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.repository.SolidifyRepository;

import ch.unige.aou.controller.AccessController;
import ch.unige.aou.model.publication.Download;

@Repository
@SuppressWarnings("squid:S1214")
@ConditionalOnBean(AccessController.class)
public interface DownloadRepository extends SolidifyRepository<Download> {

  @Query("SELECT d FROM Download d WHERE d.documentFileId = :documentId and Year(d.creation.when) = :year and Month(d.creation.when) = :month")
  Optional<Download> findByDocumentFileAndYearAndMonth(String documentId, int year, int month);

  List<Download> findByPublicationId(String publicationId);

  @Query("SELECT IFNULL(SUM(d.downloadNumber), 0) from Download d where d.publicationId = :publicationId")
  int countDownloadByPublicationId(String publicationId);
}
