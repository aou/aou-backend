/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU DataMgmt - IndexPropertiesController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.model.specification.IndexFieldAliasSpecification;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;

import ch.unige.aou.rest.AouActionName;
import ch.unige.aou.rest.UrlPath;

@RestController
@ConditionalOnProperty(prefix = "aou.module.data-mgmt", name = "enable")
@RequestMapping(UrlPath.DATA_MGMT_INDEX_PROPERTIES)
public class IndexPropertiesController {

  private final IndexFieldAliasService indexFieldAliasService;

  public IndexPropertiesController(IndexFieldAliasService indexFieldAliasService) {
    this.indexFieldAliasService = indexFieldAliasService;
  }

  @GetMapping({ "/" + AouActionName.LIST_FACET_REQUESTS + "/{indexName}" })
  public ResponseEntity<RestCollection<FacetRequest>> facetsList(@PathVariable String indexName) {
    List<FacetRequest> facetsList = this.indexFieldAliasService.getFacetRequestsListForIndex(indexName);
    final RestCollection<FacetRequest> collection = new RestCollection<>(facetsList);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  @GetMapping({ "/" + AouActionName.LIST_INDEX_FIELD_ALIASES + "/{indexName}" })
  public ResponseEntity<RestCollection<IndexFieldAlias>> getIndexFieldAliases(@PathVariable String indexName) {

    IndexFieldAlias filter = new IndexFieldAlias();
    filter.setIndexName(indexName);
    IndexFieldAliasSpecification indexSpecification = new IndexFieldAliasSpecification(filter);

    Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(IndexConstants.FACET_ORDER_FIELD));
    Page<IndexFieldAlias> indexFieldAliasesPage = this.indexFieldAliasService.findAll(indexSpecification, pageable);

    final RestCollection<IndexFieldAlias> collection = new RestCollection<>(indexFieldAliasesPage, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }
}
