/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU DataMgmt - DataMgmtController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.controller;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.business.IndexFieldAliasService;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.model.index.IndexSettings;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.rest.ModuleName;
import ch.unige.aou.rest.UrlPath;

@RestController
@ConditionalOnProperty(prefix = "aou.module.data-mgmt", name = "enable")
@RequestMapping(UrlPath.DATA_MGMT)
public class DataMgmtController extends ModuleController {

  private final IndexFieldAliasService indexFieldAliasService;

  protected DataMgmtController(AouProperties aouProperties, IndexingSettingsService<IndexSettings> indexSettingsService,
          IndexFieldAliasService indexFieldAliasService) {
    super(ModuleName.DATA_MGMT);

    this.indexFieldAliasService = indexFieldAliasService;

    // Module initialization
    if (aouProperties.getData().isInitIndexes()) {
      // Index creation
      indexSettingsService.init(aouProperties.getIndexing().getIndexDefinitionList());
    }

    if (aouProperties.getData().isInit()) {
      this.initIndexFieldAliases(aouProperties.getIndexing().getFieldAliases());
    }
  }

  private void initIndexFieldAliases(List<IndexFieldAlias> indexFieldAliases) {
    int facetPosition = 10;
    for (IndexFieldAlias indexFieldAlias : indexFieldAliases) {
      boolean isSystem = indexFieldAlias.isSystem() != null ? indexFieldAlias.isSystem() : false;
      boolean isFacet = indexFieldAlias.isFacet() != null ? indexFieldAlias.isFacet() : true;
      this.indexFieldAliasService.createIfNotExists(indexFieldAlias.getIndexName(), facetPosition, indexFieldAlias.getAlias(),
              indexFieldAlias.getField(), isFacet, isSystem, indexFieldAlias.getFacetMinCount(), indexFieldAlias.getFacetLimit());
      facetPosition += 10;
    }
  }
}
