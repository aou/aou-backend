/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU DataMgmt - IndexingProcessingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;

import ch.unige.aou.AouConstants;
import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.DataMgmtController;
import ch.unige.aou.model.exception.PublicationNotFoundException;
import ch.unige.aou.model.index.PublicationIndexEntry;
import ch.unige.aou.model.publication.Publication;
import ch.unige.aou.service.rest.trusted.TrustedPublicationRemoteResourceService;

@Service
@ConditionalOnBean(DataMgmtController.class)
public class IndexingProcessingService extends AouService {

  private static final Logger log = LoggerFactory.getLogger(IndexingProcessingService.class);

  private final String publishedPublicationsIndexName;
  private final String inProgressPublicationsIndexName;
  private final int maxNumberOfSecondsForPublicationIndexedMessage;

  private final CommonMetadataService commonMetadataService;
  private final TrustedPublicationRemoteResourceService remotePublicationService;

  private final IndexingService<PublicationIndexEntry> publicationIndexingService;
  private final PublicationIndexedService publicationIndexedService;
  private final FieldsRequest defaultFieldsRequestWithXml;

  public IndexingProcessingService(AouProperties aouProperties, MessageService messageService, CommonMetadataService commonMetadataService,
          TrustedPublicationRemoteResourceService remotePublicationService, IndexingService<PublicationIndexEntry> publicationIndexingService,
          PublicationIndexedService publicationIndexedService) {
    super(messageService);
    this.remotePublicationService = remotePublicationService;
    this.publicationIndexingService = publicationIndexingService;
    this.publicationIndexedService = publicationIndexedService;
    this.commonMetadataService = commonMetadataService;

    this.publishedPublicationsIndexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.inProgressPublicationsIndexName = aouProperties.getIndexing().getInProgressPublicationsIndexName();
    this.maxNumberOfSecondsForPublicationIndexedMessage = aouProperties.getIndexing().getMaxNumberOfSecondsForPublicationIndexedMessage();
    this.defaultFieldsRequestWithXml = aouProperties.getIndexing().getDefaultFieldsRequestWithXml();
  }

  @Transactional
  public void indexPublication(String publicationId) {
    Publication publication = null;
    PublicationIndexEntry publicationIndexEntry = null;
    boolean isDeleted = false;
    try {
      publication = this.remotePublicationService.findOne(publicationId);
      publicationIndexEntry = this.publicationIndexingService.findOne(this.publishedPublicationsIndexName, publicationId,
              this.defaultFieldsRequestWithXml);
      if (publication.getStatus() == Publication.PublicationStatus.DELETED) {
        // delete from the public index publications that are soft deleted
        this.deleteFromIndexIfExists(this.publishedPublicationsIndexName, publicationId);
        isDeleted = true;
      } else if (publication.getStatus() == Publication.PublicationStatus.IN_EDITION
              || publication.getStatus() == Publication.PublicationStatus.UPDATES_VALIDATION
              || publication.getStatus() == Publication.PublicationStatus.CANCEL_EDITION) {
        // publication is in edition --> reindex updated metadata in in_progress index and metadata backup in public index
        PublicationIndexEntry updatedIndexEntry = this.commonMetadataService.getPublicationIndexEntry(publication, publication.getMetadata());
        this.storeInIndex(this.inProgressPublicationsIndexName, updatedIndexEntry, publication);

        if (!StringTool.isNullOrEmpty(publication.getMetadataBackup())) {
          PublicationIndexEntry backIndexEntry = this.commonMetadataService.getPublicationIndexEntry(publication,
                  publication.getMetadataBackup());
          this.storeInIndex(this.publishedPublicationsIndexName, backIndexEntry, publication);
        } else {
          log.warn("Unable to reindex backup metadata for object {}: metadata backup is empty", publicationId);
        }

      } else {
        // publication is not in edition
        publicationIndexEntry = this.commonMetadataService.getPublicationIndexEntry(publication, publication.getMetadata());

        // always store in index used on admin
        this.storeInIndex(this.inProgressPublicationsIndexName, publicationIndexEntry, publication);

        // if COMPLETED or CANONICAL, store in index used on public interface as well
        if (publication.getStatus() == Publication.PublicationStatus.COMPLETED
                || publication.getStatus() == Publication.PublicationStatus.CANONICAL) {
          this.storeInIndex(this.publishedPublicationsIndexName, publicationIndexEntry, publication);
        }
      }

    } catch (PublicationNotFoundException e) {
      // publication has been deleted
      this.deleteFromIndexIfExists(this.inProgressPublicationsIndexName, publicationId);
      this.deleteFromIndexIfExists(this.publishedPublicationsIndexName, publicationId);
      isDeleted = true;
      log.info("Object {} not found on {} -> removed from index", publicationId, e.getMessage());
    } catch (Exception e) {
      log.error("Unable to index object {}", publicationId, e);
    } finally {
      if (publication != null
              && (publication.getStatus() == Publication.PublicationStatus.COMPLETED
              || publication.getStatus() == Publication.PublicationStatus.DELETED)
              && !StringTool.isNullOrEmpty(publication.getArchiveId())) {
        List<String> listCnIndividus = new ArrayList<>();
        if (publicationIndexEntry != null) {
          listCnIndividus = publicationIndexEntry.getContributorsCnIndividus();
        }
        // Send a message only for publications that already have an archiveId, as message is used to clear public pages
        this.publicationIndexedService.sendPublicationIndexedMessage(publicationId, publication.getArchiveId(), listCnIndividus, isDeleted);
      }
    }
  }

  public void cleanIndexes() {
    this.cleanIndex(this.publishedPublicationsIndexName);
    this.cleanIndex(this.inProgressPublicationsIndexName);
  }

  /**
   * For each entry in index, check if the publication still exists on admin. If not, remove it from index.
   */
  private void cleanIndex(String indexName) {
    log.info("Cleaning of index '{}' will start", indexName);
    List<PublicationIndexEntry> entriesToDelete = new ArrayList<>();
    int totalChecked = 0;
    Pageable pageable = PageRequest.of(0, 50, Sort.by(AouConstants.INDEX_FIELD_ARCHIVE_ID_INT));
    FacetPage<PublicationIndexEntry> page;
    FieldsRequest fieldsRequest = new FieldsRequest();
    fieldsRequest.getIncludes().add(AouConstants.INDEX_FIELD_ARCHIVE_ID);
    fieldsRequest.getIncludes().add(AouConstants.INDEX_FIELD_CONTRIBUTORS);
    do {
      page = this.publicationIndexingService.search(indexName, new ArrayList<>(), null, pageable, fieldsRequest);
      for (PublicationIndexEntry indexEntry : page.getContent()) {
        try {
          // Try to get publication from admin
          log.debug("Check if publication '{}' still exists on admin module", indexEntry.getResId());
          totalChecked++;
          this.remotePublicationService.findOne(indexEntry.getResId());
        } catch (SolidifyResourceNotFoundException e) {
          // If publication is not found on admin, it must be removed from index
          entriesToDelete.add(indexEntry);
          log.debug("Publication '{}' must be removed from index '{}' as it does not exist on admin module", indexEntry.getResId(), indexName);
        }
      }
      pageable = page.nextPageable();
    } while (page.hasNext());

    int totalDeleted = 0;
    for (PublicationIndexEntry indexEntry : entriesToDelete) {
      this.publicationIndexingService.delete(indexName, indexEntry);
      totalDeleted++;
      log.info("Publication '{}' removed from index '{}'", indexEntry.getResId(), indexName);
      if (!StringTool.isNullOrEmpty(indexEntry.getArchiveId())) {
        // Send a message only for publications that already have an archiveId, as message is used to clear public pages
        this.publicationIndexedService.sendPublicationIndexedMessage(indexEntry.getResId(), indexEntry.getArchiveId(),
                indexEntry.getContributorsCnIndividus(), true);
      }
    }

    log.info("Cleaning of index '{}' is over: {} checked, {} deleted", indexName, totalChecked, totalDeleted);
  }

  private void storeInIndex(String indexName, PublicationIndexEntry publicationIndexEntry, Publication publication) {

    // Remove fulltext when storing in inProgress index
    if (this.inProgressPublicationsIndexName.equals(indexName)) {
      publicationIndexEntry = this.getPublicationIndexEntryWithoutFulltext(publicationIndexEntry);
    }

    publicationIndexEntry.setIndex(indexName);
    this.deleteFromIndexIfExists(indexName, publicationIndexEntry.getResId());
    this.publicationIndexingService.save(indexName, publicationIndexEntry);
    log.info("Publication '{}' ({}) ({}) indexed in '{}' index", publication.getResId(),
            StringTool.truncateWithEllipsis(publication.getTitle(), 20), publication.getArchiveId(), indexName);
  }

  private void deleteFromIndexIfExists(String indexName, String publicationId) {
    if (this.publicationIndexingService.findOne(indexName, publicationId, this.defaultFieldsRequestWithXml) != null) {
      log.info("Publication '{}' to delete found in index '{}'", publicationId, indexName);
      this.publicationIndexingService.deleteList(indexName, List.of(publicationId));
      log.info("indexingService.deleteList() was called with index '{}' and publicationId '{}'", indexName, publicationId);
    } else {
      log.warn("Publication '{}' to delete not found in index '{}'", publicationId, indexName);
    }
  }

  private PublicationIndexEntry getPublicationIndexEntryWithoutFulltext(PublicationIndexEntry publicationIndexEntry) {
    if (publicationIndexEntry.hasFulltext()) {
      PublicationIndexEntry indexEntryWithoutFulltext = new PublicationIndexEntry();
      indexEntryWithoutFulltext.setResId(publicationIndexEntry.getResId());
      indexEntryWithoutFulltext.setIndex(publicationIndexEntry.getIndex());
      Map<String, Object> metadataCopy = new HashMap<>(publicationIndexEntry.getMetadata());
      metadataCopy.remove(AouConstants.INDEX_FIELD_FULLTEXTS);
      indexEntryWithoutFulltext.setMetadata(metadataCopy);
      return indexEntryWithoutFulltext;
    } else {
      return publicationIndexEntry;
    }
  }
}
