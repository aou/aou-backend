/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU DataMgmt - IndexingJmsListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.DataMgmtController;
import ch.unige.aou.message.CleanIndexesMessage;
import ch.unige.aou.message.PublicationIndexingMessage;

@Service
@ConditionalOnBean(DataMgmtController.class)
public class IndexingJmsListener extends MessageProcessor<PublicationIndexingMessage> {

  private static final Logger log = LoggerFactory.getLogger(IndexingJmsListener.class);

  private final IndexingProcessingService indexingProcessingService;

  public IndexingJmsListener(AouProperties aouProperties, IndexingProcessingService indexingProcessingService) {
    super(aouProperties);
    this.indexingProcessingService = indexingProcessingService;
  }

  @JmsListener(destination = "${aou.queue.publicationsIndexing}")
  @Override
  public void receiveMessage(PublicationIndexingMessage publicationIndexingMessage) {
    this.sendForParallelProcessing(publicationIndexingMessage);
  }

  @JmsListener(destination = "${aou.queue.cleanIndexes}")
  public void receiveCleanIndexesMessage(CleanIndexesMessage cleanIndexesMessage) {
    this.indexingProcessingService.cleanIndexes();
  }

  @Override
  public void processMessage(PublicationIndexingMessage publicationIndexingMessage) {
    log.info("Reading message {}", publicationIndexingMessage);
    this.indexingProcessingService.indexPublication(publicationIndexingMessage.getResId());
    log.debug("Publication '{}' indexed", publicationIndexingMessage.getResId());
  }
}
