/*-
 * %%----------------------------------------------------------------------------------------------
 * Archive ouverte UNIGE - AoU DataMgmt - PublicationIndexedService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2022 - 2023 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.aou.service;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.util.SolidifyTime;

import ch.unige.aou.config.AouProperties;
import ch.unige.aou.controller.DataMgmtController;
import ch.unige.aou.message.ContributorUpdateMessage;
import ch.unige.aou.message.PublicationIndexedMessage;
import ch.unige.aou.model.index.PublicationIndexEntry;

@Service
@ConditionalOnBean(DataMgmtController.class)
public class PublicationIndexedService {

  private static final Logger log = LoggerFactory.getLogger(PublicationIndexedService.class);

  private final String publishedPublicationsIndexName;
  private final int maxNumberOfSecondsForPublicationIndexedMessage;

  private final IndexingService<PublicationIndexEntry> publicationIndexingService;
  private final FieldsRequest defaultFieldsRequestWithXml;

  public PublicationIndexedService(AouProperties aouProperties, IndexingService<PublicationIndexEntry> publicationIndexingService) {
    this.publicationIndexingService = publicationIndexingService;
    this.publishedPublicationsIndexName = aouProperties.getIndexing().getPublishedPublicationsIndexName();
    this.maxNumberOfSecondsForPublicationIndexedMessage = aouProperties.getIndexing().getMaxNumberOfSecondsForPublicationIndexedMessage();
    this.defaultFieldsRequestWithXml = aouProperties.getIndexing().getDefaultFieldsRequestWithXml();
  }

  @Async
  public void sendPublicationIndexedMessage(String publicationId, String archiveId, List<String> listCnIndividu, boolean isDeleted) {
    if (isDeleted) {
      // Do not need to wait for reindexation
      SolidifyEventPublisher.getPublisher().publishEvent(new PublicationIndexedMessage(publicationId, archiveId, true));
      log.info("PublicationIndexedMessage sent as publication '" + publicationId + "' was deleted from index '"
              + this.publishedPublicationsIndexName + "'");

      for (String cnIndividu : listCnIndividu) {
        log.info("ContributorUpdateMessage sent for contributor '" + cnIndividu + "' because publication '" + publicationId
                + "' was deleted from index '");
        SolidifyEventPublisher.getPublisher().publishEvent(new ContributorUpdateMessage(cnIndividu));
      }
    } else {
      // Wait for the publication to be found in the index, so that the SSR cache of the publication page can be generated correctly.
      boolean publicationFound = false;
      for (int i = 0; i < this.maxNumberOfSecondsForPublicationIndexedMessage; i++) {
        SolidifyTime.waitOneSecond();
        PublicationIndexEntry publicationIndexEntry = this.publicationIndexingService.findOne(this.publishedPublicationsIndexName, publicationId,
                this.defaultFieldsRequestWithXml);
        if (publicationIndexEntry != null) {
          OffsetDateTime lastIndexationDate = publicationIndexEntry.getLastIndexationDate();
          if (lastIndexationDate.isAfter(OffsetDateTime.now().minusHours(1))) {
            publicationFound = true;

            SolidifyEventPublisher.getPublisher()
                    .publishEvent(new PublicationIndexedMessage(publicationId, archiveId, false));
            log.info("PublicationIndexedMessage sent as publication '" + publicationId + "' was found in index '"
                    + this.publishedPublicationsIndexName + "' (lastIndexationDate=" + lastIndexationDate + ")");

            Set<String> listImpactedCnIndividu = new HashSet<>();
            listImpactedCnIndividu.addAll(listCnIndividu);
            listImpactedCnIndividu.addAll(publicationIndexEntry.getContributorsCnIndividus());
            for (String cnIndividu : listImpactedCnIndividu) {
              log.info("ContributorUpdateMessage sent for contributor '" + cnIndividu + "' because publication '" + publicationId
                      + "' was found in index '"
                      + this.publishedPublicationsIndexName + "' (lastIndexationDate=" + lastIndexationDate + ")");
              SolidifyEventPublisher.getPublisher().publishEvent(new ContributorUpdateMessage(cnIndividu));
            }
            break;
          } else {
            log.info("publication '" + publicationId + "' exists in index '" + this.publishedPublicationsIndexName
                    + "' but has not been reindexed yet");
          }
        } else {
          log.info("publication '" + publicationId + "' not found in index '" + this.publishedPublicationsIndexName + "' yet");
        }
      }

      if (!publicationFound) {
        // Publication could not be found in index.
        // -> Send message as if it were deleted in order to let the portal clean the publication page SSR cache.
        for (String cnIndividu : listCnIndividu) {
          SolidifyEventPublisher.getPublisher().publishEvent(new ContributorUpdateMessage(cnIndividu));
        }
        SolidifyEventPublisher.getPublisher().publishEvent(new PublicationIndexedMessage(publicationId, archiveId, true));
        log.warn("publication '" + publicationId + "' could not be found in index '" + this.publishedPublicationsIndexName + "' after "
                + this.maxNumberOfSecondsForPublicationIndexedMessage + " seconds");
      }
    }
  }
}
